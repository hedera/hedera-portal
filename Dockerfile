ARG IMAGE_SRC
FROM ${IMAGE_SRC}

USER root

# Install jq nodejs npm packages
RUN apk update --no-cache \
    && apk upgrade --no-cache \
    && apk add --no-cache jq nodejs npm logrotate \
    && npm install -g forever

COPY ./docker/nginx.conf /etc/nginx/nginx.conf
COPY ./docker/default.conf /etc/nginx/conf.d/default.conf
COPY ./docker/forever-log-rotate.log /etc/logrotate.d/forever

COPY ./docker/docker-entrypoint.sh /
RUN chmod a+x /docker-entrypoint.sh

# Copy app code to root directory
COPY ./dist/hedera-portal/browser/. /usr/share/nginx/html/
COPY ./dist/hedera-portal/server/. /usr/share/nginx/server/
COPY ./dist/hedera-portal/ssr-worker/. /usr/share/nginx/ssr-worker/

# Create folder that will be mounted to hold environment.runtime.json, server-environment.runtime.json and logs
RUN mkdir -p /usr/share/nginx/environments \
      # Give nginx permissions on web directory
      && chown -R 101:101 /usr/share/nginx/environments \
      && chmod -R g+w /usr/share/nginx/environments \
      && chown -R 101:101 /usr/share/nginx/html \
      && chmod -R g+w /usr/share/nginx/html \
      && chown -R 101:101 /usr/share/nginx/server \
      && chmod -R g+w /usr/share/nginx/server \
      && chown -R 101:101 /usr/share/nginx/ssr-worker \
      && chmod -R g+w /usr/share/nginx/ssr-worker \
      && touch /var/cache/nginx/forever-output.log \
      && chown -R root:101 /var/cache/nginx \
      && chmod -R g+w /var/cache/nginx \
      && chown -R 101:101 /usr/local/lib/node_modules

# Must be overriden if the application must be accessible from a subpath and not from the domain
# root. For example, an application accessible with this url: *.uk8stst1.unige.ch/my-path/
# must set a WEBCONTEXT="WEBCONTEXT"
# no trailing '/' and the begining or the end.
ENV WEBCONTEXT="/"

# Must be overriden with the value of the secret on runtime
ENV SERVER_CLEAN_CACHE_PASSWORD="ChangeMe"

# Set environnement variable to specify a path for forever(nodeja process)
# by default we send the logs to /dev/null to avoid verbose logs from forever ( sometime when there are SSR errors
# logs generated are huge )
ENV LOG_FILE="/dev/null"

USER 101

# Load the entrypoint script to be run later
ENTRYPOINT ["/docker-entrypoint.sh"]

# Run the script located inside the docker-entrypoint.sh file
CMD ["start"]
