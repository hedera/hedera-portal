/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - node-tools.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AppServerModule} from "@app/app.server.module";
import {CommonEngine} from "@angular/ssr";
/* eslint-disable no-console */
import domino from "domino";
import * as express from "express";
import basicAuth from "express-basic-auth";
import fs from "fs";
import fsp from "fs/promises";
import {createProxyMiddleware} from "http-proxy-middleware";
import {join} from "path";
import {NodeToolsModel} from "solidify-frontend";
import winston from "winston";
import winstonDailyRotateFile from "winston-daily-rotate-file";
import {WebSocket} from "ws";
import zlib from "zlib";
import "zone.js/node";

const md5 = require("md5");
const proxyConfLocal = require("./proxy.conf.local.js");
const workerThreads = require("node:worker_threads");

export const nodeTools: NodeToolsModel | any = {
  join: join,
  md5: md5,
  fs: fs,
  fsp: fsp,
  zlib: zlib,
  process: process,
  domino: domino,
  global: global,
  express: express,
  basicAuth: basicAuth,
  commonEngine: CommonEngine,
  serverModule: AppServerModule,
  proxyConfLocal: proxyConfLocal,
  createProxyMiddleware: createProxyMiddleware,
  webSocket: WebSocket,
  winston: winston,
  winstonDailyRotateFile: winstonDailyRotateFile,
  workerThreads: workerThreads,
};
