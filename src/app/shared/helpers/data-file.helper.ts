/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - data-file.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  DataFile,
  SourceDatasetFile,
} from "@models";
import {
  ArrayUtil,
  FileStatusEnum,
  SolidifyDataFileModel,
  StringUtil,
} from "solidify-frontend";

export class DataFileHelper {
  static readonly SEPARATOR: string = "/";
  static readonly ROOT: string = DataFileHelper.SEPARATOR;

  static dataFileAdapterWithoutStatus(dataFile: SourceDatasetFile): SolidifyDataFileModel {
    return {
      fileName: dataFile.fileName,
      fileSize: dataFile.fileSize,
      status: null,
    };
  }

  static dataFileAdapterWithStatusReady(dataFile: DataFile): SolidifyDataFileModel {
    return {
      fileName: dataFile?.fileName,
      fileSize: dataFile?.fileSize,
      mimetype: dataFile?.mimetype || dataFile?.["mimeType"],
      status: FileStatusEnum.READY,
    };
  }

  static dataFileAdapter(dataFile: DataFile): SolidifyDataFileModel {
    return {
      fileName: dataFile?.fileName,
      fileSize: dataFile?.fileSize,
      status: dataFile?.status,
      mimetype: dataFile?.mimetype || dataFile?.["mimeType"],
    };
  }

  static createIntermediateFolders(listFolders: string[]): string[] {
    let listNewIntermediateFolder = [];

    listFolders.forEach(path => {
      if (path === this.ROOT) {
        return;
      }
      this._recursivelyAddParentIfMissing(listFolders, listNewIntermediateFolder, path);
    });
    listNewIntermediateFolder = ArrayUtil.distinct(listNewIntermediateFolder);
    listFolders.push(...listNewIntermediateFolder);
    if (!listFolders.includes(this.ROOT)) {
      listFolders.push(this.ROOT);
    }
    this._sort(listFolders);
    return listNewIntermediateFolder;
  }

  private static _sort(listFolders: string[]): void {
    listFolders.sort((a, b) => {
      const aTransform = StringUtil.replaceAll(a, " ", "/").toLowerCase();
      const bTransform = StringUtil.replaceAll(b, " ", "/").toLowerCase();
      if (aTransform < bTransform) {
        return -1;
      }
      if (aTransform > bTransform) {
        return 1;
      }
      return 0;
    });
  }

  private static _recursivelyAddParentIfMissing(listFolders: string[], listNewIntermediateFolder: string[], path: string): void {
    const lastIndexSeparator = path.lastIndexOf(this.SEPARATOR);
    if (lastIndexSeparator === 0) {
      return;
    }
    const parentPath = path.substring(0, lastIndexSeparator);
    if (!listFolders.includes(parentPath)) {
      listNewIntermediateFolder.push(parentPath);
      this._recursivelyAddParentIfMissing(listFolders, listNewIntermediateFolder, parentPath);
    }
  }
}
