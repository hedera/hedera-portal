/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - toc.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {Renderer2} from "@angular/core";
import {environment} from "@environments/environment";
import {ApiEnum} from "@shared/enums/api.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {isNotNullNorUndefined} from "solidify-frontend";

export class TocHelper {
  static updateLinkToc(document: Document, renderer: Renderer2, classContainerLink: string, isUserGuide: boolean): void {
    let urlBase = ApiEnum.adminDocs;
    if (isUserGuide && isNotNullNorUndefined(environment.documentationTocUserGuidePath)) {
      urlBase = environment.documentationTocUserGuidePath;
    }
    const listA = document.querySelectorAll(`.${classContainerLink} a:not([active="true"])`);
    listA.forEach(a => {
      const existingHref = a.attributes["href"].value;
      renderer.setAttribute(a, "href", urlBase + urlSeparator + existingHref);
      renderer.setAttribute(a, "active", "true");
    });
  }
}
