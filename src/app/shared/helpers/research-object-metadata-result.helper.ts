/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - research-object-metadata-result.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AbstractResearchElementMetadataResult} from "@models";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  DataTableColumns,
  DataTableFieldTypeEnum,
  OrderEnum,
} from "solidify-frontend";

export class ResearchObjectMetadataResultHelper {
  static columns: DataTableColumns<AbstractResearchElementMetadataResult>[] = [
    {
      field: "metadata.uri" as any,
      header: LabelTranslateEnum.uri,
      type: DataTableFieldTypeEnum.string,
      order: OrderEnum.none,
      isSortable: true,
      sortableField: "uriSort" as any,
      isFilterable: false,
    },
    {
      field: "metadata.objectDate" as any,
      header: LabelTranslateEnum.date,
      type: DataTableFieldTypeEnum.datetime,
      order: OrderEnum.none,
      isSortable: true,
      sortableField: "objectDate" as any,
      isFilterable: false,
      width: "40px",
    },
    {
      field: "metadata.researchProject.name" as any,
      header: LabelTranslateEnum.project,
      type: DataTableFieldTypeEnum.string,
      order: OrderEnum.none,
      isSortable: true,
      sortableField: "projects" as any,
      isFilterable: false,
    },
    {
      field: "metadata.researchObjectType.name" as any,
      header: LabelTranslateEnum.researchObjectType,
      type: DataTableFieldTypeEnum.string,
      order: OrderEnum.none,
      isSortable: true,
      sortableField: "research-object-types" as any,
      isFilterable: false,
    },
  ];
}
