/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - research-element.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {MetadataUtil} from "@home/helpers/metadata.util";
import {
  AbstractResearchElementMetadataResult,
  ResearchDataFileMetadataResult,
} from "@models";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {DataFileHelper} from "@shared/helpers/data-file.helper";
import {
  Observable,
  of,
} from "rxjs";
import {
  catchError,
  map,
} from "rxjs/operators";
import {
  DownloadService,
  FileInput,
  FileVisualizerHelper,
  ImageFileVisualizerService,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  MappingObject,
  MappingObjectUtil,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

export class ResearchElementHelper {
  static dataFileThumbnailBlobUrlMap: MappingObject<string, string> = {};
  static projectThumbnailBlobUrlMap: MappingObject<string, string> = {};

  static getProjectThumbnailUrl(downloadService: DownloadService, currentResearchElement: AbstractResearchElementMetadataResult): Observable<string> {
    const project = currentResearchElement?.metadata?.researchProject;
    const key = project.shortName;
    const downloadFileName = project.shortName;
    const thumbnailUrl = `${ApiEnum.accessProjects}/${project.shortName}/${ApiActionNameEnum.DOWNLOAD_LOGO}`;
    return this._downloadAndStoreThumbnail(downloadService, this.projectThumbnailBlobUrlMap, thumbnailUrl, key, downloadFileName);
  }

  static getDataFileThumbnailUrl(downloadService: DownloadService, imageFileVisualizerService: ImageFileVisualizerService, currentResearchElement: AbstractResearchElementMetadataResult, researchDataFile?: ResearchDataFileMetadataResult): Observable<string> {
    if (MetadataUtil.isResearchDataFile(currentResearchElement)) {
      researchDataFile = MetadataUtil.getResearchDataFileResult(currentResearchElement);
    }
    if (isNullOrUndefined(researchDataFile)) {
      return of(undefined);
    }
    const fileName = researchDataFile.metadata.fileName;
    const fileExtension = FileVisualizerHelper.getFileExtension(fileName);
    const fileInfo: FileInput = {
      dataFile: DataFileHelper.dataFileAdapterWithStatusReady({
        fileName: fileName,
        fileSize: researchDataFile.metadata.fileSize,
        mimetype: researchDataFile.metadata.mimeType,
      }),
      fileExtension: fileExtension,
    };
    const project = researchDataFile.metadata.researchProject;
    const key = `${project.shortName}/${researchDataFile.metadata.fileName}`;
    const downloadFileName = researchDataFile.metadata.fileName;
    const mimeType = researchDataFile.metadata.mimeType;
    let thumbnailUrl: string;
    if (researchDataFile.metadata.accessibleFrom === Enums.ResearchDataFile.AccessibleFromEnum.IIIF) {
      thumbnailUrl = MetadataUtil.getIiifImageUrlForThumbnail(researchDataFile);
    } else if (FileVisualizerHelper.canHandle([imageFileVisualizerService], fileInfo)) {
      thumbnailUrl = MetadataUtil.getFileDownloadUrl(researchDataFile);
    }
    if (isNullOrUndefined(thumbnailUrl)) {
      return of(undefined);
    }
    return this._downloadAndStoreThumbnail(downloadService, this.dataFileThumbnailBlobUrlMap, thumbnailUrl, key, downloadFileName, mimeType);
  }

  private static _downloadAndStoreThumbnail(downloadService: DownloadService, thumbnailBlobUrlMap: MappingObject<string, string>, url: string, key: string, fileName: string, mimeType?: string): Observable<string> {
    let blobUrl = MappingObjectUtil.get(thumbnailBlobUrlMap, key);
    if (isNotNullNorUndefinedNorWhiteString(blobUrl)) {
      return of(blobUrl);
    }
    return downloadService.downloadInMemory(url, fileName, false, mimeType).pipe(
      map(blob => {
        blobUrl = URL.createObjectURL(blob);
        MappingObjectUtil.set(thumbnailBlobUrlMap, key, blobUrl);
        return blobUrl;
      }),
      catchError(error => {
        throw environment.errorToSkipInErrorHandler;
      }),
    );
  }

  static getDownloadTokenDownloadIfNeeded(downloadService: DownloadService, isAccessPublic: boolean = true, downloadTokenUrl: string): Observable<boolean | undefined> {
    if (isAccessPublic) {
      return of(true);
    } else {
      return downloadService.getAndStoreDownloadToken(downloadTokenUrl).pipe(
        map(token => false),
      );
    }
  }

  static getIiifDownloadToken(url: string): string {
    const path = url.substring(ApiEnum.iiif.length + 1);
    const protocolProject = path.split(SOLIDIFY_CONSTANTS.URL_SEPARATOR).slice(0, 2);
    return `${ApiEnum.iiif}/${protocolProject.join(SOLIDIFY_CONSTANTS.URL_SEPARATOR)}/${ApiActionNameEnum.DOWNLOAD_TOKEN}`;
  }
}
