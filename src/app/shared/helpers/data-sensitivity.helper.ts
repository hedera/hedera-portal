/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - data-sensitivity.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {Enums} from "@enums";
import {isNotNullNorUndefined} from "solidify-frontend";

export class DataSensitivityHelper {
  static getColor(dataSensitivity: Enums.Dataset.DataSensitivityEnum): DataSensitivityColor {
    switch (dataSensitivity) {
      case Enums.Dataset.DataSensitivityEnum.BLUE:
        return "blue";
      case Enums.Dataset.DataSensitivityEnum.GREEN:
        return "green";
      case Enums.Dataset.DataSensitivityEnum.YELLOW:
        return "yellow";
      case Enums.Dataset.DataSensitivityEnum.ORANGE:
        return "orange";
      case Enums.Dataset.DataSensitivityEnum.RED:
        return "red";
      case Enums.Dataset.DataSensitivityEnum.CRIMSON:
        return "crimson";
      case Enums.Dataset.DataSensitivityEnum.UNDEFINED:
      default:
        return "undefined";
    }
  }

  static isPartiallySupported(dataSensitivity: Enums.Dataset.DataSensitivityEnum): boolean {
    return isNotNullNorUndefined(dataSensitivity) &&
      (dataSensitivity === Enums.Dataset.DataSensitivityEnum.ORANGE
        || dataSensitivity === Enums.Dataset.DataSensitivityEnum.RED
        || dataSensitivity === Enums.Dataset.DataSensitivityEnum.CRIMSON);
  }
}

export type DataSensitivityColor = "blue" | "green" | "yellow" | "orange" | "red" | "crimson" | "undefined";
