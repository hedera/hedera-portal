/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - user.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {User} from "@models";
import {
  isEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  StringUtil,
} from "solidify-frontend";

export class UserHelper {
  static extraInfoSecondLineLabelCallback: (user: User) => string = (user) => {
    let extraLabel = StringUtil.stringEmpty;
    if (isNullOrUndefined(user.person)) {
      return extraLabel;
    }
    if (isNotNullNorUndefined(user.person.institutions)) {
      extraLabel = user.person.institutions.join(", ");
    }
    if (user.lastName !== user.person.lastName || user.firstName !== user.person.firstName) {
      if (!isEmptyString(extraLabel)) {
        extraLabel += " - ";
      }
      extraLabel += user.person.lastName + " " + user.person.firstName;
    }
    return extraLabel;
  };
}
