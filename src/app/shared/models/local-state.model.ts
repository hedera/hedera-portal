/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - local-state.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {AdminStateModel} from "@app/features/admin/stores/admin.state";
import {IngestStateModel} from "@app/features/ingest/stores/ingest.state";
import {BrowseStateModel} from "@app/features/browse/stores/browse.state";
import {StateEnum} from "@shared/enums/state.enum";
import {HomeStateModel} from "@shared/stores/home/home.state";
import {SharedStateModel} from "@app/shared/stores/shared.state";
import {AppStateModel} from "@app/stores/app.state";
import {RouterStateModel} from "@ngxs/router-plugin";
import {StateModel} from "solidify-frontend";

export class LocalStateModel implements StateModel {
  router: RouterStateModel;
  [StateEnum.application]: AppStateModel;
  [StateEnum.home]: HomeStateModel;
  [StateEnum.ingest]: IngestStateModel;
  [StateEnum.browse]: BrowseStateModel;
  [StateEnum.admin]: AdminStateModel;
  [StateEnum.shared]: SharedStateModel;
}
