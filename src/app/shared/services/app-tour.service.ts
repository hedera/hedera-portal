/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - app-tour.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  Inject,
  Injectable,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {Store} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  RoutesEnum,
  SourceDatasetRoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StepTourSectionNameEnum} from "@shared/enums/step-tour-section-name.enum";
import {TourRouteIdEnum} from "@shared/enums/tour-route-id.enum";
import {TourEnum} from "@shared/enums/tour.enum";
import {
  IStepOption,
  TourService,
} from "ngx-ui-tour-md-menu";
import {
  AbstractAppTourService,
  DefaultSolidifyEnvironment,
  ENVIRONMENT,
  ITourSection,
  LABEL_TRANSLATE,
  LabelTranslateInterface,
  MARK_AS_TRANSLATABLE,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class AppTourService extends AbstractAppTourService {
  constructor(protected readonly _store: Store,
              protected readonly _matDialog: MatDialog,
              protected readonly _tourService: TourService,
              @Inject(LABEL_TRANSLATE) protected readonly _labelTranslate: LabelTranslateInterface,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super(_store, _matDialog, _tourService, tourSteps, _labelTranslate, _environment);
  }
}

const mainTourSteps: IStepOption[] = [
  {
    anchorId: TourEnum.mainSearchBar,
    content: MARK_AS_TRANSLATABLE("tour.main.searchBar.content"),
    route: RoutesEnum.homePage,
  },
  {
    anchorId: TourEnum.mainMenuDataset,
    content: MARK_AS_TRANSLATABLE("tour.main.menuDataset.content"),
  },
];

// const datasetListTourSteps: IStepOption[] = [
//   {
//     anchorId: TourEnum.commonContent,
//     title: LabelTranslateEnum.ingest,
//     content: MARK_AS_TRANSLATABLE("tour.ingest.list.intro.content"),
//     route: RoutesEnum.ingest + urlSeparator + TourRouteIdEnum.tourProjectId + urlSeparator + DatasetTabStatusEnum.inProgress,
//   },
//   {
//     anchorId: TourEnum.datasetCreate,
//     content: MARK_AS_TRANSLATABLE("tour.ingest.list.create.content"),
//   },
//   {
//     anchorId: TourEnum.datasetStatusTabs,
//     content: MARK_AS_TRANSLATABLE("tour.ingest.list.statusTabs.content"),
//   },
//   {
//     anchorId: TourEnum.datasetProjectSelector,
//     content: MARK_AS_TRANSLATABLE("tour.ingest.list.projectSelector.content"),
//   },
// ];

const datasetMetadataTourSteps: IStepOption[] = [
  {
    anchorId: TourEnum.datasetSourceDatasetMetadata,
    content: MARK_AS_TRANSLATABLE("tour.ingest.metadata.intro.content"),
    route: RoutesEnum.ingest + urlSeparator + TourRouteIdEnum.tourProjectId + urlSeparator + SourceDatasetRoutesEnum.detail + urlSeparator + TourRouteIdEnum.tourDatasetId + urlSeparator + SourceDatasetRoutesEnum.metadata,
  },
  {
    anchorId: TourEnum.datasetSourceDatasetMetadataThumbnail,
    content: MARK_AS_TRANSLATABLE("tour.ingest.metadata.thumbnail.content"),
  },
];

const datasetDataTourSteps: IStepOption[] = [
  {
    anchorId: TourEnum.datasetSourceDatasetUpload,
    content: MARK_AS_TRANSLATABLE("tour.ingest.data.upload.content"),
    route: RoutesEnum.ingest + urlSeparator + TourRouteIdEnum.tourProjectId + urlSeparator + SourceDatasetRoutesEnum.detail + urlSeparator + TourRouteIdEnum.tourDatasetId + urlSeparator + SourceDatasetRoutesEnum.data,
  },
  {
    anchorId: TourEnum.datasetSourceDatasetMetadata,
    content: MARK_AS_TRANSLATABLE("tour.ingest.data.remindTooltip.content"),
  },
];

const homeSearchTourSteps: IStepOption[] = [
  {
    anchorId: TourEnum.commonContent,
    title: LabelTranslateEnum.search,
    content: MARK_AS_TRANSLATABLE("tour.home.intro.content"),
    route: RoutesEnum.homeSearch,
  },
  {
    anchorId: TourEnum.homeSearchSearchBar,
    content: MARK_AS_TRANSLATABLE("tour.home.searchBar.content"),
  },
  {
    anchorId: TourEnum.homeSearchListTiles,
    content: MARK_AS_TRANSLATABLE("tour.home.listTiles.content"),
  },
  {
    anchorId: TourEnum.homeSearchFacets,
    content: MARK_AS_TRANSLATABLE("tour.home.facets.content"),
  },
];

const tourSteps: ITourSection[] = [
  {
    key: StepTourSectionNameEnum.main,
    stepTour: mainTourSteps,
  },
  // {
  //   key: StepTourSectionNameEnum.ingest,
  //   stepTour: datasetListTourSteps,
  // },
  {
    key: StepTourSectionNameEnum.datasetMetadata,
    stepTour: datasetMetadataTourSteps,
  },
  {
    key: StepTourSectionNameEnum.datasetData,
    stepTour: datasetDataTourSteps,
  },
  {
    key: StepTourSectionNameEnum.search,
    stepTour: homeSearchTourSteps,
  },
];
