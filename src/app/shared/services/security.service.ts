/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - security.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {AppMemberProjectState} from "@app/stores/member-project/app-member-project.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Project,
  Role,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  Observable,
  of,
} from "rxjs";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  MemoizedUtil,
  SolidifySecurityService,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class SecurityService extends SolidifySecurityService {

  private readonly _DATASET_ROLE_NEED_TO_CREATE: Enums.Role.RoleEnum[] = [Enums.Role.RoleEnum.MANAGER, Enums.Role.RoleEnum.CREATOR];
  private readonly _DATASET_ROLE_NEED_TO_EDIT: Enums.Role.RoleEnum[] = [Enums.Role.RoleEnum.MANAGER, Enums.Role.RoleEnum.CREATOR];

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions) {
    super(_store, _actions$, environment);
  }

  canCreateDatasetOnProject(projectId: string, adminBypassProjectRole: boolean = true): boolean {
    if (isNullOrUndefinedOrWhiteString(projectId)) {
      return false;
    }
    if (this.isRootOrAdminIfBypassRole(adminBypassProjectRole)) {
      return true;
    }
    if (!this.isMemberOfProjectAboveVisitor(projectId)) {
      return false;
    }
    return this._currentUserHaveRoleInListForProject(projectId, this._DATASET_ROLE_NEED_TO_CREATE);
  }

  canEditDatasetOnProject(projectId: string, adminBypassProjectRole: boolean = true): boolean {
    if (isNullOrUndefinedOrWhiteString(projectId)) {
      return false;
    }
    if (this.isRootOrAdminIfBypassRole(adminBypassProjectRole)) {
      return true;
    }
    if (!this.isMemberOfProjectAboveVisitor(projectId)) {
      return false;
    }
    return this._currentUserHaveRoleInListForProject(projectId, this._DATASET_ROLE_NEED_TO_EDIT);
  }

  private _isRoleInProject(expectedRole: Enums.Role.RoleEnum[], projectId: string, adminBypassProjectRole: boolean = true): boolean {
    if (isNullOrUndefinedOrWhiteString(projectId)) {
      return false;
    }
    if (this.isRootOrAdminIfBypassRole(adminBypassProjectRole)) {
      return true;
    }
    if (!this.isMemberOfProject(projectId)) {
      return false;
    }
    return this._currentUserHaveRoleInListForProject(projectId, expectedRole);
  }

  isManagerOfProject(projectId: string, adminBypassProjectRole: boolean = true): boolean {
    return this._isRoleInProject([Enums.Role.RoleEnum.MANAGER], projectId, adminBypassProjectRole);
  }

  isManagerOfAnyProject(projectList: Project[] = this.getListMemberProjects()): boolean {
    const result: boolean = projectList?.some(o => this.isManagerOfProject(o.resId));
    return result;
  }

  canSeeDetailSourceDatasetById(datasetId: string, adminBypassProjectRole: boolean = true): Observable<boolean> {
    if (this.isRootOrAdminIfBypassRole(adminBypassProjectRole)) {
      return of(true);
    }
    return of(true);

    // let currentDataset = MemoizedUtil.currentSnapshot(this._store, DatasetState);
    // if (isNullOrUndefined(currentDataset) || currentDataset.resId !== datasetId) {
    //   return this._store.dispatch(new DatasetAction.GetById(datasetId)).pipe(
    //     map(state => {
    //       currentDataset = MemoizedUtil.currentSnapshot(this._store, DatasetState);
    //       return this.isMemberOfProject(currentDataset.projectId);
    //     }),
    //   );
    // } else {
    //   return of(this.isMemberOfProject(currentDataset.projectId));
    // }
  }

  isRootOrAdminIfBypassRole(adminBypassProjectRole: boolean = true): boolean {
    if (adminBypassProjectRole) {
      return super.isRootOrAdmin();
    }
    return super.isRoot();
  }

  // private _datasetEditionModeStep(dataset: Dataset): IngestEditModeEnum {
  //   if (SecurityService._datasetInState(dataset, this._DATASET_STATUS_ALLOW_METADATA_ALTERATION)) {
  //     return IngestEditModeEnum.metadata;
  //   }
  //   if (SecurityService._datasetInState(dataset, this._DATASET_STATUS_ALLOW_FULL_ALTERATION)) {
  //     return IngestEditModeEnum.full;
  //   }
  //   return IngestEditModeEnum.none;
  // }

  private static _isExpectedRoleOnListAuthorized(currentRoleOnProject: Role, listRolesAuthorized: Enums.Role.RoleEnum[]): boolean {
    return listRolesAuthorized.includes(currentRoleOnProject?.resId as Enums.Role.RoleEnum);
  }

  canEditDatasetById(datasetId: string, adminBypassProjectRole: boolean = true): Observable<boolean> {
    if (this.isRootOrAdminIfBypassRole(adminBypassProjectRole)) {
      return of(true);
    }
    return of(false);

    // return this.canSeeDetailDatasetById(datasetId).pipe(
    //   map((isAuthorized: boolean) => {
    //     if (isFalse(isAuthorized)) {
    //       return false;
    //     }
    //     const currentDataset = MemoizedUtil.currentSnapshot(this._store, DatasetState);
    //     if (!this._datasetEditionModeStep(currentDataset)) {
    //       return false;
    //     }
    //     return this._currentUserHaveRoleInListForProject(currentDataset.projectId, this._DATASET_ROLE_NEED_TO_EDIT);
    //   }),
    // );
  }

  private _currentUserHaveRoleInListForProject(projectId: string, listRolesAuthorized: Enums.Role.RoleEnum[]): boolean {
    const currentRoleInProject = this.getRoleInProject(projectId);
    if (isNullOrUndefined(currentRoleInProject)) {
      return false;
    }
    return SecurityService._isExpectedRoleOnListAuthorized(currentRoleInProject, listRolesAuthorized);
  }

  getRoleInProject(projectId: string): Role | undefined {
    return this.getListMemberProjects()?.find(o => isNotNullNorUndefined(o.role) && o.resId === projectId)?.role;
  }

  getListMemberProjects(): Project[] {
    return MemoizedUtil.listSnapshot(this._store, AppMemberProjectState);
  }

  getRoleEnumInProject(projectId: string): Enums.Role.RoleEnum | undefined {
    return this.getRoleInProject(projectId)?.resId as Enums.Role.RoleEnum;
  }

  isMemberOfProject(projectId: string): boolean {
    return isNotNullNorUndefined(this.getRoleInProject(projectId));
  }

  isMemberOfProjectAboveVisitor(projectId: string): boolean {
    const role: Role = this.getRoleInProject(projectId);
    return isNotNullNorUndefined(role) && role.resId !== Enums.Role.RoleEnum.VISITOR;
  }
}
