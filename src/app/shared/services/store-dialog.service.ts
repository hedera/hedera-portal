/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - store-dialog.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminFundingAgenciesActionNameSpace} from "@admin/funding-agencies/stores/admin-funding-agencies.action";
import {adminIIIFCollectionSettingsActionNameSpace} from "@admin/iiif-collection-settings/stores/admin-iiif-collection-settings.action";
import {adminInstitutionActionNameSpace} from "@admin/institution/stores/admin-institution.action";
import {adminLanguageActionNameSpace} from "@admin/language/stores/admin-language.action";
import {adminLicenseActionNameSpace} from "@admin/license/stores/admin-license.action";
import {adminOntologyActionNameSpace} from "@admin/ontology/stores/admin-ontology.action";
import {adminPersonActionNameSpace} from "@admin/person/stores/admin-person.action";
import {adminProjectActionNameSpace} from "@admin/project/stores/admin-project.action";
import {adminResearchObjectTypeActionNameSpace} from "@admin/research-object-type/stores/admin-research-object-type.action";
import {adminRmlActionNameSpace} from "@admin/rml/stores/admin-rml.action";
import {adminScheduledTaskActionNameSpace} from "@admin/scheduled-task/stores/admin-scheduled-task.action";
import {adminUserActionNameSpace} from "@admin/user/stores/admin-user.action";
import {
  Inject,
  Injectable,
} from "@angular/core";
import {ingestRdfDatasetFileActionNameSpace} from "@app/features/ingest/features/rdf-dataset-file/stores/rdf-dataset-file/ingest-rdf-dataset-file.action";
import {ingestResearchDataFileActionNameSpace} from "@app/features/ingest/features/research-data-file/stores/research-data-file/ingest-research-data-file.action";
import {ingestSourceDatasetFileActionNameSpace} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/ingest-source-dataset-file.action";
import {ingestSourceDatasetActionNameSpace} from "@app/features/ingest/features/source-dataset/stores/source-dataset/ingest-source-dataset.action";
import {environment} from "@environments/environment";
import {StateEnum} from "@shared/enums/state.enum";
import {
  AbstractStoreDialogService,
  DeleteDialogData,
  isNotNullNorUndefined,
  LABEL_TRANSLATE,
  LabelTranslateInterface,
  MARK_AS_TRANSLATABLE,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class StoreDialogService extends AbstractStoreDialogService {
  constructor(@Inject(LABEL_TRANSLATE) protected readonly _labelTranslate: LabelTranslateInterface) {
    super(environment, _labelTranslate);
  }

  protected _deleteDataInternal(state: StateEnum): DeleteDialogData | undefined {
    const sharedDeleteDialogData = {} as DeleteDialogData;

    if (state === StateEnum.ingest_sourceDataset) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("ingest.sourceDataset.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = ingestSourceDatasetActionNameSpace;
    }
    if (state === StateEnum.ingest_sourceDataset_file) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("ingest.sourceDataset.dialog.delete.message");
      sharedDeleteDialogData.compositionNameSpace = ingestSourceDatasetFileActionNameSpace;
    }
    if (state === StateEnum.ingest_researchDataFile) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("ingest.researchDataFile.dialog.delete.message");
      sharedDeleteDialogData.compositionNameSpace = ingestResearchDataFileActionNameSpace;
    }
    if (state === StateEnum.ingest_rdfDatasetFile) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("ingest.rdfDatasetFile.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = ingestRdfDatasetFileActionNameSpace;
    }
    if (state === StateEnum.admin_project) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.project.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminProjectActionNameSpace;
    }
    if (state === StateEnum.admin_license) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.license.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminLicenseActionNameSpace;
    }
    if (state === StateEnum.admin_institution) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.institution.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminInstitutionActionNameSpace;
    }
    if (state === StateEnum.admin_user) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.user.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminUserActionNameSpace;
    }
    if (state === StateEnum.admin_person) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.person.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminPersonActionNameSpace;
    }
    if (state === StateEnum.admin_fundingAgencies) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.funding-agencies.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminFundingAgenciesActionNameSpace;
    }
    if (state === StateEnum.admin_language) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.language.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminLanguageActionNameSpace;
    }
    if (state === StateEnum.admin_scheduledTask) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.scheduledTask.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminScheduledTaskActionNameSpace;
    }
    if (state === StateEnum.admin_ontology) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.ontology.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminOntologyActionNameSpace;
    }
    if (state === StateEnum.admin_researchObjectTypes) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.researchObjectType.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminResearchObjectTypeActionNameSpace;
    }
    if (state === StateEnum.admin_iiifCollectionSettings) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.iiifCollectionSettings.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminIIIFCollectionSettingsActionNameSpace;
    }
    if (state === StateEnum.admin_rml) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.rml.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminRmlActionNameSpace;
    }
    if (isNotNullNorUndefined(sharedDeleteDialogData.message)) {
      return sharedDeleteDialogData;
    }
    return undefined;
  }

}
