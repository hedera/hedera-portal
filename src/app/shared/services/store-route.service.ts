/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - store-route.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {AbstractStoreRouteService} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class StoreRouteService extends AbstractStoreRouteService {
  constructor() {
    super(environment);
  }

  protected _getCreateRouteInternal(state: StateEnum): string | undefined {
    if (state === StateEnum.ingest_sourceDataset) {
      return RoutesEnum.ingestSourceDatasetCreate;
    }
    if (state === StateEnum.admin_project) {
      return RoutesEnum.adminProjectCreate;
    }
    if (state === StateEnum.admin_license) {
      return RoutesEnum.adminLicenseCreate;
    }
    if (state === StateEnum.admin_institution) {
      return RoutesEnum.adminInstitutionCreate;
    }
    if (state === StateEnum.admin_person) {
      return RoutesEnum.adminPersonCreate;
    }
    if (state === StateEnum.admin_role) {
      return RoutesEnum.adminRoleCreate;
    }
    if (state === StateEnum.admin_fundingAgencies) {
      return RoutesEnum.adminFundingAgenciesCreate;
    }
    if (state === StateEnum.admin_scheduledTask) {
      return RoutesEnum.adminScheduledTaskCreate;
    }
    if (state === StateEnum.admin_ontology) {
      return RoutesEnum.adminOntologyCreate;
    }
    if (state === StateEnum.admin_researchObjectTypes) {
      return RoutesEnum.adminResearchObjectTypeCreate;
    }
    if (state === StateEnum.admin_iiifCollectionSettings) {
      return RoutesEnum.adminIIIFCollectionSettingsCreate;
    }
    if (state === StateEnum.admin_rml) {
      return RoutesEnum.adminRmlCreate;
    }
    return undefined;
  }

  protected _getDetailRouteInternal(state: StateEnum): string | undefined {
    if (state === StateEnum.ingest_sourceDataset) {
      return RoutesEnum.ingestSourceDatasetDetail;
    }
    if (state === StateEnum.browse_ontology) {
      return RoutesEnum.browseOntologyDetail;
    }
    if (state === StateEnum.admin_project) {
      return RoutesEnum.adminProjectDetail;
    }
    if (state === StateEnum.admin_license) {
      return RoutesEnum.adminLicenseDetail;
    }
    if (state === StateEnum.admin_institution) {
      return RoutesEnum.adminInstitutionDetail;
    }
    if (state === StateEnum.admin_user) {
      return RoutesEnum.adminUserDetail;
    }
    if (state === StateEnum.admin_person) {
      return RoutesEnum.adminPersonDetail;
    }
    if (state === StateEnum.admin_role) {
      return RoutesEnum.adminRoleDetail;
    }
    if (state === StateEnum.admin_fundingAgencies) {
      return RoutesEnum.adminFundingAgenciesDetail;
    }
    if (state === StateEnum.admin_scheduledTask) {
      return RoutesEnum.adminScheduledTaskDetail;
    }
    if (state === StateEnum.admin_ontology) {
      return RoutesEnum.adminOntologyDetail;
    }
    if (state === StateEnum.admin_researchObjectTypes) {
      return RoutesEnum.adminResearchObjectTypeDetail;
    }
    if (state === StateEnum.admin_iiifCollectionSettings) {
      return RoutesEnum.adminIIIFCollectionSettingsDetail;
    }
    if (state === StateEnum.admin_rml) {
      return RoutesEnum.adminRmlDetail;
    }
    if (state === StateEnum.browse_project) {
      return RoutesEnum.browseProjectDetail;
    }
    return undefined;
  }

  protected _getRootRouteInternal(state: StateEnum): string | undefined {
    if (state === StateEnum.ingest_sourceDataset) {
      return RoutesEnum.ingestSourceDataset;
    }
    if (state === StateEnum.browse_ontology) {
      return RoutesEnum.browseOntology;
    }
    if (state === StateEnum.admin_project) {
      return RoutesEnum.adminProject;
    }
    if (state === StateEnum.admin_license) {
      return RoutesEnum.adminLicense;
    }
    if (state === StateEnum.admin_institution) {
      return RoutesEnum.adminInstitution;
    }
    if (state === StateEnum.admin_user) {
      return RoutesEnum.adminUser;
    }
    if (state === StateEnum.admin_person) {
      return RoutesEnum.adminPerson;
    }
    if (state === StateEnum.admin_role) {
      return RoutesEnum.adminRole;
    }
    if (state === StateEnum.admin_fundingAgencies) {
      return RoutesEnum.adminFundingAgencies;
    }
    if (state === StateEnum.admin_scheduledTask) {
      return RoutesEnum.adminScheduledTask;
    }
    if (state === StateEnum.admin_ontology) {
      return RoutesEnum.adminOntology;
    }
    if (state === StateEnum.admin_researchObjectTypes) {
      return RoutesEnum.adminResearchObjectType;
    }
    if (state === StateEnum.admin_iiifCollectionSettings) {
      return RoutesEnum.adminIIIFCollectionSettings;
    }
    if (state === StateEnum.admin_rml) {
      return RoutesEnum.adminRml;
    }
    if (state === StateEnum.browse_project) {
      return RoutesEnum.browse;
    }
    return undefined;
  }
}
