/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - translate-facet.pipe.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  Pipe,
  PipeTransform,
} from "@angular/core";
import {AppState} from "@app/stores/app.state";
import {Enums} from "@enums";
import {
  FacetProperty,
  Label,
  SystemProperty,
} from "@models";
import {Store} from "@ngxs/store";
import {SharedAbstractPipe} from "@shared/pipes/shared-abstract/shared-abstract.pipe";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {
  AppSystemPropertyState,
  isNullOrUndefined,
  MemoizedUtil,
} from "solidify-frontend";

@Pipe({
  name: "translateFacet",
  pure: false,
})
export class TranslateFacetPipe extends SharedAbstractPipe implements PipeTransform {
  appLanguageObs: Observable<Enums.Language.LanguageEnum> = MemoizedUtil.select(this._store, AppState, state => state.appLanguage);
  systemPropertyObs: Observable<SystemProperty> = MemoizedUtil.select(this._store, AppSystemPropertyState, state => state.current);

  currentLanguage: Enums.Language.LanguageEnum;
  searchFacetProperties: FacetProperty[];

  lastReturnedValue: string;
  lastReturnLanguage: Enums.Language.LanguageEnum;

  constructor(private readonly _store: Store) {
    super();
    this.subscribe(this.appLanguageObs.pipe(tap(value => {
      this.currentLanguage = value;
      this.lastReturnLanguage = null;
    })));

    this.subscribe(this.systemPropertyObs.pipe(tap(value => {
      this.searchFacetProperties = value.searchFacets;
    })));
  }

  transform(facetName: string): string {
    if (this.currentLanguage === this.lastReturnLanguage && !isNullOrUndefined(this.lastReturnedValue)) {
      return this.lastReturnedValue;
    } else {
      let value: string = facetName;
      if (!isNullOrUndefined(this.searchFacetProperties) && !isNullOrUndefined(this.currentLanguage)) {
        const facetProperties: FacetProperty[] = this.searchFacetProperties.filter(facetProp => facetProp.name === facetName);

        if (facetProperties.length === 1) {
          const facetProperty: FacetProperty = facetProperties[0];
          const currentLang: string = this.currentLanguage;
          const labels: Label[] = facetProperty.labels.filter(label => label.language === currentLang);

          if (labels.length === 1) {
            value = labels[0].text;
          }
        }
      }

      this.lastReturnLanguage = this.currentLanguage;
      this.lastReturnedValue = value;
      return value;
    }
  }
}
