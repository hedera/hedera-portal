/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - project-role-guard.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {Injectable} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
} from "@angular/router";
import {Store} from "@ngxs/store";
import {AppRoutesEnum} from "@shared/enums/routes.enum";
import {HederaRouteData} from "@shared/models/hedera-route.model";
import {SecurityService} from "@shared/services/security.service";
import {
  ApiService,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class ProjectRoleGuardService implements CanActivate {
  constructor(public readonly router: Router,
              public readonly store: Store,
              public readonly apiService: ApiService,
              private readonly _securityService: SecurityService,
              private readonly _notificationService: NotificationService) {
  }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const data = route.data as HederaRouteData;
    const projectPermissionNeed = isNullOrUndefined(data) ? [] : data.projectPermissionNeed;
    let projectId = route.parent.params[AppRoutesEnum.paramIdProjectWithoutPrefixParam];
    if (isNullOrUndefined(projectId)) {
      projectId = route.parent.parent.params[AppRoutesEnum.paramIdProjectWithoutPrefixParam];
    }
    if (isNullOrUndefined(projectId)) {
      // eslint-disable-next-line no-console
      console.warn("Unable to extract projectId from url");
      return false;
    }
    if (projectPermissionNeed.length === 0) {
      return true;
    }
    if (this._securityService.isRootOrAdmin()) {
      return true;
    }
    const authorized = projectPermissionNeed.indexOf(this._securityService.getRoleEnumInProject(projectId)) !== -1;
    if (authorized === false) {
      this._notificationService.showWarning(MARK_AS_TRANSLATABLE("project.security.notification.noRightForThisAction"));
    }
    return authorized;
  }
}
