/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-role-guard-detail.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
} from "@angular/router";
import {AppState} from "@app/stores/app.state";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {
  AppRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {TourRouteIdEnum} from "@shared/enums/tour-route-id.enum";
import {SecurityService} from "@shared/services/security.service";
import {
  Observable,
  of,
} from "rxjs";
import {take} from "rxjs/operators";
import {
  ApiService,
  MemoizedUtil,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class IngestRoleGuardDetailService implements CanActivate {
  constructor(public readonly router: Router,
              public readonly store: Store,
              public readonly apiService: ApiService,
              private readonly _securityService: SecurityService) {
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const datasetId: string = route.params[AppRoutesEnum.paramIdWithoutPrefixParam];
    const isInTourMode = MemoizedUtil.selectSnapshot(this.store, AppState, state => state.isInTourMode);
    const isDatasetIdForTour = datasetId === TourRouteIdEnum.tourDatasetId;
    if (isDatasetIdForTour || isInTourMode) {
      const isAuthorizedToDisplayTour = isDatasetIdForTour && isInTourMode;
      if (!isAuthorizedToDisplayTour) {
        this.store.dispatch(new Navigate([RoutesEnum.ingest]));
      }
      return of(isAuthorizedToDisplayTour);
    }
    return this._securityService.canSeeDetailSourceDatasetById(datasetId).pipe(take(1));
  }
}
