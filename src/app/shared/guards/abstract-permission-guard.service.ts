/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - abstract-permission-guard.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from "@angular/router";
import {AppState} from "@app/stores/app.state";
import {environment} from "@environments/environment";
import {Store} from "@ngxs/store";
import {AppRoutesEnum} from "@shared/enums/routes.enum";
import {PublicUrlHelper} from "@shared/helpers/public-url.helper";
import {
  Observable,
  of,
  switchMap,
} from "rxjs";
import {
  filter,
  take,
} from "rxjs/operators";
import {
  AbstractBaseService,
  isFalse,
  isNotNullNorUndefinedNorWhiteString,
  isTrue,
  LoginMode,
  MappingObjectUtil,
  MemoizedUtil,
  OAuth2Service,
  UrlQueryParamHelper,
  UrlUtil,
} from "solidify-frontend";

export abstract class AbstractPermissionGuardService extends AbstractBaseService implements CanActivate {
  constructor(protected readonly _router: Router,
              protected readonly _store: Store,
              protected readonly _oauthService: OAuth2Service) {
    super();
  }

  canActivate(activatedRoute: ActivatedRouteSnapshot, routerState: RouterStateSnapshot): Observable<boolean> {
    return MemoizedUtil.select(this._store, AppState, state => state.isApplicationInitialized).pipe(
      filter(isApplicationInitialized => isApplicationInitialized),
      take(1),
      switchMap(() => this._shouldNavigate(activatedRoute, routerState)),
    );
  }

  protected abstract _isAuthorized(activatedRoute: ActivatedRouteSnapshot): boolean;

  protected _shouldNavigate(activatedRoute: ActivatedRouteSnapshot, routerState: RouterStateSnapshot): Observable<boolean> {
    const isAuthorized = this._isAuthorized(activatedRoute);
    if (isTrue(isAuthorized)) {
      this._redirectToRequestedRoute(activatedRoute, routerState);
      return of(true);
    }

    const isLoggedIn = MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isLoggedIn);
    if (isFalse(isLoggedIn)) {
      const pathRequested = routerState.url;
      const pathRequestedWithoutQueryParam = UrlUtil.removeParametersFromUrl(pathRequested);
      const isPublicUrl = pathRequestedWithoutQueryParam === AppRoutesEnum.root || PublicUrlHelper.listPublicUrls.findIndex(url => pathRequestedWithoutQueryParam.startsWith(url)) !== -1;
      if (isFalse(isPublicUrl)) {
        return this._oauthService.initAuthorizationCodeFlow(LoginMode.STANDARD, pathRequested);
      }
    }

    this._router.navigate([AppRoutesEnum.home]);
    return of(false);
  }

  protected _redirectToRequestedRoute(activatedRoute: ActivatedRouteSnapshot, routerState: RouterStateSnapshot): void {
    const redirectPath: string = UrlQueryParamHelper.getRedirectPathFromPath(routerState.url, environment);
    const isRedirectPathDefined = isNotNullNorUndefinedNorWhiteString(redirectPath);
    if (isRedirectPathDefined) {
      this._redirectToInitialTarget(redirectPath);
    } else {
      this._redirectWithoutOAuth2QueryParamIfPresent(activatedRoute);
    }
  }

  /***
   * Redirect to initial target after login
   * @param redirectPath (ex: "/admin/organizational-unit/create?roleId=MANAGER&personId=64344f4e-5e87-463d-a213-48fa4ab3be85&orgUnitName=Test%20creation%20org%20unit")
   * @private
   */
  protected _redirectToInitialTarget(redirectPath: string): void {
    const path = UrlQueryParamHelper.removeParametersFromPath(redirectPath);
    const queryParamMapping = UrlQueryParamHelper.getQueryParamMappingObjectFromUrl(redirectPath, true);
    const queryParamObject = MappingObjectUtil.toObject(queryParamMapping);
    this._router.navigate([path], {queryParams: queryParamObject});
  }

  protected _redirectWithoutOAuth2QueryParamIfPresent(activatedRoute: ActivatedRouteSnapshot): void {
    const isOAuth2InfoPresent = activatedRoute.queryParamMap.has(UrlQueryParamHelper.QUERY_PARAM_OAUTH_CODE) || activatedRoute.queryParamMap.has(UrlQueryParamHelper.QUERY_PARAM_OAUTH_STATE);
    if (isOAuth2InfoPresent) {
      this._redirectWithoutOAuth2QueryParam(activatedRoute);
    }
  }

  protected _redirectWithoutOAuth2QueryParam(activatedRoute: ActivatedRouteSnapshot): void {
    const queryParamCleaned = Object.assign({}, activatedRoute.queryParams);
    MappingObjectUtil.delete(queryParamCleaned, UrlQueryParamHelper.QUERY_PARAM_OAUTH_CODE);
    MappingObjectUtil.delete(queryParamCleaned, UrlQueryParamHelper.QUERY_PARAM_OAUTH_STATE);
    this._router.navigate([AppRoutesEnum.home], {queryParams: queryParamCleaned});
  }
}
