/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - wait-app-initialized-guard.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
} from "@angular/router";
import {AppState} from "@app/stores/app.state";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  filter,
  map,
  take,
} from "rxjs/operators";
import {MemoizedUtil} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class WaitAppInitializedGuardService implements CanActivate {
  constructor(public readonly router: Router,
              private readonly _store: Store,
              private readonly _actions$: Actions) {
  }

  canActivate(activatedRoute: ActivatedRouteSnapshot): Observable<boolean> {
    return MemoizedUtil.select(this._store, AppState, state => state.isApplicationInitialized).pipe(
      filter(isApplicationInitialized => isApplicationInitialized),
      take(1),
      map(() => true),
    );
  }
}
