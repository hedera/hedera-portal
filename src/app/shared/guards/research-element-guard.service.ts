/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - research-element-guard.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {Injectable} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
} from "@angular/router";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  AppRoutesEnum,
  BrowseProjectRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {HomeAction} from "@shared/stores/home/home.action";
import {HomeState} from "@shared/stores/home/home.state";
import {RouteUtil} from "@shared/utils/route.util";
import {Observable} from "rxjs";
import {
  map,
  take,
} from "rxjs/operators";
import {
  isNullOrUndefined,
  MemoizedUtil,
  ofSolidifyActionCompleted,
  StoreUtil,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class ResearchElementGuardService implements CanActivate {
  constructor(public readonly router: Router,
              private readonly _store: Store,
              private readonly _actions$: Actions) {
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const researchObjectId: string = route.params[AppRoutesEnum.paramIdWithoutPrefixParam];

    return StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(this._store, [
      {
        action: new HomeAction.SearchDetail(researchObjectId),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(HomeAction.SearchDetailSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(HomeAction.SearchDetailFail)),
        ],
      },
    ]).pipe(
      take(1),
      map(result => {
        if (result.success) {
          const researchElement = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.current);

          if (isNullOrUndefined(researchElement) || researchElement.resId !== researchObjectId) {
            return this._redirectToPageNotFound(route, researchObjectId);
          }
          return true;
        } else {
          return this._redirectToPageNotFound(route, researchObjectId);
        }
      }),
    );
  }

  private _redirectToPageNotFound(route: ActivatedRouteSnapshot, id: string): false {
    const path = RouteUtil.generateFullUrlFromActivatedRouteSnapshot(route);
    const isHome = path[0] === AppRoutesEnum.home;
    if (isHome) {
      this._store.dispatch(new Navigate([RoutesEnum.homeNotFound, id]));
    } else {
      this._store.dispatch(new Navigate([...path.slice(0, path.length - 1), BrowseProjectRoutesEnum.notFound, id]));
    }
    return false;
  }
}
