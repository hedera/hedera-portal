/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - application-authorized-project.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {Injectable} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from "@angular/router";
import {AppAuthorizedProjectState} from "@app/stores/authorized-project/app-authorized-project.state";
import {Store} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  filter,
  map,
  take,
} from "rxjs/operators";
import {
  isFalse,
  MemoizedUtil,
  OAuth2Service,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class ApplicationAuthorizedProjectService implements CanActivate {
  constructor(private readonly _router: Router,
              private readonly _store: Store,
              private readonly _oauthService: OAuth2Service) {
  }

  canActivate(activatedRoute: ActivatedRouteSnapshot, routerState: RouterStateSnapshot): Observable<boolean> {
    return MemoizedUtil.isLoading(this._store, AppAuthorizedProjectState).pipe(
      filter(isLoading => isFalse(isLoading)),
      take(1),
      map(isLoading => {
        const isAuthorizedProjectFinishedToLoad = isFalse(isLoading);
        return isAuthorizedProjectFinishedToLoad;
      }),
    );
  }
}
