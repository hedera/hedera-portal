/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - is-manager-of-at-least-one-project-guard.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  Router,
} from "@angular/router";
import {Store} from "@ngxs/store";
import {ApplicationRoleGuardService} from "@shared/guards/application-role-guard.service";
import {SecurityService} from "@shared/services/security.service";
import {OAuth2Service} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class IsManagerOfAtLeastOneProjectGuardService extends ApplicationRoleGuardService {
  constructor(protected readonly _router: Router,
              protected readonly _store: Store,
              protected readonly _oauthService: OAuth2Service,
              protected readonly _securityService: SecurityService) {
    super(_router, _store, _oauthService, _securityService);
  }

  protected _isAuthorized(activatedRoute: ActivatedRouteSnapshot): boolean {
    return super._isAuthorized(activatedRoute) || (this._securityService.isLoggedIn() && this._securityService.isManagerOfAnyProject());
  }
}
