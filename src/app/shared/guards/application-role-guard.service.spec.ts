/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - application-role-guard.service.spec.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {OverlayModule} from "@angular/cdk/overlay";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {TestBed} from "@angular/core/testing";
import {MatSnackBar} from "@angular/material/snack-bar";
import {
  ActivatedRouteSnapshot,
  Router,
} from "@angular/router";
import {appModuleState} from "@app/app.module";
import {ApplicationRolePermissionEnum} from "@app/shared/enums/application-role-permission.enum";
import {ApplicationRoleGuardService} from "@app/shared/guards/application-role-guard.service";
import {HederaRouteData} from "@app/shared/models/hedera-route.model";
import {
  AppState,
  AppStateModel,
} from "@app/stores/app.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {TranslateService} from "@ngx-translate/core";
import {
  NgxsModule,
  Store,
} from "@ngxs/store";
import {Token} from "@shared/models/token.model";
import {
  ENVIRONMENT,
  JwtTokenHelper,
  SNACK_BAR,
  SolidifyAppAction,
} from "solidify-frontend";

describe("ApplicationRoleGuardService", () => {
  const router = {
    navigate: jasmine.createSpy("navigate"),
    parseUrl: jasmine.createSpy("parseUrl").and.returnValue(false),
  };
  const mockTranslateService = jasmine.createSpyObj("TranslateService", ["getTranslation", "setDefaultLang", "use"]);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, NgxsModule.forRoot([
        ...appModuleState,
      ]), OverlayModule],
      providers: [
        {
          provide: ApplicationRoleGuardService,
          useClass: ApplicationRoleGuardService,
        },
        {
          provide: TranslateService,
          useValue: mockTranslateService,
        },
        {
          provide: Router,
          useValue: router,
        },
        {
          provide: ENVIRONMENT,
          useValue: environment,
        },
        {
          provide: SNACK_BAR,
          useClass: MatSnackBar,
        },
      ],
    });
  });

  // eslint-disable-next-line prefer-arrow/prefer-arrow-functions
  async function markAppAsInitialized(store: Store): Promise<void> {
    await store.dispatch(new SolidifyAppAction.InitApplicationSuccess(null, {
      success: true,
    } as any)).toPromise();
    const state = store.selectSnapshot(AppState);
    expect(state.isApplicationInitialized).toBe(true);
  }

  const checkCanHitRoute = (data: HederaRouteData, canHit: boolean): void => {
    const service: ApplicationRoleGuardService = TestBed.get(ApplicationRoleGuardService);
    service.canActivate({data: data} as ActivatedRouteSnapshot, {url: undefined} as any).subscribe(res => expect(res).toBe(canHit));
  };

  it("be able to hit route when user is logged in", async () => {
    const store: Store = TestBed.get(Store);

    store.dispatch(new SolidifyAppAction.LoginSuccess(null));
    const state = store.selectSnapshot(AppState);
    expect(state.isLoggedIn).toBe(true);

    await markAppAsInitialized(store);
    checkCanHitRoute({permission: ApplicationRolePermissionEnum.noPermission}, true);
  });

  it("not be able to hit route when user is not logged in", async () => {
    const store: Store = TestBed.get(Store);

    await store.dispatch(new SolidifyAppAction.LoginFail(null)).toPromise();
    const state = store.selectSnapshot(AppState);
    expect(state.isLoggedIn).toBe(false);

    await markAppAsInitialized(store);
    checkCanHitRoute({permission: ApplicationRolePermissionEnum.userPermission}, false);
  });

  it("should be created", () => {
    const service: ApplicationRoleGuardService = TestBed.get(ApplicationRoleGuardService);
    expect(service).toBeTruthy();
  });

  it("be able to hit route for permission user when user have role [USER]", async () => {
    const store: Store = TestBed.get(Store);
    const simulatingCurrentUserRole = [Enums.UserApplicationRole.UserApplicationRoleEnum.user];
    spyOn(JwtTokenHelper, "decodeToken").and.returnValue({authorities: simulatingCurrentUserRole} as Token);

    store.dispatch(new SolidifyAppAction.LoginSuccess(null));
    const state: AppStateModel = store.selectSnapshot(AppState);
    expect(state.isLoggedIn).toBe(true);
    expect(state.userRoles).toEqual(simulatingCurrentUserRole);

    await markAppAsInitialized(store);
    checkCanHitRoute({permission: ApplicationRolePermissionEnum.userPermission}, true);
  });

  it("be able to hit route for permission admin when user have role [ADMIN]", async () => {
    const store: Store = TestBed.get(Store);
    const simulatingCurrentUserRole = [Enums.UserApplicationRole.UserApplicationRoleEnum.admin];
    spyOn(JwtTokenHelper, "decodeToken").and.returnValue({authorities: simulatingCurrentUserRole} as Token);

    store.dispatch(new SolidifyAppAction.LoginSuccess(null));
    const state: AppStateModel = store.selectSnapshot(AppState);
    expect(state.isLoggedIn).toBe(true);
    expect(state.userRoles).toEqual(simulatingCurrentUserRole);

    await markAppAsInitialized(store);
    checkCanHitRoute({permission: ApplicationRolePermissionEnum.adminPermission}, true);
  });

  it("be able to hit route for permission user when user have role [ADMIN]", async () => {
    const store: Store = TestBed.get(Store);
    const simulatingCurrentUserRole = [Enums.UserApplicationRole.UserApplicationRoleEnum.admin];
    spyOn(JwtTokenHelper, "decodeToken").and.returnValue({authorities: simulatingCurrentUserRole} as Token);

    store.dispatch(new SolidifyAppAction.LoginSuccess(null));
    const state: AppStateModel = store.selectSnapshot(AppState);
    expect(state.isLoggedIn).toBe(true);
    expect(state.userRoles).toEqual(simulatingCurrentUserRole);

    await markAppAsInitialized(store);
    checkCanHitRoute({permission: ApplicationRolePermissionEnum.userPermission}, true);
  });

  it("not be able to hit route for permission root when user have role [ADMIN]", async () => {
    const store: Store = TestBed.get(Store);
    const simulatingCurrentUserRole = [Enums.UserApplicationRole.UserApplicationRoleEnum.admin];
    spyOn(JwtTokenHelper, "decodeToken").and.returnValue({authorities: simulatingCurrentUserRole} as Token);

    store.dispatch(new SolidifyAppAction.LoginSuccess(null));
    const state: AppStateModel = store.selectSnapshot(AppState);
    expect(state.isLoggedIn).toBe(true);
    expect(state.userRoles).toEqual(simulatingCurrentUserRole);

    await markAppAsInitialized(store);
    checkCanHitRoute({permission: ApplicationRolePermissionEnum.rootPermission}, false);
  });

  it("be able to hit route for permission admin when user have role [USER, ADMIN]", async () => {
    const store: Store = TestBed.get(Store);
    const simulatingCurrentUserRole = [Enums.UserApplicationRole.UserApplicationRoleEnum.user, Enums.UserApplicationRole.UserApplicationRoleEnum.admin];
    spyOn(JwtTokenHelper, "decodeToken").and.returnValue({authorities: simulatingCurrentUserRole} as Token);

    store.dispatch(new SolidifyAppAction.LoginSuccess(null));
    const state: AppStateModel = store.selectSnapshot(AppState);
    expect(state.isLoggedIn).toBe(true);
    expect(state.userRoles).toEqual(simulatingCurrentUserRole);

    await markAppAsInitialized(store);
    checkCanHitRoute({permission: ApplicationRolePermissionEnum.adminPermission}, true);
  });

  it("not be able to hit route for permission admin when user have role [USER]", async () => {
    const service: ApplicationRoleGuardService = TestBed.get(ApplicationRoleGuardService);
    const store: Store = TestBed.get(Store);
    const simulatingCurrentUserRole = [Enums.UserApplicationRole.UserApplicationRoleEnum.user];
    spyOn(JwtTokenHelper, "decodeToken").and.returnValue({authorities: simulatingCurrentUserRole} as Token);

    store.dispatch(new SolidifyAppAction.LoginSuccess(null));
    const state: AppStateModel = store.selectSnapshot(AppState);
    expect(state.isLoggedIn).toBe(true);
    expect(state.userRoles).toEqual(simulatingCurrentUserRole);

    await markAppAsInitialized(store);
    checkCanHitRoute({permission: ApplicationRolePermissionEnum.adminPermission}, false);
  });

  it("not be able to hit route for permission root when user have role [USER, ADMIN]", async () => {
    const service: ApplicationRoleGuardService = TestBed.get(ApplicationRoleGuardService);
    const store: Store = TestBed.get(Store);
    const simulatingCurrentUserRole = [Enums.UserApplicationRole.UserApplicationRoleEnum.user, Enums.UserApplicationRole.UserApplicationRoleEnum.admin];
    spyOn(JwtTokenHelper, "decodeToken").and.returnValue({authorities: simulatingCurrentUserRole} as Token);

    store.dispatch(new SolidifyAppAction.LoginSuccess(null));
    const state: AppStateModel = store.selectSnapshot(AppState);
    expect(state.isLoggedIn).toBe(true);
    expect(state.userRoles).toEqual(simulatingCurrentUserRole);

    await markAppAsInitialized(store);
    checkCanHitRoute({permission: ApplicationRolePermissionEnum.rootPermission}, false);
  });

  it("be able to hit route for permission root when user have role [USER, ADMIN, ROOT]", async () => {
    const service: ApplicationRoleGuardService = TestBed.get(ApplicationRoleGuardService);
    const store: Store = TestBed.get(Store);
    const simulatingCurrentUserRole = [Enums.UserApplicationRole.UserApplicationRoleEnum.user, Enums.UserApplicationRole.UserApplicationRoleEnum.admin, Enums.UserApplicationRole.UserApplicationRoleEnum.root];
    spyOn(JwtTokenHelper, "decodeToken").and.returnValue({authorities: simulatingCurrentUserRole} as Token);

    store.dispatch(new SolidifyAppAction.LoginSuccess(null));
    const state: AppStateModel = store.selectSnapshot(AppState);
    expect(state.isLoggedIn).toBe(true);
    expect(state.userRoles).toEqual(simulatingCurrentUserRole);

    await markAppAsInitialized(store);
    checkCanHitRoute({permission: ApplicationRolePermissionEnum.rootPermission}, true);
  });

  it("be able to hit route without permission when user have no role", async () => {
    const store: Store = TestBed.get(Store);
    const simulatingCurrentUserRole = null;
    spyOn(JwtTokenHelper, "decodeToken").and.returnValue({authorities: simulatingCurrentUserRole} as Token);

    store.dispatch(new SolidifyAppAction.LoginSuccess(null));
    const state: AppStateModel = store.selectSnapshot(AppState);
    expect(state.isLoggedIn).toBe(true);
    expect(state.userRoles).toEqual(simulatingCurrentUserRole);

    await markAppAsInitialized(store);
    checkCanHitRoute({}, true);
  });

  it("be able to hit route without permission when user have no role", async () => {
    const store: Store = TestBed.get(Store);
    const simulatingCurrentUserRole = null;
    spyOn(JwtTokenHelper, "decodeToken").and.returnValue({authorities: simulatingCurrentUserRole} as Token);

    store.dispatch(new SolidifyAppAction.LoginSuccess(null));
    const state: AppStateModel = store.selectSnapshot(AppState);
    expect(state.isLoggedIn).toBe(true);
    expect(state.userRoles).toEqual(simulatingCurrentUserRole);

    await markAppAsInitialized(store);
    checkCanHitRoute({permission: ApplicationRolePermissionEnum.noPermission}, true);
  });

  it("not be able to hit route for permission user when user have no role", async () => {
    const store: Store = TestBed.get(Store);
    const simulatingCurrentUserRole = null;
    spyOn(JwtTokenHelper, "decodeToken").and.returnValue({authorities: simulatingCurrentUserRole} as Token);

    store.dispatch(new SolidifyAppAction.LoginSuccess(null));
    const state: AppStateModel = store.selectSnapshot(AppState);
    expect(state.isLoggedIn).toBe(true);
    expect(state.userRoles).toEqual(simulatingCurrentUserRole);

    await markAppAsInitialized(store);
    checkCanHitRoute({permission: ApplicationRolePermissionEnum.userPermission}, false);
  });

  it("not be able to hit route for permission user when user have empty role", async () => {
    const store: Store = TestBed.get(Store);
    const simulatingCurrentUserRole = [];
    spyOn(JwtTokenHelper, "decodeToken").and.returnValue({authorities: simulatingCurrentUserRole} as Token);

    store.dispatch(new SolidifyAppAction.LoginSuccess(null));
    const state: AppStateModel = store.selectSnapshot(AppState);
    expect(state.isLoggedIn).toBe(true);
    expect(state.userRoles).toEqual(simulatingCurrentUserRole);

    await markAppAsInitialized(store);
    checkCanHitRoute({permission: ApplicationRolePermissionEnum.userPermission}, false);
  });

  it("not be able to hit route for permission admin when user have no role", async () => {
    const store: Store = TestBed.get(Store);
    const simulatingCurrentUserRole = null;
    spyOn(JwtTokenHelper, "decodeToken").and.returnValue({authorities: simulatingCurrentUserRole} as Token);

    store.dispatch(new SolidifyAppAction.LoginSuccess(null));
    const state: AppStateModel = store.selectSnapshot(AppState);
    expect(state.isLoggedIn).toBe(true);
    expect(state.userRoles).toEqual(simulatingCurrentUserRole);

    await markAppAsInitialized(store);
    checkCanHitRoute({permission: ApplicationRolePermissionEnum.adminPermission}, false);
  });

  it("not be able to hit route for permission admin when user have empty role", async () => {
    const store: Store = TestBed.get(Store);
    const simulatingCurrentUserRole = [];
    spyOn(JwtTokenHelper, "decodeToken").and.returnValue({authorities: simulatingCurrentUserRole} as Token);

    store.dispatch(new SolidifyAppAction.LoginSuccess(null));
    const state: AppStateModel = store.selectSnapshot(AppState);
    expect(state.isLoggedIn).toBe(true);
    expect(state.userRoles).toEqual(simulatingCurrentUserRole);

    await markAppAsInitialized(store);
    checkCanHitRoute({permission: ApplicationRolePermissionEnum.adminPermission}, false);
  });

  it("be able to hit route for no permission when user have empty role", async () => {
    const store: Store = TestBed.get(Store);
    const simulatingCurrentUserRole = [];
    spyOn(JwtTokenHelper, "decodeToken").and.returnValue({authorities: simulatingCurrentUserRole} as Token);

    store.dispatch(new SolidifyAppAction.LoginSuccess(null));
    const state: AppStateModel = store.selectSnapshot(AppState);
    expect(state.isLoggedIn).toBe(true);
    expect(state.userRoles).toEqual(simulatingCurrentUserRole);

    await markAppAsInitialized(store);
    checkCanHitRoute({permission: ApplicationRolePermissionEnum.noPermission}, true);
  });
});
