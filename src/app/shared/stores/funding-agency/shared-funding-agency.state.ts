/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-funding-agency.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {Injectable} from "@angular/core";
import {HEDERA_CONSTANTS} from "@app/constants";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";
import {FundingAgency} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {
  SharedFundingAgencyAction,
  sharedFundingAgencyActionNameSpace,
} from "@shared/stores/funding-agency/shared-funding-agency.action";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  defaultResourceFileStateInitValue,
  DownloadService,
  NotificationService,
  QueryParameters,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
} from "solidify-frontend";

export interface SharedFundingAgencyStateModel extends ResourceFileStateModel<FundingAgency> {
}

@Injectable()
@State<SharedFundingAgencyStateModel>({
  name: StateEnum.shared_fundingAgency,
  defaults: {
    ...defaultResourceFileStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
  },
})
export class SharedFundingAgencyState extends ResourceFileState<SharedFundingAgencyStateModel, FundingAgency> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: sharedFundingAgencyActionNameSpace,
    }, _downloadService, ResourceFileStateModeEnum.logo, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminFundingAgencies;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Action(SharedFundingAgencyAction.GetByIdentifierId)
  getByIdentifierId(ctx: SolidifyStateContext<SharedFundingAgencyStateModel>, action: SharedFundingAgencyAction.GetByIdentifierId): Observable<FundingAgency> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.getById<FundingAgency>(this._urlResource, action.identifierId, {
      [HEDERA_CONSTANTS.IDENTIFIER_TYPE_PARAM]: action.identifierType,
    })
      .pipe(
        tap(result => {
          ctx.dispatch(new SharedFundingAgencyAction.GetByIdentifierIdSuccess(action, result));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedFundingAgencyAction.GetByIdentifierIdFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedFundingAgencyAction.GetByIdentifierIdSuccess)
  getByIdentifierIdSuccess(ctx: SolidifyStateContext<SharedFundingAgencyStateModel>, action: SharedFundingAgencyAction.GetByIdentifierIdSuccess): void {
    ctx.patchState({
      current: action.fundingAgency,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedFundingAgencyAction.GetByIdentifierIdFail)
  getByIdentifierIdFail(ctx: SolidifyStateContext<SharedFundingAgencyStateModel>, action: SharedFundingAgencyAction.GetByIdentifierIdFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
