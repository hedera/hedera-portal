/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - status-history.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Actions,
  Store,
} from "@ngxs/store";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiKeyword} from "@shared/enums/api.enum";
import {StatusHistoryNamespace} from "@shared/stores/status-history/status-history-namespace.model";
import {StatusHistoryAction} from "@shared/stores/status-history/status-history.action";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  BaseOptions,
  BaseResourceType,
  BaseState,
  BaseStateModel,
  CollectionTyped,
  defaultBaseStateInitValue,
  isNullOrUndefined,
  NotificationService,
  ObjectUtil,
  QueryParameters,
  RegisterDefaultAction,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StatusHistory,
} from "solidify-frontend";

export const defaultStatusHistoryInitValue: () => StatusHistoryStateModel<BaseResourceType> = () =>
  ({
    ...defaultBaseStateInitValue(),
    history: [],
    queryParameters: new QueryParameters(),
  });

export interface StatusHistoryStateModel<TResource extends BaseResourceType> extends BaseStateModel {
  history: StatusHistory[];
  queryParameters: QueryParameters;
}

export abstract class StatusHistoryState<TStateModel extends BaseStateModel, TResource extends BaseResourceType> extends BaseState<TStateModel> {
  protected readonly _nameSpace: StatusHistoryNamespace;

  protected abstract get _urlResource(): string;

  constructor(protected _apiService: ApiService,
              protected _store: Store,
              protected _notificationService: NotificationService,
              protected _actions$: Actions,
              protected _options: BaseOptions) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: _options.nameSpace,
    }, StatusHistoryState);
  }

  private _evaluateSubResourceUrl(parentId: string): string {
    if (parentId !== null) {
      return this._urlResource.toString().replace(`{${ApiKeyword.PARENT_ID}}`, parentId);
    } else {
      return this._urlResource.toString();
    }
  }

  @RegisterDefaultAction((statusHistoryNamespace: StatusHistoryNamespace) => statusHistoryNamespace.History)
  history(ctx: SolidifyStateContext<StatusHistoryStateModel<TResource>>, action: StatusHistoryAction.History): Observable<CollectionTyped<StatusHistory>> {
    ctx.patchState({
      history: [],
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: this.getQueryParametersToApply(action.queryParameters, ctx),
    });
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.getCollection<StatusHistory>(`${url}/${action.id}/${ApiResourceNameEnum.HISTORY}`, ctx.getState().queryParameters)
      .pipe(
        tap((list: CollectionTyped<StatusHistory>) => {
          ctx.dispatch(new this._nameSpace.HistorySuccess(action, list));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new this._nameSpace.HistoryFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  getQueryParametersToApply(queryParameters: QueryParameters, ctx: SolidifyStateContext<StatusHistoryStateModel<TResource>>): QueryParameters {
    return isNullOrUndefined(queryParameters) ? ctx.getState().queryParameters : queryParameters;
  }

  updateQueryParameters<T>(ctx: SolidifyStateContext<StatusHistoryStateModel<TResource>>, list: CollectionTyped<T> | null | undefined): QueryParameters {
    const queryParameters = ObjectUtil.clone(ctx.getState().queryParameters);
    const paging = ObjectUtil.clone(queryParameters.paging);
    paging.length = isNullOrUndefined(list) ? 0 : list._page.totalItems;
    queryParameters.paging = paging;
    return queryParameters;
  }

  @RegisterDefaultAction((statusHistoryNamespace: StatusHistoryNamespace) => statusHistoryNamespace.HistorySuccess)
  historySuccess(ctx: SolidifyStateContext<StatusHistoryStateModel<TResource>>, action: StatusHistoryAction.HistorySuccess): void {
    const queryParameters = this.updateQueryParameters(ctx, action.list);
    ctx.patchState({
      history: action.list._data,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters,
    });
  }

  @RegisterDefaultAction((statusHistoryNamespace: StatusHistoryNamespace) => statusHistoryNamespace.HistoryFail)
  historyFail(ctx: SolidifyStateContext<StatusHistoryStateModel<TResource>>, action: StatusHistoryAction.HistoryFail): void {
    ctx.patchState({
      history: [],
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @RegisterDefaultAction((statusHistoryNamespace: StatusHistoryNamespace) => statusHistoryNamespace.ChangeQueryParameters)
  changeQueryParameters(ctx: SolidifyStateContext<StatusHistoryStateModel<TResource>>, action: StatusHistoryAction.ChangeQueryParameters): void {
    ctx.patchState({
      queryParameters: action.queryParameters,
    });

    ctx.dispatch(new this._nameSpace.History(action.id, action.parentId, undefined));
  }
}
