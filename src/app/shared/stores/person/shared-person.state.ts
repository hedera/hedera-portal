/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-person.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {
  SharedPersonAction,
  sharedPersonActionNameSpace,
} from "@app/shared/stores/person/shared-person.action";
import {environment} from "@environments/environment";
import {
  Institution,
  Person,
} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SharedPersonInstitutionState} from "@shared/stores/person/institution/shared-people-institution.state";
import {Observable} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ActionSubActionCompletionsWrapper,
  ApiService,
  CollectionTyped,
  defaultResourceFileStateInitValue,
  DownloadService,
  isNonEmptyArray,
  isNullOrUndefined,
  isWhiteString,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ObjectUtil,
  ofSolidifyActionCompleted,
  OverrideDefaultAction,
  QueryParameters,
  QueryParametersUtil,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  ResourceStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";
import {ApiActionNameEnum} from "../../enums/api-action-name.enum";

export interface SharedPersonStateModel extends ResourceFileStateModel<Person> {
  listPersonMatching: Person[];
}

@Injectable()
@State<SharedPersonStateModel>({
  name: StateEnum.shared_person,
  defaults: {
    ...defaultResourceFileStateInitValue(),
    listPersonMatching: [],
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
  },
  children: [
    SharedPersonInstitutionState,
  ],
})
export class SharedPersonState extends ResourceFileState<SharedPersonStateModel, Person> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: sharedPersonActionNameSpace,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("shared.person.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("shared.person.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("shared.person.notification.resource.update"),
    }, _downloadService, ResourceFileStateModeEnum.avatar, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminPeople;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static isLoading<T>(state: ResourceStateModel<T>): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Action(SharedPersonAction.Search)
  search(ctx: SolidifyStateContext<SharedPersonStateModel>, action: SharedPersonAction.Search): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new SharedPersonAction.SearchPerson(action.person),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedPersonAction.SearchPersonSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedPersonAction.SearchPersonFail)),
        ],
      },
      {
        action: new SharedPersonAction.SearchInstitutions(),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedPersonAction.SearchInstitutionsSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedPersonAction.SearchInstitutionsFail)),
        ],
      },
    ]).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new SharedPersonAction.SearchSuccess(action));
        } else {
          ctx.dispatch(new SharedPersonAction.SearchFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(SharedPersonAction.SearchSuccess)
  searchSuccess(ctx: SolidifyStateContext<SharedPersonStateModel>, action: SharedPersonAction.SearchSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedPersonAction.SearchFail)
  searchFail(ctx: SolidifyStateContext<SharedPersonStateModel>, action: SharedPersonAction.SearchFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedPersonAction.SearchPerson)
  searchPerson(ctx: SolidifyStateContext<SharedPersonStateModel>, action: SharedPersonAction.SearchPerson): Observable<CollectionTyped<Person>> {
    const searchItems = new Map<string, string>();
    searchItems.set("firstName", action.person.firstName);
    searchItems.set("lastName", action.person.lastName);
    const queryParameters = new QueryParameters();
    queryParameters.search = {
      searchItems: searchItems,
    };
    return this._apiService.getCollection<Person>(this._urlResource, queryParameters)
      .pipe(
        tap(collection => {
          ctx.dispatch(new SharedPersonAction.SearchPersonSuccess(action, collection._data));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedPersonAction.SearchPersonFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedPersonAction.SearchPersonSuccess)
  searchPersonSuccess(ctx: SolidifyStateContext<SharedPersonStateModel>, action: SharedPersonAction.SearchPersonSuccess): void {
    ctx.patchState({
      listPersonMatching: action.list,
    });
  }

  @Action(SharedPersonAction.SearchInstitutions)
  searchInstitutions(ctx: SolidifyStateContext<SharedPersonStateModel>, action: SharedPersonAction.SearchInstitutions): Observable<any> {
    const listActionSubActionCompletionsWrapper: ActionSubActionCompletionsWrapper[] = [];

    if (isNonEmptyArray(ctx.getState().listPersonMatching)) {
      ctx.getState().listPersonMatching.forEach(p => {
        listActionSubActionCompletionsWrapper.push({
          action: new SharedPersonAction.SearchPersonInstitution(p),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(SharedPersonAction.SearchPersonInstitutionSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(SharedPersonAction.SearchPersonInstitutionFail)),
          ],
        } as ActionSubActionCompletionsWrapper);
      });

      return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, listActionSubActionCompletionsWrapper)
        .pipe(
          map(result => {
            if (result.success) {
              ctx.dispatch(new SharedPersonAction.SearchInstitutionsSuccess(action));
            } else {
              ctx.dispatch(new SharedPersonAction.SearchInstitutionsFail(action));
            }
            return result.success;
          }),
        );
    }

    return ctx.dispatch(new SharedPersonAction.SearchInstitutionsSuccess(action)).pipe(
      map(result => true),
    );
  }

  @Action(SharedPersonAction.SearchPersonInstitution)
  searchPersonInstitutions(ctx: SolidifyStateContext<SharedPersonStateModel>, action: SharedPersonAction.SearchPersonInstitution): Observable<CollectionTyped<Institution>> {
    return this._apiService.getCollection<Institution>(this._urlResource + urlSeparator + action.person.resId + urlSeparator + ApiResourceNameEnum.INSTITUTION, null)
      .pipe(
        tap(collectionInstitutions => {
          ctx.dispatch(new SharedPersonAction.SearchPersonInstitutionSuccess(action, action.person, collectionInstitutions._data));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedPersonAction.SearchPersonInstitutionFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedPersonAction.SearchPersonInstitutionSuccess)
  searchPersonInstitutionsSuccess(ctx: SolidifyStateContext<SharedPersonStateModel>, action: SharedPersonAction.SearchPersonInstitutionSuccess): void {
    const listPersons: Person[] = [...ctx.getState().listPersonMatching];
    const personIndex = listPersons.findIndex(p => p.resId === action.person.resId);
    listPersons[personIndex] = ObjectUtil.clone(listPersons[personIndex]);
    listPersons[personIndex].institutions = action.institutions;
    ctx.patchState({
      listPersonMatching: listPersons,
    });
  }

  @OverrideDefaultAction()
  @Action(SharedPersonAction.GetAll)
  getAll(ctx: SolidifyStateContext<ResourceStateModel<Person>>, action: SharedPersonAction.GetAll): Observable<CollectionTyped<Person>> {
    const onlyWithUser = action.onlyWithUser;
    let url = this._urlResource;
    const searchItems = QueryParametersUtil.getSearchItems(action.queryParameters);
    const fullName = MappingObjectUtil.get(searchItems, "fullName");
    if (!isNullOrUndefined(fullName) && !isWhiteString(fullName)) {
      if (onlyWithUser) {
        MappingObjectUtil.set(searchItems, "search", fullName);
      } else {
        url = this._urlResource + urlSeparator + ApiActionNameEnum.SEARCH;

        let searchValue = "";
        fullName.trim().split(" ").forEach(term => {
          if (!isWhiteString(term)) {
            if (searchValue.length > 0) {
              searchValue += ",";
            }
            const partOfORCIDRegex = /^\-?(\d{1,4}\-?){1,3}((\d{1,4})|(\d{1,3}(x|X)))?$/;
            if (partOfORCIDRegex.test(term)) {
              searchValue += `i-orcid~${term}`;
            } else {
              searchValue += `i-firstName~${term},i-lastName~${term}`;
            }
          }
        });

        MappingObjectUtil.set(searchItems, "search", searchValue);
        MappingObjectUtil.delete(searchItems, "fullName");
        MappingObjectUtil.set(searchItems, "match", "any");
      }
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
    });
    if (onlyWithUser) {
      url = this._urlResource + urlSeparator + ApiActionNameEnum.SEARCH_WITH_USER;
    }
    return this._apiService.getCollection<Person>(url, ctx.getState().queryParameters)
      .pipe(
        StoreUtil.cancelUncompleted(action, ctx, this._actions$, [/*SharedPersonAction.GetAll*/]),
        tap((collection: CollectionTyped<Person>) => {
          ctx.dispatch(new SharedPersonAction.GetAllSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedPersonAction.GetAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }
}
