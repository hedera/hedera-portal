/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-person.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  Institution,
  Person,
} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  QueryParameters,
  ResourceAction,
  ResourceFileAction,
  ResourceFileNameSpace,
  TypeDefaultAction,
} from "solidify-frontend";

const state = StateEnum.shared_person;

export namespace SharedPersonAction {
  @TypeDefaultAction(state)
  export class LoadResource extends ResourceAction.LoadResource {
  }

  @TypeDefaultAction(state)
  export class LoadResourceSuccess extends ResourceAction.LoadResourceSuccess {
  }

  @TypeDefaultAction(state)
  export class LoadResourceFail extends ResourceAction.LoadResourceFail {
  }

  @TypeDefaultAction(state)
  export class ChangeQueryParameters extends ResourceAction.ChangeQueryParameters {
  }

  @TypeDefaultAction(state)
  export class GetAll extends ResourceAction.GetAll {
    constructor(public queryParameters?: QueryParameters, public keepCurrentContext: boolean = false, public cancelIncomplete: boolean = true, public onlyWithUser: boolean = false) {
      super();
    }
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends ResourceAction.GetAllSuccess<Person> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends ResourceAction.GetAllFail<Person> {
  }

  @TypeDefaultAction(state)
  export class GetByListId extends ResourceAction.GetByListId {
  }

  @TypeDefaultAction(state)
  export class GetByListIdSuccess extends ResourceAction.GetByListIdSuccess {
  }

  @TypeDefaultAction(state)
  export class GetByListIdFail extends ResourceAction.GetByListIdFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends ResourceAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends ResourceAction.GetByIdSuccess<Person> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends ResourceAction.GetByIdFail<Person> {
  }

  @TypeDefaultAction(state)
  export class Create extends ResourceAction.Create<Person> {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends ResourceAction.CreateSuccess<Person> {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends ResourceAction.CreateFail<Person> {
  }

  @TypeDefaultAction(state)
  export class Update extends ResourceAction.Update<Person> {
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends ResourceAction.UpdateSuccess<Person> {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends ResourceAction.UpdateFail<Person> {
  }

  @TypeDefaultAction(state)
  export class Delete extends ResourceAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends ResourceAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends ResourceAction.DeleteFail {
  }

  @TypeDefaultAction(state)
  export class DeleteList extends ResourceAction.DeleteList {
  }

  @TypeDefaultAction(state)
  export class DeleteListSuccess extends ResourceAction.DeleteListSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteListFail extends ResourceAction.DeleteListFail {
  }

  @TypeDefaultAction(state)
  export class AddInList extends ResourceAction.AddInList<Person> {
  }

  @TypeDefaultAction(state)
  export class AddInListById extends ResourceAction.AddInListById {
  }

  @TypeDefaultAction(state)
  export class AddInListByIdSuccess extends ResourceAction.AddInListByIdSuccess<Person> {
  }

  @TypeDefaultAction(state)
  export class AddInListByIdFail extends ResourceAction.AddInListByIdFail<Person> {
  }

  @TypeDefaultAction(state)
  export class RemoveInListById extends ResourceAction.RemoveInListById {
  }

  @TypeDefaultAction(state)
  export class RemoveInListByListId extends ResourceAction.RemoveInListByListId {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkList extends ResourceAction.LoadNextChunkList {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkListSuccess extends ResourceAction.LoadNextChunkListSuccess<Person> {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkListFail extends ResourceAction.LoadNextChunkListFail {
  }

  @TypeDefaultAction(state)
  export class Clean extends ResourceAction.Clean {
  }

  @TypeDefaultAction(state)
  export class GetFile extends ResourceFileAction.GetFile {
  }

  @TypeDefaultAction(state)
  export class GetFileSuccess extends ResourceFileAction.GetFileSuccess {
  }

  @TypeDefaultAction(state)
  export class GetFileFail extends ResourceFileAction.GetFileFail {
  }

  @TypeDefaultAction(state)
  export class GetFileByResId extends ResourceFileAction.GetFileByResId {
  }

  @TypeDefaultAction(state)
  export class GetFileByResIdSuccess extends ResourceFileAction.GetFileByResIdSuccess {
  }

  @TypeDefaultAction(state)
  export class GetFileByResIdFail extends ResourceFileAction.GetFileByResIdFail {
  }

  @TypeDefaultAction(state)
  export class UploadFile extends ResourceFileAction.UploadFile {
  }

  @TypeDefaultAction(state)
  export class UploadFileSuccess extends ResourceFileAction.UploadFileSuccess {
  }

  @TypeDefaultAction(state)
  export class UploadFileFail extends ResourceFileAction.UploadFileFail {
  }

  @TypeDefaultAction(state)
  export class DeleteFile extends ResourceFileAction.DeleteFile {
  }

  @TypeDefaultAction(state)
  export class DeleteFileSuccess extends ResourceFileAction.DeleteFileSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFileFail extends ResourceFileAction.DeleteFileFail {
  }

  export class Search extends BaseAction {
    static readonly type: string = `[${state}] Search`;

    constructor(public person: Person) {
      super();
    }
  }

  export class SearchSuccess extends BaseSubActionSuccess<Search> {
    static readonly type: string = `[${state}] Search Success`;
  }

  export class SearchFail extends BaseSubActionFail<Search> {
    static readonly type: string = `[${state}] Search Fail`;
  }

  export class SearchPerson extends BaseAction {
    static readonly type: string = `[${state}] Search Person`;

    constructor(public person: Person) {
      super();
    }
  }

  export class SearchPersonSuccess extends BaseSubActionSuccess<SearchPerson> {
    static readonly type: string = `[${state}] Search Person Success`;

    constructor(public parentAction: SearchPerson, public list: Person[]) {
      super(parentAction);
    }
  }

  export class SearchPersonFail extends BaseSubActionFail<SearchPerson> {
    static readonly type: string = `[${state}] Search Person Fail`;
  }

  export class SearchInstitutions extends BaseAction {
    static readonly type: string = `[${state}] Search Institutions`;
  }

  export class SearchInstitutionsSuccess extends BaseSubActionSuccess<SearchInstitutions> {
    static readonly type: string = `[${state}] Search Institutions Success`;
  }

  export class SearchInstitutionsFail extends BaseSubActionFail<SearchInstitutions> {
    static readonly type: string = `[${state}] Search Institutions Fail`;
  }

  export class SearchPersonInstitution extends BaseAction {
    static readonly type: string = `[${state}] Search Person Institution`;

    constructor(public person: Person) {
      super();
    }
  }

  export class SearchPersonInstitutionSuccess extends BaseSubActionSuccess<SearchPersonInstitution> {
    static readonly type: string = `[${state}] Search Person Institution Success`;

    constructor(public parentAction: SearchPersonInstitution, public person: Person, public institutions: Institution[]) {
      super(parentAction);
    }
  }

  export class SearchPersonInstitutionFail extends BaseSubActionFail<SearchPersonInstitution> {
    static readonly type: string = `[${state}] Search Person Institution Fail`;
  }
}

export const sharedPersonActionNameSpace: ResourceFileNameSpace = SharedPersonAction;
