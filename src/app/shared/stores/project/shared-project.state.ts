/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-project.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {
  SharedProjectAction,
  sharedProjectActionNameSpace,
} from "@app/shared/stores/project/shared-project.action";
import {AppState} from "@app/stores/app.state";
import {environment} from "@environments/environment";
import {Project} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  ApiService,
  defaultResourceFileStateInitValue,
  DownloadService,
  isNullOrUndefined,
  MemoizedUtil,
  NotificationService,
  OverrideDefaultAction,
  QueryParameters,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  SolidifyStateContext,
} from "solidify-frontend";

export interface SharedProjectStateModel extends ResourceFileStateModel<Project> {
}

@Injectable()
@State<SharedProjectStateModel>({
  name: StateEnum.shared_project,
  defaults: {
    ...defaultResourceFileStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
  },
  children: [],
})
export class SharedProjectState extends ResourceFileState<SharedProjectStateModel, Project> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: sharedProjectActionNameSpace,
    }, _downloadService, ResourceFileStateModeEnum.logo, environment);
  }

  protected get _urlResource(): string {
    let isLogged = MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isLoggedIn);
    if (isNullOrUndefined(isLogged)) {
      isLogged = false;
    }
    return isLogged ? ApiEnum.adminProjects : ApiEnum.accessProjects;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @OverrideDefaultAction()
  @Action(SharedProjectAction.GetById)
  getById(ctx: SolidifyStateContext<SharedProjectStateModel>, action: SharedProjectAction.GetById): Observable<Project> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return super.getById(ctx, action);
  }

  @OverrideDefaultAction()
  @Action(SharedProjectAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<SharedProjectStateModel>, action: SharedProjectAction.GetByIdSuccess): void {
    super.getByIdSuccess(ctx, action);
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @OverrideDefaultAction()
  @Action(SharedProjectAction.GetByIdFail)
  getByIdFail(ctx: SolidifyStateContext<SharedProjectStateModel>, action: SharedProjectAction.GetByIdFail): void {
    super.getByIdFail(ctx, action);
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
