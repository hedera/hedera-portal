/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-user.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";
import {User} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  SharedUserAction,
  sharedUserActionNameSpace,
} from "@shared/stores/user/shared-user.action";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  defaultResourceStateInitValue,
  isFalse,
  isNullOrUndefined,
  isTrue,
  isWhiteString,
  MappingObjectUtil,
  NotificationService,
  OverrideDefaultAction,
  QueryParameters,
  QueryParametersUtil,
  ResourceState,
  ResourceStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";
import {ApiActionNameEnum} from "../../enums/api-action-name.enum";

export interface SharedUserStateModel extends ResourceStateModel<User> {
  listPendingExternalUid: string[] | undefined;
}

@Injectable()
@State<SharedUserStateModel>({
  name: StateEnum.shared_user,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
    listPendingExternalUid: [],
  },
})
export class SharedUserState extends ResourceState<SharedUserStateModel, User> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedUserActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminUsers;
  }

  @Selector()
  static isLoading<T>(state: ResourceStateModel<T>): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Action(SharedUserAction.GetByExternalUid)
  getByExternalUid(ctx: SolidifyStateContext<SharedUserStateModel>, action: SharedUserAction.GetByExternalUid): Observable<User> {
    let indexAlreadyExisting = -1;
    if (!isNullOrUndefined(ctx.getState().list) && isTrue(action.avoidDuplicate)) {
      indexAlreadyExisting = ctx.getState().list.findIndex(item => item.externalUid === action.externalUid);
      if (indexAlreadyExisting !== -1 && isFalse(action.replace)) {
        return;
      }
    }
    if (ctx.getState().listPendingExternalUid.find(pending => pending === action.externalUid)) {
      return;
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      listPendingExternalUid: [...(isNullOrUndefined(ctx.getState().listPendingExternalUid) ? [] : ctx.getState().listPendingExternalUid), action.externalUid],
    });
    const queryParameter = new QueryParameters(1);
    MappingObjectUtil.set(queryParameter.search.searchItems, "externalUid", action.externalUid);
    return this._apiService.getCollection<User>(this._urlResource, queryParameter)
      .pipe(
        tap((list) => {
          if (list._data.length === 0) {
            ctx.dispatch(new SharedUserAction.GetByExternalUidFail(action));
          } else {
            ctx.dispatch(new SharedUserAction.GetByExternalUidSuccess(action, list._data[0], indexAlreadyExisting));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedUserAction.GetByExternalUidFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedUserAction.GetByExternalUidSuccess)
  getByExternalUidSuccess(ctx: SolidifyStateContext<SharedUserStateModel>, action: SharedUserAction.GetByExternalUidSuccess): Observable<User> {
    if (isNullOrUndefined(action.model)) {
      return;
    }
    let list = ctx.getState().list;
    if (isNullOrUndefined(list)) {
      list = [];
    }
    if (action.indexAlreadyExisting !== -1 && isTrue(action.parentAction.replace)) {
      list = [...list];
      list[action.indexAlreadyExisting] = action.model;
    } else {
      list = [...list, action.model];
    }
    ctx.patchState({
      list: list,
      total: list.length,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      listPendingExternalUid: this._getListPendingExternalUidWithValueRemoved(ctx, action.parentAction.externalUid),
    });
  }

  @Action(SharedUserAction.GetByExternalUidFail)
  getByExternalUidFail(ctx: SolidifyStateContext<SharedUserStateModel>, action: SharedUserAction.GetByExternalUidFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      listPendingExternalUid: this._getListPendingExternalUidWithValueRemoved(ctx, action.parentAction.externalUid),
    });
  }

  @OverrideDefaultAction()
  @Action(SharedUserAction.GetAll)
  getAll(ctx: SolidifyStateContext<ResourceStateModel<User>>, action: SharedUserAction.GetAll): Observable<CollectionTyped<User>> {
    let url = this._urlResource;
    const searchItems = QueryParametersUtil.getSearchItems(action.queryParameters);
    const lastName = MappingObjectUtil.get(searchItems, "lastName");
    if (!isNullOrUndefined(lastName) && !isWhiteString(lastName)) {
      url = this._urlResource + urlSeparator + ApiActionNameEnum.SEARCH;
      let searchValue = "";
      lastName.trim().split(" ").forEach(term => {
        if (!isWhiteString(term)) {
          if (searchValue.length > 0) {
            searchValue += ",";
          }
          searchValue += `i-firstName~${term},i-lastName~${term}`;
        }
      });

      MappingObjectUtil.set(searchItems, "search", searchValue);
      MappingObjectUtil.delete(searchItems, "lastName");
      MappingObjectUtil.set(searchItems, "match", "any");
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
    });
    return this._apiService.getCollection<User>(url, ctx.getState().queryParameters)
      .pipe(
        StoreUtil.cancelUncompleted(action, ctx, this._actions$, [SharedUserAction.GetAll]),
        tap((collection: CollectionTyped<User>) => {
          ctx.dispatch(new SharedUserAction.GetAllSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedUserAction.GetAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  private _getListPendingExternalUidWithValueRemoved(ctx: SolidifyStateContext<SharedUserStateModel>, externalUid: string): string[] {
    let listPendingExternalUid = ctx.getState().listPendingExternalUid;
    const indexOf = listPendingExternalUid.indexOf(externalUid);
    if (indexOf === -1) {
      return listPendingExternalUid;
    }
    listPendingExternalUid = [...listPendingExternalUid];
    listPendingExternalUid.splice(indexOf, 1);
    return listPendingExternalUid;
  }
}
