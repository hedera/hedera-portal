/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - home.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Enums} from "@enums";
import {ArchiveStatisticsDto} from "@home/models/archive-statistics-dto.model";
import {AbstractResearchElementMetadataResult} from "@models";
import {ResearchElementModuleEnum} from "@shared/enums/research-element-module.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ViewModeTableEnum} from "@shared/enums/view-mode-table.enum";
import {
  BaseAction,
  BaseSubAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  Citation,
  CollectionTyped,
  MappingObject,
  QueryParameters,
} from "solidify-frontend";

const state = StateEnum.home;

export namespace HomeAction {
  export class ChangeQueryParameters {
    static readonly type: string = `[${state}] Change Query Parameters`;

    constructor(public queryParameters?: QueryParameters) {
    }
  }

  export class Search extends BaseAction {
    static readonly type: string = `[${state}] Search`;

    constructor(public resetPagination: boolean, public search?: string, public searchModeIsQuery?: boolean, public facetsSelected?: MappingObject<Enums.Facet.Name, string[]>, public viewMode?: ViewModeTableEnum, public queryParameters?: QueryParameters) {
      super();
    }
  }

  export class SearchSuccess extends BaseSubAction<Search> {
    static readonly type: string = `[${state}] Search Success`;

    constructor(public parentAction: Search, public collection?: CollectionTyped<AbstractResearchElementMetadataResult> | null | undefined) {
      super(parentAction);
    }
  }

  export class SearchFail extends BaseSubAction<Search> {
    static readonly type: string = `[${state}] Search Fail`;
  }

  export class SearchRelativeArchive extends BaseAction {
    static readonly type: string = `[${state}] Search Relative Research Object`;

    constructor(public archive: AbstractResearchElementMetadataResult) {
      super();
    }
  }

  export class SearchRelativeArchiveSuccess extends BaseSubActionSuccess<SearchRelativeArchive> {
    static readonly type: string = `[${state}] Search Relative Research Object Success`;

    constructor(public parentAction: SearchRelativeArchive, public list: CollectionTyped<AbstractResearchElementMetadataResult> | null | undefined) {
      super(parentAction);
    }
  }

  export class SearchRelativeArchiveFail extends BaseSubActionFail<SearchRelativeArchive> {
    static readonly type: string = `[${state}] Search Relative Research Object Fail`;
  }

  export class SearchDetail extends BaseAction {
    static readonly type: string = `[${state}] Search Detail`;

    constructor(public resId: string) {
      super();
    }
  }

  export class SearchDetailSuccess extends BaseSubActionSuccess<SearchDetail> {
    static readonly type: string = `[${state}] Search Detail Success`;

    constructor(public parentAction: SearchDetail, public model: AbstractResearchElementMetadataResult) {
      super(parentAction);
    }
  }

  export class SearchDetailFail extends BaseSubActionFail<SearchDetail> {
    static readonly type: string = `[${state}] Search Detail Fail`;
  }

  export class Download extends BaseAction {
    static readonly type: string = `[${state}] Download`;

    constructor(public archive: AbstractResearchElementMetadataResult) {
      super();
    }
  }

  export class DownloadStart extends BaseSubAction<Download> {
    static readonly type: string = `[${state}] Download Start`;
  }

  export class DownloadSuccess extends BaseSubActionSuccess<DownloadStart> {
    static readonly type: string = `[${state}] Download Success`;
  }

  export class DownloadFail extends BaseSubActionFail<DownloadStart> {
    static readonly type: string = `[${state}] Download Fail`;
  }

  export class CleanCurrent {
    static readonly type: string = `[${state}] Clean Current`;
  }

  export class GetStatistics extends BaseAction {
    static readonly type: string = `[${state}] Get Statistics`;

    constructor(public archiveId: string) {
      super();
    }
  }

  export class GetStatisticsSuccess extends BaseSubActionSuccess<GetStatistics> {
    static readonly type: string = `[${state}] Get Statistics Success`;

    constructor(public parentAction: GetStatistics, public archiveStatisticDto: ArchiveStatisticsDto) {
      super(parentAction);
    }
  }

  export class GetStatisticsFail extends BaseSubActionFail<GetStatistics> {
    static readonly type: string = `[${state}] Get Statistics Fail`;
  }

  export class GetBibliographies extends BaseAction {
    static readonly type: string = `[${state}] Get Bibliographies`;

    constructor(public archiveId: string) {
      super();
    }
  }

  export class GetBibliographiesSuccess extends BaseSubActionSuccess<GetBibliographies> {
    static readonly type: string = `[${state}] Get Bibliographies Success`;

    constructor(public parentAction: GetBibliographies, public bibliographies: Citation[]) {
      super(parentAction);
    }
  }

  export class GetBibliographiesFail extends BaseSubActionFail<GetBibliographies> {
    static readonly type: string = `[${state}] Get Bibliographies Fail`;
  }

  export class GetCitations extends BaseAction {
    static readonly type: string = `[${state}] Get Citations`;

    constructor(public archiveId: string) {
      super();
    }
  }

  export class GetCitationsSuccess extends BaseSubActionSuccess<GetCitations> {
    static readonly type: string = `[${state}] Get Citations Success`;

    constructor(public parentAction: GetCitations, public citations: Citation[]) {
      super(parentAction);
    }
  }

  export class GetCitationsFail extends BaseSubActionFail<GetCitations> {
    static readonly type: string = `[${state}] Get Citations Fail`;
  }

  export class FollowRedirect extends BaseAction {
    static readonly type: string = `[${state}] Follow Redirect`;

    constructor(public url: string, public module: ResearchElementModuleEnum, public projectId: string) {
      super();
    }
  }

  export class GetRdf extends BaseAction {
    static readonly type: string = `[${state}] Get Rdf`;

    constructor(public url: string, public format: Enums.Access.RdfFormatEnum) {
      super();
    }
  }

  export class GetRdfSuccess extends BaseSubActionSuccess<GetRdf> {
    static readonly type: string = `[${state}] Get Rdf Success`;

    constructor(public parentAction: GetRdf, public rdf: string) {
      super(parentAction);
    }
  }

  export class GetRdfFail extends BaseSubActionFail<GetRdf> {
    static readonly type: string = `[${state}] Get Rdf Fail`;
  }

  export class GetResearchElementByHederaId extends BaseAction {
    static readonly type: string = `[${state}] Get Research Element By HederaId`;

    constructor(public hederaId: string, public researchElementTypeEnum: Enums.SearchMetadata.ResearchElementTypeEnum) {
      super();
    }
  }

  export class GetResearchElementByHederaIdSuccess extends BaseSubActionSuccess<GetResearchElementByHederaId> {
    static readonly type: string = `[${state}] Get Research Element By HederaId Success`;

    constructor(public parentAction: GetResearchElementByHederaId, public researchElement: AbstractResearchElementMetadataResult) {
      super(parentAction);
    }
  }

  export class GetResearchElementByHederaIdFail extends BaseSubActionFail<GetResearchElementByHederaId> {
    static readonly type: string = `[${state}] Get Research Element By HederaId Fail`;
  }
}
