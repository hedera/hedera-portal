/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - home.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpClient,
  HttpHeaders,
} from "@angular/common/http";
import {
  Injectable,
  makeStateKey,
} from "@angular/core";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {HomeHelper} from "@home/helpers/home.helper";
import {MetadataUtil} from "@home/helpers/metadata.util";
import {ArchiveStatisticsDto} from "@home/models/archive-statistics-dto.model";
import {
  AbstractResearchElementMetadata,
  AbstractResearchElementMetadataResult,
  ResearchDataFileMetadataResult,
  ResearchObjectMetadataResult,
  SearchCondition,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {ResearchElementModuleEnum} from "@shared/enums/research-element-module.enum";
import {
  BrowseProjectRoutesEnum,
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ViewModeTableEnum} from "@shared/enums/view-mode-table.enum";
import {HomeAction} from "@shared/stores/home/home.action";
import {
  Observable,
  of,
} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  BasicState,
  Citation,
  CollectionTyped,
  defaultResourceStateInitValue,
  Facet,
  isEmptyString,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  MappingObject,
  MappingObjectUtil,
  NotificationService,
  QueryParameters,
  QueryParametersUtil,
  ResourceStateModel,
  SOLIDIFY_CONSTANTS,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
  StringUtil,
  TransferStateService,
} from "solidify-frontend";

export const HOME_SEARCH_ALL: string = "*";
const DEFAULT_VIEW_MODE_TABLE: ViewModeTableEnum = environment.defaultHomeViewModeTableEnum;

export interface HomeStateModel extends ResourceStateModel<AbstractResearchElementMetadataResult> {
  search: string;
  searchModeIsQuery: boolean;
  facets: Facet[];
  viewModeTableEnum: ViewModeTableEnum;
  listRelativeResearchObject: AbstractResearchElementMetadataResult[];
  facetsSelected: MappingObject<Enums.Facet.Name, string[]>;
  archiveStatisticDto: ArchiveStatisticsDto;
  bibliographies: Citation[];
  citations: Citation[];
  rdf: string;
  isLoadingRdf: number;
  associatedResearchElement: AbstractResearchElementMetadataResult;
  associatedResearchObject: ResearchObjectMetadataResult;
  associatedResearchDataFile: ResearchDataFileMetadataResult;
}

@Injectable()
@State<HomeStateModel>({
  name: StateEnum.home,
  defaults: {
    ...defaultResourceStateInitValue(),
    search: StringUtil.stringEmpty,
    searchModeIsQuery: false,
    viewModeTableEnum: DEFAULT_VIEW_MODE_TABLE,
    queryParameters: new QueryParameters(environment.defaultPageSizeHomePage),
    listRelativeResearchObject: undefined,
    facets: undefined,
    facetsSelected: {} as MappingObject<Enums.Facet.Name, string[]>,
    archiveStatisticDto: undefined,
    bibliographies: [],
    citations: [],
    rdf: undefined,
    isLoadingRdf: 0,
    associatedResearchElement: undefined,
    associatedResearchObject: undefined,
    associatedResearchDataFile: undefined,
  },
  children: [],
})
export class HomeState extends BasicState<HomeStateModel> {
  protected get _urlResource(): string {
    return ApiEnum.accessResearchObject;
  }

  constructor(private readonly _apiService: ApiService,
              private readonly _store: Store,
              private readonly _httpClient: HttpClient,
              private readonly _notificationService: NotificationService,
              private readonly _actions$: Actions,
              private readonly _transferState: TransferStateService) {
    super();
  }

  private readonly _PATH_FILTER: string = "query";
  private readonly _DEFAULT_SEARCH: string = HOME_SEARCH_ALL;
  private readonly _SEARCH_GLOBAL_PARAM: string = "globalSearch";
  private readonly _HEDERA_ID: keyof AbstractResearchElementMetadata = "hederaId";

  @Selector()
  static current(state: HomeStateModel): AbstractResearchElementMetadataResult {
    return state.current;
  }

  @Selector()
  static search(state: HomeStateModel): string {
    return state.search;
  }

  @Selector()
  static isLoading(state: HomeStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static currentTitle(state: HomeStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.metadata.uri;
  }

  @Action(HomeAction.ChangeQueryParameters)
  changeQueryParameters(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.ChangeQueryParameters): void {
    ctx.patchState({
      queryParameters: action.queryParameters,
    });
    ctx.dispatch(new HomeAction.Search(false, ctx.getState().search, ctx.getState().searchModeIsQuery, ctx.getState().facetsSelected));
  }

  @Action(HomeAction.Search)
  search(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.Search): Observable<CollectionTyped<any/*ArchiveMetadata*/>> {
    const searchString = this._getSearchStringToApply(ctx, action);
    const viewMode = this._getViewModeToApply(ctx, action);
    let queryParameters = StoreUtil.getQueryParametersToApply(action.queryParameters, ctx);
    if (action.resetPagination) {
      queryParameters = QueryParametersUtil.resetToFirstPage(queryParameters);
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      search: searchString,
      searchModeIsQuery: action.searchModeIsQuery,
      facetsSelected: action.facetsSelected,
      viewModeTableEnum: viewMode,
      queryParameters: queryParameters,
    });

    const facetsSelectedForBackend = HomeHelper.getFacetsSelectedForBackend(action.facetsSelected);

    if (action.searchModeIsQuery) {
      queryParameters = QueryParametersUtil.clone(queryParameters);
      MappingObjectUtil.set(QueryParametersUtil.getSearchItems(queryParameters), this._PATH_FILTER, isEmptyString(searchString) ? HOME_SEARCH_ALL : searchString);
    } else if (!isNullOrUndefined(searchString) && !isEmptyString(searchString)) {
      facetsSelectedForBackend.push({
        field: this._SEARCH_GLOBAL_PARAM,
        value: searchString,
        type: Enums.SearchCondition.Type.MATCH,
        booleanClauseType: Enums.SearchCondition.BooleanClauseType.MUST,
      } as SearchCondition);
    }
    return this._apiService.postQueryParameters<SearchCondition[], CollectionTyped<AbstractResearchElementMetadataResult>>(this._urlResource + urlSeparator + ApiActionNameEnum.SEARCH, facetsSelectedForBackend, queryParameters)
      .pipe(
        StoreUtil.cancelUncompleted(action, ctx, this._actions$, [HomeAction.Search]),
        tap((collection: CollectionTyped<AbstractResearchElementMetadataResult>) => {
          // const collectionArchive = {
          //   _links: collection._links,
          //   _page: collection._page,
          //   _facets: collection._facets,
          // } as CollectionTyped<ResearchObject>;
          // collectionArchive._data = collection._data as any; // TODO Convert here ArchiveHelper.adaptListArchivesMetadataInArchive(collection._data);
          ctx.dispatch(new HomeAction.SearchSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.SearchFail(action));
          throw error;
        }),
      );
  }

  private _getSearchStringToApply(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.Search): string {
    if (isNullOrUndefined(action.search)) {
      return this._getDefaultSearchStringIfNotDefined(ctx);
    }
    return action.search;
  }

  private _getDefaultSearchStringIfNotDefined(ctx: SolidifyStateContext<HomeStateModel>): string {
    if (ctx.getState().search === null || ctx.getState().search === undefined) {
      return this._DEFAULT_SEARCH;
    }
    return ctx.getState().search;
  }

  @Action(HomeAction.SearchSuccess)
  searchSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.SearchSuccess): void {
    const queryParameters = StoreUtil.updateQueryParameters(ctx, action.collection);

    ctx.patchState({
      list: isNullOrUndefined(action.collection) ? [] : action.collection._data,
      total: isNullOrUndefined(action.collection) ? 0 : action.collection._page.totalItems,
      facets: isNullOrUndefined(action.collection) ? [] : action.collection._facets,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters,
    });
  }

  @Action(HomeAction.SearchFail)
  searchFail(ctx: SolidifyStateContext<HomeStateModel>): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      list: [],
      total: 0,
    });
  }

  @Action(HomeAction.SearchRelativeArchive)
  searchRelativeArchive(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.SearchRelativeArchive): Observable<CollectionTyped<AbstractResearchElementMetadataResult>> {
    const searchString = `researchProject.id:${action.archive.metadata.researchProject.id}`;

    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const queryParameters = new QueryParameters(5);
    MappingObjectUtil.set(QueryParametersUtil.getSearchItems(queryParameters), this._PATH_FILTER, searchString);

    const STATE_KEY = makeStateKey<CollectionTyped<AbstractResearchElementMetadataResult>>(`HomeState-SearchRelativeResearchObject-${action.archive.metadata.researchProject.id}`);
    if (this._transferState.hasKey(STATE_KEY)) {
      const collectionTransferState = this._transferState.get(STATE_KEY, undefined);
      this._transferState.remove(STATE_KEY);
      ctx.dispatch(new HomeAction.SearchRelativeArchiveSuccess(action, collectionTransferState));
      return of(collectionTransferState);
    }

    return this._apiService.getCollection<AbstractResearchElementMetadataResult>(this._urlResource + urlSeparator + "search", queryParameters)
      .pipe(
        StoreUtil.cancelUncompleted(action, ctx, this._actions$, [HomeAction.Search]),
        map((collection: CollectionTyped<AbstractResearchElementMetadataResult>) => {
          const listArchiveMetadata = collection._data;
          const listArchive = collection._data as any; // TODO Convert here ArchiveHelper.adaptListArchivesMetadataInArchive(listArchiveMetadata);
          const collectionArchive = {
            _links: collection._links,
            _page: collection._page,
            _facets: collection._facets,
            _data: listArchive,
          } as CollectionTyped<AbstractResearchElementMetadataResult>;
          this._transferState.set(STATE_KEY, collectionArchive);
          ctx.dispatch(new HomeAction.SearchRelativeArchiveSuccess(action, collectionArchive));
          return collectionArchive;
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.SearchRelativeArchiveFail(action));
          throw error;
        }),
      );
  }

  @Action(HomeAction.SearchRelativeArchiveSuccess)
  searchRelativeArchiveSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.SearchRelativeArchiveSuccess): void {
    ctx.patchState({
      listRelativeResearchObject: isNullOrUndefined(action.list) ? [] : action.list._data.filter(archive => archive.resId !== action.parentAction.archive.resId),
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.SearchRelativeArchiveFail)
  searchRelativeArchiveFail(ctx: SolidifyStateContext<HomeStateModel>): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.SearchDetail)
  detail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.SearchDetail): Observable<AbstractResearchElementMetadataResult> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      current: undefined,
      listRelativeResearchObject: undefined,
    });

    const STATE_KEY = makeStateKey<AbstractResearchElementMetadataResult>(`HomeState-GetById-${action.resId}`);
    if (this._transferState.hasKey(STATE_KEY)) {
      const archiveTransferState = this._transferState.get(STATE_KEY, undefined);
      this._transferState.remove(STATE_KEY);
      ctx.dispatch(new HomeAction.SearchDetailSuccess(action, archiveTransferState));
      return of(archiveTransferState);
    }

    return this._apiService.getById<AbstractResearchElementMetadataResult>(this._urlResource, action.resId)
      .pipe(
        tap(model => {
          const archive = model as any; // TODO Convert here ArchiveHelper.adaptArchiveMetadataInArchive(model);
          this._transferState.set(STATE_KEY, archive);
          ctx.dispatch(new HomeAction.SearchDetailSuccess(action, archive));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.SearchDetailFail(action));
          throw error;
        }),
      );
  }

  @Action(HomeAction.SearchDetailSuccess)
  detailSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.SearchDetailSuccess): void {
    ctx.patchState({
      current: action.model,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new HomeAction.SearchRelativeArchive(action.model));

    let associatedResearchElementTypeEnum: Enums.SearchMetadata.ResearchElementTypeEnum;
    if (MetadataUtil.isResearchObject(action.model)) {
      associatedResearchElementTypeEnum = Enums.SearchMetadata.ResearchElementTypeEnum.ResearchDataFile;
    } else if (MetadataUtil.isResearchDataFile(action.model)) {
      associatedResearchElementTypeEnum = Enums.SearchMetadata.ResearchElementTypeEnum.ResearchObject;
    }
    if (isNotNullNorUndefined(associatedResearchElementTypeEnum)) {
      ctx.dispatch(new HomeAction.GetResearchElementByHederaId(action.model.metadata.hederaId, associatedResearchElementTypeEnum));
    }
  }

  @Action(HomeAction.SearchDetailFail)
  detailFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.SearchDetailFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.CleanCurrent)
  cleanCurrent(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.CleanCurrent): void {
    ctx.patchState({
      current: undefined,
      listRelativeResearchObject: undefined,
      isLoadingRdf: 0,
      rdf: undefined,
      archiveStatisticDto: undefined,
      citations: [],
      bibliographies: [],
      associatedResearchElement: undefined,
      associatedResearchObject: undefined,
      associatedResearchDataFile: undefined,
    });
  }

  private _getViewModeToApply(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.Search): ViewModeTableEnum {
    const currentViewModeInState = ctx.getState().viewModeTableEnum;
    const currentViewToUse = isNullOrUndefined(currentViewModeInState) ? DEFAULT_VIEW_MODE_TABLE : currentViewModeInState;
    return isNullOrUndefined(action.viewMode) ? currentViewToUse : action.viewMode;
  }

  @Action(HomeAction.GetStatistics)
  getStatistics(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetStatistics): Observable<ArchiveStatisticsDto> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      archiveStatisticDto: undefined,
    });

    const STATE_KEY = makeStateKey<ArchiveStatisticsDto>(`HomeState-Statistics-${action.archiveId}`);
    if (this._transferState.hasKey(STATE_KEY)) {
      const statisticsTransferState = this._transferState.get(STATE_KEY, undefined);
      this._transferState.remove(STATE_KEY);
      ctx.dispatch(new HomeAction.GetStatisticsSuccess(action, statisticsTransferState));
      return of(statisticsTransferState);
    }

    return this._apiService.get<ArchiveStatisticsDto>(`${this._urlResource}/${action.archiveId}/${ApiActionNameEnum.STATISTICS}`)
      .pipe(
        tap((model: ArchiveStatisticsDto) => {
          this._transferState.set(STATE_KEY, model);
          ctx.dispatch(new HomeAction.GetStatisticsSuccess(action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.GetStatisticsFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(HomeAction.GetStatisticsSuccess)
  getStatisticsSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetStatisticsSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      archiveStatisticDto: action.archiveStatisticDto,
    });
  }

  @Action(HomeAction.GetStatisticsFail)
  getStatisticsFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetStatisticsFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.GetBibliographies)
  getBibliographies(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetBibliographies): Observable<Citation[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      archiveStatisticDto: undefined,
    });

    const STATE_KEY = makeStateKey<Citation[]>(`HomeState-Bibliographies-${action.archiveId}`);
    if (this._transferState.hasKey(STATE_KEY)) {
      const bibliographiesTransferState = this._transferState.get(STATE_KEY, undefined);
      this._transferState.remove(STATE_KEY);
      ctx.dispatch(new HomeAction.GetBibliographiesSuccess(action, bibliographiesTransferState));
      return of(bibliographiesTransferState);
    }

    return this._apiService.get<Citation[]>(`${this._urlResource}/${action.archiveId}/${ApiActionNameEnum.BIBLIOGRAPHIES}`)
      .pipe(
        tap((model: Citation[]) => {
          this._transferState.set(STATE_KEY, model);
          ctx.dispatch(new HomeAction.GetBibliographiesSuccess(action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.GetBibliographiesFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(HomeAction.GetBibliographiesSuccess)
  getBibliographiesSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetBibliographiesSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      bibliographies: action.bibliographies,
    });
  }

  @Action(HomeAction.GetBibliographiesFail)
  getBibliographiesFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetBibliographiesFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.GetCitations)
  getCitations(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetCitations): Observable<Citation[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      archiveStatisticDto: undefined,
    });

    const STATE_KEY = makeStateKey<Citation[]>(`HomeState-Citations-${action.archiveId}`);
    if (this._transferState.hasKey(STATE_KEY)) {
      const citationsTransferState = this._transferState.get(STATE_KEY, undefined);
      this._transferState.remove(STATE_KEY);
      ctx.dispatch(new HomeAction.GetCitationsSuccess(action, citationsTransferState));
      return of(citationsTransferState);
    }

    return this._apiService.get<Citation[]>(`${this._urlResource}/${action.archiveId}/${ApiActionNameEnum.CITATIONS}`)
      .pipe(
        tap((model: Citation[]) => {
          this._transferState.set(STATE_KEY, model);
          ctx.dispatch(new HomeAction.GetCitationsSuccess(action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.GetCitationsFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(HomeAction.GetCitationsSuccess)
  getCitationsSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetCitationsSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      citations: action.citations,
    });
  }

  @Action(HomeAction.GetCitationsFail)
  getCitationsFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetCitationsFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.FollowRedirect)
  followRedirect(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.FollowRedirect): void {
    fetch(action.url).then(response => {
      const responseURL = response.url;
      if (isNotNullNorUndefinedNorWhiteString(responseURL)) {
        const lastIndexOfSlash = responseURL.lastIndexOf(SOLIDIFY_CONSTANTS.URL_SEPARATOR);
        const id = responseURL.substring(lastIndexOfSlash + 1);
        let route = [RoutesEnum.homeDetail];
        if (action.module === ResearchElementModuleEnum.browse) {
          route = [RoutesEnum.browseProjectDetail, action.projectId, BrowseProjectRoutesEnum.researchObject];
        }
        ctx.dispatch(new Navigate([...route, id]));
      }
    });
  }

  @Action(HomeAction.GetRdf)
  getRdf(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetRdf): Observable<string> {
    ctx.patchState({
      isLoadingRdf: ctx.getState().isLoadingRdf + 1,
      rdf: undefined,
    });

    // const STATE_KEY = makeStateKey<Citation[]>(`HomeState-Citations-${action.archiveId}`);
    // if (this._transferState.hasKey(STATE_KEY)) {
    //   const citationsTransferState = this._transferState.get(STATE_KEY, undefined);
    //   this._transferState.remove(STATE_KEY);
    //   ctx.dispatch(new HomeAction.GetAsSuccess(action, citationsTransferState));
    //   return of(citationsTransferState);
    // }

    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.set("Content-Type", this._getAcceptHeaderValue(action.format));
    return this._httpClient.get<string>(action.url, {
      headers,
      responseType: "text",
    } as any)
      .pipe(
        tap((result: any) => {
          ctx.dispatch(new HomeAction.GetRdfSuccess(action, result as string));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.GetRdfFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(HomeAction.GetRdfSuccess)
  getRdfSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetRdfSuccess): void {
    ctx.patchState({
      isLoadingRdf: ctx.getState().isLoadingRdf - 1,
      rdf: action.rdf,
    });
  }

  @Action(HomeAction.GetRdfFail)
  getRdfFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetRdfFail): void {
    ctx.patchState({
      isLoadingRdf: ctx.getState().isLoadingRdf - 1,
    });
  }

  private _getAcceptHeaderValue(format: Enums.Access.RdfFormatEnum): string {
    switch (format) {
      case Enums.Access.RdfFormatEnum.TURTLE:
        return "text/turtle";
      case Enums.Access.RdfFormatEnum.N_TRIPLES:
        return "application/n-triples";
      case Enums.Access.RdfFormatEnum.JSON_LD:
        return "application/ld+json";
      case Enums.Access.RdfFormatEnum.RDF_XML:
        return "application/rdf+xml";
      case Enums.Access.RdfFormatEnum.RDF_JSON:
        return "application/json";
      case Enums.Access.RdfFormatEnum.N3:
        return "text/n3";
      default:
        return "application/ld+json";
    }
  }

  @Action(HomeAction.GetResearchElementByHederaId)
  getResearchElementByHederaId(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetResearchElementByHederaId): Observable<AbstractResearchElementMetadataResult> {
    ctx.patchState({
      associatedResearchElement: undefined,
      associatedResearchObject: undefined,
      associatedResearchDataFile: undefined,
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const queryParameters = new QueryParameters(1);
    MappingObjectUtil.set(QueryParametersUtil.getSearchItems(queryParameters), this._PATH_FILTER, `${this._HEDERA_ID}:${action.hederaId}`);
    const facetsSelectedForBackend = HomeHelper.getFacetsSelectedForBackend({
      [Enums.Facet.Name.ITEM_TYPES]: [action.researchElementTypeEnum],
    } as MappingObject<Enums.Facet.Name, string[]>);
    return this._apiService.postQueryParameters<SearchCondition[], CollectionTyped<AbstractResearchElementMetadataResult>>(`${this._urlResource}/${ApiActionNameEnum.SEARCH}`, facetsSelectedForBackend, queryParameters)
      .pipe(
        StoreUtil.cancelUncompleted(action, ctx, this._actions$, [HomeAction.Search]),
        map((collection: CollectionTyped<AbstractResearchElementMetadataResult>) => {
          if (collection._page.totalItems === 0) {
            return undefined;
          }
          return collection._data[0];
        }),
        tap(researchElement => {
          if (isNullOrUndefined(researchElement)) {
            ctx.dispatch(new HomeAction.GetResearchElementByHederaIdFail(action));
          } else {
            ctx.dispatch(new HomeAction.GetResearchElementByHederaIdSuccess(action, researchElement));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.GetResearchElementByHederaIdFail(action));
          throw error;
        }),
      );
  }

  @Action(HomeAction.GetResearchElementByHederaIdSuccess)
  getResearchElementByHederaIdSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetResearchElementByHederaIdSuccess): void {
    const state: Partial<HomeStateModel> = {
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      associatedResearchElement: action.researchElement,
    };
    if (MetadataUtil.isResearchObject(action.researchElement)) {
      state.associatedResearchObject = action.researchElement as ResearchObjectMetadataResult;
    } else if (MetadataUtil.isResearchDataFile(action.researchElement)) {
      state.associatedResearchDataFile = action.researchElement as ResearchDataFileMetadataResult;
    }
    ctx.patchState(state);
  }

  @Action(HomeAction.GetResearchElementByHederaIdFail)
  getResearchElementByHederaIdFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetResearchElementByHederaIdFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}

