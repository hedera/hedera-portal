/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - hedera-resource.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {environment} from "@environments/environment";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {HederaResourceOptions} from "@shared/stores/hedera-resource/hedera-resource-options.model";
import {HederaResourceAction} from "@shared/stores/hedera-resource/hedera-resource.action";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  BaseResourceType,
  BaseStateModel,
  CollectionTyped,
  defaultResourceStateInitValue,
  NotificationService,
  RegisterDefaultAction,
  ResourceState,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StatusHistory,
} from "solidify-frontend";
import {HederaResourceActionHelper} from "./hedera-resource-action.helper";
import {HederaResourceNameSpace} from "./hedera-resource-namespace.model";
import {HederaResourceStateModel} from "./hedera-resource-state.model";

export const defaultHederaResourceStateInitValue: HederaResourceStateModel<BaseResourceType> = {
  ...defaultResourceStateInitValue(),
  history: [],
  isLoadingHistoryCounter: 0,
};

export abstract class HederaResourceState<TResource extends BaseResourceType> extends ResourceState<BaseStateModel, TResource> {
  protected readonly _nameSpace: HederaResourceNameSpace;

  protected constructor(protected _apiService: ApiService,
                        protected _store: Store,
                        protected _notificationService: NotificationService,
                        protected _actions$: Actions,
                        protected _options: HederaResourceOptions) {
    super(_apiService, _store, _notificationService, _actions$, environment, _options);
  }

  @RegisterDefaultAction((hederaResourceNameSpace: HederaResourceNameSpace) => hederaResourceNameSpace.History, {}, ResourceState)
  history(ctx: SolidifyStateContext<HederaResourceStateModel<TResource>>, action: HederaResourceAction.History): Observable<CollectionTyped<StatusHistory>> {
    ctx.patchState({
      isLoadingHistoryCounter: ctx.getState().isLoadingHistoryCounter + 1,
    });
    return this._apiService.getCollection<StatusHistory>(`${this._urlResource}/${action.id}/${ApiResourceNameEnum.HISTORY}`, null)
      .pipe(
        tap((list: CollectionTyped<StatusHistory>) => {
          ctx.dispatch(HederaResourceActionHelper.historySuccess(this._nameSpace, action, list));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(HederaResourceActionHelper.historyFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @RegisterDefaultAction((hederaResourceNameSpace: HederaResourceNameSpace) => hederaResourceNameSpace.HistorySuccess, {}, ResourceState)
  historySuccess(ctx: SolidifyStateContext<HederaResourceStateModel<TResource>>, action: HederaResourceAction.HistorySuccess): void {
    ctx.patchState({
      history: action.list._data,
      isLoadingHistoryCounter: ctx.getState().isLoadingHistoryCounter - 1,
    });
  }
}
