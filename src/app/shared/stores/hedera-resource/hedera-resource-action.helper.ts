/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - hedera-resource-action.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {ResourceActionHelper} from "solidify-frontend";
import {HederaResourceNameSpace} from "./hedera-resource-namespace.model";
import {HederaResourceAction} from "./hedera-resource.action";

export class HederaResourceActionHelper extends ResourceActionHelper {
  static history(hederaResourceNameSpace: HederaResourceNameSpace, ...args: ConstructorParameters<typeof HederaResourceAction.History>): HederaResourceAction.History {
    return new hederaResourceNameSpace.History(...args);
  }

  static historySuccess(hederaResourceNameSpace: HederaResourceNameSpace, ...args: ConstructorParameters<typeof HederaResourceAction.HistorySuccess>): HederaResourceAction.HistorySuccess {
    return new hederaResourceNameSpace.HistorySuccess(...args);
  }

  static historyFail(hederaResourceNameSpace: HederaResourceNameSpace, ...args: ConstructorParameters<typeof HederaResourceAction.HistoryFail>): HederaResourceAction.HistoryFail {
    return new hederaResourceNameSpace.HistoryFail(...args);
  }
}
