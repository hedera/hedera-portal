/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  SharedInstitutionState,
  SharedInstitutionStateModel,
} from "@app/shared/stores/institution/shared-institution.state";
import {
  SharedLicenseState,
  SharedLicenseStateModel,
} from "@app/shared/stores/license/shared-license.state";
import {
  SharedPersonState,
  SharedPersonStateModel,
} from "@app/shared/stores/person/shared-person.state";
import {
  SharedProjectState,
  SharedProjectStateModel,
} from "@app/shared/stores/project/shared-project.state";
import {State} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {
  SharedFundingAgencyState,
  SharedFundingAgencyStateModel,
} from "@shared/stores/funding-agency/shared-funding-agency.state";
import {
  SharedIIIFCollectionSettingsState,
  SharedIIIFCollectionSettingsStateModel,
} from "@shared/stores/iiif-collection-settings/shared-iiif-collection-settings.state";
import {
  SharedOntologyState,
  SharedOntologyStateModel,
} from "@shared/stores/ontology/shared-ontology.state";
import {
  SharedResearchDataFileState,
  SharedResearchDataFileStateModel,
} from "@shared/stores/research-data-file/shared-research-data-file.state";
import {
  SharedResearchObjectTypeState,
  SharedResearchObjectTypeStateModel,
} from "@shared/stores/research-object-type/shared-research-object-type.state";
import {
  SharedRmlState,
  SharedRmlStateModel,
} from "@shared/stores/rml/shared-rml.state";
import {
  SharedRoleState,
  SharedRoleStateModel,
} from "@shared/stores/role/shared-role.state";
import {
  SharedSourceDatasetFileState,
  SharedSourceDatasetFileStateModel,
} from "@shared/stores/source-dataset-file/shared-source-dataset-file.state";
import {
  SharedSourceDatasetState,
  SharedSourceDatasetStateModel,
} from "@shared/stores/source-dataset/shared-source-dataset.state";
import {SharedUserState} from "@shared/stores/user/shared-user.state";
import {
  BaseStateModel,
  SharedGlobalBannerState,
  SharedGlobalBannerStateModel,
  SharedIndexFieldAliasState,
  SharedIndexFieldAliasStateModel,
  SharedLanguageState,
  SharedLanguageStateModel,
  SharedOaiMetadataPrefixState,
  SharedOaiMetadataPrefixStateModel,
  SharedOaiSetState,
  SharedOaiSetStateModel,
} from "solidify-frontend";

export interface SharedStateModel extends BaseStateModel {
  [StateEnum.shared_project]: SharedProjectStateModel;
  [StateEnum.shared_language]: SharedLanguageStateModel;
  [StateEnum.shared_license]: SharedLicenseStateModel;
  [StateEnum.shared_globalBanner]: SharedGlobalBannerStateModel;
  [StateEnum.shared_person]: SharedPersonStateModel;
  [StateEnum.shared_institution]: SharedInstitutionStateModel;
  [StateEnum.shared_role]: SharedRoleStateModel;
  [StateEnum.shared_fundingAgency]: SharedFundingAgencyStateModel;
  [StateEnum.shared_iiifCollectionSettings]: SharedIIIFCollectionSettingsStateModel;
  [StateEnum.shared_indexFieldAlias]: SharedIndexFieldAliasStateModel;
  [StateEnum.shared_oaiSet]: SharedOaiSetStateModel;
  [StateEnum.shared_oaiMetadataPrefix]: SharedOaiMetadataPrefixStateModel;
  [StateEnum.shared_ontology]: SharedOntologyStateModel;
  [StateEnum.shared_researchObjectType]: SharedResearchObjectTypeStateModel;
  [StateEnum.shared_rml]: SharedRmlStateModel;
  [StateEnum.shared_sourceDataset]: SharedSourceDatasetStateModel;
  [StateEnum.shared_sourceDatasetFile]: SharedSourceDatasetFileStateModel;
  [StateEnum.shared_researchDataFile]: SharedResearchDataFileStateModel;
}

@Injectable()
@State<SharedStateModel>({
  name: StateEnum.shared,
  defaults: {
    isLoadingCounter: 0,
    [StateEnum.shared_project]: null,
    [StateEnum.shared_language]: null,
    [StateEnum.shared_license]: null,
    [StateEnum.shared_globalBanner]: undefined,
    [StateEnum.shared_person]: null,
    [StateEnum.shared_institution]: null,
    [StateEnum.shared_role]: null,
    [StateEnum.shared_fundingAgency]: null,
    [StateEnum.shared_iiifCollectionSettings]: null,
    [StateEnum.shared_indexFieldAlias]: null,
    [StateEnum.shared_oaiSet]: undefined,
    [StateEnum.shared_oaiMetadataPrefix]: undefined,
    [StateEnum.shared_ontology]: undefined,
    [StateEnum.shared_researchObjectType]: undefined,
    [StateEnum.shared_rml]: undefined,
    [StateEnum.shared_sourceDataset]: undefined,
    [StateEnum.shared_sourceDatasetFile]: undefined,
    [StateEnum.shared_researchDataFile]: undefined,
  },
  children: [
    SharedProjectState,
    SharedLanguageState,
    SharedLicenseState,
    SharedGlobalBannerState,
    SharedPersonState,
    SharedInstitutionState,
    SharedRoleState,
    SharedFundingAgencyState,
    SharedUserState,
    SharedOaiSetState,
    SharedOaiMetadataPrefixState,
    SharedIIIFCollectionSettingsState,
    SharedIndexFieldAliasState,
    SharedOntologyState,
    SharedRmlState,
    SharedResearchObjectTypeState,
    SharedSourceDatasetState,
    SharedSourceDatasetFileState,
    SharedResearchDataFileState,
  ],
})
export class SharedState {
}
