/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-institution.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {Injectable} from "@angular/core";
import {HEDERA_CONSTANTS} from "@app/constants";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {
  SharedInstitutionAction,
  sharedInstitutionActionNameSpace,
} from "@app/shared/stores/institution/shared-institution.action";
import {environment} from "@environments/environment";
import {Institution} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  defaultResourceFileStateInitValue,
  DownloadService,
  NotificationService,
  QueryParameters,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
} from "solidify-frontend";

export interface SharedInstitutionStateModel extends ResourceFileStateModel<Institution> {
}

@Injectable()
@State<SharedInstitutionStateModel>({
  name: StateEnum.shared_institution,
  defaults: {
    ...defaultResourceFileStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
  },
})
export class SharedInstitutionState extends ResourceFileState<SharedInstitutionStateModel, Institution> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: sharedInstitutionActionNameSpace,
    }, _downloadService, ResourceFileStateModeEnum.logo, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminInstitutions;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Action(SharedInstitutionAction.GetByIdentifierId)
  getByIdentifierId(ctx: SolidifyStateContext<SharedInstitutionStateModel>, action: SharedInstitutionAction.GetByIdentifierId): Observable<Institution> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.getById<Institution>(this._urlResource, action.identifierId, {
      [HEDERA_CONSTANTS.IDENTIFIER_TYPE_PARAM]: action.identifierType,
    })
      .pipe(
        tap(result => {
          ctx.dispatch(new SharedInstitutionAction.GetByIdentifierIdSuccess(action, result));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedInstitutionAction.GetByIdentifierIdFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedInstitutionAction.GetByIdentifierIdSuccess)
  getByIdentifierIdSuccess(ctx: SolidifyStateContext<SharedInstitutionStateModel>, action: SharedInstitutionAction.GetByIdentifierIdSuccess): void {
    ctx.patchState({
      current: action.institution,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedInstitutionAction.GetByIdentifierIdFail)
  getByIdentifierIdFail(ctx: SolidifyStateContext<SharedInstitutionStateModel>, action: SharedInstitutionAction.GetByIdentifierIdFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
