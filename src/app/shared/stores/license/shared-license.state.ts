/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-license.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {Injectable} from "@angular/core";
import {HEDERA_CONSTANTS} from "@app/constants";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {
  SharedLicenseAction,
  sharedLicenseActionNameSpace,
} from "@app/shared/stores/license/shared-license.action";
import {License} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  defaultResourceFileStateInitValue,
  DownloadService,
  NotificationService,
  QueryParameters,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  ResourceStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";
import {environment} from "../../../../environments/environment";

export interface SharedLicenseStateModel extends ResourceFileStateModel<License> {
}

@Injectable()
@State<SharedLicenseStateModel>({
  name: StateEnum.shared_license,
  defaults: {
    ...defaultResourceFileStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
  },
})
export class SharedLicenseState extends ResourceFileState<SharedLicenseStateModel, License> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: sharedLicenseActionNameSpace,
    }, _downloadService, ResourceFileStateModeEnum.logo, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminLicenses;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static isLoading<T>(state: ResourceStateModel<T>): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Action(SharedLicenseAction.GetByIdentifierId)
  getByIdentifierId(ctx: SolidifyStateContext<SharedLicenseStateModel>, action: SharedLicenseAction.GetByIdentifierId): Observable<License> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.getById<License>(this._urlResource, action.identifierId, {
      [HEDERA_CONSTANTS.IDENTIFIER_TYPE_PARAM]: action.identifierType,
    })
      .pipe(
        tap(result => {
          ctx.dispatch(new SharedLicenseAction.GetByIdentifierIdSuccess(action, result));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedLicenseAction.GetByIdentifierIdFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedLicenseAction.GetByIdentifierIdSuccess)
  getByIdentifierIdSuccess(ctx: SolidifyStateContext<SharedLicenseStateModel>, action: SharedLicenseAction.GetByIdentifierIdSuccess): void {
    ctx.patchState({
      current: action.license,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedLicenseAction.GetByIdentifierIdFail)
  getByIdentifierIdFail(ctx: SolidifyStateContext<SharedLicenseStateModel>, action: SharedLicenseAction.GetByIdentifierIdFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
