/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - hedera-user-preferences.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Project} from "@models";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {
  CookieConsentUtil,
  CookieType,
  isNullOrUndefined,
  LocalStorageHelper,
  UserPreferencesUtil,
} from "solidify-frontend";

export class HederaUserPreferencesUtil extends UserPreferencesUtil {
  static getCurrentProject(listAuthorizedProject: Project[]): Project | undefined {
    const projectId = LocalStorageHelper.getItem(LocalStorageEnum.currentProject);
    if (isNullOrUndefined(listAuthorizedProject) || listAuthorizedProject.length === 0) {
      return undefined;
    }
    const defaultProject = listAuthorizedProject[0];
    if (isNullOrUndefined(projectId)) {
      return defaultProject;
    }
    let authorizedProject = listAuthorizedProject.find(o => o.resId === projectId);
    if (isNullOrUndefined(authorizedProject)) {
      authorizedProject = defaultProject;
    }
    return authorizedProject;
  }

  static setCurrentProject(projectId: string): string | undefined {
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.currentProject)) {
      LocalStorageHelper.setItem(LocalStorageEnum.currentProject, projectId);
    }
    if (isNullOrUndefined(projectId)) {
      return undefined;
    }
    return projectId;
  }

  static cleanCurrentProject(): void {
    LocalStorageHelper.removeItem(LocalStorageEnum.currentProject);
  }
}
