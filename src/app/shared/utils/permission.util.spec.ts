/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - permission.util.spec.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {PermissionUtil} from "@app/shared/utils/permission.util";
import {Enums} from "@enums";

describe("RoleUtil", () => {
  describe("isRootPermission", () => {
    it("should return true when user is ROOT", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.root];
      expect(PermissionUtil.isRootPermission(userRoles)).toBeTruthy();
    });

    it("should return false when user is ADMIN", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.admin];
      expect(PermissionUtil.isRootPermission(userRoles)).toBeFalsy();
    });

    it("should return false when user is USER", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.user];
      expect(PermissionUtil.isRootPermission(userRoles)).toBeFalsy();
    });

    it("should return false when user is GUEST", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.guest];
      expect(PermissionUtil.isRootPermission(userRoles)).toBeFalsy();
    });

    it("should return false when user have no role", () => {
      const userRoles = [];
      expect(PermissionUtil.isRootPermission(userRoles)).toBeFalsy();
    });

    it("should return true when user is [ROOT, ADMIN]", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.root, Enums.UserApplicationRole.UserApplicationRoleEnum.admin];
      expect(PermissionUtil.isRootPermission(userRoles)).toBeTruthy();
    });

    it("should return true when user is [ROOT, ADMIN, USER]", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.root, Enums.UserApplicationRole.UserApplicationRoleEnum.admin, Enums.UserApplicationRole.UserApplicationRoleEnum.user];
      expect(PermissionUtil.isRootPermission(userRoles)).toBeTruthy();
    });

    it("should return true when user is [ROOT, USER]", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.root, Enums.UserApplicationRole.UserApplicationRoleEnum.user];
      expect(PermissionUtil.isRootPermission(userRoles)).toBeTruthy();
    });

    it("should return false when user is [ADMIN, USER]", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.admin, Enums.UserApplicationRole.UserApplicationRoleEnum.user];
      expect(PermissionUtil.isRootPermission(userRoles)).toBeFalsy();
    });

    it("should return false when user is [GUEST, USER]", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.guest, Enums.UserApplicationRole.UserApplicationRoleEnum.user];
      expect(PermissionUtil.isRootPermission(userRoles)).toBeFalsy();
    });
  });

  describe("isAdminPermission", () => {
    it("should return true when user is ROOT", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.root];
      expect(PermissionUtil.isAdminPermission(userRoles)).toBeTruthy();
    });

    it("should return true when user is ADMIN", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.admin];
      expect(PermissionUtil.isAdminPermission(userRoles)).toBeTruthy();
    });

    it("should return false when user is USER", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.user];
      expect(PermissionUtil.isAdminPermission(userRoles)).toBeFalsy();
    });

    it("should return false when user is GUEST", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.guest];
      expect(PermissionUtil.isAdminPermission(userRoles)).toBeFalsy();
    });

    it("should return false when user have no role", () => {
      const userRoles = [];
      expect(PermissionUtil.isAdminPermission(userRoles)).toBeFalsy();
    });

    it("should return true when user is [ROOT, ADMIN]", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.root, Enums.UserApplicationRole.UserApplicationRoleEnum.admin];
      expect(PermissionUtil.isAdminPermission(userRoles)).toBeTruthy();
    });

    it("should return true when user is [ROOT, ADMIN, USER]", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.root, Enums.UserApplicationRole.UserApplicationRoleEnum.admin, Enums.UserApplicationRole.UserApplicationRoleEnum.user];
      expect(PermissionUtil.isAdminPermission(userRoles)).toBeTruthy();
    });

    it("should return true when user is [ROOT, USER]", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.root, Enums.UserApplicationRole.UserApplicationRoleEnum.user];
      expect(PermissionUtil.isAdminPermission(userRoles)).toBeTruthy();
    });

    it("should return true when user is [ADMIN, USER]", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.admin, Enums.UserApplicationRole.UserApplicationRoleEnum.user];
      expect(PermissionUtil.isAdminPermission(userRoles)).toBeTruthy();
    });

    it("should return false when user is [GUEST, USER]", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.guest, Enums.UserApplicationRole.UserApplicationRoleEnum.user];
      expect(PermissionUtil.isAdminPermission(userRoles)).toBeFalsy();
    });
  });

  describe("isUser", () => {
    it("should return true when user is ROOT", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.root];
      expect(PermissionUtil.isUserPermission(userRoles)).toBeTruthy();
    });

    it("should return true when user is ADMIN", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.admin];
      expect(PermissionUtil.isUserPermission(userRoles)).toBeTruthy();
    });

    it("should return true when user is USER", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.user];
      expect(PermissionUtil.isUserPermission(userRoles)).toBeTruthy();
    });

    it("should return false when user is GUEST", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.guest];
      expect(PermissionUtil.isUserPermission(userRoles)).toBeFalsy();
    });

    it("should return false when user have no role", () => {
      const userRoles = [];
      expect(PermissionUtil.isUserPermission(userRoles)).toBeFalsy();
    });

    it("should return true when user is [ROOT, ADMIN]", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.root, Enums.UserApplicationRole.UserApplicationRoleEnum.admin];
      expect(PermissionUtil.isUserPermission(userRoles)).toBeTruthy();
    });

    it("should return true when user is [ROOT, ADMIN, USER]", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.root, Enums.UserApplicationRole.UserApplicationRoleEnum.admin, Enums.UserApplicationRole.UserApplicationRoleEnum.user];
      expect(PermissionUtil.isUserPermission(userRoles)).toBeTruthy();
    });

    it("should return true when user is [ROOT, USER]", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.root, Enums.UserApplicationRole.UserApplicationRoleEnum.user];
      expect(PermissionUtil.isUserPermission(userRoles)).toBeTruthy();
    });

    it("should return true when user is [ADMIN, USER]", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.admin, Enums.UserApplicationRole.UserApplicationRoleEnum.user];
      expect(PermissionUtil.isUserPermission(userRoles)).toBeTruthy();
    });

    it("should return true when user is [GUEST, USER]", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.guest, Enums.UserApplicationRole.UserApplicationRoleEnum.user];
      expect(PermissionUtil.isUserPermission(userRoles)).toBeTruthy();
    });
  });

  describe("isNoPermission", () => {
    it("should return true when user is ROOT", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.root];
      expect(PermissionUtil.isNoPermission(userRoles)).toBeTruthy();
    });

    it("should return true when user is ADMIN", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.admin];
      expect(PermissionUtil.isNoPermission(userRoles)).toBeTruthy();
    });

    it("should return true when user is USER", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.user];
      expect(PermissionUtil.isNoPermission(userRoles)).toBeTruthy();
    });

    it("should return true when user is GUEST", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.guest];
      expect(PermissionUtil.isNoPermission(userRoles)).toBeTruthy();
    });

    it("should return true when user have no role", () => {
      const userRoles = [];
      expect(PermissionUtil.isNoPermission(userRoles)).toBeTruthy();
    });

    it("should return true when user is [ROOT, ADMIN]", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.root, Enums.UserApplicationRole.UserApplicationRoleEnum.admin];
      expect(PermissionUtil.isNoPermission(userRoles)).toBeTruthy();
    });

    it("should return true when user is [ROOT, ADMIN, USER]", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.root, Enums.UserApplicationRole.UserApplicationRoleEnum.admin, Enums.UserApplicationRole.UserApplicationRoleEnum.user];
      expect(PermissionUtil.isNoPermission(userRoles)).toBeTruthy();
    });

    it("should return true when user is [ROOT, USER]", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.root, Enums.UserApplicationRole.UserApplicationRoleEnum.user];
      expect(PermissionUtil.isNoPermission(userRoles)).toBeTruthy();
    });

    it("should return true when user is [ADMIN, USER]", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.admin, Enums.UserApplicationRole.UserApplicationRoleEnum.user];
      expect(PermissionUtil.isNoPermission(userRoles)).toBeTruthy();
    });

    it("should return true when user is [GUEST, USER]", () => {
      const userRoles = [Enums.UserApplicationRole.UserApplicationRoleEnum.guest, Enums.UserApplicationRole.UserApplicationRoleEnum.user];
      expect(PermissionUtil.isNoPermission(userRoles)).toBeTruthy();
    });
  });
});
