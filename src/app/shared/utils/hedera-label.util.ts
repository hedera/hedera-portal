/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - hedera-label.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {Label} from "@models";
import {Store} from "@ngxs/store";
import {
  LabelUtil,
  LanguagePartialEnum,
} from "solidify-frontend";

export class HederaLabelUtil {
  static getTranslationFromListLabelsWithStore(store: Store, listLabels: Label[]): string {
    return LabelUtil.getTranslationFromListLabelsWithStore(store, environment, listLabels);
  }

  static getTranslationFromListLabels(currentLanguage: Enums.Language.LanguageEnum, listLabels: Label[]): string {
    return LabelUtil.getTranslationFromListLabels(currentLanguage as LanguagePartialEnum, listLabels, environment.labelEnumConverter);
  }

  static isSameLanguage: (currentLanguage: Enums.Language.LanguageEnum, label: Label) => boolean = (currentLanguage, label) =>
    LabelUtil.isSameLanguage(currentLanguage as LanguagePartialEnum, label, environment.labelEnumConverter);
}
