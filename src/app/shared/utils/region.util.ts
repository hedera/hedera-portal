/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - region.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  getNames,
  LocalizedLanguageNames,
  registerLocale,
  toAlpha3T,
} from "@cospired/i18n-iso-languages";
import {Enums} from "@enums";
import {AdminLanguage} from "@shared/models/admin-language.model";

declare const require: any;

export class RegionUtil {

  static getLanguagesWithIsoCode(currentLanguage: Enums.Language.LanguageEnum): AdminLanguage[] {
    registerLocale(require("@cospired/i18n-iso-languages/langs/" + currentLanguage + ".json"));

    const listLanguages: LocalizedLanguageNames = getNames(currentLanguage);
    return Object.entries(listLanguages).map(([k, v]) => ({iso: k.toUpperCase(), name: v, iso3: toAlpha3T(k).toUpperCase()}));
  }
}
