/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - permission.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {ApplicationRolePermissionEnum} from "@app/shared/enums/application-role-permission.enum";
import {Enums} from "@enums";
import {
  isEmptyArray,
  isFalse,
  isNullOrUndefined,
} from "solidify-frontend";

export class PermissionUtil {
  static isRootPermission(userRoles: Enums.UserApplicationRole.UserApplicationRoleEnum[]): boolean {
    return userRoles.includes(Enums.UserApplicationRole.UserApplicationRoleEnum.root);
  }

  static isAdminPermission(userRoles: Enums.UserApplicationRole.UserApplicationRoleEnum[]): boolean {
    return userRoles.includes(Enums.UserApplicationRole.UserApplicationRoleEnum.admin) || userRoles.includes(Enums.UserApplicationRole.UserApplicationRoleEnum.root);
  }

  static isUserPermission(userRoles: Enums.UserApplicationRole.UserApplicationRoleEnum[]): boolean {
    return userRoles.includes(Enums.UserApplicationRole.UserApplicationRoleEnum.user) || userRoles.includes(Enums.UserApplicationRole.UserApplicationRoleEnum.admin) || userRoles.includes(Enums.UserApplicationRole.UserApplicationRoleEnum.root);
  }

  static isNoPermission(userRoles: Enums.UserApplicationRole.UserApplicationRoleEnum[]): boolean {
    return isNullOrUndefined(userRoles)
      || isEmptyArray(userRoles)
      || userRoles.includes(Enums.UserApplicationRole.UserApplicationRoleEnum.guest)
      || userRoles.includes(Enums.UserApplicationRole.UserApplicationRoleEnum.user)
      || userRoles.includes(Enums.UserApplicationRole.UserApplicationRoleEnum.admin)
      || userRoles.includes(Enums.UserApplicationRole.UserApplicationRoleEnum.root);
  }

  static isUserHavePermission(isLogged: boolean, permission: ApplicationRolePermissionEnum, currentUserRole: Enums.UserApplicationRole.UserApplicationRoleEnum[]): boolean {
    if (isNullOrUndefined(permission) || permission === ApplicationRolePermissionEnum.noPermission) {
      return this.isNoPermission(currentUserRole);
    }

    if (isFalse(isLogged) || isNullOrUndefined(currentUserRole) || isEmptyArray(currentUserRole)) {
      return false;
    }

    switch (permission) {
      case ApplicationRolePermissionEnum.rootPermission:
        return this.isRootPermission(currentUserRole);
      case ApplicationRolePermissionEnum.adminPermission:
        return this.isAdminPermission(currentUserRole);
      case ApplicationRolePermissionEnum.userPermission:
        return this.isUserPermission(currentUserRole);
      default:
        return true;
    }
  }
}
