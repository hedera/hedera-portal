/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {DragDropModule} from "@angular/cdk/drag-drop";
import {LayoutModule} from "@angular/cdk/layout";
import {TextFieldModule} from "@angular/cdk/text-field";
import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {
  FormsModule,
  ReactiveFormsModule,
} from "@angular/forms";
import {
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatDialogConfig,
} from "@angular/material/dialog";
import {RouterModule} from "@angular/router";
import {MaterialModule} from "@app/material.module";
import {SharedProjectNameContainer} from "@app/shared/components/containers/shared-project-name/shared-project-name.container";
import {SharedTableProjectContainer} from "@app/shared/components/containers/shared-table-project/shared-table-project.container";
import {SharedProjectOverlayPresentational} from "@app/shared/components/presentationals/shared-project-overlay/shared-project-overlay.presentational";
import {SharedInstitutionState} from "@app/shared/stores/institution/shared-institution.state";
import {SharedLicenseState} from "@app/shared/stores/license/shared-license.state";
import {SharedPersonState} from "@app/shared/stores/person/shared-person.state";
import {SharedProjectState} from "@app/shared/stores/project/shared-project.state";
import {SharedState} from "@app/shared/stores/shared.state";
import {
  FaIconLibrary,
  FontAwesomeModule,
} from "@fortawesome/angular-fontawesome";
import {fas} from "@fortawesome/free-solid-svg-icons";
import {FormlyModule} from "@ngx-formly/core";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {SharedAdditionalInformationPanelContainer} from "@shared/components/containers/shared-additional-information-panel/shared-additional-information-panel.container";
import {SharedDataFileInformationContainer} from "@shared/components/containers/shared-data-file-information/shared-data-file-information.container";
import {SharedResourceRoleMemberContainer} from "@shared/components/containers/shared-resource-role-member/shared-resource-role-member-container.component";
import {SharedTablePersonRoleContainer} from "@shared/components/containers/shared-table-person-role/shared-table-person-role.container";
import {SharedTableProjectRoleContainer} from "@shared/components/containers/shared-table-project-role/shared-table-project-role.container";
import {SharedUserguideSidebarContainer} from "@shared/components/containers/shared-userguide-sidebar/shared-userguide-sidebar.container";
import {SharedAccessLevelWithEmbargoPresentational} from "@shared/components/presentationals/shared-access-level-with-embargo/shared-access-level-with-embargo.presentational";
import {SharedAccessLevelPresentational} from "@shared/components/presentationals/shared-access-level/shared-access-level.presentational";
import {SharedChecksumItemPresentational} from "@shared/components/presentationals/shared-checksum-item/shared-checksum-item.presentational";
import {SharedChecksumPresentational} from "@shared/components/presentationals/shared-checksum/shared-checksum.presentational";
import {SharedFundingAgencyOverlayPresentational} from "@shared/components/presentationals/shared-funding-agency-overlay/shared-funding-agency-overlay.presentational";
import {SharedIdentifiersPresentational} from "@shared/components/presentationals/shared-identifiers/shared-identifiers.presentational";
import {SharedInstitutionOverlayPresentational} from "@shared/components/presentationals/shared-institution-overlay/shared-institution-overlay.presentational";
import {SharedLicenseOverlayPresentational} from "@shared/components/presentationals/shared-license-overlay/shared-license-overlay.presentational";
import {SharedOntologyOverlayPresentational} from "@shared/components/presentationals/shared-ontology-overlay/shared-ontology-overlay.presentational";
import {SharedPackageButtonNavigatorPresentational} from "@shared/components/presentationals/shared-package-button-navigator/shared-package-button-navigator.presentational";
import {SharedPersonFormPresentational} from "@shared/components/presentationals/shared-person-form/shared-person-form.presentational";
import {SharedPersonOverlayPresentational} from "@shared/components/presentationals/shared-person-overlay/shared-person-overlay.presentational";
import {SharedProgressBarPresentational} from "@shared/components/presentationals/shared-progress-bar/shared-progress-bar.presentational";
import {SharedProjectStatisticsPresentational} from "@shared/components/presentationals/shared-project-statistics/shared-project-statistics.presentational";
import {SharedResearchElementFormPresentational} from "@shared/components/presentationals/shared-research-element-form/shared-research-element-form.presentational";
import {SharedResearchObjectTilePresentational} from "@shared/components/presentationals/shared-research-object-tile/shared-research-object-tile.presentational";
import {SharedResearchObjectTypeOverlayPresentational} from "@shared/components/presentationals/shared-research-object-type-overlay/shared-research-object-type-overlay.presentational";
import {SharedSparqlCodeEditorPresentational} from "@shared/components/presentationals/shared-sparql-code-editor/shared-sparql-code-editor.presentational";
import {SharedTocPresentational} from "@shared/components/presentationals/shared-toc/shared-toc.presentational";
import {SharedResearchElementDetailRoutable} from "@shared/components/routables/shared-research-element-detail/shared-research-element-detail.routable";
import {SharedResearchObjectNotFoundRoutable} from "@shared/components/routables/shared-research-object-not-found/shared-research-object-not-found.routable";
import {SharedFundingAgencyState} from "@shared/stores/funding-agency/shared-funding-agency.state";
import {HomeState} from "@shared/stores/home/home.state";
import {SharedIIIFCollectionSettingsState} from "@shared/stores/iiif-collection-settings/shared-iiif-collection-settings.state";
import {SharedOntologyState} from "@shared/stores/ontology/shared-ontology.state";
import {SharedPersonInstitutionState} from "@shared/stores/person/institution/shared-people-institution.state";
import {SharedResearchDataFileState} from "@shared/stores/research-data-file/shared-research-data-file.state";
import {SharedResearchObjectTypeState} from "@shared/stores/research-object-type/shared-research-object-type.state";
import {SharedRmlState} from "@shared/stores/rml/shared-rml.state";
import {SharedRoleState} from "@shared/stores/role/shared-role.state";
import {SharedSourceDatasetFileState} from "@shared/stores/source-dataset-file/shared-source-dataset-file.state";
import {SharedSourceDatasetState} from "@shared/stores/source-dataset/shared-source-dataset.state";
import {SharedUserState} from "@shared/stores/user/shared-user.state";
import {HighlightModule} from "ngx-highlightjs";
import {ImageCropperComponent} from "ngx-image-cropper";
import {TourMatMenuModule} from "ngx-ui-tour-md-menu";
import {
  SharedGlobalBannerState,
  SharedIndexFieldAliasState,
  SharedLanguageState,
  SharedOaiMetadataPrefixState,
  SharedOaiSetState,
  SolidifyFrontendApplicationModule,
  SolidifyFrontendRichTextEditorModule,
  SolidifyFrontendTwitterModule,
} from "solidify-frontend";

const routables = [
  SharedResearchElementDetailRoutable,
  SharedResearchObjectNotFoundRoutable,
];
const containers = [
  SharedTableProjectContainer,
  SharedProjectNameContainer,
  SharedResourceRoleMemberContainer,
  SharedUserguideSidebarContainer,
  SharedTablePersonRoleContainer,
  SharedTableProjectRoleContainer,
  SharedAdditionalInformationPanelContainer,
  SharedDataFileInformationContainer,
];
const dialogs = [];
const contents = [];
const presentationals = [
  SharedTocPresentational,
  SharedPersonFormPresentational,
  SharedAccessLevelPresentational,
  SharedAccessLevelWithEmbargoPresentational,
  SharedProgressBarPresentational,
  SharedPersonOverlayPresentational,
  SharedInstitutionOverlayPresentational,
  SharedLicenseOverlayPresentational,
  SharedProjectOverlayPresentational,
  SharedResearchObjectTypeOverlayPresentational,
  SharedOntologyOverlayPresentational,
  SharedFundingAgencyOverlayPresentational,
  SharedPackageButtonNavigatorPresentational,
  SharedIdentifiersPresentational,
  SharedSparqlCodeEditorPresentational,
  SharedResearchObjectTilePresentational,
  SharedResearchElementFormPresentational,
  SharedProjectStatisticsPresentational,
  SharedChecksumPresentational,
  SharedChecksumItemPresentational,
];
const directives = [];
const pipes = [];
const modules = [
  CommonModule,
  FormsModule,
  RouterModule,
  ReactiveFormsModule,
  FormlyModule,
  MaterialModule,
  FontAwesomeModule,
  TranslateModule,
  TextFieldModule,
  LayoutModule,
  DragDropModule,
  HighlightModule,
  TourMatMenuModule,
  SolidifyFrontendApplicationModule,
  SolidifyFrontendRichTextEditorModule,
  SolidifyFrontendTwitterModule,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...contents,
    ...presentationals,
    ...directives,
    ...pipes,
  ],
  imports: [
    ...modules,
    ImageCropperComponent,
    NgxsModule.forFeature([
      HomeState,
      SharedState,
      SharedLicenseState,
      SharedGlobalBannerState,
      SharedLanguageState,
      SharedProjectState,
      SharedPersonState,
      SharedPersonInstitutionState,
      SharedInstitutionState,
      SharedRoleState,
      SharedFundingAgencyState,
      SharedUserState,
      SharedIIIFCollectionSettingsState,
      SharedIndexFieldAliasState,
      SharedOaiSetState,
      SharedOaiMetadataPrefixState,
      SharedIndexFieldAliasState,
      SharedOntologyState,
      SharedRmlState,
      SharedResearchObjectTypeState,
      SharedSourceDatasetState,
      SharedSourceDatasetFileState,
      SharedResearchDataFileState,
    ]),
  ],
  exports: [
    ...modules,
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
    ...contents,
    ...directives,
    ...pipes,
  ],
  providers: [
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {panelClass: "solidify-dialogs", hasBackdrop: true} as MatDialogConfig},
  ],
})
export class SharedModule {
  constructor(private readonly _library: FaIconLibrary) {
    // Fontawesome library
    _library.addIconPacks(fas);
  }
}
