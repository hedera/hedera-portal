/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-institution-overlay.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
} from "@angular/core";
import {Enums} from "@enums";
import {Institution} from "@models";
import {SharedResourceLogoOverlayPresentational} from "@shared/components/presentationals/shared-resource-logo-overlay/shared-resource-logo-overlay.presentational";
import {overlayAnimation} from "@shared/components/presentationals/shared-resource-overlay/shared-resource-overlay.presentational";
import {SharedInstitutionAction} from "@shared/stores/institution/shared-institution.action";
import {SharedInstitutionState} from "@shared/stores/institution/shared-institution.state";
import {
  MappingObjectUtil,
  ResourceFileNameSpace,
  ResourceFileState,
} from "solidify-frontend";

@Component({
  selector: "hedera-shared-institution-overlay",
  templateUrl: "./shared-institution-overlay.presentational.html",
  styleUrls: ["./shared-institution-overlay.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [overlayAnimation],
})
export class SharedInstitutionOverlayPresentational extends SharedResourceLogoOverlayPresentational<Institution> {
  logoActionNameSpace: ResourceFileNameSpace = SharedInstitutionAction;
  logoState: typeof ResourceFileState = SharedInstitutionState as any;

  rodIdUrl: string = undefined;

  protected override _postUpdateData(): void {
    this.rodIdUrl = MappingObjectUtil.get(this.data.identifiers, Enums.IdentifiersEnum.ROR);
  }
}
