/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-toc.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  Output,
  Renderer2,
  ViewChild,
} from "@angular/core";
import {
  MatMenu,
  MatMenuItem,
} from "@angular/material/menu";
import {environment} from "@environments/environment";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {TocHelper} from "@shared/helpers/toc.helper";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {distinctUntilChanged} from "rxjs/operators";
import {
  ArrayUtil,
  BreakpointService,
  KeyValue,
  MappingObject,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  OaiPmhHelper,
  ObservableUtil,
  SsrUtil,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-shared-toc",
  templateUrl: "./shared-toc.presentational.html",
  styleUrls: ["./shared-toc.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedTocPresentational extends SharedAbstractPresentational implements AfterViewInit, OnInit {
  @Input()
  integrationGuide: string;

  apisKeyValue: KeyValue[] = [];

  @Input()
  set apis(value: MappingObject<string, string>) {
    this.apisKeyValue = [];

    MappingObjectUtil.forEach(value, (v, key) => {
      this.apisKeyValue.push({
        key: StringUtil.capitalize(key),
        value: v,
      });
    });
    this.apisKeyValue = ArrayUtil.sortAscendant(this.apisKeyValue, "key");
  }

  @Input()
  toolsGuide: string;

  @Input()
  isLoading: boolean;

  get oaiPmhProvider(): string {
    return OaiPmhHelper.getOaiPmhProviderUrl(environment);
  }

  @ViewChild("menu")
  readonly menu: MatMenu;

  @ViewChild("integrationGuideMenu")
  readonly integrationGuideMenu: MatMenu;

  private readonly _aboutBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("aboutChange")
  readonly aboutObs: Observable<void> = ObservableUtil.asObservable(this._aboutBS);

  matMenuLinkUpdated: MatMenuItem[] = [];
  isMobile: boolean;
  readonly CLASS_MAT_MENU_WITH_LINK: string = "shared-toc-container-panel";

  constructor(private readonly _renderer: Renderer2,
              public readonly breakpointService: BreakpointService,
              private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    this.subscribe(this.breakpointService.isSmallerOrEqualThanMdObs().pipe(
      distinctUntilChanged(),
    ), result => {
      this.isMobile = result;
      this._changeDetector.detectChanges();
    });
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();

    this.subscribe(this.menu._hovered(), (matMenuItem: MatMenuItem) => {
      if (this.matMenuLinkUpdated.includes(matMenuItem)) {
        return;
      }
      this.matMenuLinkUpdated.push(matMenuItem);
      setTimeout(() => {
        TocHelper.updateLinkToc(SsrUtil.window?.document, this._renderer, this.CLASS_MAT_MENU_WITH_LINK, false);
      }, 0);
    });
  }

  getTooltipButton(): string {
    if (this.isMobile) {
      return MARK_AS_TRANSLATABLE("toc.button.doc");
    }
    return MARK_AS_TRANSLATABLE("toc.button.devDoc");
  }

  navigateToAbout(): void {
    this._aboutBS.next();
  }

  navigateTo(url: string): void {
    SsrUtil.window?.open(url, "_blank");
  }
}
