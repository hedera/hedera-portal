/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-sparql-code-editor.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
  Optional,
  Self,
} from "@angular/core";
import {NgControl} from "@angular/forms";
import {environment} from "@environments/environment";
import {Store} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  CodeEditorPresentational,
  isNullOrUndefined,
  NotificationService,
  SsrUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-shared-sparql-code-editor",
  templateUrl: "../../../../../../node_modules/solidify-frontend/lib/code-editor/components/presentationals/code-editor/code-editor.presentational.html",
  styleUrls: ["../../../../../../node_modules/solidify-frontend/lib/code-editor/components/presentationals/code-editor/code-editor.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedSparqlCodeEditorPresentational extends CodeEditorPresentational implements OnInit {
  sparqlParser: any;

  override syntaxValidMessage: string = LabelTranslateEnum.queryValid;
  override syntaxInvalidMessage: string = LabelTranslateEnum.queryInvalid;
  override COPY_BUTTON_LABEL: string = LabelTranslateEnum.copyQuery;
  protected override _COPY_NOTIFICATION_LABEL: string = LabelTranslateEnum.copiedQueryToClipboard;

  override mode: "sparql" = "sparql";

  constructor(protected readonly _injector: Injector,
              @Self() @Optional() protected readonly _ngControl: NgControl,
              protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _notificationService: NotificationService) {
    super(_injector, environment, _ngControl, _store, _changeDetector, _notificationService);
    if (SsrUtil.isBrowser) {
      this.sparqlParser = new (require("sparqljs").Parser)({skipValidation: false});
      this.parser = (value: string) => {
        if (isNullOrUndefined(this.sparqlParser)) {
          return undefined;
        }
        // Throw an error if wrong syntax
        return this.sparqlParser.parse(value);
      };
    }
  }
}
