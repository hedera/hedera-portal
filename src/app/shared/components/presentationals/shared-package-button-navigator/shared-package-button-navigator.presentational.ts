/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-package-button-navigator.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {
  RoutesEnum,
  SourceDatasetRoutesEnum,
} from "@shared/enums/routes.enum";
import {isNullOrUndefinedOrWhiteString} from "solidify-frontend";

@Component({
  selector: "hedera-shared-package-button-navigator",
  templateUrl: "./shared-package-button-navigator.presentational.html",
  styleUrls: ["./shared-package-button-navigator.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedPackageButtonNavigatorPresentational extends SharedAbstractPresentational {
  @Input()
  currentViewMode: PackageButtonNavigatorTypeEnum | undefined;

  @Input()
  datasetId: string;

  @Input()
  projectId: string;

  @Input()
  sipId: string;

  @Input()
  dipId: string;

  @Input()
  aipId: string;

  @Input()
  currentStorageIndex: number;

  get packageButtonNavigatorTypeEnum(): typeof PackageButtonNavigatorTypeEnum {
    return PackageButtonNavigatorTypeEnum;
  }

  constructor(private readonly _store: Store) {
    super();
  }

  navigateToSourceDataset(): void {
    if (isNullOrUndefinedOrWhiteString(this.projectId) || isNullOrUndefinedOrWhiteString(this.datasetId)) {
      return;
    }
    this._store.dispatch(new Navigate([RoutesEnum.ingestSourceDatasetCreate, this.projectId, SourceDatasetRoutesEnum.detail, this.datasetId]));
  }

  navigateTo(targetPackage: PackageButtonNavigatorTypeEnum, id: string): void {
    if (isNullOrUndefinedOrWhiteString(id)) {
      return;
    }
    const url = this._getPath(targetPackage);
    this._store.dispatch(new Navigate([...url, id]));
  }

  private _getPath(targetPackage: PackageButtonNavigatorTypeEnum): string[] {
    return [RoutesEnum.homeDetail];
  }
}

export type PackageButtonNavigatorTypeEnum = "dataset";
export const PackageButtonNavigatorTypeEnum = {
  dataset: "dataset" as PackageButtonNavigatorTypeEnum,
};
