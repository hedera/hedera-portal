/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-doi-menu.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild,
} from "@angular/core";
import {MatMenu} from "@angular/material/menu";
import {appActionNameSpace} from "@app/stores/app.action";
import {environment} from "@environments/environment";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {Observable} from "rxjs";
import {
  ClipboardUtil,
  isFalse,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefinedOrWhiteString,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-shared-doi-menu",
  templateUrl: "./shared-doi-menu.presentational.html",
  styleUrls: ["./shared-doi-menu.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  exportAs: "doiMenu",
})
export class SharedDoiMenuPresentational extends SharedAbstractPresentational implements OnChanges {
  @Input()
  mode: "dataset" | "archive";

  @Input()
  doi: string;

  shortDoi: string | undefined;

  @ViewChild(MatMenu, {static: true}) menu: MatMenu;

  private _inErrorWhenRetrieveShortDoi: boolean = false;

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _notificationService: NotificationService) {
    super();
  }

  ngOnChanges(changes: SimpleChanges): void {
    super.ngOnChanges(changes);

    if (isNotNullNorUndefinedNorWhiteString(this.doi) && this.mode === "archive" && isNullOrUndefinedOrWhiteString(this.shortDoi) && isFalse(this._inErrorWhenRetrieveShortDoi)) {
      this.subscribe(this._getShortDoiObs());
    }
  }

  private _getShortDoiObs(): Observable<any> {
    return StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new appActionNameSpace.GetShortDoi(this.doi),
      appActionNameSpace.GetShortDoiSuccess,
      result => {
        this.shortDoi = result.shortDoi;
        this._changeDetector.detectChanges();
      },
      appActionNameSpace.GetShortDoiFail,
      () => {
        this._inErrorWhenRetrieveShortDoi = true;
      });
  }

  copyShortDoi(): void {
    ClipboardUtil.copyStringToClipboard(this.shortDoi);
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("doi.notification.shortDoiCopyToClipboard"));
  }

  copyLongDoi(): void {
    ClipboardUtil.copyStringToClipboard(this.doi);
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("doi.notification.longDoiCopyToClipboard"));
  }

  copyUrlShortDoi(): void {
    ClipboardUtil.copyStringToClipboard(environment.doiLink + this.shortDoi);
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("doi.notification.urlShortDoiCopyToClipboard"));
  }

  copyUrlLongDoi(): void {
    ClipboardUtil.copyStringToClipboard(environment.doiLink + this.doi);
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("doi.notification.urlLongDoiCopyToClipboard"));
  }

  navigateToDoi(): void {
    window.open(environment.doiLink + this.doi, "_blank");
  }
}

