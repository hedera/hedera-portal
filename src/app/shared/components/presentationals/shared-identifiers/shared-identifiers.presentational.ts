/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-identifiers.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {Enums} from "@enums";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {
  MappingObject,
  MappingObjectUtil,
} from "solidify-frontend";
import IdentifiersEnum = Enums.IdentifiersEnum;

@Component({
  selector: "hedera-shared-identifiers",
  templateUrl: "./shared-identifiers.presentational.html",
  styleUrls: ["./shared-identifiers.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedIdentifiersPresentational extends SharedAbstractPresentational {
  @Input()
  identifiers: MappingObject<IdentifiersEnum, string>;

  get identifiersKeys(): IdentifiersEnum[] {
    return MappingObjectUtil.keys(this.identifiers);
  }

  getIdentifierValue(identifier: IdentifiersEnum): string {
    return MappingObjectUtil.get(this.identifiers, identifier);
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  constructor() {
    super();
  }

  getIcon(identifier: IdentifiersEnum): IconNameEnum {
    if (identifier === IdentifiersEnum.CrossrefFunder) {
      return IconNameEnum.crossref;
    }
    if (identifier === IdentifiersEnum.GRID) {
      return IconNameEnum.grid;
    }
    if (identifier === IdentifiersEnum.ISNI) {
      return IconNameEnum.isni;
    }
    if (identifier === IdentifiersEnum.ROR) {
      return IconNameEnum.ror;
    }
    if (identifier === IdentifiersEnum.WEB) {
      return IconNameEnum.externalLink;
    }
    if (identifier === IdentifiersEnum.Wikidata) {
      return IconNameEnum.wikidata;
    }
    return IconNameEnum.notFound;
  }
}
