/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-abstract.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {Directive} from "@angular/core";
import {
  FloatLabelType,
  MatFormFieldAppearance,
  SubscriptSizing,
} from "@angular/material/form-field";
import {environment} from "@environments/environment";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AbstractPresentational,
  EnumUtil,
  FormValidationHelper,
} from "solidify-frontend";

@Directive()
export abstract class SharedAbstractPresentational extends AbstractPresentational {
  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get dataTestEnum(): typeof DataTestEnum {
    return DataTestEnum;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  get appearanceInputMaterial(): MatFormFieldAppearance {
    return environment.appearanceInputMaterial;
  }

  get displayEmptyRequiredFieldInError(): boolean {
    return environment.displayEmptyRequiredFieldInError;
  }

  get positionLabelInputMaterial(): FloatLabelType {
    return environment.positionLabelInputMaterial;
  }

  get subscriptSizingInputMaterial(): SubscriptSizing {
    return environment.subscriptSizingInputMaterial;
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }
}
