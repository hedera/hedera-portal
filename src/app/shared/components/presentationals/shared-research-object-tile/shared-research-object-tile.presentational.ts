/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-research-object-tile.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostListener,
  Input,
  Output,
} from "@angular/core";
import {MetadataUtil} from "@home/helpers/metadata.util";
import {AbstractResearchElementMetadataResult} from "@models";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {ImageDisplayModeEnum} from "@shared/enums/image-display-mode.enum";
import {ResearchElementHelper} from "@shared/helpers/research-element.helper";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {tap} from "rxjs/operators";
import {
  ButtonThemeEnum,
  DownloadService,
  ImageFileVisualizerService,
  ObservableUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-shared-research-object-tile",
  templateUrl: "./shared-research-object-tile.presentational.html",
  styleUrls: ["./shared-research-object-tile.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedResearchObjectTilePresentational extends SharedAbstractPresentational {
  private _researchElement: AbstractResearchElementMetadataResult;

  thumbnailUrl: string | undefined;

  @Input()
  set researchElement(value: AbstractResearchElementMetadataResult) {
    this._researchElement = value;
    this.computeThumbnailUrl();
  }

  get researchElement(): AbstractResearchElementMetadataResult {
    return this._researchElement;
  }

  @Input()
  isNotLogged: boolean = false;

  private readonly _downloadBS: BehaviorSubject<AbstractResearchElementMetadataResult | undefined> = new BehaviorSubject<AbstractResearchElementMetadataResult | undefined>(undefined);
  @Output("downloadChange")
  readonly downloadObs: Observable<AbstractResearchElementMetadataResult | undefined> = ObservableUtil.asObservable(this._downloadBS);

  private readonly _deleteBS: BehaviorSubject<AbstractResearchElementMetadataResult | undefined> = new BehaviorSubject<AbstractResearchElementMetadataResult | undefined>(undefined);
  @Output("deleteChange")
  readonly deleteObs: Observable<AbstractResearchElementMetadataResult | undefined> = ObservableUtil.asObservable(this._deleteBS);

  private readonly _selectBS: BehaviorSubject<AbstractResearchElementMetadataResult | undefined> = new BehaviorSubject<AbstractResearchElementMetadataResult | undefined>(undefined);
  @Output("selectChange")
  readonly selectObs: Observable<AbstractResearchElementMetadataResult | undefined> = ObservableUtil.asObservable(this._selectBS);

  @HostListener("click", ["$event"]) click(mouseEvent: MouseEvent): void {
    this._selectBS.next(this.researchElement);
  }

  get downloadButtonTheme(): ButtonThemeEnum {
    return ButtonThemeEnum.flatButton;
  }

  get imageDisplayModeEnum(): typeof ImageDisplayModeEnum {
    return ImageDisplayModeEnum;
  }

  get metadataUtil(): typeof MetadataUtil {
    return MetadataUtil;
  }

  get isResearchObject(): boolean {
    return MetadataUtil.isResearchObject(this.researchElement);
  }

  constructor(private readonly _imageFileVisualizerService: ImageFileVisualizerService,
              private readonly _downloadService: DownloadService,
              private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  download(mouseEvent: MouseEvent): void {
    mouseEvent.stopPropagation();
    this._downloadBS.next(this.researchElement);
  }

  computeThumbnailUrl(): void {
    this.thumbnailUrl = undefined;
    this.subscribe(ResearchElementHelper.getDataFileThumbnailUrl(this._downloadService, this._imageFileVisualizerService, this.researchElement).pipe(
      tap(thumbnailUrl => {
        this.thumbnailUrl = null;
        this._changeDetector.detectChanges();
        this.thumbnailUrl = thumbnailUrl;
        this._changeDetector.detectChanges();
      }),
    ));
  }
}
