/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-project-overlay.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
} from "@angular/core";
import {SharedProjectAction} from "@app/shared/stores/project/shared-project.action";
import {SharedProjectState} from "@app/shared/stores/project/shared-project.state";
import {Project} from "@models";
import {SharedResourceLogoOverlayPresentational} from "@shared/components/presentationals/shared-resource-logo-overlay/shared-resource-logo-overlay.presentational";
import {overlayAnimation} from "@shared/components/presentationals/shared-resource-overlay/shared-resource-overlay.presentational";
import {
  ResourceFileNameSpace,
  ResourceFileState,
} from "solidify-frontend";

@Component({
  selector: "hedera-shared-project-overlay",
  templateUrl: "./shared-project-overlay.presentational.html",
  styleUrls: ["./shared-project-overlay.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [overlayAnimation],
})
export class SharedProjectOverlayPresentational extends SharedResourceLogoOverlayPresentational<Project> {
  logoActionNameSpace: ResourceFileNameSpace = SharedProjectAction;
  logoState: typeof ResourceFileState = SharedProjectState as any;
}
