/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-access-level.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from "@angular/core";
import {Enums} from "@enums";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {EnumUtil} from "solidify-frontend";

@Component({
  selector: "hedera-shared-access-level",
  templateUrl: "./shared-access-level.presentational.html",
  styleUrls: ["./shared-access-level.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedAccessLevelPresentational extends SharedAbstractPresentational {
  _accessLevel: Enums.Access.AccessEnum;
  accessLevelIconName: IconNameEnum;
  labelToTranslate: string;

  @Input()
  set accessLevel(accessLevel: Enums.Access.AccessEnum) {
    this._accessLevel = accessLevel;
    this._computeAccessLevelIcon(accessLevel);
    this._computeLabel(accessLevel);
  }

  get accessLevel(): Enums.Access.AccessEnum {
    return this._accessLevel;
  }

  @Input()
  size: string = "size-24";

  @Input()
  withTooltip: boolean = false;

  constructor(private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  private _computeAccessLevelIcon(accessLevel: Enums.Access.AccessEnum): void {
    switch (accessLevel) {
      case Enums.Access.AccessEnum.RESTRICTED:
        this.accessLevelIconName = IconNameEnum.accessLevelRestricted;
        break;
      case Enums.Access.AccessEnum.CLOSED:
        this.accessLevelIconName = IconNameEnum.accessLevelClosed;
        break;
      case Enums.Access.AccessEnum.PUBLIC:
        this.accessLevelIconName = IconNameEnum.accessLevelPublic;
        break;
      default:
        this.accessLevelIconName = IconNameEnum.accessLevelUndefined;
        break;
    }
  }

  private _computeLabel(accessLevel: Enums.Access.AccessEnum): void {
    this.labelToTranslate = EnumUtil.getLabel(Enums.Access.AccessEnumTranslate, accessLevel);
  }
}
