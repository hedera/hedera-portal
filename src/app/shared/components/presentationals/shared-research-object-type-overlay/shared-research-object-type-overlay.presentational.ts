/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-research-object-type-overlay.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
} from "@angular/core";
import {ResearchObjectType} from "@models";
import {
  overlayAnimation,
  SharedResourceOverlayPresentational,
} from "@shared/components/presentationals/shared-resource-overlay/shared-resource-overlay.presentational";

@Component({
  selector: "hedera-shared-research-object-type-overlay",
  templateUrl: "./shared-research-object-type-overlay.presentational.html",
  styleUrls: ["./shared-research-object-type-overlay.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [overlayAnimation],
})
export class SharedResearchObjectTypeOverlayPresentational extends SharedResourceOverlayPresentational<ResearchObjectType> {

}
