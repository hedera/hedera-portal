/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-research-element-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnInit,
  Output,
} from "@angular/core";
import {FormControl} from "@angular/forms";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {Enums} from "@enums";
import {MetadataUtil} from "@home/helpers/metadata.util";
import {
  AbstractResearchElementMetadataResult,
  ResearchDataFileMetadataResult,
  ResearchObjectMetadataResult,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {SharedOntologyOverlayPresentational} from "@shared/components/presentationals/shared-ontology-overlay/shared-ontology-overlay.presentational";
import {SharedProjectOverlayPresentational} from "@shared/components/presentationals/shared-project-overlay/shared-project-overlay.presentational";
import {SharedResearchObjectTypeOverlayPresentational} from "@shared/components/presentationals/shared-research-object-type-overlay/shared-research-object-type-overlay.presentational";
import {SharedResourceOverlayPresentational} from "@shared/components/presentationals/shared-resource-overlay/shared-resource-overlay.presentational";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {ImageDisplayModeEnum} from "@shared/enums/image-display-mode.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {ResearchElementModuleEnum} from "@shared/enums/research-element-module.enum";
import {
  BrowseProjectRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {ViewModeTableEnum} from "@shared/enums/view-mode-table.enum";
import {ResearchElementHelper} from "@shared/helpers/research-element.helper";
import {KeyValueLogo} from "@shared/models/key-value-logo.model";
import {SecurityService} from "@shared/services/security.service";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  BaseResource,
  ClipboardUtil,
  CookieConsentUtil,
  CookieType,
  DateUtil,
  DownloadService,
  EnumUtil,
  ExtendEnum,
  FileUtil,
  IconNamePartialEnum,
  ImageFileVisualizerService,
  isNonEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  KeyValue,
  LocalStorageHelper,
  MappingObjectUtil,
  NotificationService,
  ObservableUtil,
  RegexUtil,
  SsrUtil,
  StringUtil,
  Type,
} from "solidify-frontend";
import AccessibleFromEnum = Enums.ResearchDataFile.AccessibleFromEnum;

@Component({
  selector: "hedera-shared-research-element-form",
  templateUrl: "./shared-research-element-form.presentational.html",
  styleUrls: ["./shared-research-element-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedResearchElementFormPresentational extends SharedAbstractPresentational implements OnInit, OnChanges, AfterViewInit {
  // readonly TIME_BEFORE_DISPLAY_TOOLTIP: number = environment.timeBeforeDisplayTooltip;
  // readonly disableAccessRequest: boolean = environment.disableAccessRequest;

  projectThumbnailUrl: string | undefined;
  dataFileThumbnailUrl: string | undefined;

  formControlRdfFormat: FormControl = new FormControl();

  private readonly _downloadBS: BehaviorSubject<AbstractResearchElementMetadataResult | undefined> = new BehaviorSubject<AbstractResearchElementMetadataResult | undefined>(undefined);
  @Output("downloadChange")
  readonly downloadObs: Observable<AbstractResearchElementMetadataResult | undefined> = ObservableUtil.asObservable(this._downloadBS);

  private readonly _showDetailBS: BehaviorSubject<AbstractResearchElementMetadataResult | undefined> = new BehaviorSubject<AbstractResearchElementMetadataResult | undefined>(undefined);
  @Output("showDetailChange")
  readonly showDetailObs: Observable<AbstractResearchElementMetadataResult | undefined> = ObservableUtil.asObservable(this._showDetailBS);

  private readonly _addToCartBS: BehaviorSubject<AbstractResearchElementMetadataResult | undefined> = new BehaviorSubject<AbstractResearchElementMetadataResult | undefined>(undefined);
  @Output("addToCartChange")
  readonly addToCartObs: Observable<AbstractResearchElementMetadataResult | undefined> = ObservableUtil.asObservable(this._addToCartBS);

  private readonly _askAccessBS: BehaviorSubject<AbstractResearchElementMetadataResult | undefined> = new BehaviorSubject<AbstractResearchElementMetadataResult | undefined>(undefined);
  @Output("askAccessChange")
  readonly askAccessObs: Observable<AbstractResearchElementMetadataResult | undefined> = ObservableUtil.asObservable(this._askAccessBS);

  private readonly _navigateBS: BehaviorSubject<Navigate> = new BehaviorSubject<Navigate>(undefined);
  @Output("navigateChange")
  readonly navigateObs: Observable<Navigate> = ObservableUtil.asObservable(this._navigateBS);

  private readonly _rdfFormatBS: BehaviorSubject<Enums.Access.RdfFormatEnum> = new BehaviorSubject<Enums.Access.RdfFormatEnum>(undefined);
  @Output("rdfFormatChange")
  readonly rdfFormatObs: Observable<Enums.Access.RdfFormatEnum> = ObservableUtil.asObservable(this._rdfFormatBS);

  private readonly _rdfNavigationBS: BehaviorSubject<string> = new BehaviorSubject<string>(undefined);
  @Output("rdfNavigationChange")
  readonly rdfNavigationObs: Observable<string> = ObservableUtil.asObservable(this._rdfNavigationBS);

  // private readonly _rateBS: BehaviorSubject<ArchiveUserRating[]> = new BehaviorSubject<ArchiveUserRating[]>(undefined);
  // @Output("rateChange")
  // readonly rateObs: Observable<ArchiveUserRating[]> = ObservableUtil.asObservable(this._rateBS);

  private _currentResearchElement: AbstractResearchElementMetadataResult;

  @Input()
  set currentResearchElement(value: AbstractResearchElementMetadataResult) {
    this._currentResearchElement = value;
    this.computeProjectThumbnailUrl();
    this.computeDataFileThumbnailUrl();
  }

  get currentResearchElement(): AbstractResearchElementMetadataResult {
    return this._currentResearchElement;
  }

  get rdfFormatEnumTranslate(): KeyValue[] {
    return Enums.Access.RdfFormatEnumTranslate;
  }

  get objectTypeEnumTranslate(): KeyValueLogo[] {
    return Enums.ResearchObject.ObjectTypeEnumTranslate;
  }

  getObjectTypeIcon(key: string): ExtendEnum<IconNamePartialEnum> {
    const icon = Enums.ResearchObject.ObjectTypeEnumTranslate.find(k => k.key === key)?.icon;
    if (isNullOrUndefinedOrWhiteString(icon)) {
      return IconNameEnum.other;
    }
    return icon;
  }

  // @Input()
  // archiveStatistics: ArchiveStatisticsDto;

  // @Input()
  // archivePackages: ArchiveMetadataPackages;

  // @Input()
  // listArchiveUserRating: ArchiveUserRating[];

  @Input()
  relativeResearchObject: AbstractResearchElementMetadataResult[];

  @Input()
  associatedResearchElement: AbstractResearchElementMetadataResult | undefined;

  private _associatedResearchDataFile: ResearchDataFileMetadataResult | undefined;

  @Input()
  set associatedResearchDataFile(value: ResearchDataFileMetadataResult | undefined) {
    this._associatedResearchDataFile = value;
    if (isNotNullNorUndefined(value)) {
      this.computeDataFileThumbnailUrl();
    }
  }

  get associatedResearchDataFile(): ResearchDataFileMetadataResult | undefined {
    return this._associatedResearchDataFile;
  }

  @Input()
  associatedResearchObject: ResearchObjectMetadataResult | undefined;

  get researchDataFile(): ResearchDataFileMetadataResult | undefined {
    if (MetadataUtil.isResearchDataFile(this.currentResearchElement)) {
      return MetadataUtil.getResearchDataFileResult(this.currentResearchElement);
    }
    return this.associatedResearchDataFile;
  }

  get researchObject(): ResearchObjectMetadataResult | undefined {
    if (MetadataUtil.isResearchObject(this.currentResearchElement)) {
      return MetadataUtil.getResearchObjectResult(this.currentResearchElement);
    }
    return this.associatedResearchObject;
  }

  rdfAsWithLink: string;

  private _rdf: string;

  @Input()
  set rdf(value: string) {
    this._rdf = value;
    this.computeLinkInRdfAs();
  }

  get rdf(): string {
    return this._rdf;
  }

  @Input()
  module: ResearchElementModuleEnum;

  @Input()
  isLoadingRdf: boolean;

  @Input()
  isLoadingPrepareDownload: boolean;

  @Input()
  isLoggedIn: boolean;

  @Input()
  isLoadingArchiveRating: boolean;

  listInfo: Info[];

  viewModeList: ViewModeTableEnum = ViewModeTableEnum.tiles;

  projectOverlayComponent: Type<SharedProjectOverlayPresentational> = SharedProjectOverlayPresentational;

  get viewModeListEnum(): typeof ViewModeTableEnum {
    return ViewModeTableEnum;
  }

  get typeInfoEnum(): typeof TypeInfoEnum {
    return TypeInfoEnum;
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  get imageDisplayModeEnum(): typeof ImageDisplayModeEnum {
    return ImageDisplayModeEnum;
  }

  get metadataUtil(): typeof MetadataUtil {
    return MetadataUtil;
  }

  get dateUtil(): typeof DateUtil {
    return DateUtil;
  }

  get projectRoute(): string {
    return `/${RoutesEnum.browseProjectDetail}/${this.currentResearchElement.metadata.researchProject.id}/${BrowseProjectRoutesEnum.metadata}`;
  }

  researchElementRoute(researchElement: AbstractResearchElementMetadataResult): string | undefined {
    const id = researchElement?.resId;
    if (isNullOrUndefinedOrWhiteString(id)) {
      return undefined;
    }
    if (this.module === ResearchElementModuleEnum.home) {
      return `/${RoutesEnum.homeDetail}/${id}`;
    } else if (this.module === ResearchElementModuleEnum.browse) {
      const projectId = researchElement.metadata.researchProject.id;
      let route = undefined;
      if (MetadataUtil.isResearchObject(researchElement)) {
        route = BrowseProjectRoutesEnum.researchObject;
      } else if (MetadataUtil.isResearchDataFile(researchElement)) {
        route = BrowseProjectRoutesEnum.researchDataFile;
      } else {
        return undefined;
      }
      return `/${RoutesEnum.browseProjectDetail}/${projectId}/${route}/${id}`;
    }
  }

  get researchElementMetadataEnumTranslate(): typeof Enums.SearchMetadata.ResearchElementMetadataEnumTranslate {
    return Enums.SearchMetadata.ResearchElementMetadataEnumTranslate;
  }

  get isResearchDataFile(): boolean {
    return MetadataUtil.isResearchDataFile(this.currentResearchElement);
  }

  get isResearchObject(): boolean {
    return MetadataUtil.isResearchObject(this.currentResearchElement);
  }

  get accessibleFromEnum(): typeof Enums.ResearchDataFile.AccessibleFromEnum {
    return Enums.ResearchDataFile.AccessibleFromEnum;
  }

  constructor(private readonly _securityService: SecurityService,
              private readonly _notificationService: NotificationService,
              private readonly _imageFileVisualizerService: ImageFileVisualizerService,
              private readonly _downloadService: DownloadService,
              private readonly _changeDetector: ChangeDetectorRef,
  ) {
    super();

    this.subscribe(this.formControlRdfFormat.valueChanges.pipe(
      distinctUntilChanged(),
      tap((rdfFormat: Enums.Access.RdfFormatEnum) => {
        if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.preferredRdfFormat)) {
          LocalStorageHelper.setItem(LocalStorageEnum.preferredRdfFormat, rdfFormat);
        }
        this._rdfFormatBS.next(rdfFormat);
      }),
    ));
  }

  ngOnInit(): void {
    this._defineListInfo();
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();

    let defaultFormat = Enums.Access.RdfFormatEnum.RDF_XML;
    const preferredFormat = LocalStorageHelper.getItem(LocalStorageEnum.preferredRdfFormat);
    if (isNotNullNorUndefinedNorWhiteString(preferredFormat) && Enums.Access.RdfFormatEnumTranslate.findIndex(r => r.key === preferredFormat) >= 0) {
      defaultFormat = preferredFormat as Enums.Access.RdfFormatEnum;
    }
    this.formControlRdfFormat.setValue(defaultFormat);
  }

  trackByFn(index: number, info: Info): string {
    return index + "";
  }

  private _defineListInfo(): void {
    const researchDataFile = MetadataUtil.getResearchDataFile(this.currentResearchElement);
    const width = researchDataFile?.metadataList?.width;
    const height = researchDataFile?.metadataList?.height;
    this.listInfo = [
      {
        labelToTranslate: LabelTranslateEnum.researchObjectType,
        value: `${this.currentResearchElement.metadata.researchObjectType.rdfType} - ${this.currentResearchElement.metadata.researchObjectType.description}`,
        overlayComponent: SharedResearchObjectTypeOverlayPresentational,
        overlayData: this.currentResearchElement.metadata.researchObjectType,
      },
      {
        labelToTranslate: LabelTranslateEnum.ontologies,
        value: this.currentResearchElement.metadata.researchObjectType.ontology.description,
        link: this.currentResearchElement.metadata.researchObjectType.ontology.url,
        overlayComponent: SharedOntologyOverlayPresentational,
        overlayData: this.currentResearchElement.metadata.researchObjectType.ontology,
      },
      {
        labelToTranslate: LabelTranslateEnum.size,
        value: FileUtil.transformFileSize(MetadataUtil.getSize(this.currentResearchElement)),
      },
      {
        labelToTranslate: LabelTranslateEnum.accessibleFrom,
        value: MetadataUtil.getResearchDataFile(this.currentResearchElement)?.accessibleFrom,
      },
      {
        labelToTranslate: LabelTranslateEnum.mimeType,
        value: MetadataUtil.getResearchDataFile(this.currentResearchElement)?.mimeType,
      },
      {
        labelToTranslate: LabelTranslateEnum.dimension,
        value: width && height ? `${width} x ${height}` : undefined,
      },
      {
        labelToTranslate: LabelTranslateEnum.width,
        value: width ? `${width}px` : undefined,
      },
      {
        labelToTranslate: LabelTranslateEnum.height,
        value: height ? `${height}px` : undefined,
      },
      ...MappingObjectUtil.keys(MetadataUtil.getMetadataWithoutKeyToHide(this.currentResearchElement)).map(key => {
        const value = MappingObjectUtil.get(this.currentResearchElement.metadata.metadataList, key) + "";
        return {
          labelToTranslate: StringUtil.capitalize(key),
          value: value,
          link: this._getUrlIfPresent(value),
        };
      }),
    ];
    this.listInfo = this.listInfo.filter(i => isNonEmptyString(i.value) || isNonEmptyArray(i.values));
  }

  private _getUrlIfPresent(value: string): string | undefined {
    if (value?.startsWith("http://") || value?.startsWith("https://")) {
      return value;
    }
  }

  // askAccess(archive: AbstractResearchElementMetadataResult): void {
  //   this._askAccessBS.next(archive);
  // }

  showDetail(researchElement: AbstractResearchElementMetadataResult): void {
    this._showDetailBS.next(researchElement);
  }

  // navigateToFilesList(): void {
  //   if (this.researchObject.isCollection) {
  //     this._navigateBS.next(new Navigate([RoutesEnum.homeDetail, this.researchObject.resId, HomePageRoutesEnum.collections]));
  //   } else {
  //     this._navigateBS.next(new Navigate([RoutesEnum.homeDetail, this.researchObject.resId, HomePageRoutesEnum.files]));
  //   }
  // }

  // getMetadataVersion(archivePackages: ArchiveMetadataPackages): string {
  //   return archivePackages.metadata[MetadataPackagesEnum.metadataVersion];
  // }
  //
  // getRetentionExpiration(archivePackages: ArchiveMetadataPackages): Date {
  //   const retentionEnd = archivePackages.metadata[MetadataPackagesEnum.aipRetentionEnd];
  //   return DateUtil.convertOffsetDateTimeIso8601ToDate(retentionEnd);
  // }
  //
  // getCreationDate(archivePackages: ArchiveMetadataPackages): Date {
  //   const creation = archivePackages.metadata[MetadataPackagesEnum.creation];
  //   return DateUtil.convertOffsetDateTimeIso8601ToDate(creation);
  // }

  copyIdToClipboard(id: string): void {
    if (ClipboardUtil.copyStringToClipboard(id)) {
      this._notificationService.showInformation(LabelTranslateEnum.idCopyToClipboard);
    }
  }

  copyLinkToClipboard(link: string): void {
    if (ClipboardUtil.copyStringToClipboard(link)) {
      this._notificationService.showInformation(LabelTranslateEnum.copiedLinkToClipboard);
    }
  }

  computeLink(text: string): string {
    if (isNullOrUndefined(text)) {
      return text;
    }
    return text.replace(RegexUtil.urlInComplexText, match => `<a href="${match}" target="_blank" class="no-hover-animation">${match}</a>`);
  }

  // updateRate($event: ArchiveUserRating[]): void {
  //   this._rateBS.next($event);
  // }

  readonly CONTRIBUTOR_SEPARATOR: string = ", ";

  // getContributorOverlayData(contributor: ArchiveContributor): Person {
  //   const fullName = contributor.creatorName;
  //   const indexOfSpace = fullName.indexOf(this.CONTRIBUTOR_SEPARATOR);
  //   const lastName = indexOfSpace === -1 ? fullName : fullName.substring(0, indexOfSpace);
  //   const firstName = indexOfSpace === -1 ? "" : fullName.substring(indexOfSpace + this.CONTRIBUTOR_SEPARATOR.length);
  //   return {
  //     firstName: firstName,
  //     lastName: lastName,
  //     orcid: contributor.orcid,
  //     institutions: contributor.affiliations.map(a => ({
  //       name: a.name,
  //       rorId: isNotNullNorUndefinedNorWhiteString(a.rorIdLink) ? a.rorIdLink.substring(a.rorIdLink.lastIndexOf(SOLIDIFY_CONSTANTS.URL_SEPARATOR) + 1) : undefined,
  //       identifiers: {
  //         [Enums.IdentifiersEnum.ROR]: a.rorIdLink,
  //       } as MappingObject<Enums.IdentifiersEnum, string>,
  //     })),
  //   };
  // }

  private get _isManifestResearchDataFile(): boolean {
    return this.researchDataFile?.metadata.accessibleFrom === AccessibleFromEnum.IIIF_MANIFEST;
  }

  private get _isManifestResearchObject(): boolean {
    return this.researchObject?.metadata.objectType === Enums.ResearchObject.ObjectTypeEnum.MANIFEST;
  }

  get isManifest(): boolean {
    return this._isManifestResearchDataFile || this._isManifestResearchObject;
  }

  computeProjectThumbnailUrl(): void {
    this.projectThumbnailUrl = undefined;
    this.subscribe(ResearchElementHelper.getProjectThumbnailUrl(this._downloadService, this.currentResearchElement)
      .pipe(
        tap(thumbnailProjectUrl => {
          this.projectThumbnailUrl = null;
          this._changeDetector.detectChanges();
          this.projectThumbnailUrl = thumbnailProjectUrl;
          this._changeDetector.detectChanges();
        }),
      ));
  }

  computeDataFileThumbnailUrl(): void {
    this.dataFileThumbnailUrl = undefined;
    this.subscribe(ResearchElementHelper.getDataFileThumbnailUrl(
      this._downloadService, this._imageFileVisualizerService, this.currentResearchElement, this.researchDataFile)
      .pipe(
        tap(thumbnailUrl => {
          this.dataFileThumbnailUrl = null;
          this._changeDetector.detectChanges();
          this.dataFileThumbnailUrl = thumbnailUrl;
          this._changeDetector.detectChanges();
        }),
      ));
  }

  downloadDataFile(researchDataFile: ResearchDataFileMetadataResult): void {
    const webUrl = MetadataUtil.getFileUrl(researchDataFile);
    this._openExternalPage(`${webUrl}/${ApiActionNameEnum.DOWNLOAD}`, `${webUrl}/${ApiActionNameEnum.DOWNLOAD_TOKEN}`);
  }

  getIiifLink(researchDataFile: ResearchDataFileMetadataResult): string {
    return MetadataUtil.getWebUrl(researchDataFile);
  }

  getInfoLink(researchDataFile: ResearchDataFileMetadataResult): string {
    const webUrl = MetadataUtil.getWebUrl(researchDataFile);
    const iiifLinkSplited = webUrl.split("/");
    const iiifBase = iiifLinkSplited.slice(0, iiifLinkSplited.length - 4).join("/");
    return `${iiifBase}/info.json`;
  }

  getManifestLink(researchElement: AbstractResearchElementMetadataResult): string {
    return `${ApiEnum.iiifManifest}/${researchElement.metadata.researchProject.shortName}/${researchElement.metadata.hederaId}`;
  }

  openIiifManifestFile(url: string): void {
    this._downloadIiifResource(url);
  }

  openIiifInfoFile(url: string): void {
    this._downloadIiifResource(url);
  }

  openIiifImage(url: string): void {
    this._downloadIiifResource(url);
  }

  private _downloadIiifResource(url: string): void {
    const downloadTokenUrl = ResearchElementHelper.getIiifDownloadToken(url);
    this._openExternalPage(url, downloadTokenUrl);
  }

  private _openExternalPage(url: string, downloadTokenUrl: string): void {
    const isAccessPublic = this.currentResearchElement?.metadata?.researchProject?.accessPublic;
    this.subscribe(ResearchElementHelper.getDownloadTokenDownloadIfNeeded(this._downloadService, isAccessPublic, downloadTokenUrl)
      .pipe(
        tap(() => SsrUtil.window.open(url, "_blank")),
      ));
  }

  computeLinkInRdfAs(): void {
    if (isNullOrUndefinedOrWhiteString(this._rdf)) {
      this.rdfAsWithLink = undefined;
      return;
    }
    this.rdfAsWithLink = this._rdf;
    this.rdfAsWithLink = StringUtil.replaceAll(this.rdfAsWithLink, "<", "&lt;");
    this.rdfAsWithLink = this.rdfAsWithLink.replace(RegexUtil.urlInComplexText, match => `<a class="no-hover-animation">${match}</a>`);
  }

  copyRdf(): void {
    if (ClipboardUtil.copyStringToClipboard(this.rdf)) {
      this._notificationService.showInformation(LabelTranslateEnum.rdfCopiedToClipboard);
    }
  }

  rdfClickEvent($event: MouseEvent, link?: string): void {
    const target = $event.target;
    if (target["nodeName"] !== "A") {
      return;
    }
    const url = isNotNullNorUndefinedNorWhiteString(link) ? link : target["textContent"];
    this._rdfNavigationBS.next(url);
  }
}

class Info {
  labelToTranslate: string;
  value?: string;
  values?: string[];
  code?: boolean = false;
  type?: TypeInfoEnum;
  link?: string;
  overlayComponent?: Type<SharedResourceOverlayPresentational<any, any>>;
  overlayData?: BaseResource;
}

enum TypeInfoEnum {
  translateLabel = "translateLabel",
}
