/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-access-level-with-embargo.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  Input,
} from "@angular/core";
import {Enums} from "@enums";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {DateUtil} from "solidify-frontend";

@Component({
  selector: "hedera-shared-access-level-with-embargo",
  templateUrl: "./shared-access-level-with-embargo.presentational.html",
  styleUrls: ["./shared-access-level-with-embargo.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedAccessLevelWithEmbargoPresentational extends SharedAbstractPresentational {
  @Input()
  accessLevel: Enums.Access.AccessEnum;

  @Input()
  @HostBinding("class.have-embargo")
  embargoAccessLevel: Enums.Access.AccessEnum;

  @Input()
  embargoEndDate: Date;

  @Input()
  size: string = "size-24";

  @Input()
  withTooltip: boolean = false;

  get dateUtil(): typeof DateUtil {
    return DateUtil;
  }

  constructor(private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }
}
