/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-research-element-detail.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from "@angular/core";
import {Meta} from "@angular/platform-browser";
import {
  ActivatedRoute,
  Router,
} from "@angular/router";
import {
  LIST_RESEARCH_OBJECT_META_NAME,
  metaInfoResearchElement,
} from "@app/meta-info-list";
import {AppState} from "@app/stores/app.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {HomeHelper} from "@home/helpers/home.helper";
import {MetadataUtil} from "@home/helpers/metadata.util";
import {PortalSearch} from "@home/models/portal-search.model";
import {
  AbstractResearchElementMetadataResult,
  ResearchDataFileMetadataResult,
  ResearchObjectMetadataResult,
  SystemProperty,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {ResearchElementModuleEnum} from "@shared/enums/research-element-module.enum";
import {
  BrowseProjectRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {HomeAction} from "@shared/stores/home/home.action";
import {HomeState} from "@shared/stores/home/home.state";
import {RouteUtil} from "@shared/utils/route.util";
import {
  Observable,
  pipe,
} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  take,
  tap,
} from "rxjs/operators";
import {
  AppSystemPropertyState,
  isNotNullNorUndefined,
  MemoizedUtil,
  MetaService,
  MetaUtil,
  SsrUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-shared-research-element-detail-routable",
  templateUrl: "./shared-research-element-detail.routable.html",
  styleUrls: ["./shared-research-element-detail.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedResearchElementDetailRoutable extends SharedAbstractRoutable implements OnInit, OnDestroy {
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, HomeState);
  currentObs: Observable<AbstractResearchElementMetadataResult> = MemoizedUtil.select(this._store, HomeState, state => state.current);
  isLoggedInObs: Observable<boolean> = MemoizedUtil.select(this._store, AppState, state => state.isLoggedIn);
  rdfAsObs: Observable<string> = MemoizedUtil.select(this._store, HomeState, state => state.rdf);
  isLoadingRdfAsObs: Observable<boolean> = MemoizedUtil.select(this._store, HomeState, state => state.isLoadingRdf > 0);
  associatedResearchElementObs: Observable<AbstractResearchElementMetadataResult> = MemoizedUtil.select(this._store, HomeState, state => state.associatedResearchElement);
  associatedResearchDataFileObs: Observable<ResearchDataFileMetadataResult> = MemoizedUtil.select(this._store, HomeState, state => state.associatedResearchDataFile);
  associatedResearchObjectObs: Observable<ResearchObjectMetadataResult> = MemoizedUtil.select(this._store, HomeState, state => state.associatedResearchObject);

  listResearchObjectFromProjectObs: Observable<AbstractResearchElementMetadataResult[]> = MemoizedUtil.select(this._store, HomeState, state => state.listRelativeResearchObject);

  systemPropertyObs: Observable<SystemProperty> = MemoizedUtil.select(this._store, AppSystemPropertyState, state => state.current);

  _resId: string;

  module: ResearchElementModuleEnum;

  get researchElementModuleEnum(): typeof ResearchElementModuleEnum {
    return ResearchElementModuleEnum;
  }

  get browseParentRoute(): string[] {
    const routes = RouteUtil.generateFullUrlFromActivatedRouteNormal(this._activatedRoute);
    return routes.slice(0, routes.length - 1);
  }

  constructor(private readonly _store: Store,
              private readonly _metaService: MetaService,
              private readonly _meta: Meta,
              private readonly _translateService: TranslateService,
              private readonly _router: Router,
              private readonly _activatedRoute: ActivatedRoute,
  ) {
    super();
    this._defineModule();
  }

  ngOnInit(): void {
    super.ngOnInit();

    if (SsrUtil.isBrowser) {
      this.subscribe(this._translateService.onLangChange.pipe(
        tap(() => {
          const researchObject = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.current);
          this._metaService.setMetaFromInfo(metaInfoResearchElement, researchObject);
        }),
      ));
    }

    this.subscribe(this.currentObs.pipe(
      filter(researchElement => isNotNullNorUndefined(researchElement) && researchElement.resId !== this._resId),
      distinctUntilChanged((previous, current) => previous?.resId === current?.resId),
      SsrUtil.isServer ? take(1) : pipe(),
      tap(researchElement => {
        this._resId = researchElement.resId;
        this._metaService.setMetaFromInfo(metaInfoResearchElement, researchElement);
      }),
    ));
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  private _clean(): void {
    this._store.dispatch(new HomeAction.CleanCurrent());
    MetaUtil.cleanListMetaByName(this._meta, LIST_RESEARCH_OBJECT_META_NAME);
  }

  private _defineModule(): void {
    const url = this._router.url;
    this.module = url.startsWith(`/${RoutesEnum.homeDetail}`) ? ResearchElementModuleEnum.home : ResearchElementModuleEnum.browse;
  }

  back(): void {
    if (this.module === ResearchElementModuleEnum.home) {
      const search = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.search);
      const viewMode = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.viewModeTableEnum);
      const facetsSelected = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.facetsSelected);
      const searchModeIsQuery = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.searchModeIsQuery);

      const portalSearch = {
        searchText: search,
        searchModeIsQuery: searchModeIsQuery,
        facetsSelected: facetsSelected,
        viewModeTable: viewMode,
      } as PortalSearch;

      HomeHelper.navigateToSearch(this._store, portalSearch);
    } else if (this.module === ResearchElementModuleEnum.browse) {
      this._store.dispatch(new Navigate(this.browseParentRoute));
    }
  }

  showDetail(researchElementMetadataResult: AbstractResearchElementMetadataResult): void {
    let route = undefined;
    const id = researchElementMetadataResult.resId;
    if (this.module === ResearchElementModuleEnum.home) {
      route = [RoutesEnum.homeDetail, id];
    } else if (this.module === ResearchElementModuleEnum.browse) {
      const projectId = researchElementMetadataResult.metadata.researchProject.id;
      if (MetadataUtil.isResearchObject(researchElementMetadataResult)) {
        route = BrowseProjectRoutesEnum.researchObject;
      } else if (MetadataUtil.isResearchDataFile(researchElementMetadataResult)) {
        route = BrowseProjectRoutesEnum.researchDataFile;
      } else {
        return undefined;
      }
      route = [RoutesEnum.browseProjectDetail, projectId, route, id];
    }
    this._clean();
    this._store.dispatch(new Navigate(route));
  }

  navigate(navigate: Navigate): void {
    this._store.dispatch(navigate);
  }

  rdfFormatChange(rdfFormat: Enums.Access.RdfFormatEnum): void {
    const current = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.current);
    this._store.dispatch(new HomeAction.GetRdf(current.metadata.uri, rdfFormat));
  }

  rdfNavigation(url: string): void {
    if (url.startsWith(environment.linkedData)) {
      const researchElementMetadataResult = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.current);
      const projectId = researchElementMetadataResult.metadata.researchProject.id;
      this._store.dispatch(new HomeAction.FollowRedirect(url, this.module, projectId));
      return;
    }
    SsrUtil.window.open(url, "_blank");
  }
}
