/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-research-object-not-found.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
} from "@angular/core";
import {
  ActivatedRoute,
  NavigationEnd,
  Router,
} from "@angular/router";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {ResearchElementModuleEnum} from "@shared/enums/research-element-module.enum";
import {
  AppRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {RouteUtil} from "@shared/utils/route.util";
import {
  distinctUntilChanged,
  filter,
  tap,
} from "rxjs/operators";

@Component({
  selector: "hedera-shared-research-object-not-found-routable",
  templateUrl: "./shared-research-object-not-found.routable.html",
  styleUrls: ["./shared-research-object-not-found.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedResearchObjectNotFoundRoutable extends SharedAbstractRoutable implements OnInit {
  researchObjectId: string;
  module: ResearchElementModuleEnum;

  get researchElementModuleEnum(): typeof ResearchElementModuleEnum {
    return ResearchElementModuleEnum;
  }

  constructor(private readonly _store: Store,
              private readonly _route: ActivatedRoute,
              private readonly _router: Router) {
    super();
    this._defineModule();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.retrieveAttribute();

    this.subscribe(this._router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        distinctUntilChanged(),
        tap(event => {
          this.retrieveAttribute();
        }),
      ),
    );
  }

  retrieveAttribute(): void {
    this.researchObjectId = this._route.snapshot.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
  }

  back(): void {
    const path = RouteUtil.generateFullUrlFromActivatedRouteNormal(this._route);
    if (this.module === ResearchElementModuleEnum.home) {
      this._navigate([RoutesEnum.homeSearch]);
    } else if (this.module === ResearchElementModuleEnum.browse) {
      this._navigate([...path.slice(0, path.length - 2)]);
    }
  }

  private _defineModule(): void {
    const url = this._router.url;
    this.module = url.startsWith(`/${RoutesEnum.homeDetail}`) ? ResearchElementModuleEnum.home : ResearchElementModuleEnum.browse;
  }

  private _navigate(path: (string | number)[]): void {
    this._store.dispatch(new Navigate(path));
  }
}
