/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-abstract-data-file-detail.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectorRef,
  Directive,
  Inject,
  OnInit,
  Type,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {IngestService} from "@app/features/ingest/services/ingest.service";
import {
  AbstractResearchElementMetadata,
  DataFile,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  DataFileProperties,
  SharedDatafileData,
  SharedDataFileInformationContainer,
  SharedDataFileInformationEnum,
} from "@shared/components/containers/shared-data-file-information/shared-data-file-information.container";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {LocalStateModel} from "@shared/models/local-state.model";
import {StoreDialogService} from "@shared/services/store-dialog.service";
import {Observable} from "rxjs";
import {
  filter,
  tap,
} from "rxjs/operators";
import {
  BaseAction,
  CompositionNameSpace,
  DeleteDialog,
  DialogUtil,
  ExtraButtonToolbar,
  FileVisualizerService,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isTrue,
  LABEL_TRANSLATE,
  LabelTranslateInterface,
  MemoizedUtil,
  MemoizedUtilStateType,
  NotificationService,
  ResourceFileNameSpace,
  SOLIDIFY_CONSTANTS,
  StatusHistoryNamespace,
  StatusHistoryState,
  StatusHistoryStateModel,
  StatusModel,
  StoreUtil,
} from "solidify-frontend";

@Directive()
export abstract class SharedAbstractDataFileDetailRoutable<TResource extends AbstractResearchElementMetadata> extends SharedAbstractRoutable implements OnInit {
  currentObs: Observable<TResource>;
  current: TResource;
  _resId: string;
  protected _parentId: string | undefined;

  protected _projectId: string | undefined;

  @ViewChild("sharedDataFileInformationContainer")
  readonly sharedDataFileInformationContainer: SharedDataFileInformationContainer<TResource>;

  data: SharedDatafileData<TResource>;
  mode: SharedDataFileInformationEnum;

  protected _listButtonMerged?: ExtraButtonToolbar<TResource>[];

  listOriginalButton: ExtraButtonToolbar<TResource>[] = [
    {
      labelToTranslate: (current) => LabelTranslateEnum.download,
      color: "primary",
      icon: IconNameEnum.download,
      callback: current => this.downloadDataFile(),
      order: 52,
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _storeDialogService: StoreDialogService,
              protected readonly _fileVisualizerService: FileVisualizerService,
              protected readonly _changeDetector: ChangeDetectorRef,
              @Inject(LABEL_TRANSLATE) protected readonly _labelTranslateInterface: LabelTranslateInterface,
              protected readonly _notificationService: NotificationService,
              protected readonly _datasetService: IngestService,
              protected readonly _state: MemoizedUtilStateType<any, TResource, any>,
              protected readonly _stateAction: CompositionNameSpace | ResourceFileNameSpace,
              protected readonly _statusHistoryState: Type<StatusHistoryState<StatusHistoryStateModel<TResource>, TResource>>,
              protected readonly _statusHistoryNamespace: StatusHistoryNamespace,
              protected readonly _statusHistoryEnum: StatusModel[]) {
    super();
    this._datasetService.hideDepositHeader.next(true);
    this.currentObs = MemoizedUtil.current(this._store, this._state);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._retrieveResIdFromUrl();
    this._getOrRefreshResearchDataFile();
    this._mergeExtraButtonsWithOriginalButtons();
    this.subscribe(this.currentObs.pipe(
      filter(resource => isNotNullNorUndefined(resource)),
      tap(resource => {
        this.current = resource;
        this.subscribe(this._observableCreateData());
      })));
  }

  protected _retrieveResIdFromUrl(): void {
    this._resId = this._route.snapshot.paramMap.get("idFile");
    if (isNullOrUndefined(this._resId)) {
      this._resId = this._route.parent.snapshot.paramMap.get(SOLIDIFY_CONSTANTS.ID);
    }
    this._parentId = this._route.snapshot.paramMap.get("id");
    if (isNullOrUndefined(this._resId)) {
      this._resId = this._route.parent.snapshot.paramMap.get(SOLIDIFY_CONSTANTS.ID);
    }
  }

  protected _getOrRefreshResearchDataFile(): void {
    if (this.mode === SharedDataFileInformationEnum.parentId) {
      this._store.dispatch(new this._stateAction.GetById(this._parentId, this._resId));
    } else if (this.mode === SharedDataFileInformationEnum.projectId) {
      this._store.dispatch(new this._stateAction.GetById(this._projectId, this._resId));
    } else {
      this._store.dispatch(new this._stateAction.GetById(this._resId));
    }
  }

  protected _observableCreateData(): Observable<TResource> {
    return this.currentObs.pipe(
      filter(resource => isNotNullNorUndefined(resource)),
      tap(resource => {
        this.data = {
          parentId: this._parentId,
          projectId: this._projectId,
          current: this.current,
          buttons: this._listButtonMerged,
          statusHistoryState: this._statusHistoryState,
          statusHistoryNamespace: this._statusHistoryNamespace,
          statusHistoryEnum: this._statusHistoryEnum,
          properties: this.defineProperties(),
          dataFile: this.defineDataFile(),
          mode: this.mode,
        } as SharedDatafileData<TResource>;
        this._changeDetector.detectChanges();
      }),
    );
  }

  private _mergeExtraButtonsWithOriginalButtons(): void {
    const originalButtons = isNotNullNorUndefined(this.listOriginalButton) ? this.listOriginalButton : [];
    const extraButtons = isNotNullNorUndefined(this.defineButtons()) ? this.defineButtons() : [];
    this._listButtonMerged = [...originalButtons, ...extraButtons];
    this._listButtonMerged.sort((a, b) => {
      const orderA = a.order || 0;
      const orderB = b.order || 0;
      if (orderA === orderB) {
        return 0;
      } else if (orderA < orderB) {
        return -1;
      }
      return 1;
    });
  }

  abstract defineProperties(): DataFileProperties<TResource>[];

  abstract defineDataFile(): DataFile<TResource>;

  abstract defineButtons(): ExtraButtonToolbar<TResource>[];

  back(): void {
    this._store.dispatch(this.backNavigate());
  }

  backNavigate(): Navigate {
    const currentUrl = this._store.selectSnapshot((s: LocalStateModel) => s.router.state.url);
    const previousUrl = currentUrl.substring(0, currentUrl.lastIndexOf(urlSeparator));
    return new Navigate([previousUrl]);
  }

  delete(): void {
    const stateEnum = StoreUtil.getStateNameFromClass(this._state);
    const deleteData = this._storeDialogService.deleteData(stateEnum);
    deleteData.name = this.data.dataFile.fileName;
    this.subscribe(DialogUtil.open(this._dialog, DeleteDialog, deleteData,
      {
        width: "400px",
      },
      isConfirmed => {
        if (isTrue(isConfirmed)) {
          this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
            this.getAction(),
            this._stateAction.DeleteSuccess,
            resultAction => {
              this.sharedDataFileInformationContainer.back();
            }));
        }
      }));
  }

  getAction(): BaseAction {
    if (this.mode === SharedDataFileInformationEnum.parentId) {
      return new this._stateAction.Delete(this.data.parentId, this._resId);
    }
    if (this.mode === SharedDataFileInformationEnum.projectId) {
      return new this._stateAction.Delete(this.data.projectId, this.current.resId);
    } else {
      return new this._stateAction.Delete(this.current.resId);
    }

  }

  downloadDataFile(): void {
    if (this.mode === SharedDataFileInformationEnum.parentId) {
      this._store.dispatch(new this._stateAction["Download"](this.data.parentId, this.current));
    } else {
      this._store.dispatch(new this._stateAction["Download"](this.data.projectId, this.current));
    }
  }

}
