/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-resource-role-member-container.component.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from "@angular/core";
import {Enums} from "@enums";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {BaseResourceRole} from "@shared/models/base-resource-role.model";
import {SecurityService} from "@shared/services/security.service";
import {
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
} from "solidify-frontend";

@Component({
  selector: "hedera-shared-resource-role-member-container",
  templateUrl: "./shared-resource-role-member-container.component.html",
  styleUrls: ["./shared-resource-role-member-container.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class SharedResourceRoleMemberContainer<TResource extends BaseResourceRole> extends SharedAbstractPresentational implements OnInit {

  iconMember: IconNameEnum | undefined = undefined;
  roleName: string | undefined = undefined;

  @Input()
  resource: TResource;

  @Input()
  mode: SharedResourceRoleMemberContainerMode;

  tooltip: string;

  ngOnInit(): void {
    super.ngOnInit();

    this._computeLogoAndTooltip();
  }

  private _computeLogoAndTooltip(): void {
    if (isNullOrUndefined(this.resource) || isNullOrUndefined(this.mode)) {
      return;
    }
    let role;
    if (this.mode === SharedResourceRoleMemberContainerMode.project) {
      role = this._securityService.getRoleInProject(this.resource.resId);
      this.tooltip = MARK_AS_TRANSLATABLE("resourceRoleMember.memberOfProject");
    }
    this.roleName = role?.name;
    switch (role?.resId) {
      case Enums.Role.RoleEnum.MANAGER:
        this.iconMember = IconNameEnum.roleManager;
        break;
      case Enums.Role.RoleEnum.CREATOR:
        this.iconMember = IconNameEnum.roleCreator;
        break;
      case Enums.Role.RoleEnum.VISITOR:
        this.iconMember = IconNameEnum.roleVisitor;
        break;
      default:
        this.iconMember = undefined;
        break;
    }
  }

  constructor(public readonly _securityService: SecurityService) {
    super();
  }
}

export type SharedResourceRoleMemberContainerMode =
  "project";
export const SharedResourceRoleMemberContainerMode = {
  project: "project" as SharedResourceRoleMemberContainerMode,
};
