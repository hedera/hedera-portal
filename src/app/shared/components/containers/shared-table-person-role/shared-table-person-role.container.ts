/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-table-person-role.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  OnInit,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormBuilder,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {getMostImportantRole} from "@shared/shared.const";
import {sharedPersonActionNameSpace} from "@shared/stores/person/shared-person.action";
import {SharedPersonState} from "@shared/stores/person/shared-person.state";
import {
  AbstractTableResourceRoleContainer,
  OrderEnum,
  ResourceNameSpace,
  Sort,
} from "solidify-frontend";

@Component({
  selector: "hedera-shared-table-person-role-container",
  templateUrl: "../../../../../../node_modules/solidify-frontend/lib/application/components/containers/abstract-table-person-resource-role/abstract-table-resource-role.container.html",
  styleUrls: ["../../../../../../node_modules/solidify-frontend/lib/application/components/containers/abstract-table-person-resource-role/abstract-table-resource-role.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: SharedTablePersonRoleContainer,
    },
  ],
})
export class SharedTablePersonRoleContainer extends AbstractTableResourceRoleContainer implements ControlValueAccessor, OnInit {
  resourceLabelKey: string = "fullName";

  resourceLabelToTranslate: string = LabelTranslateEnum.person;

  resourceSort: Sort = {
    field: "lastName",
    order: OrderEnum.ascending,
  };

  resourceActionNameSpace: ResourceNameSpace = sharedPersonActionNameSpace;

  resourceState: typeof SharedPersonState | any = SharedPersonState;

  resourceTooltipNavigateToTranslate: string = LabelTranslateEnum.seePersonDetail;

  messageNoItem: string = LabelTranslateEnum.noPersonSelected;

  elementsToAddLabelToTranslate: string = LabelTranslateEnum.peopleToAdd;

  detailPath: string = RoutesEnum.adminPersonDetail;

  constructor(protected readonly _injector: Injector,
              protected readonly _translate: TranslateService,
              protected readonly _store: Store,
              protected readonly _fb: FormBuilder,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef) {
    super(_injector,
      _translate,
      _store,
      _fb,
      _changeDetector,
      _elementRef,
      environment);
  }

  protected _doGetMostImportantRole(listRoles: any[]): Enums.Role.RoleEnum {
    return getMostImportantRole(listRoles);
  }
}
