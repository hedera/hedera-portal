/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-data-file-information.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnInit,
  Output,
  Type,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {environment} from "@environments/environment";
import {AbstractResearchElementMetadata} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractContainer} from "@shared/components/containers/shared-abstract/shared-abstract.container";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {DataFileHelper} from "@shared/helpers/data-file.helper";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  BaseResource,
  BreakpointService,
  DialogUtil,
  ExtendEnum,
  ExtraButtonToolbar,
  FileInput,
  FileVisualizerHelper,
  FileVisualizerService,
  IconNamePartialEnum,
  isFalse,
  isNullOrUndefined,
  LABEL_TRANSLATE,
  LabelTranslateInterface,
  MemoizedUtil,
  NotificationService,
  ObservableUtil,
  StatusHistoryDialog,
  StatusHistoryNamespace,
  StatusHistoryState,
  StatusHistoryStateModel,
  StatusModel,
} from "solidify-frontend";

@Component({
  selector: "hedera-shared-data-file-information-container",
  templateUrl: "./shared-data-file-information.container.html",
  styleUrls: ["./shared-data-file-information.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedDataFileInformationContainer<TResource extends AbstractResearchElementMetadata> extends SharedAbstractContainer implements OnInit {
  previewAvailable: boolean;
  visualizationErrorMessage: string;
  isInFullscreenMode: boolean = false;
  zoomEnable: boolean = false;

  private readonly _fullScreenBS: BehaviorSubject<boolean | undefined> = new BehaviorSubject<boolean | undefined>(undefined);
  readonly fullScreenObs: Observable<boolean | undefined> = ObservableUtil.asObservable(this._fullScreenBS);

  private readonly _enterFullScreenBS: BehaviorSubject<boolean | undefined> = new BehaviorSubject<boolean | undefined>(undefined);
  @Output("enterFullScreen")
  readonly enterFullScreenBS: Observable<boolean | undefined> = ObservableUtil.asObservable(this._enterFullScreenBS);

  private readonly _deleteBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("deleteChange")
  readonly deleteObs: Observable<void> = ObservableUtil.asObservable(this._deleteBS);

  _data: SharedDatafileData<TResource>;

  @Input()
  set data(value: SharedDatafileData<TResource>) {
    this._data = value;
    this._computePreviewAvailable();
  }

  get data(): SharedDatafileData<TResource> {
    return this._data;
  }

  @Input()
  canDelete: boolean = false;

  @Input()
  canPreview: boolean = true;

  @Input()
  fileUrl: string;

  @Input()
  backNavigate: Navigate;

  smallButtons: ExtraButtonToolbar<TResource>[] = [
    {
      displayCondition: () => this.previewAvailable && this.isInFullscreenMode,
      icon: IconNameEnum.fullScreenLeave,
      callback: () => this.leaveFullScreen(),
      labelToTranslate: () => LabelTranslateEnum.exitFullscreen,
      color: "primary",
    },
  ];

  get dataFilePropertiesTypeEnum(): typeof DataFilePropertiesTypeEnum {
    return DataFilePropertiesTypeEnum;
  }

  constructor(protected readonly _store: Store,
              protected readonly _dialog: MatDialog,
              protected readonly _actions$: Actions,
              protected readonly _breakpointService: BreakpointService,
              protected readonly _notificationService: NotificationService,
              protected readonly _changeDetector: ChangeDetectorRef,
              private readonly _fileVisualizerService: FileVisualizerService,
              @Inject(LABEL_TRANSLATE) protected readonly _labelTranslateInterface: LabelTranslateInterface,
  ) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._computePreviewAvailable();
  }

  back(): void {
    this._store.dispatch(this.backNavigate);
  }

  delete(): void {
    this._deleteBS.next();
  }

  enterFullScreen(): void {
    this.isInFullscreenMode = true;
    this.zoomEnable = true;
    this._enterFullScreenBS.next(true);
    this._changeDetector.detectChanges();
  }

  leaveFullScreen(): void {
    this.isInFullscreenMode = false;
    this.zoomEnable = false;
    this._enterFullScreenBS.next(false);
    this._changeDetector.detectChanges();
  }

  changeFullScreen($event: boolean): void {
    this._fullScreenBS.next($event);
  }

  showHistory(): void {
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: isNullOrUndefined(this.data.parentId) ? this.data.projectId : this.data.parentId,
      resourceResId: this.data.current.resId,
      name: this.data.dataFile.fileName,
      statusHistory: MemoizedUtil.select(this._store, this.data.statusHistoryState, state => state.history),
      isLoading: MemoizedUtil.isLoading(this._store, this.data.statusHistoryState),
      queryParametersObs: MemoizedUtil.select(this._store, this.data.statusHistoryState, state => state.queryParameters),
      state: this.data.statusHistoryNamespace,
      statusEnums: this.data.statusHistoryEnum,
    }, {
      width: environment.modalWidth,
    });
  }

  private _computePreviewAvailable(): void {
    if (isNullOrUndefined(this.data)) {
      return;
    }
    if (isFalse(this.canPreview)) {
      this.previewAvailable = false;
      return;
    }
    if (this.previewAvailable) {
      return;
    }

    this.previewAvailable = FileVisualizerHelper.canHandle(this._fileVisualizerService.listFileVisualizer, {
      dataFile: DataFileHelper.dataFileAdapterWithStatusReady(this.data?.dataFile as any),
      fileExtension: FileVisualizerHelper.getFileExtension(this.data?.dataFile.fileName),
    });
    if (!this.previewAvailable) {
      this.visualizationErrorMessage = FileVisualizerHelper.errorMessage(this._fileVisualizerService.listFileVisualizer, {
        dataFile: DataFileHelper.dataFileAdapterWithStatusReady(this.data?.dataFile as any),
        fileExtension: FileVisualizerHelper.getFileExtension(this.data?.dataFile.fileName),
      }, this._labelTranslateInterface);
    }
  }

  getFileInput(): FileInput {
    return {
      dataFile: DataFileHelper.dataFileAdapterWithStatusReady(this.data.current as any),
    };
  }

  get dataProperties(): DataFileProperties<TResource>[] {
    return this.data?.properties;
  }

  fileDownloadUrl(): string {
    return this.fileUrl;
  }
}

export interface SharedDatafileData<TResource extends AbstractResearchElementMetadata> {
  parentId?: string;
  projectId: string;
  current: TResource;
  buttons: ExtraButtonToolbar<TResource>[];
  statusHistoryState?: Type<StatusHistoryState<StatusHistoryStateModel<TResource>, TResource>>;
  statusHistoryNamespace?: StatusHistoryNamespace;
  statusHistoryEnum?: StatusModel[];
  properties: DataFileProperties<TResource>[];
  dataFile: DataFile<TResource>;
}

export interface DataFileProperties<TResource extends BaseResource> {
  key: string;
  value: string | number;
  callback?: (current: TResource) => void;
  logo?: ExtendEnum<IconNamePartialEnum>;
  type?: DataFilePropertiesTypeEnum;
}

export interface DataFile<TResource extends BaseResource> {
  fileName?: string;
  fileSize?: number;
  mimeType?: string;
  status?: string;
}

export enum SharedDataFileInformationEnum {
  parentId,
  projectId,
  resId
}

export enum DataFilePropertiesTypeEnum {
  text = "text",
  status = "status",
  size = "size",
}
