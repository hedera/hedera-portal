/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-userguide-sidebar.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  OnInit,
  Renderer2,
} from "@angular/core";
import {appActionNameSpace} from "@app/stores/app.action";
import {AppState} from "@app/stores/app.state";
import {AppTocState} from "@app/stores/toc/app-toc.state";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractContainer} from "@shared/components/containers/shared-abstract/shared-abstract.container";
import {TocHelper} from "@shared/helpers/toc.helper";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  take,
  tap,
} from "rxjs/operators";
import {
  isNotNullNorUndefined,
  MemoizedUtil,
  ofSolidifyActionCompleted,
  SsrUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-shared-userguide-sidebar-container",
  templateUrl: "./shared-userguide-sidebar.container.html",
  styleUrls: ["./shared-userguide-sidebar.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger("sidebarAnimation", [
      state("void", style({opacity: "0" /*right: "-400px"*/})),
      transition(":enter", animate("300ms ease")),
      transition(":leave", animate("300ms ease")),
    ]),
    trigger("backdropAnimation", [
      state("void", style({opacity: "0"})),
      transition(":enter", animate("300ms ease")),
      transition(":leave", animate("300ms ease")),
    ]),
  ],
})
export class SharedUserguideSidebarContainer extends SharedAbstractContainer implements OnInit {
  userDocumentationObs: Observable<string> = MemoizedUtil.select(this._store, AppTocState, s => s.userDocumentation);
  isOpenedUserGuideSidebarObs: Observable<boolean> = MemoizedUtil.select(this._store, AppState, s => s.isOpenedSidebarUserGuide);

  @HostBinding("class.hide")
  hide: boolean = true;

  constructor(private readonly _store: Store,
              private readonly _renderer: Renderer2,
              protected readonly _actions$: Actions) {
    super();
  }

  ngOnInit(): void {
    this.subscribe(this.userDocumentationObs.pipe(
        distinctUntilChanged(),
        filter(doc => isNotNullNorUndefined(doc)),
        take(1),
      ),
      () => this.hide = false);

    this.subscribe(this._actions$.pipe(
      ofSolidifyActionCompleted(appActionNameSpace.ChangeDisplaySidebarUserGuide),
      filter(action => action.action.isOpen),
      tap(action => this._updateLinkUserGuide()),
    ));
  }

  private _updateLinkUserGuide(): void {
    setTimeout(() => {
      TocHelper.updateLinkToc(SsrUtil.window?.document, this._renderer, "user-guide-sidebar", true);
    }, 0);
  }

  closeUserGuideSidebar(): void {
    this._store.dispatch(new appActionNameSpace.ChangeDisplaySidebarUserGuide(false));
  }
}
