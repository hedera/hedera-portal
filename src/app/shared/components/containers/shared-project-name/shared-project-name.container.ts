/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-project-name.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from "@angular/core";
import {environment} from "@environments/environment";
import {Project} from "@models";
import {Store} from "@ngxs/store";
import {SharedAbstractContainer} from "@shared/components/containers/shared-abstract/shared-abstract.container";
import {SharedProjectOverlayPresentational} from "@app/shared/components/presentationals/shared-project-overlay/shared-project-overlay.presentational";
import {SharedProjectAction} from "@app/shared/stores/project/shared-project.action";
import {SharedProjectState} from "@app/shared/stores/project/shared-project.state";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  take,
} from "rxjs/operators";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
  MemoizedUtil,
  StringUtil,
  Type,
} from "solidify-frontend";

@Component({
  selector: "hedera-shared-project-name-container",
  templateUrl: "./shared-project-name.container.html",
  styleUrls: ["./shared-project-name.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedProjectNameContainer extends SharedAbstractContainer {
  readonly TIME_BEFORE_DISPLAY_TOOLTIP: number = environment.timeBeforeDisplayTooltip;

  projectOverlayComponent: Type<SharedProjectOverlayPresentational> = SharedProjectOverlayPresentational;

  project: Project | undefined;

  get name(): string {
    if (isNotNullNorUndefined(this.project)) {
      return this.project.name;
    }
    return StringUtil.stringEmpty;
  }

  listProjectObs: Observable<Project[]> = MemoizedUtil.list(this._store, SharedProjectState);
  isLoading: boolean = false;

  _id: string;

  @Input()
  set id(id: string) {
    this._id = id;
    if (isNullOrUndefined(id)) {
      return;
    }
    const listSnapshot = MemoizedUtil.listSnapshot(this._store, SharedProjectState);
    this.project = this._getProject(listSnapshot);
    if (isNullOrUndefined(this.project)) {
      this.subscribe(this.listProjectObs.pipe(
        distinctUntilChanged(),
        filter(list => isNotNullNorUndefined(this._getProject(list))),
        take(1),
      ), list => {
        this.project = this._getProject(list);
        this.isLoading = false;
        this._changeDetector.detectChanges();
      });
      this._store.dispatch(new SharedProjectAction.AddInListById(id, true));
      this.isLoading = true;
    }
  }

  get id(): string {
    return this._id;
  }

  @Input()
  inline: boolean = false;

  @Input()
  displayOverlay: boolean = false;

  constructor(public readonly _store: Store,
              private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  private _getProject(list: Project[]): Project {
    if (isNullOrUndefined(list)) {
      return undefined;
    }
    const project = list.find(o => o.resId === this._id);
    if (isNullOrUndefined(project)) {
      return undefined;
    }
    return project;
  }
}
