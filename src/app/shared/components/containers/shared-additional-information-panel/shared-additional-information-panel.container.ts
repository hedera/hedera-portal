/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-additional-information-panel.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  Type,
} from "@angular/core";
import {User} from "@models";
import {SharedAbstractContainer} from "@shared/components/containers/shared-abstract/shared-abstract.container";
import {sharedUserActionNameSpace} from "@shared/stores/user/shared-user.action";
import {SharedUserState} from "@shared/stores/user/shared-user.state";
import {
  AdditionalInformationPanelMode,
  BaseResourceType,
  ResourceNameSpace,
  ResourceState,
} from "solidify-frontend";

@Component({
  selector: "hedera-shared-additional-information-panel-container",
  templateUrl: "./shared-additional-information-panel.container.html",
  styleUrls: ["./shared-additional-information-panel.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedAdditionalInformationPanelContainer extends SharedAbstractContainer implements OnInit {
  state: Type<ResourceState<any, User>> = SharedUserState;
  resourceNameSpace: ResourceNameSpace = sharedUserActionNameSpace;
  correspondingWhoChangeInfoAttributeKey: string = "externalUid";
  actionNameGetUserFromWhoId: string = "GetByExternalUid";

  @Input()
  resource: BaseResourceType;

  @Input()
  mode: AdditionalInformationPanelMode = "standalone";
}
