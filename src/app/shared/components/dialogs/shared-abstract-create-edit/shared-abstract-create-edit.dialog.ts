/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - shared-abstract-create-edit.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  ChangeDetectorRef,
  Directive,
  Inject,
  Injector,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {ActionCompletion} from "@ngxs/store/src/operators/of-action";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {CreateEditDialog} from "@shared/models/detail-edit-dialog.model";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {
  AbstractCrudRoutable,
  AbstractDialogInterface,
  AbstractFormPresentational,
  BaseResourceType,
  CrudHelper,
  FormControlKey,
  isNullOrUndefined,
  isTrue,
  MARK_AS_TRANSLATABLE,
  ModelFormControlEvent,
  ofSolidifyActionCompleted,
  QueryParameters,
  ResourceActionHelper,
  ResourceNameSpace,
  ResourceStateModel,
  StoreUtil,
} from "solidify-frontend";

@Directive()
export abstract class SharedAbstractCreateEditDialog<TResourceModel extends BaseResourceType, UResourceStateModel extends ResourceStateModel<TResourceModel>, TData>
  extends AbstractCrudRoutable<TResourceModel, UResourceStateModel> implements OnInit, OnDestroy, AbstractDialogInterface<TData, boolean> {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_UPDATE_BUTTON: string = MARK_AS_TRANSLATABLE("dialog.createEdit.button.update");
  readonly KEY_CANCEL_BUTTON: string | undefined = MARK_AS_TRANSLATABLE("dialog.createEdit.button.cancel");
  readonly KEY_CREATE_TITLE: string | undefined = MARK_AS_TRANSLATABLE("dialog.createEdit.title.create");
  readonly KEY_UPDATE_TITLE: string | undefined = MARK_AS_TRANSLATABLE("dialog.createEdit.title.update");

  current: TResourceModel | undefined;
  isLoadingObs: Observable<boolean> = this._store.select(s => StoreUtil.isLoadingState(super.getState(s)));

  checkAvailableResourceNameSpace: ResourceNameSpace | undefined;

  paramMessage: any = undefined;
  titleToTranslate: string = undefined;
  tooltipToTranslate: string = undefined;

  get isUpdateMode(): boolean {
    return !this.isCreateMode;
  }

  get isCreateMode(): boolean {
    return isNullOrUndefined(this._data.current);
  }

  @ViewChild("formPresentational")
  override readonly formPresentational: AbstractFormPresentational<any>;

  protected constructor(@Inject(MAT_DIALOG_DATA) protected readonly _data: CreateEditDialog<TResourceModel>,
                        protected readonly _store: Store,
                        protected readonly _route: ActivatedRoute,
                        protected readonly _actions$: Actions,
                        protected readonly _changeDetector: ChangeDetectorRef,
                        protected readonly _dialog: MatDialog,
                        protected readonly _dialogRef: MatDialogRef<SharedAbstractCreateEditDialog<TResourceModel, UResourceStateModel, TData>, boolean>,
                        protected readonly _state: StateEnum,
                        protected readonly _resourceNameSpace: ResourceNameSpace,
                        protected readonly _injector: Injector,
                        protected readonly _parentState?: StateEnum) {
    super(_store, _state, _injector, _parentState);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.current = this._data.current;
    this._retrieveResource();
    if (!isNullOrUndefined(this.current)) {
      this._getSubResourceWithParentId(this.current.resId);
    }
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  protected _retrieveResource(): void {
    this._store.dispatch(ResourceActionHelper.loadResource(this._resourceNameSpace));
  }

  protected _cleanState(): void {
    this._store.dispatch(ResourceActionHelper.clean(this._resourceNameSpace, false, new QueryParameters()));
  }

  protected abstract _getSubResourceWithParentId(id: string): void;

  update(model: ModelFormControlEvent<TResourceModel>): Observable<any> {
    super.saveInProgress();
    if (this.isUpdateMode) {
      return this._store.dispatch(ResourceActionHelper.update<TResourceModel>(this._resourceNameSpace, model));
    }
    if (this.isCreateMode) {
      return this._store.dispatch(ResourceActionHelper.create<TResourceModel>(this._resourceNameSpace, model));
    }
  }

  confirm(): void {
    this.formPresentational.onSubmit();

    if (this.isUpdateMode) {
      this.subscribe(this._actions$.pipe(
        ofSolidifyActionCompleted(this._resourceNameSpace.UpdateSuccess),
        tap((result) => {
          this._closeDialogIfSuccess(result);
        }),
      ));
    }

    if (this.isCreateMode) {
      this.subscribe(this._actions$.pipe(
        ofSolidifyActionCompleted(this._resourceNameSpace.CreateSuccess),
        tap((result) => {
          this._closeDialogIfSuccess(result);
        }),
      ));
    }
  }

  private _closeDialogIfSuccess(result: ActionCompletion<boolean>): void {
    if (isTrue(result.result.successful)) {
      this.submit();
    }
  }

  close(): void {
    this._dialogRef.close(undefined);
  }

  submit(): void {
    this._dialogRef.close(true);
  }

  checkAvailable(formControlKey: FormControlKey): void {
    const resourceNameSpace = isNullOrUndefined(this.checkAvailableResourceNameSpace) ? this._resourceNameSpace : this.checkAvailableResourceNameSpace;
    this.subscribe(CrudHelper.checkAvailable(formControlKey, this._store, this._actions$, resourceNameSpace, this.current?.resId, this.labelTranslateInterface.applicationAlreadyUsed));
  }
}
