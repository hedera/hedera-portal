/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - api.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ApiModuleEnum} from "@app/shared/enums/api-module.enum";
import {ApiResourceNameEnum} from "@app/shared/enums/api-resource-name.enum";
import {environment} from "@environments/environment";
import {HederaEnvironment} from "@environments/environment.defaults.model";
import {
  isNullOrUndefinedOrWhiteString,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

export class ApiKeyword {
  static PARENT_ID: string = "parentId";
  static SUB_RESOURCE: string = "subResource";
}

const SEPARATOR: string = SOLIDIFY_CONSTANTS.URL_SEPARATOR;
const subResource: string = `{${ApiKeyword.SUB_RESOURCE}}`;
const parentId: string = `{${ApiKeyword.PARENT_ID}}`;
const parentIdInUrl: string = SEPARATOR + parentId + SEPARATOR;
const search: string = "search";
const api: string = "api";
const apiInUrl: string = SEPARATOR + api + SEPARATOR;

export class ApiEnum {
  static checkModuleDefine(module: keyof HederaEnvironment): void {
    if (isNullOrUndefinedOrWhiteString(environment[module])) {
      // eslint-disable-next-line no-console
      console.error(`The property '${module}' is not defined. Please check that the endpoint '${this.adminModules}' return a value for module '${module}'.`);
    }
  }

  static get access(): string {
    this.checkModuleDefine("access");
    return environment.access;
  }

  static get accessPublicMetadata(): string {
    return ApiEnum.access + SEPARATOR + ApiResourceNameEnum.PUBLIC_METADATA;
  }

  static get accessPrivateMetadata(): string {
    return ApiEnum.access + SEPARATOR + ApiResourceNameEnum.PRIVATE_METADATA;
  }

  static get accessResearchObject(): string {
    return ApiEnum.access + SEPARATOR + ApiResourceNameEnum.RESEARCH_OBJECT;
  }

  static get accessOrders(): string {
    return ApiEnum.access + SEPARATOR + ApiResourceNameEnum.ORDER;
  }

  static get accessProjects(): string {
    return ApiEnum.access + SEPARATOR + ApiResourceNameEnum.PROJECT;
  }

  static get ontologies(): string {
    this.checkModuleDefine("ontologies");
    return environment.ontologies;
  }

  static get sparql(): string {
    this.checkModuleDefine("sparql");
    return environment.sparql;
  }

  static get sparqlQuery(): string {
    return ApiEnum.sparql;
  }

  static get oaiInfo(): string {
    this.checkModuleDefine("oaiInfo");
    return environment.oaiInfo;
  }

  static get iiif(): string {
    this.checkModuleDefine("iiif");
    return environment.iiif;
  }

  static get iiifManifest(): string {
    return `${ApiEnum.iiif}/${ApiResourceNameEnum.MANIFEST}`;
  }

  static get iiifCollection(): string {
    return `${ApiEnum.iiif}/${ApiResourceNameEnum.COLLECTION}`;
  }

  static get linkedData(): string {
    this.checkModuleDefine("linkedData");
    return environment.linkedData;
  }

  static get admin(): string {
    if (isNullOrUndefinedOrWhiteString(environment.admin)) {
      // eslint-disable-next-line no-console
      console.error(`The property 'admin' is not defined in environment config files.`);
    }
    return environment.admin;
  }

  static get adminTriplestore(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.TRIPLESTORE;
  }

  static get adminGlobalBanners(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.GLOBAL_BANNERS;
  }

  static get adminRmls(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.RML;
  }

  static get adminFundingAgencies(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.FUNDING_AGENCY;
  }

  static get adminInstitutions(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.INSTITUTION;
  }

  static get adminLanguages(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.LANGUAGE;
  }

  static get adminLicenses(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.LICENSE;
  }

  static get adminOrcid(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.ORCID;
  }

  static get adminProjects(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.PROJECT;
  }

  static get adminAuthorizedProjects(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.AUTHORIZED_PROJECT;
  }

  static get adminPeople(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.PERSON;
  }

  static get adminMembers(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.MEMBER;
  }

  static get adminResearchObjects(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.RESEARCH_OBJECT;
  }

  static get adminResearchObjectTypes(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.RESEARCH_OBJECT_TYPE;
  }

  static get adminIIIFCollectionSettings(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.IIIF_COLLECTION_CONFIG;
  }

  static get adminRoles(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.ROLE;
  }

  static get adminUsers(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.USER;
  }

  static get adminSystemProperties(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.SYSTEM_PROPERTY;
  }

  static get adminScheduledTask(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.SCHEDULED_TASK;
  }

  static get adminModules(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.MODULES;
  }

  static get adminDocs(): string {
    const urlAdmin = environment.admin.substring(0, environment.admin.lastIndexOf("/"));
    return urlAdmin + SEPARATOR + ApiModuleEnum.DOCS;
  }

  static get adminOntologies(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.ONTOLOGY;
  }

  static get adminResourceSrv(): string {
    return ApiEnum.admin + SEPARATOR + ApiModuleEnum.RES_SRV;
  }

  static get ingest(): string {
    this.checkModuleDefine("ingest");
    return environment.ingest;
  }

  static get ingestSourceDatasets(): string {
    return ApiEnum.ingest + SEPARATOR + ApiResourceNameEnum.SOURCE_DATASETS;
  }

  static get ingestSourceDatasetFiles(): string {
    return ApiEnum.ingest + SEPARATOR + ApiResourceNameEnum.SOURCE_DATASETS + parentIdInUrl + ApiResourceNameEnum.SOURCE_DATASET_FILE;
  }

  static get ingestResearchDataFiles(): string {
    return ApiEnum.ingest + SEPARATOR + ApiResourceNameEnum.RESEARCH_DATA_FILES;
  }

  static get ingestRdfDatasetFiles(): string {
    return ApiEnum.ingest + SEPARATOR + ApiResourceNameEnum.RDF_DATASET_FILES;
  }

  static get ingestSourceDatasetFile(): string {
    return ApiEnum.ingest + SEPARATOR + ApiResourceNameEnum.SOURCE_DATASET_FILE;
  }

  static get index(): string {
    this.checkModuleDefine("index");
    return environment.index;
  }

  static get indexResourceIndexFieldAliases(): string {
    return ApiEnum.index + SEPARATOR + ApiResourceNameEnum.INDEX_FIELD_ALIASES;
  }
}
