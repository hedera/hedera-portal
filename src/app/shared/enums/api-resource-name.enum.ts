/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - api-resource-name.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {ApiResourceNamePartialEnum} from "solidify-frontend";

enum ApiResourceNameExtendEnum {
  SOURCE_DATASETS = "source-datasets",
  SOURCE_DATASET_FILE = "source-dataset-files",
  RESEARCH_DATA_FILES = "research-data-files",
  RDF_DATASET_FILES = "rdf-dataset-files",
  TRIPLESTORE = "triplestore",

  ORDER = "orders",
  PERSON = "people",
  MEMBER = "members",
  PROJECT = "projects",
  CURRENT_VERSION = "current-version",
  AUTHORIZED_PROJECT = "authorized-projects",
  INSTITUTION = "institutions",
  DI = "di",
  DI_PUBLIC = "public-di",
  DI_PRIVATE = "private-di",
  SETTING = "settings",
  LICENSE = "licenses",
  RESEARCH_OBJECT = "research-objects",

  RESEARCH_OBJECT_TYPE = "research-object-types",
  IIIF_COLLECTION_CONFIG = "iiif-collection-configs",
  ROLE = "roles",
  ROLE_APPLICATION = "application-roles",
  FUNDING_AGENCY = "funding-agencies",
  MODULES = "modules",
  USER = "users",
  ORCID = "orcid",
  BASIC_AUTH_CLIENT = "basic-auth-clients",
  LOGIN = "shiblogin",
  ITEM = "items",
  MONITOR = "monitor",
  MODULE = "modules",
  SCHEDULED_TASK = "scheduled-tasks",
  GLOBAL_BANNERS = "global-banners",
  QUERY = "queries",

  // Miscellaneous
  SCHEMA = "schema",
  PROFILE = "profile",
  XSL = "xsl",
  RESERVE_DOI = "reserve-doi",
  ARCHIVAL_TYPE = "archivalUnit",
  PUBLIC_METADATA = "metadata",
  PRIVATE_METADATA = "private-metadata",
  ALL_METADATA = "all-metadata",
  ARCHIVE = "archives",
  MANIFEST = "manifests",
  COLLECTION = "collections",

  // OAI
  ONTOLOGY = "ontologies",
  RML = "rmls",
  RDF_CLASS = "rdf-classes",
  RDF_PROPERTY = "rdf-properties",
}

export const ApiResourceNameEnum = {...ApiResourceNamePartialEnum, ...ApiResourceNameExtendEnum} as Omit<typeof ApiResourceNamePartialEnum, keyof typeof ApiResourceNameExtendEnum> & typeof ApiResourceNameExtendEnum;
export type ApiResourceNameEnum = typeof ApiResourceNameEnum[keyof typeof ApiResourceNameEnum];
