/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - state.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {StatePartialEnum} from "solidify-frontend";

enum StateExtendEnum {
  router = "router",

  application = "application",
  application_toc = "application_toc",
  application_user = "application_user",
  application_user_logo = "application_user_logo",
  application_systemProperty = "application_systemProperty",
  application_person = "application_person",
  application_person_institution = "application_person_institution",
  application_authorizedProject = "application_authorizedProject",
  application_memberProject = "application_memberProject",

  home = "home",

  ingest = "ingest",
  ingest_sourceDataset = "ingest_sourceDataset",
  ingest_sourceDataset_statusHistory = "ingest_sourceDataset_statusHistory",
  ingest_sourceDataset_file = "ingest_sourceDataset_file",
  ingest_sourceDataset_file_upload = "ingest_sourceDataset_file_upload",
  ingest_sourceDataset_file_statusHistory = "ingest_sourceDataset_file_statusHistory",

  ingest_researchDataFile = "ingest_researchDataFile",
  ingest_researchDataFile_upload = "ingest_researchDataFile_upload",
  ingest_researchDataFile_history = "ingest_researchDataFile_history",

  ingest_rdfDatasetFile = "ingest_rdfDatasetFile",
  ingest_rdfDatasetFile_history = "ingest_rdfDatasetFile_history",

  ingest_authorizedProject = "ingest_authorizedProject",
  ingest_project_rml = "ingest_project_rml",
  ingest_project_researchObjectType = "ingest_project_researchObjectType",

  browse = "browse",
  browse_project = "browse_project",
  browse_project_rml = "browse_project_rml",
  browse_project_researchDataFile = "browse_project_researchDataFile",
  browse_project_iiifManifest = "browse_project_iiifManifest",
  browse_project_iiifCollection = "browse_project_iiifCollection",
  browse_project_rdfObject = "browse_project_rdfObject",
  browse_project_researchObject = "browse_project_researchObject",
  browse_project_researchObjectType = "browse_project_researchObjectType",
  browse_project_personRole = "browse_project_personRole",
  browse_project_institutions = "browse_project_institutions",
  browse_project_fundingAgency = "browse_project_fundingAgency",
  browse_ontology = "browse_ontology",

  shared = "shared",
  shared_license = "shared_license",
  shared_sourceDataset = "shared_sourceDataset",
  shared_sourceDatasetFile = "shared_sourceDatasetFile",

  shared_ontology = "shared_ontology",
  shared_researchDataFile = "shared_researchDataFile",
  shared_role = "shared_role",
  shared_user = "shared_user",
  shared_institution = "shared_institution",
  shared_fundingAgency = "shared_fundingAgency",
  shared_person = "shared_person",
  shared_person_institution = "shared_person_institution",
  shared_project = "shared_project",
  shared_researchObjectType = "shared_researchObjectType",
  shared_iiifCollectionSettings = "shared_iiifCollectionSettings",
  shared_rml = "shared_rml",

  admin = "admin",
  admin_license = "admin_license",
  admin_scheduledTask = "admin_scheduledTask",
  admin_project = "admin_project",
  admin_project_personRole = "admin_project_personRole",
  admin_project_fundingAgency = "admin_project_fundingAgency",
  admin_project_institution = "admin_project_institution",
  admin_project_rml = "admin_project_rml",
  admin_project_researchObjectType = "admin_project_researchObjectType",
  admin_fundingAgencies_project = "admin_fundingAgencies_project",
  admin_institution = "admin_institution",
  admin_institution_project = "admin_institution_project",
  admin_user = "admin_user",
  admin_person = "admin_person",
  admin_person_projectRole = "admin_person_projectRole",
  admin_person_institutions = "admin_person_institutions",
  admin_researchObjectTypes = "admin_researchObjectTypes",
  admin_iiifCollectionSettings = "admin_iiifCollectionSettings",
  admin_role = "admin_role",
  admin_fundingAgencies = "admin_fundingAgencies",
  admin_language = "admin_language",
  admin_ontology = "admin_ontology",
  admin_rml = "admin_rml",
}

export const StateEnum = {...StatePartialEnum, ...StateExtendEnum} as Omit<typeof StatePartialEnum, keyof typeof StateExtendEnum> & typeof StateExtendEnum;
export type StateEnum = typeof StateEnum[keyof typeof StateEnum];
