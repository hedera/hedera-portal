/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - routes.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AppRoutesPartialEnum,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

export const urlSeparator: string = SOLIDIFY_CONSTANTS.URL_SEPARATOR;

enum AppRoutesExtendEnum {
  ingest = "ingest",
  admin = "admin",
  sparql = "sparql",
  browse = "browse",
  paramIdDataFile = ":idFile",
  paramIdDataFileWithoutPrefixParam = "idFile",
  paramIdProject = ":idProject",
  paramIdProjectWithoutPrefixParam = "idProject",
  paramIdExecution = ":idExecution",
  paramIdExecutionWithoutPrefixParam = "idExecution",
}

export const AppRoutesEnum = {...AppRoutesPartialEnum, ...AppRoutesExtendEnum} as Omit<typeof AppRoutesPartialEnum, keyof typeof AppRoutesExtendEnum> & typeof AppRoutesExtendEnum;
export type AppRoutesEnum = typeof AppRoutesEnum[keyof typeof AppRoutesEnum];

export enum HomePageRoutesEnum {
  detail = "detail",
  notFound = "not-found",
  search = "search",
  files = "files",
  collections = "collections",
}

export enum SparqlRoutesEnum {
  query = "query",
}

export enum IngestRoutesEnum {
  sourceDataset = "source-dataset",
  researchDataFile = "research-data-file",
  rdfDatasetFile = "rdf-dataset-file",
  paramTab = ":tab",
  paramTabWithoutPrefix = "tab",
}

export enum SourceDatasetRoutesEnum {
  create = "create",
  detail = "detail",
  edit = "edit",
  metadata = "metadata",
  data = "data",
  files = "files",
  collections = "collections",
  paramTab = ":tab",
  paramTabWithoutPrefix = "tab",
}

export enum ResearchDataFileRoutesEnum {
  create = "create",
  detail = "detail",
  edit = "edit",
  metadata = "metadata",
  data = "data",
  files = "files",
  collections = "collections",
  paramTab = ":tab",
  paramTabWithoutPrefix = "tab",
}

export enum AdminRoutesEnum {
  license = "license",
  licenseCreate = "create",
  licenseEdit = "edit",
  licenseDetail = "detail",

  project = "project",
  projectCreate = "create",
  projectDetail = "detail",
  projectEdit = "edit",

  institution = "institution",
  institutionCreate = "create",
  institutionDetail = "detail",
  institutionEdit = "edit",

  language = "language",

  user = "user",
  userDetail = "detail",
  userEdit = "edit",

  person = "person",
  personDetail = "detail",
  personCreate = "create",
  personEdit = "edit",

  researchObject = "researchObject",
  researchObjectDetail = "detail",
  researchObjectCreate = "create",
  researchObjectEdit = "edit",

  researchObjectType = "researchObjectType",
  researchObjectTypeDetail = "detail",
  researchObjectTypeCreate = "create",
  researchObjectTypeEdit = "edit",

  role = "role",
  roleDetail = "detail",
  roleCreate = "create",
  roleEdit = "edit",

  rml = "rml",
  rmlDetail = "detail",
  rmlCreate = "create",
  rmlEdit = "edit",

  fundingAgencies = "funding-agencies",
  fundingAgenciesDetail = "detail",
  fundingAgenciesCreate = "create",
  fundingAgenciesEdit = "edit",

  scheduledTask = "scheduled-task",
  scheduledTaskCreate = "create",
  scheduledTaskEdit = "edit",
  scheduledTaskDetail = "detail",

  ontology = "ontology",
  ontologyCreate = "create",
  ontologyEdit = "edit",
  ontologyDetail = "detail",

  iiifCollectionSettings = "iiif-collection-setting",
  iiifCollectionSettingsDetail = "detail",
  iiifCollectionSettingsCreate = "create",
  iiifCollectionSettingsEdit = "edit",
}

export enum BrowseRoutesEnum {
  project = "project",
  ontology = "ontology",
}

export enum BrowseProjectRoutesEnum {
  create = "create",
  detail = "detail",
  edit = "edit",
  metadata = "metadata",
  researchObject = "research-object",
  researchDataFile = "research-data-file",
  rdfObject = "rdf-object",
  manifest = "manifest",
  iiifCollection = "iiif-collection",
  notFound = "not-found",
}

export enum BrowseOntologyRoutesEnum {
  detail = "detail",
}

export class RoutesEnum implements RoutesEnum {
  static index: string = AppRoutesEnum.index;
  static login: string = AppRoutesEnum.login;
  static maintenance: string = AppRoutesEnum.maintenance;
  static about: string = AppRoutesEnum.about;
  static releaseNotes: string = AppRoutesEnum.releaseNotes;
  static changelog: string = AppRoutesEnum.changelog;
  static serverOffline: string = AppRoutesEnum.serverOffline;
  static unableToLoadApp: string = AppRoutesEnum.unableToLoadApp;
  static colorCheck: string = AppRoutesEnum.colorCheck;

  static homePage: string = AppRoutesEnum.home;
  static homeSearch: string = AppRoutesEnum.home + urlSeparator + HomePageRoutesEnum.search;
  static homeDetail: string = AppRoutesEnum.home + urlSeparator + HomePageRoutesEnum.detail;
  static homeNotFound: string = AppRoutesEnum.home + urlSeparator + HomePageRoutesEnum.notFound;

  static sparql: string = AppRoutesEnum.sparql;
  static sparqlQuery: string = SparqlRoutesEnum.query;

  static admin: string = AppRoutesEnum.admin;

  static adminLicense: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.license;
  static adminLicenseCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.license + urlSeparator + AdminRoutesEnum.licenseCreate;
  static adminLicenseDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.license + urlSeparator + AdminRoutesEnum.licenseDetail;

  static adminRml: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.rml;
  static adminRmlCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.rml + urlSeparator + AdminRoutesEnum.rmlCreate;
  static adminRmlDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.rml + urlSeparator + AdminRoutesEnum.rmlDetail;

  static adminProject: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.project;
  static adminProjectCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.project + urlSeparator + AdminRoutesEnum.projectCreate;
  static adminProjectDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.project + urlSeparator + AdminRoutesEnum.projectDetail;

  static adminInstitution: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.institution;
  static adminInstitutionCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.institution + urlSeparator + AdminRoutesEnum.institutionCreate;
  static adminInstitutionDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.institution + urlSeparator + AdminRoutesEnum.institutionDetail;

  static adminLanguage: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.language;

  static adminUser: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.user;
  static adminUserDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.user + urlSeparator + AdminRoutesEnum.userDetail;

  static adminPerson: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.person;
  static adminPersonCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.person + urlSeparator + AdminRoutesEnum.personCreate;
  static adminPersonDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.person + urlSeparator + AdminRoutesEnum.personDetail;

  static adminRole: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.role;
  static adminRoleCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.role + urlSeparator + AdminRoutesEnum.roleCreate;
  static adminRoleDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.role + urlSeparator + AdminRoutesEnum.roleDetail;

  static adminResearchObject: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.researchObject;
  static adminResearchObjectCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.researchObject + urlSeparator + AdminRoutesEnum.researchObjectCreate;
  static adminResearchObjectDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.researchObject + urlSeparator + AdminRoutesEnum.researchObjectDetail;

  static adminResearchObjectType: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.researchObjectType;
  static adminResearchObjectTypeCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.researchObjectType + urlSeparator + AdminRoutesEnum.researchObjectTypeCreate;
  static adminResearchObjectTypeDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.researchObjectType + urlSeparator + AdminRoutesEnum.researchObjectTypeDetail;

  static adminIIIFCollectionSettings: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.iiifCollectionSettings;
  static adminIIIFCollectionSettingsCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.iiifCollectionSettings + urlSeparator + AdminRoutesEnum.iiifCollectionSettingsCreate;
  static adminIIIFCollectionSettingsDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.iiifCollectionSettings + urlSeparator + AdminRoutesEnum.iiifCollectionSettingsDetail;

  static adminFundingAgencies: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.fundingAgencies;
  static adminFundingAgenciesCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.fundingAgencies + urlSeparator + AdminRoutesEnum.fundingAgenciesCreate;
  static adminFundingAgenciesDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.fundingAgencies + urlSeparator + AdminRoutesEnum.fundingAgenciesDetail;

  static adminScheduledTask: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.scheduledTask;
  static adminScheduledTaskCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.scheduledTask + urlSeparator + AdminRoutesEnum.scheduledTaskCreate;
  static adminScheduledTaskDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.scheduledTask + urlSeparator + AdminRoutesEnum.scheduledTaskDetail;

  static adminOntology: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.ontology;
  static adminOntologyCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.ontology + urlSeparator + AdminRoutesEnum.ontologyCreate;
  static adminOntologyDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.ontology + urlSeparator + AdminRoutesEnum.ontologyDetail;

  static ingest: string = AppRoutesEnum.ingest;
  static ingestSourceDataset: string = AppRoutesEnum.ingest + urlSeparator + IngestRoutesEnum.sourceDataset;
  static ingestSourceDatasetCreate: string = AppRoutesEnum.ingest + urlSeparator + IngestRoutesEnum.sourceDataset + urlSeparator + SourceDatasetRoutesEnum.create;
  static ingestSourceDatasetDetail: string = AppRoutesEnum.ingest + urlSeparator + IngestRoutesEnum.sourceDataset + urlSeparator + SourceDatasetRoutesEnum.detail;

  static ingestResearchDataFile: string = AppRoutesEnum.ingest + urlSeparator + IngestRoutesEnum.researchDataFile;
  static ingestResearchDataFileCreate: string = AppRoutesEnum.ingest + urlSeparator + IngestRoutesEnum.researchDataFile + urlSeparator + SourceDatasetRoutesEnum.create;
  static ingestResearchDataFileDetail: string = AppRoutesEnum.ingest + urlSeparator + IngestRoutesEnum.researchDataFile + urlSeparator + SourceDatasetRoutesEnum.detail;

  static ingestRdfDatasetFile: string = AppRoutesEnum.ingest + urlSeparator + IngestRoutesEnum.rdfDatasetFile;
  static ingestRdfDatasetFileCreate: string = AppRoutesEnum.ingest + urlSeparator + IngestRoutesEnum.rdfDatasetFile + urlSeparator + SourceDatasetRoutesEnum.create;
  static ingestRdfDatasetFileDetail: string = AppRoutesEnum.ingest + urlSeparator + IngestRoutesEnum.rdfDatasetFile + urlSeparator + SourceDatasetRoutesEnum.detail;

  static browse: string = AppRoutesEnum.browse;
  static browseProject: string = AppRoutesEnum.browse + urlSeparator + BrowseRoutesEnum.project;
  static browseProjectDetail: string = AppRoutesEnum.browse + urlSeparator + BrowseRoutesEnum.project + urlSeparator + BrowseProjectRoutesEnum.detail;
  static browseOntology: string = AppRoutesEnum.browse + urlSeparator + BrowseRoutesEnum.ontology;
  static browseOntologyDetail: string = AppRoutesEnum.browse + urlSeparator + BrowseRoutesEnum.ontology + urlSeparator + BrowseOntologyRoutesEnum.detail;
}
