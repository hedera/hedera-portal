/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - api-action-name.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ApiActionNamePartialEnum} from "solidify-frontend";

export enum ApiActionNameExtendEnum {
  LIST_AIP_CONTAINER = "list-aip-containers",
  LIST_ACCESS_TYPE = "list-accesses",
  LIST_DATA_CATEGORY = "list-data-categories",
  LIST_DATA_TYPE = "list-data-types",
  LIST_QUERY_TYPE = "list-query-types",
  LIST_JOB_TYPE = "list-job-types",
  LIST_JOB_RECURRENCE = "list-job-recurrences",
  LIST_OAUTH2_GRANT_TYPE = "list-grant-types",
  LIST_RESEARCH_DOMAIN_SOURCES = "list-sources",
  LIST_STATUS = "list-status",
  LIST_FOLDERS = "list-folders",
  LIST_EXCLUDE_FILES = "list-exclude-files",
  LIST_IGNORE_FILES = "list-ignore-files",

  ARCHIVAL_UNITS = "archivalUnits",
  ARCHIVAL_COLLECTIONS = "archivalCollections",

  ORDER_LIST_BY_USER = "list-for-user",

  REINDEX = "reindex",
  SET_ROLE = "set-role",
  CHECK = "check",
  CHECK_COMPLIANCE = "check-compliance",
  CHECK_FIXITY = "check-fixity",
  SEARCH_DOI = "search-doi",
  SEARCH_WITH_USER = "search-with-user",
  APPROVE_DISPOSAL = "approve-disposal",
  EXTEND_RETENTION = "extend-retention",

  SELF = "self",
  MODULE = "module",
  PARENT = "parent",
  STATUS = "status",
  DASHBOARD = "dashboard",
  RESOURCES = "resources",
  LIST = "list",
  FILE = "file",
  COUNT = "count",
  PREVIOUS = "previous",
  NEXT = "next",
  CREATE = "new",
  READ = "get",
  UPDATE = "edit",
  DELETE = "remove",
  VALUES = "values",
  SEARCH = "search",
  LASTCREATED = "lastCreated",
  LASTUPDATE = "lastUpdated",
  RANGE = "searchRange",
  SIZE = "size",
  PAGE = "page",
  SORT = "sort",
  CREATION = "creation.when",
  UPDATED = "lastUpdate.when",
  ERROR = "error",
  UL = "upload",
  UL_ARCHIVE = "upload-archive",
  DL = "download",
  HISTORY = "history",
  RESUME = "resume",
  START = "start",
  ALL = "All",
  VIEW = "view",
  SAVE = "save",
  IMPORT = "import",
  IMPORTED = "imported",
  CLOSE = "close",
  DELETE_CACHE = "delete-cache",
  DOWNLOAD_STATUS = "download-status",
  LIST_CURRENT_STATUS = "list-current-status",
  PREPARE_DOWNLOAD = "prepare-download",
  RESERVE_DOI = "reserve-doi",
  DELETE_FOLDER = "delete-folder",
  RESUBMIT = "resubmit",
  PUT_IN_ERROR = "put-in-error",
  DOWNLOAD_FILE = "download-file",
  UPLOAD_FILE = "upload-file",
  DELETE_FILE = "delete-file",
  UPLOAD_ZIP = "upload-zip",

  LIST_AIP_WITH_STATUS = "list-aip-statuses",
  DL_STATUS = "download-status",
  LIST_DL_STATUS = "list-download-status",

  AUTHORIZE = "authorize",
  TOKEN = "token",
  CHECK_TOKEN = "check_token",
  REVOKE_ALL_TOKENS = "revoke-all-tokens",
  REVOKE_MY_TOKENS = "revoke-my-tokens",
  USER_ID = "externalUid",
  AUTHENTICATED = "authenticated",
  CLEAN = "clean",

  SUBMIT = "submit",
  SUBMIT_FOR_APPROVAL = "submit-for-approval",
  APPROVE = "approve",
  REJECT = "reject",
  ENABLE_REVISION = "enable-revision",
  INIT = "initialize",
  REPORT = "reports",
  DETAIL = "detail",
  VALIDATE = "validate",
  SENT = "sent",
  INBOX = "inbox",
  START_METADATA_EDITING = "start-metadata-editing",
  SET_PROCESSED = "set-processed",
  SET_REFUSED = "set-refused",
  SET_PENDING = "set-pending",
  CANCEL_METADATA_EDITING = "cancel-metadata-editing",
  REFRESH = "refresh",
  IIIF_REFRESH = "iiif-refresh",
  PURGE_RDF = "purge-rdf",
  SYNCHRONIZE = "synchronize",
  UPLOAD_LOGO = "upload-logo",
  DOWNLOAD_LOGO = "download-logo",
  DELETE_LOGO = "delete-logo",
  UPLOAD_AVATAR = "upload-avatar",
  DOWNLOAD_AVATAR = "download-avatar",
  DELETE_AVATAR = "delete-avatar",
  UPLOAD_THUMBNAIL = "upload-thumbnail",
  DOWNLOAD_THUMBNAIL = "download-thumbnail",
  DELETE_THUMBNAIL = "delete-thumbnail",
  THUMBNAIL = "thumbnail",
  DOWNLOAD_RDF = "download-rdf",
  UPLOAD_RDF = "upload-rdf",
  DELETE_RDF = "delete-rdf",

  DOWNLOAD_ONTOLOGY = "download-ontology",
  UPLOAD_ONTOLOGY = "upload-ontology",
  DELETE_ONTOLOGY = "delete-ontology",
  README = "readme",
  STATISTICS = "statistics",
  RATING = "ratings",
  RATING_BY_USER = "list-for-user",
  BY_RATING = "by-rating",
  BIBLIOGRAPHIES = "bibliographies",
  CITATIONS = "citations",
  GET_MY_ACLS = "get-my-acls",
  LIST_REFERENCE_TYPES = "list-reference-types",
  GET_ACTIVE = "get-active",
  LIST_INHERITED_PERSON_ROLES = "list-inherited-person-roles",
  SCHEDULED_ENABLED_TASK = "schedule-enabled-tasks",
  DISABLE_TASKS_SCHEDULING = "disable-tasks-scheduling",
  KILL_TASK = "kill-task",
  APPLY_RML = "apply-rml",
  TRANSFORM_RDF = "transform-rdf",
  ADD_DATASET = "add-dataset",
  REPLACE_DATASET = "replace-dataset",
  DELETE_DATASET = "delete-dataset",
  RELOAD = "reload",
  IS_PART_OF = "is-part-of",
}

export const ApiActionNameEnum = {...ApiActionNamePartialEnum, ...ApiActionNameExtendEnum} as Omit<typeof ApiActionNamePartialEnum, keyof typeof ApiActionNameExtendEnum> & typeof ApiActionNameExtendEnum;
export type ApiActionNameEnum = typeof ApiActionNameEnum[keyof typeof ApiActionNameEnum];
