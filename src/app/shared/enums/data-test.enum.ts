/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - data-test.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {DataTestPartialEnum} from "solidify-frontend";

enum DataTestExtendEnum {
  // Auth
  loginHorizontalInput = "login-horizontal-input",
  loginMobileInput = "login-mobile-input",
  logoutMobileInput = "logout-mobile-input",

  // Link Angular Module
  linkMenuAdmin = "link-menu-admin",
  linkMenuIngest = "link-menu-ingest",
  linkMenuBrowse = "link-menu-browse",
  linkMenuHome = "link-menu-home",
  linkMenuSparqlQuery = "link-menu-sparql-query",

  // Main Button
  back = "back",
  create = "create",
  save = "save",
  delete = "delete",

  // Home
  homeSearchInput = "home-search-input",
  homeDataTableSearch = "home-data-table-search",
  homeTile = "home-tile",
  homeButtonViewTile = "home-button-view-tile",
  homeButtonViewList = "home-button-view-list",
  homeFacet = "home-facet",
  homeArchiveDetailDownloadButton = "home-archive-detail-download-button",
  homeArchiveDetailRequestAccessButton = "home-archive-detail-request-access-button",
  homeArchiveDetailLinkFilesOrCollections = "home-archive-detail-link-files-or-collections",

  // Browse
  browseTabProject = "browse-tab-project",

  // Dataset
  ingestDataTable = "ingest-data-table",
  ingestTitle = "ingest-title",
  ingestDescription = "ingest-description",
  ingestPublicationDate = "ingest-publicationDate",
  ingestAccessLevel = "ingest-accessLevel",
  ingestAddMeAuthor = "ingest-addMeAuthor",
  ingestLanguage = "ingest-language",
  ingestDataType = "ingest-data-type",
  ingestFileDataTable = "ingest-file-data-table",
  ingestTabSourceDataset = "ingest-tab-sourceDataset",
  ingestTabResearchDataFile = "ingest-tab-researchDataFile",
  ingestSourceDatasetListSearchName = "ingest-sourceDataset-list-searchName",
  ingestSourceDatasetName = "ingest-sourceDataset-name",
  ingestSourceDatasetDescription = "ingest-sourceDataset-description",

  // Admin Home
  adminTileProject = "admin-tile-project",
  adminTileUsers = "admin-tile-users",

  // Admin Project
  adminProjectName = "admin-project-name",
  adminProjectListSearchName = "admin-project-list-searchName",
  adminProjectSubmissionPolicy = "admin-project-submissionPolicy",
  adminProjectPreservationPolicy = "admin-project-preservationPolicy",

  // Shared
  sharedProjectButtonDelete = "shared-project-buttonDelete",
  sharedPersonProjectRoleButtonAdd = "shared-personProjectRole-buttonAdd",
  sharedPersonProjectRoleButtonDelete = "shared-personProjectRole-buttonDelete",
  sharedPersonProjectRoleInputRole = "shared-personProjectRole-inputRole",
  sharedPersonProjectRoleInputProject = "shared-personProjectRole-inputProject",
  sharedPersonProjectRoleInputPerson = "shared-personProjectRole-inputPerson",

}

export const DataTestEnum = {...DataTestPartialEnum, ...DataTestExtendEnum} as Omit<typeof DataTestPartialEnum, keyof typeof DataTestExtendEnum> & typeof DataTestExtendEnum;
export type DataTestEnum = typeof DataTestEnum[keyof typeof DataTestEnum];
