/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - api-module.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



export class ApiModuleEnum {
  public static AUTH: string = "oauth";
  public static RES_SRV: string = "resource-srv";
  public static ADMIN: string = "admin";
  public static INGEST: string = "ingest";
  public static ACCESS: string = "access";
  public static DATAMGMT: string = "data-mgmt";
  public static DOCS: string = "docs";
}
