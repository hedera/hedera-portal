/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - step-tour-section-name.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {StepTourSectionNamePartialEnum} from "solidify-frontend";

enum StepTourSectionNameExtendEnum {
  main = "main",
  dataset = "dataset",
  datasetMetadata = "datasetMetadata",
  datasetData = "datasetData",
  preservationSpace = "preservationSpace",
  project = "project",
  search = "search",
}

export const StepTourSectionNameEnum = {...StepTourSectionNamePartialEnum, ...StepTourSectionNameExtendEnum} as Omit<typeof StepTourSectionNamePartialEnum, keyof typeof StepTourSectionNameExtendEnum> & typeof StepTourSectionNameExtendEnum;
export type StepTourSectionNameEnum = typeof StepTourSectionNameEnum[keyof typeof StepTourSectionNameEnum];
