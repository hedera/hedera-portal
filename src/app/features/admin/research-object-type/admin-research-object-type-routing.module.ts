/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-research-object-type-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {AdminResearchObjectTypeCreateRoutable} from "@admin/research-object-type/components/routables/admin-research-object-type-create/admin-research-object-type-create.routable";
import {AdminResearchObjectTypeDetailEditRoutable} from "@admin/research-object-type/components/routables/admin-research-object-type-detail-edit/admin-research-object-type-detail-edit.routable";
import {AdminResearchObjectTypeListRoutable} from "@admin/research-object-type/components/routables/admin-research-object-type-list/admin-research-object-type-list.routable";
import {AdminResearchObjectTypeState} from "@admin/research-object-type/stores/admin-research-object-type.state";
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {ApplicationRolePermissionEnum} from "@shared/enums/application-role-permission.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AdminRoutesEnum,
  AppRoutesEnum,
} from "@shared/enums/routes.enum";
import {ApplicationRoleGuardService} from "@shared/guards/application-role-guard.service";
import {HederaRoutes} from "@shared/models/hedera-route.model";
import {
  CanDeactivateGuard,
  CombinedGuardService,
  EmptyContainer,
} from "solidify-frontend";

const routes: HederaRoutes = [
  {
    path: AppRoutesEnum.root,
    component: AdminResearchObjectTypeListRoutable,
    data: {},
  },
  {
    path: AdminRoutesEnum.researchObjectTypeDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: AdminResearchObjectTypeDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: AdminResearchObjectTypeState.currentName,
    },
    children: [
      {
        path: AdminRoutesEnum.researchObjectTypeEdit,
        component: EmptyContainer,
        data: {
          breadcrumb: LabelTranslateEnum.edit,
        },
        canDeactivate: [CanDeactivateGuard],
      },
    ],
  },
  {
    path: AdminRoutesEnum.researchObjectTypeCreate,
    component: AdminResearchObjectTypeCreateRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.create,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
    canDeactivate: [CanDeactivateGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminResearchObjectTypeRoutingModule {
}
