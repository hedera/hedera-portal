/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-research-object-type-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {
  Ontology,
  ResearchObjectType,
} from "@models";
import {SharedAbstractFormPresentational} from "@shared/components/presentationals/shared-abstract-form/shared-abstract.presentational";
import {SharedOntologyOverlayPresentational} from "@shared/components/presentationals/shared-ontology-overlay/shared-ontology-overlay.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {sharedOntologyActionNameSpace} from "@shared/stores/ontology/shared-ontology.action";
import {SharedOntologyState} from "@shared/stores/ontology/shared-ontology.state";
import {
  isEmptyString,
  isNullOrUndefined,
  OrderEnum,
  PropertyName,
  ResourceNameSpace,
  SolidifyValidator,
  Sort,
  Type,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-research-object-type-form",
  templateUrl: "./admin-research-object-type-form.presentational.html",
  styleUrls: ["./admin-research-object-type-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminResearchObjectTypeFormPresentational extends SharedAbstractFormPresentational<ResearchObjectType> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  sharedOntologySort: Sort<Ontology> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedOntologyActionNameSpace: ResourceNameSpace = sharedOntologyActionNameSpace;
  sharedOntologyState: typeof SharedOntologyState = SharedOntologyState;

  ontologieCallback: (value: Ontology) => string = (value: Ontology) => value.name + " (" + value.version + ")";

  ontologyOverlayData: Ontology;
  ontologyOverlayComponent: Type<SharedOntologyOverlayPresentational> = SharedOntologyOverlayPresentational;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.rdfType]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: ["", [SolidifyValidator]],
      [this.formDefinition.ontology]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.sparqlListQuery]: ["", [SolidifyValidator]],
      [this.formDefinition.sparqlDetailQuery]: ["", [SolidifyValidator]],
    });
  }

  protected _bindFormTo(researchObjectType: ResearchObjectType): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: [researchObjectType.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.rdfType]: [researchObjectType.rdfType, [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: [researchObjectType.description, [SolidifyValidator]],
      [this.formDefinition.ontology]: [researchObjectType?.ontology?.resId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.sparqlListQuery]: [researchObjectType.sparqlListQuery, [SolidifyValidator]],
      [this.formDefinition.sparqlDetailQuery]: [researchObjectType.sparqlDetailQuery, [SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(researchObjectType: ResearchObjectType): ResearchObjectType {
    const ontologyId = this.form.get(this.formDefinition.ontology).value;
    if (isNullOrUndefined(ontologyId) || isEmptyString(ontologyId)) {
      researchObjectType.ontology = null;
    } else {
      researchObjectType.ontology = {resId: ontologyId};
    }
    return researchObjectType;
  }

  navigateToOntology(ontologyId: string): void {
    this.navigate([RoutesEnum.adminOntologyDetail, ontologyId]);
  }

  setOntologyOverlayData(ontology: Ontology): void {
    this.ontologyOverlayData = ontology;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() rdfType: string;
  @PropertyName() description: string;
  @PropertyName() sparqlListQuery: string;
  @PropertyName() sparqlDetailQuery: string;
  @PropertyName() ontology: string;
}
