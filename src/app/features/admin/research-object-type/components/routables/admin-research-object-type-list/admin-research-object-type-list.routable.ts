/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-research-object-type-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminResearchObjectTypeActionNameSpace} from "@admin/research-object-type/stores/admin-research-object-type.action";
import {AdminResearchObjectTypeStateModel} from "@admin/research-object-type/stores/admin-research-object-type.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {sharedOntologyActionNameSpace} from "@app/shared/stores/ontology/shared-ontology.action";
import {
  Ontology,
  Project,
  ResearchObjectType,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {SharedOntologyState} from "@shared/stores/ontology/shared-ontology.state";
import {
  AbstractListRoutable,
  DataTableFieldTypeEnum,
  OrderEnum,
  ResourceNameSpace,
  RouterExtensionService,
  Sort,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-research-object-type-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./admin-research-object-type-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminResearchObjectTypeListRoutable extends AbstractListRoutable<ResearchObjectType, AdminResearchObjectTypeStateModel> {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToAdmin;
  readonly KEY_PARAM_NAME: keyof ResearchObjectType & string = "name";

  sharedOntologySort: Sort<Ontology> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedOntologyActionNameSpace: ResourceNameSpace = sharedOntologyActionNameSpace;
  sharedOntologyState: typeof SharedOntologyState = SharedOntologyState;
  ontologieCallback: (value: Ontology) => string = (value: Ontology) => value.name + " (" + value.version + ")";

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.admin_researchObjectTypes, adminResearchObjectTypeActionNameSpace, _injector, {
      canCreate: _securityService.isRootOrAdmin(),
    }, StateEnum.admin);
  }

  conditionDisplayEditButton(model: Project | undefined): boolean {
    return this._securityService.isRootOrAdmin();
  }

  conditionDisplayDeleteButton(model: Project | undefined): boolean {
    return this._securityService.isRootOrAdmin();
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "description",
        header: LabelTranslateEnum.description,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },

      {
        field: "rdfType",
        header: LabelTranslateEnum.rdfTypeLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "ontology.name" as any,
        header: LabelTranslateEnum.ontologie,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        filterableField: "ontology.resId" as any,
        sortableField: "ontology.resId" as any,
        resourceNameSpace: this.sharedOntologyActionNameSpace,
        resourceState: this.sharedOntologyState as any,
        searchableSingleSelectSort: this.sharedOntologySort,
        resourceLabelCallback: this.ontologieCallback,
        callbackValue: rowData => this.ontologieCallback(rowData.ontology),
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
    ];
  }
}
