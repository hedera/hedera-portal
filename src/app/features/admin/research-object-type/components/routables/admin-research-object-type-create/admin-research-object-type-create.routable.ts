/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-research-object-type-create.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  AdminResearchObjectTypeState,
  AdminResearchObjectTypeStateModel,
} from "@admin/research-object-type/stores/admin-research-object-type.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {ResearchObjectType} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {sharedResearchObjectTypeActionNameSpace} from "@shared/stores/research-object-type/shared-research-object-type.action";
import {Observable} from "rxjs";
import {
  AbstractCreateRoutable,
  ResourceNameSpace,
} from "solidify-frontend";
import {adminResearchObjectTypeActionNameSpace} from "@admin/research-object-type/stores/admin-research-object-type.action";

@Component({
  selector: "hedera-admin-research-object-type-create.routable",
  templateUrl: "./admin-research-object-type-create.routable.html",
  styleUrls: ["./admin-research-object-type-create.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminResearchObjectTypeCreateRoutable extends AbstractCreateRoutable<ResearchObjectType, AdminResearchObjectTypeStateModel> {
  @Select(AdminResearchObjectTypeState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminResearchObjectTypeState.isReadyToBeDisplayedInCreateMode) isReadyToBeDisplayedInCreateModeObs: Observable<boolean>;

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedResearchObjectTypeActionNameSpace;

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector) {
    super(_store, _actions$, _changeDetector, StateEnum.admin_researchObjectTypes, _injector, adminResearchObjectTypeActionNameSpace, StateEnum.admin);
  }

}
