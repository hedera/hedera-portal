/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-research-object-type.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {AdminResearchObjectTypeRoutingModule} from "@admin/research-object-type/admin-research-object-type-routing.module";
import {AdminResearchObjectTypeFormPresentational} from "@admin/research-object-type/components/presentationals/admin-research-object-type-form/admin-research-object-type-form.presentational";
import {AdminResearchObjectTypeCreateRoutable} from "@admin/research-object-type/components/routables/admin-research-object-type-create/admin-research-object-type-create.routable";
import {AdminResearchObjectTypeDetailEditRoutable} from "@admin/research-object-type/components/routables/admin-research-object-type-detail-edit/admin-research-object-type-detail-edit.routable";
import {AdminResearchObjectTypeListRoutable} from "@admin/research-object-type/components/routables/admin-research-object-type-list/admin-research-object-type-list.routable";
import {AdminResearchObjectTypeState} from "@admin/research-object-type/stores/admin-research-object-type.state";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

const routables = [
  AdminResearchObjectTypeListRoutable,
  AdminResearchObjectTypeCreateRoutable,
  AdminResearchObjectTypeDetailEditRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminResearchObjectTypeFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminResearchObjectTypeRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminResearchObjectTypeState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminResearchObjectTypeModule {
}
