/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {AdminHomeRoutable} from "@app/features/admin/components/routables/admin-home/admin-home.routable";
import {
  AdminRoutesEnum,
  AppRoutesEnum,
} from "@app/shared/enums/routes.enum";
import {ApplicationRoleGuardService} from "@app/shared/guards/application-role-guard.service";
import {ApplicationRolePermissionEnum} from "@shared/enums/application-role-permission.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {HederaRoutes} from "@shared/models/hedera-route.model";
import {
  CombinedGuardService,
  labelSolidifyOaiPmh,
  labelSolidifySearch,
} from "solidify-frontend";

const routes: HederaRoutes = [
  {
    path: AppRoutesEnum.root,
    component: AdminHomeRoutable,
    data: {},
  },
  {
    path: AdminRoutesEnum.license,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./license/admin-license.module").then(m => m.AdminLicenseModule),
    data: {
      breadcrumb: LabelTranslateEnum.licenses,
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.globalBanner,
    // @ts-ignore Dynamic import
    loadChildren: () => import("solidify-frontend").then(m => m.AdminGlobalBannerModule),
    data: {
      breadcrumb: LabelTranslateEnum.globalBanners,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.institution,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./institution/admin-institution.module").then(m => m.AdminInstitutionModule),
    data: {
      breadcrumb: LabelTranslateEnum.institutions,
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.fundingAgencies,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./funding-agencies/admin-funding-agencies.module").then(m => m.AdminFundingAgenciesModule),
    data: {
      breadcrumb: LabelTranslateEnum.fundingAgencies,
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.indexFieldAlias,
    // @ts-ignore Dynamic import
    loadChildren: () => import("solidify-frontend").then(m => m.AdminIndexFieldAliasModule),
    data: {
      breadcrumb: labelSolidifySearch.searchIndexFieldAliasBreadcrumb,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.oaiMetadataPrefix,
    // @ts-ignore Dynamic import
    loadChildren: () => import("solidify-frontend").then(m => m.AdminOaiMetadataPrefixModule),
    data: {
      breadcrumb: labelSolidifyOaiPmh.oaiPmhOaiMetadataPrefixBreadcrumb,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.oaiSet,
    // @ts-ignore Dynamic import
    loadChildren: () => import("solidify-frontend").then(m => m.AdminOaiSetModule),
    data: {
      breadcrumb: labelSolidifyOaiPmh.oaiPmhOaiSetBreadcrumb,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.project,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./project/admin-project.module").then(m => m.AdminProjectModule),
    data: {
      breadcrumb: LabelTranslateEnum.projects,
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.person,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./person/admin-person.module").then(m => m.AdminPersonModule),
    data: {
      breadcrumb: LabelTranslateEnum.people,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.role,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./role/admin-role.module").then(m => m.AdminRoleModule),
    data: {
      breadcrumb: LabelTranslateEnum.roles,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.ontology,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./ontology/admin-ontology.module").then(m => m.AdminOntologyModule),
    data: {
      breadcrumb: LabelTranslateEnum.ontologies,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.user,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./user/admin-user.module").then(m => m.AdminUserModule),
    data: {
      breadcrumb: LabelTranslateEnum.users,
      permission: ApplicationRolePermissionEnum.rootPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.language,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./language/admin-language.module").then(m => m.AdminLanguageModule),
    data: {
      breadcrumb: LabelTranslateEnum.languages,
      permission: ApplicationRolePermissionEnum.rootPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.scheduledTask,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./scheduled-task/admin-scheduled-task.module").then(m => m.AdminScheduledTaskModule),
    data: {
      breadcrumb: LabelTranslateEnum.scheduledTask,
      permission: ApplicationRolePermissionEnum.rootPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.rml,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./rml/admin-rml.module").then(m => m.AdminRmlModule),
    data: {
      breadcrumb: LabelTranslateEnum.rmls,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.researchObjectType,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./research-object-type/admin-research-object-type.module").then(m => m.AdminResearchObjectTypeModule),
    data: {
      breadcrumb: LabelTranslateEnum.researchObjectTypes,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.iiifCollectionSettings,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./iiif-collection-settings/admin-iiif-collection-settings.module").then(m => m.AdminIIIFCollectionSettingsModule),
    data: {
      breadcrumb: LabelTranslateEnum.iiifCollectionSettings,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {
}
