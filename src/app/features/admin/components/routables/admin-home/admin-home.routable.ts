/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-home.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  ChangeDetectionStrategy,
  Component,
  Injector,
} from "@angular/core";
import {ApplicationRolePermissionEnum} from "@app/shared/enums/application-role-permission.enum";
import {RoutesEnum} from "@app/shared/enums/routes.enum";
import {AppState} from "@app/stores/app.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngxs/store";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {PermissionUtil} from "@shared/utils/permission.util";
import {
  AbstractHomeRoutable,
  HomeTileModel,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-home-routable",
  templateUrl: "./admin-home.routable.html",
  styleUrls: ["./admin-home.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminHomeRoutable extends AbstractHomeRoutable {
  protected _userRolesObs: Enums.UserApplicationRole.UserApplicationRoleEnum[] = MemoizedUtil.selectSnapshot(this._store, AppState, state => state.userRoles);

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  _tiles: HomeTileModel[] = [
    {
      avatarIcon: IconNameEnum.institutions,
      titleToTranslate: LabelTranslateEnum.institutions,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("admin.institution.home.subtitle"),
      path: RoutesEnum.adminInstitution,
      permission: ApplicationRolePermissionEnum.noPermission,
    },
    {
      avatarIcon: IconNameEnum.project,
      titleToTranslate: LabelTranslateEnum.projects,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("admin.project.home.subtitle"),
      path: RoutesEnum.adminProject,
      permission: ApplicationRolePermissionEnum.noPermission,
      dataTest: DataTestEnum.adminTileProject,
    },
    {
      avatarIcon: IconNameEnum.fundingAgencies,
      titleToTranslate: LabelTranslateEnum.fundingAgencies,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("admin.funding-agencies.home.subtitle"),
      path: RoutesEnum.adminFundingAgencies,
      permission: ApplicationRolePermissionEnum.adminPermission,
    },
    {
      avatarIcon: IconNameEnum.ontologies,
      titleToTranslate: LabelTranslateEnum.ontologies,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("admin.ontology.home.subtitle"),
      path: RoutesEnum.adminOntology,
      permission: ApplicationRolePermissionEnum.adminPermission,
    },
    {
      avatarIcon: IconNameEnum.researchObjects,
      titleToTranslate: LabelTranslateEnum.researchObjectTypes,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("admin.researchObjectType.home.subtitle"),
      path: RoutesEnum.adminResearchObjectType,
      permission: ApplicationRolePermissionEnum.adminPermission,
    },
    {
      avatarIcon: IconNameEnum.rml,
      titleToTranslate: LabelTranslateEnum.rmls,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("admin.rml.home.subtitle"),
      path: RoutesEnum.adminRml,
      permission: ApplicationRolePermissionEnum.adminPermission,
    },
    {
      avatarIcon: IconNameEnum.collection,
      titleToTranslate: LabelTranslateEnum.iiifCollectionSettings,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("admin.iiifCollectionSettings.home.subtitle"),
      path: RoutesEnum.adminIIIFCollectionSettings,
      permission: ApplicationRolePermissionEnum.adminPermission,
    },
    {
      avatarIcon: IconNameEnum.oaiMetadataPrefixes,
      titleToTranslate: this.labelTranslateInterface.oaiPmhOaiMetadataPrefixTileTitle,
      subtitleToTranslate: this.labelTranslateInterface.oaiPmhOaiMetadataPrefixTileSubtitle,
      path: environment.routeAdminOaiMetadataPrefix,
      permission: ApplicationRolePermissionEnum.adminPermission,
    },
    {
      avatarIcon: IconNameEnum.oaiSets,
      titleToTranslate: this.labelTranslateInterface.oaiPmhOaiSetTileTitle,
      subtitleToTranslate: this.labelTranslateInterface.oaiPmhOaiSetTileSubtitle,
      path: environment.routeAdminOaiSet,
      permission: ApplicationRolePermissionEnum.adminPermission,
    },
    {
      avatarIcon: IconNameEnum.indexFieldAliases,
      titleToTranslate: this.labelTranslateInterface.searchIndexFieldAliasTileTitle,
      subtitleToTranslate: this.labelTranslateInterface.searchIndexFieldAliasTileSubtitle,
      path: environment.routeAdminIndexFieldAlias,
      permission: ApplicationRolePermissionEnum.rootPermission,
    },
    {
      avatarIcon: IconNameEnum.globalBanners,
      titleToTranslate: this.labelTranslateInterface.globalBannerAdminTileTitle,
      subtitleToTranslate: this.labelTranslateInterface.globalBannerAdminTileSubtitle,
      path: environment.routeAdminGlobalBanner,
      permission: ApplicationRolePermissionEnum.adminPermission,
    },
    {
      avatarIcon: IconNameEnum.licenses,
      titleToTranslate: LabelTranslateEnum.licenses,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("admin.license.home.subtitle"),
      path: RoutesEnum.adminLicense,
      permission: ApplicationRolePermissionEnum.noPermission,
    },
    {
      avatarIcon: IconNameEnum.users,
      titleToTranslate: LabelTranslateEnum.users,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("admin.user.home.subtitle"),
      path: RoutesEnum.adminUser,
      permission: ApplicationRolePermissionEnum.rootPermission,
      dataTest: DataTestEnum.adminTileUsers,
    },
    {
      avatarIcon: IconNameEnum.roles,
      titleToTranslate: LabelTranslateEnum.roles,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("admin.roles.home.subtitle"),
      path: RoutesEnum.adminRole,
      permission: ApplicationRolePermissionEnum.rootPermission,
    },
    {
      avatarIcon: IconNameEnum.peoples,
      titleToTranslate: LabelTranslateEnum.people,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("admin.person.home.subtitle"),
      path: RoutesEnum.adminPerson,
      permission: ApplicationRolePermissionEnum.adminPermission,
    },
    {
      avatarIcon: IconNameEnum.languages,
      titleToTranslate: LabelTranslateEnum.languages,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("admin.language.home.subtitle"),
      path: RoutesEnum.adminLanguage,
      permission: ApplicationRolePermissionEnum.rootPermission,
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _translate: TranslateService,
              protected readonly _injector: Injector) {
    super(_store, _translate, _injector);
  }

  protected _displayTile(tile: HomeTileModel): boolean {
    return PermissionUtil.isUserHavePermission(true, tile.permission as ApplicationRolePermissionEnum, this._userRolesObs);
  }
}
