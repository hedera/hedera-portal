/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-language-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {AdminLanguageCreateEditDialog} from "@admin/language/components/dialogs/admin-language-create-edit/admin-language-create-edit.dialog";
import {adminLanguageActionNameSpace} from "@admin/language/stores/admin-language.action";
import {AdminLanguageStateModel} from "@admin/language/stores/admin-language.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Language} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  AbstractListRoutable,
  DataTableFieldTypeEnum,
  DialogUtil,
  isTrue,
  isTruthyObject,
  OrderEnum,
  RouterExtensionService,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-language-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./admin-language-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminLanguageListRoutable extends AbstractListRoutable<Language, AdminLanguageStateModel> {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToAdmin;
  readonly KEY_PARAM_NAME: keyof Language & string = "code";

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.admin_language, adminLanguageActionNameSpace, _injector, {}, StateEnum.admin);
  }

  conditionDisplayEditButton(model: Language | undefined): boolean {
    return false;
  }

  conditionDisplayDeleteButton(model: Language | undefined): boolean {
    return true;
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "resId",
        header: LabelTranslateEnum.identifier,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        width: "100px",
      },
      {
        field: "code",
        header: LabelTranslateEnum.code,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
    ];
  }

  override create(element: ElementRef): void {
    this.subscribe(DialogUtil.open(this._dialog, AdminLanguageCreateEditDialog, {
      current: undefined,
    }, {
      minWidth: "500px",
    }, isConfirmed => {
      if (isTrue(isConfirmed)) {
        if (isTruthyObject(element)) {
          element.nativeElement.focus();
        }
        this.getAll();
      }
    }, () => {
      if (isTruthyObject(element)) {
        element.nativeElement.focus();
      }
    }));
  }

  override goToEdit(model: Language): void {
  }

  override showDetail(model: Language): void {
  }
}
