/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-language-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {AppState} from "@app/stores/app.state";
import {Language} from "@models";
import {Store} from "@ngxs/store";
import {SharedAbstractFormPresentational} from "@shared/components/presentationals/shared-abstract-form/shared-abstract.presentational";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {AdminLanguage} from "@shared/models/admin-language.model";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {RegionUtil} from "@shared/utils/region.util";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  MemoizedUtil,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-subject-area-form",
  templateUrl: "./admin-language-form.presentational.html",
  styleUrls: ["./admin-language-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminLanguageFormPresentational extends SharedAbstractFormPresentational<Language> implements OnInit {
  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  listLanguages: AdminLanguage[] = RegionUtil.getLanguagesWithIsoCode(MemoizedUtil.selectSnapshot(this._storeNgxs, AppState, state => state.appLanguage));

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              private readonly _storeNgxs: Store) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribe(this.form.get(this.formDefinition.resId).valueChanges.pipe(
      distinctUntilChanged(),
      tap((resId: string) => {
        this.form.get(this.formDefinition.code).setValue(resId.toLocaleLowerCase());
      }),
    ));
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.code]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.resId]: ["", [Validators.required, SolidifyValidator]],
    });
  }

  protected _bindFormTo(languages: Language): void {
    this.form = this._fb.group({
      [this.formDefinition.code]: [languages.code, [Validators.required, SolidifyValidator]],
      [this.formDefinition.resId]: [languages.resId, [Validators.required, SolidifyValidator]],
    });
  }

  override onSubmit(): void {
    const model = this._treatmentBeforeSubmit(this.form.value as Language);
    this._submitBS.next({
      model: model,
      formControl: this.form,
      changeDetectorRef: this._changeDetectorRef,
    });
  }

  protected _treatmentBeforeSubmit(languages: Language): Language {
    return languages;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() code: string;
  @PropertyName() resId: string;
}
