/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-language.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {AdminLanguageRoutingModule} from "@admin/language/admin-language-routing.module";
import {AdminLanguageCreateEditDialog} from "@admin/language/components/dialogs/admin-language-create-edit/admin-language-create-edit.dialog";
import {AdminLanguageFormPresentational} from "@admin/language/components/presentationals/admin-language-form/admin-language-form.presentational";
import {AdminLanguageListRoutable} from "@admin/language/components/routables/admin-language-list/admin-language-list.routable";
import {AdminLanguageState} from "@admin/language/stores/admin-language.state";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

const routables = [
  AdminLanguageListRoutable,
];
const containers = [];
const dialogs = [
  AdminLanguageCreateEditDialog,
];
const presentationals = [
  AdminLanguageFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminLanguageRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminLanguageState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminLanguageModule {
}
