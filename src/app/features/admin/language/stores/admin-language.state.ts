/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-language.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminLanguageAction,
  adminLanguageActionNameSpace,
} from "@admin/language/stores/admin-language.action";
import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {Language} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {
  ApiService,
  defaultResourceStateInitValue,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ofSolidifyActionCompleted,
  ResourceState,
  ResourceStateModel,
  SharedLanguageAction,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";

export interface AdminLanguageStateModel extends ResourceStateModel<Language> {

}

@Injectable()
@State<AdminLanguageStateModel>({
  name: StateEnum.admin_language,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
})
export class AdminLanguageState extends ResourceState<AdminLanguageStateModel, Language> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: adminLanguageActionNameSpace,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminLanguage,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.language.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.language.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.language.notification.resource.update"),
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminLanguages;
  }

  @Selector()
  static isLoading(state: AdminLanguageStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminLanguageStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentTitle(state: AdminLanguageStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.code;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminLanguageStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminLanguageStateModel): boolean {
    return true;
  }

  @Action(AdminLanguageAction.LoadResource)
  loadResource(ctx: SolidifyStateContext<AdminLanguageStateModel>, action: AdminLanguageAction.LoadResource): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new SharedLanguageAction.GetAll(null, false, false),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedLanguageAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedLanguageAction.GetAllFail)),
        ],
      },
    ]).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new AdminLanguageAction.LoadResourceSuccess(action));
        } else {
          ctx.dispatch(new AdminLanguageAction.LoadResourceFail(action));
        }
        return result.success;
      }),
    );
  }
}
