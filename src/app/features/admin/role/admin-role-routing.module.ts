/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-role-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {AdminRoleDetailEditRoutable} from "@admin/role/components/routables/admin-role-detail-edit/admin-role-detail-edit.routable";
import {AdminRoleListRoutable} from "@admin/role/components/routables/admin-role-list/admin-role-list.routable";
import {AdminRoleState} from "@admin/role/stores/admin-role.state";
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AdminRoutesEnum,
  AppRoutesEnum,
} from "@shared/enums/routes.enum";
import {HederaRoutes} from "@shared/models/hedera-route.model";
import {
  CanDeactivateGuard,
  EmptyContainer,
} from "solidify-frontend";

const routes: HederaRoutes = [
  {
    path: AppRoutesEnum.root,
    component: AdminRoleListRoutable,
    data: {},
  },
  {
    path: AdminRoutesEnum.roleDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: AdminRoleDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: AdminRoleState.currentTitle,
    },
    children: [
      {
        path: AdminRoutesEnum.roleEdit,
        component: EmptyContainer,
        data: {
          breadcrumb: LabelTranslateEnum.edit,
        },
        canDeactivate: [CanDeactivateGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoleRoutingModule {
}
