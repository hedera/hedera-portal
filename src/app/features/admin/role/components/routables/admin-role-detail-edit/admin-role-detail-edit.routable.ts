/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-role-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {adminRoleActionNameSpace} from "@admin/role/stores/admin-role.action";
import {
  AdminRoleState,
  AdminRoleStateModel,
} from "@admin/role/stores/admin-role.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Role} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {sharedRoleActionNameSpace} from "@shared/stores/role/shared-role.action";
import {Observable} from "rxjs";
import {
  AbstractDetailEditCommonRoutable,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-role-detail-edit-routable",
  templateUrl: "./admin-role-detail-edit.routable.html",
  styleUrls: ["./admin-role-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminRoleDetailEditRoutable extends AbstractDetailEditCommonRoutable<Role, AdminRoleStateModel> {
  @Select(AdminRoleState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminRoleState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  readonly KEY_PARAM_NAME: keyof Role & string = "name";

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedRoleActionNameSpace;

  override readonly deleteAvailable: boolean = false;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_role, _injector, adminRoleActionNameSpace, StateEnum.admin);
  }

  _getSubResourceWithParentId(id: string): void {
  }
}
