/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-role.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {AdminRoleRoutingModule} from "@admin/role/admin-role-routing.module";
import {AdminRoleFormPresentational} from "@admin/role/components/presentationals/admin-role-form/admin-role-form.presentational";
import {AdminRoleDetailEditRoutable} from "@admin/role/components/routables/admin-role-detail-edit/admin-role-detail-edit.routable";
import {AdminRoleListRoutable} from "@admin/role/components/routables/admin-role-list/admin-role-list.routable";
import {AdminRoleState} from "@admin/role/stores/admin-role.state";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

const routables = [
  AdminRoleListRoutable,
  AdminRoleDetailEditRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminRoleFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminRoleRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminRoleState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminRoleModule {
}
