/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-project-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {PersonRole} from "@admin/models/person-role.model";
import {adminProjectActionNameSpace} from "@admin/project/stores/admin-project.action";
import {
  AdminProjectState,
  AdminProjectStateModel,
} from "@admin/project/stores/admin-project.state";
import {AdminProjectFundingAgencyAction} from "@admin/project/stores/funding-agency/admin-project-funding-agency.action";
import {AdminProjectFundingAgencyState} from "@admin/project/stores/funding-agency/admin-project-funding-agency.state";
import {AdminProjectInstitutionAction} from "@admin/project/stores/institution/admin-project-institution.action";
import {AdminProjectInstitutionState} from "@admin/project/stores/institution/admin-project-institution.state";
import {AdminProjectPersonRoleAction} from "@admin/project/stores/project-person-role/admin-project-person-role.action";
import {AdminProjectPersonRoleState} from "@admin/project/stores/project-person-role/admin-project-person-role.state";
import {AdminProjectResearchObjectTypeAction} from "@admin/project/stores/research-object-type/admin-project-research-object-type.action";
import {AdminProjectResearchObjectTypeState} from "@admin/project/stores/research-object-type/admin-project-research-object-type.state";
import {AdminProjectRmlAction} from "@admin/project/stores/rml/admin-project-rml.action";
import {AdminProjectRmlState} from "@admin/project/stores/rml/admin-project-rml.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  FundingAgency,
  Institution,
  Project,
  ResearchObjectType,
  Rml,
  Role,
  SystemProperty,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {sharedProjectActionNameSpace} from "@shared/stores/project/shared-project.action";
import {SharedRoleState} from "@shared/stores/role/shared-role.state";
import {Observable} from "rxjs";
import {
  AbstractDetailEditCommonRoutable,
  AppSystemPropertyState,
  ExtraButtonToolbar,
  MemoizedUtil,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "hedera-project-detail-edit-routable",
  templateUrl: "./admin-project-detail-edit.routable.html",
  styleUrls: ["./admin-project-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminProjectDetailEditRoutable extends AbstractDetailEditCommonRoutable<Project, AdminProjectStateModel> implements OnInit {
  @Select(AdminProjectState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminProjectState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;
  systemPropertyObs: Observable<SystemProperty> = MemoizedUtil.select(this._store, AppSystemPropertyState, state => state.current);
  selectedFundingAgenciesObs: Observable<FundingAgency[]> = MemoizedUtil.selected(this._store, AdminProjectFundingAgencyState);
  selectedInstitutionsObs: Observable<Institution[]> = MemoizedUtil.selected(this._store, AdminProjectInstitutionState);
  listRoleObs: Observable<Role[]> = MemoizedUtil.list(this._store, SharedRoleState);
  selectedPersonRoleObs: Observable<PersonRole[]> = MemoizedUtil.selected(this._store, AdminProjectPersonRoleState);
  selectedRmlObs: Observable<Rml[]> = MemoizedUtil.selected(this._store, AdminProjectRmlState);
  selectedResearchObjectTypeObs: Observable<ResearchObjectType[]> = MemoizedUtil.selected(this._store, AdminProjectResearchObjectTypeState);

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedProjectActionNameSpace;

  readonly KEY_PARAM_NAME: keyof Project & string = "name";

  listExtraButtons: ExtraButtonToolbar<Project> [] = [
    {
      color: "primary",
      icon: IconNameEnum.browse,
      callback: current => this._navigateToBrowse(),
      labelToTranslate: current => LabelTranslateEnum.browse,
      order: 30,
    },
    {
      color: "primary",
      icon: IconNameEnum.ingest,
      callback: current => this._navigateToIngest(),
      labelToTranslate: current => LabelTranslateEnum.toImport,
      order: 31,
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_project, _injector, adminProjectActionNameSpace, StateEnum.admin);
    this.deleteAvailable = this._securityService.isRootOrAdmin();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.editAvailable = this._securityService.isRootOrAdmin() || this._securityService.isManagerOfProject(this._resId);
    this._changeDetector.detectChanges();
  }

  _getSubResourceWithParentId(id: string): void {
    this._store.dispatch(new AdminProjectPersonRoleAction.GetAll(id));
    this._store.dispatch(new AdminProjectFundingAgencyAction.GetAll(id));
    this._store.dispatch(new AdminProjectInstitutionAction.GetAll(id));
    this._store.dispatch(new AdminProjectRmlAction.GetAll(id));
    this._store.dispatch(new AdminProjectResearchObjectTypeAction.GetAll(id));
  }

  private _navigateToBrowse(): void {
    this._store.dispatch(new Navigate([RoutesEnum.browseProjectDetail, this._resId]));
  }

  private _navigateToIngest(): void {
    this._store.dispatch(new Navigate([RoutesEnum.ingest, this._resId]));
  }
}
