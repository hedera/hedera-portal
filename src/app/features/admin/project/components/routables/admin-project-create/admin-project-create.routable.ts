/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-project-create.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  AbstractCreateRoutable,
  AppSystemPropertyState,
  MemoizedUtil,
  ResourceNameSpace,
} from "solidify-frontend";
import {FundingAgency, Institution, Project, ResearchObjectType, Rml, Role, SystemProperty} from "@models";
import {AdminProjectState, AdminProjectStateModel} from "@admin/project/stores/admin-project.state";
import {adminProjectActionNameSpace} from "@admin/project/stores/admin-project.action";
import {sharedProjectActionNameSpace} from "@shared/stores/project/shared-project.action";
import {SharedRoleState} from "@shared/stores/role/shared-role.state";
import {PersonRole} from "@admin/models/person-role.model";
import {AdminProjectInstitutionState} from "@admin/project/stores/institution/admin-project-institution.state";
import {AdminProjectFundingAgencyState} from "@admin/project/stores/funding-agency/admin-project-funding-agency.state";
import {AdminProjectRmlState} from "@admin/project/stores/rml/admin-project-rml.state";
import {AdminProjectResearchObjectTypeState} from "@admin/project/stores/research-object-type/admin-project-research-object-type.state";

@Component({
  selector: "hedera-admin-project-create-routable",
  templateUrl: "./admin-project-create.routable.html",
  styleUrls: ["./admin-project-create.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminProjectCreateRoutable extends AbstractCreateRoutable<Project, AdminProjectStateModel> {
  @Select(AdminProjectState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminProjectState.isReadyToBeDisplayedInCreateMode) isReadyToBeDisplayedInCreateModeObs: Observable<boolean>;
  systemPropertyObs: Observable<SystemProperty> = MemoizedUtil.select(this._store, AppSystemPropertyState, state => state.current);
  listRoleObs: Observable<Role[]> = MemoizedUtil.list(this._store, SharedRoleState);
  selectedInstitutionsObs: Observable<Institution[]> = MemoizedUtil.selected(this._store, AdminProjectInstitutionState);
  selectedFundingAgenciesObs: Observable<FundingAgency[]> = MemoizedUtil.selected(this._store, AdminProjectFundingAgencyState);
  selectedRmlObs: Observable<Rml[]> = MemoizedUtil.selected(this._store, AdminProjectRmlState);
  selectedResearchObjectTypeObs: Observable<ResearchObjectType[]> = MemoizedUtil.selected(this._store, AdminProjectResearchObjectTypeState);

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedProjectActionNameSpace;
  selectedPersonRole: PersonRole[] = [];

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector) {
    super(_store, _actions$, _changeDetector, StateEnum.admin_project, _injector, adminProjectActionNameSpace, StateEnum.admin);
  }

}
