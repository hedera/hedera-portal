/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-project-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {PersonRole} from "@admin/models/person-role.model";
import {AdminProjectAction} from "@admin/project/stores/admin-project.action";
import {AdminProjectState} from "@admin/project/stores/admin-project.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
  ViewChild,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {Enums} from "@enums";
import {
  FundingAgency,
  Institution,
  License,
  Project,
  ResearchObjectType,
  Rml,
  Role,
} from "@models";
import {SharedAbstractFormPresentational} from "@shared/components/presentationals/shared-abstract-form/shared-abstract.presentational";
import {SharedFundingAgencyOverlayPresentational} from "@shared/components/presentationals/shared-funding-agency-overlay/shared-funding-agency-overlay.presentational";
import {SharedInstitutionOverlayPresentational} from "@shared/components/presentationals/shared-institution-overlay/shared-institution-overlay.presentational";
import {SharedLicenseOverlayPresentational} from "@shared/components/presentationals/shared-license-overlay/shared-license-overlay.presentational";
import {SharedResearchObjectTypeOverlayPresentational} from "@shared/components/presentationals/shared-research-object-type-overlay/shared-research-object-type-overlay.presentational";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {sharedFundingAgencyActionNameSpace} from "@shared/stores/funding-agency/shared-funding-agency.action";
import {SharedFundingAgencyState} from "@shared/stores/funding-agency/shared-funding-agency.state";
import {sharedInstitutionActionNameSpace} from "@shared/stores/institution/shared-institution.action";
import {SharedInstitutionState} from "@shared/stores/institution/shared-institution.state";
import {sharedLicenseActionNameSpace} from "@shared/stores/license/shared-license.action";
import {
  SharedLicenseState,
  SharedLicenseStateModel,
} from "@shared/stores/license/shared-license.state";
import {sharedResearchObjectTypeActionNameSpace} from "@shared/stores/research-object-type/shared-research-object-type.action";
import {SharedResearchObjectTypeState} from "@shared/stores/research-object-type/shared-research-object-type.state";
import {sharedRmlActionNameSpace} from "@shared/stores/rml/shared-rml.action";
import {SharedRmlState} from "@shared/stores/rml/shared-rml.state";
import {
  BreakpointService,
  DateUtil,
  isArray,
  isEmptyString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  KeyValue,
  OrderEnum,
  PropertyName,
  RegexUtil,
  ResourceFileNameSpace,
  ResourceNameSpace,
  SearchableSingleSelectPresentational,
  SolidifyValidator,
  Sort,
  Type,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-project-form",
  templateUrl: "./admin-project-form.presentational.html",
  styleUrls: ["./admin-project-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminProjectFormPresentational extends SharedAbstractFormPresentational<Project> {
  readonly classIgnoreEditField: string = this.environment.classInputIgnored;
  defaultRole: Enums.Role.RoleEnum = Enums.Role.RoleEnum.VISITOR;

  @Input()
  defaultPlatformLicenseId: string;

  @Input()
  selectedFundingAgencies: FundingAgency[];

  @Input()
  selectedInstitutions: Institution[];

  @Input()
  selectedRmls: Rml[];

  @Input()
  selectedResearchObjectTypes: ResearchObjectType[];

  @Input()
  listRole: Role[];

  @Input()
  selectedPersonRole: PersonRole[];

  @Input()
  defaultCopyrightHolder: string;

  @ViewChild("licenseSingleSearchableSelect")
  licenseSingleSearchableSelect: SearchableSingleSelectPresentational<SharedLicenseStateModel, License>;

  get dataTestEnum(): typeof DataTestEnum {
    return DataTestEnum;
  }

  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  listStorageType: KeyValue[] = Enums.Project.StorageTypeEnumTranslate;

  adminProjectActionNamespace: ResourceFileNameSpace = AdminProjectAction;
  adminProjectState: typeof AdminProjectState = AdminProjectState;

  sharedInstitutionSort: Sort<Institution> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedInstitutionActionNameSpace: ResourceNameSpace = sharedInstitutionActionNameSpace;
  sharedInstitutionState: typeof SharedInstitutionState = SharedInstitutionState;

  sharedFundingAgencySort: Sort<FundingAgency> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedFundingAgencyActionNameSpace: ResourceNameSpace = sharedFundingAgencyActionNameSpace;
  sharedFundingAgencyState: typeof SharedFundingAgencyState = SharedFundingAgencyState;

  sharedLicenseSort: Sort<License> = {
    field: "title",
    order: OrderEnum.ascending,
  };
  sharedLicenseActionNameSpace: ResourceNameSpace = sharedLicenseActionNameSpace;
  sharedLicenseState: typeof SharedLicenseState = SharedLicenseState;

  sharedRmlSort: Sort<Rml> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedRmlActionNameSpace: ResourceNameSpace = sharedRmlActionNameSpace;
  sharedRmlState: typeof SharedRmlState = SharedRmlState;

  sharedResearchObjectTypeSort: Sort<Rml> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedResearchObjectTypeActionNameSpace: ResourceNameSpace = sharedResearchObjectTypeActionNameSpace;
  sharedResearchObjectTypeState: typeof SharedResearchObjectTypeState = SharedResearchObjectTypeState;
  researchObjectTypeCallback: (value: ResearchObjectType) => string = (value: ResearchObjectType) => value.name;
  researchObjectTypeCallbackSecondLine: (value: ResearchObjectType) => string = (value: ResearchObjectType) => `${value.ontology.name} (${value.ontology.version}) : ${value.rdfType}`;

  licenseCallback: (value: License) => string = (value: License) => value.openLicenseId + " (" + value.title + ")";

  institutionOverlayComponent: Type<SharedInstitutionOverlayPresentational> = SharedInstitutionOverlayPresentational;
  fundingAgencyOverlayComponent: Type<SharedFundingAgencyOverlayPresentational> = SharedFundingAgencyOverlayPresentational;
  licenseOverlayComponent: Type<SharedLicenseOverlayPresentational> = SharedLicenseOverlayPresentational;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  iiifManifestResearchObjectTypeOverlayData: ResearchObjectType;
  researchDataFileResearchObjectTypeOverlayData: ResearchObjectType;

  researchObjectTypeOverlayComponent: Type<SharedResearchObjectTypeOverlayPresentational> = SharedResearchObjectTypeOverlayPresentational;

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              readonly breakpointService: BreakpointService) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: ["", [SolidifyValidator]],
      [this.formDefinition.openingDate]: [""],
      [this.formDefinition.closingDate]: [""],
      [this.formDefinition.url]: ["", [SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.personRole]: ["", [SolidifyValidator]],
      [this.formDefinition.fundingAgencies]: [[], [SolidifyValidator]],
      [this.formDefinition.institutions]: [[], [SolidifyValidator]],
      [this.formDefinition.keywords]: [[], [SolidifyValidator]],
      [this.formDefinition.defaultLicense]: ["", [SolidifyValidator]],
      [this.formDefinition.rmls]: [[], [SolidifyValidator]],
      [this.formDefinition.researchObjectTypes]: [[], [SolidifyValidator]],
      [this.formDefinition.accessPublic]: [true, [Validators.required, SolidifyValidator]],
      [this.formDefinition.hasData]: [false, [SolidifyValidator]],
      [this.formDefinition.iiifManifestSparqlQuery]: ["", [SolidifyValidator]],
      [this.formDefinition.storageType]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.shortName]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.copyrightHolder]: [this.defaultCopyrightHolder, [Validators.required, SolidifyValidator]],
      [this.formDefinition.iiifManifestResearchObjectType]: ["", [SolidifyValidator]],
      [this.formDefinition.researchDataFileResearchObjectType]: ["", [SolidifyValidator]],
    });
  }

  protected _bindFormTo(project: Project): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: [project.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: [project.description, [SolidifyValidator]],
      [this.formDefinition.openingDate]: [project.openingDate, [SolidifyValidator]],
      [this.formDefinition.closingDate]: [project.closingDate, [SolidifyValidator]],
      [this.formDefinition.url]: [project.url, [SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.personRole]: ["", [SolidifyValidator]],
      [this.formDefinition.fundingAgencies]: [this.selectedFundingAgencies.map(i => i.resId), [SolidifyValidator]],
      [this.formDefinition.institutions]: [this.selectedInstitutions.map(i => i.resId), [SolidifyValidator]],
      [this.formDefinition.keywords]: [[...project.keywords], [SolidifyValidator]],
      [this.formDefinition.defaultLicense]: [project?.defaultLicense?.resId, [SolidifyValidator]],
      [this.formDefinition.rmls]: [this.selectedRmls.map(i => i.resId), [SolidifyValidator]],
      [this.formDefinition.researchObjectTypes]: [this.selectedResearchObjectTypes.map(i => i.resId), [SolidifyValidator]],
      [this.formDefinition.accessPublic]: [project.accessPublic, [Validators.required, SolidifyValidator]],
      [this.formDefinition.hasData]: [project.hasData, [SolidifyValidator]],
      [this.formDefinition.iiifManifestSparqlQuery]: [project.iiifManifestSparqlQuery, [SolidifyValidator]],
      [this.formDefinition.storageType]: [project.storageType, [Validators.required, SolidifyValidator]],
      [this.formDefinition.shortName]: [project.shortName, [Validators.required, SolidifyValidator]],
      [this.formDefinition.copyrightHolder]: [project.copyrightHolder, [Validators.required, SolidifyValidator]],
      [this.formDefinition.iiifManifestResearchObjectType]: [project?.iiifManifestResearchObjectType?.resId, [SolidifyValidator]],
      [this.formDefinition.researchDataFileResearchObjectType]: [project?.researchDataFileResearchObjectType?.resId, [SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(project: Project): Project {
    project.openingDate = DateUtil.convertToLocalDateDateSimple(project.openingDate);
    project.closingDate = DateUtil.convertToLocalDateDateSimple(project.closingDate);

    const fundingAgencies = this.form.get(this.formDefinition.fundingAgencies).value;
    project.fundingAgencies = isArray(fundingAgencies) ? fundingAgencies.map(id => ({resId: id})) : [];

    const institutions = this.form.get(this.formDefinition.institutions).value;
    project.institutions = isArray(institutions) ? institutions.map(id => ({resId: id})) : [];

    const rmls = this.form.get(this.formDefinition.rmls).value;
    project.rmls = isArray(rmls) ? rmls.map(id => ({resId: id})) : [];

    const researchObjectTypes = this.form.get(this.formDefinition.researchObjectTypes).value;
    project.researchObjectTypes = isArray(researchObjectTypes) ? researchObjectTypes.map(id => ({resId: id})) : [];

    const defaultLicenseResId = this.form.get(this.formDefinition.defaultLicense).value;
    if (this.licenseSingleSearchableSelect.isDefaultValueSelected || isNullOrUndefined(defaultLicenseResId) || isEmptyString(defaultLicenseResId)) {
      project.defaultLicense = null;
    } else {
      project.defaultLicense = {resId: defaultLicenseResId};
    }

    const iiifManifestResearchObjectTypeResId = this.form.get(this.formDefinition.iiifManifestResearchObjectType).value;
    if (isNullOrUndefinedOrWhiteString(iiifManifestResearchObjectTypeResId)) {
      project.iiifManifestResearchObjectType = null;
    } else {
      project.iiifManifestResearchObjectType = {
        resId: iiifManifestResearchObjectTypeResId,
        name: null,
        rdfType: null,
        sparqlListQuery: null,
      };
    }

    const researchDataFileResearchObjectTypeId = this.form.get(this.formDefinition.researchDataFileResearchObjectType).value;
    if (isNullOrUndefinedOrWhiteString(researchDataFileResearchObjectTypeId)) {
      project.researchDataFileResearchObjectType = null;
    } else {
      project.researchDataFileResearchObjectType = {
        resId: researchDataFileResearchObjectTypeId,
        name: null,
        rdfType: null,
        sparqlListQuery: null,
        ontology: null,
      };
    }

    return project;
  }

  navigateToFundingAgency(fundingAgency: FundingAgency): void {
    this.navigate([RoutesEnum.adminFundingAgenciesDetail, fundingAgency.resId]);
  }

  navigateToInstitution(institution: Institution): void {
    this.navigate([RoutesEnum.adminInstitutionDetail, institution.resId]);
  }

  navigateToLicense(licenseId: string): void {
    this.navigate([RoutesEnum.adminLicenseDetail, licenseId]);
  }

  navigateToRml(rml: Rml): void {
    this.navigate([RoutesEnum.adminRmlDetail, rml.resId]);
  }

  navigateToResearchObject(researchObject: ResearchObjectType): void {
    this.navigate([RoutesEnum.adminResearchObjectTypeDetail, researchObject.resId]);
  }

  navigateToResearchObjectById(researchObjectId: string): void {
    this.navigate([RoutesEnum.adminResearchObjectTypeDetail, researchObjectId]);
  }

  setDefaultCopyrightHolderIfEmpty(): void {
    const fc = this.form.get(this.formDefinition.copyrightHolder);
    if (isNullOrUndefinedOrWhiteString(fc.value)) {
      fc.setValue(this.defaultCopyrightHolder);
    }
  }

  protected override _disableSpecificField(): void {
    this.form.get(this.formDefinition.hasData).disable();
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() description: string;
  @PropertyName() closingDate: string;
  @PropertyName() openingDate: string;
  @PropertyName() url: string;
  @PropertyName() personRole: string;
  @PropertyName() fundingAgencies: string;
  @PropertyName() institutions: string;
  @PropertyName() keywords: string;
  @PropertyName() defaultLicense: string;
  @PropertyName() rmls: string;
  @PropertyName() researchObjectTypes: string;
  @PropertyName() accessPublic: string;
  @PropertyName() hasData: string;
  @PropertyName() iiifManifestSparqlQuery: string;
  @PropertyName() storageType: string;
  @PropertyName() shortName: string;
  @PropertyName() copyrightHolder: string;
  @PropertyName() iiifManifestResearchObjectType: string;
  @PropertyName() researchDataFileResearchObjectType: string;
}
