/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-project-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {ApplicationRolePermissionEnum} from "@shared/enums/application-role-permission.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AdminRoutesEnum,
  AppRoutesEnum,
} from "@shared/enums/routes.enum";
import {ApplicationRoleGuardService} from "@shared/guards/application-role-guard.service";
import {HederaRoutes} from "@shared/models/hedera-route.model";
import {
  CanDeactivateGuard,
  CombinedGuardService,
  EmptyContainer,
} from "solidify-frontend";
import {AdminProjectListRoutable} from "@admin/project/components/routables/admin-project-list/admin-project-list.routable";
import {AdminProjectState} from "@admin/project/stores/admin-project.state";
import {AdminProjectDetailEditRoutable} from "@admin/project/components/routables/admin-project-detail-edit/admin-project-detail-edit.routable";
import {AdminProjectCreateRoutable} from "@admin/project/components/routables/admin-project-create/admin-project-create.routable";

const routes: HederaRoutes = [
  {
    path: AppRoutesEnum.root,
    component: AdminProjectListRoutable,
    data: {},
  },
  {
    path: AdminRoutesEnum.projectCreate,
    component: AdminProjectCreateRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.create,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
    canDeactivate: [CanDeactivateGuard],
  },
  {
    path: AdminRoutesEnum.projectDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: AdminProjectDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: AdminProjectState.currentName,
    },
    children: [
      {
        path: AdminRoutesEnum.projectEdit,
        component: EmptyContainer,
        data: {
          breadcrumb: LabelTranslateEnum.edit,
        },
        canDeactivate: [CanDeactivateGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminProjectRoutingModule {
}
