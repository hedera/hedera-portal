/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-project.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminProjectAction,
  adminProjectActionNameSpace,
} from "@admin/project/stores/admin-project.action";
import {AdminProjectFundingAgencyAction} from "@admin/project/stores/funding-agency/admin-project-funding-agency.action";
import {
  AdminProjectFundingAgencyState,
  AdminProjectFundingAgencyStateModel,
} from "@admin/project/stores/funding-agency/admin-project-funding-agency.state";
import {AdminProjectInstitutionAction} from "@admin/project/stores/institution/admin-project-institution.action";
import {AdminProjectInstitutionState} from "@admin/project/stores/institution/admin-project-institution.state";
import {AdminProjectPersonRoleAction} from "@admin/project/stores/project-person-role/admin-project-person-role.action";
import {
  AdminProjectPersonRoleState,
  AdminProjectPersonRoleStateModel,
  defaultAdminProjectPersonRoleStateModel,
} from "@admin/project/stores/project-person-role/admin-project-person-role.state";
import {AdminProjectResearchObjectTypeAction} from "@admin/project/stores/research-object-type/admin-project-research-object-type.action";
import {
  AdminProjectResearchObjectTypeState,
  AdminProjectResearchObjectTypeStateModel,
} from "@admin/project/stores/research-object-type/admin-project-research-object-type.state";
import {AdminProjectRmlAction} from "@admin/project/stores/rml/admin-project-rml.action";
import {
  AdminProjectRmlState,
  AdminProjectRmlStateModel,
} from "@admin/project/stores/rml/admin-project-rml.state";
import {Injectable} from "@angular/core";
import {AppMemberProjectAction} from "@app/stores/member-project/app-member-project.action";
import {AppUserState} from "@app/stores/user/app-user.state";
import {environment} from "@environments/environment";
import {Project} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {
  ActionSubActionCompletionsWrapper,
  ApiService,
  defaultAssociationStateInitValue,
  defaultResourceFileStateInitValue,
  DispatchMethodEnum,
  DownloadService,
  isArray,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  OverrideDefaultAction,
  Relation3TiersForm,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";

export interface AdminProjectStateModel extends ResourceFileStateModel<Project> {
  admin_project_fundingAgency: AdminProjectFundingAgencyStateModel;
  admin_project_institution: AdminProjectFundingAgencyStateModel;
  admin_project_personRole: AdminProjectPersonRoleStateModel;
  admin_project_rml: AdminProjectRmlStateModel;
  admin_project_researchObjectType: AdminProjectResearchObjectTypeStateModel;
}

@Injectable()
@State<AdminProjectStateModel>({
  name: StateEnum.admin_project,
  defaults: {
    ...defaultResourceFileStateInitValue(),
    admin_project_fundingAgency: {...defaultAssociationStateInitValue()},
    admin_project_institution: {...defaultAssociationStateInitValue()},
    admin_project_personRole: {...defaultAdminProjectPersonRoleStateModel()},
    admin_project_rml: {...defaultAssociationStateInitValue()},
    admin_project_researchObjectType: {...defaultAssociationStateInitValue()},
  },
  children: [
    AdminProjectFundingAgencyState,
    AdminProjectInstitutionState,
    AdminProjectPersonRoleState,
    AdminProjectRmlState,
    AdminProjectResearchObjectTypeState,
  ],
})
export class AdminProjectState extends ResourceFileState<AdminProjectStateModel, Project> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService,
              private readonly _securityService: SecurityService,
  ) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: adminProjectActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminProjectDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminProjectDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminProject,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.project.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.project.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.project.notification.resource.update"),
      updateSubResourceDispatchMethod: DispatchMethodEnum.SEQUENCIAL,
    }, _downloadService, ResourceFileStateModeEnum.logo, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminProjects;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static isLoading(state: AdminProjectStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminProjectStateModel): boolean {
    return this.isLoading(state)
      || StoreUtil.isLoadingState(state.admin_project_institution)
      || StoreUtil.isLoadingState(state.admin_project_fundingAgency)
      || StoreUtil.isLoadingState(state.admin_project_rml)
      || StoreUtil.isLoadingState(state.admin_project_personRole)
      || StoreUtil.isLoadingState(state.admin_project_researchObjectType);
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminProjectStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current)
      && !isNullOrUndefined(state.admin_project_fundingAgency.selected)
      && !isNullOrUndefined(state.admin_project_institution.selected)
      && !isNullOrUndefined(state.admin_project_rml.selected)
      && !isNullOrUndefined(state.admin_project_researchObjectType.selected)
      && !isNullOrUndefined(state.admin_project_personRole);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminProjectStateModel): boolean {
    return true;
  }

  @Selector()
  static currentName(state: AdminProjectStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @OverrideDefaultAction()
  @Action(AdminProjectAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<AdminProjectStateModel>, action: AdminProjectAction.GetByIdSuccess): void {
    super.getByIdSuccess(ctx, action);
    if (isNotNullNorUndefined(action.model.logo)) {
      ctx.dispatch(new AdminProjectAction.GetFile(action.model.resId));
    }
  }

  protected override _getListActionsUpdateSubResource(model: Project, action: AdminProjectAction.Create | AdminProjectAction.Update, ctx: SolidifyStateContext<AdminProjectStateModel>): ActionSubActionCompletionsWrapper[] {
    const actions = super._getListActionsUpdateSubResource(model, action, ctx);
    const projectId = model.resId;

    const newFundingAgencyIds = action.modelFormControlEvent.model.fundingAgencies.map(p => p.resId) ?? [];

    const newInstitutionsIds = action.modelFormControlEvent.model.institutions.map(p => p.resId) ?? [];

    const newRmlsIds = action.modelFormControlEvent.model.rmls.map(p => p.resId) ?? [];

    const newResearchObjectTypesIds = action.modelFormControlEvent.model.researchObjectTypes.map(p => p.resId) ?? [];

    let concernCurrentUser = false;
    const currentPersonId = MemoizedUtil.currentSnapshot(this._store, AppUserState)?.person?.resId;

    const oldListPersonsRoles = MemoizedUtil.selectedSnapshot(this._store, AdminProjectPersonRoleState);
    if (isNotNullNorUndefined(oldListPersonsRoles) && oldListPersonsRoles.findIndex(p => p.resId === currentPersonId) >= 0) {
      concernCurrentUser = true;
    }

    let newPersonRole = action.modelFormControlEvent.formControl.get("personRole").value as Relation3TiersForm[];
    newPersonRole = newPersonRole.filter(p => isNotNullNorUndefinedNorWhiteString(p.id));
    newPersonRole.forEach((elem) => {
      if (elem.id === currentPersonId) {
        concernCurrentUser = true;
      }
      if (!isNullOrUndefined(elem.listId) && !isArray(elem.listId)) {
        elem.listId = [elem.listId];
      }
    });

    actions.push(...[
      {
        action: new AdminProjectFundingAgencyAction.Update(projectId, newFundingAgencyIds),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminProjectFundingAgencyAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminProjectFundingAgencyAction.UpdateFail)),
        ],
      },
      {
        action: new AdminProjectInstitutionAction.Update(projectId, newInstitutionsIds),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminProjectInstitutionAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminProjectInstitutionAction.UpdateFail)),
        ],
      },
      {
        action: new AdminProjectRmlAction.Update(projectId, newRmlsIds),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminProjectRmlAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminProjectRmlAction.UpdateFail)),
        ],
      },
      {
        action: new AdminProjectResearchObjectTypeAction.Update(projectId, newResearchObjectTypesIds),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminProjectResearchObjectTypeAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminProjectResearchObjectTypeAction.UpdateFail)),
        ],
      },
      {
        action: new AdminProjectPersonRoleAction.Update(projectId, newPersonRole),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminProjectPersonRoleAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminProjectPersonRoleAction.UpdateFail)),
        ],
      },
    ]);
    if (concernCurrentUser) {
      actions.push(
        {
          action: new AppMemberProjectAction.GetAll(),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(AppMemberProjectAction.GetAllSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(AppMemberProjectAction.GetAllFail)),
          ],
        },
      );

    }
    return actions;
  }
}
