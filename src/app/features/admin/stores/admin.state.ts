/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminFundingAgenciesState,
  AdminFundingAgenciesStateModel,
} from "@admin/funding-agencies/stores/admin-funding-agencies.state";
import {
  AdminIIIFCollectionSettingsState,
  AdminIIIFCollectionSettingsStateModel,
} from "@admin/iiif-collection-settings/stores/admin-iiif-collection-settings.state";
import {
  AdminInstitutionState,
  AdminInstitutionStateModel,
} from "@admin/institution/stores/admin-institution.state";
import {
  AdminLanguageState,
  AdminLanguageStateModel,
} from "@admin/language/stores/admin-language.state";
import {
  AdminLicenseState,
  AdminLicenseStateModel,
} from "@admin/license/stores/admin-license.state";
import {
  AdminOntologyState,
  AdminOntologyStateModel,
} from "@admin/ontology/stores/admin-ontology.state";
import {
  AdminPersonState,
  AdminPersonStateModel,
} from "@admin/person/stores/admin-person.state";
import {
  AdminProjectState,
  AdminProjectStateModel,
} from "@admin/project/stores/admin-project.state";
import {
  AdminResearchObjectTypeState,
  AdminResearchObjectTypeStateModel,
} from "@admin/research-object-type/stores/admin-research-object-type.state";
import {
  AdminRmlState,
  AdminRmlStateModel,
} from "@admin/rml/stores/admin-rml.state";
import {
  AdminRoleState,
  AdminRoleStateModel,
} from "@admin/role/stores/admin-role.state";
import {
  AdminScheduledTaskState,
  AdminScheduledTaskStateModel,
} from "@admin/scheduled-task/stores/admin-scheduled-task.state";
import {
  AdminUserState,
  AdminUserStateModel,
} from "@admin/user/stores/admin-user.state";
import {Injectable} from "@angular/core";
import {
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {
  AdminGlobalBannerState,
  AdminGlobalBannerStateModel,
  AdminIndexFieldAliasState,
  AdminIndexFieldAliasStateModel,
  AdminOaiMetadataPrefixState,
  AdminOaiMetadataPrefixStateModel,
  AdminOaiSetState,
  AdminOaiSetStateModel,
  BaseStateModel,
} from "solidify-frontend";

export interface AdminStateModel extends BaseStateModel {
  [StateEnum.admin_project]: AdminProjectStateModel;
  [StateEnum.admin_license]: AdminLicenseStateModel;
  [StateEnum.admin_globalBanner]: AdminGlobalBannerStateModel;
  [StateEnum.admin_institution]: AdminInstitutionStateModel;
  [StateEnum.admin_user]: AdminUserStateModel;
  [StateEnum.admin_oaiMetadataPrefix]: AdminOaiMetadataPrefixStateModel;
  [StateEnum.admin_oaiSet]: AdminOaiSetStateModel;
  [StateEnum.admin_person]: AdminPersonStateModel;
  [StateEnum.admin_role]: AdminRoleStateModel;
  [StateEnum.admin_fundingAgencies]: AdminFundingAgenciesStateModel;
  [StateEnum.admin_indexFieldAlias]: AdminIndexFieldAliasStateModel;
  [StateEnum.admin_iiifCollectionSettings]: AdminIIIFCollectionSettingsStateModel;
  [StateEnum.admin_language]: AdminLanguageStateModel;
  [StateEnum.admin_ontology]: AdminOntologyStateModel;
  [StateEnum.admin_researchObjectTypes]: AdminResearchObjectTypeStateModel;
  [StateEnum.admin_rml]: AdminRmlStateModel;
  [StateEnum.admin_scheduledTask]: AdminScheduledTaskStateModel;
}

@Injectable()
@State<AdminStateModel>({
  name: StateEnum.admin,
  defaults: {
    isLoadingCounter: 0,
    [StateEnum.admin_license]: null,
    [StateEnum.admin_globalBanner]: null,
    [StateEnum.admin_project]: null,
    [StateEnum.admin_institution]: null,
    [StateEnum.admin_user]: null,
    [StateEnum.admin_oaiMetadataPrefix]: null,
    [StateEnum.admin_oaiSet]: null,
    [StateEnum.admin_person]: null,
    [StateEnum.admin_role]: null,
    [StateEnum.admin_fundingAgencies]: null,
    [StateEnum.admin_indexFieldAlias]: null,
    [StateEnum.admin_iiifCollectionSettings]: null,
    [StateEnum.admin_language]: null,
    [StateEnum.admin_ontology]: null,
    [StateEnum.admin_researchObjectTypes]: null,
    [StateEnum.admin_rml]: null,
    [StateEnum.admin_scheduledTask]: null,
  },
  children: [
    AdminLicenseState,
    AdminGlobalBannerState,
    AdminProjectState,
    AdminInstitutionState,
    AdminUserState,
    AdminOaiMetadataPrefixState,
    AdminOaiSetState,
    AdminPersonState,
    AdminRoleState,
    AdminFundingAgenciesState,
    AdminIndexFieldAliasState,
    AdminIIIFCollectionSettingsState,
    AdminLanguageState,
    AdminOntologyState,
    AdminResearchObjectTypeState,
    AdminRmlState,
    AdminScheduledTaskState,
  ],
})
export class AdminState {
  constructor(protected readonly _store: Store) {
  }
}
