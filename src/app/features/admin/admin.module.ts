/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminFundingAgenciesState} from "@admin/funding-agencies/stores/admin-funding-agencies.state";
import {AdminFundingAgenciesProjectState} from "@admin/funding-agencies/stores/project/admin-project-funding-agencies.state";
import {AdminIIIFCollectionSettingsState} from "@admin/iiif-collection-settings/stores/admin-iiif-collection-settings.state";
import {AdminInstitutionState} from "@admin/institution/stores/admin-institution.state";
import {AdminInstitutionProjectState} from "@admin/institution/stores/project/admin-institution-project.state";
import {AdminLanguageState} from "@admin/language/stores/admin-language.state";
import {AdminLicenseState} from "@admin/license/stores/admin-license.state";
import {AdminOntologyState} from "@admin/ontology/stores/admin-ontology.state";
import {AdminPersonState} from "@admin/person/stores/admin-person.state";
import {AdminPersonInstitutionsState} from "@admin/person/stores/institutions/admin-people-institutions.state";
import {AdminPersonProjectRoleState} from "@admin/person/stores/person-project-role/admin-person-project-role.state";
import {AdminProjectState} from "@admin/project/stores/admin-project.state";
import {AdminProjectFundingAgencyState} from "@admin/project/stores/funding-agency/admin-project-funding-agency.state";
import {AdminProjectInstitutionState} from "@admin/project/stores/institution/admin-project-institution.state";
import {AdminProjectPersonRoleState} from "@admin/project/stores/project-person-role/admin-project-person-role.state";
import {AdminProjectResearchObjectTypeState} from "@admin/project/stores/research-object-type/admin-project-research-object-type.state";
import {AdminProjectRmlState} from "@admin/project/stores/rml/admin-project-rml.state";
import {AdminResearchObjectTypeState} from "@admin/research-object-type/stores/admin-research-object-type.state";
import {AdminRmlState} from "@admin/rml/stores/admin-rml.state";
import {AdminRoleState} from "@admin/role/stores/admin-role.state";
import {AdminScheduledTaskState} from "@admin/scheduled-task/stores/admin-scheduled-task.state";
import {AdminUserState} from "@admin/user/stores/admin-user.state";
import {NgModule} from "@angular/core";
import {AdminRoutingModule} from "@app/features/admin/admin-routing.module";
import {AdminHomeRoutable} from "@app/features/admin/components/routables/admin-home/admin-home.routable";
import {AdminState} from "@app/features/admin/stores/admin.state";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {ModuleLoadedEnum} from "@shared/enums/module-loaded.enum";
import {
  AdminIndexFieldAliasState,
  AdminOaiMetadataPrefixState,
  AdminOaiSetState,
  AdminGlobalBannerState,
  SsrUtil,
} from "solidify-frontend";

const routables = [
  AdminHomeRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminState,
      AdminLicenseState,
      AdminGlobalBannerState,
      AdminInstitutionState,
      AdminInstitutionProjectState,
      AdminProjectState,
      AdminProjectPersonRoleState,
      AdminProjectFundingAgencyState,
      AdminProjectInstitutionState,
      AdminProjectRmlState,
      AdminProjectResearchObjectTypeState,
      AdminUserState,
      AdminOaiMetadataPrefixState,
      AdminOaiSetState,
      AdminGlobalBannerState,
      AdminPersonState,
      AdminPersonProjectRoleState,
      AdminRoleState,
      AdminIndexFieldAliasState,
      AdminFundingAgenciesState,
      AdminFundingAgenciesProjectState,
      AdminPersonInstitutionsState,
      AdminLanguageState,
      AdminScheduledTaskState,
      AdminOntologyState,
      AdminRmlState,
      AdminResearchObjectTypeState,
      AdminIIIFCollectionSettingsState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminModule {
  constructor() {
    if (SsrUtil.window) {
      SsrUtil.window[ModuleLoadedEnum.adminModuleLoaded] = true;
    }
  }
}
