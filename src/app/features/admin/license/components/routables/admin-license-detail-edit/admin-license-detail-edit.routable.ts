/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-license-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {adminLicenseActionNameSpace} from "@admin/license/stores/admin-license.action";
import {
  AdminLicenseState,
  AdminLicenseStateModel,
} from "@admin/license/stores/admin-license.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {License} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {sharedLicenseActionNameSpace} from "@shared/stores/license/shared-license.action";
import {Observable} from "rxjs";
import {
  AbstractDetailEditCommonRoutable,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-license-detail-edit-routable",
  templateUrl: "./admin-license-detail-edit.routable.html",
  styleUrls: ["./admin-license-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminLicenseDetailEditRoutable extends AbstractDetailEditCommonRoutable<License, AdminLicenseStateModel> {
  @Select(AdminLicenseState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminLicenseState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedLicenseActionNameSpace;

  readonly KEY_PARAM_NAME: keyof License & string = "title";

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_license, _injector, adminLicenseActionNameSpace, StateEnum.admin);
    this.editAvailable = this._securityService.isRootOrAdmin();
    this.deleteAvailable = this._securityService.isRootOrAdmin();
  }

  _getSubResourceWithParentId(id: string): void {
  }
}
