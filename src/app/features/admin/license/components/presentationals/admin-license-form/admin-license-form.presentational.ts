/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-license-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminLicenseState} from "@admin/license/stores/admin-license.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {Enums} from "@enums";
import {License} from "@models";
import {SharedAbstractFormPresentational} from "@shared/components/presentationals/shared-abstract-form/shared-abstract.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {
  KeyValue,
  PropertyName,
  RegexUtil,
  ResourceFileNameSpace,
  SolidifyValidator,
} from "solidify-frontend";
import {AdminLicenseAction} from "../../../stores/admin-license.action";

@Component({
  selector: "hedera-admin-license-form",
  templateUrl: "./admin-license-form.presentational.html",
  styleUrls: ["./admin-license-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminLicenseFormPresentational extends SharedAbstractFormPresentational<License> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  licensesStatusEnum: KeyValue[] = Enums.License.StatusEnumTranslate;
  licenseOdConformanceEnum: KeyValue[] = Enums.License.OdConformanceEnumTranslate;
  licenseOsdConformanceEnum: KeyValue[] = Enums.License.OsdConformanceEnumTranslate;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  adminLicenseActionNamespace: ResourceFileNameSpace = AdminLicenseAction;
  adminLicenseState: typeof AdminLicenseState = AdminLicenseState;

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.title]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.openLicenseId]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.url]: ["", [SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.maintainer]: ["", [SolidifyValidator]],
      [this.formDefinition.odConformance]: ["", [SolidifyValidator]],
      [this.formDefinition.osdConformance]: ["", [SolidifyValidator]],
      [this.formDefinition.status]: [Enums.License.StatusEnum.RETIRED, [SolidifyValidator]],
      [this.formDefinition.isGeneric]: [false, [SolidifyValidator]],
      [this.formDefinition.family]: ["", [SolidifyValidator]],
      [this.formDefinition.domainContent]: [false, [SolidifyValidator]],
      [this.formDefinition.domainData]: [false, [SolidifyValidator]],
      [this.formDefinition.domainSoftware]: [false, [SolidifyValidator]],
    });
  }

  protected _bindFormTo(licenses: License): void {
    this.form = this._fb.group({
      [this.formDefinition.title]: [licenses.title, [Validators.required, SolidifyValidator]],
      [this.formDefinition.openLicenseId]: [licenses.openLicenseId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.url]: [licenses.url, [SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.maintainer]: [licenses.maintainer, [SolidifyValidator]],
      [this.formDefinition.odConformance]: [licenses.odConformance, [SolidifyValidator]],
      [this.formDefinition.osdConformance]: [licenses.osdConformance, [SolidifyValidator]],
      [this.formDefinition.status]: [licenses.status, [Validators.required, SolidifyValidator]],
      [this.formDefinition.isGeneric]: [licenses.isGeneric, [SolidifyValidator]],
      [this.formDefinition.family]: [licenses.family, [SolidifyValidator]],
      [this.formDefinition.domainContent]: [licenses.domainContent, [SolidifyValidator]],
      [this.formDefinition.domainData]: [licenses.domainData, [SolidifyValidator]],
      [this.formDefinition.domainSoftware]: [licenses.domainSoftware, [SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(licenses: License): License {
    return licenses;
  }

}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() title: string;
  @PropertyName() openLicenseId: string;
  @PropertyName() url: string;
  @PropertyName() maintainer: string;
  @PropertyName() odConformance: string;
  @PropertyName() osdConformance: string;
  @PropertyName() status: string;
  @PropertyName() isGeneric: string;
  @PropertyName() family: string;
  @PropertyName() domainContent: string;
  @PropertyName() domainData: string;
  @PropertyName() domainSoftware: string;
}
