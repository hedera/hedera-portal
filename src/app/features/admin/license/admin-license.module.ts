/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-license.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {AdminLicenseRoutingModule} from "@admin/license/admin-license-routing.module";
import {AdminLicenseFormPresentational} from "@admin/license/components/presentationals/admin-license-form/admin-license-form.presentational";
import {AdminLicenseDetailEditRoutable} from "@admin/license/components/routables/admin-license-detail-edit/admin-license-detail-edit.routable";
import {AdminLicenseState} from "@admin/license/stores/admin-license.state";
import {NgModule} from "@angular/core";
import {AdminLicenseCreateRoutable} from "@app/features/admin/license/components/routables/admin-license-create/admin-license-create.routable";
import {AdminLicenseListRoutable} from "@app/features/admin/license/components/routables/admin-license-list/admin-license-list.routable";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

const routables = [
  AdminLicenseCreateRoutable,
  AdminLicenseDetailEditRoutable,
  AdminLicenseListRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminLicenseFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminLicenseRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminLicenseState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminLicenseModule {
}
