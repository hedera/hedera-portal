/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-license.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  AdminLicenseAction,
  adminLicenseActionNameSpace,
} from "@admin/license/stores/admin-license.action";
import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {License} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  defaultResourceFileStateInitValue,
  DownloadService,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  OverrideDefaultAction,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";

export interface AdminLicenseStateModel extends ResourceFileStateModel<License> {
}

@Injectable()
@State<AdminLicenseStateModel>({
  name: StateEnum.admin_license,
  defaults: {
    ...defaultResourceFileStateInitValue(),
  },
})
export class AdminLicenseState extends ResourceFileState<AdminLicenseStateModel, License> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: adminLicenseActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminLicenseDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminLicenseDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminLicense,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.license.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.license.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.license.notification.resource.update"),
    }, _downloadService, ResourceFileStateModeEnum.logo, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminLicenses;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static isLoading(state: AdminLicenseStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminLicenseStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentTitle(state: AdminLicenseStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.title;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminLicenseStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminLicenseStateModel): boolean {
    return true;
  }

  @OverrideDefaultAction()
  @Action(AdminLicenseAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<AdminLicenseStateModel>, action: AdminLicenseAction.GetByIdSuccess): void {
    super.getByIdSuccess(ctx, action);
    if (isNotNullNorUndefined(action.model.logo)) {
      ctx.dispatch(new AdminLicenseAction.GetFile(action.model.resId));
    }
  }
}
