/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-license-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {AdminLicenseCreateRoutable} from "@admin/license/components/routables/admin-license-create/admin-license-create.routable";
import {AdminLicenseDetailEditRoutable} from "@admin/license/components/routables/admin-license-detail-edit/admin-license-detail-edit.routable";
import {AdminLicenseListRoutable} from "@admin/license/components/routables/admin-license-list/admin-license-list.routable";
import {AdminLicenseState} from "@admin/license/stores/admin-license.state";
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {ApplicationRolePermissionEnum} from "@shared/enums/application-role-permission.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AdminRoutesEnum,
  AppRoutesEnum,
} from "@shared/enums/routes.enum";
import {ApplicationRoleGuardService} from "@shared/guards/application-role-guard.service";
import {HederaRoutes} from "@shared/models/hedera-route.model";
import {
  CanDeactivateGuard,
  CombinedGuardService,
  EmptyContainer,
} from "solidify-frontend";

const routes: HederaRoutes = [
  {
    path: AppRoutesEnum.root,
    component: AdminLicenseListRoutable,
    data: {},
  },
  {
    path: AdminRoutesEnum.licenseCreate,
    component: AdminLicenseCreateRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.create,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
    canDeactivate: [CanDeactivateGuard],
  },
  {
    path: AdminRoutesEnum.licenseDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: AdminLicenseDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: AdminLicenseState.currentTitle,
    },
    children: [
      {
        path: AdminRoutesEnum.licenseEdit,
        component: EmptyContainer,
        data: {
          breadcrumb: LabelTranslateEnum.edit,
        },
        canDeactivate: [CanDeactivateGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminLicenseRoutingModule {
}
