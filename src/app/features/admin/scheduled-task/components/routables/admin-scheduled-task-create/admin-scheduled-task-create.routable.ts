/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-scheduled-task-create.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Injector} from "@angular/core";
import {AbstractCreateRoutable} from "solidify-frontend";
import {Actions, Select, Store} from "@ngxs/store";
import {Observable} from "rxjs";
import {StateEnum} from "@shared/enums/state.enum";
import {ScheduledTask} from "@models";
import {AdminScheduledTaskState, AdminScheduledTaskStateModel} from "@admin/scheduled-task/stores/admin-scheduled-task.state";
import {adminScheduledTaskActionNameSpace} from "@admin/scheduled-task/stores/admin-scheduled-task.action";

@Component({
  selector: "hedera-admin-scheduled-task-create-routable",
  templateUrl: "./admin-scheduled-task-create.routable.html",
  styleUrls: ["./admin-scheduled-task-create.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminScheduledTaskCreateRoutable extends AbstractCreateRoutable<ScheduledTask, AdminScheduledTaskStateModel> {
  @Select(AdminScheduledTaskState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminScheduledTaskState.isReadyToBeDisplayedInCreateMode) isReadyToBeDisplayedInCreateModeObs: Observable<boolean>;

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector) {
    super(_store, _actions$, _changeDetector, StateEnum.admin_scheduledTask, _injector, adminScheduledTaskActionNameSpace, StateEnum.admin);
  }

}
