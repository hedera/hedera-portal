/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-scheduled-task-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  AdminScheduledTaskAction,
  adminScheduledTaskActionNameSpace,
} from "@admin/scheduled-task/stores/admin-scheduled-task.action";
import {AdminScheduledTaskStateModel} from "@admin/scheduled-task/stores/admin-scheduled-task.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Enums} from "@enums";
import {ScheduledTask} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {
  AbstractListRoutable,
  ButtonColorEnum,
  DataTableFieldTypeEnum,
  OrderEnum,
  RouterExtensionService,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-scheduled-task-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./admin-scheduled-task-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminScheduledTaskListRoutable extends AbstractListRoutable<ScheduledTask, AdminScheduledTaskStateModel> {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToAdmin;
  readonly KEY_PARAM_NAME: keyof ScheduledTask & string = "name";

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.admin_scheduledTask, adminScheduledTaskActionNameSpace, _injector, {
      listExtraButtons: [
        {
          color: ButtonColorEnum.primary,
          icon: IconNameEnum.enabledScheduled,
          labelToTranslate: (current) => LabelTranslateEnum.scheduledEnabledTask,
          callback: () => this._enabledTask(),
          displayCondition: () => this._securityService.isRoot(),
          order: 40,
        },
        {
          color: ButtonColorEnum.primary,
          icon: IconNameEnum.disabledScheduled,
          labelToTranslate: (current) => LabelTranslateEnum.disabledScheduledTask,
          callback: () => this._disableTask(),
          displayCondition: () => this._securityService.isRoot(),
          order: 40,
        },
      ],
      canCreate: _securityService.isRootOrAdmin(),
    }, StateEnum.admin);
  }

  conditionDisplayEditButton(model: ScheduledTask | undefined): boolean {
    return this._securityService.isRootOrAdmin();
  }

  conditionDisplayDeleteButton(model: ScheduledTask | undefined): boolean {
    return this._securityService.isRootOrAdmin();
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "taskType",
        header: LabelTranslateEnum.taskTypeLabel,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        filterableField: "taskType" as any,
        sortableField: "taskType" as any,
        translate: true,
        filterEnum: Enums.ScheduledTask.TaskTypeEnumTranslate,
      },
      {
        field: "enabled",
        header: LabelTranslateEnum.enabled,
        type: DataTableFieldTypeEnum.boolean,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "cronExpression",
        header: LabelTranslateEnum.cronExpression,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: true,
      },
      {
        field: "lastExecutionStart" as any,
        header: LabelTranslateEnum.lastExecutionStart,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "lastExecutionEnd" as any,
        header: LabelTranslateEnum.lastExecutionEnd,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
      },
    ];
  }

  private _enabledTask(): void {
    this._store.dispatch(new AdminScheduledTaskAction.ScheduledEnabledTask());
  }

  private _disableTask(): void {
    this._store.dispatch(new AdminScheduledTaskAction.DisableTaskScheduling());
  }
}
