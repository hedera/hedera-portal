/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-scheduled-task-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  AdminScheduledTaskAction,
  adminScheduledTaskActionNameSpace,
} from "@admin/scheduled-task/stores/admin-scheduled-task.action";
import {
  AdminScheduledTaskState,
  AdminScheduledTaskStateModel,
} from "@admin/scheduled-task/stores/admin-scheduled-task.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {ScheduledTask} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  AbstractDetailEditCommonRoutable,
  ExtraButtonToolbar,
  isTrue,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-scheduled-task-detail-edit-routable",
  templateUrl: "./admin-scheduled-task-detail-edit.routable.html",
  styleUrls: ["./admin-scheduled-task-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminScheduledTaskDetailEditRoutable extends AbstractDetailEditCommonRoutable<ScheduledTask, AdminScheduledTaskStateModel> {
  @Select(AdminScheduledTaskState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminScheduledTaskState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  readonly KEY_PARAM_NAME: keyof ScheduledTask & string = "name";

  listExtraButtons: ExtraButtonToolbar<ScheduledTask> [] = [
    {
      color: "primary",
      icon: IconNameEnum.stop,
      callback: () => this._killTask(),
      displayCondition: current => isTrue(current?.enabled),
      labelToTranslate: (current) => LabelTranslateEnum.killScheduledTask,
      order: 40,
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_scheduledTask, _injector, adminScheduledTaskActionNameSpace, StateEnum.admin);
    this.editAvailable = this._securityService.isRootOrAdmin();
    this.deleteAvailable = this._securityService.isRootOrAdmin();
  }

  _getSubResourceWithParentId(id: string): void {
  }

  private _killTask(): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new AdminScheduledTaskAction.KillTask(this._resId),
      AdminScheduledTaskAction.KillTaskSuccess,
      resultAction => {
        this._store.dispatch(new AdminScheduledTaskAction.GetById(this._resId));
      }));
  }
}
