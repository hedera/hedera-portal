/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-scheduled-task-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {Enums} from "@enums";
import {ScheduledTask} from "@models";
import {SharedAbstractFormPresentational} from "@shared/components/presentationals/shared-abstract-form/shared-abstract.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {
  KeyValue,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-scheduled-task-form",
  templateUrl: "./admin-scheduled-task-form.presentational.html",
  styleUrls: ["./admin-scheduled-task-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminScheduledTaskFormPresentational extends SharedAbstractFormPresentational<ScheduledTask> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  taskTypeEnumTranslate: KeyValue[] = Enums.ScheduledTask.TaskTypeEnumTranslate;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.enabled]: [false, [SolidifyValidator]],
      [this.formDefinition.taskType]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.cronExpression]: ["", [Validators.required, SolidifyValidator]],
    });
  }

  protected _bindFormTo(scheduledTask: ScheduledTask): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: [scheduledTask.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.enabled]: [scheduledTask.enabled, [SolidifyValidator]],
      [this.formDefinition.taskType]: [scheduledTask.taskType, [Validators.required, SolidifyValidator]],
      [this.formDefinition.cronExpression]: [scheduledTask.cronExpression, [Validators.required, SolidifyValidator]],
      [this.formDefinition.lastExecutionStart]: [scheduledTask.lastExecutionStart, [SolidifyValidator]],
      [this.formDefinition.lastExecutionEnd]: [scheduledTask.lastExecutionEnd, [SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(scheduledTask: ScheduledTask): ScheduledTask {
    return scheduledTask;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() taskType: string;
  @PropertyName() enabled: string;
  @PropertyName() cronExpression: string;
  @PropertyName() lastExecutionStart: string;
  @PropertyName() lastExecutionEnd: string;

}
