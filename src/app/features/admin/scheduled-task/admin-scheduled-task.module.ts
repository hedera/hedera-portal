/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-scheduled-task.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {NgModule} from "@angular/core";
import {SharedModule} from "@shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {AdminScheduledTaskState} from "@admin/scheduled-task/stores/admin-scheduled-task.state";
import {AdminScheduledTaskListRoutable} from "@admin/scheduled-task/components/routables/admin-scheduled-task-list/admin-scheduled-task-list.routable";
import {
  AdminScheduledTaskFormPresentational,
} from "@admin/scheduled-task/components/presentationals/admin-scheduled-task-form/admin-scheduled-task-form.presentational";
import {AdminScheduledTaskRoutingModule} from "@admin/scheduled-task/admin-scheduled-task-routing.module";
import {
  AdminScheduledTaskDetailEditRoutable,
} from "@admin/scheduled-task/components/routables/admin-scheduled-task-detail-edit/admin-scheduled-task-detail-edit.routable";
import {AdminScheduledTaskCreateRoutable} from "@admin/scheduled-task/components/routables/admin-scheduled-task-create/admin-scheduled-task-create.routable";

const routables = [
  AdminScheduledTaskListRoutable,
  AdminScheduledTaskDetailEditRoutable,
  AdminScheduledTaskCreateRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminScheduledTaskFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminScheduledTaskRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminScheduledTaskState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminScheduledTaskModule {
}
