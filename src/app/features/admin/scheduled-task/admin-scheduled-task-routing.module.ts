/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-scheduled-task-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */




import {HederaRoutes} from "@shared/models/hedera-route.model";
import {AdminRoutesEnum, AppRoutesEnum} from "@shared/enums/routes.enum";
import {CanDeactivateGuard, EmptyContainer} from "solidify-frontend";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {AdminScheduledTaskListRoutable} from "@admin/scheduled-task/components/routables/admin-scheduled-task-list/admin-scheduled-task-list.routable";
import {AdminScheduledTaskState} from "@admin/scheduled-task/stores/admin-scheduled-task.state";
import {
  AdminScheduledTaskDetailEditRoutable,
} from "@admin/scheduled-task/components/routables/admin-scheduled-task-detail-edit/admin-scheduled-task-detail-edit.routable";
import {
  AdminScheduledTaskCreateRoutable,
} from "@admin/scheduled-task/components/routables/admin-scheduled-task-create/admin-scheduled-task-create.routable";

const routes: HederaRoutes = [
  {
    path: AppRoutesEnum.root,
    component: AdminScheduledTaskListRoutable,
    data: {},
  },
  {
    path: AdminRoutesEnum.scheduledTaskDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: AdminScheduledTaskDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: AdminScheduledTaskState.currentTitle,
    },
    children: [
      {
        path: AdminRoutesEnum.scheduledTaskEdit,
        component: EmptyContainer,
        data: {
          breadcrumb: LabelTranslateEnum.edit,
        },
        canDeactivate: [CanDeactivateGuard],
      },
    ],
  },
  {
    component: AdminScheduledTaskCreateRoutable,
    path: AdminRoutesEnum.scheduledTaskCreate,
    data: {
      breadcrumb: LabelTranslateEnum.create,
    },
    canDeactivate: [CanDeactivateGuard],
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminScheduledTaskRoutingModule {
}
