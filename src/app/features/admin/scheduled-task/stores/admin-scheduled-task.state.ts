/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-scheduled-task.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  AdminScheduledTaskAction,
  adminScheduledTaskActionNameSpace,
} from "@admin/scheduled-task/stores/admin-scheduled-task.action";
import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {ScheduledTask} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  defaultResourceStateInitValue,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ResourceState,
  ResourceStateModel,
  Result,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export interface AdminScheduledTaskStateModel extends ResourceStateModel<ScheduledTask> {
}

@Injectable()
@State<AdminScheduledTaskStateModel>({
  name: StateEnum.admin_scheduledTask,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
})
export class AdminScheduledTaskState extends ResourceState<AdminScheduledTaskStateModel, ScheduledTask> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: adminScheduledTaskActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminScheduledTaskDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminScheduledTaskDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminScheduledTask,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.scheduledTask.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.scheduledTask.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.scheduledTask.notification.resource.update"),
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminScheduledTask;
  }

  @Selector()
  static currentTitle(state: AdminScheduledTaskStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @Selector()
  static isLoading(state: AdminScheduledTaskStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminScheduledTaskStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminScheduledTaskStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminScheduledTaskStateModel): boolean {
    return true;
  }

  @Action(AdminScheduledTaskAction.ScheduledEnabledTask)
  scheduledEnabledTask(ctx: SolidifyStateContext<AdminScheduledTaskStateModel>, action: AdminScheduledTaskAction.ScheduledEnabledTask): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<undefined, Result>(this._urlResource + urlSeparator + ApiActionNameEnum.SCHEDULED_ENABLED_TASK)
      .pipe(
        tap(deposit => ctx.dispatch(new AdminScheduledTaskAction.ScheduledEnabledTaskSuccess(action))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AdminScheduledTaskAction.ScheduledEnabledTaskFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(AdminScheduledTaskAction.ScheduledEnabledTaskSuccess)
  scheduledEnabledTaskSuccess(ctx: SolidifyStateContext<AdminScheduledTaskStateModel>, action: AdminScheduledTaskAction.ScheduledEnabledTaskSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("admin.scheduledTask.scheduledEnableTask.success"));
    ctx.dispatch(new AdminScheduledTaskAction.GetAll());
  }

  @Action(AdminScheduledTaskAction.ScheduledEnabledTaskFail)
  scheduledEnabledTaskFail(ctx: SolidifyStateContext<AdminScheduledTaskStateModel>, action: AdminScheduledTaskAction.ScheduledEnabledTaskFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("admin.scheduledTask.scheduledEnableTask.fail"));
  }

  @Action(AdminScheduledTaskAction.DisableTaskScheduling)
  disableTaskScheduling(ctx: SolidifyStateContext<AdminScheduledTaskStateModel>, action: AdminScheduledTaskAction.DisableTaskScheduling): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<undefined, Result>(this._urlResource + urlSeparator + ApiActionNameEnum.DISABLE_TASKS_SCHEDULING)
      .pipe(
        tap(deposit => ctx.dispatch(new AdminScheduledTaskAction.DisableTaskSchedulingSuccess(action))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AdminScheduledTaskAction.DisableTaskSchedulingFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(AdminScheduledTaskAction.DisableTaskSchedulingSuccess)
  disableTaskSchedulingSuccess(ctx: SolidifyStateContext<AdminScheduledTaskStateModel>, action: AdminScheduledTaskAction.DisableTaskSchedulingSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("admin.scheduledTask.disableTaskSchedulingFail.success"));
    ctx.dispatch(new AdminScheduledTaskAction.GetAll());
  }

  @Action(AdminScheduledTaskAction.DisableTaskSchedulingFail)
  disableTaskSchedulingFail(ctx: SolidifyStateContext<AdminScheduledTaskStateModel>, action: AdminScheduledTaskAction.DisableTaskSchedulingFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("admin.scheduledTask.disableTaskSchedulingFail.fail"));
  }

  @Action(AdminScheduledTaskAction.KillTask)
  killTask(ctx: SolidifyStateContext<AdminScheduledTaskStateModel>, action: AdminScheduledTaskAction.KillTask): Observable<ScheduledTask> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<ScheduledTask>(`${this._urlResource}/${action.resId}/${ApiActionNameEnum.KILL_TASK}`)
      .pipe(
        tap(() => {
          ctx.dispatch(new AdminScheduledTaskAction.KillTaskSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AdminScheduledTaskAction.KillTaskFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(AdminScheduledTaskAction.KillTaskSuccess)
  killTaskSuccess(ctx: SolidifyStateContext<AdminScheduledTaskStateModel>, action: AdminScheduledTaskAction.KillTaskSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("admin.scheduledTask.killTask.success"));
  }

  @Action(AdminScheduledTaskAction.KillTaskFail)
  killTaskFail(ctx: SolidifyStateContext<AdminScheduledTaskStateModel>, action: AdminScheduledTaskAction.KillTaskFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("admin.scheduledTask.killTask.fail"));
  }
}
