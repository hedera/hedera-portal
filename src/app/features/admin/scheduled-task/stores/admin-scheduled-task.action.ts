/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-scheduled-task.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {ScheduledTask} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  ResourceAction,
  ResourceNameSpace,
  TypeDefaultAction,
} from "solidify-frontend";

const state = StateEnum.admin_scheduledTask;

export namespace AdminScheduledTaskAction {
  @TypeDefaultAction(state)
  export class LoadResource extends ResourceAction.LoadResource {
  }

  @TypeDefaultAction(state)
  export class LoadResourceSuccess extends ResourceAction.LoadResourceSuccess {
  }

  @TypeDefaultAction(state)
  export class LoadResourceFail extends ResourceAction.LoadResourceFail {
  }

  @TypeDefaultAction(state)
  export class ChangeQueryParameters extends ResourceAction.ChangeQueryParameters {
  }

  @TypeDefaultAction(state)
  export class GetAll extends ResourceAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends ResourceAction.GetAllSuccess<ScheduledTask> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends ResourceAction.GetAllFail<ScheduledTask> {
  }

  @TypeDefaultAction(state)
  export class GetByListId extends ResourceAction.GetByListId {
  }

  @TypeDefaultAction(state)
  export class GetByListIdSuccess extends ResourceAction.GetByListIdSuccess {
  }

  @TypeDefaultAction(state)
  export class GetByListIdFail extends ResourceAction.GetByListIdFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends ResourceAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends ResourceAction.GetByIdSuccess<ScheduledTask> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends ResourceAction.GetByIdFail<ScheduledTask> {
  }

  @TypeDefaultAction(state)
  export class Create extends ResourceAction.Create<ScheduledTask> {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends ResourceAction.CreateSuccess<ScheduledTask> {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends ResourceAction.CreateFail<ScheduledTask> {
  }

  @TypeDefaultAction(state)
  export class Update extends ResourceAction.Update<ScheduledTask> {
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends ResourceAction.UpdateSuccess<ScheduledTask> {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends ResourceAction.UpdateFail<ScheduledTask> {
  }

  @TypeDefaultAction(state)
  export class Delete extends ResourceAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends ResourceAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends ResourceAction.DeleteFail {
  }

  @TypeDefaultAction(state)
  export class DeleteList extends ResourceAction.DeleteList {
  }

  @TypeDefaultAction(state)
  export class DeleteListSuccess extends ResourceAction.DeleteListSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteListFail extends ResourceAction.DeleteListFail {
  }

  @TypeDefaultAction(state)
  export class AddInList extends ResourceAction.AddInList<ScheduledTask> {
  }

  @TypeDefaultAction(state)
  export class AddInListById extends ResourceAction.AddInListById {
  }

  @TypeDefaultAction(state)
  export class AddInListByIdSuccess extends ResourceAction.AddInListByIdSuccess<ScheduledTask> {
  }

  @TypeDefaultAction(state)
  export class AddInListByIdFail extends ResourceAction.AddInListByIdFail<ScheduledTask> {
  }

  @TypeDefaultAction(state)
  export class RemoveInListById extends ResourceAction.RemoveInListById {
  }

  @TypeDefaultAction(state)
  export class RemoveInListByListId extends ResourceAction.RemoveInListByListId {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkList extends ResourceAction.LoadNextChunkList {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkListSuccess extends ResourceAction.LoadNextChunkListSuccess<ScheduledTask> {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkListFail extends ResourceAction.LoadNextChunkListFail {
  }

  @TypeDefaultAction(state)
  export class Clean extends ResourceAction.Clean {
  }

  export class ScheduledEnabledTask extends BaseAction {
    static readonly type: string = `[${state}] Scheduled Enable Task`;
  }

  export class ScheduledEnabledTaskSuccess extends BaseSubActionSuccess<ScheduledEnabledTask> {
    static readonly type: string = `[${state}] Scheduled Enable Task Success`;
  }

  export class ScheduledEnabledTaskFail extends BaseSubActionFail<ScheduledEnabledTask> {
    static readonly type: string = `[${state}] Scheduled Enable Task Fail`;
  }

  export class DisableTaskScheduling extends BaseAction {
    static readonly type: string = `[${state}] Disable Task Scheduling`;
  }

  export class DisableTaskSchedulingSuccess extends BaseSubActionSuccess<DisableTaskScheduling> {
    static readonly type: string = `[${state}] Disable Task Scheduling Success`;
  }

  export class DisableTaskSchedulingFail extends BaseSubActionFail<DisableTaskScheduling> {
    static readonly type: string = `[${state}] Disable Task Scheduling Fail`;
  }

  export class KillTask extends BaseAction {
    static readonly type: string = `[${state}] Stop executing Scheduled Task`;

    constructor(public resId: string) {
      super();
    }
  }

  export class KillTaskSuccess extends BaseSubActionSuccess<KillTask> {
    static readonly type: string = `[${state}] Stop executing Scheduled Task Success`;
  }

  export class KillTaskFail extends BaseSubActionFail<KillTask> {
    static readonly type: string = `[${state}] Stop executing Scheduled Task Fail`;
  }
}

export const adminScheduledTaskActionNameSpace: ResourceNameSpace = AdminScheduledTaskAction;
