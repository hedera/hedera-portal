/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-user-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminUserAction} from "@admin/user/stores/admin-user.action";
import {AdminUserState} from "@admin/user/stores/admin-user.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {sharedPersonActionNameSpace} from "@app/shared/stores/person/shared-person.action";
import {Enums} from "@enums";
import {
  Person,
  User,
} from "@models";
import {SharedAbstractFormPresentational} from "@shared/components/presentationals/shared-abstract-form/shared-abstract.presentational";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {SharedPersonState} from "@shared/stores/person/shared-person.state";
import {
  DateUtil,
  isEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  KeyValue,
  OrderEnum,
  PropertyName,
  ResourceFileNameSpace,
  ResourceNameSpace,
  SolidifyValidator,
  Sort,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-user-form",
  templateUrl: "./admin-user-form.presentational.html",
  styleUrls: ["./admin-user-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminUserFormPresentational extends SharedAbstractFormPresentational<User> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  applicationRolesNames: KeyValue[] = Enums.UserApplicationRole.UserApplicationRoleEnumTranslate;

  userAvatarActionNameSpace: ResourceFileNameSpace = AdminUserAction;
  userState: typeof AdminUserState = AdminUserState;

  @Input()
  currentUser: User;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  sharedPersonSort: Sort<Person> = {
    field: "lastName",
    order: OrderEnum.ascending,
  };
  sharedPersonActionNameSpace: ResourceNameSpace = sharedPersonActionNameSpace;
  sharedPersonState: typeof SharedPersonState = SharedPersonState;
  labelPersonCallback: (person: Person) => string = person => person.lastName + ", " + person.firstName;

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _bindFormTo(user: User): void {
    this.form = this._fb.group({
      [this.formDefinition.externalUid]: [user.externalUid, [Validators.required, SolidifyValidator]],
      [this.formDefinition.firstName]: [user.firstName, [Validators.required, SolidifyValidator]],
      [this.formDefinition.lastName]: [user.lastName, [Validators.required, SolidifyValidator]],
      [this.formDefinition.homeOrganization]: [user.homeOrganization, [Validators.required, SolidifyValidator]],
      [this.formDefinition.email]: [user.email, [Validators.required, SolidifyValidator]],
      [this.formDefinition.person]: isNullOrUndefined(user.person) ? [] : [user.person.resId],
      [this.formDefinition.applicationRole]: [user.applicationRole.resId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.lastLoginIpAddress]: [user.lastLoginIpAddress, [SolidifyValidator]],
      [this.formDefinition.lastLoginTime]: [DateUtil.convertDateToDateTimeString(new Date(user.lastLoginTime)), [SolidifyValidator]],
    });
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.externalUid]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.firstName]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.lastName]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.homeOrganization]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.email]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.person]: [""],
      [this.formDefinition.applicationRole]: ["", [Validators.required, SolidifyValidator]],
    });
  }

  protected _disableSpecificField(): void {
    if (this.currentUser.resId === this.model?.resId) {
      this.form.get(this.formDefinition.applicationRole)?.disable();
    }
  }

  protected _treatmentBeforeSubmit(user: User): User {
    delete user.lastLoginTime;
    delete user.lastLoginIpAddress;
    user.applicationRole = {resId: this.form.get(this.formDefinition.applicationRole).value};
    // create a new person with the corresponding resId, if needed.
    const personResId: string = user.person as any;
    if (isNullOrUndefined(personResId) || isEmptyString(personResId)) {
      user.person = null;
    } else {
      user.person = {resId: personResId};
    }
    return user;
  }

  goToPerson(personId: string): void {
    this._navigateBS.next([RoutesEnum.adminPersonDetail, personId]);
  }

  getUser(): User {
    if (isNullOrUndefined(this.form)) {
      return this.model;
    }
    return this.form.value as User;
  }

  get person(): Person | undefined {
    return this.model?.person;
  }

  get personResId(): string {
    return this.person?.resId;
  }

  get isAvatarPresent(): boolean {
    return isNotNullNorUndefined(this.person?.avatar);
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() externalUid: string;
  @PropertyName() firstName: string;
  @PropertyName() lastName: string;
  @PropertyName() homeOrganization: string;
  @PropertyName() email: string;
  @PropertyName() person: string;
  @PropertyName() accessToken: string;
  @PropertyName() refreshToken: string;
  @PropertyName() applicationRole: string;
  @PropertyName() lastLoginIpAddress: string;
  @PropertyName() lastLoginTime: string;
}
