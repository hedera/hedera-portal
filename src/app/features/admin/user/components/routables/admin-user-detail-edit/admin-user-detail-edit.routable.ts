/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-user-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  AdminUserAction,
  adminUserActionNameSpace,
} from "@admin/user/stores/admin-user.action";
import {
  AdminUserState,
  AdminUserStateModel,
} from "@admin/user/stores/admin-user.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {AppUserAction} from "@app/stores/user/app-user.action";
import {AppUserState} from "@app/stores/user/app-user.state";
import {User} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {sharedUserActionNameSpace} from "@shared/stores/user/shared-user.action";
import {Observable} from "rxjs";
import {
  filter,
  take,
  tap,
} from "rxjs/operators";
import {
  AbstractDetailEditCommonRoutable,
  isNotNullNorUndefined,
  MemoizedUtil,
  ofSolidifyActionCompleted,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-user-detail-edit-routable",
  templateUrl: "./admin-user-detail-edit.routable.html",
  styleUrls: ["./admin-user-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminUserDetailEditRoutable extends AbstractDetailEditCommonRoutable<User, AdminUserStateModel> implements OnInit {
  @Select(AdminUserState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminUserState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;
  currentUserObs: Observable<User> = MemoizedUtil.current(this._store, AppUserState);

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedUserActionNameSpace;

  readonly KEY_PARAM_NAME: keyof User & string = "externalUid";

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_user, _injector, adminUserActionNameSpace, StateEnum.admin);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribe(this.currentUserObs.pipe(
      filter(user => isNotNullNorUndefined(user)),
      take(1),
      tap(currentUser => {
        this._refreshCurrentUserWhenUpdated(currentUser);
      }),
    ));
  }

  _getSubResourceWithParentId(id: string): void {
  }

  private _refreshCurrentUserWhenUpdated(currentUser: User): void {
    if (currentUser?.resId === this._resId) {
      this.subscribe(this._actions$.pipe(ofSolidifyActionCompleted(AdminUserAction.UploadFileSuccess))
        .pipe(
          tap(result => {
            this._store.dispatch(new AppUserAction.GetCurrentUser());
          }),
        ));
      this.subscribe(this._actions$.pipe(ofSolidifyActionCompleted(AdminUserAction.DeleteFileSuccess))
        .pipe(
          tap(result => {
            this._store.dispatch(new AppUserAction.GetCurrentUser());
          }),
        ));
    }
  }

  navigate($event: string[]): void {
    this._store.dispatch(new Navigate($event));
  }
}
