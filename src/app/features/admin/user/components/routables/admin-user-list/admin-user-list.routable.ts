/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-user-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  AdminUserAction,
  adminUserActionNameSpace,
} from "@admin/user/stores/admin-user.action";
import {
  AdminUserState,
  AdminUserStateModel,
} from "@admin/user/stores/admin-user.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {IconNameEnum} from "@app/shared/enums/icon-name.enum";
import {SecurityService} from "@app/shared/services/security.service";
import {AppState} from "@app/stores/app.state";
import {User} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {UserInfoDataTableColumn} from "@shared/models/user-info-data-table-column.model";
import {
  AbstractListRoutable,
  ButtonColorEnum,
  DataTableFieldTypeEnum,
  isNotNullNorUndefined,
  OrderEnum,
  RouterExtensionService,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-user-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./admin-user-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminUserListRoutable extends AbstractListRoutable<User, AdminUserStateModel> implements OnInit {
  readonly KEY_CREATE_BUTTON: string = undefined;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToAdmin;
  readonly KEY_PARAM_NAME: keyof User & string = "externalUid";

  adminUserState: typeof AdminUserState = AdminUserState;

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.admin_user, adminUserActionNameSpace, _injector, {
      listExtraButtons: [
        {
          color: ButtonColorEnum.primary,
          icon: IconNameEnum.synchronize,
          labelToTranslate: (current) => LabelTranslateEnum.synchronize,
          callback: () => this._synchronize(),
          displayCondition: () => this._securityService.isRoot(),
          order: 40,
        },
      ],
      canCreate: false,
    }, StateEnum.admin);
  }

  private _synchronize(): void {
    this._store.dispatch(new AdminUserAction.Synchronize());
  }

  ngOnInit(): void {
    super.ngOnInit();
    const currentUser: User = this._store.selectSnapshot(AppState.currentUser);
    this.listNewId = [currentUser.resId];
  }

  conditionDisplayEditButton(model: User | undefined): boolean {
    return true;
  }

  conditionDisplayDeleteButton(model: User | undefined): boolean {
    return true;
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "avatar" as any,
        header: LabelTranslateEnum.avatar,
        order: OrderEnum.none,
        resourceNameSpace: adminUserActionNameSpace,
        resourceState: this.adminUserState as any,
        isFilterable: false,
        isSortable: false,
        component: DataTableComponentHelper.get(DataTableComponentEnum.logo),
        isUser: true,
        idResource: (user: User) => user.person?.resId,
        isLogoPresent: (user: User) => isNotNullNorUndefined(user.person?.avatar),
      } as UserInfoDataTableColumn<User>,
      {
        field: "lastName",
        header: LabelTranslateEnum.lastName,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "firstName",
        header: LabelTranslateEnum.firstName,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "email",
        header: LabelTranslateEnum.email,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "lastLoginTime" as any,
        header: LabelTranslateEnum.lastLoginTime,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
    ];
  }
}
