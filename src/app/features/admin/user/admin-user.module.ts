/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-user.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {AdminUserRoutingModule} from "@admin/user/admin-user-routing.module";
import {AdminUserFormPresentational} from "@admin/user/components/presentationals/admin-user-form/admin-user-form.presentational";
import {AdminUserDetailEditRoutable} from "@admin/user/components/routables/admin-user-detail-edit/admin-user-detail-edit.routable";
import {AdminUserListRoutable} from "@admin/user/components/routables/admin-user-list/admin-user-list.routable";
import {AdminUserState} from "@admin/user/stores/admin-user.state";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

const routables = [
  AdminUserListRoutable,
  AdminUserDetailEditRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminUserFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminUserRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminUserState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminUserModule {
}
