/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-user.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  AdminUserAction,
  adminUserActionNameSpace,
} from "@admin/user/stores/admin-user.action";
import {Injectable} from "@angular/core";
import {ApiActionNameEnum} from "@app/shared/enums/api-action-name.enum";
import {environment} from "@environments/environment";
import {User} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ActionSubActionCompletionsWrapper,
  ApiService,
  defaultResourceFileStateInitValue,
  DownloadService,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  OverrideDefaultAction,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  Result,
  ResultActionStatusEnum,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export interface AdminUserStateModel extends ResourceFileStateModel<User> {
}

@Injectable()
@State<AdminUserStateModel>({
  name: StateEnum.admin_user,
  defaults: {
    ...defaultResourceFileStateInitValue(),
  },
})
export class AdminUserState extends ResourceFileState<AdminUserStateModel, User> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: adminUserActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminUserDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminUserDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminUser,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.user.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.user.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.user.notification.resource.update"),
    }, _downloadService, ResourceFileStateModeEnum.avatar, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminUsers;
  }

  protected get _urlFileResource(): string {
    return ApiEnum.adminPeople;
  }

  @Selector()
  static isLoading(state: AdminUserStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminUserStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentTitle(state: AdminUserStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.externalUid;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminUserStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminUserStateModel): boolean {
    return true;
  }

  @OverrideDefaultAction()
  @Action(AdminUserAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<AdminUserStateModel>, action: AdminUserAction.GetByIdSuccess): void {
    super.getByIdSuccess(ctx, action);

    if (isNotNullNorUndefined(action.model.person?.avatar)) {
      ctx.dispatch(new AdminUserAction.GetFile(action.model.person.resId));
    }
  }

  @Action(AdminUserAction.Synchronize)
  synchronize(ctx: SolidifyStateContext<AdminUserStateModel>, action: AdminUserAction.Synchronize): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<undefined, Result>(this._urlResource + urlSeparator + ApiActionNameEnum.SYNCHRONIZE).pipe(
      tap(result => {
        if (result?.status === ResultActionStatusEnum.EXECUTED) {
          ctx.dispatch(new AdminUserAction.SynchronizeSuccess(action, result));
        } else {
          ctx.dispatch(new AdminUserAction.SynchronizeFail(action));
        }
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new AdminUserAction.SynchronizeFail(action));
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(AdminUserAction.SynchronizeSuccess)
  synchronizeSuccess(ctx: SolidifyStateContext<AdminUserStateModel>, action: AdminUserAction.SynchronizeSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(action.result.message);
    ctx.dispatch(new AdminUserAction.GetAll());
  }

  @Action(AdminUserAction.SynchronizeFail)
  synchronizeFail(ctx: SolidifyStateContext<AdminUserStateModel>, action: AdminUserAction.SynchronizeFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("admin.user.notification.synchronize.fail"));
  }

  protected override _getListActionsUpdateSubResource(model: User, action: AdminUserAction.Create | AdminUserAction.Update, ctx: SolidifyStateContext<AdminUserStateModel>): ActionSubActionCompletionsWrapper[] {
    const user = ctx.getState().current;
    return super._getActionUpdateFile(user.person, action, ctx);
  }
}
