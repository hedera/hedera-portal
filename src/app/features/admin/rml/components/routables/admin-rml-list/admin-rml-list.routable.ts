/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-rml-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {adminRmlActionNameSpace} from "@admin/rml/stores/admin-rml.action";
import {AdminRmlStateModel} from "@admin/rml/stores/admin-rml.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Enums} from "@enums";
import {Rml} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {
  AbstractListRoutable,
  DataTableFieldTypeEnum,
  OrderEnum,
  RouterExtensionService,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-rml-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./admin-rml-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminRmlListRoutable extends AbstractListRoutable<Rml, AdminRmlStateModel> {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToAdmin;
  readonly KEY_PARAM_NAME: keyof Rml & string = "name";

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.admin_rml, adminRmlActionNameSpace, _injector, {
      canCreate: _securityService.isRootOrAdmin(),
    }, StateEnum.admin);
  }

  conditionDisplayEditButton(model: Rml | undefined): boolean {
    return true;
  }

  conditionDisplayDeleteButton(model: Rml | undefined): boolean {
    return true;
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "format",
        header: LabelTranslateEnum.format,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        filterableField: "format" as any,
        sortableField: "format" as any,
        translate: true,
        filterEnum: Enums.Rml.RmlFormatEnumTranslate,
      },
      {
        field: "version",
        header: LabelTranslateEnum.version,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: true,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
    ];
  }
}
