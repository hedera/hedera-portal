/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-rml-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminRmlAction} from "@admin/rml/stores/admin-rml.action";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {Enums} from "@enums";
import {
  DataFile,
  Rml,
} from "@models";
import {SharedAbstractFormPresentational} from "@shared/components/presentationals/shared-abstract-form/shared-abstract.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {DataFileHelper} from "@shared/helpers/data-file.helper";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {
  isNotNullNorUndefined,
  KeyValue,
  PropertyName,
  ResourceFileNameSpace,
  SolidifyDataFileModel,
  SolidifyFileUploadInputRequiredValidator,
  SolidifyFileUploadStatus,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-rml-form",
  templateUrl: "./admin-rml-form.presentational.html",
  styleUrls: ["./admin-rml-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminRmlFormPresentational extends SharedAbstractFormPresentational<Rml> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  rmlFormatEnum: KeyValue[] = Enums.Rml.RmlFormatEnumTranslate;

  adminRmlActionNameSpace: ResourceFileNameSpace = AdminRmlAction;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  dataFileAdapter: (dataFile: DataFile) => SolidifyDataFileModel = DataFileHelper.dataFileAdapter;

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _bindFormTo(rml: Rml): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: [rml.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.version]: [rml.version, [Validators.required, SolidifyValidator]],
      [this.formDefinition.format]: [rml.format, [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: [rml.description, [SolidifyValidator]],
      [this.formDefinition.rdfFileChange]: [isNotNullNorUndefined(rml.rdfFile) ? SolidifyFileUploadStatus.UNCHANGED : undefined, [SolidifyFileUploadInputRequiredValidator, SolidifyValidator]],
    });
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.version]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.format]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: ["", [SolidifyValidator]],
      [this.formDefinition.rdfFileChange]: [undefined, [Validators.required, SolidifyFileUploadInputRequiredValidator, SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(rml: Rml): Rml {
    return rml;
  }

}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() version: string;
  @PropertyName() description: string;
  @PropertyName() format: string;
  @PropertyName() rdfFileChange: string;
}
