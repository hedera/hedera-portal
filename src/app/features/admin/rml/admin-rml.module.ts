/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-rml.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {AdminRmlCreateRoutable} from "@admin/rml/components/routables/admin-rml-create/admin-rml-create.routable";
import {AdminRmlDetailEditRoutable} from "@admin/rml/components/routables/admin-rml-detail-edit/admin-rml-detail-edit.routable";
import {AdminRmlListRoutable} from "@admin/rml/components/routables/admin-rml-list/admin-rml-list.routable";
import {AdminRmlFormPresentational} from "@admin/rml/components/presentationals/admin-rml-form/admin-rml-form.presentational";
import {AdminRmlState} from "@admin/rml/stores/admin-rml.state";
import {AdminRmlRoutingModule} from "@admin/rml/admin-rml-routing.module";

const routables = [
  AdminRmlCreateRoutable,
  AdminRmlDetailEditRoutable,
  AdminRmlListRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminRmlFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminRmlRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminRmlState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminRmlModule {
}
