/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-rml.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminRmlAction,
  adminRmlActionNameSpace,
} from "@admin/rml/stores/admin-rml.action";
import {
  HttpEventType,
  HttpStatusCode,
} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {Rml} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {RmlFileUploadWrapperModel} from "@shared/models/rml-file-upload-wrapper.model";
import {Observable} from "rxjs";
import {
  catchError,
  filter,
  map,
} from "rxjs/operators";
import {
  ActionSubActionCompletionsWrapper,
  ApiService,
  defaultResourceFileStateInitValue,
  DownloadService,
  isInstanceOf,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ofSolidifyActionCompleted,
  OverrideDefaultAction,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  SolidifyFileUploadStatus,
  SolidifyFileUploadType,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  StoreUtil,
  UploadEventModel,
} from "solidify-frontend";

const ATTRIBUTE_FILE: string = "rdfFile";

export interface AdminRmlStateModel extends ResourceFileStateModel<Rml> {
}

@Injectable()
@State<AdminRmlStateModel>({
  name: StateEnum.admin_rml,
  defaults: {
    ...defaultResourceFileStateInitValue(),
  },
  children: [],
})
export class AdminRmlState extends ResourceFileState<AdminRmlStateModel, Rml> {
  private readonly _NAME_KEY: string = "name";
  private readonly _DESCRIPTION_KEY: string = "description";
  private readonly _VERSION_KEY: string = "version";
  private readonly _FORMAT_KEY: string = "format";

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: adminRmlActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminRmlDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminRmlDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminRml,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.rml.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.rml.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.rml.notification.resource.update"),
      downloadInMemory: false,
      resourceFileApiActionNameDownloadCustom: ApiActionNameEnum.DOWNLOAD_RDF,
      resourceFileApiActionNameDeleteCustom: ApiActionNameEnum.DELETE_RDF,
      resourceFileApiActionNameUploadCustom: ApiActionNameEnum.UPLOAD_FILE,
      customFileAttribute: ATTRIBUTE_FILE,
      addExtraFormDataDuringUpload: (formData, action) => {
        const fileUploadWrapper = action.fileUploadWrapper as RmlFileUploadWrapperModel;
        formData.append(this._NAME_KEY, fileUploadWrapper.name);
        formData.append(this._DESCRIPTION_KEY, fileUploadWrapper.description);
        formData.append(this._VERSION_KEY, fileUploadWrapper.version);
        formData.append(this._FORMAT_KEY, fileUploadWrapper.format);
      },
    }, _downloadService, ResourceFileStateModeEnum.custom, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminRmls;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static isLoading(state: AdminRmlStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminRmlStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentName(state: AdminRmlStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminRmlStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminRmlStateModel): boolean {
    return true;
  }

  protected override _internalCreate(ctx: SolidifyStateContext<AdminRmlStateModel>, action: AdminRmlAction.Create): Observable<Rml> {
    ctx.dispatch(new AdminRmlAction.Clean(true));

    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const model = action.modelFormControlEvent?.model;
    const file = (model.rdfFileChange as File);
    const formData = new FormData();
    formData.append(this._FILE_KEY, file, file.name);
    formData.append("name", model.name);
    formData.append("description", model.description);
    formData.append("format", model.format);
    formData.append("version", model.version);
    formData.append("mimeType", file.type);

    return this._apiService.upload(`${this._urlResource}/${ApiActionNameEnum.UPLOAD_RDF}`, formData)
      .pipe(
        map((event: UploadEventModel) => {
          switch (event.type) {
            case HttpEventType.UploadProgress:
              return;
            case HttpEventType.Response:
              if ([HttpStatusCode.Created, HttpStatusCode.Ok].includes(event.status) && isNotNullNorUndefined(event.body)) {
                return event.body as Rml;
              }
              return;
            default:
              return;
          }
        }),
        filter(rml => isNotNullNorUndefined(rml)), // Allow to wait that upload finish
        catchError((error: SolidifyHttpErrorResponseModel) => {
          throw error;
        }),
        StoreUtil.catchValidationErrors(ctx as any, action.modelFormControlEvent as any, this._notificationService, this._optionsState.autoScrollToFirstValidationError),
      );
  }

  protected override _getListActionsUpdateSubResource(model: Rml, action: AdminRmlAction.Create | AdminRmlAction.Update, ctx: SolidifyStateContext<AdminRmlStateModel>): ActionSubActionCompletionsWrapper[] {
    if (isInstanceOf(action, AdminRmlAction.Create)) {
      return [];
    }
    action = action as AdminRmlAction.Update;
    const actions = super._getListActionsUpdateSubResource(model, action, ctx);
    const rmlId = model.resId;
    const rml: Rml = action.modelFormControlEvent.model;
    const fileChange: SolidifyFileUploadType = rml.rdfFileChange;

    if (fileChange === SolidifyFileUploadStatus.UNCHANGED) {
      // do nothing
    } else if (fileChange === SolidifyFileUploadStatus.DELETED) {
      // do nothing, not allow to delete
    } else if (isNotNullNorUndefined(fileChange)) {
      actions.push({
        action: new AdminRmlAction.UploadFile(rmlId, {
          file: fileChange as File,
        }, action.modelFormControlEvent),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminRmlAction.UploadFileSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminRmlAction.UploadFileFail)),
        ],
      });
    }
    return actions;
  }

  @OverrideDefaultAction()
  @Action(AdminRmlAction.UploadFile)
  uploadFile(ctx: SolidifyStateContext<AdminRmlStateModel>, action: AdminRmlAction.UploadFile): Observable<any> {
    return super.uploadFile(ctx, action).pipe(
      StoreUtil.catchValidationErrors(ctx as any, action.modelFormControlEvent as any, this._notificationService, this._optionsState.autoScrollToFirstValidationError),
    );
  }
}
