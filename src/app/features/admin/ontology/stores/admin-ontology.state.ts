/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-ontology.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminOntologyAction,
  adminOntologyActionNameSpace,
} from "@admin/ontology/stores/admin-ontology.action";
import {
  HttpClient,
  HttpEventType,
  HttpStatusCode,
} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {Ontology} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {RmlFileUploadWrapperModel} from "@shared/models/rml-file-upload-wrapper.model";
import {Observable} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ActionSubActionCompletionsWrapper,
  ApiService,
  defaultResourceFileStateInitValue,
  DownloadService,
  isInstanceOf,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ofSolidifyActionCompleted,
  OverrideDefaultAction,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  SolidifyFileUploadStatus,
  SolidifyFileUploadType,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
  UploadEventModel,
} from "solidify-frontend";

const ATTRIBUTE_FILE: string = "ontologyFile";

export interface AdminOntologyStateModel extends ResourceFileStateModel<Ontology> {
  listRdfClasses: string[];
  listRdfProperties: string[];
}

@Injectable()
@State<AdminOntologyStateModel>({
  name: StateEnum.admin_ontology,
  defaults: {
    ...defaultResourceFileStateInitValue(),
    listRdfClasses: undefined,
    listRdfProperties: undefined,
  },
})
export class AdminOntologyState extends ResourceFileState<AdminOntologyStateModel, Ontology> {

  private readonly _NAME_KEY: string = "name";
  private readonly _DESCRIPTION_KEY: string = "description";
  private readonly _VERSION_KEY: string = "version";
  private readonly _FORMAT_KEY: string = "format";

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService,
              protected readonly _httpClient: HttpClient) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: adminOntologyActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminOntologyDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminOntologyDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminOntology,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.ontology.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.ontology.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.ontology.notification.resource.update"),
      downloadInMemory: false,
      resourceFileApiActionNameDownloadCustom: ApiActionNameEnum.DOWNLOAD_ONTOLOGY,
      resourceFileApiActionNameDeleteCustom: ApiActionNameEnum.DELETE_ONTOLOGY,
      resourceFileApiActionNameUploadCustom: ApiActionNameEnum.UPLOAD_FILE,
      customFileAttribute: ATTRIBUTE_FILE,
      addExtraFormDataDuringUpload: (formData, action) => {
        const fileUploadWrapper = action.fileUploadWrapper as RmlFileUploadWrapperModel;
        formData.append(this._NAME_KEY, fileUploadWrapper.name);
        formData.append(this._DESCRIPTION_KEY, fileUploadWrapper.description);
        formData.append(this._VERSION_KEY, fileUploadWrapper.version);
        formData.append(this._FORMAT_KEY, fileUploadWrapper.format);

      },
    }, _downloadService, ResourceFileStateModeEnum.custom, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminOntologies;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static isLoading(state: AdminOntologyStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminOntologyStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminOntologyStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminOntologyStateModel): boolean {
    return true;
  }

  @Selector()
  static currentName(state: AdminOntologyStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  protected override _internalCreate(ctx: SolidifyStateContext<AdminOntologyStateModel>, action: AdminOntologyAction.Create): Observable<Ontology> {
    ctx.dispatch(new AdminOntologyAction.Clean(true));

    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const model = action.modelFormControlEvent?.model;
    const file = (model.ontologyFileChange as File);
    const formData = new FormData();
    formData.append(this._FILE_KEY, file, file.name);
    formData.append("name", model.name);
    formData.append("description", model.description);
    formData.append("format", model.format);
    formData.append("url", model.url);
    formData.append("version", model.version);
    formData.append("mimeType", file.type);
    formData.append("baseUri", model.baseUri);

    return this._apiService.upload(`${this._urlResource}/${ApiActionNameEnum.UPLOAD_ONTOLOGY}`, formData)
      .pipe(
        map((event: UploadEventModel) => {
          switch (event.type) {
            case HttpEventType.UploadProgress:
              return;
            case HttpEventType.Response:
              if ([HttpStatusCode.Created, HttpStatusCode.Ok].includes(event.status) && isNotNullNorUndefined(event.body)) {
                return event.body as Ontology;
              }
              return;
            default:
              return;
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          throw error;
        }),
        StoreUtil.catchValidationErrors(ctx as any, action.modelFormControlEvent as any, this._notificationService, this._optionsState.autoScrollToFirstValidationError),
      );
  }

  protected override _getListActionsUpdateSubResource(model: Ontology, action: AdminOntologyAction.Create | AdminOntologyAction.Update, ctx: SolidifyStateContext<AdminOntologyStateModel>): ActionSubActionCompletionsWrapper[] {
    if (isInstanceOf(action, AdminOntologyAction.Create)) {
      return [];
    }
    action = action as AdminOntologyAction.Update;
    const actions = super._getListActionsUpdateSubResource(model, action, ctx);
    const ontologyId = model.resId;
    const ontology: Ontology = action.modelFormControlEvent.model;
    const fileChange: SolidifyFileUploadType = ontology.ontologyFileChange;

    if (fileChange === SolidifyFileUploadStatus.UNCHANGED) {
      // do nothing
    } else if (fileChange === SolidifyFileUploadStatus.DELETED) {
      // do nothing, not allow to delete
    } else if (isNotNullNorUndefined(fileChange)) {
      actions.push({
        action: new AdminOntologyAction.UploadFile(ontologyId, {
          file: fileChange as File,
        }, action.modelFormControlEvent),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminOntologyAction.UploadFileSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminOntologyAction.UploadFileFail)),
        ],
      });
    }
    return actions;
  }

  @OverrideDefaultAction()
  @Action(AdminOntologyAction.UploadFile)
  uploadFile(ctx: SolidifyStateContext<AdminOntologyStateModel>, action: AdminOntologyAction.UploadFile): Observable<any> {
    return super.uploadFile(ctx, action).pipe(
      StoreUtil.catchValidationErrors(ctx as any, action.modelFormControlEvent as any, this._notificationService, this._optionsState.autoScrollToFirstValidationError),
    );
  }

  @Action(AdminOntologyAction.GetRdfClasses)
  getRdfClasses(ctx: SolidifyStateContext<AdminOntologyStateModel>, action: AdminOntologyAction.GetRdfClasses): Observable<string[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.get<string[]>(`${this._urlResource}/${action.ontologyId}/${ApiResourceNameEnum.RDF_CLASS}`)
      .pipe(
        tap((list: string[]) => {
          ctx.dispatch(new AdminOntologyAction.GetRdfClassesSuccess(action, list));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AdminOntologyAction.GetRdfClassesFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(AdminOntologyAction.GetRdfClassesSuccess)
  getRdfClassesSuccess(ctx: SolidifyStateContext<AdminOntologyStateModel>, action: AdminOntologyAction.GetRdfClassesSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      listRdfClasses: action.list,
    });
  }

  @Action(AdminOntologyAction.GetRdfClassesFail)
  getRdfClassesFail(ctx: SolidifyStateContext<AdminOntologyStateModel>, action: AdminOntologyAction.GetRdfClassesFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(AdminOntologyAction.GetRdfProperties)
  getRdfProperties(ctx: SolidifyStateContext<AdminOntologyStateModel>, action: AdminOntologyAction.GetRdfProperties): Observable<string[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.get<string[]>(`${this._urlResource}/${action.ontologyId}/${ApiResourceNameEnum.RDF_PROPERTY}`)
      .pipe(
        tap((list: string[]) => {
          ctx.dispatch(new AdminOntologyAction.GetRdfPropertiesSuccess(action, list));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AdminOntologyAction.GetRdfPropertiesFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(AdminOntologyAction.GetRdfPropertiesSuccess)
  getRdfPropertiesSuccess(ctx: SolidifyStateContext<AdminOntologyStateModel>, action: AdminOntologyAction.GetRdfPropertiesSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      listRdfProperties: action.list,
    });
  }

  @Action(AdminOntologyAction.GetRdfPropertiesFail)
  getRdfPropertiesFail(ctx: SolidifyStateContext<AdminOntologyStateModel>, action: AdminOntologyAction.GetRdfPropertiesFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(AdminOntologyAction.IsPartOf)
  isPartOf(ctx: SolidifyStateContext<AdminOntologyStateModel>, action: AdminOntologyAction.IsPartOf): Observable<Enums.Ontology.RdfCategoryEnum> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._httpClient.post(`${this._urlResource}/${action.ontologyId}/${ApiActionNameEnum.IS_PART_OF}/${action.rdfType}`, undefined, {
      responseType: "text",
    }).pipe(
      tap((rdfCategory: Enums.Ontology.RdfCategoryEnum) => {
        ctx.dispatch(new AdminOntologyAction.IsPartOfSuccess(action, rdfCategory));
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new AdminOntologyAction.IsPartOfFail(action, error));
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(AdminOntologyAction.IsPartOfSuccess)
  isPartOfSuccess(ctx: SolidifyStateContext<AdminOntologyStateModel>, action: AdminOntologyAction.IsPartOfSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    let message = MARK_AS_TRANSLATABLE("admin.ontology.notification.typeIsPartOfOntology.isPartOf");
    if (action.rdfCategory === Enums.Ontology.RdfCategoryEnum.ONTOLOGY_RDF_CLASS) {
      message = MARK_AS_TRANSLATABLE("admin.ontology.notification.typeIsPartOfOntology.isClass");
    } else if (action.rdfCategory === Enums.Ontology.RdfCategoryEnum.ONTOLOGY_RDF_PROPERTY) {
      message = MARK_AS_TRANSLATABLE("admin.ontology.notification.typeIsPartOfOntology.isProperty");
    }
    this._notificationService.showSuccess(message);
  }

  @Action(AdminOntologyAction.IsPartOfFail)
  isPartOfFail(ctx: SolidifyStateContext<AdminOntologyStateModel>, action: AdminOntologyAction.IsPartOfFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    if (action.error.status === HttpStatusCode.NotFound) {
      this._notificationService.showError(MARK_AS_TRANSLATABLE("admin.ontology.notification.typeIsPartOfOntology.notPartOf"));
    }
  }
}
