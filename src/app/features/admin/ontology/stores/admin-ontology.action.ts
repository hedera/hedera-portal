/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-ontology.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Enums} from "@enums";
import {Ontology} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  FileUploadWrapper,
  ModelFormControlEvent,
  ResourceAction,
  ResourceFileAction,
  ResourceFileNameSpace,
  SolidifyHttpErrorResponseModel,
  TypeDefaultAction,
} from "solidify-frontend";

const state = StateEnum.admin_ontology;

export namespace AdminOntologyAction {
  @TypeDefaultAction(state)
  export class LoadResource extends ResourceAction.LoadResource {
  }

  @TypeDefaultAction(state)
  export class LoadResourceSuccess extends ResourceAction.LoadResourceSuccess {
  }

  @TypeDefaultAction(state)
  export class LoadResourceFail extends ResourceAction.LoadResourceFail {
  }

  @TypeDefaultAction(state)
  export class ChangeQueryParameters extends ResourceAction.ChangeQueryParameters {
  }

  @TypeDefaultAction(state)
  export class GetAll extends ResourceAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends ResourceAction.GetAllSuccess<Ontology> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends ResourceAction.GetAllFail<Ontology> {
  }

  @TypeDefaultAction(state)
  export class GetByListId extends ResourceAction.GetByListId {
  }

  @TypeDefaultAction(state)
  export class GetByListIdSuccess extends ResourceAction.GetByListIdSuccess {
  }

  @TypeDefaultAction(state)
  export class GetByListIdFail extends ResourceAction.GetByListIdFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends ResourceAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends ResourceAction.GetByIdSuccess<Ontology> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends ResourceAction.GetByIdFail<Ontology> {
  }

  @TypeDefaultAction(state)
  export class Create extends ResourceAction.Create<Ontology> {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends ResourceAction.CreateSuccess<Ontology> {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends ResourceAction.CreateFail<Ontology> {
  }

  @TypeDefaultAction(state)
  export class Update extends ResourceAction.Update<Ontology> {
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends ResourceAction.UpdateSuccess<Ontology> {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends ResourceAction.UpdateFail<Ontology> {
  }

  @TypeDefaultAction(state)
  export class Delete extends ResourceAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends ResourceAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends ResourceAction.DeleteFail {
  }

  @TypeDefaultAction(state)
  export class DeleteList extends ResourceAction.DeleteList {
  }

  @TypeDefaultAction(state)
  export class DeleteListSuccess extends ResourceAction.DeleteListSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteListFail extends ResourceAction.DeleteListFail {
  }

  @TypeDefaultAction(state)
  export class AddInList extends ResourceAction.AddInList<Ontology> {
  }

  @TypeDefaultAction(state)
  export class AddInListById extends ResourceAction.AddInListById {
  }

  @TypeDefaultAction(state)
  export class AddInListByIdSuccess extends ResourceAction.AddInListByIdSuccess<Ontology> {
  }

  @TypeDefaultAction(state)
  export class AddInListByIdFail extends ResourceAction.AddInListByIdFail<Ontology> {
  }

  @TypeDefaultAction(state)
  export class RemoveInListById extends ResourceAction.RemoveInListById {
  }

  @TypeDefaultAction(state)
  export class RemoveInListByListId extends ResourceAction.RemoveInListByListId {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkList extends ResourceAction.LoadNextChunkList {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkListSuccess extends ResourceAction.LoadNextChunkListSuccess<Ontology> {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkListFail extends ResourceAction.LoadNextChunkListFail {
  }

  @TypeDefaultAction(state)
  export class Clean extends ResourceAction.Clean {
  }

  @TypeDefaultAction(state)
  export class GetFile extends ResourceFileAction.GetFile {
  }

  @TypeDefaultAction(state)
  export class GetFileSuccess extends ResourceFileAction.GetFileSuccess {
  }

  @TypeDefaultAction(state)
  export class GetFileFail extends ResourceFileAction.GetFileFail {
  }

  @TypeDefaultAction(state)
  export class GetFileByResId extends ResourceFileAction.GetFileByResId {
  }

  @TypeDefaultAction(state)
  export class GetFileByResIdSuccess extends ResourceFileAction.GetFileByResIdSuccess {
  }

  @TypeDefaultAction(state)
  export class GetFileByResIdFail extends ResourceFileAction.GetFileByResIdFail {
  }

  @TypeDefaultAction(state)
  export class UploadFile extends ResourceFileAction.UploadFile {
    constructor(public resId: string, public fileUploadWrapper: FileUploadWrapper, public modelFormControlEvent: ModelFormControlEvent<Ontology>) {
      super(resId, fileUploadWrapper);
    }
  }

  @TypeDefaultAction(state)
  export class UploadFileSuccess extends ResourceFileAction.UploadFileSuccess {
  }

  @TypeDefaultAction(state)
  export class UploadFileFail extends ResourceFileAction.UploadFileFail {
  }

  @TypeDefaultAction(state)
  export class DeleteFile extends ResourceFileAction.DeleteFile {
  }

  @TypeDefaultAction(state)
  export class DeleteFileSuccess extends ResourceFileAction.DeleteFileSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFileFail extends ResourceFileAction.DeleteFileFail {
  }

  export class GetRdfClasses extends BaseAction {
    static readonly type: string = `[${state}] Get Rdf Classes`;

    constructor(public ontologyId: string) {
      super();
    }
  }

  export class GetRdfClassesSuccess extends BaseSubActionSuccess<GetRdfClasses> {
    static readonly type: string = `[${state}] Get Rdf Classes Success`;

    constructor(public parentAction: GetRdfClasses, public list: string[]) {
      super(parentAction);
    }
  }

  export class GetRdfClassesFail extends BaseSubActionFail<GetRdfClasses> {
    static readonly type: string = `[${state}] Get Rdf Classes Fail`;
  }

  export class GetRdfProperties extends BaseAction {
    static readonly type: string = `[${state}] Get Rdf Properties`;

    constructor(public ontologyId: string) {
      super();
    }
  }

  export class GetRdfPropertiesSuccess extends BaseSubActionSuccess<GetRdfProperties> {
    static readonly type: string = `[${state}] Get Rdf Properties Success`;

    constructor(public parentAction: GetRdfProperties, public list: string[]) {
      super(parentAction);
    }
  }

  export class GetRdfPropertiesFail extends BaseSubActionFail<GetRdfProperties> {
    static readonly type: string = `[${state}] Get Rdf Properties Fail`;
  }

  export class IsPartOf extends BaseAction {
    static readonly type: string = `[${state}] Is Part Of`;

    constructor(public ontologyId: string, public rdfType: string) {
      super();
    }
  }

  export class IsPartOfSuccess extends BaseSubActionSuccess<IsPartOf> {
    static readonly type: string = `[${state}] Is Part Of Success`;

    constructor(public parentAction: IsPartOf, public rdfCategory: Enums.Ontology.RdfCategoryEnum) {
      super(parentAction);
    }
  }

  export class IsPartOfFail extends BaseSubActionFail<IsPartOf> {
    static readonly type: string = `[${state}] Is Part Of Fail`;
    constructor(public parentAction: IsPartOf, public error: SolidifyHttpErrorResponseModel) {
      super(parentAction);
    }
  }
}

export const adminOntologyActionNameSpace: ResourceFileNameSpace = AdminOntologyAction;
