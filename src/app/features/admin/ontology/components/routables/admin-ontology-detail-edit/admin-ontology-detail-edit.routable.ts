/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-ontology-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminOntologyAction,
  adminOntologyActionNameSpace,
} from "@admin/ontology/stores/admin-ontology.action";
import {
  AdminOntologyState,
  AdminOntologyStateModel,
} from "@admin/ontology/stores/admin-ontology.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Enums} from "@enums";
import {Ontology} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {sharedOntologyActionNameSpace} from "@shared/stores/ontology/shared-ontology.action";
import {Observable} from "rxjs";
import {
  AbstractDetailEditCommonRoutable,
  ButtonColorEnum,
  ConfirmDialog,
  ConfirmDialogInputEnum,
  DialogUtil,
  ExtraButtonToolbar,
  MemoizedUtil,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "hedera-ontology-detail-edit-routable",
  templateUrl: "./admin-ontology-detail-edit.routable.html",
  styleUrls: ["./admin-ontology-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminOntologyDetailEditRoutable extends AbstractDetailEditCommonRoutable<Ontology, AdminOntologyStateModel> {
  @Select(AdminOntologyState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminOntologyState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  listRdfClassesObs: Observable<string[]> = MemoizedUtil.select(this._store, AdminOntologyState, state => state.listRdfClasses);
  listRdfPropertiesObs: Observable<string[]> = MemoizedUtil.select(this._store, AdminOntologyState, state => state.listRdfProperties);

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedOntologyActionNameSpace;

  readonly KEY_PARAM_NAME: keyof Ontology & string = "name";

  listExtraButtons: ExtraButtonToolbar<Ontology> [] = [
    {
      color: "primary",
      icon: IconNameEnum.check,
      callback: () => this._checkIsPartOf(),
      labelToTranslate: (current) => LabelTranslateEnum.checkIsPartOf,
      order: 40,
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_ontology, _injector, adminOntologyActionNameSpace, StateEnum.admin);
    this.editAvailable = this._securityService.isRootOrAdmin();
    this.deleteAvailable = this._securityService.isRootOrAdmin();
  }

  _getSubResourceWithParentId(id: string): void {
  }

  private _checkIsPartOf(): void {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: LabelTranslateEnum.checkIsPartOf,
      inputLabelToTranslate: LabelTranslateEnum.rdfTypeLabel,
      confirmButtonToTranslate: LabelTranslateEnum.check,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.primary,
      withInput: ConfirmDialogInputEnum.INPUT_TEXT,
      inputRequired: true,
    }, undefined, (rdfType: Enums.Ontology.RdfCategoryEnum) => {
      this._store.dispatch(new AdminOntologyAction.IsPartOf(this._resId, rdfType));
    }));
  }

  loadRdfDetail(): void {
    this._store.dispatch([
      new AdminOntologyAction.GetRdfClasses(this._resId),
      new AdminOntologyAction.GetRdfProperties(this._resId),
    ]);
  }
}
