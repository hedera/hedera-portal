/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-ontology-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminOntologyAction} from "@admin/ontology/stores/admin-ontology.action";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
  Output,
  ViewChild,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {Enums} from "@enums";
import {
  DataFile,
  Ontology,
} from "@models";
import {SharedAbstractFormPresentational} from "@shared/components/presentationals/shared-abstract-form/shared-abstract.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {DataFileHelper} from "@shared/helpers/data-file.helper";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  DataTableColumns,
  DataTableFieldTypeEnum,
  isFalse,
  isNotNullNorUndefined,
  KeyValue,
  ObservableUtil,
  OrderEnum,
  PropertyName,
  RegexUtil,
  ResourceFileNameSpace,
  SolidifyDataFileModel,
  SolidifyFileUploadInputRequiredValidator,
  SolidifyFileUploadStatus,
  SolidifyValidator,
  SsrUtil,
  Tab,
  TabsContainer,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-ontology-form",
  templateUrl: "./admin-ontology-form.presentational.html",
  styleUrls: ["./admin-ontology-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminOntologyFormPresentational extends SharedAbstractFormPresentational<Ontology> {
  readonly TAB_RDF_CLASSES: string = "RDF_CLASSES";
  readonly TAB_RDF_PROPERTIES: string = "RDF_PROPERTIES";

  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  ontologyFormatsEnum: KeyValue[] = Enums.FormatEnumTranslate;

  adminOntologyActionNameSpace: ResourceFileNameSpace = AdminOntologyAction;

  private _listRdfClasses: string[];

  @Input()
  set listRdfClasses(value: string[]) {
    this._listRdfClasses = value;
    this.tabsContainer?.externalDetectChanges();
  }

  get listRdfClasses(): string[] {
    return this._listRdfClasses;
  }

  private _listRdfProperties: string[];

  @Input()
  set listRdfProperties(value: string[]) {
    this._listRdfProperties = value;
    this.tabsContainer?.externalDetectChanges();
  }

  get listRdfProperties(): string[] {
    return this._listRdfProperties;
  }

  private readonly _loadRdfDetailBS: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  @Output("loadRdfDetailChange")
  readonly loadRdfDetailObs: Observable<boolean> = ObservableUtil.asObservable(this._loadRdfDetailBS);

  @ViewChild("tabs")
  tabsContainer: TabsContainer;

  tabsRdf: Tab[] = [
    {
      id: this.TAB_RDF_CLASSES,
      icon: IconNameEnum.rdfClasses,
      titleToTranslate: LabelTranslateEnum.rdfClasses,
      numberNew: () => this.listRdfClasses?.length,
    },
    {
      id: this.TAB_RDF_PROPERTIES,
      icon: IconNameEnum.rdfProperties,
      titleToTranslate: LabelTranslateEnum.rdfProperties,
      numberNew: () => this.listRdfProperties?.length,
    },
  ];

  columns: DataTableColumns<string>[] = [
    {
      field: undefined,
      header: LabelTranslateEnum.nameLabel,
      type: DataTableFieldTypeEnum.string,
      order: OrderEnum.ascending,
      translate: false,
      isSortable: true,
      isFilterable: false,
    },
  ];

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  dataFileAdapter: (dataFile: DataFile) => SolidifyDataFileModel = DataFileHelper.dataFileAdapter;

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.version]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.baseUri]: ["", [Validators.required, SolidifyValidator, Validators.pattern(RegexUtil.uri)]],
      [this.formDefinition.url]: ["", [SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.external]: [true, [SolidifyValidator]],
      [this.formDefinition.format]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: ["", [SolidifyValidator]],
      [this.formDefinition.ontologyFileChange]: [undefined, [Validators.required, SolidifyFileUploadInputRequiredValidator, SolidifyValidator]],
    });
  }

  protected _bindFormTo(ontology: Ontology): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: [ontology.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.version]: [ontology.version, [Validators.required, SolidifyValidator]],
      [this.formDefinition.baseUri]: [ontology.baseUri, [Validators.required, SolidifyValidator, Validators.pattern(RegexUtil.uri)]],
      [this.formDefinition.url]: [ontology.url, [SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.external]: [ontology.external, [SolidifyValidator]],
      [this.formDefinition.format]: [ontology.format, [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: [ontology.description, [SolidifyValidator]],
      [this.formDefinition.ontologyFileChange]: [isNotNullNorUndefined(ontology.ontologyFile) ? SolidifyFileUploadStatus.UNCHANGED : undefined, [SolidifyFileUploadInputRequiredValidator, SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(ontology: Ontology): Ontology {
    return ontology;
  }

  openExternal(url: string): void {
    SsrUtil.window.open(url, "_blank");
  }

  loadRdfDetail(): void {
    if (isFalse(this._loadRdfDetailBS.value)) {
      this._loadRdfDetailBS.next(true);
    }
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() version: string;
  @PropertyName() description: string;
  @PropertyName() baseUri: string;
  @PropertyName() url: string;
  @PropertyName() external: string;
  @PropertyName() format: string;
  @PropertyName() ontologyFileChange: string;
}
