/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-ontology.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminOntologyRoutingModule} from "@admin/ontology/admin-ontology-routing.module";
import {AdminOntologyFormPresentational} from "@admin/ontology/components/presentationals/admin-ontology-form/admin-ontology-form.presentational";
import {AdminOntologyCreateRoutable} from "@admin/ontology/components/routables/admin-ontology-create/admin-ontology-create.routable";
import {AdminOntologyDetailEditRoutable} from "@admin/ontology/components/routables/admin-ontology-detail-edit/admin-ontology-detail-edit.routable";
import {AdminOntologyListRoutable} from "@admin/ontology/components/routables/admin-ontology-list/admin-ontology-list.routable";
import {AdminOntologyState} from "@admin/ontology/stores/admin-ontology.state";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

const routables = [
  AdminOntologyCreateRoutable,
  AdminOntologyDetailEditRoutable,
  AdminOntologyListRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminOntologyFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminOntologyRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminOntologyState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminOntologyModule {
}
