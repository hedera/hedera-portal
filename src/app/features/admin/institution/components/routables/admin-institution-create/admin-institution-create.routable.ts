/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-institution-create.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {adminInstitutionActionNameSpace} from "@admin/institution/stores/admin-institution.action";
import {
  AdminInstitutionState,
  AdminInstitutionStateModel,
} from "@admin/institution/stores/admin-institution.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {
  Institution,
  Role,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {sharedInstitutionActionNameSpace} from "@shared/stores/institution/shared-institution.action";
import {SharedRoleState} from "@shared/stores/role/shared-role.state";
import {Observable} from "rxjs";
import {
  AbstractCreateRoutable,
  MemoizedUtil,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-institution-create-routable",
  templateUrl: "./admin-institution-create.routable.html",
  styleUrls: ["./admin-institution-create.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminInstitutionCreateRoutable extends AbstractCreateRoutable<Institution, AdminInstitutionStateModel> implements OnInit {
  @Select(AdminInstitutionState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminInstitutionState.isReadyToBeDisplayedInCreateMode) isReadyToBeDisplayedInCreateModeObs: Observable<boolean>;

  listRoleObs: Observable<Role[]> = MemoizedUtil.list(this._store, SharedRoleState);

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedInstitutionActionNameSpace;

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector) {
    super(_store, _actions$, _changeDetector, StateEnum.admin_institution, _injector, adminInstitutionActionNameSpace, StateEnum.admin);
  }

  navigate($event: string[]): void {
    this._store.dispatch(new Navigate($event));
  }
}
