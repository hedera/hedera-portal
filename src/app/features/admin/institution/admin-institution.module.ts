/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-institution.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {AdminModule} from "@admin/admin.module";
import {AdminInstitutionRoutingModule} from "@admin/institution/admin-institution-routing.module";
import {AdminInstitutionFormPresentational} from "@admin/institution/components/presentationals/admin-institution-form/admin-institution-form.presentational";
import {AdminInstitutionCreateRoutable} from "@admin/institution/components/routables/admin-institution-create/admin-institution-create.routable";
import {AdminInstitutionDetailEditRoutable} from "@admin/institution/components/routables/admin-institution-detail-edit/admin-institution-detail-edit.routable";
import {AdminInstitutionListRoutable} from "@admin/institution/components/routables/admin-institution-list/admin-institution-list.routable";
import {AdminInstitutionState} from "@admin/institution/stores/admin-institution.state";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

const routables = [
  AdminInstitutionListRoutable,
  AdminInstitutionDetailEditRoutable,
  AdminInstitutionCreateRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminInstitutionFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminInstitutionRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminInstitutionState,
    ]),
    AdminModule,
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminInstitutionModule {
}
