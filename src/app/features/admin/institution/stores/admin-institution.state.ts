/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-institution.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  AdminInstitutionAction,
  adminInstitutionActionNameSpace,
} from "@admin/institution/stores/admin-institution.action";
import {
  AdminInstitutionProjectState,
  AdminInstitutionProjectStateModel,
} from "@admin/institution/stores/project/admin-institution-project.state";
import {Injectable} from "@angular/core";
import {AppAuthorizedProjectAction} from "@app/stores/authorized-project/app-authorized-project.action";
import {environment} from "@environments/environment";
import {Institution} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {
  ActionSubActionCompletionsWrapper,
  ApiService,
  defaultAssociationStateInitValue,
  defaultResourceFileStateInitValue,
  DispatchMethodEnum,
  DownloadService,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ofSolidifyActionCompleted,
  OverrideDefaultAction,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";
import {AdminInstitutionProjectAction} from "./project/admin-institution-project.action";

export interface AdminInstitutionStateModel extends ResourceFileStateModel<Institution> {
  [StateEnum.admin_institution_project]: AdminInstitutionProjectStateModel;
}

@Injectable()
@State<AdminInstitutionStateModel>({
    name: StateEnum.admin_institution,
    defaults: {
      ...defaultResourceFileStateInitValue(),
      [StateEnum.admin_institution_project]: {...defaultAssociationStateInitValue()},
    },
    children: [
      AdminInstitutionProjectState,
    ],
  },
)
export class AdminInstitutionState extends ResourceFileState<AdminInstitutionStateModel, Institution> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService,
              private readonly _securityService: SecurityService,
  ) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: adminInstitutionActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminInstitutionDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminInstitutionDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminInstitution,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.institution.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.institution.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.institution.notification.resource.update"),
      updateSubResourceDispatchMethod: DispatchMethodEnum.SEQUENCIAL,
    }, _downloadService, ResourceFileStateModeEnum.logo, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminInstitutions;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static isLoading(state: AdminInstitutionStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminInstitutionStateModel): boolean {
    return this.isLoading(state)
      || StoreUtil.isLoadingState(state[StateEnum.admin_institution_project]);
  }

  @Selector()
  static currentTitle(state: AdminInstitutionStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminInstitutionStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && isNotNullNorUndefined(state.current)
      && isNotNullNorUndefined(state[StateEnum.admin_institution_project].selected);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminInstitutionStateModel): boolean {
    return true;
  }

  @OverrideDefaultAction()
  @Action(AdminInstitutionAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<AdminInstitutionStateModel>, action: AdminInstitutionAction.GetByIdSuccess): void {
    super.getByIdSuccess(ctx, action);
    if (isNotNullNorUndefined(action.model.logo)) {
      ctx.dispatch(new AdminInstitutionAction.GetFile(action.model.resId));
    }
  }

  protected override _getListActionsUpdateSubResource(model: Institution, action: AdminInstitutionAction.Create | AdminInstitutionAction.Update, ctx: SolidifyStateContext<AdminInstitutionStateModel>): ActionSubActionCompletionsWrapper[] {
    const actions = super._getListActionsUpdateSubResource(model, action, ctx);

    const institutionId = model.resId;

    const newProjectIds = action.modelFormControlEvent.model.projects.map(p => p.resId);

    actions.push(...[
      {
        action: new AdminInstitutionProjectAction.Update(institutionId, newProjectIds),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminInstitutionProjectAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminInstitutionProjectAction.UpdateFail)),
        ],
      },
    ]);

    actions.push(...[
      {
        action: new AppAuthorizedProjectAction.GetAll(undefined, false, false),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedProjectAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedProjectAction.GetAllFail)),
        ],
      },
    ]);

    return actions;
  }
}
