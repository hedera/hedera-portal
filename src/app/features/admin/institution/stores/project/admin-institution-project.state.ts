/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-institution-project.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {adminInstitutionProjectNamespace} from "@app/features/admin/institution/stores/project/admin-institution-project.action";
import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {
  Project,
} from "@models";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  AssociationState,
  AssociationStateModel,
  defaultAssociationStateInitValue,
  NotificationService,
} from "solidify-frontend";

export interface AdminInstitutionProjectStateModel extends AssociationStateModel<Project> {
}

@Injectable()
@State<AdminInstitutionProjectStateModel>({
  name: StateEnum.admin_institution_project,
  defaults: {
    ...defaultAssociationStateInitValue(),
  },
})
export class AdminInstitutionProjectState extends AssociationState<AdminInstitutionProjectStateModel, Project> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: adminInstitutionProjectNamespace,
      resourceName: ApiResourceNameEnum.PROJECT,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminInstitutions;
  }
}
