/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-person-create.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {adminPersonActionNameSpace} from "@admin/person/stores/admin-person.action";
import {
  AdminPersonState,
  AdminPersonStateModel,
} from "@admin/person/stores/admin-person.state";
import {AdminPersonInstitutionsState} from "@admin/person/stores/institutions/admin-people-institutions.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {
  Institution,
  Person,
  Role,
} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {sharedPersonActionNameSpace} from "@shared/stores/person/shared-person.action";
import {SharedRoleState} from "@shared/stores/role/shared-role.state";
import {Observable} from "rxjs";
import {
  AbstractCreateRoutable,
  MemoizedUtil,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-person-create-routable",
  templateUrl: "./admin-person-create.routable.html",
  styleUrls: ["./admin-person-create.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminPersonCreateRoutable extends AbstractCreateRoutable<Person, AdminPersonStateModel> {
  @Select(AdminPersonState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminPersonState.isReadyToBeDisplayedInCreateMode) isReadyToBeDisplayedInCreateModeObs: Observable<boolean>;
  selectedInstitutionsObs: Observable<Institution[]> = MemoizedUtil.selected(this._store, AdminPersonInstitutionsState);
  listRoleObs: Observable<Role[]> = MemoizedUtil.list(this._store, SharedRoleState);

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedPersonActionNameSpace;

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector) {
    super(_store, _actions$, _changeDetector, StateEnum.admin_person, _injector, adminPersonActionNameSpace, StateEnum.admin);
  }
}
