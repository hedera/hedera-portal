/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-person-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ProjectRole} from "@admin/models/project-role.model";
import {AdminPersonAction} from "@admin/person/stores/admin-person.action";
import {AdminPersonState} from "@admin/person/stores/admin-person.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {sharedInstitutionActionNameSpace} from "@app/shared/stores/institution/shared-institution.action";
import {
  Institution,
  Person,
  Role,
  User,
} from "@models";
import {SharedAbstractFormPresentational} from "@shared/components/presentationals/shared-abstract-form/shared-abstract.presentational";
import {SharedInstitutionOverlayPresentational} from "@shared/components/presentationals/shared-institution-overlay/shared-institution-overlay.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {SharedInstitutionState} from "@shared/stores/institution/shared-institution.state";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
  OrderEnum,
  PropertyName,
  ResourceFileNameSpace,
  ResourceNameSpace,
  SolidifyValidator,
  Sort,
  Type,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-person-form",
  templateUrl: "./admin-person-form.presentational.html",
  styleUrls: ["./admin-person-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminPersonFormPresentational extends SharedAbstractFormPresentational<Person> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  personAvatarActionNameSpace: ResourceFileNameSpace = AdminPersonAction;
  personState: typeof AdminPersonState = AdminPersonState;

  @Input()
  selectedProjectRole: ProjectRole[];

  @Input()
  selectedInstitutions: Institution[];

  @Input()
  listRole: Role[];

  sharedInstitutionSort: Sort<Institution> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedInstitutionActionNameSpace: ResourceNameSpace = sharedInstitutionActionNameSpace;
  sharedInstitutionState: typeof SharedInstitutionState = SharedInstitutionState;

  institutionOverlayComponent: Type<SharedInstitutionOverlayPresentational> = SharedInstitutionOverlayPresentational;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get personResId(): string {
    return this.model?.resId;
  }

  get isAvatarPresent(): boolean {
    return isNotNullNorUndefined(this.model?.avatar);
  }

  getUser(): User {
    if (isNullOrUndefined(this.form)) {
      return this.model;
    }
    return this.form.value as User;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _bindFormTo(person: Person): void {
    this.form = this._fb.group({
      [this.formDefinition.firstName]: [person.firstName, [Validators.required, SolidifyValidator]],
      [this.formDefinition.lastName]: [person.lastName, [Validators.required, SolidifyValidator]],
      [this.formDefinition.orcid]: [person.orcid, [SolidifyValidator]],
      [this.formDefinition.projectRole]: [[], [SolidifyValidator]],
      [this.formDefinition.institutions]: [this.selectedInstitutions.map(i => i.resId)],
    });
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.firstName]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.lastName]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.orcid]: ["", [SolidifyValidator]],
      [this.formDefinition.projectRole]: [[], [SolidifyValidator]],
      [this.formDefinition.institutions]: [],
    });
  }

  protected _treatmentBeforeSubmit(person: Person): Person {
    person.institutions = [];
    const listInstitutions = this.form.get(this.formDefinition.institutions).value;
    if (!isNullOrUndefined(listInstitutions)) {
      listInstitutions.forEach(resId => {
        person.institutions.push({resId: resId});
      });
    }
    return person;
  }

  navigateToInstitution(institution: Institution): void {
    this.navigate([RoutesEnum.adminInstitutionDetail, institution.resId]);
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() firstName: string;
  @PropertyName() lastName: string;
  @PropertyName() orcid: string;
  @PropertyName() projectRole: string;
  @PropertyName() institutions: string;
}
