/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-person.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminPersonAction,
  adminPersonActionNameSpace,
} from "@admin/person/stores/admin-person.action";
import {AdminPersonInstitutionsAction} from "@admin/person/stores/institutions/admin-people-institutions.action";
import {
  AdminPersonInstitutionsState,
  AdminPersonInstitutionsStateModel,
} from "@admin/person/stores/institutions/admin-people-institutions.state";
import {AdminPersonProjectRoleAction} from "@admin/person/stores/person-project-role/admin-person-project-role.action";
import {
  AdminPersonProjectRoleState,
  AdminPersonProjectRoleStateModel,
  defaultAdminPersonProjectRoleStateModel,
} from "@admin/person/stores/person-project-role/admin-person-project-role.state";
import {Injectable} from "@angular/core";
import {AppAuthorizedProjectAction} from "@app/stores/authorized-project/app-authorized-project.action";
import {AppUserState} from "@app/stores/user/app-user.state";
import {environment} from "@environments/environment";
import {Person} from "@models";
import {
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ActionSubActionCompletionsWrapper,
  ApiService,
  defaultAssociationStateInitValue,
  defaultResourceFileStateInitValue,
  DispatchMethodEnum,
  DownloadService,
  isArray,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  Relation3TiersForm,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";

export interface AdminPersonStateModel extends ResourceFileStateModel<Person> {
  admin_person_projectRole?: AdminPersonProjectRoleStateModel;
  admin_person_institutions?: AdminPersonInstitutionsStateModel;
}

@Injectable()
@State<AdminPersonStateModel>({
  name: StateEnum.admin_person,
  defaults: {
    ...defaultResourceFileStateInitValue(),
    admin_person_projectRole: {...defaultAdminPersonProjectRoleStateModel()},
    admin_person_institutions: {...defaultAssociationStateInitValue()},
  },
  children: [
    AdminPersonProjectRoleState,
    AdminPersonInstitutionsState,
  ],
})
export class AdminPersonState extends ResourceFileState<AdminPersonStateModel, Person> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: adminPersonActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminPersonDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminPersonDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminPerson,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.person.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.person.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.person.notification.resource.update"),
      updateSubResourceDispatchMethod: DispatchMethodEnum.PARALLEL,
    }, _downloadService, ResourceFileStateModeEnum.avatar, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminPeople;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static isLoading(state: AdminPersonStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminPersonStateModel): boolean {
    return this.isLoading(state)
      || StoreUtil.isLoadingState(state.admin_person_projectRole)
      || StoreUtil.isLoadingState(state.admin_person_institutions);
  }

  @Selector()
  static currentTitle(state: AdminPersonStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.fullName;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminPersonStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current)
      && !isNullOrUndefined(state.admin_person_projectRole.selected)
      && !isNullOrUndefined(state.admin_person_institutions.selected);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminPersonStateModel): boolean {
    return true;
  }

  protected override _getListActionsUpdateSubResource(model: Person, action: AdminPersonAction.Create | AdminPersonAction.Update, ctx: SolidifyStateContext<AdminPersonStateModel>): ActionSubActionCompletionsWrapper[] {
    const actions = super._getListActionsUpdateSubResource(model, action, ctx);

    const currentPersonId = MemoizedUtil.currentSnapshot(this._store, AppUserState)?.person?.resId;
    const personId = model.resId;
    const concernCurrentUser = currentPersonId === personId;

    const newProjectRole = action.modelFormControlEvent.formControl.get("projectRole").value as Relation3TiersForm[];
    newProjectRole.forEach((elem) => {
      if (!isNullOrUndefined(elem.listId) && !isArray(elem.listId)) {
        elem.listId = [elem.listId];
      }
    });

    const newInstitutions = action.modelFormControlEvent.model.institutions.map(o => o.resId);

    actions.push(...[
      {
        action: new AdminPersonProjectRoleAction.Update(personId, newProjectRole),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonProjectRoleAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonProjectRoleAction.UpdateFail)),
        ],
      },
      {
        action: new AdminPersonInstitutionsAction.Update(personId, newInstitutions),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonInstitutionsAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonInstitutionsAction.UpdateFail)),
        ],
      },
    ]);

    if (concernCurrentUser) {
      actions.push(...[
        {
          action: new AppAuthorizedProjectAction.GetAll(undefined, false, false),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedProjectAction.GetAllSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedProjectAction.GetAllFail)),
          ],
        },
      ]);
    }

    return actions;
  }
}
