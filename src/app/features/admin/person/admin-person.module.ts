/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-person.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {AdminPersonFormPresentational} from "@admin/person/components/presentationals/admin-person-form/admin-person-form.presentational";
import {AdminPersonCreateRoutable} from "@admin/person/components/routables/admin-person-create/admin-person-create.routable";
import {AdminPersonDetailEditRoutable} from "@admin/person/components/routables/admin-person-detail-edit/admin-person-detail-edit.routable";
import {AdminPersonListRoutable} from "@admin/person/components/routables/admin-person-list/admin-person-list.routable";
import {AdminPersonState} from "@admin/person/stores/admin-person.state";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {AdminPersonRoutingModule} from "./admin-person-routing.module";

const routables = [
  AdminPersonListRoutable,
  AdminPersonDetailEditRoutable,
  AdminPersonCreateRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminPersonFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminPersonRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminPersonState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminPersonModule {
}
