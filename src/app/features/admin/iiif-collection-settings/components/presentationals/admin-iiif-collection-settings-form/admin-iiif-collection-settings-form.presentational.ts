/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-iiif-collection-settings-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {
  IIIFCollectionSettings,
  Project,
} from "@models";
import {SharedAbstractFormPresentational} from "@shared/components/presentationals/shared-abstract-form/shared-abstract.presentational";
import {SharedProjectOverlayPresentational} from "@shared/components/presentationals/shared-project-overlay/shared-project-overlay.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {sharedProjectActionNameSpace} from "@shared/stores/project/shared-project.action";
import {SharedProjectState} from "@shared/stores/project/shared-project.state";
import {
  isNullOrUndefinedOrWhiteString,
  OrderEnum,
  PropertyName,
  ResourceNameSpace,
  SolidifyValidator,
  Sort,
  Type,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-iiif-collection-settings-form",
  templateUrl: "./admin-iiif-collection-settings-form.presentational.html",
  styleUrls: ["./admin-iiif-collection-settings-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminIIIFCollectionSettingsFormPresentational extends SharedAbstractFormPresentational<IIIFCollectionSettings> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  sharedProjectSort: Sort<Project> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedProjectActionNameSpace: ResourceNameSpace = sharedProjectActionNameSpace;
  sharedProjectState: typeof SharedProjectState = SharedProjectState;

  projectCallback: (value: Project) => string = (value: Project) => value.name;

  projectOverlayComponent: Type<SharedProjectOverlayPresentational> = SharedProjectOverlayPresentational;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.iiifCollectionSparqlQuery]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.project]: ["", [Validators.required, SolidifyValidator]],
    });
  }

  protected _bindFormTo(iiifCollectionSettings: IIIFCollectionSettings): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: [iiifCollectionSettings.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: [iiifCollectionSettings.description, [Validators.required, SolidifyValidator]],
      [this.formDefinition.iiifCollectionSparqlQuery]: [iiifCollectionSettings.iiifCollectionSparqlQuery, [Validators.required, SolidifyValidator]],
      [this.formDefinition.project]: [iiifCollectionSettings.project?.resId, [Validators.required, SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(iiifCollectionSettings: IIIFCollectionSettings): IIIFCollectionSettings {
    const projectId = this.form.get(this.formDefinition.project).value;
    if (isNullOrUndefinedOrWhiteString(projectId)) {
      iiifCollectionSettings.project = null;
    } else {
      iiifCollectionSettings.project = {resId: projectId};
    }
    return iiifCollectionSettings;
  }

  navigateToProject(projectId: string): void {
    this.navigate([RoutesEnum.adminProjectDetail, projectId]);
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() description: string;
  @PropertyName() iiifCollectionSparqlQuery: string;
  @PropertyName() project: string;
}
