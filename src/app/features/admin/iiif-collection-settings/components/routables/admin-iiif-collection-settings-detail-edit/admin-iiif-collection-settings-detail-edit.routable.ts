/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-iiif-collection-settings-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminIIIFCollectionSettingsActionNameSpace} from "@admin/iiif-collection-settings/stores/admin-iiif-collection-settings.action";
import {
  AdminIIIFCollectionSettingsState,
  AdminIIIFCollectionSettingsStateModel,
} from "@admin/iiif-collection-settings/stores/admin-iiif-collection-settings.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {IIIFCollectionSettings} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {AbstractDetailEditCommonRoutable} from "solidify-frontend";

@Component({
  selector: "hedera-admin-iiif-collection-settings-edit-routable",
  templateUrl: "./admin-iiif-collection-settings-detail-edit.routable.html",
  styleUrls: ["./admin-iiif-collection-settings-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminIIIFCollectionSettingsDetailEditRoutable extends AbstractDetailEditCommonRoutable<IIIFCollectionSettings, AdminIIIFCollectionSettingsStateModel> {
  @Select(AdminIIIFCollectionSettingsState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminIIIFCollectionSettingsState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  readonly KEY_PARAM_NAME: keyof IIIFCollectionSettings & string = "name";

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_iiifCollectionSettings, _injector, adminIIIFCollectionSettingsActionNameSpace, StateEnum.admin);
    this.editAvailable = this._securityService.isRootOrAdmin();
    this.deleteAvailable = this._securityService.isRootOrAdmin();
  }

  _getSubResourceWithParentId(id: string): void {
  }
}
