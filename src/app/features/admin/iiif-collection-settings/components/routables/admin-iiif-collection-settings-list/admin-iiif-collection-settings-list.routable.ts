/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-iiif-collection-settings-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminIIIFCollectionSettingsActionNameSpace} from "@admin/iiif-collection-settings/stores/admin-iiif-collection-settings.action";
import {AdminIIIFCollectionSettingsStateModel} from "@admin/iiif-collection-settings/stores/admin-iiif-collection-settings.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import { sharedProjectActionNameSpace } from "@app/shared/stores/project/shared-project.action";
import {
  IIIFCollectionSettings,
  Project,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {SharedProjectState} from "@shared/stores/project/shared-project.state";
import {
  AbstractListRoutable,
  DataTableFieldTypeEnum,
  OrderEnum,
  ResourceNameSpace,
  RouterExtensionService,
  Sort,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-iiif-collection-settings-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./admin-iiif-collection-settings-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminIIIFCollectionSettingsListRoutable extends AbstractListRoutable<IIIFCollectionSettings, AdminIIIFCollectionSettingsStateModel> {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToAdmin;
  readonly KEY_PARAM_NAME: keyof IIIFCollectionSettings & string = "name";

  sharedProjectSort: Sort<Project> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedProjectActionNameSpace: ResourceNameSpace = sharedProjectActionNameSpace;
  sharedProjectState: typeof SharedProjectState = SharedProjectState;

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.admin_iiifCollectionSettings, adminIIIFCollectionSettingsActionNameSpace, _injector, {
      canCreate: _securityService.isRootOrAdmin(),
    }, StateEnum.admin);
  }

  conditionDisplayEditButton(model: IIIFCollectionSettings | undefined): boolean {
    return this._securityService.isRootOrAdmin();
  }

  conditionDisplayDeleteButton(model: IIIFCollectionSettings | undefined): boolean {
    return this._securityService.isRootOrAdmin();
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "project.resId" as any,
        header: LabelTranslateEnum.project,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        resourceNameSpace: this.sharedProjectActionNameSpace,
        resourceState: this.sharedProjectState as any,
        searchableSingleSelectSort: this.sharedProjectSort,
        resourceLabelCallback: (value: Project) => value.name,
        resourceLabelKey: "name",
      },
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "description",
        header: LabelTranslateEnum.description,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
    ];
  }
}
