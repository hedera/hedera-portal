/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-iiif-collection-settings.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminIIIFCollectionSettingsRoutingModule} from "@admin/iiif-collection-settings/admin-iiif-collection-settings-routing.module";
import {AdminIIIFCollectionSettingsFormPresentational} from "@admin/iiif-collection-settings/components/presentationals/admin-iiif-collection-settings-form/admin-iiif-collection-settings-form.presentational";
import {AdminIIIFCollectionSettingsCreateRoutable} from "@admin/iiif-collection-settings/components/routables/admin-iiif-collection-settings-create/admin-iiif-collection-settings-create.routable";
import {AdminIIIFCollectionSettingsDetailEditRoutable} from "@admin/iiif-collection-settings/components/routables/admin-iiif-collection-settings-detail-edit/admin-iiif-collection-settings-detail-edit.routable";
import {AdminIIIFCollectionSettingsListRoutable} from "@admin/iiif-collection-settings/components/routables/admin-iiif-collection-settings-list/admin-iiif-collection-settings-list.routable";
import {AdminIIIFCollectionSettingsState} from "@admin/iiif-collection-settings/stores/admin-iiif-collection-settings.state";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

const routables = [
  AdminIIIFCollectionSettingsListRoutable,
  AdminIIIFCollectionSettingsCreateRoutable,
  AdminIIIFCollectionSettingsDetailEditRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminIIIFCollectionSettingsFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminIIIFCollectionSettingsRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminIIIFCollectionSettingsState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminIIIFCollectionSettingsModule {
}
