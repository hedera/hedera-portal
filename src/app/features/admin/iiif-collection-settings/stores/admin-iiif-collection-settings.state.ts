/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-iiif-collection-settings.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminIIIFCollectionSettingsActionNameSpace} from "@admin/iiif-collection-settings/stores/admin-iiif-collection-settings.action";
import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {IIIFCollectionSettings} from "@models";
import {
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  defaultResourceStateInitValue,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ResourceState,
  ResourceStateModel,
  StoreUtil,
} from "solidify-frontend";

export interface AdminIIIFCollectionSettingsStateModel extends ResourceStateModel<IIIFCollectionSettings> {
}

@Injectable()
@State<AdminIIIFCollectionSettingsStateModel>({
  name: StateEnum.admin_iiifCollectionSettings,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
})
export class AdminIIIFCollectionSettingsState extends ResourceState<AdminIIIFCollectionSettingsStateModel, IIIFCollectionSettings> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: adminIIIFCollectionSettingsActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminIIIFCollectionSettingsDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminIIIFCollectionSettingsDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminIIIFCollectionSettings,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.iiifCollectionSettings.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.iiifCollectionSettings.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.iiifCollectionSettings.notification.resource.update"),
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminIIIFCollectionSettings;
  }

  @Selector()
  static currentName(state: AdminIIIFCollectionSettingsStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @Selector()
  static isLoading(state: AdminIIIFCollectionSettingsStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminIIIFCollectionSettingsStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminIIIFCollectionSettingsStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminIIIFCollectionSettingsStateModel): boolean {
    return true;
  }

}
