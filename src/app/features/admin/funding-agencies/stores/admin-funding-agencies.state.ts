/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-funding-agencies.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  AdminFundingAgenciesAction,
  adminFundingAgenciesActionNameSpace,
} from "@admin/funding-agencies/stores/admin-funding-agencies.action";
import {
  AdminFundingAgenciesProjectState,
  AdminFundingAgenciesProjectStateModel,
} from "@admin/funding-agencies/stores/project/admin-project-funding-agencies.state";
import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {FundingAgency} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  Observable,
  of,
  switchMap,
} from "rxjs";
import {map} from "rxjs/operators";
import {
  ActionSubActionCompletionsWrapper,
  ApiService,
  defaultAssociationStateInitValue,
  defaultResourceFileStateInitValue,
  DispatchMethodEnum,
  DownloadService,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ofSolidifyActionCompleted,
  OverrideDefaultAction,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";
import {
  AdminFundingAgenciesProjectAction,
  adminFundingAgenciesProjectActionNameSpace,
} from "./project/admin-project-funding-agencies.action";

export interface AdminFundingAgenciesStateModel extends ResourceFileStateModel<FundingAgency> {
  admin_fundingAgencies_project: AdminFundingAgenciesProjectStateModel;
}

@Injectable()
@State<AdminFundingAgenciesStateModel>({
  name: StateEnum.admin_fundingAgencies,
  defaults: {
    ...defaultResourceFileStateInitValue(),
    admin_fundingAgencies_project: {...defaultAssociationStateInitValue()},
  },
  children: [
    AdminFundingAgenciesProjectState,
  ],
})
export class AdminFundingAgenciesState extends ResourceFileState<AdminFundingAgenciesStateModel, FundingAgency> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: adminFundingAgenciesActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminFundingAgenciesDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminFundingAgenciesDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminFundingAgencies,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.funding-agencies.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.funding-agencies.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.funding-agencies.notification.resource.update"),
      updateSubResourceDispatchMethod: DispatchMethodEnum.PARALLEL,
    }, _downloadService, ResourceFileStateModeEnum.logo, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminFundingAgencies;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static isLoading(state: AdminFundingAgenciesStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminFundingAgenciesStateModel): boolean {
    return this.isLoading(state)
      || StoreUtil.isLoadingState(state.admin_fundingAgencies_project);
  }

  @Selector()
  static currentTitle(state: AdminFundingAgenciesStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminFundingAgenciesStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current)
      && !isNullOrUndefined(state.admin_fundingAgencies_project.selected);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminFundingAgenciesStateModel): boolean {
    return true;
  }

  protected override _getListActionsUpdateSubResource(model: FundingAgency, action: AdminFundingAgenciesAction.Create | AdminFundingAgenciesAction.Update, ctx: SolidifyStateContext<AdminFundingAgenciesStateModel>): ActionSubActionCompletionsWrapper[] {
    const actions = super._getListActionsUpdateSubResource(model, action, ctx);

    const fundingAgencyId = model.resId;
    const newOrgUnit = action.modelFormControlEvent.model.projects.map(o => o.resId);
    actions.push({
      action: new AdminFundingAgenciesProjectAction.Update(fundingAgencyId, newOrgUnit),
      subActionCompletions: [
        this._actions$.pipe(ofSolidifyActionCompleted(AdminFundingAgenciesProjectAction.UpdateSuccess)),
        this._actions$.pipe(ofSolidifyActionCompleted(AdminFundingAgenciesProjectAction.UpdateFail)),
      ],
    });

    return actions;
  }

  @OverrideDefaultAction()
  @Action(AdminFundingAgenciesAction.Delete)
  delete(ctx: SolidifyStateContext<AdminFundingAgenciesStateModel>, action: AdminFundingAgenciesAction.Delete): Observable<FundingAgency> {
    const oldOrgUnit = ctx.getState().admin_fundingAgencies_project.selected.map(o => o.resId);

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(
      ctx,
      [
        {
          action: new adminFundingAgenciesProjectActionNameSpace.DeleteList(action.resId, oldOrgUnit),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(adminFundingAgenciesProjectActionNameSpace.DeleteListSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(adminFundingAgenciesProjectActionNameSpace.DeleteListFail)),
          ],
        },
      ],
    ).pipe(
      map(result => result.success),
      switchMap(success => {
        if (success) {
          return super.delete(ctx, action);
        } else {
          super.deleteFail(ctx, action as any);
          return of(null);
        }
      }),
    );
  }

  @OverrideDefaultAction()
  @Action(AdminFundingAgenciesAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<AdminFundingAgenciesStateModel>, action: AdminFundingAgenciesAction.GetByIdSuccess): void {
    super.getByIdSuccess(ctx, action);
    if (isNotNullNorUndefined(action.model.logo)) {
      ctx.dispatch(new AdminFundingAgenciesAction.GetFile(action.model.resId));
    }
  }
}
