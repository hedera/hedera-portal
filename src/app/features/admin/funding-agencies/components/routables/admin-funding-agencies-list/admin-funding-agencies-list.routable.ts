/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-funding-agencies-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {adminFundingAgenciesActionNameSpace} from "@admin/funding-agencies/stores/admin-funding-agencies.action";
import {
  AdminFundingAgenciesState,
  AdminFundingAgenciesStateModel,
} from "@admin/funding-agencies/stores/admin-funding-agencies.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {FundingAgency} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {SecurityService} from "@shared/services/security.service";
import {
  AbstractListRoutable,
  DataTableColumns,
  DataTableFieldTypeEnum,
  isNotNullNorUndefined,
  OrderEnum,
  RouterExtensionService,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-submission-policy-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./admin-funding-agendies-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminFundingAgenciesListRoutable extends AbstractListRoutable<FundingAgency, AdminFundingAgenciesStateModel> {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToAdmin;
  readonly KEY_PARAM_NAME: keyof FundingAgency & string = "name";

  adminFundingAgenciesState: typeof AdminFundingAgenciesState = AdminFundingAgenciesState;

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.admin_fundingAgencies, adminFundingAgenciesActionNameSpace, _injector, {
      canCreate: _securityService.isRootOrAdmin(),
    }, StateEnum.admin);
  }

  conditionDisplayEditButton(model: FundingAgency | undefined): boolean {
    return this._securityService.isRootOrAdmin();
  }

  conditionDisplayDeleteButton(model: FundingAgency | undefined): boolean {
    return this._securityService.isRootOrAdmin();
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "avatar" as string,
        header: LabelTranslateEnum.logo,
        order: OrderEnum.none,
        resourceNameSpace: adminFundingAgenciesActionNameSpace,
        resourceState: this.adminFundingAgenciesState as any,
        isFilterable: false,
        isSortable: false,
        component: DataTableComponentHelper.get(DataTableComponentEnum.logo),
        isUser: false,
        skipIfAvatarMissing: true,
        idResource: (fundingAgency: FundingAgency) => fundingAgency.resId,
        isLogoPresent: (fundingAgency: FundingAgency) => isNotNullNorUndefined(fundingAgency.logo),
        width: "45px",
      } as DataTableColumns<FundingAgency>,
      {
        field: "acronym",
        header: LabelTranslateEnum.acronym,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
    ];
  }

}
