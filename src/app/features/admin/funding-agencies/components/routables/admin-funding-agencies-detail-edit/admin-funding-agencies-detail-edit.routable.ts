/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-funding-agencies-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {adminFundingAgenciesActionNameSpace} from "@admin/funding-agencies/stores/admin-funding-agencies.action";
import {
  AdminFundingAgenciesState,
  AdminFundingAgenciesStateModel,
} from "@admin/funding-agencies/stores/admin-funding-agencies.state";
import {AdminFundingAgenciesProjectAction} from "@admin/funding-agencies/stores/project/admin-project-funding-agencies.action";
import {AdminFundingAgenciesProjectState} from "@admin/funding-agencies/stores/project/admin-project-funding-agencies.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  FundingAgency,
  Project,
} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {sharedFundingAgencyActionNameSpace} from "@shared/stores/funding-agency/shared-funding-agency.action";
import {SharedProjectState} from "@shared/stores/project/shared-project.state";
import {Observable} from "rxjs";
import {
  AbstractDetailEditCommonRoutable,
  MemoizedUtil,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-funding-agency-detail-edit-routable",
  templateUrl: "./admin-funding-agencies-detail-edit.routable.html",
  styleUrls: ["./admin-funding-agencies-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminFundingAgenciesDetailEditRoutable extends AbstractDetailEditCommonRoutable<FundingAgency, AdminFundingAgenciesStateModel> {
  @Select(AdminFundingAgenciesState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminFundingAgenciesState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;
  listProjectsObs: Observable<Project[]>;
  selectedProjectsObs: Observable<Project[]>;

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedFundingAgencyActionNameSpace;

  readonly KEY_PARAM_NAME: keyof FundingAgency & string = "name";

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_fundingAgencies, _injector, adminFundingAgenciesActionNameSpace, StateEnum.admin);
    this.listProjectsObs = MemoizedUtil.list(this._store, SharedProjectState);
    this.selectedProjectsObs = MemoizedUtil.selected(this._store, AdminFundingAgenciesProjectState);
    this.editAvailable = this._securityService.isRootOrAdmin();
    this.deleteAvailable = this._securityService.isRootOrAdmin();
  }

  _getSubResourceWithParentId(id: string): void {
    this._store.dispatch(new AdminFundingAgenciesProjectAction.GetAll(id));
  }
}
