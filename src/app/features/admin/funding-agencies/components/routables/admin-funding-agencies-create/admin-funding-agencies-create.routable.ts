/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-funding-agencies-create.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {adminFundingAgenciesActionNameSpace} from "@admin/funding-agencies/stores/admin-funding-agencies.action";
import {
  AdminFundingAgenciesState,
  AdminFundingAgenciesStateModel,
} from "@admin/funding-agencies/stores/admin-funding-agencies.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {SharedProjectState} from "@app/shared/stores/project/shared-project.state";
import {
  FundingAgency,
  Project,
} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {sharedFundingAgencyActionNameSpace} from "@shared/stores/funding-agency/shared-funding-agency.action";
import {Observable} from "rxjs";
import {
  AbstractCreateRoutable,
  MemoizedUtil,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-funding-agency-create-routable",
  templateUrl: "./admin-funding-agencies-create.routable.html",
  styleUrls: ["./admin-funding-agencies-create.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminFundingAgenciesCreateRoutable extends AbstractCreateRoutable<FundingAgency, AdminFundingAgenciesStateModel> {
  @Select(AdminFundingAgenciesState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminFundingAgenciesState.isReadyToBeDisplayedInCreateMode) isReadyToBeDisplayedInCreateModeObs: Observable<boolean>;
  listProjectsObs: Observable<Project[]>;

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedFundingAgencyActionNameSpace;

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector) {
    super(_store, _actions$, _changeDetector, StateEnum.admin_fundingAgencies, _injector, adminFundingAgenciesActionNameSpace, StateEnum.admin);
    this.listProjectsObs = MemoizedUtil.list(this._store, SharedProjectState);
  }

}
