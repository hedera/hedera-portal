/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-funding-agencies-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminFundingAgenciesAction} from "@admin/funding-agencies/stores/admin-funding-agencies.action";
import {AdminFundingAgenciesState} from "@admin/funding-agencies/stores/admin-funding-agencies.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {sharedProjectActionNameSpace} from "@app/shared/stores/project/shared-project.action";
import {
  FundingAgency,
  Project,
} from "@models";
import {SharedAbstractFormPresentational} from "@shared/components/presentationals/shared-abstract-form/shared-abstract.presentational";
import {SharedProjectOverlayPresentational} from "@shared/components/presentationals/shared-project-overlay/shared-project-overlay.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {SharedProjectState} from "@shared/stores/project/shared-project.state";
import {
  isNullOrUndefined,
  OrderEnum,
  PropertyName,
  RegexUtil,
  ResourceFileNameSpace,
  ResourceNameSpace,
  SolidifyValidator,
  Sort,
  Type,
} from "solidify-frontend";

@Component({
  selector: "hedera-admin-funding-agencies-form",
  templateUrl: "./admin-funding-agencies-form.presentational.html",
  styleUrls: ["./admin-funding-agencies-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminFundingAgenciesFormPresentational extends SharedAbstractFormPresentational<FundingAgency> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  @Input()
  selectedProjects: Project[];

  @Input()
  listProjects: Project[];

  adminFundingAgenciesActionNameSpace: ResourceFileNameSpace = AdminFundingAgenciesAction;
  adminFundingAgenciesState: typeof AdminFundingAgenciesState = AdminFundingAgenciesState;

  sharedProjectSort: Sort<Project> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedProjectActionNameSpace: ResourceNameSpace = sharedProjectActionNameSpace;
  sharedProjectState: typeof SharedProjectState = SharedProjectState;

  projectOverlayComponent: Type<SharedProjectOverlayPresentational> = SharedProjectOverlayPresentational;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.acronym]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: ["", [SolidifyValidator]],
      [this.formDefinition.url]: ["", [SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.projects]: [],
      [this.formDefinition.rorId]: ["", SolidifyValidator],
    });
  }

  protected _bindFormTo(fundingAgency: FundingAgency): void {
    this.form = this._fb.group({
      [this.formDefinition.acronym]: [fundingAgency.acronym, [Validators.required, SolidifyValidator]],
      [this.formDefinition.name]: [fundingAgency.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: [fundingAgency.description, [SolidifyValidator]],
      [this.formDefinition.url]: [fundingAgency.url, [SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.projects]: [this.selectedProjects.map(o => o.resId)],
      [this.formDefinition.rorId]: [fundingAgency.rorId, SolidifyValidator],
    });
  }

  protected _treatmentBeforeSubmit(fundingAgency: FundingAgency): FundingAgency {
    fundingAgency.projects = [];
    const listProjectId = this.form.get(this.formDefinition.projects).value;
    if (!isNullOrUndefined(listProjectId)) {
      listProjectId.forEach(resId => {
        fundingAgency.projects.push({resId: resId});
      });
    }
    return fundingAgency;
  }

  navigateToProject(project: Project): void {
    this.navigate([RoutesEnum.adminProjectDetail, project.resId]);
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() acronym: string;
  @PropertyName() name: string;
  @PropertyName() description: string;
  @PropertyName() projects: string;
  @PropertyName() url: string;
  @PropertyName() rorId: string;
}
