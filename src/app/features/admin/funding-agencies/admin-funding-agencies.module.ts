/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - admin-funding-agencies.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {AdminModule} from "@admin/admin.module";
import {AdminFundingAgenciesRoutingModule} from "@admin/funding-agencies/admin-funding-agencies-routing.module";
import {AdminFundingAgenciesFormPresentational} from "@admin/funding-agencies/components/presentationals/admin-funding-agencies-form/admin-funding-agencies-form.presentational";
import {AdminFundingAgenciesCreateRoutable} from "@admin/funding-agencies/components/routables/admin-funding-agencies-create/admin-funding-agencies-create.routable";
import {AdminFundingAgenciesDetailEditRoutable} from "@admin/funding-agencies/components/routables/admin-funding-agencies-detail-edit/admin-funding-agencies-detail-edit.routable";
import {AdminFundingAgenciesListRoutable} from "@admin/funding-agencies/components/routables/admin-funding-agencies-list/admin-funding-agencies-list.routable";
import {AdminFundingAgenciesState} from "@admin/funding-agencies/stores/admin-funding-agencies.state";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

const routables = [
  AdminFundingAgenciesListRoutable,
  AdminFundingAgenciesDetailEditRoutable,
  AdminFundingAgenciesCreateRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminFundingAgenciesFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminFundingAgenciesRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminFundingAgenciesState,
    ]),
    AdminModule,
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminFundingAgenciesModule {
}
