/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-home.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {
  BrowseRoutesEnum,
  RoutesEnum,
} from "@app/shared/enums/routes.enum";
import {LocalStateModel} from "@app/shared/models/local-state.model";
import {Enums} from "@enums";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {Observable} from "rxjs";
import {Tab} from "solidify-frontend";

@Component({
  selector: "hedera-browse-home-routable",
  templateUrl: "./browse-home.routable.html",
  styleUrls: ["./browse-home.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BrowseHomeRoutable extends SharedAbstractPresentational {
  isLoadingObs: Observable<boolean>;

  @Input()
  isLoading: boolean;

  userRolesObs: Enums.UserApplicationRole.UserApplicationRoleEnum[];

  private _currentTab: Tab;

  private get _rootUrl(): string[] {
    return [RoutesEnum.browse];
  }

  browseTabs: Tab[] = [
    {
      id: "PROJECT",
      icon: IconNameEnum.project,
      titleToTranslate: LabelTranslateEnum.projects,
      suffixUrl: BrowseRoutesEnum.project,
      route: () => [...this._rootUrl, BrowseRoutesEnum.project],
      dataTest: DataTestEnum.browseTabProject,
    },
    {
      id: "ONTOLOGY",
      icon: IconNameEnum.ontologies,
      titleToTranslate: LabelTranslateEnum.ontologies,
      suffixUrl: BrowseRoutesEnum.ontology,
      route: () => [...this._rootUrl, BrowseRoutesEnum.ontology],
    },
  ];

  constructor(private readonly _store: Store) {
    super();
    this.userRolesObs = this._store.selectSnapshot((state: LocalStateModel) => state.application.userRoles);
  }

  navigate(path: RoutesEnum): void {
    this._store.dispatch(new Navigate([path]));
  }

  setCurrentTab($event: Tab): void {
    this._currentTab = $event;
  }

}
