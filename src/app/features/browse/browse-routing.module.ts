/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {BrowseHomeRoutable} from "@app/features/browse/components/routables/browse-home/browse-home.routable";
import {HederaRoutes} from "@app/shared/models/hedera-route.model";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  BrowseRoutesEnum,
} from "@shared/enums/routes.enum";
import {CombinedGuardService} from "solidify-frontend";

const routes: HederaRoutes = [
  {
    path: AppRoutesEnum.root,
    redirectTo: BrowseRoutesEnum.project,
    pathMatch: "full",
  },
  {
    path: AppRoutesEnum.root,
    component: BrowseHomeRoutable,
    data: {},
    children: [
      {
        path: BrowseRoutesEnum.project,
        // @ts-ignore Dynamic import
        loadChildren: () => import("./project/browse-project.module").then(m => m.BrowseProjectModule),
        data: {
          breadcrumb: LabelTranslateEnum.project,
        },
        canActivate: [CombinedGuardService],
      },
      {
        path: BrowseRoutesEnum.ontology,
        // @ts-ignore Dynamic import
        loadChildren: () => import("./ontology/browse-ontology.module").then(m => m.BrowseOntologyModule),
        data: {
          breadcrumb: LabelTranslateEnum.ontologie,
        },
        canActivate: [CombinedGuardService],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BrowseRoutingModule {
}
