/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  BrowseOntologyState,
  BrowseOntologyStateModel,
} from "@app/features/browse/ontology/stores/browse-ontology.state";
import {
  BrowseProjectState,
  BrowseProjectStateModel,
} from "@app/features/browse/project/stores/browse-project.state";
import {
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseStateModel,
  defaultResourceStateInitValue,
} from "solidify-frontend";

export interface BrowseStateModel extends BaseStateModel {
  [StateEnum.browse_project]: BrowseProjectStateModel;
  [StateEnum.browse_ontology]: BrowseOntologyStateModel;
}

@Injectable()
@State<BrowseStateModel>({
  name: StateEnum.browse,
  defaults: {
    ...defaultResourceStateInitValue(),
    [StateEnum.browse_project]: null,
    [StateEnum.browse_ontology]: null,
  },
  children: [
    BrowseProjectState,
    BrowseOntologyState,
  ],
})
export class BrowseState {
  constructor(protected readonly _store: Store) {
  }
}
