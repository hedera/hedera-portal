/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-project.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {BrowseProjectFormPresentational} from "@app/features/browse/project/components/presentationals/browse-project-from/browse-project-form.presentational";
import {BrowseProjectDetailRoutable} from "@app/features/browse/project/components/routables/browse-project-detail/browse-project-detail.routable";
import {BrowseProjectHomeRoutable} from "@app/features/browse/project/components/routables/browse-project-home/browse-project-home.routable";
import {BrowseProjectIiifCollectionDetailRoutable} from "@app/features/browse/project/components/routables/browse-project-iiif-collection-detail/browse-project-iiif-collection-detail.routable";
import {BrowseProjectIiifCollectionListRoutable} from "@app/features/browse/project/components/routables/browse-project-iiif-collection-list/browse-project-iiif-collection-list.routable";
import {BrowseProjectListRoutable} from "@app/features/browse/project/components/routables/browse-project-list/browse-project-list.routable";
import {BrowseProjectManifestListRoutable} from "@app/features/browse/project/components/routables/browse-project-manifest-list/browse-project-manifest-list.routable";
import {BrowseProjectResearchDataFileListRoutable} from "@app/features/browse/project/components/routables/browse-project-research-data-file-list/browse-project-research-data-file-list.routable";
import {BrowseProjectRdfObjectListRoutable} from "@app/features/browse/project/components/routables/browse-project-rdf-object-list/browse-project-rdf-object-list.routable";
import {BrowseProjectResearchObjectListRoutable} from "@app/features/browse/project/components/routables/browse-project-research-object-list/browse-project-research-object-list.routable";
import {BrowseProjectRoutingModule} from "@app/features/browse/project/browse-project-routing.module";
import {BrowseProjectFundingAgencyState} from "@app/features/browse/project/stores/funding-agency/browse-project-funding-agency.state";
import {BrowseProjectInstitutionState} from "@app/features/browse/project/stores/institution/browse-project-institution.state";
import {BrowseProjectPersonRoleState} from "@app/features/browse/project/stores/person-role/browse-project-person-role.state";
import {BrowseProjectState} from "@app/features/browse/project/stores/browse-project.state";
import {BrowseProjectResearchDataFileState} from "@app/features/browse/project/stores/research-data-file/browse-project-research-data-file.state";
import {BrowseProjectResearchObjectTypeState} from "@app/features/browse/project/stores/research-object-type/browse-project-research-object-type.state";
import {BrowseProjectRdfObjectState} from "@app/features/browse/project/stores/rdf-object/browse-project-rdf-object.state";
import {BrowseProjectRmlState} from "@app/features/browse/project/stores/rml/browse-project-rml.state";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {ModuleLoadedEnum} from "@shared/enums/module-loaded.enum";
import {SharedModule} from "@shared/shared.module";
import {SsrUtil} from "solidify-frontend";

const routables = [
  BrowseProjectHomeRoutable,
  BrowseProjectListRoutable,
  BrowseProjectDetailRoutable,
  BrowseProjectRdfObjectListRoutable,
  BrowseProjectResearchDataFileListRoutable,
  BrowseProjectResearchObjectListRoutable,
  BrowseProjectManifestListRoutable,
  BrowseProjectIiifCollectionListRoutable,
  BrowseProjectIiifCollectionDetailRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  BrowseProjectFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    BrowseProjectRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      BrowseProjectState,
      BrowseProjectRmlState,
      BrowseProjectResearchObjectTypeState,
      BrowseProjectRdfObjectState,
      BrowseProjectResearchDataFileState,
      BrowseProjectPersonRoleState,
      BrowseProjectInstitutionState,
      BrowseProjectFundingAgencyState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class BrowseProjectModule {
  constructor() {
    if (SsrUtil.window) {
      SsrUtil.window[ModuleLoadedEnum.browseProjectModuleLoaded] = true;
    }
  }
}
