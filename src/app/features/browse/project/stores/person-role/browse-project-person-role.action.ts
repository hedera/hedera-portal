/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-project-person-role.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {PersonRole} from "@admin/models/person-role.model";
import {StateEnum} from "@shared/enums/state.enum";
import {
  Relation3TiersAction,
  Relation3TiersNameSpace,
  TypeDefaultAction,
} from "solidify-frontend";

const state = StateEnum.browse_project_personRole;

export namespace BrowseProjectPersonRoleAction {

  @TypeDefaultAction(state)
  export class GetAll extends Relation3TiersAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends Relation3TiersAction.GetAllSuccess<PersonRole> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends Relation3TiersAction.GetAllFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends Relation3TiersAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends Relation3TiersAction.GetByIdSuccess<PersonRole> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends Relation3TiersAction.GetByIdFail {
  }

  @TypeDefaultAction(state)
  export class Update extends Relation3TiersAction.Update {
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends Relation3TiersAction.UpdateSuccess {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends Relation3TiersAction.UpdateFail {
  }

  @TypeDefaultAction(state)
  export class UpdateItem extends Relation3TiersAction.UpdateItem {
  }

  @TypeDefaultAction(state)
  export class UpdateItemSuccess extends Relation3TiersAction.UpdateItemSuccess {
  }

  @TypeDefaultAction(state)
  export class UpdateItemFail extends Relation3TiersAction.UpdateItemFail {
  }

  @TypeDefaultAction(state)
  export class SetGrandChildList extends Relation3TiersAction.SetGrandChildList {
  }

  @TypeDefaultAction(state)
  export class SetGrandChildListSuccess extends Relation3TiersAction.SetGrandChildListSuccess {
  }

  @TypeDefaultAction(state)
  export class SetGrandChildListFail extends Relation3TiersAction.SetGrandChildListFail {
  }

  @TypeDefaultAction(state)
  export class Create extends Relation3TiersAction.Create {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends Relation3TiersAction.CreateSuccess {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends Relation3TiersAction.CreateFail {
  }

  @TypeDefaultAction(state)
  export class CreateResource extends Relation3TiersAction.CreateResource {
  }

  @TypeDefaultAction(state)
  export class CreateResourceSuccess extends Relation3TiersAction.CreateResourceSuccess {
  }

  @TypeDefaultAction(state)
  export class CreateResourceFail extends Relation3TiersAction.CreateResourceFail {
  }

  // @TypeDefaultAction(state)
  // export class DeleteList extends Relation3TiersAction.DeleteList {
  // }
  //
  // @TypeDefaultAction(state)
  // export class DeleteListSuccess extends Relation3TiersAction.DeleteListSuccess {
  // }
  //
  // @TypeDefaultAction(state)
  // export class DeleteListFail extends Relation3TiersAction.DeleteListFail {
  // }

  @TypeDefaultAction(state)
  export class Delete extends Relation3TiersAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends Relation3TiersAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends Relation3TiersAction.DeleteFail {
  }
}

export const browseProjectPersonRoleActionNamespace: Relation3TiersNameSpace = BrowseProjectPersonRoleAction;
