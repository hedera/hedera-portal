/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-project-research-object-type.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Rml} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  AssociationAction,
  AssociationNameSpace,
  TypeDefaultAction,
} from "solidify-frontend";

const state = StateEnum.browse_project_researchObjectType;

export namespace BrowseProjectResearchObjectTypeAction {

  @TypeDefaultAction(state)
  export class GetAll extends AssociationAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends AssociationAction.GetAllSuccess<Rml> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends AssociationAction.GetAllFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends AssociationAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends AssociationAction.GetByIdSuccess<Rml> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends AssociationAction.GetByIdFail {
  }

  @TypeDefaultAction(state)
  export class Update extends AssociationAction.Update {
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends AssociationAction.UpdateSuccess {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends AssociationAction.UpdateFail {
  }

  @TypeDefaultAction(state)
  export class Create extends AssociationAction.Create {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends AssociationAction.CreateSuccess {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends AssociationAction.CreateFail {
  }

  @TypeDefaultAction(state)
  export class DeleteList extends AssociationAction.DeleteList {
  }

  @TypeDefaultAction(state)
  export class DeleteListSuccess extends AssociationAction.DeleteListSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteListFail extends AssociationAction.DeleteListFail {
  }

  @TypeDefaultAction(state)
  export class Delete extends AssociationAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends AssociationAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends AssociationAction.DeleteFail {
  }

  @TypeDefaultAction(state)
  export class Clean extends AssociationAction.Clean {
  }
}

export const browseProjectResearchObjectTypeNamespace: AssociationNameSpace = BrowseProjectResearchObjectTypeAction;
