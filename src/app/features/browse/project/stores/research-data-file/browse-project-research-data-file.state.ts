/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-project-research-data-file.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  BrowseProjectResearchDataFileAction,
  browseProjectResearchDataFileNamespace,
} from "@app/features/browse/project/stores/research-data-file/browse-project-research-data-file.action";
import {environment} from "@environments/environment";
import {ResearchDataFileMetadataResult} from "@models";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  ApiService,
  CollectionTyped,
  defaultResourceStateInitValue,
  NotificationService,
  QueryParameters,
  ResourceState,
  ResourceStateModel,
  SolidifyStateContext,
} from "solidify-frontend";

export interface BrowseProjectResearchDataFileStateModel extends ResourceStateModel<ResearchDataFileMetadataResult> {
}

@Injectable()
@State<BrowseProjectResearchDataFileStateModel>({
  name: StateEnum.browse_project_researchDataFile,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
})
export class BrowseProjectResearchDataFileState extends ResourceState<BrowseProjectResearchDataFileStateModel, ResearchDataFileMetadataResult> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: browseProjectResearchDataFileNamespace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.accessProjects;
  }

  protected override _callGetAllEndpoint<U>(ctx: SolidifyStateContext<BrowseProjectResearchDataFileStateModel>, action: BrowseProjectResearchDataFileAction.GetAll, queryParameters: QueryParameters): Observable<CollectionTyped<U>> {
    const url = `${this._urlResource}/${action.shortName}/${ApiResourceNameEnum.RESEARCH_DATA_FILES}`;
    return this._apiService.getCollection<U>(url, queryParameters);
  }
}
