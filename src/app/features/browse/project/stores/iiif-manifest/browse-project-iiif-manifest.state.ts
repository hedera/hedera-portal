/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-project-iiif-manifest.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  BrowseProjectIiifManifestAction,
  browseProjectIiifManifestNamespace,
} from "@app/features/browse/project/stores/iiif-manifest/browse-project-iiif-manifest.action";
import {environment} from "@environments/environment";
import {IIIFManifestEntry} from "@models";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  ApiService,
  CollectionTyped,
  defaultResourceStateInitValue,
  NotificationService,
  QueryParameters,
  ResourceState,
  ResourceStateModel,
  SolidifyStateContext,
} from "solidify-frontend";

export interface BrowseProjectIiifManifestStateModel extends ResourceStateModel<IIIFManifestEntry> {
}

@Injectable()
@State<BrowseProjectIiifManifestStateModel>({
  name: StateEnum.browse_project_iiifManifest,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
})
export class BrowseProjectIiifManifestState extends ResourceState<BrowseProjectIiifManifestStateModel, IIIFManifestEntry> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: browseProjectIiifManifestNamespace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.iiifManifest;
  }

  protected override _callGetAllEndpoint<U>(ctx: SolidifyStateContext<BrowseProjectIiifManifestStateModel>, action: BrowseProjectIiifManifestAction.GetAll, queryParameters: QueryParameters): Observable<CollectionTyped<U>> {
    const url = `${this._urlResource}/${action.shortName}`;
    return this._apiService.getCollection<U>(url, queryParameters);
  }
}
