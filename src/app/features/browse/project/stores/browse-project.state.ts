/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-project.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  BrowseProjectAction,
  browseProjectActionNamespace,
} from "@app/features/browse/project/stores/browse-project.action";
import {
  BrowseProjectFundingAgencyState,
  BrowseProjectFundingAgencyStateModel,
} from "@app/features/browse/project/stores/funding-agency/browse-project-funding-agency.state";
import {
  BrowseProjectIiifCollectionState,
  BrowseProjectIiifCollectionStateModel,
} from "@app/features/browse/project/stores/iiif-collection/browse-project-iiif-collection.state";
import {
  BrowseProjectIiifManifestState,
  BrowseProjectIiifManifestStateModel,
} from "@app/features/browse/project/stores/iiif-manifest/browse-project-iiif-manifest.state";
import {
  BrowseProjectInstitutionState,
  BrowseProjectInstitutionStateModel,
} from "@app/features/browse/project/stores/institution/browse-project-institution.state";
import {
  BrowseProjectPersonRoleState,
  BrowseProjectPersonRoleStateModel,
  defaultBrowseProjectPersonRoleStateModel,
} from "@app/features/browse/project/stores/person-role/browse-project-person-role.state";
import {
  BrowseProjectRdfObjectState,
  BrowseProjectRdfObjectStateModel,
} from "@app/features/browse/project/stores/rdf-object/browse-project-rdf-object.state";
import {
  BrowseProjectResearchDataFileState,
  BrowseProjectResearchDataFileStateModel,
} from "@app/features/browse/project/stores/research-data-file/browse-project-research-data-file.state";
import {
  BrowseProjectResearchObjectTypeState,
  BrowseProjectResearchObjectTypeStateModel,
} from "@app/features/browse/project/stores/research-object-type/browse-project-research-object-type.state";
import {
  BrowseProjectResearchObjectState,
  BrowseProjectResearchObjectStateModel,
} from "@app/features/browse/project/stores/research-object/browse-project-research-object.state";
import {
  BrowseProjectRmlState,
  BrowseProjectRmlStateModel,
} from "@app/features/browse/project/stores/rml/browse-project-rml.state";
import {environment} from "@environments/environment";
import {
  IIIFCollection,
  Project,
} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  defaultAssociationStateInitValue,
  defaultResourceFileStateInitValue,
  defaultResourceStateInitValue,
  DispatchMethodEnum,
  DownloadService,
  isFalse,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isUndefined,
  NotificationService,
  OverrideDefaultAction,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export interface BrowseProjectStateModel extends ResourceFileStateModel<Project> {
  [StateEnum.browse_project_rml]: BrowseProjectRmlStateModel;
  [StateEnum.browse_project_researchObjectType]: BrowseProjectResearchObjectTypeStateModel;
  [StateEnum.browse_project_personRole]: BrowseProjectPersonRoleStateModel;
  [StateEnum.browse_project_institutions]: BrowseProjectInstitutionStateModel;
  [StateEnum.browse_project_fundingAgency]: BrowseProjectFundingAgencyStateModel;
  [StateEnum.browse_project_researchObject]: BrowseProjectResearchObjectStateModel;
  [StateEnum.browse_project_researchDataFile]: BrowseProjectResearchDataFileStateModel;
  [StateEnum.browse_project_rdfObject]: BrowseProjectRdfObjectStateModel;
  [StateEnum.browse_project_iiifManifest]: BrowseProjectIiifManifestStateModel;
  [StateEnum.browse_project_iiifCollection]: BrowseProjectIiifCollectionStateModel;
  currentUserIsLogged: boolean;
  currentUserIsManager: boolean;
  currentUserIsMemberProject: boolean;
  iiifCollections: IIIFCollection;
}

@Injectable()
@State<BrowseProjectStateModel>({
  name: StateEnum.browse_project,
  defaults: {
    ...defaultResourceFileStateInitValue(),
    currentUserIsLogged: undefined,
    currentUserIsManager: undefined,
    currentUserIsMemberProject: undefined,
    [StateEnum.browse_project_rml]: {...defaultAssociationStateInitValue()},
    [StateEnum.browse_project_researchObjectType]: {...defaultAssociationStateInitValue()},
    [StateEnum.browse_project_personRole]: {...defaultBrowseProjectPersonRoleStateModel()},
    [StateEnum.browse_project_institutions]: {...defaultAssociationStateInitValue()},
    [StateEnum.browse_project_fundingAgency]: {...defaultAssociationStateInitValue()},
    [StateEnum.browse_project_researchObject]: {...defaultResourceStateInitValue()},
    [StateEnum.browse_project_researchDataFile]: {...defaultResourceStateInitValue()},
    [StateEnum.browse_project_rdfObject]: {...defaultResourceStateInitValue()},
    [StateEnum.browse_project_iiifManifest]: {...defaultResourceStateInitValue()},
    [StateEnum.browse_project_iiifCollection]: {...defaultResourceStateInitValue()},
    iiifCollections: undefined,
  },
  children: [
    BrowseProjectRmlState,
    BrowseProjectResearchObjectTypeState,
    BrowseProjectPersonRoleState,
    BrowseProjectInstitutionState,
    BrowseProjectFundingAgencyState,
    BrowseProjectRdfObjectState,
    BrowseProjectResearchDataFileState,
    BrowseProjectResearchObjectState,
    BrowseProjectIiifManifestState,
    BrowseProjectIiifCollectionState,
  ],
})
export class BrowseProjectState extends ResourceFileState<BrowseProjectStateModel, Project> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService,
              private readonly _securityService: SecurityService,
  ) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: browseProjectActionNamespace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.browseProjectDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.browseProjectDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.browse,
      keepCurrentStateAfterUpdate: true,
      updateSubResourceDispatchMethod: DispatchMethodEnum.SEQUENCIAL,
    }, _downloadService, ResourceFileStateModeEnum.logo, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.accessProjects;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static currentUserIsManager(state: BrowseProjectStateModel): boolean | undefined {
    return state.currentUserIsManager;
  }

  @Selector()
  static currentUserIsMemberProject(state: BrowseProjectStateModel): boolean | undefined {
    return state.currentUserIsMemberProject;
  }

  @Selector()
  static currentTitle(state: BrowseProjectStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @Selector()
  static isLoading(state: BrowseProjectStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: BrowseProjectStateModel): boolean {
    if (isFalse(state.currentUserIsLogged)) {
      return this.isLoading(state);
    }
    return this.isLoading(state)
      || isUndefined(this.currentUserIsManager(state))
      || isUndefined(this.currentUserIsMemberProject(state))
      || StoreUtil.isLoadingState(state[StateEnum.browse_project_personRole])
      || StoreUtil.isLoadingState(state[StateEnum.browse_project_researchObjectType])
      || StoreUtil.isLoadingState(state[StateEnum.browse_project_institutions])
      || StoreUtil.isLoadingState(state[StateEnum.browse_project_fundingAgency])
      || StoreUtil.isLoadingState(state[StateEnum.browse_project_rml]);

  }

  @Selector()
  static isReadyToBeDisplayed(state: BrowseProjectStateModel): boolean {
    return isNotNullNorUndefined(state.current)
      && (isFalse(state.currentUserIsLogged) ||
        (!isUndefined(this.currentUserIsManager(state))
          && !isUndefined(this.currentUserIsMemberProject(state))
          && (isFalse(this.currentUserIsManager(state)) ||
            isNotNullNorUndefined(state[StateEnum.browse_project_personRole].selected)
          )
          && (isFalse(this.currentUserIsManager(state)) || (
            isNotNullNorUndefined(state[StateEnum.browse_project_researchObjectType].selected)
            && isNotNullNorUndefined(state[StateEnum.browse_project_rml].selected)
            && isNotNullNorUndefined(state[StateEnum.browse_project_fundingAgency].selected)
          ))
          && isNotNullNorUndefined(state[StateEnum.browse_project_institutions].selected)
        ));
  }

  @Action(BrowseProjectAction.SaveCurrentUserAffiliation)
  saveCurrentUserAffiliation(ctx: SolidifyStateContext<BrowseProjectStateModel>, action: BrowseProjectAction.SaveCurrentUserAffiliation): void {
    ctx.patchState({
      currentUserIsLogged: this._securityService.isLoggedIn(),
      currentUserIsManager: action.isManager,
      currentUserIsMemberProject: action.isMemberProject,
    });
  }

  @Action(BrowseProjectAction.LoadOnlyMyProjects)
  loadOnlyMyProjects(ctx: SolidifyStateContext<BrowseProjectStateModel>, action: BrowseProjectAction.LoadOnlyMyProjects): Observable<CollectionTyped<Project>> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
    });

    return this._apiService.getCollection<Project>(ApiEnum.adminAuthorizedProjects, ctx.getState().queryParameters)
      .pipe(
        tap((collection: CollectionTyped<Project>) => {
          ctx.dispatch(new BrowseProjectAction.LoadOnlyMyProjectsSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new BrowseProjectAction.LoadOnlyMyProjectsFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(BrowseProjectAction.LoadOnlyMyProjectsSuccess)
  loadOnlyMyProjectsSuccess(ctx: SolidifyStateContext<BrowseProjectStateModel>, action: BrowseProjectAction.LoadOnlyMyProjectsSuccess): void {
    const queryParameters = StoreUtil.updateQueryParameters(ctx, action.list);

    ctx.patchState({
      list: action.list._data,
      total: action.list._page.totalItems,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters,
    });
  }

  @Action(BrowseProjectAction.LoadOnlyMyProjectsFail)
  loadOnlyMyProjectsFail(ctx: SolidifyStateContext<BrowseProjectStateModel>, action: BrowseProjectAction.LoadOnlyMyProjectsFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @OverrideDefaultAction()
  @Action(BrowseProjectAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<BrowseProjectStateModel>, action: BrowseProjectAction.GetByIdSuccess): void {
    super.getByIdSuccess(ctx, action);
    if (isNotNullNorUndefined(action.model.logo)) {
      ctx.dispatch(new BrowseProjectAction.GetFile(action.model.resId));
    }
  }

  @Action(BrowseProjectAction.GetIiifCollection)
  getIiifCollection(ctx: SolidifyStateContext<BrowseProjectStateModel>, action: BrowseProjectAction.GetIiifCollection): Observable<IIIFCollection> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.get<IIIFCollection>(`${ApiEnum.iiifCollection}/${action.projectShortName}/${action.collectionName}`)
      .pipe(
        tap((iiifCollections: IIIFCollection) => {
          ctx.dispatch(new BrowseProjectAction.GetIiifCollectionSuccess(action, iiifCollections));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new BrowseProjectAction.GetIiifCollectionFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(BrowseProjectAction.GetIiifCollectionSuccess)
  getIiifCollectionSuccess(ctx: SolidifyStateContext<BrowseProjectStateModel>, action: BrowseProjectAction.GetIiifCollectionSuccess): void {
    ctx.patchState({
      iiifCollections: action.iiifCollections,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(BrowseProjectAction.GetIiifCollectionFail)
  getIiifCollectionFail(ctx: SolidifyStateContext<BrowseProjectStateModel>, action: BrowseProjectAction.GetIiifCollectionFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
