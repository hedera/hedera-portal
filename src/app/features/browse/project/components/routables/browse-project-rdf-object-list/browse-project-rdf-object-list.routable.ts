/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-project-rdf-object-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnDestroy,
} from "@angular/core";
import {FormControl} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {BrowseProjectState} from "@app/features/browse/project/stores/browse-project.state";
import {
  BrowseProjectRdfObjectAction,
  browseProjectRdfObjectNamespace,
} from "@app/features/browse/project/stores/rdf-object/browse-project-rdf-object.action";
import {
  BrowseProjectRdfObjectState,
  BrowseProjectRdfObjectStateModel,
} from "@app/features/browse/project/stores/rdf-object/browse-project-rdf-object.state";
import {BrowseProjectResearchObjectTypeAction} from "@app/features/browse/project/stores/research-object-type/browse-project-research-object-type.action";
import {BrowseProjectResearchObjectTypeState} from "@app/features/browse/project/stores/research-object-type/browse-project-research-object-type.state";
import {
  RdfObject,
  ResearchObjectType,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  BrowseProjectRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  take,
  tap,
} from "rxjs/operators";
import {
  AbstractListRoutable,
  ClipboardUtil,
  DataTableActions,
  DataTableFieldTypeEnum,
  FormValidationHelper,
  isNonEmptyArray,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  MemoizedUtil,
  NotificationService,
  ObjectUtil,
  OrderEnum,
  QueryParameters,
  RouterExtensionService,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

@Component({
  selector: "hedera-browse-project-rdf-object-list-routable",
  templateUrl: "./browse-project-rdf-object-list.routable.html",
  styleUrls: ["./browse-project-rdf-object-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BrowseProjectRdfObjectListRoutable extends AbstractListRoutable<RdfObject, BrowseProjectRdfObjectStateModel> implements OnDestroy {
  readonly _LINK_KEY: string = "properties.s.value";
  readonly KEY_CREATE_BUTTON: string = undefined;
  readonly KEY_BACK_BUTTON: string | undefined = undefined;
  readonly KEY_PARAM_NAME: keyof RdfObject & any = "name";

  override cleanIsNeeded: boolean = false;
  override displayActionCopyIdToClipboard: boolean = false;

  researchObjectTypeFormControl: FormControl = new FormControl();

  listProjectResearchObjectTypeObs: Observable<ResearchObjectType[]> = MemoizedUtil.selected(this._store, BrowseProjectResearchObjectTypeState);

  resId: string;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _notificationService: NotificationService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.browse_project_rdfObject, browseProjectRdfObjectNamespace, _injector, {
      canCreate: false,
      canGoBack: false,
    }, StateEnum.browse_project);
    this.listObs = MemoizedUtil.list(this._store, BrowseProjectRdfObjectState);
    this.queryParametersObs = MemoizedUtil.queryParameters(this._store, BrowseProjectRdfObjectState);
    this.isLoadingObs = MemoizedUtil.isLoading(this._store, BrowseProjectRdfObjectState);

    this._retrieveResIdFromUrl();
    this.subscribe(MemoizedUtil.current(this._store, BrowseProjectState).pipe(
      filter(project => project?.resId === this.resId),
      take(1),
      tap(project => this._init()),
    ));
  }

  private _init(): void {
    this.subscribe(this.researchObjectTypeFormControl.valueChanges.pipe(
      distinctUntilChanged(),
      tap((researchObjectTypeName: string) => {
        if (isNotNullNorUndefinedNorWhiteString(researchObjectTypeName)) {
          const queryParameters = MemoizedUtil.queryParametersSnapshot(this._store, BrowseProjectRdfObjectState);
          this.onQueryParametersEvent(queryParameters);
        } else {
          this._store.dispatch(new BrowseProjectRdfObjectAction.Clean(false));
        }
      }),
    ));

    this.subscribe(this.listProjectResearchObjectTypeObs.pipe(
      filter(listResearchObjectType => isNonEmptyArray(listResearchObjectType)),
      take(1),
      tap(listResearchObjectType => {
        if (isNullOrUndefinedOrWhiteString(this.researchObjectTypeFormControl.value)) {
          this.researchObjectTypeFormControl.setValue(listResearchObjectType[0].name);
        }
      }),
    ));
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this._store.dispatch(new BrowseProjectResearchObjectTypeAction.Clean(false));
  }

  private _retrieveResIdFromUrl(): void {
    this.resId = this._route.snapshot.paramMap.get(SOLIDIFY_CONSTANTS.ID);
    if (isNullOrUndefined(this.resId)) {
      this.resId = this._route.parent.snapshot.paramMap.get(SOLIDIFY_CONSTANTS.ID);
    }
  }

  conditionDisplayEditButton(model: RdfObject | undefined): boolean {
    return false;
  }

  conditionDisplayDeleteButton(model: RdfObject | undefined): boolean {
    return false;
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "properties.s.nodeType" as any,
        header: LabelTranslateEnum.type,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: false,
      },
      {
        field: this._LINK_KEY as any,
        header: LabelTranslateEnum.link,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: false,
      },
    ];
  }

  override onQueryParametersEvent(queryParameters: QueryParameters): void {
    const researchObjectTypeName = this.researchObjectTypeFormControl.value;
    const projectShortName = MemoizedUtil.currentSnapshot(this._store, BrowseProjectState)?.shortName;
    if (isNullOrUndefinedOrWhiteString(researchObjectTypeName) || isNullOrUndefinedOrWhiteString(projectShortName)) {
      return;
    }
    this._store.dispatch(new BrowseProjectRdfObjectAction.GetAll(projectShortName, researchObjectTypeName, queryParameters));
  }

  override getState(rootStateModel: any): BrowseProjectRdfObjectStateModel {
    const state = rootStateModel[StateEnum.browse][this._parentState][this._state];
    if (isNullOrUndefined(state)) {
      // eslint-disable-next-line no-console
      console.error("There is no state corresponding to path " + this._parentState + " > " + this._state);
    }
    return state;
  }

  private _getUri(rdfObject: RdfObject): string {
    return ObjectUtil.getNestedValue(rdfObject, this._LINK_KEY);
  }

  override showDetail(rdfObject: RdfObject): void {
    const link = this._getUri(rdfObject);
    if (isNullOrUndefinedOrWhiteString(link)) {
      return;
    }
    const lastIndex = link.lastIndexOf(SOLIDIFY_CONSTANTS.URL_SEPARATOR);
    const id = link.substring(lastIndex + 1);
    const projectId = MemoizedUtil.currentSnapshot(this._store, BrowseProjectState).resId;
    this._store.dispatch(new Navigate([RoutesEnum.browseProjectDetail, projectId, BrowseProjectRoutesEnum.researchObject, id]));
  }

  private _copyLink(rdfObject: RdfObject): void {
    const link = this._getUri(rdfObject);
    if (isNotNullNorUndefinedNorWhiteString(link) && ClipboardUtil.copyStringToClipboard(link)) {
      this._notificationService.showInformation(LabelTranslateEnum.copiedLinkToClipboard);
    }
  }

  protected _defineActions(): DataTableActions<RdfObject>[] {
    return [
      {
        logo: IconNameEnum.copyToClipboard,
        callback: rdfObject => this._copyLink(rdfObject),
        placeholder: current => LabelTranslateEnum.copyLinkToClipboard,
        isWrapped: false,
      },
    ];
  }
}
