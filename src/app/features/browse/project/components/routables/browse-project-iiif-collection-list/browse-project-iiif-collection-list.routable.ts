/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-project-iiif-collection-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {BrowseProjectState} from "@app/features/browse/project/stores/browse-project.state";
import {
  BrowseProjectIiifCollectionAction,
  browseProjectIiifCollectionNamespace,
} from "@app/features/browse/project/stores/iiif-collection/browse-project-iiif-collection.action";
import {
  BrowseProjectIiifCollectionState,
  BrowseProjectIiifCollectionStateModel,
} from "@app/features/browse/project/stores/iiif-collection/browse-project-iiif-collection.state";
import {BrowseProjectResearchObjectStateModel} from "@app/features/browse/project/stores/research-object/browse-project-research-object.state";
import {LocalStateModel} from "@app/shared/models/local-state.model";
import {environment} from "@environments/environment";
import {IIIFCollectionEntry} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  BrowseProjectRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  distinctUntilChanged,
  filter,
  tap,
} from "rxjs/operators";
import {
  AbstractListRoutable,
  ClipboardUtil,
  DataTableActions,
  DataTableFieldTypeEnum,
  FormValidationHelper,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  MemoizedUtil,
  NotificationService,
  OrderEnum,
  QueryParameters,
  RouterExtensionService,
  SOLIDIFY_CONSTANTS,
  SsrUtil,
  UrlUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-browse-project-iiif-collection-list-routable",
  templateUrl: "./browse-project-iiif-collection-list.routable.html",
  styleUrls: ["./browse-project-iiif-collection-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BrowseProjectIiifCollectionListRoutable extends AbstractListRoutable<IIIFCollectionEntry, BrowseProjectIiifCollectionStateModel> implements OnInit {
  readonly KEY_CREATE_BUTTON: string = undefined;
  readonly KEY_BACK_BUTTON: string | undefined = undefined;
  readonly KEY_PARAM_NAME: keyof IIIFCollectionEntry & any = undefined;

  override cleanIsNeeded: boolean = false;
  override skipInitialQuery: boolean = true;

  stickyTopPosition: number = environment.defaultStickyDatatableHeight;

  isDetail: boolean = false;

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  resId: string;

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _notificationService: NotificationService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.browse_project_iiifCollection, browseProjectIiifCollectionNamespace, _injector, {
      canCreate: false,
      canGoBack: false,
    }, StateEnum.browse_project);
    this._retrieveResIdFromUrl();
    this.listObs = MemoizedUtil.list(this._store, BrowseProjectIiifCollectionState);
    this.queryParametersObs = MemoizedUtil.queryParameters(this._store, BrowseProjectIiifCollectionState);
    this.isLoadingObs = MemoizedUtil.isLoading(this._store, BrowseProjectIiifCollectionState);

    this.subscribe(MemoizedUtil.current(this._store, BrowseProjectState).pipe(
      filter(p => p?.resId === this.resId),
      tap(p => {
        this.onQueryParametersEvent(MemoizedUtil.queryParametersSnapshot(this._store, BrowseProjectIiifCollectionState));
      }),
    ));
  }

  private _retrieveResIdFromUrl(): void {
    this.resId = this._route.snapshot.paramMap.get(SOLIDIFY_CONSTANTS.ID);
    if (isNullOrUndefined(this.resId)) {
      this.resId = this._route.parent.snapshot.paramMap.get(SOLIDIFY_CONSTANTS.ID);
    }
  }

  conditionDisplayEditButton(model: IIIFCollectionEntry | undefined): boolean {
    return false;
  }

  conditionDisplayDeleteButton(model: IIIFCollectionEntry | undefined): boolean {
    return false;
  }

  ngOnInit(): void {
    super.ngOnInit();

    if (SsrUtil.isBrowser) {
      this.subscribe(this._store.select((s: LocalStateModel) => s.router.state.url).pipe(
          distinctUntilChanged(),
          tap(url => {
            const path = UrlUtil.getPath(url);
            this.isDetail = !path.endsWith(BrowseProjectRoutesEnum.iiifCollection);
            this._changeDetector.detectChanges();
          }),
        ),
      );
    }
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "id",
        header: LabelTranslateEnum.identifier,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isSortable: true,
        isFilterable: true,
      },
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isSortable: true,
        isFilterable: true,
      },
      {
        field: "url",
        header: LabelTranslateEnum.url,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isSortable: true,
        isFilterable: true,
      },
      {
        field: "itemNumber",
        header: LabelTranslateEnum.size,
        type: DataTableFieldTypeEnum.number,
        order: OrderEnum.none,
        isSortable: true,
        isFilterable: true,
      },
    ];
  }

  protected _defineActions(): DataTableActions<IIIFCollectionEntry>[] {
    return [
      {
        logo: IconNameEnum.externalLink,
        callback: iiifCollection => this._openLink(iiifCollection),
        placeholder: current => LabelTranslateEnum.show,
        isWrapped: false,
      },
      {
        logo: IconNameEnum.copyToClipboard,
        callback: iiifCollection => this._copyLink(iiifCollection),
        placeholder: current => LabelTranslateEnum.copyLinkToClipboard,
        isWrapped: false,
      },
    ];
  }

  onQueryParametersEvent(queryParameters: QueryParameters): void {
    const shortName = MemoizedUtil.currentSnapshot(this._store, BrowseProjectState)?.shortName;
    if (isNullOrUndefinedOrWhiteString(shortName)) {
      return;
    }
    this._store.dispatch(new BrowseProjectIiifCollectionAction.GetAll(shortName, queryParameters));
  }

  showDetail(iiifCollection: IIIFCollectionEntry): void {
    this._store.dispatch(new Navigate([RoutesEnum.browseProjectDetail, this.resId, BrowseProjectRoutesEnum.iiifCollection, iiifCollection.id]));
  }

  private _copyLink(iiifCollection: IIIFCollectionEntry): void {
    if (ClipboardUtil.copyStringToClipboard(iiifCollection.url)) {
      this._notificationService.showInformation(LabelTranslateEnum.copiedLinkToClipboard);
    }
  }

  private _openLink(iiifCollection: IIIFCollectionEntry): void {
    SsrUtil.window.open(iiifCollection.url, "_blank");
  }

  override getState(rootStateModel: any): BrowseProjectResearchObjectStateModel {
    const state = rootStateModel[StateEnum.browse][this._parentState][this._state];
    if (isNullOrUndefined(state)) {
      // eslint-disable-next-line no-console
      console.error("There is no state corresponding to path " + this._parentState + " > " + this._state);
    }
    return state;
  }
}
