/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-project-detail.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {PersonRole} from "@admin/models/person-role.model";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {
  ActivatedRoute,
  RouterOutlet,
} from "@angular/router";
import {BrowseProjectFundingAgencyAction} from "@app/features/browse/project/stores/funding-agency/browse-project-funding-agency.action";
import {BrowseProjectFundingAgencyState} from "@app/features/browse/project/stores/funding-agency/browse-project-funding-agency.state";
import {BrowseProjectInstitutionAction} from "@app/features/browse/project/stores/institution/browse-project-institution.action";
import {BrowseProjectInstitutionState} from "@app/features/browse/project/stores/institution/browse-project-institution.state";
import {BrowseProjectPersonRoleAction} from "@app/features/browse/project/stores/person-role/browse-project-person-role.action";
import {BrowseProjectPersonRoleState} from "@app/features/browse/project/stores/person-role/browse-project-person-role.state";
import {browseProjectActionNamespace} from "@app/features/browse/project/stores/browse-project.action";
import {
  BrowseProjectState,
  BrowseProjectStateModel,
} from "@app/features/browse/project/stores/browse-project.state";
import {BrowseProjectResearchObjectTypeState} from "@app/features/browse/project/stores/research-object-type/browse-project-research-object-type.state";
import {BrowseProjectRmlAction} from "@app/features/browse/project/stores/rml/browse-project-rml.action";
import {BrowseProjectRmlState} from "@app/features/browse/project/stores/rml/browse-project-rml.state";
import {metaInfoBrowseProjectDetail} from "@app/meta-info-list";
import {
  FundingAgency,
  Institution,
  Project,
  ResearchObjectType,
  Rml,
  Role,
  SystemProperty,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {SharedRoleState} from "@shared/stores/role/shared-role.state";
import {
  Observable,
  pipe,
} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  take,
  tap,
} from "rxjs/operators";
import {
  AbstractDetailEditCommonRoutable,
  AppSystemPropertyState,
  MemoizedUtil,
  MetaService,
  SsrUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-browse-project-detail-routable",
  templateUrl: "./browse-project-detail.routable.html",
  styleUrls: ["./browse-project-detail.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BrowseProjectDetailRoutable extends AbstractDetailEditCommonRoutable<Project, BrowseProjectStateModel> implements OnInit {
  @Select(BrowseProjectState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(BrowseProjectState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;
  systemPropertyObs: Observable<SystemProperty> = MemoizedUtil.select(this._store, AppSystemPropertyState, state => state.current);
  selectedRmlObs: Observable<Rml[]> = MemoizedUtil.selected(this._store, BrowseProjectRmlState);
  selectedResearchObjectTypeObs: Observable<ResearchObjectType[]> = MemoizedUtil.selected(this._store, BrowseProjectResearchObjectTypeState);

  selectedFundingAgenciesObs: Observable<FundingAgency[]> = MemoizedUtil.selected(this._store, BrowseProjectFundingAgencyState);
  selectedInstitutionsObs: Observable<Institution[]> = MemoizedUtil.selected(this._store, BrowseProjectInstitutionState);
  listRoleObs: Observable<Role[]> = MemoizedUtil.list(this._store, SharedRoleState);

  selectedPersonRoleObs: Observable<PersonRole[]> = MemoizedUtil.selected(this._store, BrowseProjectPersonRoleState);
  currentUserIsManagerObs: Observable<boolean> = MemoizedUtil.select(this._store, BrowseProjectState, state => state.currentUserIsManager);
  currentUserIsMemberProjectObs: Observable<boolean> = MemoizedUtil.select(this._store, BrowseProjectState, state => state.currentUserIsMemberProject);

  readonly KEY_PARAM_NAME: keyof Project & string;

  override readonly deleteAvailable: boolean = false;
  override readonly editAvailable: boolean = false;
  override getByIdIfAlreadyInState: boolean = false;

  @ViewChild(RouterOutlet) outlet: RouterOutlet;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _securityService: SecurityService,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _translateService: TranslateService,
              private readonly _metaService: MetaService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.browse_project, _injector, browseProjectActionNamespace, StateEnum.browse);
  }

  ngOnInit(): void {
    super.ngOnInit();

    if (SsrUtil.isBrowser) {
      this.subscribe(this._translateService.onLangChange.pipe(
        tap(() => {
          const ontology = MemoizedUtil.currentSnapshot(this._store, BrowseProjectState);
          this._metaService.setMetaFromInfo(metaInfoBrowseProjectDetail, ontology);
        }),
      ));
    }

    this.subscribe(this.currentObs.pipe(
      filter(ontology => ontology?.resId === this._resId),
      distinctUntilChanged((previous, current) => previous?.resId === current?.resId),
      SsrUtil.isServer ? take(1) : pipe(),
      tap(ontology => {
        this._resId = ontology.resId;
        this._metaService.setMetaFromInfo(metaInfoBrowseProjectDetail, ontology);
      }),
    ));
  }

  protected _getSubResourceWithParentId(id: string): void {
    if (!this._securityService.isLoggedIn()) {
      return;
    }
    this._store.dispatch(new BrowseProjectRmlAction.GetAll(id));
    this._store.dispatch(new BrowseProjectFundingAgencyAction.GetAll(id));
    this._store.dispatch(new BrowseProjectInstitutionAction.GetAll(id));
    if (this._securityService.isManagerOfProject(id)) {
      this._store.dispatch(new BrowseProjectPersonRoleAction.GetAll(id));
    }
  }

  protected override _cleanState(): void {
    // MOVE IN PARENT COMPONENT
  }
}
