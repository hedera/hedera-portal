/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-project-manifest-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {BrowseProjectState} from "@app/features/browse/project/stores/browse-project.state";
import {
  BrowseProjectIiifManifestAction,
  browseProjectIiifManifestNamespace,
} from "@app/features/browse/project/stores/iiif-manifest/browse-project-iiif-manifest.action";
import {
  BrowseProjectIiifManifestState,
  BrowseProjectIiifManifestStateModel,
} from "@app/features/browse/project/stores/iiif-manifest/browse-project-iiif-manifest.state";
import {environment} from "@environments/environment";
import {IIIFManifestEntry} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  BrowseProjectRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  filter,
  tap,
} from "rxjs/operators";
import {
  AbstractListRoutable,
  ClipboardUtil,
  DataTableActions,
  DataTableFieldTypeEnum,
  FormValidationHelper,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  MemoizedUtil,
  NotificationService,
  OrderEnum,
  QueryParameters,
  RouterExtensionService,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

@Component({
  selector: "hedera-browse-project-manifest-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./browse-project-manifest-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BrowseProjectManifestListRoutable extends AbstractListRoutable<IIIFManifestEntry, BrowseProjectIiifManifestStateModel> {
  readonly KEY_CREATE_BUTTON: string = undefined;
  readonly KEY_BACK_BUTTON: string | undefined = undefined;
  readonly KEY_PARAM_NAME: keyof IIIFManifestEntry & any = undefined;

  override cleanIsNeeded: boolean = false;
  override skipInitialQuery: boolean = true;

  stickyTopPosition: number = environment.defaultStickyDatatableHeight;

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  resId: string;

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _notificationService: NotificationService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.browse_project_iiifManifest, browseProjectIiifManifestNamespace, _injector, {
      canCreate: false,
      canGoBack: false,
    }, StateEnum.browse_project);
    this._retrieveResIdFromUrl();
    this.listObs = MemoizedUtil.list(this._store, BrowseProjectIiifManifestState);
    this.queryParametersObs = MemoizedUtil.queryParameters(this._store, BrowseProjectIiifManifestState);
    this.isLoadingObs = MemoizedUtil.isLoading(this._store, BrowseProjectIiifManifestState);

    this.subscribe(MemoizedUtil.current(this._store, BrowseProjectState).pipe(
      filter(p => p?.resId === this.resId),
      tap(p => {
        this.onQueryParametersEvent(MemoizedUtil.queryParametersSnapshot(this._store, BrowseProjectIiifManifestState));
      }),
    ));
  }

  private _retrieveResIdFromUrl(): void {
    this.resId = this._route.snapshot.paramMap.get(SOLIDIFY_CONSTANTS.ID);
    if (isNullOrUndefined(this.resId)) {
      this.resId = this._route.parent.snapshot.paramMap.get(SOLIDIFY_CONSTANTS.ID);
    }
  }

  conditionDisplayEditButton(model: IIIFManifestEntry | undefined): boolean {
    return false;
  }

  conditionDisplayDeleteButton(model: IIIFManifestEntry | undefined): boolean {
    return false;
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "id",
        header: LabelTranslateEnum.identifier,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        alignment: "center",
        isSortable: true,
        isFilterable: true,
        width: "100px",
      },
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "url",
        header: LabelTranslateEnum.manifest,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
    ];
  }

  onQueryParametersEvent(queryParameters: QueryParameters): void {
    const shortName = MemoizedUtil.currentSnapshot(this._store, BrowseProjectState)?.shortName;
    if (isNullOrUndefinedOrWhiteString(shortName)) {
      return;
    }
    this._store.dispatch(new BrowseProjectIiifManifestAction.GetAll(shortName, queryParameters));
  }

  override showDetail(model: IIIFManifestEntry): void {
    const id = model.id;
    const projectId = MemoizedUtil.currentSnapshot(this._store, BrowseProjectState).resId;
    this._store.dispatch(new Navigate([RoutesEnum.browseProjectDetail, projectId, BrowseProjectRoutesEnum.researchObject, id]));
  }

  override getState(rootStateModel: any): BrowseProjectIiifManifestStateModel {
    const state = rootStateModel[StateEnum.browse][this._parentState][this._state];
    if (isNullOrUndefined(state)) {
      // eslint-disable-next-line no-console
      console.error("There is no state corresponding to path " + this._parentState + " > " + this._state);
    }
    return state;
  }

  protected _defineActions(): DataTableActions<IIIFManifestEntry>[] {
    return [
      {
        logo: IconNameEnum.copyToClipboard,
        callback: iiifManifestItem => this._copyLink(iiifManifestItem.url),
        placeholder: current => LabelTranslateEnum.copyLinkToClipboard,
        isWrapped: false,
      },
    ];
  }

  private _copyLink(link: string): void {
    if (ClipboardUtil.copyStringToClipboard(link)) {
      this._notificationService.showInformation(LabelTranslateEnum.copiedLinkToClipboard);
    }
  }
}
