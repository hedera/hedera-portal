/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-project-research-data-file-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {BrowseProjectState} from "@app/features/browse/project/stores/browse-project.state";
import {
  BrowseProjectResearchDataFileAction,
  browseProjectResearchDataFileNamespace,
} from "@app/features/browse/project/stores/research-data-file/browse-project-research-data-file.action";
import {
  BrowseProjectResearchDataFileState,
  BrowseProjectResearchDataFileStateModel,
} from "@app/features/browse/project/stores/research-data-file/browse-project-research-data-file.state";
import {ResearchDataFileMetadataResult} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  BrowseProjectRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ResearchObjectMetadataResultHelper} from "@shared/helpers/research-object-metadata-result.helper";
import {SecurityService} from "@shared/services/security.service";
import {
  filter,
  tap,
} from "rxjs/operators";
import {
  AbstractListRoutable,
  FormValidationHelper,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  MemoizedUtil,
  QueryParameters,
  RouterExtensionService,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

@Component({
  selector: "hedera-browse-project-research-data-file-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./browse-project-research-data-file-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BrowseProjectResearchDataFileListRoutable extends AbstractListRoutable<ResearchDataFileMetadataResult, BrowseProjectResearchDataFileStateModel> {
  readonly KEY_CREATE_BUTTON: string = undefined;
  readonly KEY_BACK_BUTTON: string | undefined = undefined;
  readonly KEY_PARAM_NAME: keyof ResearchDataFileMetadataResult & any = undefined;

  override cleanIsNeeded: boolean = false;
  override skipInitialQuery: boolean = true;

  resId: string;

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.browse_project_researchDataFile, browseProjectResearchDataFileNamespace, _injector, {
      canCreate: false,
      canGoBack: false,
    }, StateEnum.browse_project);
    this._retrieveResIdFromUrl();
    this.listObs = MemoizedUtil.list(this._store, BrowseProjectResearchDataFileState);
    this.queryParametersObs = MemoizedUtil.queryParameters(this._store, BrowseProjectResearchDataFileState);
    this.isLoadingObs = MemoizedUtil.isLoading(this._store, BrowseProjectResearchDataFileState);

    this.subscribe(MemoizedUtil.current(this._store, BrowseProjectState).pipe(
      filter(p => p?.resId === this.resId),
      tap(p => {
        this.onQueryParametersEvent(MemoizedUtil.queryParametersSnapshot(this._store, BrowseProjectResearchDataFileState));
      }),
    ));
  }

  private _retrieveResIdFromUrl(): void {
    this.resId = this._route.snapshot.paramMap.get(SOLIDIFY_CONSTANTS.ID);
    if (isNullOrUndefined(this.resId)) {
      this.resId = this._route.parent.snapshot.paramMap.get(SOLIDIFY_CONSTANTS.ID);
    }
  }

  conditionDisplayEditButton(model: ResearchDataFileMetadataResult | undefined): boolean {
    return false;
  }

  conditionDisplayDeleteButton(model: ResearchDataFileMetadataResult | undefined): boolean {
    return false;
  }

  defineColumns(): void {
    this.columns = ResearchObjectMetadataResultHelper.columns;
  }

  override onQueryParametersEvent(queryParameters: QueryParameters): void {
    const shortName = MemoizedUtil.currentSnapshot(this._store, BrowseProjectState)?.shortName;
    if (isNullOrUndefinedOrWhiteString(shortName)) {
      return;
    }
    this._store.dispatch(new BrowseProjectResearchDataFileAction.GetAll(shortName, queryParameters));
  }

  override showDetail(model: ResearchDataFileMetadataResult): void {
    this._store.dispatch(new Navigate([RoutesEnum.browseProjectDetail, model.metadata.researchProject.id, BrowseProjectRoutesEnum.researchDataFile, model.resId]));
  }

  override getState(rootStateModel: any): BrowseProjectResearchDataFileStateModel {
    const state = rootStateModel[StateEnum.browse][this._parentState][this._state];
    if (isNullOrUndefined(state)) {
      // eslint-disable-next-line no-console
      console.error("There is no state corresponding to path " + this._parentState + " > " + this._state);
    }
    return state;
  }

}
