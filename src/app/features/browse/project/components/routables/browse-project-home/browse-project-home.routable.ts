/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-project-home.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {BrowseProjectAction} from "@app/features/browse/project/stores/browse-project.action";
import {BrowseProjectState} from "@app/features/browse/project/stores/browse-project.state";
import {BrowseProjectResearchObjectTypeAction} from "@app/features/browse/project/stores/research-object-type/browse-project-research-object-type.action";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {
  BrowseProjectRoutesEnum,
  RoutesEnum,
} from "@app/shared/enums/routes.enum";
import {LocalStateModel} from "@app/shared/models/local-state.model";
import {Enums} from "@enums";
import {Project} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {SharedResourceRoleMemberContainerMode} from "@shared/components/containers/shared-resource-role-member/shared-resource-role-member-container.component";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  ExtraButtonToolbar,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MemoizedUtil,
  SOLIDIFY_CONSTANTS,
  Tab,
} from "solidify-frontend";

@Component({
  selector: "hedera-browse-project-home-routable",
  templateUrl: "./browse-project-home.routable.html",
  styleUrls: ["./browse-project-home.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BrowseProjectHomeRoutable extends SharedAbstractPresentational implements OnDestroy {
  projectObs: Observable<Project> = MemoizedUtil.current(this._store, BrowseProjectState);
  isLoadingObs: Observable<boolean>;

  @Input()
  isLoading: boolean;

  userRolesObs: Enums.UserApplicationRole.UserApplicationRoleEnum[];

  listExtraButtons: ExtraButtonToolbar<Project> [] = [
    {
      color: "primary",
      icon: IconNameEnum.ingest,
      callback: current => this._navigateToIngest(current?.resId),
      displayCondition: current => this._securityService.isLoggedIn() && isNotNullNorUndefined(current) && this._securityService.isManagerOfProject(current.resId),
      labelToTranslate: current => LabelTranslateEnum.toImport,
      order: 41,
    },
    {
      color: "primary",
      icon: IconNameEnum.administration,
      callback: current => this._navigateToAdmin(current?.resId),
      displayCondition: current => this._securityService.isLoggedIn() && isNotNullNorUndefined(current) && this._securityService.isManagerOfProject(current.resId),
      labelToTranslate: current => LabelTranslateEnum.manage,
      order: 41,
    },
  ];

  resId: string;
  private _currentTab: Tab;

  private get _rootUrl(): string[] {
    return [RoutesEnum.browseProjectDetail, this.resId];
  }

  browseTabs: Tab[] = [
    {
      id: ProjectTabIdEnum.METADATA,
      icon: IconNameEnum.metadata,
      titleToTranslate: LabelTranslateEnum.detail,
      suffixUrl: BrowseProjectRoutesEnum.metadata,
      route: () => [...this._rootUrl, BrowseProjectRoutesEnum.metadata],
    },
    {
      id: ProjectTabIdEnum.RESEARCH_OBJECT,
      icon: IconNameEnum.researchObjects,
      titleToTranslate: LabelTranslateEnum.researchObjects,
      suffixUrl: BrowseProjectRoutesEnum.researchObject,
      route: () => [...this._rootUrl, BrowseProjectRoutesEnum.researchObject],
    },
    {
      id: ProjectTabIdEnum.RESEARCH_DATA_FILE,
      icon: IconNameEnum.researchDataFile,
      titleToTranslate: LabelTranslateEnum.researchDataFiles,
      suffixUrl: BrowseProjectRoutesEnum.researchDataFile,
      route: () => [...this._rootUrl, BrowseProjectRoutesEnum.researchDataFile],
    },
    {
      id: ProjectTabIdEnum.RDF_OBJECT,
      icon: IconNameEnum.rdf,
      titleToTranslate: LabelTranslateEnum.managedRdfObjects,
      suffixUrl: BrowseProjectRoutesEnum.rdfObject,
      route: () => [...this._rootUrl, BrowseProjectRoutesEnum.rdfObject],
    },
    {
      id: ProjectTabIdEnum.MANIFEST,
      icon: IconNameEnum.manifest,
      titleToTranslate: LabelTranslateEnum.manifests,
      suffixUrl: BrowseProjectRoutesEnum.manifest,
      route: () => [...this._rootUrl, BrowseProjectRoutesEnum.manifest],
    },
    {
      id: ProjectTabIdEnum.IIIF_COLLECTION,
      icon: IconNameEnum.collection,
      titleToTranslate: LabelTranslateEnum.iiifCollections,
      suffixUrl: BrowseProjectRoutesEnum.iiifCollection,
      route: () => [...this._rootUrl, BrowseProjectRoutesEnum.iiifCollection],
    },
  ];

  get sharedResourceRoleMemberContainerMode(): typeof SharedResourceRoleMemberContainerMode {
    return SharedResourceRoleMemberContainerMode;
  }

  constructor(private readonly _store: Store,
              private readonly _route: ActivatedRoute,
              private readonly _securityService: SecurityService) {
    super();
    this.userRolesObs = this._store.selectSnapshot((state: LocalStateModel) => state.application.userRoles);
    this._retrieveResIdFromUrl();
    this._store.dispatch(new BrowseProjectAction.GetById(this.resId));
    this._store.dispatch(new BrowseProjectResearchObjectTypeAction.GetAll(this.resId));
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this._cleanState();
  }

  navigate(path: RoutesEnum): void {
    this._store.dispatch(new Navigate([path]));
  }

  setCurrentTab($event: Tab): void {
    this._currentTab = $event;
  }

  private _retrieveResIdFromUrl(): void {
    this.resId = this._route.snapshot.paramMap.get(SOLIDIFY_CONSTANTS.ID);
    if (isNullOrUndefined(this.resId)) {
      this.resId = this._route.parent.snapshot.paramMap.get(SOLIDIFY_CONSTANTS.ID);
    }
  }

  protected _cleanState(): void {
    const queryParameters = MemoizedUtil.queryParametersSnapshot(this._store, BrowseProjectState);
    this._store.dispatch(new BrowseProjectAction.Clean(false, queryParameters));
  }

  backToProjectListNavigate(): Navigate {
    return new Navigate([RoutesEnum.browseProject]);
  }

  backToProjectList(): void {
    this._store.dispatch(this.backToProjectListNavigate());
  }

  private _navigateToIngest(projectId: string): void {
    this._store.dispatch(new Navigate([RoutesEnum.ingest, projectId]));
  }

  private _navigateToAdmin(projectId: string): void {
    this._store.dispatch(new Navigate([RoutesEnum.adminProjectDetail, projectId]));
  }
}

export type ProjectTabIdEnum =
  "METADATA"
  | "RESEARCH-OBJECT"
  | "RESEARCH-DATA-FILE"
  | "RDF-OBJECT"
  | "MANIFEST"
  | "IIIF_COLLECTION";
export const ProjectTabIdEnum = {
  METADATA: "METADATA" as ProjectTabIdEnum,
  RESEARCH_OBJECT: "RESEARCH-OBJECT" as ProjectTabIdEnum,
  RESEARCH_DATA_FILE: "RESEARCH-DATA-FILE" as ProjectTabIdEnum,
  RDF_OBJECT: "RDF-OBJECT" as ProjectTabIdEnum,
  MANIFEST: "MANIFEST" as ProjectTabIdEnum,
  IIIF_COLLECTION: "IIIF_COLLECTION" as ProjectTabIdEnum,
};
