/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-project-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  BrowseProjectAction,
  browseProjectActionNamespace,
} from "@app/features/browse/project/stores/browse-project.action";
import {
  BrowseProjectState,
  BrowseProjectStateModel,
} from "@app/features/browse/project/stores/browse-project.state";
import {Project} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {
  BrowseProjectRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {SecurityService} from "@shared/services/security.service";
import {
  AbstractListRoutable,
  CookieConsentUtil,
  CookieType,
  DataTableColumns,
  DataTableFieldTypeEnum,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isTrue,
  LocalStorageHelper,
  MappingObjectUtil,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
  QueryParametersUtil,
  RouterExtensionService,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

@Component({
  selector: "hedera-browse-project-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./browse-project-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BrowseProjectListRoutable extends AbstractListRoutable<Project, BrowseProjectStateModel> {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = undefined;
  readonly KEY_PARAM_NAME: keyof Project & string = "name";

  browseProjectState: typeof BrowseProjectState = BrowseProjectState;
  protected _onlyMyProjects: boolean = undefined;

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.browse_project, browseProjectActionNamespace, _injector, {
      canCreate: false,
      canGoBack: false,
      listExtraButtons: [
        {
          color: "primary",
          icon: null,
          labelToTranslate: (current) => LabelTranslateEnum.showOnlyMyProjects,
          callback: (model, buttonElementRef, checked) => this._showOnlyMyProject(checked),
          order: 40,
          isToggleButton: true,
          isToggleChecked: LocalStorageHelper.getItem(LocalStorageEnum.browseProjectShowOnlyMyProjects) !== SOLIDIFY_CONSTANTS.STRING_FALSE,
          displayCondition: current => _securityService.isLoggedIn(),
        },
      ],
    }, StateEnum.browse);

    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.browseProjectShowOnlyMyProjects)) {
      this._onlyMyProjects = LocalStorageHelper.getItem(LocalStorageEnum.browseProjectShowOnlyMyProjects) === SOLIDIFY_CONSTANTS.STRING_TRUE;
    }
  }

  conditionDisplayEditButton(model: Project | undefined): boolean {
    if (isNullOrUndefined(model)) {
      return true;
    }
    return this._securityService.isRootOrAdmin();
  }

  conditionDisplayDeleteButton(model: Project | undefined): boolean {
    return false;
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "logo" as any,
        header: LabelTranslateEnum.logo,
        order: OrderEnum.none,
        resourceNameSpace: browseProjectActionNamespace,
        resourceState: this.browseProjectState as any,
        isFilterable: false,
        isSortable: false,
        component: DataTableComponentHelper.get(DataTableComponentEnum.logo),
        isUser: false,
        skipIfAvatarMissing: true,
        idResource: (project: Project) => project.resId,
        isLogoPresent: (project: Project) => isNotNullNorUndefined(project.logo),
        width: "45px",
      } as DataTableColumns<Project>,
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "accessPublic",
        header: LabelTranslateEnum.isPublic,
        type: DataTableFieldTypeEnum.boolean,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
      {
        field: "tripleNumber",
        header: LabelTranslateEnum.triples,
        type: DataTableFieldTypeEnum.number,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
      {
        field: "fileNumber",
        header: LabelTranslateEnum.files,
        type: DataTableFieldTypeEnum.number,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
      {
        field: "totalSize",
        header: LabelTranslateEnum.size,
        type: DataTableFieldTypeEnum.size,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        width: "100px",
      },
      {
        field: "openingDate",
        header: LabelTranslateEnum.opening,
        type: DataTableFieldTypeEnum.date,
        order: OrderEnum.descending,
        alignment: "center",
        isFilterable: false,
        isSortable: true,
      },
      {
        field: "closingDate" as any,
        header: LabelTranslateEnum.closing,
        type: DataTableFieldTypeEnum.date,
        order: OrderEnum.none,
        alignment: "center",
        isFilterable: false,
        isSortable: true,
      },
      {
        field: "resId",
        header: "",
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        component: DataTableComponentHelper.get(DataTableComponentEnum.projectMember),
        isFilterable: false,
        isSortable: false,
        width: "40px",
      },
    ];
  }

  private _showOnlyMyProject(checked: boolean): void {
    if (checked) {
      //show only authorized projects
      const currentQueryParam = MemoizedUtil.queryParametersSnapshot(this._store, BrowseProjectState);
      const newQueryParam = new QueryParameters(currentQueryParam?.paging?.pageSize);
      this._store.dispatch(new BrowseProjectAction.LoadOnlyMyProjects(newQueryParam));
      this._onlyMyProjects = true;
    } else {
      // all org units
      // remove filter by name if applied
      const queryParameters = QueryParametersUtil.clone(MemoizedUtil.queryParametersSnapshot(this._store, BrowseProjectState));
      MappingObjectUtil.delete(QueryParametersUtil.getSearchItems(queryParameters), this.KEY_PARAM_NAME);
      this._store.dispatch(new BrowseProjectAction.GetAll(queryParameters));
      this._onlyMyProjects = false;
    }

    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.browseProjectShowOnlyMyProjects)) {
      LocalStorageHelper.setItem(LocalStorageEnum.browseProjectShowOnlyMyProjects, this._onlyMyProjects + "");
    }

    this.defineColumns();
    this._changeDetector.detectChanges();
  }

  override onQueryParametersEvent(queryParameters: QueryParameters): void {
    if (LocalStorageHelper.getItem(LocalStorageEnum.browseProjectShowOnlyMyProjects) !== SOLIDIFY_CONSTANTS.STRING_FALSE &&
      (isTrue(this._onlyMyProjects) || isNullOrUndefined(this._onlyMyProjects))
      && this._securityService.isLoggedIn()) {
      this._store.dispatch(new BrowseProjectAction.LoadOnlyMyProjects(queryParameters));
    } else {
      this._store.dispatch(new BrowseProjectAction.ChangeQueryParameters(queryParameters, true));
    }
    this._changeDetector.detectChanges(); // Allow to display spinner the first time
  }

  override goToEdit(project: Project): void {
    this._store.dispatch(new Navigate([RoutesEnum.adminProjectDetail, project.resId, BrowseProjectRoutesEnum.edit]));
  }

  override showDetail(model: Project): void {
    this._store.dispatch(new Navigate([RoutesEnum.browseProjectDetail, model.resId, BrowseProjectRoutesEnum.metadata]));
  }
}
