/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-project-iiif-collection-detail.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {BrowseProjectAction} from "@app/features/browse/project/stores/browse-project.action";
import {BrowseProjectState} from "@app/features/browse/project/stores/browse-project.state";
import {environment} from "@environments/environment";
import {
  IIIFCollection,
  IIIFCollectionItem,
  ResearchObjectMetadataResult,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  BrowseProjectRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {Observable} from "rxjs";
import {
  filter,
  tap,
} from "rxjs/operators";
import {
  ClipboardUtil,
  DataTableActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  isNullOrUndefinedOrWhiteString,
  MemoizedUtil,
  NotificationService,
  OrderEnum,
  QueryParameters,
  SOLIDIFY_CONSTANTS,
  SsrUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-browse-project-iiif-collection-detail-routable",
  templateUrl: "./browse-project-iiif-collection-detail.routable.html",
  styleUrls: ["./browse-project-iiif-collection-detail.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BrowseProjectIiifCollectionDetailRoutable extends SharedAbstractRoutable {
  readonly KEY_CREATE_BUTTON: string = undefined;
  readonly KEY_BACK_BUTTON: string | undefined = undefined;
  readonly KEY_PARAM_NAME: keyof ResearchObjectMetadataResult & any = undefined;

  stickyTopPosition: number = environment.defaultStickyDatatableHeight;

  currentObs: Observable<IIIFCollection> = MemoizedUtil.select(this._store, BrowseProjectState, state => state.iiifCollections);
  listObs: Observable<IIIFCollectionItem[]> = MemoizedUtil.select(this._store, BrowseProjectState, state => state.iiifCollections?.items);
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, BrowseProjectState);

  columns: DataTableColumns<IIIFCollectionItem>[] = [
    {
      field: "type",
      header: LabelTranslateEnum.type,
      type: DataTableFieldTypeEnum.string,
      order: OrderEnum.none,
      isSortable: false,
      isFilterable: false,
    },
    {
      field: "label.en" as any,
      header: LabelTranslateEnum.nameLabel,
      type: DataTableFieldTypeEnum.string,
      order: OrderEnum.none,
      isSortable: false,
      isFilterable: false,
    },
    {
      field: "id",
      header: LabelTranslateEnum.link,
      type: DataTableFieldTypeEnum.string,
      order: OrderEnum.none,
      isSortable: false,
      isFilterable: false,
    },
  ];

  actions: DataTableActions<IIIFCollectionItem | any>[] = [
    {
      logo: IconNameEnum.externalLink,
      callback: collectionItem => this._openLink(collectionItem),
      placeholder: current => LabelTranslateEnum.show,
      isWrapped: false,
    },
    {
      logo: IconNameEnum.copyToClipboard,
      callback: collectionItem => this._copyLink(collectionItem.id),
      placeholder: current => LabelTranslateEnum.copyLinkToClipboard,
      isWrapped: false,
    },
  ];

  projectId: string;
  collectionId: string;

  constructor(private readonly _store: Store,
              private readonly _route: ActivatedRoute,
              private readonly _notificationService: NotificationService) {
    super();

    this.projectId = this._route.parent.parent.snapshot.paramMap.get(SOLIDIFY_CONSTANTS.ID);
    this.collectionId = this._route.snapshot.paramMap.get(SOLIDIFY_CONSTANTS.ID);

    this.subscribe(MemoizedUtil.current(this._store, BrowseProjectState).pipe(
      filter(p => p?.resId === this.projectId),
      tap(p => {
        this._getIiifCollection();
      }),
    ));
  }

  private _getIiifCollection(): void {
    const shortName = MemoizedUtil.currentSnapshot(this._store, BrowseProjectState)?.shortName;
    if (isNullOrUndefinedOrWhiteString(shortName)) {
      return;
    }
    this._store.dispatch(new BrowseProjectAction.GetIiifCollection(shortName, this.collectionId));
  }

  onQueryParametersEvent(queryParameters: QueryParameters): void {
    this._getIiifCollection();
  }

  showDetail(iiifCollectionItem: IIIFCollectionItem): void {
    const link = iiifCollectionItem.id;
    const lastIndex = link.lastIndexOf(SOLIDIFY_CONSTANTS.URL_SEPARATOR);
    const id = link.substring(lastIndex + 1);
    this._store.dispatch(new Navigate([RoutesEnum.browseProjectDetail, this.projectId, BrowseProjectRoutesEnum.researchObject, id]));
  }

  back(): void {
    this._store.dispatch(new Navigate([RoutesEnum.browseProjectDetail, this.projectId, BrowseProjectRoutesEnum.iiifCollection]));
  }

  private _copyLink(link: string): void {
    if (ClipboardUtil.copyStringToClipboard(link)) {
      this._notificationService.showInformation(LabelTranslateEnum.copiedLinkToClipboard);
    }
  }

  private _openLink(iiifCollectionItem: IIIFCollectionItem): void {
    SsrUtil.window.open(iiifCollectionItem.id, "_blank");
  }
}
