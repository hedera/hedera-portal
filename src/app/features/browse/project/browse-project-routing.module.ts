/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-project-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {BrowseProjectDetailRoutable} from "@app/features/browse/project/components/routables/browse-project-detail/browse-project-detail.routable";
import {BrowseProjectHomeRoutable} from "@app/features/browse/project/components/routables/browse-project-home/browse-project-home.routable";
import {BrowseProjectIiifCollectionDetailRoutable} from "@app/features/browse/project/components/routables/browse-project-iiif-collection-detail/browse-project-iiif-collection-detail.routable";
import {BrowseProjectIiifCollectionListRoutable} from "@app/features/browse/project/components/routables/browse-project-iiif-collection-list/browse-project-iiif-collection-list.routable";
import {BrowseProjectListRoutable} from "@app/features/browse/project/components/routables/browse-project-list/browse-project-list.routable";
import {BrowseProjectManifestListRoutable} from "@app/features/browse/project/components/routables/browse-project-manifest-list/browse-project-manifest-list.routable";
import {BrowseProjectRdfObjectListRoutable} from "@app/features/browse/project/components/routables/browse-project-rdf-object-list/browse-project-rdf-object-list.routable";
import {BrowseProjectResearchDataFileListRoutable} from "@app/features/browse/project/components/routables/browse-project-research-data-file-list/browse-project-research-data-file-list.routable";
import {BrowseProjectResearchObjectListRoutable} from "@app/features/browse/project/components/routables/browse-project-research-object-list/browse-project-research-object-list.routable";
import {BrowseProjectState} from "@app/features/browse/project/stores/browse-project.state";
import {BrowseStateModel} from "@app/features/browse/stores/browse.state";
import {HederaRoutes} from "@app/shared/models/hedera-route.model";
import {SharedResearchElementDetailRoutable} from "@shared/components/routables/shared-research-element-detail/shared-research-element-detail.routable";
import {SharedResearchObjectNotFoundRoutable} from "@shared/components/routables/shared-research-object-not-found/shared-research-object-not-found.routable";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  HomePageRoutesEnum,
  BrowseProjectRoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ProjectComputeIsManagerGuardService} from "@shared/guards/project-compute-is-manager.service";
import {ResearchElementGuardService} from "@shared/guards/research-element-guard.service";
import {HomeState} from "@shared/stores/home/home.state";
import {
  CombinedGuardService,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

const routes: HederaRoutes = [
  {
    path: AppRoutesEnum.root,
    component: BrowseProjectListRoutable,
    data: {},
  },
  {
    path: BrowseProjectRoutesEnum.detail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: BrowseProjectHomeRoutable,
    data: {
      breadcrumbMemoizedSelector: BrowseProjectState.currentTitle,
      noBreadcrumbLink: true,
      guards: [ProjectComputeIsManagerGuardService],
    },
    children: [
      {
        path: AppRoutesEnum.root,
        redirectTo: BrowseProjectRoutesEnum.metadata,
        pathMatch: "prefix",
      },
      {
        path: BrowseProjectRoutesEnum.metadata,
        component: BrowseProjectDetailRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.detail,
        },
        canActivate: [CombinedGuardService],
      },
      {
        path: BrowseProjectRoutesEnum.researchObject,
        component: BrowseProjectResearchObjectListRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.researchObjects,
        },
        canActivate: [CombinedGuardService],
      },
      {
        path: BrowseProjectRoutesEnum.researchObject + AppRoutesEnum.separator + AppRoutesEnum.paramId,
        component: SharedResearchElementDetailRoutable,
        data: {
          breadcrumbMemoizedSelector: HomeState.currentTitle,
          guards: [ResearchElementGuardService],
        },
        canActivate: [CombinedGuardService],
      },
      {
        path: BrowseProjectRoutesEnum.researchObject + AppRoutesEnum.separator + HomePageRoutesEnum.notFound + AppRoutesEnum.separator + AppRoutesEnum.paramId,
        component: SharedResearchObjectNotFoundRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.researchObjectNotFound,
        },
      },
      {
        path: BrowseProjectRoutesEnum.researchDataFile,
        component: BrowseProjectResearchDataFileListRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.researchDataFiles,
        },
        canActivate: [CombinedGuardService],
      },
      {
        path: BrowseProjectRoutesEnum.researchDataFile + AppRoutesEnum.separator + AppRoutesEnum.paramId,
        component: SharedResearchElementDetailRoutable,
        data: {
          breadcrumbMemoizedSelector: HomeState.currentTitle,
          guards: [ResearchElementGuardService],
        },
        canActivate: [CombinedGuardService],
      },
      {
        path: BrowseProjectRoutesEnum.researchDataFile + AppRoutesEnum.separator + HomePageRoutesEnum.notFound + AppRoutesEnum.separator + AppRoutesEnum.paramId,
        component: SharedResearchObjectNotFoundRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.researchObjectNotFound,
        },
      },
      {
        path: BrowseProjectRoutesEnum.rdfObject,
        component: BrowseProjectRdfObjectListRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.rdfObjects,
        },
        canActivate: [CombinedGuardService],
      },
      {
        path: BrowseProjectRoutesEnum.manifest,
        component: BrowseProjectManifestListRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.manifests,
        },
        canActivate: [CombinedGuardService],
      },
      {
        path: BrowseProjectRoutesEnum.iiifCollection,
        component: BrowseProjectIiifCollectionListRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.iiifCollections,
        },
        canActivate: [CombinedGuardService],
        children: [
          {
            path: AppRoutesEnum.paramId,
            component: BrowseProjectIiifCollectionDetailRoutable,
            data: {
              breadcrumbMemoizedSelector: state => {
                const link = (state[StateEnum.browse] as BrowseStateModel)?.[StateEnum.browse_project]?.iiifCollections?.id;
                return link?.substring(link.lastIndexOf(SOLIDIFY_CONSTANTS.URL_SEPARATOR) + 1);
              },
            },
            canActivate: [CombinedGuardService],
          },
        ],
      },
    ],
    canActivate: [CombinedGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BrowseProjectRoutingModule {
}
