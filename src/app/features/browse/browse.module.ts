/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {BrowseHomeRoutable} from "@app/features/browse/components/routables/browse-home/browse-home.routable";
import {BrowseOntologyState} from "@app/features/browse/ontology/stores/browse-ontology.state";
import {BrowseRoutingModule} from "@app/features/browse/browse-routing.module";
import {BrowseProjectFundingAgencyState} from "@app/features/browse/project/stores/funding-agency/browse-project-funding-agency.state";
import {BrowseProjectIiifCollectionState} from "@app/features/browse/project/stores/iiif-collection/browse-project-iiif-collection.state";
import {BrowseProjectIiifManifestState} from "@app/features/browse/project/stores/iiif-manifest/browse-project-iiif-manifest.state";
import {BrowseProjectInstitutionState} from "@app/features/browse/project/stores/institution/browse-project-institution.state";
import {BrowseProjectPersonRoleState} from "@app/features/browse/project/stores/person-role/browse-project-person-role.state";
import {BrowseProjectState} from "@app/features/browse/project/stores/browse-project.state";
import {BrowseProjectResearchDataFileState} from "@app/features/browse/project/stores/research-data-file/browse-project-research-data-file.state";
import {BrowseProjectResearchObjectTypeState} from "@app/features/browse/project/stores/research-object-type/browse-project-research-object-type.state";
import {BrowseProjectRdfObjectState} from "@app/features/browse/project/stores/rdf-object/browse-project-rdf-object.state";
import {BrowseProjectResearchObjectState} from "@app/features/browse/project/stores/research-object/browse-project-research-object.state";
import {BrowseProjectRmlState} from "@app/features/browse/project/stores/rml/browse-project-rml.state";
import {BrowseState} from "@app/features/browse/stores/browse.state";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {ModuleLoadedEnum} from "@shared/enums/module-loaded.enum";
import {SharedModule} from "@shared/shared.module";
import {SsrUtil} from "solidify-frontend";

const routables = [
  BrowseHomeRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    BrowseRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      BrowseState,
      BrowseProjectState,
      BrowseProjectRmlState,
      BrowseProjectResearchObjectTypeState,
      BrowseProjectRdfObjectState,
      BrowseProjectResearchDataFileState,
      BrowseProjectResearchObjectState,
      BrowseProjectPersonRoleState,
      BrowseProjectInstitutionState,
      BrowseProjectFundingAgencyState,
      BrowseProjectIiifManifestState,
      BrowseProjectIiifCollectionState,
      BrowseOntologyState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class BrowseModule {
  constructor() {
    if (SsrUtil.window) {
      SsrUtil.window[ModuleLoadedEnum.browseModuleLoaded] = true;
    }
  }
}
