/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-ontology-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {PublicOntology} from "@models";
import {SharedAbstractFormPresentational} from "@shared/components/presentationals/shared-abstract-form/shared-abstract.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {
  DataTableColumns,
  DataTableFieldTypeEnum,
  OrderEnum,
  PropertyName,
  RegexUtil,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "hedera-browse-ontology-form",
  templateUrl: "./browse-ontology-form.presentational.html",
  styleUrls: ["./browse-ontology-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BrowseOntologyFormPresentational extends SharedAbstractFormPresentational<PublicOntology> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  columns: DataTableColumns<string>[] = [
    {
      field: undefined,
      header: LabelTranslateEnum.nameLabel,
      type: DataTableFieldTypeEnum.string,
      order: OrderEnum.ascending,
      translate: false,
      isSortable: true,
      isFilterable: false,
    },
  ];

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.version]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: ["", [SolidifyValidator]],
      [this.formDefinition.url]: ["", [SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.external]: [true, [SolidifyValidator]],
    });
  }

  protected _bindFormTo(ontology: PublicOntology): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: [ontology.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.version]: [ontology.version, [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: [ontology.description, [SolidifyValidator]],
      [this.formDefinition.url]: [ontology.url, [SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.external]: [ontology.external, [SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(ontology: PublicOntology): PublicOntology {
    return ontology;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() version: string;
  @PropertyName() description: string;
  @PropertyName() url: string;
  @PropertyName() external: string;
}
