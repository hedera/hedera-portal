/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-ontology-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {browseOntologyActionNameSpace} from "@app/features/browse/ontology/stores/browse-ontology.action";
import {BrowseOntologyStateModel} from "@app/features/browse/ontology/stores/browse-ontology.state";
import {PublicOntology} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  AbstractListRoutable,
  DataTableActions,
  DataTableFieldTypeEnum,
  OrderEnum,
  RouterExtensionService,
  SsrUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-browse-ontology-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./browse-ontology-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BrowseOntologyListRoutable extends AbstractListRoutable<PublicOntology, BrowseOntologyStateModel> {
  readonly KEY_CREATE_BUTTON: string = undefined;
  readonly KEY_BACK_BUTTON: string | undefined = undefined;
  readonly KEY_PARAM_NAME: keyof PublicOntology & string = "name";

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.browse_ontology, browseOntologyActionNameSpace, _injector, {
      canCreate: false,
      canGoBack: false,
    }, StateEnum.browse);
  }

  conditionDisplayEditButton(model: PublicOntology | undefined): boolean {
    return false;
  }

  conditionDisplayDeleteButton(model: PublicOntology | undefined): boolean {
    return false;
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "version",
        header: LabelTranslateEnum.version,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        alignment: "center",
        isFilterable: true,
        isSortable: true,
        width: "100px",
      },
      {
        field: "external",
        header: LabelTranslateEnum.external,
        type: DataTableFieldTypeEnum.boolean,
        order: OrderEnum.none,
        alignment: "center",
        isFilterable: true,
        isSortable: true,
        width: "40px",
      },
      {
        field: "url",
        header: LabelTranslateEnum.link,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: false,
      },
    ];
  }

  protected _defineActions(): DataTableActions<PublicOntology>[] {
    return [
      {
        logo: IconNameEnum.download,
        callback: current => this._download(current),
        placeholder: current => LabelTranslateEnum.download,
        isWrapped: false,
      },
    ];
  }

  private _download(ontology: PublicOntology): void {
    SsrUtil.window.open(`${ApiEnum.ontologies}/${ontology.resId}`, "_blank");
  }
}
