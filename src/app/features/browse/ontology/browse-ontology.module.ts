/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-ontology.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {BrowseOntologyFormPresentational} from "@app/features/browse/ontology/components/presentationals/browse-ontology-form/browse-ontology-form.presentational";
import {BrowseOntologyDetailEditRoutable} from "@app/features/browse/ontology/components/routables/browse-ontology-detail-edit/browse-ontology-detail-edit.routable";
import {BrowseOntologyListRoutable} from "@app/features/browse/ontology/components/routables/browse-ontology-list/browse-ontology-list.routable";
import {BrowseOntologyRoutingModule} from "@app/features/browse/ontology/browse-ontology-routing.module";
import {BrowseOntologyState} from "@app/features/browse/ontology/stores/browse-ontology.state";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {ModuleLoadedEnum} from "@shared/enums/module-loaded.enum";
import {SsrUtil} from "solidify-frontend";

const routables = [
  BrowseOntologyDetailEditRoutable,
  BrowseOntologyListRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  BrowseOntologyFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    BrowseOntologyRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      BrowseOntologyState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class BrowseOntologyModule {
  constructor() {
    if (SsrUtil.window) {
      SsrUtil.window[ModuleLoadedEnum.browseOntologyModuleLoaded] = true;
    }
  }
}
