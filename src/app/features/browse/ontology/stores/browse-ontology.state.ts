/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-ontology.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpClient,
  HttpHeaders,
} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {
  BrowseOntologyAction,
  browseOntologyActionNameSpace,
} from "@app/features/browse/ontology/stores/browse-ontology.action";
import {environment} from "@environments/environment";
import {PublicOntology} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  defaultResourceFileStateInitValue,
  DownloadService,
  isNullOrUndefined,
  NotificationService,
  OverrideDefaultAction,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

const ATTRIBUTE_FILE: string = "ontologyFile";

export interface BrowseOntologyStateModel extends ResourceFileStateModel<PublicOntology> {
}

@Injectable()
@State<BrowseOntologyStateModel>({
  name: StateEnum.browse_ontology,
  defaults: {
    ...defaultResourceFileStateInitValue(),
  },
})
export class BrowseOntologyState extends ResourceFileState<BrowseOntologyStateModel, PublicOntology> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService,
              protected readonly _httpClient: HttpClient) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: browseOntologyActionNameSpace,
      downloadInMemory: false,
      resourceFileApiActionNameDownloadCustom: ApiActionNameEnum.DOWNLOAD_ONTOLOGY,
      customFileAttribute: ATTRIBUTE_FILE,
    }, _downloadService, ResourceFileStateModeEnum.custom, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.ontologies;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static isLoading(state: BrowseOntologyStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: BrowseOntologyStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static isReadyToBeDisplayed(state: BrowseOntologyStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: BrowseOntologyStateModel): boolean {
    return true;
  }

  @Selector()
  static currentName(state: BrowseOntologyStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @OverrideDefaultAction()
  @Action(BrowseOntologyAction.GetById)
  getById(ctx: SolidifyStateContext<BrowseOntologyStateModel>, action: BrowseOntologyAction.GetById): Observable<PublicOntology> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.set("Content-Type", "application/hal+json");
    return this._httpClient.get<PublicOntology>(`${this._urlResource}/${action.id}`, {
      headers: headers,
    }).pipe(
      tap(ontology => ctx.dispatch(new BrowseOntologyAction.GetByIdSuccess(action, ontology))),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new BrowseOntologyAction.GetByIdFail(action));
        throw new SolidifyStateError(this, error);
      }),
    );
    // return this._httpClient.get<string[]>(`${this._urlResource}/${ApiActionNameEnum.LIST_FOLDERS}?projectId=${action.projectId}`)
    //   .pipe(
    //     tap((listFolder: string[]) => {
    //       ctx.dispatch(new IngestResearchDataFileAction.GetListFolderSuccess(action, listFolder));
    //     }),
    //     catchError((error: SolidifyHttpErrorResponseModel) => {
    //       ctx.dispatch(new IngestResearchDataFileAction.GetListFolderFail(action));
    //       throw new SolidifyStateError(this, error);
    //     }),
    //   );
    //
    // let apiResultObs = undefined;
    // if (isFunction(this._optionsState.apiPathGetById)) {
    //   apiResultObs = this._apiService.get<TResource>(this._optionsState.apiPathGetById(action.id));
    // } else {
    //   apiResultObs = this._apiService.getById<TResource>(this._urlResource, action.id);
    // }
    // this.
    // return apiResultObs
    //   .pipe(
    //     tap((model: TResource) => {
    //       ctx.dispatch(ResourceActionHelper.getByIdSuccess(this._nameSpace, action, model));
    //     }),
    //     catchError((error: SolidifyHttpErrorResponseModel) => {
    //       ctx.dispatch(ResourceActionHelper.getByIdFail(this._nameSpace, action));
    //       throw new SolidifyStateError(this, error);
    //     }),
    //   );
  }
}
