/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - browse-ontology-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {BrowseOntologyDetailEditRoutable} from "@app/features/browse/ontology/components/routables/browse-ontology-detail-edit/browse-ontology-detail-edit.routable";
import {BrowseOntologyListRoutable} from "@app/features/browse/ontology/components/routables/browse-ontology-list/browse-ontology-list.routable";
import {BrowseOntologyState} from "@app/features/browse/ontology/stores/browse-ontology.state";
import {
  AppRoutesEnum,
  BrowseOntologyRoutesEnum,
} from "@shared/enums/routes.enum";
import {HederaRoutes} from "@shared/models/hedera-route.model";

const routes: HederaRoutes = [
  {
    path: AppRoutesEnum.root,
    component: BrowseOntologyListRoutable,
    data: {},
  },
  {
    path: `${BrowseOntologyRoutesEnum.detail}/${AppRoutesEnum.paramId}`,
    component: BrowseOntologyDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: BrowseOntologyState.currentName,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BrowseOntologyRoutingModule {
}
