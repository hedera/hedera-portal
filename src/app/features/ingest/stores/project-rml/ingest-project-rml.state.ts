/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-project-rml.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ingestProjectRmlNamespace} from "@app/features/ingest/stores/project-rml/ingest-project-rml.action";
import {environment} from "@environments/environment";
import {Rml} from "@models";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  AssociationState,
  AssociationStateModel,
  defaultAssociationStateInitValue,
  NotificationService,
  QueryParameters,
} from "solidify-frontend";

export interface IngestProjectRmlStateModel extends AssociationStateModel<Rml> {
}

export const defaultIngestProjectRmlStateModelValue: () => IngestProjectRmlStateModel = () =>
  ({
    ...defaultAssociationStateInitValue(),
    queryParameters: new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo),
  });

@Injectable()
@State<IngestProjectRmlStateModel>({
  name: StateEnum.ingest_project_rml,
  defaults: {
    ...defaultIngestProjectRmlStateModelValue(),
  },
})
export class IngestProjectRmlState extends AssociationState<IngestProjectRmlStateModel, Rml> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: ingestProjectRmlNamespace,
      resourceName: ApiResourceNameEnum.RML,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminProjects;
  }
}
