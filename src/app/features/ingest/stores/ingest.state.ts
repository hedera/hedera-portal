/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {IngestEditModeEnum} from "@app/features/ingest/enums/ingest-edit-mode.enum";
import {IngestTabEnum} from "@app/features/ingest/enums/ingest-tab.enum";
import {IngestRdfDatasetFileAction} from "@app/features/ingest/features/rdf-dataset-file/stores/rdf-dataset-file/ingest-rdf-dataset-file.action";
import {
  defaultIngestRdfDatasetFileStateModelValue,
  IngestRdfDatasetFileState,
  IngestRdfDatasetFileStateModel,
} from "@app/features/ingest/features/rdf-dataset-file/stores/rdf-dataset-file/ingest-rdf-dataset-file.state";
import {IngestResearchDataFileAction} from "@app/features/ingest/features/research-data-file/stores/research-data-file/ingest-research-data-file.action";
import {
  defaultIngestResearchDataFileStateModelValue,
  IngestResearchDataFileState,
  IngestResearchDataFileStateModel,
} from "@app/features/ingest/features/research-data-file/stores/research-data-file/ingest-research-data-file.state";
import {IngestSourceDatasetAction} from "@app/features/ingest/features/source-dataset/stores/source-dataset/ingest-source-dataset.action";
import {
  defaultIngestSourceDatasetStateModelValue,
  IngestSourceDatasetState,
  IngestSourceDatasetStateModel,
} from "@app/features/ingest/features/source-dataset/stores/source-dataset/ingest-source-dataset.state";
import {
  defaultIngestAuthorizedProjectStateModelValue,
  IngestAuthorizedProjectState,
  IngestAuthorizedProjectStateModel,
} from "@app/features/ingest/stores/authorized-projects/ingest-authorized-projects.state";
import {IngestAction} from "@app/features/ingest/stores/ingest.action";
import {IngestProjectResearchObjectTypeAction} from "@app/features/ingest/stores/project-research-object-type/ingest-project-research-object-type.action";
import {
  IngestProjectResearchObjectTypeState,
  IngestProjectResearchObjectTypeStateModel,
} from "@app/features/ingest/stores/project-research-object-type/ingest-project-research-object-type.state";
import {IngestProjectRmlAction} from "@app/features/ingest/stores/project-rml/ingest-project-rml.action";
import {
  defaultIngestProjectRmlStateModelValue,
  IngestProjectRmlState,
  IngestProjectRmlStateModel,
} from "@app/features/ingest/stores/project-rml/ingest-project-rml.state";
import {AppAuthorizedProjectState} from "@app/stores/authorized-project/app-authorized-project.state";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {HederaUserPreferencesUtil} from "@shared/utils/hedera-user-preferences.util";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  BaseStateModel,
  BasicState,
  isEmptyArray,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MemoizedUtil,
  NotificationService,
  SOLIDIFY_CONSTANTS,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
} from "solidify-frontend";

export interface IngestStateModel extends BaseStateModel {
  [StateEnum.ingest_authorizedProject]: IngestAuthorizedProjectStateModel;
  [StateEnum.ingest_project_rml]: IngestProjectRmlStateModel;
  [StateEnum.ingest_project_researchObjectType]: IngestProjectResearchObjectTypeStateModel;
  [StateEnum.ingest_sourceDataset]: IngestSourceDatasetStateModel;
  [StateEnum.ingest_researchDataFile]: IngestResearchDataFileStateModel;
  [StateEnum.ingest_rdfDatasetFile]: IngestRdfDatasetFileStateModel;
  activeListTab: IngestTabEnum;
  canCreate: boolean;
  canEdit: IngestEditModeEnum;
  projectId: string;
}

export const defaultIngestStateModelValue: () => IngestStateModel = () =>
  ({
    isLoadingCounter: 0,
    [StateEnum.ingest_authorizedProject]: {...defaultIngestAuthorizedProjectStateModelValue()},
    [StateEnum.ingest_project_rml]: {...defaultIngestProjectRmlStateModelValue()},
    [StateEnum.ingest_project_researchObjectType]: {...defaultIngestProjectRmlStateModelValue()},
    [StateEnum.ingest_sourceDataset]: {...defaultIngestSourceDatasetStateModelValue()},
    [StateEnum.ingest_researchDataFile]: {...defaultIngestResearchDataFileStateModelValue()},
    [StateEnum.ingest_rdfDatasetFile]: {...defaultIngestRdfDatasetFileStateModelValue()},
    activeListTab: IngestTabEnum.sourceDataset,
    canCreate: false,
    canEdit: IngestEditModeEnum.none,
    projectId: undefined,
  });

@Injectable()
@State<IngestStateModel>({
  name: StateEnum.ingest,
  defaults: {
    ...defaultIngestStateModelValue(),
  },
  children: [
    IngestAuthorizedProjectState,
    IngestProjectRmlState,
    IngestProjectResearchObjectTypeState,
    IngestSourceDatasetState,
    IngestResearchDataFileState,
    IngestRdfDatasetFileState,
  ],
})
export class IngestState extends BasicState<IngestStateModel> {

  constructor(protected readonly _store: Store,
              protected readonly _securityService: SecurityService,
              protected readonly _apiService: ApiService,
              protected readonly _notificationService: NotificationService,
  ) {
    super();
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: IngestStateModel): boolean {
    return !isNullOrUndefined(state?.[StateEnum.ingest_authorizedProject]) && !isNullOrUndefined(state?.[StateEnum.ingest_authorizedProject].current);
  }

  @Action(IngestAction.CanCreate)
  canCreate(ctx: SolidifyStateContext<IngestStateModel>, action: IngestAction.CanCreate): void {
    ctx.patchState({
      canCreate: action.canCreate,
    });
  }

  @Action(IngestAction.CanEdit)
  canEdit(ctx: SolidifyStateContext<IngestStateModel>, action: IngestAction.CanEdit): void {
    ctx.patchState({
      canEdit: action.canEdit ? IngestEditModeEnum.full : IngestEditModeEnum.none,
    });
  }

  @Action(IngestAction.SetActiveListTab)
  setActiveListTab(ctx: SolidifyStateContext<IngestStateModel>, action: IngestAction.SetActiveListTab): void {
    ctx.patchState({
      activeListTab: action.activeListTab,
    });
  }

  @Action(IngestAction.SetProject)
  setProject(ctx: SolidifyStateContext<IngestStateModel>, action: IngestAction.SetProject): void {
    const authorizedProject = ctx.getState()[StateEnum.ingest_authorizedProject];
    ctx.patchState({
      ...defaultIngestStateModelValue(),
      [StateEnum.ingest_authorizedProject]: authorizedProject,
    });
    const listAuthorizedProject = MemoizedUtil.listSnapshot(this._store, AppAuthorizedProjectState);
    if (isNullOrUndefined(listAuthorizedProject) || isEmptyArray(listAuthorizedProject)) {
      HederaUserPreferencesUtil.cleanCurrentProject();
      ctx.dispatch(new Navigate([RoutesEnum.ingest]));
      return;
    }
    const isAuthorizedProject = listAuthorizedProject.findIndex(p => p.resId === action.projectId) >= 0;
    if (!isAuthorizedProject) {
      const defaultProjectId = HederaUserPreferencesUtil.getCurrentProject(listAuthorizedProject)?.resId;
      if (isNotNullNorUndefined(defaultProjectId)) {
        ctx.dispatch(new Navigate([RoutesEnum.ingest, defaultProjectId]));
      }
      return;
    }
    ctx.patchState({
      projectId: action.projectId,
    });
    HederaUserPreferencesUtil.setCurrentProject(action.projectId);

    const canCreate = this._securityService.canCreateDatasetOnProject(action.projectId);
    const canEdit = this._securityService.canEditDatasetOnProject(action.projectId);
    ctx.dispatch([
      new IngestAction.CanCreate(canCreate),
      new IngestAction.CanEdit(canEdit),
      new IngestAction.RefreshAllCounter(action.projectId),
      new IngestProjectRmlAction.GetAll(action.projectId),
      new IngestProjectResearchObjectTypeAction.GetAll(action.projectId),
      new IngestResearchDataFileAction.ChangeCurrentFolder(SOLIDIFY_CONSTANTS.FILE_SEPARATOR),
    ]);
  }

  @Action(IngestAction.RefreshAllCounter)
  refreshAllCounter(ctx: SolidifyStateContext<IngestStateModel>, action: IngestAction.RefreshAllCounter): void {
    ctx.dispatch([
      new IngestSourceDatasetAction.RefreshTotal(action.projectId),
      new IngestRdfDatasetFileAction.RefreshTotal(action.projectId),
      new IngestResearchDataFileAction.RefreshTotal(action.projectId),
    ]);
  }

  @Action(IngestAction.RefreshIIIF)
  refreshIIIF(ctx: SolidifyStateContext<IngestStateModel>, action: IngestAction.RefreshIIIF): Observable<void> {
    return this._apiService.post<void, void>(`${ApiEnum.ingest}/${ApiActionNameEnum.IIIF_REFRESH}/${action.projectId}`)
      .pipe(
        tap(() => {
          ctx.dispatch(new IngestAction.RefreshIIIFSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new IngestAction.RefreshIIIFFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(IngestAction.RefreshIIIFSuccess)
  refreshIIIFSuccess(ctx: SolidifyStateContext<IngestStateModel>, action: IngestAction.RefreshIIIFSuccess): void {
    this._notificationService.showSuccess(LabelTranslateEnum.refreshIiifResourcesSuccessfully);
  }

  @Action(IngestAction.Refresh)
  refresh(ctx: SolidifyStateContext<IngestStateModel>, action: IngestAction.Refresh): Observable<void> {
    return this._apiService.post<void, void>(`${ApiEnum.ingest}/${ApiActionNameEnum.REFRESH}/${action.projectId}`)
      .pipe(
        tap(() => {
          ctx.dispatch(new IngestAction.RefreshSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new IngestAction.RefreshFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(IngestAction.RefreshSuccess)
  refreshSuccess(ctx: SolidifyStateContext<IngestStateModel>, action: IngestAction.RefreshSuccess): void {
    this._notificationService.showSuccess(LabelTranslateEnum.refreshResourcesSuccessfully);
  }

  @Action(IngestAction.PurgeRDF)
  purgeRDF(ctx: SolidifyStateContext<IngestStateModel>, action: IngestAction.PurgeRDF): Observable<void> {
    return this._apiService.post<void, void>(`${ApiEnum.ingest}/${ApiActionNameEnum.PURGE_RDF}/${action.projectId}`)
      .pipe(
        tap(() => {
          ctx.dispatch(new IngestAction.PurgeRDFSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new IngestAction.PurgeRDFFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(IngestAction.PurgeRDFSuccess)
  purgeRDFSuccess(ctx: SolidifyStateContext<IngestStateModel>, action: IngestAction.PurgeRDFSuccess): void {
    this._notificationService.showSuccess(LabelTranslateEnum.purgeRdfResourcesSuccessfully);
  }
}
