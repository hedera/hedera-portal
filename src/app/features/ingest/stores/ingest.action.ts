/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {IngestTabEnum} from "@app/features/ingest/enums/ingest-tab.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseStoreNameSpace,
  BaseSubActionFail,
  BaseSubActionSuccess,
} from "solidify-frontend";

const state = StateEnum.ingest;

export namespace IngestAction {
  export class CanCreate extends BaseAction {
    static readonly type: string = `[${state}] Can Create`;

    constructor(public canCreate: boolean) {
      super();
    }
  }

  export class CanEdit extends BaseAction {
    static readonly type: string = `[${state}] Can Edit`;

    constructor(public canEdit: boolean) {
      super();
    }
  }

  export class SetActiveListTab extends BaseAction {
    static readonly type: string = `[${state}] Set Active List Tab`;

    constructor(public activeListTab: IngestTabEnum) {
      super();
    }
  }

  export class SetProject {
    static readonly type: string = `[${state}] Set Project`;

    constructor(public projectId: string) {
    }
  }

  export class RefreshAllCounter {
    static readonly type: string = `[${state}] Refresh All Counter`;

    constructor(public projectId: string) {
    }
  }

  export class RefreshIIIF extends BaseAction {
    static readonly type: string = `[${state}] Refresh IIIF`;

    constructor(public projectId: string) {
      super();
    }
  }

  export class RefreshIIIFSuccess extends BaseSubActionSuccess<RefreshIIIF> {
    static readonly type: string = `[${state}] Refresh IIIF Success`;

    constructor(public parentAction: RefreshIIIF) {
      super(parentAction);
    }
  }

  export class RefreshIIIFFail extends BaseSubActionFail<RefreshIIIF> {
    static readonly type: string = `[${state}] Refresh IIIF Fail`;
  }

  export class Refresh extends BaseAction {
    static readonly type: string = `[${state}] Refresh`;

    constructor(public projectId: string) {
      super();
    }
  }

  export class RefreshSuccess extends BaseSubActionSuccess<Refresh> {
    static readonly type: string = `[${state}] Refresh Success`;

    constructor(public parentAction: Refresh) {
      super(parentAction);
    }
  }

  export class RefreshFail extends BaseSubActionFail<Refresh> {
    static readonly type: string = `[${state}] Refresh Fail`;
  }

  export class PurgeRDF extends BaseAction {
    static readonly type: string = `[${state}] Purge RDF`;

    constructor(public projectId: string) {
      super();
    }
  }

  export class PurgeRDFSuccess extends BaseSubActionSuccess<PurgeRDF> {
    static readonly type: string = `[${state}] Purge RDF Success`;

    constructor(public parentAction: PurgeRDF) {
      super(parentAction);
    }
  }

  export class PurgeRDFFail extends BaseSubActionFail<PurgeRDF> {
    static readonly type: string = `[${state}] Purge RDF Fail`;
  }
}

export const ingestActionNameSpace: BaseStoreNameSpace = IngestAction;

