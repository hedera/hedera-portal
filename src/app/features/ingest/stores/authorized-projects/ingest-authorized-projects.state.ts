/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-authorized-projects.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  DatasetAuthorizedProjectAction,
  ingestAuthorizedProjectNameSpace,
} from "@app/features/ingest/stores/authorized-projects/ingest-authorized-projects.action";
import {AppAuthorizedProjectAction} from "@app/stores/authorized-project/app-authorized-project.action";
import {environment} from "@environments/environment";
import {Project} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  defaultResourceStateInitValue,
  NotificationService,
  OverrideDefaultAction,
  ResourceState,
  ResourceStateModel,
  SolidifyStateContext,
} from "solidify-frontend";

export interface IngestAuthorizedProjectStateModel extends ResourceStateModel<Project> {
}

export const defaultIngestAuthorizedProjectStateModelValue: () => IngestAuthorizedProjectStateModel = () =>
  ({
    ...defaultResourceStateInitValue(),
  });

@Injectable()
@State<IngestAuthorizedProjectStateModel>({
  name: StateEnum.ingest_authorizedProject,
  defaults: {
    ...defaultIngestAuthorizedProjectStateModelValue(),
  },
})
export class IngestAuthorizedProjectState extends ResourceState<IngestAuthorizedProjectStateModel, Project> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: ingestAuthorizedProjectNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminAuthorizedProjects;
  }

  @OverrideDefaultAction()
  @Action(DatasetAuthorizedProjectAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<IngestAuthorizedProjectStateModel>, action: DatasetAuthorizedProjectAction.GetByIdSuccess): void {
    super.getByIdSuccess(ctx, action);
    ctx.dispatch(new AppAuthorizedProjectAction.AddOrUpdateInList(action.model));
  }

}
