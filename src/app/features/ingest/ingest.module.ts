/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {IngestHomeRoutable} from "@app/features/ingest/components/routables/ingest-home/ingest-home.routable";
import {IngestRootRoutable} from "@app/features/ingest/components/routables/ingest-root/ingest-root.routable";
import {IngestRdfDatasetFileState} from "@app/features/ingest/features/rdf-dataset-file/stores/rdf-dataset-file/ingest-rdf-dataset-file.state";
import {IngestRdfDatasetFileStatusHistoryState} from "@app/features/ingest/features/rdf-dataset-file/stores/rdf-dataset-file/status-history/ingest-rdf-dataset-file-status-history.state";
import {IngestResearchDataFileState} from "@app/features/ingest/features/research-data-file/stores/research-data-file/ingest-research-data-file.state";
import {IngestResearchDataFileStatusHistoryState} from "@app/features/ingest/features/research-data-file/stores/research-data-file/status-history/ingest-research-data-file-status-history.state";
import {IngestResearchDataFileUploadState} from "@app/features/ingest/features/research-data-file/stores/research-data-file/upload/ingest-research-data-file-upload.state";
import {IngestSourceDatasetFileState} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/ingest-source-dataset-file.state";
import {IngestSourceDatasetFileStatusHistoryState} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/status-history/ingest-source-dataset-file-status-history.state";
import {IngestSourceDatasetFileUploadState} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/upload/ingest-source-dataset-file-upload.state";
import {IngestSourceDatasetState} from "@app/features/ingest/features/source-dataset/stores/source-dataset/ingest-source-dataset.state";
import {IngestSourceDatasetStatusHistoryState} from "@app/features/ingest/features/source-dataset/stores/source-dataset/status-history/ingest-source-dataset-status-history.state";
import {IngestRoutingModule} from "@app/features/ingest/ingest-routing.module";
import {IngestService} from "@app/features/ingest/services/ingest.service";
import {IngestAuthorizedProjectState} from "@app/features/ingest/stores/authorized-projects/ingest-authorized-projects.state";
import {IngestState} from "@app/features/ingest/stores/ingest.state";
import {IngestProjectResearchObjectTypeState} from "@app/features/ingest/stores/project-research-object-type/ingest-project-research-object-type.state";
import {IngestProjectRmlState} from "@app/features/ingest/stores/project-rml/ingest-project-rml.state";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {ModuleLoadedEnum} from "@shared/enums/module-loaded.enum";
import {SharedModule} from "@shared/shared.module";
import {SsrUtil} from "solidify-frontend";

const routables = [
  IngestRootRoutable,
  IngestHomeRoutable,
];
const containers = [];
const dialogs = [];

const presentationals = [];

const states = [
  IngestState,
  IngestProjectRmlState,
  IngestProjectResearchObjectTypeState,
  IngestAuthorizedProjectState,
  IngestSourceDatasetState,
  IngestSourceDatasetStatusHistoryState,
  IngestSourceDatasetFileState,
  IngestSourceDatasetFileUploadState,
  IngestSourceDatasetFileStatusHistoryState,
  IngestResearchDataFileState,
  IngestResearchDataFileStatusHistoryState,
  IngestResearchDataFileUploadState,
  IngestRdfDatasetFileState,
  IngestRdfDatasetFileStatusHistoryState,
];

const services = [
  IngestService,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    IngestRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      ...states,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [
    ...services,
  ],
})
export class IngestModule {
  constructor() {
    if (SsrUtil.window) {
      SsrUtil.window[ModuleLoadedEnum.ingestModuleLoaded] = true;
    }
  }
}
