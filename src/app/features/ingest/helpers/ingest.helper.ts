/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpStatusCode} from "@angular/common/http";
import {environment} from "@environments/environment";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  ClipboardUtil,
  isNotNullNorUndefinedNorWhiteString,
  NotificationService,
  SolidifyHttpErrorResponseModel,
} from "solidify-frontend";

export class IngestHelper {
  static readonly KEY_PROJECT: string = "projectId";

  static displayErrorMessageOnBulkAction(notificationService: NotificationService, error: SolidifyHttpErrorResponseModel, defaultMessage: string): void {
    const errorMessage = error?.error?.message;
    const errorSubMessage = error?.error?.error;
    const message = isNotNullNorUndefinedNorWhiteString(errorMessage) ? errorMessage : defaultMessage;
    const subMessage = isNotNullNorUndefinedNorWhiteString(errorSubMessage) ? errorSubMessage : undefined;
    notificationService.showWarning(message, undefined, {
      text: LabelTranslateEnum.copy,
      callback: () => ClipboardUtil.copyStringToClipboard(subMessage),
    }, subMessage);
    if (error?.status === HttpStatusCode.BadRequest) {
      throw environment.errorToSkipInErrorHandler;
    }
  }
}
