/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-root.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
} from "@angular/core";
import {AppAuthorizedProjectAction} from "@app/stores/authorized-project/app-authorized-project.action";
import {AppAuthorizedProjectState} from "@app/stores/authorized-project/app-authorized-project.state";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {
  AppRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {HederaUserPreferencesUtil} from "@shared/utils/hedera-user-preferences.util";
import {tap} from "rxjs/operators";
import {
  isEmptyArray,
  isFalse,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-ingest-root-routable",
  templateUrl: "./ingest-root.routable.html",
  styleUrls: ["./ingest-root.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngestRootRoutable extends SharedAbstractRoutable implements OnInit {
  noProject: boolean = false;

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _notificationService: NotificationService) {
    super();
  }

  ngOnInit(): void {
    this.subscribe(StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(this._store, [
      {
        action: new AppAuthorizedProjectAction.GetAll(undefined, false, false),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedProjectAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedProjectAction.GetAllFail)),
        ],
      },
    ]).pipe(
      tap(result => {
        if (isFalse(result.success)) {
          return;
        }

        const listProjectAuthorized = MemoizedUtil.listSnapshot(this._store, AppAuthorizedProjectState);
        if (isNullOrUndefined(listProjectAuthorized) || isEmptyArray(listProjectAuthorized)) {
          this.noProject = true;
          this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.ingest.noProject"));
          this._store.dispatch(new Navigate([AppRoutesEnum.home]));
          return;
        }
        const preferredProject = HederaUserPreferencesUtil.getCurrentProject(listProjectAuthorized);
        if (isNullOrUndefined(preferredProject)) {
          return;
        }
        this._store.dispatch(new Navigate([RoutesEnum.ingest, preferredProject.resId]));
      }),
    ));
  }
}
