/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-home.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  ActivatedRoute,
  Router,
} from "@angular/router";
import {IngestTabEnum} from "@app/features/ingest/enums/ingest-tab.enum";
import {IngestResearchDataFileAction} from "@app/features/ingest/features/research-data-file/stores/research-data-file/ingest-research-data-file.action";
import {IngestService} from "@app/features/ingest/services/ingest.service";
import {ingestAuthorizedProjectNameSpace} from "@app/features/ingest/stores/authorized-projects/ingest-authorized-projects.action";
import {IngestAuthorizedProjectState} from "@app/features/ingest/stores/authorized-projects/ingest-authorized-projects.state";
import {IngestAction} from "@app/features/ingest/stores/ingest.action";
import {IngestState} from "@app/features/ingest/stores/ingest.state";
import {AppState} from "@app/stores/app.state";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {
  AppRoutesEnum,
  IngestRoutesEnum,
  RoutesEnum,
  SourceDatasetRoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  BaseFormDefinition,
  CookieConsentService,
  CookieConsentUtil,
  CookieType,
  FormValidationHelper,
  MemoizedUtil,
  OrderEnum,
  PropertyName,
  ResourceNameSpace,
  SolidifyValidator,
  Sort,
  Tab,
} from "solidify-frontend";

@Component({
  selector: "hedera-ingest-home-routable",
  templateUrl: "./ingest-home.routable.html",
  styleUrls: ["./ingest-home.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngestHomeRoutable extends SharedAbstractPresentational implements OnInit {
  readonly SOURCE_DATASET_TAB_ID: string = "sourceDataset";
  readonly RESEARCH_DATA_FILE_TAB_ID: string = "researchDataFile";
  private readonly _RDF_DATASET_FILE_TAB_ID: string = "rdfDatasetFile";

  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.createSourceDataset;

  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  form: FormGroup;

  lastSelectionProjectLocalStorageKey: LocalStorageEnum = undefined;

  private _projectId: string;
  private _currentTab: Tab;
  selectedTabIndex: number;

  get projectId(): string {
    return this.getFormControl(this.formDefinition.projectId)?.value;
  }

  list: boolean | undefined = undefined;

  displayProjectInput: boolean = true;

  datasetAuthorizedProjectSort: Sort = {
    field: "name",
    order: OrderEnum.ascending,
  };
  datasetAuthorizedProjectNameSpace: ResourceNameSpace = ingestAuthorizedProjectNameSpace;
  datasetAuthorizedProjectState: typeof IngestAuthorizedProjectState = IngestAuthorizedProjectState;

  canCreateObs: Observable<boolean> = MemoizedUtil.select(this._store, IngestState, state => state.canCreate);

  datasetTabs: Tab[] = [
    {
      id: this.SOURCE_DATASET_TAB_ID,
      icon: IconNameEnum.ingest,
      titleToTranslate: LabelTranslateEnum.sourceDataset,
      suffixUrl: IngestRoutesEnum.sourceDataset,
      route: () => [...this._rootUrl, this._projectId, IngestRoutesEnum.sourceDataset],
      dataTest: DataTestEnum.ingestTabSourceDataset,
      numberNew: () => MemoizedUtil.select(this._store, IngestState, state => state[StateEnum.ingest_sourceDataset]?.totalCount),
    },
    {
      id: this._RDF_DATASET_FILE_TAB_ID,
      icon: IconNameEnum.rdf,
      titleToTranslate: LabelTranslateEnum.rdfDatasetFile,
      suffixUrl: IngestRoutesEnum.rdfDatasetFile,
      route: () => [...this._rootUrl, this._projectId, IngestRoutesEnum.rdfDatasetFile],
      dataTest: DataTestEnum.ingestTabResearchDataFile,
      numberNew: () => MemoizedUtil.select(this._store, IngestState, state => state[StateEnum.ingest_rdfDatasetFile]?.totalCount),
    },
    {
      id: this.RESEARCH_DATA_FILE_TAB_ID,
      icon: IconNameEnum.researchDataFile,
      titleToTranslate: LabelTranslateEnum.researchDataFile,
      suffixUrl: IngestRoutesEnum.researchDataFile,
      route: () => [...this._rootUrl, this._projectId, IngestRoutesEnum.researchDataFile],
      dataTest: DataTestEnum.ingestTabResearchDataFile,
      numberNew: () => MemoizedUtil.select(this._store, IngestState, state => state[StateEnum.ingest_researchDataFile]?.totalCount),
    },
  ];

  private get _rootUrl(): string[] {
    return [RoutesEnum.ingest];
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              private readonly _fb: FormBuilder,
              protected readonly _route: ActivatedRoute,
              protected readonly _router: Router,
              private readonly _cookieConsentService: CookieConsentService,
              protected readonly _datasetService: IngestService,
              public readonly securityService: SecurityService) {
    super();

    this.subscribe(this._cookieConsentService.watchCookiePreferenceUpdateObs(CookieType.localStorage, LocalStorageEnum.singleSelectLastSelectionProject)
      .pipe(
        tap(p => {
          this._setLastSelectionProjectKeyIfFeatureEnabled();
          this._changeDetector.detectChanges();
        }),
      ));
    this._setLastSelectionProjectKeyIfFeatureEnabled();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._retrieveProjectInUrl();
    this._updateCurrentTab();

    this.subscribe(this._datasetService.hideDepositHeader.pipe(
      tap((detail: boolean) => {
        this.list = !detail;
        this._changeDetector.detectChanges();
      }),
    ));

    this.form = this._fb.group({
      [this.formDefinition.projectId]: [this._projectId, [Validators.required, SolidifyValidator]],
    });

    if (MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isInTourMode)) {
      // TODO MANAGE SPECIFIC BEHAVIOR IN TOUR MODE
      return;
    }

    // Project change via URL
    this.subscribe(this._route.url.pipe(distinctUntilChanged()),
      value => {
        this._retrieveProjectInUrl();
        if (this.form.get(this.formDefinition.projectId).value !== this._projectId) {
          this.form.get(this.formDefinition.projectId).setValue(this._projectId);
          this._refreshDisplayProjectInput();
        }
        this._store.dispatch(new IngestAction.SetProject(this._projectId));
        this._changeDetector.detectChanges();
      });

    // Route change via Input Selector
    this.subscribe(this.form.get(this.formDefinition.projectId).valueChanges.pipe(
      distinctUntilChanged(),
      tap(resId => {
        let route = [];
        // Redirect to same tab with new url
        if (this._currentTab.id === this.SOURCE_DATASET_TAB_ID) {
          route = [IngestRoutesEnum.sourceDataset];
        } else if (this._currentTab.id === this.RESEARCH_DATA_FILE_TAB_ID) {
          route = [IngestRoutesEnum.researchDataFile];
        } else if (this._currentTab.id === this._RDF_DATASET_FILE_TAB_ID) {
          route = [IngestRoutesEnum.rdfDatasetFile];
        }
        this._store.dispatch(new Navigate([RoutesEnum.ingest, resId, ...route]));
        this._changeDetector.detectChanges();
      }),
    ));
  }

  // private _observeRoutingUpdate(): Observable<any> {
  //   return this._router.events
  //     .pipe(
  //       filter(event => event instanceof NavigationCancel || event instanceof NavigationEnd),
  //       distinctUntilChanged(),
  //       tap(event => {
  //         this._updateCurrentTab();
  //       }),
  //     );
  // }

  private _setLastSelectionProjectKeyIfFeatureEnabled(): void {
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.singleSelectLastSelectionProject)) {
      this.lastSelectionProjectLocalStorageKey = LocalStorageEnum.singleSelectLastSelectionProject;
    } else {
      this.lastSelectionProjectLocalStorageKey = undefined;
    }
  }

  private _updateCurrentTab(): void {
    const tab: IngestTabEnum = this._route.children[0].snapshot.url[0].path as IngestTabEnum;
    this.selectedTabIndex = this.datasetTabs.findIndex(t => t.id === tab);
    if (this.selectedTabIndex !== -1) {
      this._currentTab = this.datasetTabs[this.selectedTabIndex];
    }

    this._store.dispatch(new IngestAction.SetActiveListTab(tab));
    this.setCurrentTab(this._currentTab);
  }

  private _retrieveProjectInUrl(): void {
    this._projectId = this._route.snapshot.paramMap.get(AppRoutesEnum.paramIdProjectWithoutPrefixParam);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  create(): void {
    this._store.dispatch(new Navigate([RoutesEnum.ingest, this._projectId, IngestRoutesEnum.sourceDataset, SourceDatasetRoutesEnum.create], {}, {skipLocationChange: true}));
  }

  setCurrentTab($event: Tab): void {
    this._currentTab = $event;
  }

  // onQueryParametersEvent(queryParameters: QueryParameters, resourceNameSpace: ResourceNameSpace, needCopy: boolean = true): void {
  //   if (needCopy) {
  //     queryParameters = QueryParametersUtil.clone(queryParameters);
  //   }
  //   this._updateQueryParameterWithProjectId(queryParameters, {resId: this._projectId});
  //
  //   this._store.dispatch(ResourceActionHelper.changeQueryParameters(resourceNameSpace, queryParameters, true));
  // }

  // private _updateQueryParameterWithProjectId(queryParameters: QueryParameters, project: Project | undefined): QueryParameters | undefined {
  //   if (isNullOrUndefined(project)) {
  //     return queryParameters;
  //   }
  //   HederaUserPreferencesUtil.setPreferredProjectInDatasetMenu(project.resId);
  //   MappingObjectUtil.set(queryParameters.search.searchItems, IngestHelper.KEY_PROJECT, project.resId);
  //   return queryParameters;
  // }

  private _refreshDisplayProjectInput(): void {
    this.displayProjectInput = false;
    setTimeout(() => {
      this.displayProjectInput = true;
      this._changeDetector.detectChanges();
    }, 0);
  }

  reload(): void {
    this._store.dispatch(new IngestResearchDataFileAction.Reload(this._projectId));
  }

  refresh(): void {
    this._store.dispatch(new IngestAction.Refresh(this._projectId));
  }

  refreshIiif(): void {
    this._store.dispatch(new IngestAction.RefreshIIIF(this._projectId));
  }

  purgeRdf(): void {
    this._store.dispatch(new IngestAction.PurgeRDF(this._projectId));
  }

  navigateToBrowse(): void {
    this._store.dispatch(new Navigate([RoutesEnum.browseProjectDetail, this._projectId]));
  }

  navigateToAdmin(): void {
    this._store.dispatch(new Navigate([RoutesEnum.adminProjectDetail, this._projectId]));
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() projectId: string;
}
