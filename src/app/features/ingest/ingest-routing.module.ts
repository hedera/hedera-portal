/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {IngestHomeRoutable} from "@app/features/ingest/components/routables/ingest-home/ingest-home.routable";
import {IngestRootRoutable} from "@app/features/ingest/components/routables/ingest-root/ingest-root.routable";
import {IngestTabEnum} from "@app/features/ingest/enums/ingest-tab.enum";
import {
  AppRoutesEnum,
  IngestRoutesEnum,
} from "@shared/enums/routes.enum";
import {ApplicationAuthorizedProjectService} from "@shared/guards/application-authorized-project.service";
import {HederaRoutes} from "@shared/models/hedera-route.model";
import {
  CombinedGuardService,
  MappingObject,
  MappingObjectUtil,
} from "solidify-frontend";

export const callbackIngestTabBreadcrumb: (params: MappingObject<string, IngestTabEnum>) => string | undefined = (params: MappingObject<string, IngestTabEnum>) => {
  const tabStatus = MappingObjectUtil.get(params, IngestRoutesEnum.paramTabWithoutPrefix as string);
  switch (tabStatus) {
    case IngestTabEnum.sourceDataset:
      return IngestTabEnum.sourceDataset;
    case IngestTabEnum.researchDataFile:
      return IngestTabEnum.researchDataFile;
    case IngestTabEnum.rdfDatasetFile:
      return IngestTabEnum.rdfDatasetFile;

  }
  return undefined;
};

const routes: HederaRoutes = [
  {
    path: AppRoutesEnum.root,
    component: IngestRootRoutable,
    data: {},
  },
  {
    path: AppRoutesEnum.paramIdProject,
    redirectTo: AppRoutesEnum.paramIdProject + AppRoutesEnum.separator + IngestRoutesEnum.sourceDataset,
    pathMatch: "full",
  },
  {
    path: AppRoutesEnum.paramIdProject,
    component: IngestHomeRoutable,
    data: {
      // breadcrumbMemoizedSelector: Source.currentOrgUnitName,
      guards: [ApplicationAuthorizedProjectService],
    },
    children: [
      {
        path: IngestRoutesEnum.sourceDataset,
        loadChildren: () => import("./features/source-dataset/ingest-source-dataset.module").then(m => m.IngestSourceDatasetModule),
        data: {
          breadcrumb: callbackIngestTabBreadcrumb,
        },
      },
      {
        path: IngestRoutesEnum.researchDataFile,
        loadChildren: () => import("./features/research-data-file/ingest-research-data-file.module").then(m => m.IngestResearchDataFileModule),
        data: {
          breadcrumb: callbackIngestTabBreadcrumb,
        },
      },
      {
        path: IngestRoutesEnum.rdfDatasetFile,
        loadChildren: () => import("./features/rdf-dataset-file/ingest-rdf-dataset-file.module").then(m => m.IngestRdfDatasetFileModule),
        data: {
          breadcrumb: callbackIngestTabBreadcrumb,
        },
      },
    ],
    canActivate: [CombinedGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IngestRoutingModule {
}
