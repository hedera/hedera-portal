/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-rdf-dataset-file-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {IngestEditModeEnum} from "@app/features/ingest/enums/ingest-edit-mode.enum";
import {IngestNavigationParamEnum} from "@app/features/ingest/enums/ingest-navigation-param.enum";
import {TripleStoreActionModeEnum} from "@app/features/ingest/features/rdf-dataset-file/enums/triple-store-action-mode.enum";
import {
  IngestRdfDatasetFileAction,
  ingestRdfDatasetFileActionNameSpace,
} from "@app/features/ingest/features/rdf-dataset-file/stores/rdf-dataset-file/ingest-rdf-dataset-file.action";
import {
  IngestRdfDatasetFileState,
  IngestRdfDatasetFileStateModel,
} from "@app/features/ingest/features/rdf-dataset-file/stores/rdf-dataset-file/ingest-rdf-dataset-file.state";
import {ingestRdfDatasetFileStatusHistoryNamespace} from "@app/features/ingest/features/rdf-dataset-file/stores/rdf-dataset-file/status-history/ingest-rdf-dataset-file-status-history.action";
import {IngestRdfDatasetFileStatusHistoryState} from "@app/features/ingest/features/rdf-dataset-file/stores/rdf-dataset-file/status-history/ingest-rdf-dataset-file-status-history.state";
import {IngestHelper} from "@app/features/ingest/helpers/ingest.helper";
import {IngestService} from "@app/features/ingest/services/ingest.service";
import {IngestState} from "@app/features/ingest/stores/ingest.state";
import {sharedSourceDatasetActionNameSpace} from "@app/shared/stores/source-dataset/shared-source-dataset.action";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  RdfDatasetFile,
  SourceDataset,
  SourceDatasetFile,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  IngestRoutesEnum,
  RoutesEnum,
  SourceDatasetRoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataFileHelper} from "@shared/helpers/data-file.helper";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {SecurityService} from "@shared/services/security.service";
import {sharedSourceDatasetFileActionNameSpace} from "@shared/stores/source-dataset-file/shared-source-dataset-file.action";
import {SharedSourceDatasetFileState} from "@shared/stores/source-dataset-file/shared-source-dataset-file.state";
import {SharedSourceDatasetState} from "@shared/stores/source-dataset/shared-source-dataset.state";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  map,
  tap,
} from "rxjs/operators";
import {
  AbstractListRoutable,
  ButtonColorEnum,
  ConfirmDialog,
  DataTableActions,
  DataTableBulkActions,
  DataTableFieldTypeEnum,
  DeleteDialog,
  DialogUtil,
  FilePreviewDialog,
  FileStatusEnum,
  isTrue,
  LABEL_TRANSLATE,
  LabelTranslateInterface,
  MappingObject,
  MappingObjectUtil,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
  QueryParametersUtil,
  ResourceNameSpace,
  RouterExtensionService,
  SOLIDIFY_CONSTANTS,
  Sort,
  StatusHistory,
  StatusHistoryDialog,
} from "solidify-frontend";

@Component({
  selector: "hedera-ingest-rdf-dataset-file-list-routable",
  templateUrl: "../../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./ingest-rdf-dataset-file-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngestRdfDatasetFileListRoutable extends AbstractListRoutable<RdfDatasetFile, IngestRdfDatasetFileStateModel> implements OnInit {
  readonly KEY_SOURCE_DATASET_RES_ID: string = "sourceDatasetFile.sourceDataset.resId";
  readonly KEY_DATASET_SOURCE_ID: string = "datasetSourceId";
  readonly KEY_SOURCE_DATASET_FILE_RES_ID: string = "sourceDatasetFile.resId";
  readonly KEY_DATASET_SOURCE_FILE_ID: string = "datasetSourceFileId";
  readonly KEY_CREATE_BUTTON: string = undefined;
  readonly KEY_BACK_BUTTON: string | undefined = undefined;
  readonly KEY_PARAM_NAME: keyof RdfDatasetFile & string = "fileName";

  override readonly columnsSkippedToClear: string[] = [IngestHelper.KEY_PROJECT];
  override skipInitialQuery: boolean = true;
  canEditObs: Observable<IngestEditModeEnum> = MemoizedUtil.select(this._store, IngestState, state => state.canEdit);

  projectIdObs: Observable<string> = MemoizedUtil.select(this._store, IngestState, state => state.projectId);

  sharedSourceDatasetSort: Sort<SourceDataset> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedSourceDatasetActionNameSpace: ResourceNameSpace = sharedSourceDatasetActionNameSpace;
  sharedSourceDatasetState: typeof SharedSourceDatasetState = SharedSourceDatasetState;

  sharedSourceDatasetFileSort: Sort<SourceDatasetFile> = {
    field: "fileName",
    order: OrderEnum.ascending,
  };
  sharedSourceDatasetFileActionNameSpace: ResourceNameSpace = sharedSourceDatasetFileActionNameSpace;
  sharedSourceDatasetFileState: typeof SharedSourceDatasetFileState = SharedSourceDatasetFileState;

  private _isFirstQueryParametersEvent: boolean = true;

  protected override _defineBulkActions(): DataTableBulkActions<RdfDatasetFile>[] {
    return [
      {
        icon: IconNameEnum.delete,
        callback: (list: RdfDatasetFile[]) => this._bulkDeleteDataFile(list.map(s => s.resId)),
        labelToTranslate: () => LabelTranslateEnum.deleteSelection,
        displayCondition: (list: RdfDatasetFile[]) => this.canEditObs.pipe(map(canEdit => canEdit === IngestEditModeEnum.full)),
        color: ButtonColorEnum.primary,
      },
      {
        icon: IconNameEnum.addInTripleStore,
        callback: (list: RdfDatasetFile[]) => this._bulkAddTripleStoreDataFile(list),
        labelToTranslate: () => LabelTranslateEnum.addInTripleStore,
        displayCondition: (list: RdfDatasetFile[]) => this.canEditObs.pipe(map(canEdit => canEdit === IngestEditModeEnum.full)),
        color: ButtonColorEnum.primary,
      },
      {
        icon: IconNameEnum.putInError,
        callback: (list: RdfDatasetFile[]) => this._bulkPutInError(list.map(s => s.resId)),
        labelToTranslate: () => LabelTranslateEnum.putInError,
        displayCondition: (list: RdfDatasetFile[]) => this._securityService.isRootOrAdmin(),
        color: ButtonColorEnum.primary,
      },
      // DISABLED FOR PERMORMANCE ISSUE
      // {
      //   icon: IconNameEnum.deleteFromTripleStore,
      //   callback: (list: RdfDatasetFile[]) => this._bulkDeleteFromTripleStore(list),
      //   labelToTranslate: () => LabelTranslateEnum.deleteFromTripleStore,
      //   displayCondition: (list: RdfDatasetFile[]) => this.canEditObs.pipe(map(canEdit => canEdit === IngestEditModeEnum.full)),
      //   color: ButtonColorEnum.primary,
      // },
    ];
  }

  private get _projectId(): string {
    return MemoizedUtil.selectSnapshot(this._store, IngestState, state => state.projectId);
  }

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService,
              @Inject(LABEL_TRANSLATE) readonly labelTranslate: LabelTranslateInterface,
              protected readonly _datasetService: IngestService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.ingest_rdfDatasetFile, ingestRdfDatasetFileActionNameSpace, _injector, {
      canCreate: false,
      canGoBack: false,
      canRefresh: false,
    }, StateEnum.ingest);
    this._datasetService.hideDepositHeader.next(false);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.subscribe(this.projectIdObs.pipe(
      distinctUntilChanged(),
      tap(projectId => {
        this.detailRouteToInterpolate = this._getDetailRouteToInterpolate();
        let queryParameters = MemoizedUtil.queryParametersSnapshot(this._store, IngestRdfDatasetFileState);
        queryParameters = QueryParametersUtil.resetToFirstPage(queryParameters);
        this.onQueryParametersEvent(queryParameters);
        this.defineColumns();
        this._store.dispatch(new IngestRdfDatasetFileAction.RefreshTotal(projectId));
      }),
    ));
  }

  conditionDisplayDeleteButton(model: RdfDatasetFile | undefined): boolean {
    return true;
  }

  conditionDisplayEditButton(model: RdfDatasetFile | undefined): boolean {
    return false;
  }

  private _bulkDeleteDataFile(listId: string[]): Observable<boolean> {
    return DialogUtil.open(this._dialog, DeleteDialog, {
        name: undefined,
        resourceNameSpace: ingestRdfDatasetFileActionNameSpace,
        message: LabelTranslateEnum.areYouSureYouWantToDeleteTheSelectedFiles,
      }, {
        minWidth: "500px",
      },
      isConfirmed => {
        if (isTrue(isConfirmed)) {
          this._bulkDeleteDataFileWithoutConfirmation(listId);
        }
      }).pipe(
      map(result => isTrue(result)),
    );
  }

  private _bulkAddTripleStoreDataFile(listRdfDatasetFiles: RdfDatasetFile[]): Observable<any> {
    return DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: LabelTranslateEnum.importInTripleStore,
      messageToTranslate: LabelTranslateEnum.areYouSureYouWantToImportToTripleStoreTheSelectedFiles,
      confirmButtonToTranslate: LabelTranslateEnum.confirm,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
    }, undefined, (isConfirmed: boolean) => {
      this._store.dispatch(new IngestRdfDatasetFileAction.ImportInTripleStoreAll(this._projectId, listRdfDatasetFiles));
    });
  }

  private _bulkPutInError(listId: string[]): void {
    this._store.dispatch(new IngestRdfDatasetFileAction.PutInErrorAll(this._projectId, listId));
  }

  private _bulkDeleteDataFileWithoutConfirmation(listId: string[]): void {
    this._store.dispatch(new IngestRdfDatasetFileAction.DeleteAll(listId));
  }

  private _bulkDeleteFromTripleStore(listRdfDatasetFiles: RdfDatasetFile[]): Observable<any> {
    return DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: LabelTranslateEnum.deleteFromTripleStore,
      messageToTranslate: LabelTranslateEnum.areYouSureYouWantToDeleteFromTripleStoreTheSelectedFiles,
      confirmButtonToTranslate: LabelTranslateEnum.confirm,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
    }, undefined, (isConfirmed: boolean) => {
      this._store.dispatch(new IngestRdfDatasetFileAction.DeleteFromTripleStoreAll(this._projectId, listRdfDatasetFiles));
    });
  }

  protected override _getDetailRouteToInterpolate(): string | undefined {
    return `${RoutesEnum.ingest}/${this._projectId}/${IngestRoutesEnum.rdfDatasetFile}/{${SOLIDIFY_CONSTANTS.RES_ID}}`;
  }

  private _preview(rdfDatasetFile: RdfDatasetFile): void {
    const dataFile = DataFileHelper.dataFileAdapter(rdfDatasetFile);
    dataFile.status = FileStatusEnum.READY;
    DialogUtil.open(this._dialog, FilePreviewDialog, {
        fileInput: {
          dataFile: dataFile,
        },
        fileDownloadUrl: `${ApiEnum.ingestRdfDatasetFiles}/${rdfDatasetFile.resId}/${ApiActionNameEnum.DOWNLOAD}`, // TODO URL WITH PARENT
      },
      {
        width: "max-content",
        maxWidth: "90vw",
        height: "min-content",
      });
  }

  private _download(rdfDatasetFile: RdfDatasetFile): void {
    this._store.dispatch(new IngestRdfDatasetFileAction.Download(this._projectId, rdfDatasetFile));
  }

  override onQueryParametersEvent(queryParameters: QueryParameters, withRefresh: boolean = true): void {
    queryParameters = QueryParametersUtil.clone(queryParameters);
    const searchItems = QueryParametersUtil.getSearchItems(queryParameters);
    MappingObjectUtil.set(searchItems, IngestHelper.KEY_PROJECT, this._projectId);
    if (this._isFirstQueryParametersEvent) {
      this._retrieveDatasetSourceIdFromUrl(searchItems);
    }
    super.onQueryParametersEvent(queryParameters);
    this._isFirstQueryParametersEvent = false;
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "sourceDatasetFile.dataset.name" as any,
        header: LabelTranslateEnum.sourceDataset,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        sortableField: this.KEY_SOURCE_DATASET_RES_ID as any,
        filterableField: this.KEY_DATASET_SOURCE_ID as any,
        isFilterable: true,
        isSortable: true,
        resourceNameSpace: this.sharedSourceDatasetActionNameSpace,
        resourceState: this.sharedSourceDatasetState as any,
        searchableSingleSelectSort: this.sharedSourceDatasetSort,
        resourceLabelCallback: (value: SourceDataset) => value.name,
        extraSearchQueryParam: {
          projectId: this._projectId,
        },
        resourceLabelKey: "name",
        maxWidth: "180px",
      },
      {
        field: "sourceDatasetFile.fileName" as any,
        header: LabelTranslateEnum.sourceDatasetFile,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        sortableField: this.KEY_SOURCE_DATASET_FILE_RES_ID as any,
        filterableField: this.KEY_DATASET_SOURCE_FILE_ID as any,
        isSortable: true,
        isFilterable: true,
        resourceNameSpace: this.sharedSourceDatasetFileActionNameSpace,
        resourceState: this.sharedSourceDatasetFileState as any,
        searchableSingleSelectSort: this.sharedSourceDatasetFileSort,
        resourceLabelCallback: (value: SourceDatasetFile) => value.fileName,
        resourceExtraLabelSecondLineCallback: (value: SourceDatasetFile) => value.dataset.name,
        extraSearchQueryParam: {
          projectId: this._projectId,
        },
        resourceLabelKey: "fileName",
        maxWidth: "180px",
      },
      {
        field: "fileName",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "rdfFormat",
        header: LabelTranslateEnum.rdfFormat,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        translate: true,
        filterEnum: Enums.FormatEnumTranslate,
        alignment: "center",
        maxWidth: "90px",
      },
      {
        field: "fileSize",
        header: LabelTranslateEnum.size,
        type: DataTableFieldTypeEnum.size,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: true,
      },
      {
        field: "statusImport",
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isSortable: true,
        isFilterable: true,
        translate: true,
        filterEnum: Enums.RdfDatasetFile.StatusImportEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.status),
        alignment: "center",
        minWidth: "140px",
        maxWidth: "180px",
      },
    ];
  }

  protected override _defineActions(): DataTableActions<RdfDatasetFile>[] {
    return [
      {
        logo: IconNameEnum.addInTripleStore,
        callback: (rdfDatasetFile: RdfDatasetFile) => this._actionToTripleStore(rdfDatasetFile, TripleStoreActionModeEnum.add),
        placeholder: current => LabelTranslateEnum.addInTripleStore,
        isWrapped: false,
      },
      {
        logo: IconNameEnum.replaceInTripleStore,
        callback: (rdfDatasetFile: RdfDatasetFile) => this._actionToTripleStore(rdfDatasetFile, TripleStoreActionModeEnum.replace),
        placeholder: current => LabelTranslateEnum.replaceInTripleStore,
        isWrapped: true,
      },
      {
        logo: IconNameEnum.putInError,
        callback: (rdfDatasetFile: RdfDatasetFile) => this._actionPutInError(rdfDatasetFile),
        placeholder: current => LabelTranslateEnum.putInError,
        isWrapped: true,
      },
      {
        logo: IconNameEnum.ingest,
        callback: (rdfDatasetFile: RdfDatasetFile) => this._seeDatasetSourceDataFile(rdfDatasetFile),
        placeholder: current => LabelTranslateEnum.seeDatasetSource,
        isWrapped: false,
      },
      {
        logo: IconNameEnum.preview,
        callback: (rdfDatasetFile: RdfDatasetFile) => this._preview(rdfDatasetFile),
        placeholder: current => LabelTranslateEnum.showPreview,
        isWrapped: false,
      },
      {
        logo: IconNameEnum.delete,
        callback: (rdfDatasetFile: RdfDatasetFile) => this._actionToTripleStore(rdfDatasetFile, TripleStoreActionModeEnum.delete),
        placeholder: current => LabelTranslateEnum.removeFromTripleStore,
        displayOnCondition: current => current.statusImport === Enums.RdfDatasetFile.StatusImportEnum.IMPORTED,
        isWrapped: true,
      },
      {
        logo: IconNameEnum.download,
        callback: (rdfDatasetFile: RdfDatasetFile) => this._download(rdfDatasetFile),
        placeholder: current => LabelTranslateEnum.download,
        isWrapped: true,
      },
      {
        logo: IconNameEnum.rml,
        callback: (sourceDatasetFile: SourceDatasetFile) => this._seeRmlFile(sourceDatasetFile),
        placeholder: current => LabelTranslateEnum.seeRmlDetail,
        displayOnCondition: current => this._securityService.isRootOrAdmin(),
        isWrapped: true,
      },
      {
        logo: IconNameEnum.history,
        callback: (rdfDatasetFile: RdfDatasetFile) => this._showHistory(rdfDatasetFile),
        placeholder: current => this.labelTranslate.dataTableShowHistoryStatus,
        isWrapped: true,
      },
      ...super._defineActions(),
    ];
  }

  private _actionToTripleStore(rdfDatasetFile: RdfDatasetFile, tripleStoreActionMode: TripleStoreActionModeEnum): void {
    this._store.dispatch(new IngestRdfDatasetFileAction.ActionInTripleStore(rdfDatasetFile, this._projectId, tripleStoreActionMode));
  }

  private _actionPutInError(rdfDatasetFile: RdfDatasetFile): void {
    this._store.dispatch(new IngestRdfDatasetFileAction.PutInError(this._projectId, rdfDatasetFile.resId));
  }

  private _retrieveDatasetSourceIdFromUrl(searchItems: MappingObject<string, string>): void {
    const queryParam = this._route.snapshot.queryParamMap;
    if (queryParam.has(IngestNavigationParamEnum.ingestSourceId)) {
      const datasetSourceId = queryParam.get(IngestNavigationParamEnum.ingestSourceId);
      MappingObjectUtil.set(searchItems, this.KEY_DATASET_SOURCE_ID, datasetSourceId);
    }
    if (queryParam.has(IngestNavigationParamEnum.ingestSourceFileId)) {
      const datasetSourceId = queryParam.get(IngestNavigationParamEnum.ingestSourceFileId);
      MappingObjectUtil.set(searchItems, this.KEY_DATASET_SOURCE_FILE_ID, datasetSourceId);
    }
  }

  private _seeDatasetSourceDataFile(rdfDatasetFile: RdfDatasetFile): void {
    this._store.dispatch(new Navigate([RoutesEnum.ingest, this._projectId, IngestRoutesEnum.sourceDataset, rdfDatasetFile.sourceDatasetFile.dataset.resId, SourceDatasetRoutesEnum.files, rdfDatasetFile.sourceDatasetFile.resId]));
  }

  private _seeRmlFile(rdfDatasetFile: RdfDatasetFile): void {
    this._store.dispatch(new Navigate([RoutesEnum.adminRmlDetail, rdfDatasetFile.rmlId]));
  }

  private _showHistory(rdfDatasetFile: RdfDatasetFile): void {
    const isLoadingHistoryObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, IngestRdfDatasetFileStatusHistoryState);
    const queryParametersHistoryObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, IngestRdfDatasetFileStatusHistoryState, state => state.queryParameters);
    const historyObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, IngestRdfDatasetFileStatusHistoryState, state => state.history);
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: rdfDatasetFile.projectId,
      resourceResId: rdfDatasetFile.resId,
      name: rdfDatasetFile.fileName,
      statusHistory: historyObs,
      isLoading: isLoadingHistoryObs,
      queryParametersObs: queryParametersHistoryObs,
      state: ingestRdfDatasetFileStatusHistoryNamespace,
      statusEnums: Enums.RdfDatasetFile.StatusImportEnumTranslate,
    }, {
      width: environment.modalWidth,
    });
  }
}
