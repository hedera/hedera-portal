/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-rdf-dataset-file-detail.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {TripleStoreActionModeEnum} from "@app/features/ingest/features/rdf-dataset-file/enums/triple-store-action-mode.enum";
import {
  IngestRdfDatasetFileAction,
  ingestRdfDatasetFileActionNameSpace,
} from "@app/features/ingest/features/rdf-dataset-file/stores/rdf-dataset-file/ingest-rdf-dataset-file.action";
import {IngestRdfDatasetFileState} from "@app/features/ingest/features/rdf-dataset-file/stores/rdf-dataset-file/ingest-rdf-dataset-file.state";
import {ingestRdfDatasetFileStatusHistoryNamespace} from "@app/features/ingest/features/rdf-dataset-file/stores/rdf-dataset-file/status-history/ingest-rdf-dataset-file-status-history.action";
import {IngestRdfDatasetFileStatusHistoryState} from "@app/features/ingest/features/rdf-dataset-file/stores/rdf-dataset-file/status-history/ingest-rdf-dataset-file-status-history.state";
import {IngestService} from "@app/features/ingest/services/ingest.service";
import {IngestState} from "@app/features/ingest/stores/ingest.state";
import {Enums} from "@enums";
import {
  DataFile,
  RdfDatasetFile,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  DataFileProperties,
  DataFilePropertiesTypeEnum,
  SharedDataFileInformationEnum,
} from "@shared/components/containers/shared-data-file-information/shared-data-file-information.container";
import {SharedAbstractDataFileDetailRoutable} from "@shared/components/routables/abstract-data-file-detail/shared-abstract-data-file-detail.routable";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  IngestRoutesEnum,
  RoutesEnum,
  SourceDatasetRoutesEnum,
} from "@shared/enums/routes.enum";
import {SecurityService} from "@shared/services/security.service";
import {StoreDialogService} from "@shared/services/store-dialog.service";
import {
  EnumUtil,
  ExtraButtonToolbar,
  FileVisualizerService,
  LABEL_TRANSLATE,
  LabelTranslateInterface,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
} from "solidify-frontend";

@Component({
  selector: "hedera-ingest-rdf-dataset-file-detail-routable",
  templateUrl: "./ingest-rdf-dataset-file-detail.routable.html",
  styleUrls: ["./ingest-rdf-dataset-file-detail.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngestRdfDatasetFileDetailRoutable extends SharedAbstractDataFileDetailRoutable<RdfDatasetFile> {
  _properties: DataFileProperties<RdfDatasetFile>[];

  mode: SharedDataFileInformationEnum = SharedDataFileInformationEnum.resId;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _storeDialogService: StoreDialogService,
              protected readonly _fileVisualizerService: FileVisualizerService,
              protected readonly _changeDetector: ChangeDetectorRef,
              @Inject(LABEL_TRANSLATE) protected readonly _labelTranslateInterface: LabelTranslateInterface,
              protected readonly _notificationService: NotificationService,
              private readonly _securityService: SecurityService,
              protected readonly _datasetService: IngestService) {
    super(_store, _route, _actions$, _dialog, _storeDialogService, _fileVisualizerService, _changeDetector, _labelTranslateInterface,
      _notificationService, _datasetService, IngestRdfDatasetFileState, ingestRdfDatasetFileActionNameSpace,
      IngestRdfDatasetFileStatusHistoryState as any, ingestRdfDatasetFileStatusHistoryNamespace, Enums.RdfDatasetFile.StatusImportEnumTranslate);
    this._datasetService.hideDepositHeader.next(true);
    this.currentObs = MemoizedUtil.current(this._store, IngestRdfDatasetFileState);
    this._projectId = MemoizedUtil.selectSnapshot(this._store, IngestState, state => state.projectId);
  }

  defineProperties(): DataFileProperties<RdfDatasetFile>[] {
    this._properties = [
      {
        key: LabelTranslateEnum.status,
        value: EnumUtil.getKeyValue(Enums.RdfDatasetFile.StatusImportEnumTranslate, this.current?.statusImport) as any,
        type: DataFilePropertiesTypeEnum.status,
      },
      {
        key: MARK_AS_TRANSLATABLE("ingest.rdfDatasetFile.file.detail.data.fileName"),
        value: this.current?.fileName,
        type: DataFilePropertiesTypeEnum.text,
      },
      {
        key: MARK_AS_TRANSLATABLE("ingest.rdfDatasetFile.file.detail.data.smartSize"),
        value: +this.current?.fileSize,
        type: DataFilePropertiesTypeEnum.size,
      },
      {
        key: MARK_AS_TRANSLATABLE("ingest.rdfDatasetFile.file.detail.data.extension"),
        value: this.current?.extension,
        type: DataFilePropertiesTypeEnum.text,
      },
      {
        key: MARK_AS_TRANSLATABLE("ingest.rdfDatasetFile.file.detail.data.rdfFormat"),
        value: this.current?.rdfFormat,
        type: DataFilePropertiesTypeEnum.text,
      },
      {
        key: MARK_AS_TRANSLATABLE("ingest.rdfDatasetFile.file.detail.data.sourceDatasetName"),
        value: this.current?.sourceDatasetFile.dataset.name,
        type: DataFilePropertiesTypeEnum.text,
      },
      {
        key: MARK_AS_TRANSLATABLE("ingest.rdfDatasetFile.file.detail.data.sourceDatasetFileName"),
        value: this.current?.sourceDatasetFile.fileName,
        type: DataFilePropertiesTypeEnum.text,
      },
    ];
    return this._properties;
  }

  defineButtons(): ExtraButtonToolbar<RdfDatasetFile>[] {
    return [
      {
        color: "primary",
        icon: IconNameEnum.addInTripleStore,
        callback: (rdfDatasetFile: RdfDatasetFile) => this._actionToTripleStore(rdfDatasetFile, TripleStoreActionModeEnum.add),
        labelToTranslate: current => LabelTranslateEnum.addInTripleStore,
        order: 60,
      },
      {
        color: "primary",
        icon: IconNameEnum.replaceInTripleStore,
        callback: (rdfDatasetFile: RdfDatasetFile) => this._actionToTripleStore(rdfDatasetFile, TripleStoreActionModeEnum.replace),
        labelToTranslate: current => LabelTranslateEnum.replaceInTripleStore,
        order: 61,
      },
      {
        color: "primary",
        icon: IconNameEnum.putInError,
        callback: (rdfDatasetFile: RdfDatasetFile) => this._actionToPutInError(rdfDatasetFile),
        labelToTranslate: current => LabelTranslateEnum.putInError,
        order: 61,
      },
      {
        color: "primary",
        icon: IconNameEnum.delete,
        callback: (rdfDatasetFile: RdfDatasetFile) => this._actionToTripleStore(rdfDatasetFile, TripleStoreActionModeEnum.delete),
        labelToTranslate: current => LabelTranslateEnum.removeFromTripleStore,
        displayCondition: current => current.statusImport === Enums.RdfDatasetFile.StatusImportEnum.IMPORTED,
        order: 62,
      },
      {
        color: "primary",
        icon: IconNameEnum.ingest,
        callback: (rdfDatasetFile: RdfDatasetFile) => this._seeDatasetSourceDataFile(rdfDatasetFile),
        labelToTranslate: current => LabelTranslateEnum.seeDatasetSource,
        order: 63,
      },

    ];
  }

  defineDataFile(): DataFile<RdfDatasetFile> {
    return {
      fileName: this.current.fileName,
      fileSize: this.current.fileSize,
    };
  }

  fileDownloadUrl(): string {
    return `${ApiEnum.ingestRdfDatasetFiles}/${this.current.resId}/${ApiActionNameEnum.DL}`;
  }

  private _actionToTripleStore(rdfDatasetFile: RdfDatasetFile, tripleStoreActionMode: TripleStoreActionModeEnum): void {
    this._store.dispatch(new IngestRdfDatasetFileAction.ActionInTripleStore(rdfDatasetFile, this._projectId, tripleStoreActionMode));
  }

  private _actionToPutInError(rdfDatasetFile: RdfDatasetFile): void {
    this._store.dispatch(new IngestRdfDatasetFileAction.PutInError(this._projectId, rdfDatasetFile.resId));
  }

  private _seeDatasetSourceDataFile(rdfDatasetFile: RdfDatasetFile): void {
    this._store.dispatch(new Navigate([RoutesEnum.ingest, this._projectId, IngestRoutesEnum.sourceDataset, rdfDatasetFile.sourceDatasetFile.dataset.resId, SourceDatasetRoutesEnum.files, rdfDatasetFile.sourceDatasetFile.resId]));
  }

}
