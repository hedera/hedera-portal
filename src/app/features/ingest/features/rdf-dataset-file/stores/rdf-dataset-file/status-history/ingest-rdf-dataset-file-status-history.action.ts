/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-rdf-dataset-file-status-history.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {StateEnum} from "@shared/enums/state.enum";
import {StatusHistoryNamespace} from "@shared/stores/status-history/status-history-namespace.model";
import {StatusHistoryAction} from "@shared/stores/status-history/status-history.action";
import {TypeDefaultAction} from "solidify-frontend";

const state = StateEnum.ingest_rdfDatasetFile_history;

export namespace IngestRdfDatasetFileStatusHistoryAction {

  @TypeDefaultAction(state)
  export class History extends StatusHistoryAction.History {
  }

  @TypeDefaultAction(state)
  export class HistorySuccess extends StatusHistoryAction.HistorySuccess {
  }

  @TypeDefaultAction(state)
  export class HistoryFail extends StatusHistoryAction.HistoryFail {
  }

  @TypeDefaultAction(state)
  export class ChangeQueryParameters extends StatusHistoryAction.ChangeQueryParameters {
  }
}

export const ingestRdfDatasetFileStatusHistoryNamespace: StatusHistoryNamespace = IngestRdfDatasetFileStatusHistoryAction;
