/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-rdf-dataset-file-status-history.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ingestRdfDatasetFileStatusHistoryNamespace} from "@app/features/ingest/features/rdf-dataset-file/stores/rdf-dataset-file/status-history/ingest-rdf-dataset-file-status-history.action";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {RdfDatasetFile} from "@models";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {
  defaultStatusHistoryInitValue,
  StatusHistoryState,
  StatusHistoryStateModel,
} from "@shared/stores/status-history/status-history.state";
import {
  ApiService,
  NotificationService,
} from "solidify-frontend";

export interface IngestDataFileStatusHistoryStateModel extends StatusHistoryStateModel<RdfDatasetFile> {
}

@Injectable()
@State<IngestDataFileStatusHistoryStateModel>({
  name: StateEnum.ingest_rdfDatasetFile_history,
  defaults: {
    ...defaultStatusHistoryInitValue(),
  },
})
export class IngestRdfDatasetFileStatusHistoryState extends StatusHistoryState<IngestDataFileStatusHistoryStateModel, RdfDatasetFile> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: ingestRdfDatasetFileStatusHistoryNamespace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.ingestRdfDatasetFiles;
  }
}
