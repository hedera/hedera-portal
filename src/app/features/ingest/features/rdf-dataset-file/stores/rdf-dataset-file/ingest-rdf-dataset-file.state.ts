/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-rdf-dataset-file.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {TripleStoreActionModeEnum} from "@app/features/ingest/features/rdf-dataset-file/enums/triple-store-action-mode.enum";
import {
  IngestRdfDatasetFileAction,
  ingestRdfDatasetFileActionNameSpace,
} from "@app/features/ingest/features/rdf-dataset-file/stores/rdf-dataset-file/ingest-rdf-dataset-file.action";
import {
  IngestDataFileStatusHistoryStateModel,
  IngestRdfDatasetFileStatusHistoryState,
} from "@app/features/ingest/features/rdf-dataset-file/stores/rdf-dataset-file/status-history/ingest-rdf-dataset-file-status-history.state";
import {IngestHelper} from "@app/features/ingest/helpers/ingest.helper";
import {IngestAction} from "@app/features/ingest/stores/ingest.action";
import {IngestState} from "@app/features/ingest/stores/ingest.state";
import {AppAuthorizedProjectState} from "@app/stores/authorized-project/app-authorized-project.state";
import {environment} from "@environments/environment";
import {RdfDatasetFile} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {defaultStatusHistoryInitValue} from "@shared/stores/status-history/status-history.state";
import {Observable} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  defaultResourceStateInitValue,
  DispatchMethodEnum,
  DownloadService,
  isEmptyArray,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  QueryParameters,
  QueryParametersUtil,
  ResourceState,
  ResourceStateModel,
  Result,
  ResultActionStatusEnum,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export interface IngestRdfDatasetFileStateModel extends ResourceStateModel<RdfDatasetFile> {
  totalCount: number;
  [StateEnum.ingest_rdfDatasetFile_history]: IngestDataFileStatusHistoryStateModel;
}

export const defaultIngestRdfDatasetFileStateModelValue: () => IngestRdfDatasetFileStateModel = () =>
  ({
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultPageSize),
    totalCount: 0,
    [StateEnum.ingest_rdfDatasetFile_history]: {...defaultStatusHistoryInitValue()},
  });

@Injectable()
@State<IngestRdfDatasetFileStateModel>({
  name: StateEnum.ingest_rdfDatasetFile,
  defaults: {
    ...defaultIngestRdfDatasetFileStateModelValue(),
  },
  children: [
    IngestRdfDatasetFileStatusHistoryState,
  ],
})
export class IngestRdfDatasetFileState extends ResourceState<IngestRdfDatasetFileStateModel, RdfDatasetFile> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService,
              private readonly _securityService: SecurityService,
  ) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: ingestRdfDatasetFileActionNameSpace,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.ingestRdfDatasetFile + urlSeparator + resId,
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("ingest.rdfDatasetFile.notification.resource.delete"),
      updateSubResourceDispatchMethod: DispatchMethodEnum.SEQUENCIAL,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.ingestRdfDatasetFiles;
  }

  @Selector()
  static isLoading(state: IngestRdfDatasetFileStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: IngestRdfDatasetFileStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static isReadyToBeDisplayed(state: IngestRdfDatasetFileStateModel): boolean {
    return IngestState.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Action(IngestRdfDatasetFileAction.SetProject)
  setProject(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.SetProject): void {
    const listAuthorizedProject = MemoizedUtil.listSnapshot(this._store, AppAuthorizedProjectState);
    if (isNullOrUndefined(listAuthorizedProject) || isEmptyArray(listAuthorizedProject)) {
      return;
    }
    const canCreate = this._securityService.canCreateDatasetOnProject(action.projectId);
    ctx.dispatch(new IngestAction.CanCreate(canCreate));
    const project = listAuthorizedProject.find(o => o.resId === action.projectId);
    ctx.dispatch([
      new IngestRdfDatasetFileAction.GetByIdSuccess(action as any, project),
    ]);
  }

  @Action(IngestRdfDatasetFileAction.ActionInTripleStore)
  actionInTripleStore(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.ActionInTripleStore): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    let tripleStoreActionModeApiAction;
    switch (action.tripleStoreActionMode) {
      case TripleStoreActionModeEnum.replace:
        tripleStoreActionModeApiAction = ApiActionNameEnum.REPLACE_DATASET;
        break;
      case TripleStoreActionModeEnum.delete:
        tripleStoreActionModeApiAction = ApiActionNameEnum.DELETE_DATASET;
        break;
      case TripleStoreActionModeEnum.add:
      default:
        tripleStoreActionModeApiAction = ApiActionNameEnum.ADD_DATASET;
        break;
    }
    return this._apiService.post<void, Result>(`${this._urlResource}/${action.rdfDatasetFile.resId}/${tripleStoreActionModeApiAction}/${action.tripleStoreDatasetId}`, null)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new IngestRdfDatasetFileAction.ActionInTripleStoreSuccess(action, result));
          } else {
            ctx.dispatch(new IngestRdfDatasetFileAction.ActionInTripleStoreFail(action, result));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new IngestRdfDatasetFileAction.ActionInTripleStoreFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );

  }

  @Action(IngestRdfDatasetFileAction.ActionInTripleStoreSuccess)
  actionInTripleStoreSuccess(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.ActionInTripleStoreSuccess): void {
    this._notificationService.showSuccess(action.result?.message);
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    if (action.parentAction.refreshListAfter) {
      setTimeout(() => {
        ctx.dispatch(new IngestRdfDatasetFileAction.Refresh(this.projectId));
      }, 1000);
    }
  }

  @Action(IngestRdfDatasetFileAction.ActionInTripleStoreFail)
  actionInTripleStoreFail(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.ActionInTripleStoreFail): void {
    if (isNotNullNorUndefinedNorWhiteString(action.result?.message)) {
      if (action.result.status === ResultActionStatusEnum.NON_APPLICABLE) {
        this._notificationService.showInformation(action.result?.message);
      } else if (action.result.status === ResultActionStatusEnum.NOT_EXECUTED) {
        this._notificationService.showWarning(action.result?.message);
      }
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(IngestRdfDatasetFileAction.ImportInTripleStoreAll)
  importInTripleStoreAll(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.ImportInTripleStoreAll): Observable<void> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<string[], void>(`${this._urlResource}/${ApiActionNameEnum.ADD_DATASET}/${action.projectId}`,
      action.listRdfDatasetFiles.map(r => r.resId),
    ).pipe(
      tap(() => {
        ctx.dispatch(new IngestRdfDatasetFileAction.ImportInTripleStoreAllSuccess(action));
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new IngestRdfDatasetFileAction.ImportInTripleStoreAllFail(action));
        IngestHelper.displayErrorMessageOnBulkAction(this._notificationService, error, LabelTranslateEnum.unableToDeleteAllRdfDatasetFileInTripleStore);
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(IngestRdfDatasetFileAction.ImportInTripleStoreAllSuccess)
  importInTripleStoreAllSuccess(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.ImportInTripleStoreAllSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: QueryParametersUtil.resetToFirstPage(ctx.getState().queryParameters),
    });
    this._notificationService.showSuccess(LabelTranslateEnum.allSelectedRdfDatasetFileImportedInTripleStore);
    setTimeout(() => {
      ctx.dispatch(new IngestRdfDatasetFileAction.Refresh(action.parentAction.projectId));
    }, 1000);
  }

  @Action(IngestRdfDatasetFileAction.ImportInTripleStoreAllFail)
  importInTripleStoreAllFail(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.ImportInTripleStoreAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new IngestRdfDatasetFileAction.Refresh(action.parentAction.projectId));
  }

  @Action(IngestRdfDatasetFileAction.ReplaceInTripleStoreAll)
  replaceInTripleStoreAll(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.ReplaceInTripleStoreAll): Observable<void> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<string[], void>(`${this._urlResource}/${ApiActionNameEnum.ADD_DATASET}/${action.projectId}`,
      action.listRdfDatasetFiles.map(r => r.resId),
    ).pipe(
      tap(() => {
        ctx.dispatch(new IngestRdfDatasetFileAction.ReplaceInTripleStoreAllSuccess(action));
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new IngestRdfDatasetFileAction.ReplaceInTripleStoreAllFail(action));
        IngestHelper.displayErrorMessageOnBulkAction(this._notificationService, error, LabelTranslateEnum.unableToDeleteAllRdfDatasetFileInTripleStore);
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(IngestRdfDatasetFileAction.ReplaceInTripleStoreAllSuccess)
  replaceInTripleStoreAllSuccess(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.ReplaceInTripleStoreAllSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: QueryParametersUtil.resetToFirstPage(ctx.getState().queryParameters),
    });
    this._notificationService.showSuccess(LabelTranslateEnum.allSelectedRdfDatasetFileReplacedInTripleStore);
    setTimeout(() => {
      ctx.dispatch(new IngestRdfDatasetFileAction.Refresh(action.parentAction.projectId));
    }, 1000);
  }

  @Action(IngestRdfDatasetFileAction.ReplaceInTripleStoreAllFail)
  replaceInTripleStoreAllFail(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.ReplaceInTripleStoreAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new IngestRdfDatasetFileAction.Refresh(action.parentAction.projectId));
  }

  @Action(IngestRdfDatasetFileAction.DeleteFromTripleStoreAll)
  deleteFromTripleStoreAll(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.DeleteFromTripleStoreAll): Observable<void> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<string[], void>(`${this._urlResource}/${ApiActionNameEnum.DELETE_DATASET}/${action.projectId}`,
      action.listRdfDatasetFiles.map(r => r.resId),
    ).pipe(
      tap(() => {
        ctx.dispatch(new IngestRdfDatasetFileAction.DeleteFromTripleStoreAllSuccess(action));
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new IngestRdfDatasetFileAction.DeleteFromTripleStoreAllFail(action));
        IngestHelper.displayErrorMessageOnBulkAction(this._notificationService, error, LabelTranslateEnum.unableToDeleteAllRdfDatasetFileInTripleStore);
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(IngestRdfDatasetFileAction.DeleteFromTripleStoreAllSuccess)
  deleteFromTripleStoreAllSuccess(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.DeleteFromTripleStoreAllSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: QueryParametersUtil.resetToFirstPage(ctx.getState().queryParameters),
    });
    this._notificationService.showSuccess(LabelTranslateEnum.allSelectedRdfDatasetFileDeletedInTripleStore);
    setTimeout(() => {
      ctx.dispatch(new IngestRdfDatasetFileAction.Refresh(action.parentAction.projectId));
    }, 1000);
  }

  @Action(IngestRdfDatasetFileAction.DeleteFromTripleStoreAllFail)
  deleteFromTripleStoreAllFail(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.DeleteFromTripleStoreAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new IngestRdfDatasetFileAction.Refresh(action.parentAction.projectId));
  }

  @Action(IngestRdfDatasetFileAction.Refresh)
  refresh(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.Refresh): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const queryParameters = QueryParametersUtil.clone(ctx.getState().queryParameters);
    MappingObjectUtil.set(QueryParametersUtil.getSearchItems(queryParameters), IngestHelper.KEY_PROJECT, action.parentId);

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new IngestRdfDatasetFileAction.GetAll(queryParameters, undefined, true),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(IngestRdfDatasetFileAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(IngestRdfDatasetFileAction.GetAllFail)),
        ],
      },
      {
        action: new IngestRdfDatasetFileAction.RefreshTotal(action.parentId),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(IngestRdfDatasetFileAction.RefreshTotalSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(IngestRdfDatasetFileAction.RefreshTotalFail)),
        ],
      },
    ]).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new IngestRdfDatasetFileAction.RefreshSuccess(action));
        } else {
          ctx.dispatch(new IngestRdfDatasetFileAction.RefreshFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(IngestRdfDatasetFileAction.RefreshSuccess)
  refreshSuccess(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.RefreshSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(IngestRdfDatasetFileAction.RefreshFail)
  refreshFail(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.RefreshFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(IngestRdfDatasetFileAction.DeleteAll)
  deleteAll(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.DeleteAll): Observable<string[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.delete<string[]>(`${this._urlResource}`, action.listResId)
      .pipe(
        tap(collection => {
          ctx.dispatch(new IngestRdfDatasetFileAction.DeleteAllSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new IngestRdfDatasetFileAction.DeleteAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(IngestRdfDatasetFileAction.DeleteAllSuccess)
  deleteAllSuccess(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.DeleteAllSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: QueryParametersUtil.resetToFirstPage(ctx.getState().queryParameters),
    });
    ctx.dispatch(new IngestRdfDatasetFileAction.Refresh(this.projectId));
  }

  @Action(IngestRdfDatasetFileAction.DeleteAllFail)
  deleteAllFail(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.DeleteAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: QueryParametersUtil.resetToFirstPage(ctx.getState().queryParameters),
    });
    this._notificationService.showWarning(LabelTranslateEnum.unableToDeleteAllSelectedDataFile);
    ctx.dispatch(new IngestRdfDatasetFileAction.Refresh(this.projectId));
  }

  @Action(IngestRdfDatasetFileAction.PutInError)
  putInError(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.PutInError): Observable<any> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<undefined, Result>(`${this._urlResource}/${action.dataFileId}/${ApiActionNameEnum.PUT_IN_ERROR}`)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new IngestRdfDatasetFileAction.PutInErrorSuccess(action, result));
          } else {
            ctx.dispatch(new IngestRdfDatasetFileAction.PutInErrorFail(action, result));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new IngestRdfDatasetFileAction.PutInErrorFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(IngestRdfDatasetFileAction.PutInErrorSuccess)
  putInErrorSuccess(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.PutInErrorSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(action.result.message);
    if (action.parentAction.refreshListAfter) {
      setTimeout(() => {
        ctx.dispatch(new IngestRdfDatasetFileAction.Refresh(action.parentAction.parentId));
      }, 1000);
    }
  }

  @Action(IngestRdfDatasetFileAction.PutInErrorFail)
  putInErrorFail(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.PutInErrorFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showWarning(isNotNullNorUndefinedNorWhiteString(action.result?.message) ? action.result.message : LabelTranslateEnum.unableToPutInError);
  }

  @Action(IngestRdfDatasetFileAction.PutInErrorAll)
  putInErrorAll(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.PutInErrorAll): Observable<void> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<string[], void>(`${this._urlResource}/${ApiActionNameEnum.PUT_IN_ERROR}`,
      action.listResId,
    ).pipe(
      tap(result => {
        ctx.dispatch(new IngestRdfDatasetFileAction.PutInErrorAllSuccess(action));
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new IngestRdfDatasetFileAction.PutInErrorAllFail(action));
        IngestHelper.displayErrorMessageOnBulkAction(this._notificationService, error, LabelTranslateEnum.unableToPutInErrorAllSelectedElements);
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(IngestRdfDatasetFileAction.PutInErrorAllSuccess)
  putInErrorAllSuccess(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.PutInErrorAllSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: QueryParametersUtil.resetToFirstPage(ctx.getState().queryParameters),
    });
    this._notificationService.showSuccess(LabelTranslateEnum.allSelectedElementsPutInError);
    setTimeout(() => {
      ctx.dispatch(new IngestRdfDatasetFileAction.Refresh(action.parentAction.parentId));
    }, 1000);
  }

  @Action(IngestRdfDatasetFileAction.PutInErrorAllFail)
  putInErrorAllFail(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.PutInErrorAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new IngestRdfDatasetFileAction.Refresh(action.parentAction.parentId));
  }

  private get projectId(): string {
    return MemoizedUtil.selectSnapshot(this._store, IngestState, state => state.projectId);
  }

  @Action(IngestRdfDatasetFileAction.RefreshTotal)
  refreshTotal(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.RefreshTotal): Observable<number> {
    const queryParameters = new QueryParameters(environment.minimalPageSizeToRetrievePaginationInfo);
    MappingObjectUtil.set(QueryParametersUtil.getSearchItems(queryParameters), IngestHelper.KEY_PROJECT, action.parentId);
    return this._apiService.getCollection<RdfDatasetFile>(this._urlResource, queryParameters)
      .pipe(
        map((collection: CollectionTyped<RdfDatasetFile>) => {
          const totalItems = collection._page.totalItems;
          ctx.dispatch(new IngestRdfDatasetFileAction.RefreshTotalSuccess(action, totalItems));
          return totalItems;
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new IngestRdfDatasetFileAction.RefreshTotalFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(IngestRdfDatasetFileAction.RefreshTotalSuccess)
  refreshTotalSuccess(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.RefreshTotalSuccess): void {
    ctx.patchState({
      totalCount: action.total,
    });
  }

  @Action(IngestRdfDatasetFileAction.Download)
  download(ctx: SolidifyStateContext<IngestRdfDatasetFileStateModel>, action: IngestRdfDatasetFileAction.Download): void {
    const url = `${this._urlResource}/${action.dataFile.resId}/${ApiActionNameEnum.DOWNLOAD}`;
    const fileName = action.dataFile.fileName;
    const mimetype = "text/turtle";
    if (environment.fileExtensionToCompleteWithRealExtensionOnDownload.findIndex(e => fileName.endsWith("." + e)) >= 0) {
      // Allow to download dua and thumbnail with good extension
      this.subscribe(this._downloadService.downloadInMemory(url, fileName, true, mimetype));
    } else {
      this._downloadService.download(url, fileName, action.dataFile.fileSize, false, mimetype);
    }
  }
}
