/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-rdf-dataset-file.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {TripleStoreActionModeEnum} from "@app/features/ingest/features/rdf-dataset-file/enums/triple-store-action-mode.enum";
import {RdfDatasetFile} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  FileUploadWrapper,
  ResourceAction,
  ResourceFileAction,
  ResourceNameSpace,
  Result,
  TypeDefaultAction,
} from "solidify-frontend";

const state = StateEnum.ingest_rdfDatasetFile;

export namespace IngestRdfDatasetFileAction {
  @TypeDefaultAction(state)
  export class LoadResource extends ResourceAction.LoadResource {
  }

  @TypeDefaultAction(state)
  export class LoadResourceSuccess extends ResourceAction.LoadResourceSuccess {
  }

  @TypeDefaultAction(state)
  export class LoadResourceFail extends ResourceAction.LoadResourceFail {
  }

  @TypeDefaultAction(state)
  export class ChangeQueryParameters extends ResourceAction.ChangeQueryParameters {
  }

  @TypeDefaultAction(state)
  export class GetAll extends ResourceAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends ResourceAction.GetAllSuccess<RdfDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends ResourceAction.GetAllFail<RdfDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class GetByListId extends ResourceAction.GetByListId {
  }

  @TypeDefaultAction(state)
  export class GetByListIdSuccess extends ResourceAction.GetByListIdSuccess {
  }

  @TypeDefaultAction(state)
  export class GetByListIdFail extends ResourceAction.GetByListIdFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends ResourceAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends ResourceAction.GetByIdSuccess<RdfDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends ResourceAction.GetByIdFail<RdfDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class Create extends ResourceAction.Create<RdfDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends ResourceAction.CreateSuccess<RdfDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends ResourceAction.CreateFail<RdfDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class Update extends ResourceAction.Update<RdfDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends ResourceAction.UpdateSuccess<RdfDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends ResourceAction.UpdateFail<RdfDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class Delete extends ResourceAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends ResourceAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends ResourceAction.DeleteFail {
  }

  @TypeDefaultAction(state)
  export class DeleteList extends ResourceAction.DeleteList {
  }

  @TypeDefaultAction(state)
  export class DeleteListSuccess extends ResourceAction.DeleteListSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteListFail extends ResourceAction.DeleteListFail {
  }

  @TypeDefaultAction(state)
  export class AddInList extends ResourceAction.AddInList<RdfDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class AddInListById extends ResourceAction.AddInListById {
  }

  @TypeDefaultAction(state)
  export class AddInListByIdSuccess extends ResourceAction.AddInListByIdSuccess<RdfDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class AddInListByIdFail extends ResourceAction.AddInListByIdFail<RdfDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class RemoveInListById extends ResourceAction.RemoveInListById {
  }

  @TypeDefaultAction(state)
  export class RemoveInListByListId extends ResourceAction.RemoveInListByListId {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkList extends ResourceAction.LoadNextChunkList {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkListSuccess extends ResourceAction.LoadNextChunkListSuccess<RdfDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkListFail extends ResourceAction.LoadNextChunkListFail {
  }

  @TypeDefaultAction(state)
  export class Clean extends ResourceAction.Clean {
  }

  @TypeDefaultAction(state)
  export class GetFile extends ResourceFileAction.GetFile {
  }

  @TypeDefaultAction(state)
  export class GetFileSuccess extends ResourceFileAction.GetFileSuccess {
  }

  @TypeDefaultAction(state)
  export class GetFileFail extends ResourceFileAction.GetFileFail {
  }

  @TypeDefaultAction(state)
  export class UploadFile extends ResourceFileAction.UploadFile {
    constructor(public resId: string, public fileUploadWrapper: FileUploadWrapper) {
      super(resId, fileUploadWrapper);
    }
  }

  @TypeDefaultAction(state)
  export class UploadFileSuccess extends ResourceFileAction.UploadFileSuccess {
  }

  @TypeDefaultAction(state)
  export class UploadFileFail extends ResourceFileAction.UploadFileFail {
  }

  @TypeDefaultAction(state)
  export class GetFileByResId extends ResourceFileAction.GetFileByResId {
  }

  @TypeDefaultAction(state)
  export class GetFileByResIdSuccess extends ResourceFileAction.GetFileByResIdSuccess {
  }

  @TypeDefaultAction(state)
  export class GetFileByResIdFail extends ResourceFileAction.GetFileByResIdFail {
  }

  @TypeDefaultAction(state)
  export class DeleteFile extends ResourceFileAction.DeleteFile {
  }

  @TypeDefaultAction(state)
  export class DeleteFileSuccess extends ResourceFileAction.DeleteFileSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFileFail extends ResourceFileAction.DeleteFileFail {
  }

  export class Download {
    static readonly type: string = `[${state}] Download`;

    constructor(public projectId: string, public dataFile: RdfDatasetFile) {
    }
  }

  export class SetProject {
    static readonly type: string = `[${state}] Set Project`;

    constructor(public projectId: string) {
    }
  }

  export class ActionInTripleStore extends BaseAction {
    static readonly type: string = `[${state}] Action In TripleStore`;

    constructor(public rdfDatasetFile: RdfDatasetFile, public tripleStoreDatasetId: string, public tripleStoreActionMode: TripleStoreActionModeEnum, public refreshListAfter: boolean = true) {
      super();
    }
  }

  export class ActionInTripleStoreSuccess extends BaseSubActionSuccess<ActionInTripleStore> {
    static readonly type: string = `[${state}] Action In TripleStore Success`;

    constructor(public parentAction: ActionInTripleStore, public result: Result) {
      super(parentAction);
    }
  }

  export class ActionInTripleStoreFail extends BaseSubActionFail<ActionInTripleStore> {
    static readonly type: string = `[${state}] Action In TripleStore Fail`;

    constructor(public parentAction: ActionInTripleStore, public result?: Result) {
      super(parentAction);
    }
  }

  export class Refresh extends BaseAction {
    static readonly type: string = `[${state}] Refresh`;

    constructor(public parentId: string) {
      super();
    }
  }

  export class RefreshSuccess extends BaseSubActionSuccess<Refresh> {
    static readonly type: string = `[${state}] Refresh Success`;
  }

  export class RefreshFail extends BaseSubActionFail<Refresh> {
    static readonly type: string = `[${state}] Refresh Fail`;
  }

  export class DeleteAll extends BaseAction {
    static readonly type: string = `[${state}] Delete All`;

    constructor(public listResId: string[]) {
      super();
    }
  }

  export class DeleteAllSuccess extends BaseSubActionSuccess<DeleteAll> {
    static readonly type: string = `[${state}] Delete All Success`;
  }

  export class DeleteAllFail extends BaseSubActionFail<DeleteAll> {
    static readonly type: string = `[${state}] Delete All Fail`;
  }

  export class RefreshTotal extends BaseAction {
    static readonly type: string = `[${state}] Refresh Total`;

    constructor(public parentId: string) {
      super();
    }
  }

  export class RefreshTotalSuccess extends BaseSubActionSuccess<RefreshTotal> {
    static readonly type: string = `[${state}] Refresh Total Success`;

    constructor(public parentAction: RefreshTotal, public total: number) {
      super(parentAction);
    }
  }

  export class RefreshTotalFail extends BaseSubActionFail<RefreshTotal> {
    static readonly type: string = `[${state}] Refresh Total Fail`;
  }

  export class ImportInTripleStoreAll extends BaseAction {
    static readonly type: string = `[${state}] Import In TripleStore All`;

    constructor(public projectId: string, public listRdfDatasetFiles: RdfDatasetFile[]) {
      super();
    }
  }

  export class ImportInTripleStoreAllSuccess extends BaseSubActionSuccess<ImportInTripleStoreAll> {
    static readonly type: string = `[${state}] Import In TripleStore All Success`;
  }

  export class ImportInTripleStoreAllFail extends BaseSubActionFail<ImportInTripleStoreAll> {
    static readonly type: string = `[${state}] Import In TripleStore All Fail`;
  }

  export class ReplaceInTripleStoreAll extends BaseAction {
    static readonly type: string = `[${state}] Replace In TripleStore All`;

    constructor(public projectId: string, public listRdfDatasetFiles: RdfDatasetFile[]) {
      super();
    }
  }

  export class ReplaceInTripleStoreAllSuccess extends BaseSubActionSuccess<ReplaceInTripleStoreAll> {
    static readonly type: string = `[${state}] Replace In TripleStore All Success`;
  }

  export class ReplaceInTripleStoreAllFail extends BaseSubActionFail<ReplaceInTripleStoreAll> {
    static readonly type: string = `[${state}] Replace In TripleStore All Fail`;
  }

  export class DeleteFromTripleStoreAll extends BaseAction {
    static readonly type: string = `[${state}] Delete From TripleStore All`;

    constructor(public projectId: string, public listRdfDatasetFiles: RdfDatasetFile[]) {
      super();
    }
  }

  export class DeleteFromTripleStoreAllSuccess extends BaseSubActionSuccess<DeleteFromTripleStoreAll> {
    static readonly type: string = `[${state}] Delete from TripleStore All Success`;
  }

  export class DeleteFromTripleStoreAllFail extends BaseSubActionFail<DeleteFromTripleStoreAll> {
    static readonly type: string = `[${state}] Delete from TripleStore All Fail`;
  }

  export class PutInError extends BaseAction {
    static readonly type: string = `[${state}] Put In Error`;

    constructor(public parentId: string, public dataFileId: string, public refreshListAfter: boolean = true) {
      super();
    }
  }

  export class PutInErrorSuccess extends BaseSubActionSuccess<PutInError> {
    static readonly type: string = `[${state}] Put In Error Success`;

    constructor(public parentAction: PutInError, public result: Result) {
      super(parentAction);
    }
  }

  export class PutInErrorFail extends BaseSubActionFail<PutInError> {
    static readonly type: string = `[${state}] Put In Error Fail`;

    constructor(public parentAction: PutInError, public result?: Result) {
      super(parentAction);
    }
  }

  export class PutInErrorAll extends BaseAction {
    static readonly type: string = `[${state}] Put In Error All`;

    constructor(public parentId: string, public listResId: string[]) {
      super();
    }
  }

  export class PutInErrorAllSuccess extends BaseSubActionSuccess<PutInErrorAll> {
    static readonly type: string = `[${state}] Put In Error All Success`;
  }

  export class PutInErrorAllFail extends BaseSubActionFail<PutInErrorAll> {
    static readonly type: string = `[${state}] Put In Error All Fail`;
  }
}

export const ingestRdfDatasetFileActionNameSpace: ResourceNameSpace = IngestRdfDatasetFileAction;
