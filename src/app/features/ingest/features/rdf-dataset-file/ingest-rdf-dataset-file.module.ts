/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-rdf-dataset-file.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {IngestRdfDatasetFileDetailRoutable} from "@app/features/ingest/features/rdf-dataset-file/components/routables/ingest-rdf-dataset-file-detail/ingest-rdf-dataset-file-detail.routable";
import {IngestRdfDatasetFileListRoutable} from "@app/features/ingest/features/rdf-dataset-file/components/routables/ingest-rdf-dataset-file-list/ingest-rdf-dataset-file-list.routable";
import {IngestRdfDatasetFileRoutingModule} from "@app/features/ingest/features/rdf-dataset-file/ingest-rdf-dataset-file-routing.module";
import {IngestRdfDatasetFileState} from "@app/features/ingest/features/rdf-dataset-file/stores/rdf-dataset-file/ingest-rdf-dataset-file.state";
import {IngestRdfDatasetFileStatusHistoryState} from "@app/features/ingest/features/rdf-dataset-file/stores/rdf-dataset-file/status-history/ingest-rdf-dataset-file-status-history.state";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {ModuleLoadedEnum} from "@shared/enums/module-loaded.enum";
import {SharedModule} from "@shared/shared.module";
import {SsrUtil} from "solidify-frontend";

const routables = [
  IngestRdfDatasetFileListRoutable,
  IngestRdfDatasetFileDetailRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [];

const states = [
  IngestRdfDatasetFileState,
  IngestRdfDatasetFileStatusHistoryState,
];

const services = [];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    IngestRdfDatasetFileRoutingModule,
    // IngestModule, TODO FIND A WAY TO IMPORT
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      ...states,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [
    ...services,
  ],
})
export class IngestRdfDatasetFileModule {
  constructor() {
    if (SsrUtil.window) {
      SsrUtil.window[ModuleLoadedEnum.ingestRdfDatasetFileModuleLoaded] = true;
    }
  }
}

