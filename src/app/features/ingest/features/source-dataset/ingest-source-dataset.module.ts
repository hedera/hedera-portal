/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-source-dataset.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {MatFormFieldModule} from "@angular/material/form-field";
import {IngestSourceDatasetFileContainer} from "@app/features/ingest/features/source-dataset/components/containers/ingest-source-dataset-file/ingest-source-dataset-file.container";
import {IngestSourceDatasetUploadContainer} from "@app/features/ingest/features/source-dataset/components/containers/ingest-source-dataset-upload/ingest-source-dataset-upload.container";
import {IngestSourceDatasetFileUploadArchiveDialog} from "@app/features/ingest/features/source-dataset/components/dialogs/ingest-source-dataset-file-upload-archive/ingest-source-dataset-file-upload-archive.dialog";
import {IngestSourceDatasetFileUploadDialog} from "@app/features/ingest/features/source-dataset/components/dialogs/ingest-source-dataset-file-upload/ingest-source-dataset-file-upload.dialog";
import {IngestSourceDatasetApplyRmlDialog} from "@app/features/ingest/features/source-dataset/components/dialogs/ingest-source-dataset-apply-rml/ingest-source-dataset-apply-rml.dialog";
import {IngestSourceDatasetLinkToResearchDataFileDialog} from "@app/features/ingest/features/source-dataset/components/dialogs/ingest-source-dataset-link/ingest-source-dataset-link-to-research-data-file-dialog.component";
import {IngestSourceDatasetFormPresentational} from "@app/features/ingest/features/source-dataset/components/presentationals/ingest-source-dataset-form/ingest-source-dataset-form.presentational";
import {IngestSourceDatasetCreateRoutable} from "@app/features/ingest/features/source-dataset/components/routables/ingest-source-dataset-create/ingest-source-dataset-create.routable";
import {IngestSourceDatasetDetailEditRoutable} from "@app/features/ingest/features/source-dataset/components/routables/ingest-source-dataset-detail-edit/ingest-source-dataset-detail-edit.routable";
import {IngestSourceDatasetFileDetailRoutable} from "@app/features/ingest/features/source-dataset/components/routables/ingest-source-dataset-file-detail/ingest-source-dataset-file-detail.routable";
import {IngestSourceDatasetListRoutable} from "@app/features/ingest/features/source-dataset/components/routables/ingest-source-dataset-list/ingest-source-dataset-list.routable";
import {IngestSourceDatasetRoutingModule} from "@app/features/ingest/features/source-dataset/ingest-source-dataset-routing.module";
import {IngestSourceDatasetFileState} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/ingest-source-dataset-file.state";
import {IngestSourceDatasetFileStatusHistoryState} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/status-history/ingest-source-dataset-file-status-history.state";
import {IngestSourceDatasetFileUploadState} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/upload/ingest-source-dataset-file-upload.state";
import {IngestSourceDatasetState} from "@app/features/ingest/features/source-dataset/stores/source-dataset/ingest-source-dataset.state";
import {IngestSourceDatasetStatusHistoryState} from "@app/features/ingest/features/source-dataset/stores/source-dataset/status-history/ingest-source-dataset-status-history.state";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {ModuleLoadedEnum} from "@shared/enums/module-loaded.enum";
import {SharedModule} from "@shared/shared.module";
import {SsrUtil} from "solidify-frontend";

const routables = [
  IngestSourceDatasetListRoutable,
  IngestSourceDatasetCreateRoutable,
  IngestSourceDatasetDetailEditRoutable,
  IngestSourceDatasetFileDetailRoutable,
];
const containers = [
  IngestSourceDatasetFileContainer,
  IngestSourceDatasetUploadContainer,
];
const dialogs = [
  IngestSourceDatasetFileUploadArchiveDialog,
  IngestSourceDatasetFileUploadDialog,
  IngestSourceDatasetApplyRmlDialog,
  IngestSourceDatasetLinkToResearchDataFileDialog,
];
const presentationals = [
  IngestSourceDatasetFormPresentational,
];

const states = [
  IngestSourceDatasetState,
  IngestSourceDatasetStatusHistoryState,
  IngestSourceDatasetFileState,
  IngestSourceDatasetFileUploadState,
  IngestSourceDatasetFileStatusHistoryState,
];

const services = [];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    IngestSourceDatasetRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      ...states,
    ]),
    MatFormFieldModule,
  ],
  exports: [
    ...routables,
  ],
  providers: [
    ...services,
  ],
})
export class IngestSourceDatasetModule {
  constructor() {
    if (SsrUtil.window) {
      SsrUtil.window[ModuleLoadedEnum.ingestSourceDatasetModuleLoaded] = true;
    }
  }
}

