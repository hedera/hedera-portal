/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-source-dataset-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {IngestSourceDatasetCreateRoutable} from "@app/features/ingest/features/source-dataset/components/routables/ingest-source-dataset-create/ingest-source-dataset-create.routable";
import {IngestSourceDatasetDetailEditRoutable} from "@app/features/ingest/features/source-dataset/components/routables/ingest-source-dataset-detail-edit/ingest-source-dataset-detail-edit.routable";
import {IngestSourceDatasetFileDetailRoutable} from "@app/features/ingest/features/source-dataset/components/routables/ingest-source-dataset-file-detail/ingest-source-dataset-file-detail.routable";
import {IngestSourceDatasetListRoutable} from "@app/features/ingest/features/source-dataset/components/routables/ingest-source-dataset-list/ingest-source-dataset-list.routable";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  SourceDatasetRoutesEnum,
} from "@shared/enums/routes.enum";
import {IngestRoleGuardDetailService} from "@shared/guards/ingest-role-guard-detail.service";
import {HederaRoutes} from "@shared/models/hedera-route.model";
import {
  CanDeactivateGuard,
  CombinedGuardService,
  EmptyContainer,
} from "solidify-frontend";

const routes: HederaRoutes = [
  {
    path: AppRoutesEnum.root,
    component: IngestSourceDatasetListRoutable,
    data: {},
  },
  {
    path: SourceDatasetRoutesEnum.create,
    component: IngestSourceDatasetCreateRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.create,
    },
    canDeactivate: [CanDeactivateGuard],
  },
  {
    path: AppRoutesEnum.paramId + AppRoutesEnum.separator + SourceDatasetRoutesEnum.detail,
    component: IngestSourceDatasetDetailEditRoutable,
    data: {
      //  breadcrumbMemoizedSelector: DepositState.currentTitle,
      guards: [IngestRoleGuardDetailService],
    },
    canActivate: [CombinedGuardService],
    children: [
      {
        path: SourceDatasetRoutesEnum.edit,
        component: EmptyContainer,
        data: {
          breadcrumb: LabelTranslateEnum.edit,
        },
        canDeactivate: [CanDeactivateGuard],
      },
    ],
  },
  {
    path: AppRoutesEnum.paramId + AppRoutesEnum.separator + SourceDatasetRoutesEnum.files + AppRoutesEnum.separator + AppRoutesEnum.paramIdDataFile,
    component: IngestSourceDatasetFileDetailRoutable,
    data: {
      guards: [IngestRoleGuardDetailService],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IngestSourceDatasetRoutingModule {
}
