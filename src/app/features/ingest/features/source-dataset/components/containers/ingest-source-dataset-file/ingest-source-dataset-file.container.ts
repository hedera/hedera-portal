/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-source-dataset-file.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {
  ActivatedRoute,
  NavigationExtras,
} from "@angular/router";
import {IngestEditModeEnum} from "@app/features/ingest/enums/ingest-edit-mode.enum";
import {IngestNavigationParamEnum} from "@app/features/ingest/enums/ingest-navigation-param.enum";
import {
  DatasetSourceApplyRmlDialogModeEnum,
  IngestSourceDatasetApplyRmlDialog,
  IngestSourceDatasetApplyRmlDialogData,
} from "@app/features/ingest/features/source-dataset/components/dialogs/ingest-source-dataset-apply-rml/ingest-source-dataset-apply-rml.dialog";
import {
  IngestSourceDatasetFileAction,
  ingestSourceDatasetFileActionNameSpace,
} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/ingest-source-dataset-file.action";
import {
  IngestSourceDatasetFileState,
  IngestSourceDatasetFileStateModel,
} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/ingest-source-dataset-file.state";
import {ingestSourceDatasetFileStatusHistoryNamespace} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/status-history/ingest-source-dataset-file-status-history.action";
import {IngestSourceDatasetFileStatusHistoryState} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/status-history/ingest-source-dataset-file-status-history.state";
import {IngestSourceDatasetFileUploadAction} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/upload/ingest-source-dataset-file-upload.action";
import {HederaResearchDataFileUploadWrapper} from "@app/features/ingest/models/hedera-research-data-file-upload-wrapper.model";
import {IngestState} from "@app/features/ingest/stores/ingest.state";
import {IngestProjectRmlState} from "@app/features/ingest/stores/project-rml/ingest-project-rml.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {SourceDatasetFile} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  IngestRoutesEnum,
  RoutesEnum,
  SourceDatasetRoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataFileHelper} from "@shared/helpers/data-file.helper";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {SecurityService} from "@shared/services/security.service";
import {StoreDialogService} from "@shared/services/store-dialog.service";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  map,
  take,
  tap,
} from "rxjs/operators";
import {
  AbstractResourceRoutable,
  ButtonColorEnum,
  ConfirmDialog,
  DataTableActions,
  DataTableBulkActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DataTablePresentational,
  DeleteDialog,
  DialogUtil,
  FilePreviewDialog,
  FileStatusEnum,
  FileVisualizerHelper,
  FileVisualizerService,
  isTrue,
  labelSolidifyCore,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
  SOLIDIFY_CONSTANTS,
  StatusHistory,
  StatusHistoryDialog,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-ingest-source-dataset-file-container",
  templateUrl: "./ingest-source-dataset-file.container.html",
  styleUrls: ["./ingest-source-dataset-file.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngestSourceDatasetFileContainer extends AbstractResourceRoutable<SourceDatasetFile, IngestSourceDatasetFileStateModel> implements OnInit {
  private _projectId: string;
  private _datasetId: string;
  isLoadingDataFileObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, IngestSourceDatasetFileState);
  listDataFileObs: Observable<SourceDatasetFile[]> = MemoizedUtil.list(this._store, IngestSourceDatasetFileState);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, IngestSourceDatasetFileState);
  draggingSourceDatasetFile: SourceDatasetFile | undefined = undefined;
  detailRouteToInterpolate: string;
  canEditObs: Observable<IngestEditModeEnum> = MemoizedUtil.select(this._store, IngestState, state => state.canEdit).pipe(
    distinctUntilChanged(),
    tap(canEdit => {
      if (canEdit !== IngestEditModeEnum.none) {
        this.actions = [...this.actions];
      }
    }), // Force method computeContext on datatable
  );

  // @Input()
  // mode: ModeDepositTabEnum;

  @ViewChild("dataTablePresentational")
  readonly dataTablePresentational: DataTablePresentational<SourceDatasetFile>;

  columns: DataTableColumns<SourceDatasetFile>[];

  actions: DataTableActions<SourceDatasetFile>[] = [
    {
      logo: IconNameEnum.download,
      callback: (sourceDatasetFile: SourceDatasetFile) => this._downloadDataFile(sourceDatasetFile),
      placeholder: current => LabelTranslateEnum.download,
      // displayOnCondition: current => current.status === Enums.DataFile.StatusEnum.PROCESSED || current.status === Enums.DataFile.StatusEnum.READY || current.status === Enums.DataFile.StatusEnum.VIRUS_CHECKED || current.status === Enums.DataFile.StatusEnum.IGNORED_FILE,
      isWrapped: false,
    },
    {
      logo: IconNameEnum.applyRml,
      callback: (sourceDatasetFile: SourceDatasetFile) => {
        if (sourceDatasetFile.type === Enums.SourceDataFile.TypeEnum.RDF) {
          this._transformRdf(sourceDatasetFile);
        } else {
          this._applyRml(sourceDatasetFile);
        }
      },
      placeholder: current => current.type === Enums.SourceDataFile.TypeEnum.RDF ? LabelTranslateEnum.transformRdf : LabelTranslateEnum.applyRml,
      isWrapped: false,
    },
    {
      logo: IconNameEnum.rdf,
      callback: (sourceDatasetFile: SourceDatasetFile) => this._seeGeneratedRdf(sourceDatasetFile),
      placeholder: current => LabelTranslateEnum.seeGeneratedRdf,
      displayOnCondition: current => current.status === Enums.SourceDataFile.StatusEnum.TRANSFORMED,
      isWrapped: false,
    },
    {
      logo: IconNameEnum.delete,
      callback: (sourceDatasetFile: SourceDatasetFile) => this._deleteDataFile(this._datasetId, sourceDatasetFile),
      placeholder: current => LabelTranslateEnum.delete,
      displayOnCondition: (sourceDatasetFile: SourceDatasetFile) => {
        const canEdit = MemoizedUtil.selectSnapshot(this._store, IngestState, state => state.canEdit);
        return canEdit === IngestEditModeEnum.full;
      },
    },
    {
      logo: IconNameEnum.preview,
      callback: (sourceDatasetFile: SourceDatasetFile) => this._showPreview(sourceDatasetFile),
      placeholder: current => LabelTranslateEnum.showPreview,
      displayOnCondition: (sourceDatasetFile: SourceDatasetFile) => {
        const dataFile = DataFileHelper.dataFileAdapterWithoutStatus(sourceDatasetFile);
        dataFile.status = FileStatusEnum.READY;
        return FileVisualizerHelper.canHandle(this._fileVisualizerService.listFileVisualizer, {
          dataFile: dataFile,
          fileExtension: FileVisualizerHelper.getFileExtension(sourceDatasetFile.fileName),
        });
      },
    },
    {
      logo: IconNameEnum.check,
      callback: (sourceDatasetFile: SourceDatasetFile) => this._check(sourceDatasetFile),
      placeholder: current => LabelTranslateEnum.check,
      displayOnCondition: current => current.status !== Enums.SourceDataFile.StatusEnum.TRANSFORMED,
      isWrapped: true,
    },
    {
      logo: IconNameEnum.putInError,
      callback: (sourceDatasetFile: SourceDatasetFile) => this._putInError(sourceDatasetFile),
      placeholder: current => LabelTranslateEnum.putInError,
      displayOnCondition: current => this._securityService.isRootOrAdmin(),
      isWrapped: true,
    },
  ];

  bulkActions: DataTableBulkActions<SourceDatasetFile>[] = [
    {
      icon: IconNameEnum.delete,
      callback: (list: SourceDatasetFile[]) => this._bulkDeleteDataFile(this._datasetId, list.map(s => s.resId)),
      labelToTranslate: () => LabelTranslateEnum.deleteSelection,
      displayCondition: (list: SourceDatasetFile[]) => this.canEditObs.pipe(map(canEdit => canEdit === IngestEditModeEnum.full)),
      color: ButtonColorEnum.primary,
    },
    {
      icon: IconNameEnum.applyRml,
      callback: (list: SourceDatasetFile[]) => this._bulkApplyRml(this._datasetId, list.map(s => s.resId)),
      labelToTranslate: () => LabelTranslateEnum.applyRmlOnSelection,
      displayCondition: (list: SourceDatasetFile[]) => this.canEditObs.pipe(map(canEdit => canEdit === IngestEditModeEnum.full)),
      color: ButtonColorEnum.primary,
    },
    {
      icon: IconNameEnum.applyRml,
      callback: (list: SourceDatasetFile[]) => this._bulkTransformRdf(this._datasetId, list.map(s => s.resId)),
      labelToTranslate: () => LabelTranslateEnum.transformRdfOnSelection,
      displayCondition: (list: SourceDatasetFile[]) => this.canEditObs.pipe(map(canEdit => canEdit === IngestEditModeEnum.full)),
      color: ButtonColorEnum.primary,
    },
    {
      icon: IconNameEnum.putInError,
      callback: (list: SourceDatasetFile[]) => this._bulkPutInError(this._datasetId, list.map(s => s.resId)),
      labelToTranslate: () => LabelTranslateEnum.putInErrorSelection,
      displayCondition: (list: SourceDatasetFile[]) => this._securityService.isRootOrAdmin(),
      color: ButtonColorEnum.primary,
    },
  ];

  columnsToSkippedFilter: keyof SourceDatasetFile[] | string[] = [
    "status",
  ];

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get dataTestEnum(): typeof DataTestEnum {
    return DataTestEnum;
  }

  get editModeEnum(): typeof IngestEditModeEnum {
    return IngestEditModeEnum;
  }

  readonly KEY_PARAM_NAME: keyof SourceDatasetFile & string = undefined;
  isInErrorStatusFilter: boolean = false;

  // sharedResearchObjectTypeSort: Sort<ResearchObjectType> = {
  //   field: "name",
  //   order: OrderEnum.ascending,
  // };
  // sharedResearchObjectTypeActionNameSpace: ResourceNameSpace = sharedResearchObjectTypeActionNameSpace;
  // sharedResearchObjectTypeState: typeof SharedResearchObjectTypeState = SharedResearchObjectTypeState;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _securityService: SecurityService,
              private readonly _fileVisualizerService: FileVisualizerService,
              protected readonly _storeDialogService: StoreDialogService) {
    super(StateEnum.ingest_sourceDataset, _injector, StateEnum.ingest);

    this.columns = [
      {
        field: "fileName",
        header: LabelTranslateEnum.fileName,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.ascending,
        isSortable: true,
        isFilterable: true,
      },
      {
        field: "version",
        header: LabelTranslateEnum.version,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        sortableField: "version",
        filterableField: "version",
        isSortable: true,
        isFilterable: true,
        translate: true,
        width: "60px",
        alignment: "center",
      },
      {
        field: "type",
        header: LabelTranslateEnum.type,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        sortableField: "type",
        filterableField: "type",
        isSortable: true,
        isFilterable: true,
        translate: true,
        width: "60px",
        filterEnum: Enums.SourceDataFile.TypeEnumTranslate,
        alignment: "center",
      },
      {
        field: "fileSize",
        header: LabelTranslateEnum.size,
        type: DataTableFieldTypeEnum.size,
        order: OrderEnum.none,
        sortableField: "fileSize",
        filterableField: "fileSize",
        isSortable: true,
        isFilterable: false,
        translate: true,
        alignment: "right",
        width: "100px",
      },
      {
        field: "status",
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        sortableField: "status",
        isSortable: true,
        isFilterable: true,
        translate: true,
        filterEnum: Enums.SourceDataFile.StatusEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.status),
        alignment: "center",
        maxWidth: "180px",
        width: "180px",
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: true,
        width: "40px",
      },
    ];
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._projectId = MemoizedUtil.selectSnapshot(this._store, IngestState, state => state.projectId);
    this._datasetId = this._route.snapshot.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
    this._computeDetailRouteToInterpolate();
  }

  onQueryParametersEvent(queryParameters: QueryParameters, withRefresh: boolean = true): void {
    this._store.dispatch(new IngestSourceDatasetFileAction.ChangeQueryParameters(this._datasetId, queryParameters, true, withRefresh));
  }

  private _computeDetailRouteToInterpolate(): void {
    this.detailRouteToInterpolate = `${AppRoutesEnum.ingest}/${this._projectId}/${IngestRoutesEnum.sourceDataset}/${this._datasetId}/${SourceDatasetRoutesEnum.files}/{${SOLIDIFY_CONSTANTS.RES_ID}}`;
  }

  private _bulkDeleteDataFile(parentId: string, listId: string[]): Observable<boolean> {
    return DialogUtil.open(this._dialog, DeleteDialog, {
        name: undefined,
        resourceNameSpace: ingestSourceDatasetFileActionNameSpace,
        message: LabelTranslateEnum.areYouSureYouWantToDeleteTheSelectedFiles,
      }, {
        minWidth: "500px",
      },
      isConfirmed => {
        if (isTrue(isConfirmed)) {
          this._bulkDeleteDataFileWithoutConfirmation(parentId, listId);
        }
      }).pipe(
      map(result => isTrue(result)),
    );
  }

  private _bulkApplyRml(parentId: string, listId: string[]): Observable<any> {
    return DialogUtil.open(this._dialog, IngestSourceDatasetApplyRmlDialog, {
      datasetId: parentId,
      listDataFileId: listId,
      listProjectRmlObs: MemoizedUtil.selected(this._store, IngestProjectRmlState),
      mode: DatasetSourceApplyRmlDialogModeEnum.multi,
    } as IngestSourceDatasetApplyRmlDialogData, undefined, () => {
    });
  }

  private _bulkTransformRdf(parentId: string, listId: string[]): Observable<any> {
    return StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new IngestSourceDatasetFileAction.TransformRdfAll(parentId, listId),
      IngestSourceDatasetFileAction.TransformRdfAllSuccess,
      () => {
      });
  }

  private _bulkPutInError(parentId: string, listId: string[]): Observable<any> {
    return StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new IngestSourceDatasetFileAction.PutInErrorAll(parentId, listId),
      IngestSourceDatasetFileAction.PutInErrorAllSuccess,
      () => {
      });
  }

  private _bulkDeleteDataFileWithoutConfirmation(parentId: string, listId: string[]): void {
    this._store.dispatch(new IngestSourceDatasetFileAction.DeleteAll(parentId, listId));
  }

  private _deleteDataFile(parentId: string, dataFile: SourceDatasetFile): void {
    const deleteData = this._storeDialogService.deleteData(StateEnum.ingest_sourceDataset_file);
    deleteData.name = dataFile.fileName;
    this.subscribe(DialogUtil.open(this._dialog, DeleteDialog, deleteData,
      {
        width: "400px",
      },
      isConfirmed => {
        if (isTrue(isConfirmed)) {
          this._store.dispatch(new IngestSourceDatasetFileAction.Delete(parentId, dataFile.resId));
        }
      }));
  }

  private _showPreview(sourceDatasetFile: SourceDatasetFile): void {
    const dataFile = DataFileHelper.dataFileAdapterWithoutStatus(sourceDatasetFile);
    dataFile.status = FileStatusEnum.READY;
    DialogUtil.open(this._dialog, FilePreviewDialog, {
        fileInput: {
          dataFile: dataFile,
        },
        fileDownloadUrl: `${ApiEnum.ingestSourceDatasets}/${this._datasetId}/${ApiResourceNameEnum.SOURCE_DATASET_FILE}/${sourceDatasetFile.resId}/${ApiActionNameEnum.DOWNLOAD}`, // TODO URL WITH PARENT
      },
      {
        width: "max-content",
        maxWidth: "90vw",
        height: "min-content",
      });
  }

  private _downloadDataFile(dataFile: SourceDatasetFile): void {
    this._store.dispatch(new IngestSourceDatasetFileAction.Download(this._datasetId, dataFile));
  }

  private _applyRml(dataFile: SourceDatasetFile): void {
    this.subscribe(DialogUtil.open(this._dialog, IngestSourceDatasetApplyRmlDialog, {
      datasetId: this._datasetId,
      dataFileId: dataFile.resId,
      listProjectRmlObs: MemoizedUtil.selected(this._store, IngestProjectRmlState),
      mode: DatasetSourceApplyRmlDialogModeEnum.single,
    } as IngestSourceDatasetApplyRmlDialogData, undefined, () => {
    }));
  }

  private _transformRdf(dataFile: SourceDatasetFile): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new IngestSourceDatasetFileAction.TransformRdf(this._datasetId, dataFile.resId),
      IngestSourceDatasetFileAction.TransformRdfSuccess,
      () => {
      },
    ));
  }

  private _check(dataFile: SourceDatasetFile): void {
    this._store.dispatch(new IngestSourceDatasetFileAction.Check(this._datasetId, dataFile.resId));
  }

  private _putInError(dataFile: SourceDatasetFile): void {
    this._store.dispatch(new IngestSourceDatasetFileAction.PutInError(this._datasetId, dataFile.resId));
  }

  private _seeGeneratedRdf(dataFile: SourceDatasetFile): void {
    const navigationExtras: NavigationExtras = {
      [IngestNavigationParamEnum.ingestSourceId]: dataFile.dataset.resId,
      [IngestNavigationParamEnum.ingestSourceFileId]: dataFile.resId,
    } as any;
    this._store.dispatch(new Navigate([RoutesEnum.ingest, this._projectId, IngestRoutesEnum.rdfDatasetFile], navigationExtras));
  }

  selectAndDelete(): void {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: labelSolidifyCore.coreCeleteDialogConfirmDeleteAction,
      messageToTranslate: LabelTranslateEnum.areYouSureYouWantToDeleteTheSelectedFiles,
      confirmButtonToTranslate: LabelTranslateEnum.yesImSure,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.warn,
    }, undefined, (isConfirmed: boolean) => {
      if (isTrue(isConfirmed)) {
        this.subscribe(this.dataTablePresentational.selectAllResult().pipe(
          take(1),
          tap(selection => {
            this._bulkDeleteDataFileWithoutConfirmation(this._datasetId, selection);
            this.dataTablePresentational.cleanSelection();
          }),
        ));
      }
    }));
  }

  upload($event: HederaResearchDataFileUploadWrapper): void {
    this._store.dispatch(new IngestSourceDatasetFileUploadAction.UploadFile(this._datasetId, $event));
  }

  dragStart($event: SourceDatasetFile): void {
    this.draggingSourceDatasetFile = $event;
  }

  dragEnd($event: SourceDatasetFile): void {
    this.draggingSourceDatasetFile = undefined;
  }

  showHistory(sourceDatasetFile: SourceDatasetFile): void {
    const isLoadingHistoryObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, IngestSourceDatasetFileStatusHistoryState);
    const queryParametersHistoryObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, IngestSourceDatasetFileStatusHistoryState, state => state.queryParameters);
    const historyObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, IngestSourceDatasetFileStatusHistoryState, state => state.history);
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: this._datasetId, // TODO WHEN MERGE WITH RESEARCH
      resourceResId: sourceDatasetFile.resId,
      name: sourceDatasetFile.fileName,
      statusHistory: historyObs,
      isLoading: isLoadingHistoryObs,
      queryParametersObs: queryParametersHistoryObs,
      state: ingestSourceDatasetFileStatusHistoryNamespace,
      statusEnums: Enums.SourceDataFile.StatusEnumTranslate,
    }, {
      width: environment.modalWidth,
    });
  }
}
