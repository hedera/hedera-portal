/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-source-dataset-upload.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  Input,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {IngestEditModeEnum} from "@app/features/ingest/enums/ingest-edit-mode.enum";
import {IngestSourceDatasetFileUploadArchiveDialog} from "@app/features/ingest/features/source-dataset/components/dialogs/ingest-source-dataset-file-upload-archive/ingest-source-dataset-file-upload-archive.dialog";
import {IngestSourceDatasetFileUploadDialog} from "@app/features/ingest/features/source-dataset/components/dialogs/ingest-source-dataset-file-upload/ingest-source-dataset-file-upload.dialog";
import {
  IngestSourceDatasetLinkToResearchDataFileDialog,
  IngestSourceDatasetLinkToResearchDataFileDialogData,
} from "@app/features/ingest/features/source-dataset/components/dialogs/ingest-source-dataset-link/ingest-source-dataset-link-to-research-data-file-dialog.component";
import {
  IngestSourceDatasetFileState,
  IngestSourceDatasetFileStateModel,
} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/ingest-source-dataset-file.state";
import {IngestSourceDatasetFileUploadAction} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/upload/ingest-source-dataset-file-upload.action";
import {IngestSourceDatasetFileUploadState} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/upload/ingest-source-dataset-file-upload.state";
import {IngestSourceDatasetState} from "@app/features/ingest/features/source-dataset/stores/source-dataset/ingest-source-dataset.state";
import {HederaResearchDataFileUploadWrapper} from "@app/features/ingest/models/hedera-research-data-file-upload-wrapper.model";
import {HederaSourceDatasetFileUploadWrapper} from "@app/features/ingest/models/hedera-source-dataset-file-upload-wrapper.model";
import {IngestState} from "@app/features/ingest/stores/ingest.state";
import {SourceDatasetFile} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {TourEnum} from "@shared/enums/tour.enum";
import {SecurityService} from "@shared/services/security.service";
import {
  combineLatest,
  Observable,
} from "rxjs";
import {map} from "rxjs/operators";
import {
  AbstractResourceRoutable,
  DialogUtil,
  isNonEmptyArray,
  MemoizedUtil,
  QueryParameters,
  UploadFileStatus,
} from "solidify-frontend";

@Component({
  selector: "hedera-ingest-source-dataset-upload-container",
  templateUrl: "./ingest-source-dataset-upload.container.html",
  styleUrls: ["./ingest-source-dataset-upload.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngestSourceDatasetUploadContainer extends AbstractResourceRoutable<SourceDatasetFile, IngestSourceDatasetFileStateModel> {
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, IngestSourceDatasetFileState);
  uploadStatusObs: Observable<UploadFileStatus<SourceDatasetFile, HederaSourceDatasetFileUploadWrapper>[]> = MemoizedUtil.select(this._store, IngestSourceDatasetFileUploadState, state => state.uploadStatus);
  canEditObs: Observable<IngestEditModeEnum> = MemoizedUtil.select(this._store, IngestState, state => state.canEdit);
  sourceDatasetIdObs: Observable<string> = MemoizedUtil.select(this._store, IngestSourceDatasetState, state => state.current?.resId);
  projectIdObs: Observable<string> = MemoizedUtil.select(this._store, IngestState, state => state.projectId);
  uploadStatusFilteredByCurrentContextObs: Observable<UploadFileStatus<SourceDatasetFile, HederaSourceDatasetFileUploadWrapper>[]>;

  private get _resId(): string {
    return MemoizedUtil.selectSnapshot(this._store, IngestSourceDatasetState, state => state.current?.resId);
  }

  private get _projectId(): string {
    return MemoizedUtil.selectSnapshot(this._store, IngestState, state => state.projectId);
  }

  private get _sourceDatasetId(): string {
    return MemoizedUtil.selectSnapshot(this._store, IngestSourceDatasetState, state => state.current?.resId);
  }

  readonly KEY_PARAM_NAME: keyof SourceDatasetFile & string = undefined;

  get editModeEnum(): typeof IngestEditModeEnum {
    return IngestEditModeEnum;
  }

  get tourEnum(): typeof TourEnum {
    return TourEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  @Input()
  editMode: IngestEditModeEnum = IngestEditModeEnum.none;

  isCurrentContextFile: (fileUploadWrapper: HederaSourceDatasetFileUploadWrapper) => boolean =
    fileUploadWrapper => fileUploadWrapper.projectId === this._projectId
      && fileUploadWrapper.sourceDatasetId === this._sourceDatasetId;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _securityService: SecurityService) {
    super(StateEnum.ingest_sourceDataset, _injector, StateEnum.ingest);

    this.uploadStatusFilteredByCurrentContextObs = combineLatest([this.uploadStatusObs, this.projectIdObs, this.sourceDatasetIdObs])
      .pipe(map(([listUploadStatus, projectId, sourceDatasetId]) => listUploadStatus.filter(u => u.fileUploadWrapper.projectId === projectId && u.fileUploadWrapper.sourceDatasetId === sourceDatasetId)));
  }

  upload($event: HederaSourceDatasetFileUploadWrapper): void {
    this._store.dispatch(new IngestSourceDatasetFileUploadAction.UploadFile(this._resId, $event));
  }

  uploadArchive($event: HederaResearchDataFileUploadWrapper): void {
    this._store.dispatch(new IngestSourceDatasetFileUploadAction.UploadFile(this._resId, $event, true));
  }

  retry($event: UploadFileStatus<SourceDatasetFile>): void {
    this._store.dispatch(new IngestSourceDatasetFileUploadAction.RetrySendFile(this._resId, $event));
  }

  cancel($event: UploadFileStatus<SourceDatasetFile>): void {
    this._store.dispatch(new IngestSourceDatasetFileUploadAction.MarkAsCancelFileSending(this._resId, $event));
  }

  resume($event: SourceDatasetFile): void {
    // this._store.dispatch(new IngestResearchDataFileAction.Resume(this._resId, $event));
  }

  refresh(): void {
    // this._store.dispatch(new IngestResearchDataFileAction.Refresh(this._resId));
  }

  download($event: SourceDatasetFile): void {
    // this._store.dispatch(new IngestResearchDataFileAction.Download(this._resId, $event));
  }

  openModalLinkToResearchDataFile(): void {
    this.subscribe(DialogUtil.open<IngestSourceDatasetLinkToResearchDataFileDialogData, any>(this._dialog, IngestSourceDatasetLinkToResearchDataFileDialog, {
      projectId: this._projectId,
      datasetId: this._resId,
    }, {
      width: "500px",
      disableClose: true,
    }));
  }

  openModalUpload(): void {
    this.subscribe(DialogUtil.open(this._dialog, IngestSourceDatasetFileUploadDialog, {
        projectId: this._projectId,
        sourceDatasetId: this._resId,
      }, {
        width: "500px",
        disableClose: true,
      },
      listFilesUploadWrapper => {
        if (isNonEmptyArray(listFilesUploadWrapper)) {
          listFilesUploadWrapper.forEach(f => this.upload(f));
        }
      }));
  }

  openModalUploadArchive(): void {
    this.subscribe(DialogUtil.open(this._dialog, IngestSourceDatasetFileUploadArchiveDialog, {
        projectId: this._projectId,
        sourceDatasetId: this._resId,
      }, {
        width: "500px",
      },
      fileUploadWrapper => {
        this.uploadArchive(fileUploadWrapper);
      }));
  }
}
