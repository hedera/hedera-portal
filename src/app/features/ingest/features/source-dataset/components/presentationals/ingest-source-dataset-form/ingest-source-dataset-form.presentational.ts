/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-source-dataset-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {IngestSourceDatasetAction} from "@app/features/ingest/features/source-dataset/stores/source-dataset/ingest-source-dataset.action";
import {IngestSourceDatasetState} from "@app/features/ingest/features/source-dataset/stores/source-dataset/ingest-source-dataset.state";
import {BaseFormDefinition} from "@app/shared/models/base-form-definition.model";
import {Enums} from "@enums";
import {SourceDataset} from "@models";
import {SharedAbstractFormPresentational} from "@shared/components/presentationals/shared-abstract-form/shared-abstract.presentational";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  DateUtil,
  EnumUtil,
  PropertyName,
  ResourceFileNameSpace,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "hedera-ingest-source-dataset-form",
  templateUrl: "./ingest-source-dataset-form.presentational.html",
  styleUrls: ["./ingest-source-dataset-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngestSourceDatasetFormPresentational extends SharedAbstractFormPresentational<SourceDataset> {
  readonly classIgnoreEditField: string = this.environment.classInputIgnored;

  public formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  datasetSourceDatasetActionNameSpace: ResourceFileNameSpace = IngestSourceDatasetAction;

  datasetSourceDatasetState: typeof IngestSourceDatasetState = IngestSourceDatasetState;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get dataTestEnum(): typeof DataTestEnum {
    return DataTestEnum;
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  get statusEnum(): typeof Enums.SourceDataset.StatusEnum {
    return Enums.SourceDataset.StatusEnum;
  }

  get sourceDatasetStatusEnumToTranslate(): typeof Enums.SourceDataset.StatusEnumTranslate {
    return Enums.SourceDataset.StatusEnumTranslate;
  }

  @Input()
  projectId: string;

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
  ) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.projectId]: [this.projectId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: ["", [SolidifyValidator]],
      [this.formDefinition.lastImportDate]: ["", [SolidifyValidator]],
    });

  }

  protected _bindFormTo(sourceDataset: SourceDataset): void {
    this.form = this._fb.group({
      [this.formDefinition.projectId]: [this.projectId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.name]: [sourceDataset.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: [sourceDataset.description, [SolidifyValidator]],
      [this.formDefinition.lastImportDate]: [DateUtil.convertDateToDateTimeString(new Date(sourceDataset.lastImportDate)), [SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(sourceDataset: SourceDataset): SourceDataset {
    return sourceDataset;
  }

  protected override _disableSpecificField(): void {
    super._disableSpecificField();
    this.form.get(this.formDefinition.lastImportDate).disable();
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() description: string;
  @PropertyName() lastImportDate: string;
  @PropertyName() projectId: string;
}
