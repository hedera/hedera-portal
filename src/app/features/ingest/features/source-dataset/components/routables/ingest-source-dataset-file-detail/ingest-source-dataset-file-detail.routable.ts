/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-source-dataset-file-detail.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {
  ActivatedRoute,
  NavigationExtras,
} from "@angular/router";
import {IngestNavigationParamEnum} from "@app/features/ingest/enums/ingest-navigation-param.enum";
import {
  DatasetSourceApplyRmlDialogModeEnum,
  IngestSourceDatasetApplyRmlDialog,
  IngestSourceDatasetApplyRmlDialogData,
} from "@app/features/ingest/features/source-dataset/components/dialogs/ingest-source-dataset-apply-rml/ingest-source-dataset-apply-rml.dialog";
import {
  IngestSourceDatasetFileAction,
  ingestSourceDatasetFileActionNameSpace,
} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/ingest-source-dataset-file.action";
import {IngestSourceDatasetFileState} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/ingest-source-dataset-file.state";
import {ingestSourceDatasetFileStatusHistoryNamespace} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/status-history/ingest-source-dataset-file-status-history.action";
import {IngestSourceDatasetFileStatusHistoryState} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/status-history/ingest-source-dataset-file-status-history.state";
import {IngestService} from "@app/features/ingest/services/ingest.service";
import {IngestState} from "@app/features/ingest/stores/ingest.state";
import {IngestProjectRmlState} from "@app/features/ingest/stores/project-rml/ingest-project-rml.state";
import {Enums} from "@enums";
import {
  DataFile,
  SourceDatasetFile,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  DataFileProperties,
  DataFilePropertiesTypeEnum,
  SharedDataFileInformationEnum,
} from "@shared/components/containers/shared-data-file-information/shared-data-file-information.container";
import {SharedAbstractDataFileDetailRoutable} from "@shared/components/routables/abstract-data-file-detail/shared-abstract-data-file-detail.routable";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  IngestRoutesEnum,
  RoutesEnum,
  SourceDatasetRoutesEnum,
} from "@shared/enums/routes.enum";
import {SecurityService} from "@shared/services/security.service";
import {StoreDialogService} from "@shared/services/store-dialog.service";
import {
  DialogUtil,
  EnumUtil,
  ExtraButtonToolbar,
  FileVisualizerService,
  isNotNullNorUndefined,
  LABEL_TRANSLATE,
  LabelTranslateInterface,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-ingest-source-dataset-file-detail-routable",
  templateUrl: "./ingest-source-dataset-file-detail.routable.html",
  styleUrls: ["./ingest-source-dataset-file-detail.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngestSourceDatasetFileDetailRoutable extends SharedAbstractDataFileDetailRoutable<SourceDatasetFile> implements OnInit {

  mode: SharedDataFileInformationEnum = SharedDataFileInformationEnum.parentId;
  _properties: DataFileProperties<SourceDatasetFile>[];

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _storeDialogService: StoreDialogService,
              protected readonly _fileVisualizerService: FileVisualizerService,
              protected readonly _changeDetector: ChangeDetectorRef,
              @Inject(LABEL_TRANSLATE) protected readonly _labelTranslateInterface: LabelTranslateInterface,
              protected readonly _notificationService: NotificationService,
              protected readonly _datasetService: IngestService,
              protected readonly _securityService: SecurityService) {
    super(_store, _route, _actions$, _dialog, _storeDialogService, _fileVisualizerService, _changeDetector, _labelTranslateInterface,
      _notificationService, _datasetService, IngestSourceDatasetFileState, ingestSourceDatasetFileActionNameSpace,
      IngestSourceDatasetFileStatusHistoryState as any, ingestSourceDatasetFileStatusHistoryNamespace, Enums.SourceDataFile.StatusEnumTranslate);
    this._datasetService.hideDepositHeader.next(true);
    this.currentObs = MemoizedUtil.current(this._store, IngestSourceDatasetFileState);
    this._projectId = MemoizedUtil.selectSnapshot(this._store, IngestState, state => state.projectId);

  }

  override backNavigate(): Navigate {
    return new Navigate([`${AppRoutesEnum.ingest}/${this._projectId}/${IngestRoutesEnum.sourceDataset}/${this._parentId}/${SourceDatasetRoutesEnum.detail}`]);
  }

  fileDownloadUrl(): string {
    return `${ApiEnum.ingestSourceDatasets}/${this._parentId}/${ApiResourceNameEnum.SOURCE_DATASET_FILE}/${this._resId}/${ApiActionNameEnum.DOWNLOAD}`;
  }

  defineProperties(): DataFileProperties<SourceDatasetFile>[] {
    return [
      {
        key: LabelTranslateEnum.status,
        value: EnumUtil.getKeyValue(Enums.SourceDataFile.StatusEnumTranslate, this.current?.status) as any,
        type: DataFilePropertiesTypeEnum.status,
      },
      {
        key: MARK_AS_TRANSLATABLE("ingest.sourceDataset.file.detail.data.location"),
        value: this.current?.fileName,
        type: DataFilePropertiesTypeEnum.text,
      },
      {
        key: MARK_AS_TRANSLATABLE("ingest.sourceDataset.file.detail.data.smartSize"),
        value: this.current?.fileSize,
        type: DataFilePropertiesTypeEnum.size,
      },
    ];
  }

  defineButtons(): ExtraButtonToolbar<SourceDatasetFile>[] {
    return [
      {
        color: "primary",
        icon: IconNameEnum.applyRml,
        callback: (sourceDatasetFile: SourceDatasetFile) => this._applyRml(sourceDatasetFile),
        labelToTranslate: current => LabelTranslateEnum.applyRml,
        displayCondition: current => isNotNullNorUndefined(current) && current.type !== Enums.SourceDataFile.TypeEnum.RDF,
        order: 60,
      },
      {
        color: "primary",
        icon: IconNameEnum.applyRml,
        callback: (sourceDatasetFile: SourceDatasetFile) => this._transformRdf(sourceDatasetFile),
        labelToTranslate: current => LabelTranslateEnum.transformRdf,
        displayCondition: current => isNotNullNorUndefined(current) && current.type === Enums.SourceDataFile.TypeEnum.RDF,
        order: 60,
      },
      {
        color: "primary",
        icon: IconNameEnum.check,
        callback: (sourceDatasetFile: SourceDatasetFile) => this._check(sourceDatasetFile),
        labelToTranslate: current => LabelTranslateEnum.check,
        displayCondition: current => current.status !== Enums.SourceDataFile.StatusEnum.TRANSFORMED,
        order: 61,
      },
      {
        color: "primary",
        icon: IconNameEnum.putInError,
        callback: (sourceDatasetFile: SourceDatasetFile) => this._putInError(sourceDatasetFile),
        labelToTranslate: current => LabelTranslateEnum.putInError,
        displayCondition: current => this._securityService.isRootOrAdmin(),
        order: 62,
      },
      {
        color: "primary",
        icon: IconNameEnum.rdf,
        callback: (sourceDatasetFile: SourceDatasetFile) => this._seeGeneratedRdf(sourceDatasetFile),
        labelToTranslate: current => LabelTranslateEnum.seeGeneratedRdf,
        displayCondition: current => current.status === Enums.SourceDataFile.StatusEnum.TRANSFORMED,
        order: 63,
      },
    ];
  }

  defineDataFile(): DataFile<SourceDatasetFile> {
    return {
      fileName: this.current.fileName,
      fileSize: this.current.fileSize,
    };
  }

  private _applyRml(dataFile: SourceDatasetFile): void {
    this.subscribe(DialogUtil.open(this._dialog, IngestSourceDatasetApplyRmlDialog, {
      datasetId: this._parentId,
      dataFileId: dataFile.resId,
      listProjectRmlObs: MemoizedUtil.selected(this._store, IngestProjectRmlState),
      mode: DatasetSourceApplyRmlDialogModeEnum.single,
    } as IngestSourceDatasetApplyRmlDialogData, undefined, () => {
    }));
  }

  private _transformRdf(dataFile: SourceDatasetFile): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new IngestSourceDatasetFileAction.TransformRdf(this._parentId, dataFile.resId),
      IngestSourceDatasetFileAction.TransformRdfSuccess,
      () => {
      },
    ));
  }

  private _check(dataFile: SourceDatasetFile): void {
    this._store.dispatch(new IngestSourceDatasetFileAction.Check(this._parentId, dataFile.resId));
  }

  private _putInError(dataFile: SourceDatasetFile): void {
    this._store.dispatch(new IngestSourceDatasetFileAction.PutInError(this._parentId, dataFile.resId));
  }

  private _seeGeneratedRdf(dataFile: SourceDatasetFile): void {
    const navigationExtras: NavigationExtras = {
      [IngestNavigationParamEnum.ingestSourceId]: dataFile.dataset.resId,
      [IngestNavigationParamEnum.ingestSourceFileId]: dataFile.resId,
    } as any;
    this._store.dispatch(new Navigate([RoutesEnum.ingest, this._projectId, IngestRoutesEnum.rdfDatasetFile], navigationExtras));
  }

}
