/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-source-dataset-create.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  ViewChild,
} from "@angular/core";
import {IngestEditModeEnum} from "@app/features/ingest/enums/ingest-edit-mode.enum";
import {IngestSourceDatasetFormPresentational} from "@app/features/ingest/features/source-dataset/components/presentationals/ingest-source-dataset-form/ingest-source-dataset-form.presentational";
import {ingestSourceDatasetActionNameSpace} from "@app/features/ingest/features/source-dataset/stores/source-dataset/ingest-source-dataset.action";
import {
  IngestSourceDatasetState,
  IngestSourceDatasetStateModel,
} from "@app/features/ingest/features/source-dataset/stores/source-dataset/ingest-source-dataset.state";
import {IngestService} from "@app/features/ingest/services/ingest.service";
import {IngestState} from "@app/features/ingest/stores/ingest.state";
import {
  Project,
  SourceDataset,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  IngestRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  AbstractCreateRoutable,
  ExtraButtonToolbar,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-ingest-source-dataset-create-routable",
  templateUrl: "./ingest-source-dataset-create.routable.html",
  styleUrls: ["./ingest-source-dataset-create.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngestSourceDatasetCreateRoutable extends AbstractCreateRoutable<SourceDataset, IngestSourceDatasetStateModel> {
  @Select(IngestSourceDatasetState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(IngestState.isReadyToBeDisplayedInCreateMode) isReadyToBeDisplayedInCreateModeObs: Observable<boolean>;
  canEditObs: Observable<IngestEditModeEnum> = MemoizedUtil.select(this._store, IngestSourceDatasetState, state => state.canEdit);

  projectIdObs: Observable<string> = MemoizedUtil.select(this._store, IngestState, (state) => state.projectId);

  get projectId(): string {
    return MemoizedUtil.selectSnapshot(this._store, IngestState, (state) => state.projectId);
  }

  @ViewChild("formPresentational")
  readonly formPresentational: IngestSourceDatasetFormPresentational;

  listExtraButtons: ExtraButtonToolbar<Project>[] = [
    {
      color: "primary",
      typeButton: "flat-button",
      icon: IconNameEnum.save,
      labelToTranslate: (current) => LabelTranslateEnum.save,
      order: 40,
      dataTest: DataTestEnum.save,
      callback: () => this.formPresentational.onSubmit(),
      displayCondition: (resource) => true,
      disableCondition: () => this.formPresentational?.form?.pristine || this.formPresentational?.form?.invalid,
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector,
              protected readonly _datasetService: IngestService) {
    super(_store, _actions$, _changeDetector, StateEnum.ingest_sourceDataset, _injector, ingestSourceDatasetActionNameSpace);
    this._datasetService.hideDepositHeader.next(false);
  }

  override backToListNavigate(): Navigate {
    return new Navigate([RoutesEnum.ingest, this.projectId, IngestRoutesEnum.sourceDataset]);
  }
}
