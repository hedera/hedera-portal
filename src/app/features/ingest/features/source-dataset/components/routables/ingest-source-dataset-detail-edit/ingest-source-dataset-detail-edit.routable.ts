/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-source-dataset-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {
  ActivatedRoute,
  NavigationExtras,
} from "@angular/router";
import {IngestEditModeEnum} from "@app/features/ingest/enums/ingest-edit-mode.enum";
import {IngestNavigationParamEnum} from "@app/features/ingest/enums/ingest-navigation-param.enum";
import {IngestSourceDatasetFormPresentational} from "@app/features/ingest/features/source-dataset/components/presentationals/ingest-source-dataset-form/ingest-source-dataset-form.presentational";
import {ingestSourceDatasetActionNameSpace} from "@app/features/ingest/features/source-dataset/stores/source-dataset/ingest-source-dataset.action";
import {
  IngestSourceDatasetState,
  IngestSourceDatasetStateModel,
} from "@app/features/ingest/features/source-dataset/stores/source-dataset/ingest-source-dataset.state";
import {IngestSourceDatasetStatusHistoryAction} from "@app/features/ingest/features/source-dataset/stores/source-dataset/status-history/ingest-source-dataset-status-history.action";
import {IngestSourceDatasetStatusHistoryState} from "@app/features/ingest/features/source-dataset/stores/source-dataset/status-history/ingest-source-dataset-status-history.state";
import {IngestService} from "@app/features/ingest/services/ingest.service";
import {IngestState} from "@app/features/ingest/stores/ingest.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {SourceDataset} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  IngestRoutesEnum,
  RoutesEnum,
  SourceDatasetRoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  AbstractDetailEditCommonRoutable,
  DialogUtil,
  ExtraButtonToolbar,
  MemoizedUtil,
  QueryParameters,
  StatusHistory,
  StatusHistoryDialog,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-ingest-source-dataset-detail-edit-routable",
  templateUrl: "./ingest-source-dataset-detail-edit.routable.html",
  styleUrls: ["./ingest-source-dataset-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngestSourceDatasetDetailEditRoutable extends AbstractDetailEditCommonRoutable<SourceDataset, IngestSourceDatasetStateModel> implements OnInit {
  @Select(IngestSourceDatasetState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(IngestSourceDatasetState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  canEditObs: Observable<IngestEditModeEnum> = MemoizedUtil.select(this._store, IngestState, state => state.canEdit);

  get editModeEnum(): typeof IngestEditModeEnum {
    return IngestEditModeEnum;
  }

  projectIdObs: Observable<string> = MemoizedUtil.select(this._store, IngestState, (state) => state.projectId);

  get projectId(): string {
    return MemoizedUtil.selectSnapshot(this._store, IngestState, state => state.projectId);
  }

  @ViewChild("formPresentational")
  readonly formPresentational: IngestSourceDatasetFormPresentational;

  sourceDataset: boolean | undefined = undefined;

  listExtraButtons: ExtraButtonToolbar<SourceDataset> [] = [
    {
      color: "primary",
      icon: IconNameEnum.rdf,
      callback: sourceDataset => this._seeGeneratedRdf(sourceDataset),
      labelToTranslate: (current) => LabelTranslateEnum.seeGeneratedRdf,
      order: 40,
    },
  ];

  readonly KEY_PARAM_NAME: keyof SourceDataset & string = "name";

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _securityService: SecurityService,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _datasetService: IngestService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.ingest_sourceDataset, _injector, ingestSourceDatasetActionNameSpace, StateEnum.ingest);
    this._datasetService.hideDepositHeader.next(false);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  override backToDetail(): void {
    super.backToDetail([RoutesEnum.ingest, this.projectId, IngestRoutesEnum.sourceDataset, this._resId, SourceDatasetRoutesEnum.detail]);
  }

  override backToListNavigate(): Navigate {
    return new Navigate([`${RoutesEnum.ingest}/${this.projectId}/${IngestRoutesEnum.sourceDataset}`]);
  }

  override edit(): void {
    if (this.isEdit) {
      return;
    }
    this._store.dispatch(new Navigate([RoutesEnum.ingest, this.projectId, IngestRoutesEnum.sourceDataset, this._resId, SourceDatasetRoutesEnum.detail, SourceDatasetRoutesEnum.edit]));
  }

  protected _getSubResourceWithParentId(id: string): void {
  }

  private _seeGeneratedRdf(sourceDataset: SourceDataset): void {
    const navigationExtras: NavigationExtras = {
      [IngestNavigationParamEnum.ingestSourceId]: sourceDataset.resId,
    } as any;
    this._store.dispatch(new Navigate([RoutesEnum.ingest, this.projectId, IngestRoutesEnum.rdfDatasetFile], navigationExtras));
  }

  showHistory(): void {
    const isLoadingHistoryObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, IngestSourceDatasetStatusHistoryState);
    const queryParametersHistoryObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, IngestSourceDatasetStatusHistoryState, state => state.queryParameters);
    const historyObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, IngestSourceDatasetStatusHistoryState, state => state.history);
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: null,
      resourceResId: this._resId,
      name: StringUtil.convertToPascalCase(this._state),
      statusHistory: historyObs,
      isLoading: isLoadingHistoryObs,
      queryParametersObs: queryParametersHistoryObs,
      state: IngestSourceDatasetStatusHistoryAction,
      statusEnums: Enums.SourceDataset.StatusEnumTranslate,
    }, {
      width: environment.modalWidth,
    });
  }
}

