/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-source-dataset-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {ingestSourceDatasetActionNameSpace} from "@app/features/ingest/features/source-dataset/stores/source-dataset/ingest-source-dataset.action";
import {
  IngestSourceDatasetState,
  IngestSourceDatasetStateModel,
} from "@app/features/ingest/features/source-dataset/stores/source-dataset/ingest-source-dataset.state";
import {ingestSourceDatasetStatusHistoryNamespace} from "@app/features/ingest/features/source-dataset/stores/source-dataset/status-history/ingest-source-dataset-status-history.action";
import {IngestSourceDatasetStatusHistoryState} from "@app/features/ingest/features/source-dataset/stores/source-dataset/status-history/ingest-source-dataset-status-history.state";
import {IngestHelper} from "@app/features/ingest/helpers/ingest.helper";
import {IngestService} from "@app/features/ingest/services/ingest.service";
import {IngestState} from "@app/features/ingest/stores/ingest.state";
import {Enums} from "@enums";
import {
  Project,
  SourceDataset,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  IngestRoutesEnum,
  RoutesEnum,
  SourceDatasetRoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  AbstractListRoutable,
  DataTableActions,
  DataTableFieldTypeEnum,
  isUndefined,
  MappingObjectUtil,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
  QueryParametersUtil,
  RouterExtensionService,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

@Component({
  selector: "hedera-ingest-source-dataset-list-routable",
  templateUrl: "../../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./ingest-source-dataset-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngestSourceDatasetListRoutable extends AbstractListRoutable<SourceDataset, IngestSourceDatasetStateModel> implements OnInit {
  readonly KEY_CREATE_BUTTON: string = undefined;
  readonly KEY_BACK_BUTTON: string | undefined = undefined;
  readonly KEY_PARAM_NAME: keyof SourceDataset & string = "name";

  override readonly columnsSkippedToClear: string[] = [IngestHelper.KEY_PROJECT];
  override skipInitialQuery: boolean = true;

  projectIdObs: Observable<string> = MemoizedUtil.select(this._store, IngestState, state => state.projectId);

  private get _projectId(): string {
    return MemoizedUtil.selectSnapshot(this._store, IngestState, state => state.projectId);
  }

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService,
              protected readonly _datasetService: IngestService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.ingest_sourceDataset, ingestSourceDatasetActionNameSpace, _injector, {
      canCreate: false,
      canGoBack: false,
      canRefresh: false,
      historyState: IngestSourceDatasetStatusHistoryState,
      historyStateAction: ingestSourceDatasetStatusHistoryNamespace,
      historyStatusEnumTranslate: Enums.SourceDataset.StatusEnumTranslate,
    }, StateEnum.ingest);
    this._datasetService.hideDepositHeader.next(false);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.subscribe(this.projectIdObs.pipe(
      distinctUntilChanged(),
      tap(projectId => {
        let queryParameters = MemoizedUtil.queryParametersSnapshot(this._store, IngestSourceDatasetState);
        queryParameters = QueryParametersUtil.resetToFirstPage(queryParameters);
        this.onQueryParametersEvent(queryParameters);
      }),
    ));
  }

  conditionDisplayDeleteButton(model: Project | undefined): boolean {
    if (isUndefined(model)) {
      return true;
    }
    return true;
  }

  conditionDisplayEditButton(model: Project | undefined): boolean {
    return this.conditionDisplayDeleteButton(model);
  }

  override goToEdit(sourceDataset: SourceDataset): void {
    this._store.dispatch(new Navigate([RoutesEnum.ingest, this._projectId, IngestRoutesEnum.sourceDataset, sourceDataset.resId, SourceDatasetRoutesEnum.detail, SourceDatasetRoutesEnum.edit]));
  }

  protected override _getDetailRouteToInterpolate(): string | undefined {
    return `${RoutesEnum.ingest}/${this._projectId}/${IngestRoutesEnum.sourceDataset}/{${SOLIDIFY_CONSTANTS.RES_ID}}/${SourceDatasetRoutesEnum.detail}`;
  }

  override onQueryParametersEvent(queryParameters: QueryParameters, withRefresh: boolean = true): void {
    queryParameters = QueryParametersUtil.clone(queryParameters);
    MappingObjectUtil.set(QueryParametersUtil.getSearchItems(queryParameters), IngestHelper.KEY_PROJECT, this._projectId);
    super.onQueryParametersEvent(queryParameters);
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        dataTest: DataTestEnum.ingestSourceDatasetListSearchName,
      },
      {
        field: "fileNumber" as any,
        header: LabelTranslateEnum.files,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: false,
      },
      {
        field: "status",
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        sortableField: "status",
        isSortable: true,
        isFilterable: true,
        translate: true,
        filterEnum: Enums.SourceDataset.StatusEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.status),
        alignment: "center",
        maxWidth: "180px",
        width: "180px",
      },
    ];
  }

  protected override _defineActions(): DataTableActions<Project>[] {
    return [];
  }

}
