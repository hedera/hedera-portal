/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-source-dataset-link-to-research-data-file-dialog.component.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {IngestSourceDatasetFileAction} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/ingest-source-dataset-file.action";
import {sharedResearchDataFileActionNameSpace} from "@app/shared/stores/research-data-file/shared-research-data-file.action";
import {Enums} from "@enums";
import {
  ResearchDataFile,
  SourceDatasetFile,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {SharedResearchDataFileState} from "@shared/stores/research-data-file/shared-research-data-file.state";
import {
  FormValidationHelper,
  MappingObject,
  OrderEnum,
  PropertyName,
  ResourceNameSpace,
  SOLIDIFY_CONSTANTS,
  Sort,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-ingest-source-dataset-link-to-research-data-file-dialog",
  templateUrl: "./ingest-source-dataset-link-to-research-data-file-dialog.component.html",
  styleUrls: ["./ingest-source-dataset-link-to-research-data-file-dialog.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngestSourceDatasetLinkToResearchDataFileDialog extends SharedAbstractDialog<IngestSourceDatasetLinkToResearchDataFileDialogData, true> implements OnInit {
  form: FormGroup;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  sharedResearchDataFileSort: Sort<ResearchDataFile> = {
    field: "fileName",
    order: OrderEnum.ascending,
  };
  sharedResearchDataFileActionNameSpace: ResourceNameSpace = sharedResearchDataFileActionNameSpace;
  sharedResearchDataFileState: typeof SharedResearchDataFileState = SharedResearchDataFileState;
  researchDataFileExtraSearchQueryParam: MappingObject<string, string> = {
    projectId: this.data.projectId,
  };
  researchDataFileSecondLineCallback: (value: ResearchDataFile) => string = value => value.relativeLocation === SOLIDIFY_CONSTANTS.FILE_SEPARATOR ? undefined : value.relativeLocation;

  researchDataFile: ResearchDataFile;

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  get typeEnumTranslate(): typeof Enums.SourceDataFile.TypeEnumTranslate {
    return Enums.SourceDataFile.TypeEnumTranslate;
  }

  constructor(protected readonly _dialogRef: MatDialogRef<IngestSourceDatasetLinkToResearchDataFileDialog>,
              protected readonly _fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public readonly data: IngestSourceDatasetLinkToResearchDataFileDialogData,
              protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.form = this._fb.group({
      [this.formDefinition.type]: [undefined, [Validators.required]],
      [this.formDefinition.version]: [undefined, [Validators.required]],
      [this.formDefinition.researchDataFileId]: [undefined, [Validators.required]],
    });
  }

  setResearchDataFile(researchDataFile: ResearchDataFile): void {
    this.researchDataFile = researchDataFile;
  }

  onSubmit(): void {
    const model: SourceDatasetFile = {
      version: this.form.get(this.formDefinition.version).value,
      type: this.form.get(this.formDefinition.type).value,
      researchDataFile: {
        resId: this.form.get(this.formDefinition.researchDataFileId).value,
      },
      fileName: this.researchDataFile.fileName,
      dataset: {
        resId: this.data.datasetId,
      } as any,
    };
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new IngestSourceDatasetFileAction.Create(this.data.datasetId, model),
      IngestSourceDatasetFileAction.CreateSuccess,
      result => {
        super.submit(true);
      }));
  }
}

export class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() type: string;
  @PropertyName() version: string;
  @PropertyName() researchDataFileId: string;
}

export interface IngestSourceDatasetLinkToResearchDataFileDialogData {
  projectId: string;
  datasetId: string;
}
