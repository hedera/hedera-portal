/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-source-dataset-file-upload.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  OnInit,
  ViewChild,
} from "@angular/core";
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {
  AbstractIngestDatasetSourceFileUploadFormComponentFormDefinition,
  AbstractIngestSourceDatasetFileUploadDialog,
  AbstractIngestSourceDatasetFileUploadDialogData,
} from "@app/features/ingest/features/source-dataset/components/dialogs/ingest-abstract-source-dataset-file-upload/abstract-ingest-source-dataset-file-upload.dialog";
import {HederaSourceDatasetFileUploadWrapper} from "@app/features/ingest/models/hedera-source-dataset-file-upload-wrapper.model";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {SystemProperty} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {
  crc32,
  md5,
  sha1,
  sha256,
} from "hash-wasm";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  AppSystemPropertyState,
  EnumUtil,
  FormValidationHelper,
  HTMLInputEvent,
  isFalse,
  isNonEmptyArray,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrEmpty,
  isNullOrUndefinedOrWhiteString,
  isTrue,
  MappingObject,
  MappingObjectUtil,
  MemoizedUtil,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "hedera-ingest-source-dataset-file-upload-dialog",
  templateUrl: "./ingest-source-dataset-file-upload.dialog.html",
  styleUrls: ["./ingest-source-dataset-file-upload.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngestSourceDatasetFileUploadDialog extends AbstractIngestSourceDatasetFileUploadDialog<IngestSourceDatasetFileUploadDialogData, HederaSourceDatasetFileUploadWrapper[]> implements OnInit {
  LIMIT_CHECKSUM_COMPUTE_IN_MEGABYTE: number = environment.fileSizeLimitForChecksumGenerationInMegabytes;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  formDefinitionChecksum: FormComponentFormDefinitionChecksum = new FormComponentFormDefinitionChecksum();
  filesToUpload: FileWrapper[] = [];
  formArray: FormArray;

  @ViewChild("fileInput")
  fileInput: ElementRef;

  readonly CHECKSUM_LENGTH_MD5: number = 32;
  readonly CHECKSUM_LENGTH_SHA1: number = 40;
  readonly CHECKSUM_LENGTH_SHA256: number = 64;
  readonly CHECKSUM_LENGTH_CRC32: number = 8;

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  get checksumUsed(): Enums.DataFile.Checksum.AlgoEnum[] {
    return MemoizedUtil.selectSnapshot(this._store, AppSystemPropertyState<SystemProperty>, state => state.current.checksums) as Enums.DataFile.Checksum.AlgoEnum[];
  }

  get isChecksumComputing(): boolean {
    return this.formArray.controls.some((control: AbstractControl) => control.get(this.formDefinitionChecksum.checksumGeneratedIsLoadingCounter).value > 0);
  }

  constructor(protected readonly _dialogRef: MatDialogRef<IngestSourceDatasetFileUploadDialog>,
              protected readonly _fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public readonly data: IngestSourceDatasetFileUploadDialogData,
              protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef) {
    super(_dialogRef, _fb, data, _store);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.formArray = this._fb.array([]);
  }

  private _generateFormGroupChecksum(file: File): FormGroup {
    const formGroup = this._fb.group({
      [this.formDefinitionChecksum.isChecksumUser]: [false, [SolidifyValidator]],
      [this.formDefinitionChecksum.isChecksumGenerated]: [false, [SolidifyValidator]],
      [this.formDefinitionChecksum.checksumGeneratedIsLoadingCounter]: [0, [SolidifyValidator]],
    });

    if (isNullOrUndefined(this.checksumUsed)) {
      return formGroup;
    }

    if (this.checksumUsed.indexOf(Enums.DataFile.Checksum.AlgoEnum.MD5) !== -1) {
      formGroup.addControl(this.formDefinitionChecksum.checksumGeneratedMd5, this._fb.control("", [SolidifyValidator]) as FormControl);
      formGroup.addControl(this.formDefinitionChecksum.checksumUserMd5, this._fb.control("", [SolidifyValidator]) as FormControl);
    }

    if (this.checksumUsed.indexOf(Enums.DataFile.Checksum.AlgoEnum.SHA1) !== -1) {
      formGroup.addControl(this.formDefinitionChecksum.checksumGeneratedSha1, this._fb.control("", [SolidifyValidator]) as FormControl);
      formGroup.addControl(this.formDefinitionChecksum.checksumUserSha1, this._fb.control("", [SolidifyValidator]) as FormControl);
    }

    if (this.checksumUsed.indexOf(Enums.DataFile.Checksum.AlgoEnum.SHA256) !== -1) {
      formGroup.addControl(this.formDefinitionChecksum.checksumGeneratedSha256, this._fb.control("", [SolidifyValidator]) as FormControl);
      formGroup.addControl(this.formDefinitionChecksum.checksumUserSha256, this._fb.control("", [SolidifyValidator]) as FormControl);
    }

    if (this.checksumUsed.indexOf(Enums.DataFile.Checksum.AlgoEnum.CRC32) !== -1) {
      formGroup.addControl(this.formDefinitionChecksum.checksumGeneratedCrc32, this._fb.control("", [SolidifyValidator]) as FormControl);
      formGroup.addControl(this.formDefinitionChecksum.checksumUserCrc32, this._fb.control("", [SolidifyValidator]) as FormControl);
    }

    formGroup.setValidators([atLeastOneUserChecksumSetValidator()]);

    this.subscribe(formGroup.get(this.formDefinitionChecksum.isChecksumGenerated).valueChanges.pipe(
      distinctUntilChanged(),
      tap(isChecksumGenerated => {
        if (isTrue(isChecksumGenerated)) {
          formGroup.get(this.formDefinitionChecksum.checksumGeneratedMd5)?.setValidators([Validators.required]);
          formGroup.get(this.formDefinitionChecksum.checksumGeneratedSha1)?.setValidators([Validators.required]);
          formGroup.get(this.formDefinitionChecksum.checksumGeneratedSha256)?.setValidators([Validators.required]);
          formGroup.get(this.formDefinitionChecksum.checksumGeneratedCrc32)?.setValidators([Validators.required]);
          formGroup.get(this.formDefinitionChecksum.checksumGeneratedIsLoadingCounter).setValidators([Validators.min(0), Validators.max(0)]);
        } else {
          formGroup.get(this.formDefinitionChecksum.checksumGeneratedMd5)?.setValidators([]);
          formGroup.get(this.formDefinitionChecksum.checksumGeneratedSha1)?.setValidators([]);
          formGroup.get(this.formDefinitionChecksum.checksumGeneratedSha256)?.setValidators([]);
          formGroup.get(this.formDefinitionChecksum.checksumGeneratedCrc32)?.setValidators([]);
          formGroup.get(this.formDefinitionChecksum.checksumGeneratedIsLoadingCounter).setValidators([]);
        }
        formGroup.updateValueAndValidity();
        formGroup.get(this.formDefinitionChecksum.checksumGeneratedMd5)?.updateValueAndValidity();
        formGroup.get(this.formDefinitionChecksum.checksumGeneratedSha1)?.updateValueAndValidity();
        formGroup.get(this.formDefinitionChecksum.checksumGeneratedSha256)?.updateValueAndValidity();
        formGroup.get(this.formDefinitionChecksum.checksumGeneratedCrc32)?.updateValueAndValidity();
        formGroup.get(this.formDefinitionChecksum.checksumGeneratedIsLoadingCounter).updateValueAndValidity();
      }),
    ));

    this.subscribe(formGroup.get(this.formDefinitionChecksum.isChecksumUser).valueChanges.pipe(
      distinctUntilChanged(),
      tap(isChecksumUser => {
        if (isTrue(isChecksumUser)) {
          formGroup.get(this.formDefinitionChecksum.checksumUserMd5)
            ?.setValidators([Validators.minLength(this.CHECKSUM_LENGTH_MD5), Validators.maxLength(this.CHECKSUM_LENGTH_MD5), matchGeneratedChecksumValidator(file, formGroup, this.formDefinitionChecksum.checksumUserMd5, this.CHECKSUM_LENGTH_MD5)]);
          formGroup.get(this.formDefinitionChecksum.checksumUserSha1)
            ?.setValidators([Validators.minLength(this.CHECKSUM_LENGTH_SHA1), Validators.maxLength(this.CHECKSUM_LENGTH_SHA1), matchGeneratedChecksumValidator(file, formGroup, this.formDefinitionChecksum.checksumUserSha1, this.CHECKSUM_LENGTH_SHA1)]);
          formGroup.get(this.formDefinitionChecksum.checksumUserSha256)
            ?.setValidators([Validators.minLength(this.CHECKSUM_LENGTH_SHA256), Validators.maxLength(this.CHECKSUM_LENGTH_SHA256), matchGeneratedChecksumValidator(file, formGroup, this.formDefinitionChecksum.checksumUserSha256, this.CHECKSUM_LENGTH_SHA256)]);
          formGroup.get(this.formDefinitionChecksum.checksumUserCrc32)
            ?.setValidators([Validators.minLength(this.CHECKSUM_LENGTH_CRC32), Validators.maxLength(this.CHECKSUM_LENGTH_CRC32), matchGeneratedChecksumValidator(file, formGroup, this.formDefinitionChecksum.checksumUserCrc32, this.CHECKSUM_LENGTH_CRC32)]);
        } else {
          formGroup.get(this.formDefinitionChecksum.checksumUserMd5)?.setValidators([]);
          formGroup.get(this.formDefinitionChecksum.checksumUserSha1)?.setValidators([]);
          formGroup.get(this.formDefinitionChecksum.checksumUserSha256)?.setValidators([]);
          formGroup.get(this.formDefinitionChecksum.checksumUserCrc32)?.setValidators([]);
        }
        formGroup.updateValueAndValidity();
        formGroup.get(this.formDefinitionChecksum.checksumUserMd5)?.updateValueAndValidity();
        formGroup.get(this.formDefinitionChecksum.checksumUserSha1)?.updateValueAndValidity();
        formGroup.get(this.formDefinitionChecksum.checksumUserSha256)?.updateValueAndValidity();
        formGroup.get(this.formDefinitionChecksum.checksumUserCrc32)?.updateValueAndValidity();
        this._changeDetector.detectChanges();
      }),
    ));

    return formGroup;
  }

  onFileChange(event: HTMLInputEvent): void {
    if (event.target.files.length > 0) {
      const files: FileList = event.target.files;
      this._addNewFileToExistingList(this._convertFileListToArray(files));
    }
    if (isNotNullNorUndefined(this.fileInput)) {
      this.fileInput.nativeElement.value = null;
    }
  }

  private _convertFileListToArray(files: FileList): File[] {
    const arrayFile: File[] = [];
    for (let i = 0; i < files.length; i++) {
      arrayFile.push(files.item(i));
    }
    return arrayFile;
  }

  private _addNewFileToExistingList(files: File[]): void {
    files = files.filter(fileToFilter => {
      const isIncluded = this._getIndexOfFile(fileToFilter) !== -1;
      return !isIncluded;
    });
    const listFileWrapper: FileWrapper[] = [];
    files.forEach(f => {
      const formGroup = this._generateFormGroupChecksum(f);
      this.formArray.push(formGroup);
      const fileWrapper = {
        file: f,
        isInError: this.isFileNameInvalid(f),
      };
      listFileWrapper.push(fileWrapper);
      this._generateChecksum(fileWrapper, formGroup);
    });
    this.filesToUpload.push(...listFileWrapper);
  }

  private _getIndexOfFile(file: File): number {
    return this.filesToUpload.findIndex(fileAlreadyToUpload => fileAlreadyToUpload.file.name === file.name && fileAlreadyToUpload.file.type === file.type && fileAlreadyToUpload.file.size === file.size && fileAlreadyToUpload.file.lastModified === file.lastModified);
  }

  onSubmit(): void {
    const base = this.form.value as HederaSourceDatasetFileUploadWrapper;
    const listFilesUploadWrapper: HederaSourceDatasetFileUploadWrapper[] = [];
    this.filesToUpload.forEach((fileWrapper, index) => {
      const formGroup = this.formArray.at(index) as FormGroup;
      const isChecksumUser: boolean = formGroup.get(this.formDefinitionChecksum.isChecksumUser)?.value;
      const isChecksumGenerated: boolean = formGroup.get(this.formDefinitionChecksum.isChecksumGenerated)?.value;
      const fileUploadWrapper = {} as HederaSourceDatasetFileUploadWrapper;
      Object.assign(fileUploadWrapper, base);
      fileUploadWrapper.file = fileWrapper.file;
      fileUploadWrapper.mimeType = fileWrapper.file.type;
      fileUploadWrapper.type = this.form.get(this.formDefinition.type)?.value;
      fileUploadWrapper.version = this.form.get(this.formDefinition.version)?.value;
      fileUploadWrapper.projectId = this.data.projectId;
      fileUploadWrapper.sourceDatasetId = this.data.sourceDatasetId;
      const checksums = {} as MappingObject<Enums.DataFile.Checksum.AlgoEnum, string>;
      const checksumsOrigin = {} as MappingObject<Enums.DataFile.Checksum.AlgoEnum, Enums.DataFile.Checksum.OriginEnum>;

      if (isChecksumGenerated) {
        this.checksumUsed?.forEach(algo => {
          if (isNotNullNorUndefined(this._setChecksumOnMappingObject(checksums, formGroup, algo, Enums.DataFile.Checksum.OriginEnum.PORTAL))) {
            MappingObjectUtil.set(checksumsOrigin, algo, Enums.DataFile.Checksum.OriginEnum.PORTAL);
          }
        });
      }

      if (isChecksumUser) {
        this.checksumUsed?.forEach(algo => {
          if (isNotNullNorUndefined(this._setChecksumOnMappingObject(checksums, formGroup, algo, Enums.DataFile.Checksum.OriginEnum.USER))) {
            MappingObjectUtil.set(checksumsOrigin, algo, Enums.DataFile.Checksum.OriginEnum.USER);
          }
        });
      }
      fileUploadWrapper.checksums = checksums;
      fileUploadWrapper.checksumsOrigin = checksumsOrigin;
      listFilesUploadWrapper.push(fileUploadWrapper);
    });
    this.submit(listFilesUploadWrapper);
  }

  private _setChecksumOnMappingObject(checksums: MappingObject<Enums.DataFile.Checksum.AlgoEnum, string>, formGroup: FormGroup,
                                      algo: Enums.DataFile.Checksum.AlgoEnum, origin: Enums.DataFile.Checksum.OriginEnum): string | undefined {
    const formDefinition = this._getChecksumFormDefinition(algo, origin);
    const value = formGroup.get(formDefinition)?.value;
    if (isNotNullNorUndefinedNorWhiteString(value)) {
      MappingObjectUtil.set(checksums, algo, value);
      return value;
    }
    return undefined;
  }

  delete(file: File, index?: number): void {
    this.filesToUpload.splice(this._getIndexOfFile(file), 1);
    this.formArray.removeAt(index);
  }

  isFileNameInvalid(file: File): boolean {
    const fileName = file.name;
    return this.REGEX_INVALID.test(fileName);
  }

  isCurrentFileNameInError(): boolean {
    return !isNullOrUndefined(this.filesToUpload.find(f => f.isInError));
  }

  private _isFileExceedLimitForComputeChecksum(file: File): boolean {
    return isFileExceedLimitForComputeChecksum(file);
  }

  private _generateChecksum(file: FileWrapper, formGroup: FormGroup): void {
    if (this._isFileExceedLimitForComputeChecksum(file.file)) {
      return;
    }
    const formControlCounter = formGroup.get(this.formDefinitionChecksum.checksumGeneratedIsLoadingCounter);
    formControlCounter.setValue(formControlCounter.value + 1);
    formGroup.get(this.formDefinitionChecksum.isChecksumGenerated).setValue(true);
    const reader = new FileReader();
    const fileByteArray = [];
    reader.readAsArrayBuffer(file.file as any);
    reader.onloadend = (evt) => {
      if (evt.target.readyState === FileReader.DONE) {
        const arrayBuffer = evt.target.result;
        const array = new Uint8Array(arrayBuffer as any);
        // eslint-disable-next-line @typescript-eslint/prefer-for-of
        for (let i = 0; i < array.length; i++) {
          fileByteArray.push(array[i]);
        }
        this._computeGeneratedChecksum(formGroup, array, Enums.DataFile.Checksum.AlgoEnum.MD5);
        this._computeGeneratedChecksum(formGroup, array, Enums.DataFile.Checksum.AlgoEnum.SHA1);
        this._computeGeneratedChecksum(formGroup, array, Enums.DataFile.Checksum.AlgoEnum.SHA256);
        this._computeGeneratedChecksum(formGroup, array, Enums.DataFile.Checksum.AlgoEnum.CRC32);
        formControlCounter.setValue(formControlCounter.value - 1);
      }
    };
  }

  private _computeGeneratedChecksum(formGroup: FormGroup, array: Uint8Array, algo: Enums.DataFile.Checksum.AlgoEnum): void {
    if (isNonEmptyArray(this.checksumUsed) && this.checksumUsed.indexOf(algo) !== -1) {
      const formControlCounter = formGroup.get(this.formDefinitionChecksum.checksumGeneratedIsLoadingCounter);
      formControlCounter.setValue(formControlCounter.value + 1);
      const checksumFunction = IngestSourceDatasetFileUploadDialog._getChecksumFunction(algo);
      checksumFunction(array).then(checksum => {
        formGroup.get(this._getChecksumFormDefinition(algo, Enums.DataFile.Checksum.OriginEnum.PORTAL))?.setValue(checksum);
        setTimeout(() => {
          this._changeDetector.detectChanges();
        }, 0);
      }).finally(() => {
        formControlCounter.setValue(formControlCounter.value - 1);
      });
    }
  }

  private _getChecksumFormDefinition(algo: Enums.DataFile.Checksum.AlgoEnum, origin: Enums.DataFile.Checksum.OriginEnum): string {
    if (origin !== Enums.DataFile.Checksum.OriginEnum.PORTAL && origin !== Enums.DataFile.Checksum.OriginEnum.USER) {
      throw new Error("Manage only portal and user checksum");
    }
    switch (algo) {
      case Enums.DataFile.Checksum.AlgoEnum.MD5:
        return origin === Enums.DataFile.Checksum.OriginEnum.PORTAL ? this.formDefinitionChecksum.checksumGeneratedMd5 : this.formDefinitionChecksum.checksumUserMd5;
      case Enums.DataFile.Checksum.AlgoEnum.SHA1:
        return origin === Enums.DataFile.Checksum.OriginEnum.PORTAL ? this.formDefinitionChecksum.checksumGeneratedSha1 : this.formDefinitionChecksum.checksumUserSha1;
      case Enums.DataFile.Checksum.AlgoEnum.SHA256:
        return origin === Enums.DataFile.Checksum.OriginEnum.PORTAL ? this.formDefinitionChecksum.checksumGeneratedSha256 : this.formDefinitionChecksum.checksumUserSha256;
      case Enums.DataFile.Checksum.AlgoEnum.CRC32:
        return origin === Enums.DataFile.Checksum.OriginEnum.PORTAL ? this.formDefinitionChecksum.checksumGeneratedCrc32 : this.formDefinitionChecksum.checksumUserCrc32;
    }
    throw new Error(algo + " is a unmanaged checksum algo");
  }

  private static _getChecksumFunction(algo: Enums.DataFile.Checksum.AlgoEnum): (data: Uint8Array) => Promise<string> {
    switch (algo) {
      case Enums.DataFile.Checksum.AlgoEnum.MD5:
        return md5;
      case Enums.DataFile.Checksum.AlgoEnum.SHA1:
        return sha1;
      case Enums.DataFile.Checksum.AlgoEnum.SHA256:
        return sha256;
      case Enums.DataFile.Checksum.AlgoEnum.CRC32:
        return crc32;
    }
    throw new Error(algo + " is a unmanaged checksum algo");
  }

  asFormGroup(abstractControl: AbstractControl): FormGroup {
    return abstractControl as FormGroup;
  }
}

// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
function matchGeneratedChecksumValidator(file: File, formGroup: FormGroup, formControlName: string, length: number): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (isFileExceedLimitForComputeChecksum(file)) {
      return null;
    }
    const currentChecksum = control.value as string;
    if (isNullOrUndefinedOrEmpty(currentChecksum) || currentChecksum.length !== length) {
      return null;
    }
    const formControlGenerate = getGeneratedChecksumFormDefinitionFromUserChecksum(formControlName);
    const checksumGenerated = formGroup.get(formControlGenerate)?.value;
    if (currentChecksum !== checksumGenerated) {
      return {
        mismatch: true,
      };
    }
    return null;
  };
}

// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
function atLeastOneUserChecksumSetValidator(): ValidatorFn {
  return (formGroup: FormGroup): ValidationErrors | null => {
    const formDefinitionChecksum = new FormComponentFormDefinitionChecksum();
    const isChecksumUser = formGroup.get(formDefinitionChecksum.isChecksumUser)?.value;
    if (isFalse(isChecksumUser)) {
      return null;
    }
    const checksumMd5 = formGroup.get(formDefinitionChecksum.checksumUserMd5)?.value;
    const checksumSha1 = formGroup.get(formDefinitionChecksum.checksumUserSha1)?.value;
    const checksumSha2 = formGroup.get(formDefinitionChecksum.checksumUserSha256)?.value;
    const checksumCrc32 = formGroup.get(formDefinitionChecksum.checksumUserCrc32)?.value;
    if (isNullOrUndefinedOrWhiteString(checksumMd5)
      && isNullOrUndefinedOrWhiteString(checksumSha1)
      && isNullOrUndefinedOrWhiteString(checksumSha2)
      && isNullOrUndefinedOrWhiteString(checksumCrc32)) {
      return {
        atLeastOneChecksumShouldBeProvided: true,
      };
    }
    return null;
  };
}

const getGeneratedChecksumFormDefinitionFromUserChecksum: (userChecksumFormDefinition: string) => string = userChecksumFormDefinition => {
  const formDefinitionChecksum = new FormComponentFormDefinitionChecksum();
  if (userChecksumFormDefinition === formDefinitionChecksum.checksumUserMd5) {
    return formDefinitionChecksum.checksumGeneratedMd5;
  }
  if (userChecksumFormDefinition === formDefinitionChecksum.checksumUserSha1) {
    return formDefinitionChecksum.checksumGeneratedSha1;
  }
  if (userChecksumFormDefinition === formDefinitionChecksum.checksumUserSha256) {
    return formDefinitionChecksum.checksumGeneratedSha256;
  }
  if (userChecksumFormDefinition === formDefinitionChecksum.checksumUserCrc32) {
    return formDefinitionChecksum.checksumGeneratedCrc32;
  }
  throw new Error("Not found corresponding form definition for generated checksum");
};

const isFileExceedLimitForComputeChecksum: (file: File) => boolean = file => (file.size / (1024 * 1024)) > environment.fileSizeLimitForChecksumGenerationInMegabytes;

class FormComponentFormDefinition extends AbstractIngestDatasetSourceFileUploadFormComponentFormDefinition {
}

class FormComponentFormDefinitionChecksum extends BaseFormDefinition {
  @PropertyName() isChecksumGenerated: string;
  @PropertyName() checksumGeneratedMd5: string;
  @PropertyName() checksumGeneratedSha1: string;
  @PropertyName() checksumGeneratedSha256: string;
  @PropertyName() checksumGeneratedCrc32: string;
  @PropertyName() isChecksumUser: string;
  @PropertyName() checksumUserMd5: string;
  @PropertyName() checksumUserSha1: string;
  @PropertyName() checksumUserSha256: string;
  @PropertyName() checksumUserCrc32: string;
  @PropertyName() checksumGeneratedIsLoadingCounter: string;
}

class FileWrapper {
  file: File;
  isInError: boolean;
}

export interface IngestSourceDatasetFileUploadDialogData extends AbstractIngestSourceDatasetFileUploadDialogData {
}
