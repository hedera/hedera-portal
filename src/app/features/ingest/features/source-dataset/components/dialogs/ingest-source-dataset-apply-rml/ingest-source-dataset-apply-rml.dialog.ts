/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-source-dataset-apply-rml.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  Type,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {IngestSourceDatasetFileAction} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/ingest-source-dataset-file.action";
import {Enums} from "@enums";
import {
  MappingParameters,
  Rml,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {Observable} from "rxjs";
import {
  filter,
  take,
  tap,
} from "rxjs/operators";
import {
  BaseAction,
  BaseSubActionSuccess,
  FormValidationHelper,
  isNonEmptyArray,
  isNullOrUndefined,
  KeyValue,
  PropertyName,
  SolidifyValidator,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-ingest-source-dataset-apply-rml-dialog",
  templateUrl: "./ingest-source-dataset-apply-rml.dialog.html",
  styleUrls: ["./ingest-source-dataset-apply-rml.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngestSourceDatasetApplyRmlDialog extends SharedAbstractDialog<IngestSourceDatasetApplyRmlDialogData, true> implements OnInit {
  form: FormGroup;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  listRdfFormat: KeyValue[] = Enums.FormatEnumTranslate;

  get datasetSourceApplyRmlDialogModeEnum(): typeof DatasetSourceApplyRmlDialogModeEnum {
    return DatasetSourceApplyRmlDialogModeEnum;
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  constructor(protected readonly _dialogRef: MatDialogRef<IngestSourceDatasetApplyRmlDialog>,
              protected readonly _fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public readonly data: IngestSourceDatasetApplyRmlDialogData,
              protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.form = this._fb.group({
      [this.formDefinition.rmlFileId]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.rdfFormat]: [Enums.FormatEnum.TURTLE, [Validators.required, SolidifyValidator]],
    });
    this.form.get(this.formDefinition.rdfFormat).disable();

    this.subscribe(this.data.listProjectRmlObs.pipe(
      filter(listRml => isNonEmptyArray(listRml)),
      take(1),
      tap(listRml => {
        this.form.get(this.formDefinition.rmlFileId).setValue(listRml[0]?.resId);
      }),
    ));
  }

  onSubmit(): void {
    let action: BaseAction = null;
    let subAction: Type<BaseSubActionSuccess<any>> = null;
    if (this.data.mode === DatasetSourceApplyRmlDialogModeEnum.single) {
      action = new IngestSourceDatasetFileAction.ApplyRml(this.data.datasetId, this.data.dataFileId, this.form.value);
      subAction = IngestSourceDatasetFileAction.ApplyRmlSuccess;
    } else if (this.data.mode === DatasetSourceApplyRmlDialogModeEnum.multi) {
      const mappingParameters: MappingParameters = this.form.value;
      mappingParameters.sourceDatasetFileIds = this.data.listDataFileId;
      action = new IngestSourceDatasetFileAction.ApplyRmlAll(this.data.datasetId, mappingParameters);
      subAction = IngestSourceDatasetFileAction.ApplyRmlAllSuccess;
    }

    if (isNullOrUndefined(action)) {
      return;
    }

    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, action, subAction,
      result => {
        super.submit(true);
      }));
  }
}

export class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() rmlFileId: string;
  @PropertyName() rdfFormat: string;
}

export interface IngestSourceDatasetApplyRmlDialogData {
  datasetId: string;
  /**
   * Only for single mode
   */
  dataFileId: string;

  /**
   * Only for multi mode
   */
  listDataFileId: string[];
  listProjectRmlObs: Observable<Rml[]>;
  mode: DatasetSourceApplyRmlDialogModeEnum;
}

export type DatasetSourceApplyRmlDialogModeEnum =
  "single"
  | "multi";
export const DatasetSourceApplyRmlDialogModeEnum = {
  single: "single" as DatasetSourceApplyRmlDialogModeEnum,
  multi: "multi" as DatasetSourceApplyRmlDialogModeEnum,
};
