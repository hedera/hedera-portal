/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-source-dataset.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {IngestEditModeEnum} from "@app/features/ingest/enums/ingest-edit-mode.enum";
import {
  defaultIngestSourceDatasetFileStateModelValue,
  IngestSourceDatasetFileState,
  IngestSourceDatasetFileStateModel,
} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/ingest-source-dataset-file.state";
import {
  IngestSourceDatasetAction,
  ingestSourceDatasetActionNameSpace,
} from "@app/features/ingest/features/source-dataset/stores/source-dataset/ingest-source-dataset.action";
import {
  IngestSourceDatasetStatusHistoryState,
  IngestSourceDatasetStatusHistoryStateModel,
} from "@app/features/ingest/features/source-dataset/stores/source-dataset/status-history/ingest-source-dataset-status-history.state";
import {IngestHelper} from "@app/features/ingest/helpers/ingest.helper";
import {IngestState} from "@app/features/ingest/stores/ingest.state";
import {environment} from "@environments/environment";
import {
  ResearchDataFile,
  SourceDataset,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  IngestRoutesEnum,
  RoutesEnum,
  SourceDatasetRoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  catchError,
  map,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  defaultResourceFileStateInitValue,
  defaultStatusHistoryInitValue,
  DispatchMethodEnum,
  DownloadService,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  OverrideDefaultAction,
  QueryParameters,
  QueryParametersUtil,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export interface IngestSourceDatasetStateModel extends ResourceFileStateModel<SourceDataset> {
  canEdit: IngestEditModeEnum;
  totalCount: number;
  [StateEnum.ingest_sourceDataset_statusHistory]: IngestSourceDatasetStatusHistoryStateModel;
  [StateEnum.ingest_sourceDataset_file]: IngestSourceDatasetFileStateModel;
}

export const defaultIngestSourceDatasetStateModelValue: () => IngestSourceDatasetStateModel = () =>
  ({
    ...defaultResourceFileStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultPageSize),
    canEdit: IngestEditModeEnum.none,
    totalCount: 0,
    [StateEnum.ingest_sourceDataset_statusHistory]: {...defaultStatusHistoryInitValue()},
    [StateEnum.ingest_sourceDataset_file]: {...defaultIngestSourceDatasetFileStateModelValue()},
  });

@Injectable()
@State<IngestSourceDatasetStateModel>({
  name: StateEnum.ingest_sourceDataset,
  defaults: {
    ...defaultIngestSourceDatasetStateModelValue(),
  },
  children: [
    IngestSourceDatasetFileState,
    IngestSourceDatasetStatusHistoryState,
  ],
})
export class IngestSourceDatasetState extends ResourceFileState<IngestSourceDatasetStateModel, SourceDataset> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService,
              private readonly _securityService: SecurityService,
  ) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: ingestSourceDatasetActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.ingestSourceDataset + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.ingestSourceDataset + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: (resId: string) => {
        const projectId = MemoizedUtil.selectSnapshot(this._store, IngestState, state => state.projectId);
        return [RoutesEnum.ingest, projectId, IngestRoutesEnum.sourceDataset];
      },
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("ingest.sourceDataset.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("ingest.sourceDataset.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("ingest.sourceDataset.notification.resource.update"),
      updateSubResourceDispatchMethod: DispatchMethodEnum.SEQUENCIAL,
    }, _downloadService, ResourceFileStateModeEnum.logo, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.ingestSourceDatasets;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static isLoading(state: IngestSourceDatasetStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: IngestSourceDatasetStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static isReadyToBeDisplayed(state: IngestSourceDatasetStateModel): boolean {
    return IngestState.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @OverrideDefaultAction()
  @Action(IngestSourceDatasetAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<IngestSourceDatasetStateModel>, action: IngestSourceDatasetAction.GetByIdSuccess): void {
    super.getByIdSuccess(ctx, action);
    if (isNotNullNorUndefined(action.model.logo)) {
      ctx.dispatch(new IngestSourceDatasetAction.GetFile(action.model.resId));
    }
  }

  @OverrideDefaultAction()
  @Action(IngestSourceDatasetAction.GetAll)
  getAll(ctx: SolidifyStateContext<IngestSourceDatasetStateModel>, action: IngestSourceDatasetAction.GetAll): Observable<CollectionTyped<SourceDataset>> {
    const result = super.getAll(ctx, action);
    const projectId = MemoizedUtil.selectSnapshot(this._store, IngestState, state => state.projectId);
    ctx.dispatch([
      new IngestSourceDatasetAction.RefreshTotal(projectId),
    ]);
    return result;
  }

  @OverrideDefaultAction()
  @Action(IngestSourceDatasetAction.CreateSuccess)
  createSuccess(ctx: SolidifyStateContext<IngestSourceDatasetStateModel>, action: IngestSourceDatasetAction.CreateSuccess): void {
    super.createSuccess(ctx, action);
    ctx.dispatch([
      new Navigate([RoutesEnum.ingest, action.model.projectId, IngestRoutesEnum.sourceDataset, action.model.resId, SourceDatasetRoutesEnum.detail]),
      new IngestSourceDatasetAction.RefreshTotal(action.model.projectId),
    ]);
  }

  @OverrideDefaultAction()
  @Action(IngestSourceDatasetAction.DeleteSuccess)
  deleteSuccess(ctx: SolidifyStateContext<IngestSourceDatasetStateModel>, action: IngestSourceDatasetAction.DeleteSuccess): void {
    super.deleteSuccess(ctx, action);
    const projectId = MemoizedUtil.selectSnapshot(this._store, IngestState, state => state.projectId);
    ctx.dispatch([
      new IngestSourceDatasetAction.RefreshTotal(projectId),
    ]);
  }

  @OverrideDefaultAction()
  @Action(IngestSourceDatasetAction.UpdateSuccess)
  updateSuccess(ctx: SolidifyStateContext<IngestSourceDatasetStateModel>, action: IngestSourceDatasetAction.UpdateSuccess): void {
    super.updateSuccess(ctx, action);
    ctx.dispatch(new Navigate([RoutesEnum.ingest, action.model.projectId, IngestRoutesEnum.sourceDataset, action.model.resId, SourceDatasetRoutesEnum.detail]));
  }

  @Action(IngestSourceDatasetAction.RefreshTotal)
  refreshTotal(ctx: SolidifyStateContext<IngestSourceDatasetStateModel>, action: IngestSourceDatasetAction.RefreshTotal): Observable<number> {
    const queryParameters = new QueryParameters(environment.minimalPageSizeToRetrievePaginationInfo);
    MappingObjectUtil.set(QueryParametersUtil.getSearchItems(queryParameters), IngestHelper.KEY_PROJECT, action.parentId);
    return this._apiService.getCollection<ResearchDataFile>(this._urlResource, queryParameters)
      .pipe(
        map((collection: CollectionTyped<ResearchDataFile>) => {
          const totalItems = collection._page.totalItems;
          ctx.dispatch(new IngestSourceDatasetAction.RefreshTotalSuccess(action, totalItems));
          return totalItems;
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new IngestSourceDatasetAction.RefreshTotalFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(IngestSourceDatasetAction.RefreshTotalSuccess)
  refreshTotalSuccess(ctx: SolidifyStateContext<IngestSourceDatasetStateModel>, action: IngestSourceDatasetAction.RefreshTotalSuccess): void {
    ctx.patchState({
      totalCount: action.total,
    });
  }
}
