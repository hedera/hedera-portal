/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-source-dataset.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  SourceDataset,
} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  FileUploadWrapper,
  ResourceAction,
  ResourceFileAction,
  ResourceFileNameSpace,
  Result,
  SolidifyHttpErrorResponseModel,
  TypeDefaultAction,
} from "solidify-frontend";
import {IngestEditModeEnum} from "@app/features/ingest/enums/ingest-edit-mode.enum";

const state = StateEnum.ingest_sourceDataset;

export namespace IngestSourceDatasetAction {
  @TypeDefaultAction(state)
  export class LoadResource extends ResourceAction.LoadResource {
  }

  @TypeDefaultAction(state)
  export class LoadResourceSuccess extends ResourceAction.LoadResourceSuccess {
  }

  @TypeDefaultAction(state)
  export class LoadResourceFail extends ResourceAction.LoadResourceFail {
  }

  @TypeDefaultAction(state)
  export class ChangeQueryParameters extends ResourceAction.ChangeQueryParameters {
  }

  @TypeDefaultAction(state)
  export class GetAll extends ResourceAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends ResourceAction.GetAllSuccess<SourceDataset> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends ResourceAction.GetAllFail<SourceDataset> {
  }

  @TypeDefaultAction(state)
  export class GetByListId extends ResourceAction.GetByListId {
  }

  @TypeDefaultAction(state)
  export class GetByListIdSuccess extends ResourceAction.GetByListIdSuccess {
  }

  @TypeDefaultAction(state)
  export class GetByListIdFail extends ResourceAction.GetByListIdFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends ResourceAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends ResourceAction.GetByIdSuccess<SourceDataset> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends ResourceAction.GetByIdFail<SourceDataset> {
  }

  @TypeDefaultAction(state)
  export class Create extends ResourceAction.Create<SourceDataset> {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends ResourceAction.CreateSuccess<SourceDataset> {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends ResourceAction.CreateFail<SourceDataset> {
  }

  @TypeDefaultAction(state)
  export class Update extends ResourceAction.Update<SourceDataset> {
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends ResourceAction.UpdateSuccess<SourceDataset> {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends ResourceAction.UpdateFail<SourceDataset> {
  }

  @TypeDefaultAction(state)
  export class Delete extends ResourceAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends ResourceAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends ResourceAction.DeleteFail {
  }

  @TypeDefaultAction(state)
  export class DeleteList extends ResourceAction.DeleteList {
  }

  @TypeDefaultAction(state)
  export class DeleteListSuccess extends ResourceAction.DeleteListSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteListFail extends ResourceAction.DeleteListFail {
  }

  @TypeDefaultAction(state)
  export class AddInList extends ResourceAction.AddInList<SourceDataset> {
  }

  @TypeDefaultAction(state)
  export class AddInListById extends ResourceAction.AddInListById {
  }

  @TypeDefaultAction(state)
  export class AddInListByIdSuccess extends ResourceAction.AddInListByIdSuccess<SourceDataset> {
  }

  @TypeDefaultAction(state)
  export class AddInListByIdFail extends ResourceAction.AddInListByIdFail<SourceDataset> {
  }

  @TypeDefaultAction(state)
  export class RemoveInListById extends ResourceAction.RemoveInListById {
  }

  @TypeDefaultAction(state)
  export class RemoveInListByListId extends ResourceAction.RemoveInListByListId {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkList extends ResourceAction.LoadNextChunkList {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkListSuccess extends ResourceAction.LoadNextChunkListSuccess<SourceDataset> {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkListFail extends ResourceAction.LoadNextChunkListFail {
  }

  @TypeDefaultAction(state)
  export class Clean extends ResourceAction.Clean {
  }

  @TypeDefaultAction(state)
  export class GetFile extends ResourceFileAction.GetFile {
  }

  @TypeDefaultAction(state)
  export class GetFileSuccess extends ResourceFileAction.GetFileSuccess {
  }

  @TypeDefaultAction(state)
  export class GetFileFail extends ResourceFileAction.GetFileFail {
  }

  @TypeDefaultAction(state)
  export class UploadFile extends ResourceFileAction.UploadFile {
    constructor(public resId: string, public fileUploadWrapper: FileUploadWrapper) {
      super(resId, fileUploadWrapper);
    }
  }

  @TypeDefaultAction(state)
  export class UploadFileSuccess extends ResourceFileAction.UploadFileSuccess {
  }

  @TypeDefaultAction(state)
  export class UploadFileFail extends ResourceFileAction.UploadFileFail {
  }

  @TypeDefaultAction(state)
  export class GetFileByResId extends ResourceFileAction.GetFileByResId {
  }

  @TypeDefaultAction(state)
  export class GetFileByResIdSuccess extends ResourceFileAction.GetFileByResIdSuccess {
  }

  @TypeDefaultAction(state)
  export class GetFileByResIdFail extends ResourceFileAction.GetFileByResIdFail {
  }

  @TypeDefaultAction(state)
  export class DeleteFile extends ResourceFileAction.DeleteFile {
  }

  @TypeDefaultAction(state)
  export class DeleteFileSuccess extends ResourceFileAction.DeleteFileSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFileFail extends ResourceFileAction.DeleteFileFail {
  }

  export class Download {
    static readonly type: string = `[${state}] Download`;

    constructor(public id: string, public fullFolderName?: string) {
    }
  }


  export class Submit extends BaseAction {
    static readonly type: string = `[${state}] Submit`;

    constructor(public sourceDataset: SourceDataset) {
      super();
    }
  }

  export class SubmitSuccess extends BaseSubActionSuccess<Submit> {
    static readonly type: string = `[${state}] Submit Success`;

    constructor(public parentAction: Submit, public result: Result) {
      super(parentAction);
    }
  }

  export class SubmitFail extends BaseSubActionFail<Submit> {
    static readonly type: string = `[${state}] Submit Fail`;

    constructor(public parentAction: Submit, public error?: SolidifyHttpErrorResponseModel) {
      super(parentAction);
    }
  }

  export class ChangeEditProperty {
    static readonly type: string = `[${state}] Change Edit Property`;

    constructor(public editMode: IngestEditModeEnum) {
    }
  }


  export class ComputeModeTab extends BaseAction {
    static readonly type: string = `[${state}] Compute Mode Tab`;
  }

  export class RefreshTotal extends BaseAction {
    static readonly type: string = `[${state}] Refresh Total`;

    constructor(public parentId: string) {
      super();
    }
  }

  export class RefreshTotalSuccess extends BaseSubActionSuccess<RefreshTotal> {
    static readonly type: string = `[${state}] Refresh Total Success`;

    constructor(public parentAction: RefreshTotal, public total: number) {
      super(parentAction);
    }
  }

  export class RefreshTotalFail extends BaseSubActionFail<RefreshTotal> {
    static readonly type: string = `[${state}] Refresh Total Fail`;
  }
}

export const ingestSourceDatasetActionNameSpace: ResourceFileNameSpace = IngestSourceDatasetAction;
