/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-source-dataset-file.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {
  IngestSourceDatasetFileAction,
  ingestSourceDatasetFileActionNameSpace,
} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/ingest-source-dataset-file.action";
import {
  IngestSourceDatasetFileStatusHistoryState,
  IngestSourceDatasetFileStatusHistoryStateModel,
} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/status-history/ingest-source-dataset-file-status-history.state";
import {
  IngestSourceDatasetFileUploadState,
  IngestSourceDatasetFileUploadStateModel,
} from "@app/features/ingest/features/source-dataset/stores/source-dataset-file/upload/ingest-source-dataset-file-upload.state";
import {IngestHelper} from "@app/features/ingest/helpers/ingest.helper";
import {HederaSourceDatasetFileUploadWrapper} from "@app/features/ingest/models/hedera-source-dataset-file-upload-wrapper.model";
import {environment} from "@environments/environment";
import {
  MappingParameters,
  SourceDatasetFile,
} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {defaultStatusHistoryInitValue} from "@shared/stores/status-history/status-history.state";
import {Observable} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CompositionState,
  defaultResourceStateInitValue,
  defaultUploadStateInitValue,
  DownloadService,
  isNotNullNorUndefinedNorWhiteString,
  NotificationService,
  ofSolidifyActionCompleted,
  QueryParameters,
  QueryParametersUtil,
  ResourceStateModel,
  Result,
  ResultActionStatusEnum,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export interface IngestSourceDatasetFileStateModel extends ResourceStateModel<SourceDatasetFile> {
  canCreate: boolean;
  [StateEnum.ingest_sourceDataset_file_upload]: IngestSourceDatasetFileUploadStateModel;
  [StateEnum.ingest_sourceDataset_file_statusHistory]: IngestSourceDatasetFileStatusHistoryStateModel;
}

export const defaultIngestSourceDatasetFileStateModelValue: () => IngestSourceDatasetFileStateModel = () =>
  ({
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultPageSize),
    canCreate: false,
    [StateEnum.ingest_sourceDataset_file_upload]: {...defaultUploadStateInitValue<SourceDatasetFile, HederaSourceDatasetFileUploadWrapper>()},
    [StateEnum.ingest_sourceDataset_file_statusHistory]: {...defaultStatusHistoryInitValue()},
  });

@Injectable()
@State<IngestSourceDatasetFileStateModel>({
  name: StateEnum.ingest_sourceDataset_file,
  defaults: {
    ...defaultIngestSourceDatasetFileStateModelValue(),
  },
  children: [
    IngestSourceDatasetFileUploadState,
    IngestSourceDatasetFileStatusHistoryState,
  ],
})
export class IngestSourceDatasetFileState extends CompositionState<IngestSourceDatasetFileStateModel, SourceDatasetFile> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _securityService: SecurityService,
              protected readonly _downloadService: DownloadService,
              protected readonly _httpClient: HttpClient) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: ingestSourceDatasetFileActionNameSpace,
      resourceName: ApiResourceNameEnum.SOURCE_DATASET_FILE,
      // notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("ingest.sourceDatasetFile.notification.resource.create"),
      // notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("ingest.sourceDatasetFile.notification.resource.delete"),
      // notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("ingest.sourceDatasetFile.notification.resource.update"),
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.ingestSourceDatasets;
  }

  // protected override _evaluateSubResourceUrl(parentId: string): string {
  //   return this._urlResource;
  // }

  @Action(IngestSourceDatasetFileAction.Refresh)
  refresh(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.Refresh): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new IngestSourceDatasetFileAction.GetAll(action.parentId, undefined, true),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(IngestSourceDatasetFileAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(IngestSourceDatasetFileAction.GetAllFail)),
        ],
      },
      // {
      //   action: new DatasetSourceDatasetFileAction.GetListCurrentStatus(action.parentId),
      //   subActionCompletions: [
      //     this._actions$.pipe(ofSolidifyActionCompleted(DatasetSourceDatasetFileAction.GetListCurrentStatusSuccess)),
      //     this._actions$.pipe(ofSolidifyActionCompleted(DatasetSourceDatasetFileAction.GetListCurrentStatusFail)),
      //   ],
      // },
    ]).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new IngestSourceDatasetFileAction.RefreshSuccess(action));
        } else {
          ctx.dispatch(new IngestSourceDatasetFileAction.RefreshFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(IngestSourceDatasetFileAction.RefreshSuccess)
  refreshSuccess(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.RefreshSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(IngestSourceDatasetFileAction.RefreshFail)
  refreshFail(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.RefreshFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(IngestSourceDatasetFileAction.Download)
  download(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.Download): void {
    const url = `${this._urlResource}/${action.parentId}/${ApiResourceNameEnum.SOURCE_DATASET_FILE}/${action.dataFile.resId}/${ApiActionNameEnum.DOWNLOAD}`;
    const fileName = action.dataFile.fileName;
    const mimetype = undefined; // action.dataFile.mimeType;
    if (environment.fileExtensionToCompleteWithRealExtensionOnDownload.findIndex(e => fileName.endsWith("." + e)) >= 0) {
      // Allow to download dua and thumbnail with good extension
      this.subscribe(this._downloadService.downloadInMemory(url, fileName, true, mimetype));
    } else {
      this._downloadService.download(url, fileName, action.dataFile.fileSize, false, mimetype);
    }
  }

  @Action(IngestSourceDatasetFileAction.ApplyRml)
  applyRml(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.ApplyRml): Observable<any> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<MappingParameters, Result>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${action.dataFileId}/${ApiActionNameEnum.APPLY_RML}`,
      action.rmlMappingParameters,
    ).pipe(
      tap(result => {
        if (result?.status === ResultActionStatusEnum.EXECUTED) {
          ctx.dispatch(new IngestSourceDatasetFileAction.ApplyRmlSuccess(action, result));
        } else {
          ctx.dispatch(new IngestSourceDatasetFileAction.ApplyRmlFail(action));
        }
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new IngestSourceDatasetFileAction.ApplyRmlFail(action));
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(IngestSourceDatasetFileAction.ApplyRmlSuccess)
  applyRmlSuccess(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.ApplyRmlSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(action.result.message);
    if (action.parentAction.refreshListAfter) {
      setTimeout(() => {
        ctx.dispatch(new IngestSourceDatasetFileAction.Refresh(action.parentAction.parentId));
      }, 1000);
    }
  }

  @Action(IngestSourceDatasetFileAction.ApplyRmlFail)
  applyRmlFail(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.ApplyRmlFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showWarning(isNotNullNorUndefinedNorWhiteString(action.result?.message) ? action.result.message : LabelTranslateEnum.unableToApplyRml);
  }

  @Action(IngestSourceDatasetFileAction.TransformRdf)
  transformRdf(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.TransformRdf): Observable<any> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<undefined, Result>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${action.dataFileId}/${ApiActionNameEnum.TRANSFORM_RDF}`)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new IngestSourceDatasetFileAction.TransformRdfSuccess(action, result));
          } else {
            ctx.dispatch(new IngestSourceDatasetFileAction.TransformRdfFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new IngestSourceDatasetFileAction.TransformRdfFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(IngestSourceDatasetFileAction.TransformRdfSuccess)
  transformRdfSuccess(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.TransformRdfSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(action.result.message);
    if (action.parentAction.refreshListAfter) {
      setTimeout(() => {
        ctx.dispatch(new IngestSourceDatasetFileAction.Refresh(action.parentAction.parentId));
      }, 1000);
    }
  }

  @Action(IngestSourceDatasetFileAction.TransformRdfFail)
  transformRdfFail(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.TransformRdfFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showWarning(isNotNullNorUndefinedNorWhiteString(action.result?.message) ? action.result.message : LabelTranslateEnum.unableToTransformRdf);
  }

  @Action(IngestSourceDatasetFileAction.PutInError)
  putInError(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.PutInError): Observable<any> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<undefined, Result>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${action.dataFileId}/${ApiActionNameEnum.PUT_IN_ERROR}`)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new IngestSourceDatasetFileAction.PutInErrorSuccess(action, result));
          } else {
            ctx.dispatch(new IngestSourceDatasetFileAction.PutInErrorFail(action, result));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new IngestSourceDatasetFileAction.PutInErrorFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(IngestSourceDatasetFileAction.PutInErrorSuccess)
  putInErrorSuccess(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.PutInErrorSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(action.result.message);
    if (action.parentAction.refreshListAfter) {
      setTimeout(() => {
        ctx.dispatch(new IngestSourceDatasetFileAction.Refresh(action.parentAction.parentId));
      }, 1000);
    }
  }

  @Action(IngestSourceDatasetFileAction.PutInErrorFail)
  putInErrorFail(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.PutInErrorFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showWarning(isNotNullNorUndefinedNorWhiteString(action.result?.message) ? action.result.message : LabelTranslateEnum.unableToPutInError);
  }

  @Action(IngestSourceDatasetFileAction.Check)
  check(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.Check): Observable<any> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._httpClient.post<MappingParameters>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${action.dataFileId}/${ApiActionNameEnum.CHECK}`, null)
      .pipe(
        tap(() => {
          ctx.dispatch(new IngestSourceDatasetFileAction.CheckSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new IngestSourceDatasetFileAction.CheckFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(IngestSourceDatasetFileAction.CheckSuccess)
  checkSuccess(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.CheckSuccess): void {
    this._notificationService.showSuccess(LabelTranslateEnum.checkStarted);
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    setTimeout(() => {
      ctx.dispatch(new IngestSourceDatasetFileAction.Refresh(action.parentAction.parentId));
    }, 1000);
  }

  @Action(IngestSourceDatasetFileAction.CheckFail)
  checkFail(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.CheckFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(IngestSourceDatasetFileAction.DeleteAll)
  deleteAll(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.DeleteAll): Observable<string[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.delete<string[]>(`${this._urlResource}/${action.parentId}/${this._resourceName}`, action.listResId)
      .pipe(
        tap(collection => {
          ctx.dispatch(new IngestSourceDatasetFileAction.DeleteAllSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new IngestSourceDatasetFileAction.DeleteAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(IngestSourceDatasetFileAction.DeleteAllSuccess)
  deleteAllSuccess(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.DeleteAllSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: QueryParametersUtil.resetToFirstPage(ctx.getState().queryParameters),
    });
    ctx.dispatch(new IngestSourceDatasetFileAction.Refresh(action.parentAction.parentId));
  }

  @Action(IngestSourceDatasetFileAction.DeleteAllFail)
  deleteAllFail(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.DeleteAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showWarning(LabelTranslateEnum.unableToDeleteAllSelectedDataFile);
    ctx.dispatch(new IngestSourceDatasetFileAction.Refresh(action.parentAction.parentId));
  }

  @Action(IngestSourceDatasetFileAction.TransformRdfAll)
  transformRdfAll(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.TransformRdfAll): Observable<void> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<string[], void>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${ApiActionNameEnum.TRANSFORM_RDF}`,
      action.listResId,
    ).pipe(
      tap(result => {
        ctx.dispatch(new IngestSourceDatasetFileAction.TransformRdfAllSuccess(action));
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new IngestSourceDatasetFileAction.TransformRdfAllFail(action));
        IngestHelper.displayErrorMessageOnBulkAction(this._notificationService, error, LabelTranslateEnum.unableToTransformRdfOnAllSelectedSourceDatasetFile);
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(IngestSourceDatasetFileAction.TransformRdfAllSuccess)
  transformRdfAllSuccess(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.TransformRdfAllSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: QueryParametersUtil.resetToFirstPage(ctx.getState().queryParameters),
    });
    this._notificationService.showSuccess(LabelTranslateEnum.allSelectedSourceDatasetFileTransformed);
    setTimeout(() => {
      ctx.dispatch(new IngestSourceDatasetFileAction.Refresh(action.parentAction.parentId));
    }, 1000);
  }

  @Action(IngestSourceDatasetFileAction.TransformRdfAllFail)
  transformRdfAllFail(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.TransformRdfAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new IngestSourceDatasetFileAction.Refresh(action.parentAction.parentId));
  }

  @Action(IngestSourceDatasetFileAction.PutInErrorAll)
  putInErrorAll(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.PutInErrorAll): Observable<void> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<string[], void>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${ApiActionNameEnum.PUT_IN_ERROR}`,
      action.listResId,
    ).pipe(
      tap(result => {
        ctx.dispatch(new IngestSourceDatasetFileAction.PutInErrorAllSuccess(action));
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new IngestSourceDatasetFileAction.PutInErrorAllFail(action));
        IngestHelper.displayErrorMessageOnBulkAction(this._notificationService, error, LabelTranslateEnum.unableToPutInErrorAllSelectedElements);
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(IngestSourceDatasetFileAction.PutInErrorAllSuccess)
  putInErrorAllSuccess(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.PutInErrorAllSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: QueryParametersUtil.resetToFirstPage(ctx.getState().queryParameters),
    });
    this._notificationService.showSuccess(LabelTranslateEnum.allSelectedElementsPutInError);
    setTimeout(() => {
      ctx.dispatch(new IngestSourceDatasetFileAction.Refresh(action.parentAction.parentId));
    }, 1000);
  }

  @Action(IngestSourceDatasetFileAction.PutInErrorAllFail)
  putInErrorAllFail(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.PutInErrorAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new IngestSourceDatasetFileAction.Refresh(action.parentAction.parentId));
  }

  @Action(IngestSourceDatasetFileAction.ApplyRmlAll)
  applyRmlAll(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.ApplyRmlAll): Observable<void> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.post<MappingParameters, void>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${ApiActionNameEnum.APPLY_RML}`,
      action.rmlMappingParameters,
    ).pipe(
      tap(result => {
        ctx.dispatch(new IngestSourceDatasetFileAction.ApplyRmlAllSuccess(action));
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new IngestSourceDatasetFileAction.ApplyRmlAllFail(action));
        IngestHelper.displayErrorMessageOnBulkAction(this._notificationService, error, LabelTranslateEnum.unableToApplyRmlOnAllSelectedSourceDatasetFile);
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(IngestSourceDatasetFileAction.ApplyRmlAllSuccess)
  applyRmlAllSuccess(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.ApplyRmlAllSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: QueryParametersUtil.resetToFirstPage(ctx.getState().queryParameters),
    });
    this._notificationService.showSuccess(LabelTranslateEnum.rmlAppliedOnAllSelectedSourceDatasetFile);
    setTimeout(() => {
      ctx.dispatch(new IngestSourceDatasetFileAction.Refresh(action.parentAction.parentId));
    }, 1000);
  }

  @Action(IngestSourceDatasetFileAction.ApplyRmlAllFail)
  applyRmlAllFail(ctx: SolidifyStateContext<IngestSourceDatasetFileStateModel>, action: IngestSourceDatasetFileAction.ApplyRmlAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new IngestSourceDatasetFileAction.Refresh(action.parentAction.parentId));
  }
}
