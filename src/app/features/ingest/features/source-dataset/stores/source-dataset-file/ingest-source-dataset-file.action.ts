/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-source-dataset-file.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  MappingParameters,
  SourceDatasetFile,
} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  CompositionAction,
  CompositionNameSpace,
  Result,
  TypeDefaultAction,
} from "solidify-frontend";

const state = StateEnum.ingest_sourceDataset_file;

export namespace IngestSourceDatasetFileAction {
  @TypeDefaultAction(state)
  export class ChangeQueryParameters extends CompositionAction.ChangeQueryParameters {
  }

  @TypeDefaultAction(state)
  export class GetAll extends CompositionAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends CompositionAction.GetAllSuccess<SourceDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends CompositionAction.GetAllFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends CompositionAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends CompositionAction.GetByIdSuccess<SourceDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends CompositionAction.GetByIdFail {
  }

  @TypeDefaultAction(state)
  export class Update extends CompositionAction.Update<SourceDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends CompositionAction.UpdateSuccess<SourceDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends CompositionAction.UpdateFail<SourceDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class Create extends CompositionAction.Create<SourceDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends CompositionAction.CreateSuccess<SourceDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends CompositionAction.CreateFail<SourceDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class Delete extends CompositionAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends CompositionAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends CompositionAction.DeleteFail {
  }

  @TypeDefaultAction(state)
  export class Clean extends CompositionAction.Clean {
  }

  // export class ChangeCurrentCategory {
  //   static readonly type: string = `[${state}] Change Current Category`;
  //
  //   constructor(public newCategory: string, public newType: string, public refreshNewCategory: boolean = false) {
  //   }
  // }

  export class Refresh extends BaseAction {
    static readonly type: string = `[${state}] Refresh`;

    constructor(public parentId: string) {
      super();
    }
  }

  export class RefreshSuccess extends BaseSubActionSuccess<Refresh> {
    static readonly type: string = `[${state}] Refresh Success`;
  }

  export class RefreshFail extends BaseSubActionFail<Refresh> {
    static readonly type: string = `[${state}] Refresh Fail`;
  }

  export class Download {
    static readonly type: string = `[${state}] Download`;

    constructor(public parentId: string, public dataFile: SourceDatasetFile) {
    }
  }

  export class ApplyRml extends BaseAction {
    static readonly type: string = `[${state}] Apply Rml`;

    constructor(public parentId: string, public dataFileId: string, public rmlMappingParameters: MappingParameters, public refreshListAfter: boolean = true) {
      super();
    }
  }

  export class ApplyRmlSuccess extends BaseSubActionSuccess<ApplyRml> {
    static readonly type: string = `[${state}] Apply Rml Success`;

    constructor(public parentAction: ApplyRml, public result: Result) {
      super(parentAction);
    }
  }

  export class ApplyRmlFail extends BaseSubActionFail<ApplyRml> {
    static readonly type: string = `[${state}] Apply Rml Fail`;

    constructor(public parentAction: ApplyRml, public result?: Result) {
      super(parentAction);
    }
  }

  export class TransformRdf extends BaseAction {
    static readonly type: string = `[${state}] Transform Rdf`;

    constructor(public parentId: string, public dataFileId: string, public refreshListAfter: boolean = true) {
      super();
    }
  }

  export class TransformRdfSuccess extends BaseSubActionSuccess<TransformRdf> {
    static readonly type: string = `[${state}] Transform Rdf Success`;

    constructor(public parentAction: TransformRdf, public result: Result) {
      super(parentAction);
    }
  }

  export class TransformRdfFail extends BaseSubActionFail<TransformRdf> {
    static readonly type: string = `[${state}] Transform Rdf Fail`;

    constructor(public parentAction: TransformRdf, public result?: Result) {
      super(parentAction);
    }
  }

  export class TransformRdfAll extends BaseAction {
    static readonly type: string = `[${state}] Transform Rdf All`;

    constructor(public parentId: string, public listResId: string[]) {
      super();
    }
  }

  export class TransformRdfAllSuccess extends BaseSubActionSuccess<TransformRdfAll> {
    static readonly type: string = `[${state}] Transform Rdf All Success`;
  }

  export class TransformRdfAllFail extends BaseSubActionFail<TransformRdfAll> {
    static readonly type: string = `[${state}] Transform Rdf All Fail`;
  }

  export class PutInError extends BaseAction {
    static readonly type: string = `[${state}] Put In Error`;

    constructor(public parentId: string, public dataFileId: string, public refreshListAfter: boolean = true) {
      super();
    }
  }

  export class PutInErrorSuccess extends BaseSubActionSuccess<PutInError> {
    static readonly type: string = `[${state}] Put In Error Success`;

    constructor(public parentAction: PutInError, public result: Result) {
      super(parentAction);
    }
  }

  export class PutInErrorFail extends BaseSubActionFail<PutInError> {
    static readonly type: string = `[${state}] Put In Error Fail`;

    constructor(public parentAction: PutInError, public result?: Result) {
      super(parentAction);
    }
  }

  export class PutInErrorAll extends BaseAction {
    static readonly type: string = `[${state}] Put In Error All`;

    constructor(public parentId: string, public listResId: string[]) {
      super();
    }
  }

  export class PutInErrorAllSuccess extends BaseSubActionSuccess<PutInErrorAll> {
    static readonly type: string = `[${state}] Put In Error All Success`;
  }

  export class PutInErrorAllFail extends BaseSubActionFail<PutInErrorAll> {
    static readonly type: string = `[${state}] Put In Error All Fail`;
  }

  export class Check extends BaseAction {
    static readonly type: string = `[${state}] Check`;

    constructor(public parentId: string, public dataFileId: string) {
      super();
    }
  }

  export class CheckSuccess extends BaseSubActionSuccess<Check> {
    static readonly type: string = `[${state}] Check Success`;
  }

  export class CheckFail extends BaseSubActionFail<Check> {
    static readonly type: string = `[${state}] Check Fail`;
  }

  export class DeleteAll extends BaseAction {
    static readonly type: string = `[${state}] Delete All`;

    constructor(public parentId: string, public listResId: string[]) {
      super();
    }
  }

  export class DeleteAllSuccess extends BaseSubActionSuccess<DeleteAll> {
    static readonly type: string = `[${state}] Delete All Success`;
  }

  export class DeleteAllFail extends BaseSubActionFail<DeleteAll> {
    static readonly type: string = `[${state}] Delete All Fail`;
  }

  export class ApplyRmlAll extends BaseAction {
    static readonly type: string = `[${state}] Apply Rml All`;

    constructor(public parentId: string, public rmlMappingParameters: MappingParameters) {
      super();
    }
  }

  export class ApplyRmlAllSuccess extends BaseSubActionSuccess<ApplyRmlAll> {
    static readonly type: string = `[${state}] Apply Rml All Success`;
  }

  export class ApplyRmlAllFail extends BaseSubActionFail<ApplyRmlAll> {
    static readonly type: string = `[${state}] Apply Rml All Fail`;
  }

  // export class GetListCurrentStatus extends BaseAction {
  //   static readonly type: string = `[${state}] Get List Current Status`;
  //
  //   constructor(public parentId: string) {
  //     super();
  //   }
  // }
  //
  // export class GetListCurrentStatusSuccess extends BaseSubActionSuccess<GetListCurrentStatus> {
  //   static readonly type: string = `[${state}] Get List Current Status Folder Success`;
  //
  //   constructor(public parentAction: GetListCurrentStatus, public listCurrentStatus: MappingObject<Enums.DataFile.StatusEnum, number>) {
  //     super(parentAction);
  //   }
  // }
  //
  // export class GetListCurrentStatusFail extends BaseSubActionFail<GetListCurrentStatus> {
  //   static readonly type: string = `[${state}] Get List Current Status Folder Fail`;
  // }

}

export const ingestSourceDatasetFileActionNameSpace: CompositionNameSpace = IngestSourceDatasetFileAction;
