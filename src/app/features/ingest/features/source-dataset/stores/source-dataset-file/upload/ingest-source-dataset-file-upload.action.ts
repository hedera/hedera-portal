/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-source-dataset-file-upload.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {SourceDatasetFile} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  TypeDefaultAction,
  UploadAction,
  UploadNameSpace,
} from "solidify-frontend";

const state = StateEnum.ingest_sourceDataset_file_upload;

export namespace IngestSourceDatasetFileUploadAction {
  @TypeDefaultAction(state)
  export class UploadFile extends UploadAction.UploadFile {
  }

  @TypeDefaultAction(state)
  export class UploadFileSuccess extends UploadAction.UploadFileSuccess<SourceDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class UploadFileFail extends UploadAction.UploadFileFail<SourceDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class MarkAsCancelFileSending extends UploadAction.MarkAsCancelFileSending<SourceDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class CancelFileSending extends UploadAction.CancelFileSending<SourceDatasetFile> {
  }

  @TypeDefaultAction(state)
  export class RetrySendFile extends UploadAction.RetrySendFile<SourceDatasetFile> {
  }
}

export const ingestSourceDatasetFileUploadActionNameSpace: UploadNameSpace = IngestSourceDatasetFileUploadAction;
