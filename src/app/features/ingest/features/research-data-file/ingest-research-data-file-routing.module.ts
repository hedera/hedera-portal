/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-research-data-file-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {IngestResearchDataFileDetailRoutable} from "@app/features/ingest/features/research-data-file/components/routables/ingest-research-data-file-detail/ingest-research-data-file-detail.routable";
import {IngestResearchDataFileHomeRoutable} from "@app/features/ingest/features/research-data-file/components/routables/ingest-research-data-file-home/ingest-research-data-file-home.routable";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {AppRoutesEnum} from "@shared/enums/routes.enum";
import {ApplicationAuthorizedProjectService} from "@shared/guards/application-authorized-project.service";
import {HederaRoutes} from "@shared/models/hedera-route.model";

const routes: HederaRoutes = [
  {
    path: AppRoutesEnum.root,
    component: IngestResearchDataFileHomeRoutable,
    data: {},
  },
  {
    path: AppRoutesEnum.paramIdDataFile,
    component: IngestResearchDataFileDetailRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.detail,
      guards: [ApplicationAuthorizedProjectService],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IngestResearchDataFileRoutingModule {
}

