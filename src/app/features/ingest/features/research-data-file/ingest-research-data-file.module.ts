/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-research-data-file.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {IngestResearchDataFileListContainer} from "@app/features/ingest/features/research-data-file/components/containers/ingest-research-data-file-list/ingest-research-data-file-list.container";
import {IngestResearchDataFileUploadContainer} from "@app/features/ingest/features/research-data-file/components/containers/ingest-research-data-file-upload/ingest-research-data-file-upload.container";
import {IngestResearchDataFileMoveDialog} from "@app/features/ingest/features/research-data-file/components/dialogs/ingest-research-data-file-move/ingest-research-data-file-move.dialog";
import {IngestResearchDataFileUploadArchiveDialog} from "@app/features/ingest/features/research-data-file/components/dialogs/ingest-research-data-file-upload-archive/ingest-research-data-file-upload-archive.dialog";
import {IngestResearchDataFileUploadDialog} from "@app/features/ingest/features/research-data-file/components/dialogs/ingest-research-data-file-upload/ingest-research-data-file-upload.dialog";
import {IngestResearchDataFileDetailRoutable} from "@app/features/ingest/features/research-data-file/components/routables/ingest-research-data-file-detail/ingest-research-data-file-detail.routable";
import {IngestResearchDataFileHomeRoutable} from "@app/features/ingest/features/research-data-file/components/routables/ingest-research-data-file-home/ingest-research-data-file-home.routable";
import {IngestResearchDataFileRoutingModule} from "@app/features/ingest/features/research-data-file/ingest-research-data-file-routing.module";
import {IngestResearchDataFileState} from "@app/features/ingest/features/research-data-file/stores/research-data-file/ingest-research-data-file.state";
import {IngestResearchDataFileStatusHistoryState} from "@app/features/ingest/features/research-data-file/stores/research-data-file/status-history/ingest-research-data-file-status-history.state";
import {IngestResearchDataFileUploadState} from "@app/features/ingest/features/research-data-file/stores/research-data-file/upload/ingest-research-data-file-upload.state";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {ModuleLoadedEnum} from "@shared/enums/module-loaded.enum";
import {SharedModule} from "@shared/shared.module";
import {SsrUtil} from "solidify-frontend";

const routables = [
  IngestResearchDataFileHomeRoutable,
  IngestResearchDataFileDetailRoutable,
];
const containers = [
  IngestResearchDataFileUploadContainer,
  IngestResearchDataFileListContainer,
];
const dialogs = [
  IngestResearchDataFileUploadDialog,
  IngestResearchDataFileUploadArchiveDialog,
  IngestResearchDataFileMoveDialog,
];
const presentationals = [];

const states = [
  IngestResearchDataFileState,
  IngestResearchDataFileStatusHistoryState,
  IngestResearchDataFileUploadState,
];

const services = [];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    // IngestModule, TODO FIND A WAY TO IMPORT
    IngestResearchDataFileRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      ...states,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [
    ...services,
  ],
})
export class IngestResearchDataFileModule {
  constructor() {
    if (SsrUtil.window) {
      SsrUtil.window[ModuleLoadedEnum.ingestResearchDatasetModuleLoaded] = true;
    }
  }
}

