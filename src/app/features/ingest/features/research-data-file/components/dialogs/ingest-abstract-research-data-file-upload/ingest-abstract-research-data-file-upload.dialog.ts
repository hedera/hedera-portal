/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-abstract-research-data-file-upload.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Directive,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {Enums} from "@enums";
import {Store} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {SharedResearchObjectTypeAction} from "@shared/stores/research-object-type/shared-research-object-type.action";
import {
  FormValidationHelper,
  PropertyName,
} from "solidify-frontend";

@Directive()
export abstract class IngestAbstractResearchDataFileUploadDialog<TData extends AbstractIngestResearchDataFileUploadDialogData, UResult> extends SharedAbstractDialog<TData, UResult> implements OnInit {
  form: FormGroup;
  formDefinition: AbstractIngestResearchDataFileUploadFormComponentFormDefinition = new AbstractIngestResearchDataFileUploadFormComponentFormDefinition();

  get accessibleFromEnumTranslate(): typeof Enums.ResearchDataFile.AccessibleFromEnumTranslate {
    return Enums.ResearchDataFile.AccessibleFromEnumTranslate;
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  readonly REGEX_INVALID: RegExp | undefined = undefined;
  readonly files: string = "files";

  constructor(protected readonly _dialogRef: MatDialogRef<IngestAbstractResearchDataFileUploadDialog<TData, UResult>>,
              protected readonly _fb: FormBuilder,
              protected readonly _data: TData,
              protected readonly _store: Store,
  ) {
    super(_dialogRef, _data);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._store.dispatch(new SharedResearchObjectTypeAction.GetAll());
    this.form = this._fb.group({
      [this.formDefinition.relativeLocation]: ["/", [Validators.required]],
      [this.formDefinition.accessibleFrom]: [undefined, [Validators.required]],
    });
    this.form.get(this.formDefinition.accessibleFrom).setValue(Enums.ResearchDataFile.AccessibleFromEnum.IIIF);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  abstract onFileChange(event: Event & any): void;

  getDate(lastModified: number): Date {
    return new Date(lastModified);
  }

  abstract onSubmit(): void;

  abstract delete(file: File, index?: number): void;
}

export class AbstractIngestResearchDataFileUploadFormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() accessibleFrom: string;
  @PropertyName() relativeLocation: string;
}

export interface AbstractIngestResearchDataFileUploadDialogData {
  projectId: string;
}
