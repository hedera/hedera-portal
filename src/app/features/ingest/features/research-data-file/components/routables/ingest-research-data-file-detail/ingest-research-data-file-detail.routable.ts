/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-research-data-file-detail.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {IngestResearchDataFileMoveDialog} from "@app/features/ingest/features/research-data-file/components/dialogs/ingest-research-data-file-move/ingest-research-data-file-move.dialog";
import {
  IngestResearchDataFileAction,
  ingestResearchDataFileActionNameSpace,
} from "@app/features/ingest/features/research-data-file/stores/research-data-file/ingest-research-data-file.action";
import {IngestResearchDataFileState} from "@app/features/ingest/features/research-data-file/stores/research-data-file/ingest-research-data-file.state";
import {ingestResearchDataFileStatusHistoryNamespace} from "@app/features/ingest/features/research-data-file/stores/research-data-file/status-history/ingest-research-data-file-status-history.action";
import {IngestResearchDataFileStatusHistoryState} from "@app/features/ingest/features/research-data-file/stores/research-data-file/status-history/ingest-research-data-file-status-history.state";
import {IngestService} from "@app/features/ingest/services/ingest.service";
import {IngestState} from "@app/features/ingest/stores/ingest.state";
import {Enums} from "@enums";
import {
  DataFile,
  ResearchDataFile,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  DataFileProperties,
  DataFilePropertiesTypeEnum,
  SharedDataFileInformationEnum,
} from "@shared/components/containers/shared-data-file-information/shared-data-file-information.container";
import {SharedAbstractDataFileDetailRoutable} from "@shared/components/routables/abstract-data-file-detail/shared-abstract-data-file-detail.routable";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  IngestRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ResearchElementHelper} from "@shared/helpers/research-element.helper";
import {SecurityService} from "@shared/services/security.service";
import {StoreDialogService} from "@shared/services/store-dialog.service";
import {tap} from "rxjs/operators";
import {
  DeleteDialog,
  DialogUtil,
  DownloadService,
  EnumUtil,
  ExtraButtonToolbar,
  FileVisualizerService,
  isTrue,
  LABEL_TRANSLATE,
  LabelTranslateInterface,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  SsrUtil,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-ingest-research-data-file-detail-routable",
  templateUrl: "./ingest-research-data-file-detail.routable.html",
  styleUrls: ["./ingest-research-data-file-detail.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngestResearchDataFileDetailRoutable extends SharedAbstractDataFileDetailRoutable<ResearchDataFile> {
  _projectId: string;
  _properties: DataFileProperties<ResearchDataFile>[];
  mode: SharedDataFileInformationEnum = SharedDataFileInformationEnum.projectId;
  isInFullscreenMode: boolean = false;

  get highlightLanguageEnum(): typeof Enums.HighlightJS.HighlightLanguageEnum {
    return Enums.HighlightJS.HighlightLanguageEnum;
  }

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _storeDialogService: StoreDialogService,
              protected readonly _fileVisualizerService: FileVisualizerService,
              protected readonly _changeDetector: ChangeDetectorRef,
              @Inject(LABEL_TRANSLATE) protected readonly _labelTranslateInterface: LabelTranslateInterface,
              protected readonly _notificationService: NotificationService,
              protected readonly _datasetService: IngestService,
              protected readonly _securityService: SecurityService,
              protected readonly _downloadService: DownloadService) {
    super(_store, _route, _actions$, _dialog, _storeDialogService, _fileVisualizerService, _changeDetector, _labelTranslateInterface,
      _notificationService, _datasetService, IngestResearchDataFileState, ingestResearchDataFileActionNameSpace,
      IngestResearchDataFileStatusHistoryState as any, ingestResearchDataFileStatusHistoryNamespace, Enums.ResearchDataFile.StatusEnumTranslate);
    this._datasetService.hideDepositHeader.next(true);
    this.currentObs = MemoizedUtil.current(this._store, IngestResearchDataFileState);
    this._projectId = MemoizedUtil.selectSnapshot(this._store, IngestState, state => state.projectId);
  }

  defineProperties(): DataFileProperties<ResearchDataFile>[] {
    this._properties = [
      {
        key: LabelTranslateEnum.status,
        value: EnumUtil.getKeyValue(Enums.ResearchDataFile.StatusEnumTranslate, this.current?.status) as any,
        type: DataFilePropertiesTypeEnum.status,
      },
      {
        key: MARK_AS_TRANSLATABLE("ingest.researchDataFile.detail.data.fileName"),
        value: this.current?.fileName,
        type: DataFilePropertiesTypeEnum.text,
      },
      {
        key: MARK_AS_TRANSLATABLE("ingest.researchDataFile.detail.data.location"),
        value: this.current?.relativeLocation,
        callback: () => this._editLocation(),
        logo: IconNameEnum.edit,
        type: DataFilePropertiesTypeEnum.text,
      },
      {
        key: MARK_AS_TRANSLATABLE("ingest.researchDataFile.detail.data.smartSize"),
        value: +this.current?.fileSize,
        type: DataFilePropertiesTypeEnum.size,
      },
      {
        key: MARK_AS_TRANSLATABLE("ingest.researchDataFile.detail.data.mimeType"),
        value: this.current?.mimeType,
        type: DataFilePropertiesTypeEnum.text,
      },
      {
        key: MARK_AS_TRANSLATABLE("ingest.researchDataFile.detail.data.dataAccessibilityType"),
        value: this.current?.accessibleFrom,
        type: DataFilePropertiesTypeEnum.text,
      },
      {
        key: MARK_AS_TRANSLATABLE("ingest.researchDataFile.detail.data.researchObjectType"),
        value: this.current?.researchObjectType?.name,
        type: DataFilePropertiesTypeEnum.text,
      },
    ];
    return this._properties;
  }

  defineDataFile(): DataFile<ResearchDataFile> {
    return {
      mimetype: this.current.mimeType,
      fileName: this.current.fileName,
      fileSize: this.current.fileSize,
    };
  }

  defineButtons(): ExtraButtonToolbar<ResearchDataFile>[] {
    return [
      {
        labelToTranslate: current => LabelTranslateEnum.putInError,
        color: "primary",
        icon: IconNameEnum.error,
        callback: (researchDataFile: ResearchDataFile) => this._store.dispatch(new IngestResearchDataFileAction.PutInError(researchDataFile)),
        displayCondition: (researchDataFile: ResearchDataFile) => this._securityService.isRootOrAdmin(),
        order: 40,
      },
    ];
  }

  fileDownloadUrl(): string {
    return `${ApiEnum.ingestResearchDataFiles}/${this.current.resId}/${ApiActionNameEnum.DL}`;
  }

  private _editLocation(): void {
    this.subscribe(DialogUtil.open(this._dialog, IngestResearchDataFileMoveDialog, {
        projectId: this._projectId,
        researchDataFile: this.current,
      }, undefined,
      result => {
        if (isTrue(result)) {
          this._getOrRefreshResearchDataFile();
        }
      }));
  }

  get researchDataFileMetadata(): string[] {
    return MappingObjectUtil.keys(this.current?.metadata);
  }

  getMetadataValue(property: string): string {
    return String(MappingObjectUtil.get(this.current?.metadata, property));
  }

  enterFullScreen(isInFullscreenMode: boolean): void {
    this.isInFullscreenMode = isInFullscreenMode;
  }

  override delete(): void {
    const deleteData = this._storeDialogService.deleteData(StateEnum.ingest_researchDataFile);
    deleteData.name = this.data.dataFile.fileName;
    this.subscribe(DialogUtil.open(this._dialog, DeleteDialog, deleteData,
      {
        width: "400px",
      },
      isConfirmed => {
        if (isTrue(isConfirmed)) {
          const currentFolder = MemoizedUtil.selectSnapshot(this._store, IngestResearchDataFileState, state => state.currentFolder);
          this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
            new IngestResearchDataFileAction.Delete(this._projectId, this._resId, currentFolder),
            this._stateAction.DeleteSuccess,
            resultAction => {
              this._store.dispatch(new Navigate([RoutesEnum.ingest, this._projectId, IngestRoutesEnum.researchDataFile]));
            }));

        }
      }));
  }

  openExternal(url: string): void {
    if (url.startsWith(ApiEnum.iiif)) {
      const downloadTokenUrl = ResearchElementHelper.getIiifDownloadToken(url);
      this._openExternalPage(url, downloadTokenUrl);
    } else {
      SsrUtil.window.open(url, "_blank");
    }
  }

  private _openExternalPage(url: string, downloadTokenUrl: string): void {
    const researchElement: ResearchDataFile = MemoizedUtil.currentSnapshot(this._store, IngestResearchDataFileState);
    const isAccessPublic = researchElement?.project?.accessPublic;
    this.subscribe(ResearchElementHelper.getDownloadTokenDownloadIfNeeded(this._downloadService, isAccessPublic, downloadTokenUrl)
      .pipe(
        tap(() => SsrUtil.window.open(url, "_blank")),
      ));
  }
}
