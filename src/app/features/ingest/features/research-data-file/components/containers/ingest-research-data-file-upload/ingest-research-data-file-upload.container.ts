/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-research-data-file-upload.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  Input,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {IngestEditModeEnum} from "@app/features/ingest/enums/ingest-edit-mode.enum";
import {IngestResearchDataFileUploadArchiveDialog} from "@app/features/ingest/features/research-data-file/components/dialogs/ingest-research-data-file-upload-archive/ingest-research-data-file-upload-archive.dialog";
import {IngestResearchDataFileUploadDialog} from "@app/features/ingest/features/research-data-file/components/dialogs/ingest-research-data-file-upload/ingest-research-data-file-upload.dialog";
import {
  IngestResearchDataFileState,
  IngestResearchDataFileStateModel,
} from "@app/features/ingest/features/research-data-file/stores/research-data-file/ingest-research-data-file.state";
import {IngestResearchDataFileUploadAction} from "@app/features/ingest/features/research-data-file/stores/research-data-file/upload/ingest-research-data-file-upload.action";
import {IngestResearchDataFileUploadState} from "@app/features/ingest/features/research-data-file/stores/research-data-file/upload/ingest-research-data-file-upload.state";
import {HederaResearchDataFileUploadWrapper} from "@app/features/ingest/models/hedera-research-data-file-upload-wrapper.model";
import {IngestState} from "@app/features/ingest/stores/ingest.state";
import {ResearchDataFile} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {TourEnum} from "@shared/enums/tour.enum";
import {SecurityService} from "@shared/services/security.service";
import {
  combineLatest,
  Observable,
} from "rxjs";
import {map} from "rxjs/operators";
import {
  AbstractResourceRoutable,
  DialogUtil,
  isNonEmptyArray,
  MemoizedUtil,
  QueryParameters,
  UploadFileStatus,
} from "solidify-frontend";

@Component({
  selector: "hedera-ingest-research-data-file-upload-container",
  templateUrl: "./ingest-research-data-file-upload.container.html",
  styleUrls: ["./ingest-research-data-file-upload.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngestResearchDataFileUploadContainer extends AbstractResourceRoutable<ResearchDataFile, IngestResearchDataFileStateModel> {
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, IngestResearchDataFileState);
  uploadStatusObs: Observable<UploadFileStatus<ResearchDataFile, HederaResearchDataFileUploadWrapper>[]> = MemoizedUtil.select(this._store, IngestResearchDataFileUploadState, state => state.uploadStatus);
  canEditObs: Observable<IngestEditModeEnum> = MemoizedUtil.select(this._store, IngestState, state => state.canEdit);
  projectIdObs: Observable<string> = MemoizedUtil.select(this._store, IngestState, state => state.projectId);
  uploadStatusFilteredByCurrentContextObs: Observable<UploadFileStatus<ResearchDataFile, HederaResearchDataFileUploadWrapper>[]>;

  readonly KEY_PARAM_NAME: keyof ResearchDataFile & string = undefined;

  get editModeEnum(): typeof IngestEditModeEnum {
    return IngestEditModeEnum;
  }

  get tourEnum(): typeof TourEnum {
    return TourEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  @Input()
  editMode: IngestEditModeEnum = IngestEditModeEnum.none;

  private get _projectId(): string {
    return MemoizedUtil.selectSnapshot(this._store, IngestState, state => state.projectId);
  }

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _securityService: SecurityService) {
    super(StateEnum.ingest_researchDataFile, _injector, StateEnum.ingest);

    this.uploadStatusFilteredByCurrentContextObs = combineLatest([this.uploadStatusObs, this.projectIdObs])
      .pipe(map(([listUploadStatus, projectId]) => listUploadStatus.filter(u => u.fileUploadWrapper.projectId === projectId)));
  }

  upload($event: HederaResearchDataFileUploadWrapper): void {
    this._store.dispatch(new IngestResearchDataFileUploadAction.UploadFile(this._projectId, $event));
  }

  uploadArchive($event: HederaResearchDataFileUploadWrapper): void {
    this._store.dispatch(new IngestResearchDataFileUploadAction.UploadFile(this._projectId, $event, true));
  }

  retry($event: UploadFileStatus<ResearchDataFile>): void {
    this._store.dispatch(new IngestResearchDataFileUploadAction.RetrySendFile(this._projectId, $event));
  }

  cancel($event: UploadFileStatus<ResearchDataFile>): void {
    this._store.dispatch(new IngestResearchDataFileUploadAction.MarkAsCancelFileSending(this._projectId, $event));
  }

  resume($event: ResearchDataFile): void {
    // this._store.dispatch(new IngestResearchDataFileAction.Resume(this._projectId, $event));
  }

  refresh(): void {
    // this._store.dispatch(new IngestResearchDataFileAction.Refresh(this._projectId));
  }

  download($event: ResearchDataFile): void {
    // this._store.dispatch(new IngestResearchDataFileAction.Download(this._projectId, $event));
  }

  openModalUpload(): void {
    this.subscribe(DialogUtil.open(this._dialog, IngestResearchDataFileUploadDialog, {
        projectId: this._projectId,
      }, {
        width: "500px",
        disableClose: true,
      },
      listFilesUploadWrapper => {
        if (isNonEmptyArray(listFilesUploadWrapper)) {
          listFilesUploadWrapper.forEach(f => this.upload(f));
        }
      }));
  }

  openModalUploadArchive(): void {
    this.subscribe(DialogUtil.open(this._dialog, IngestResearchDataFileUploadArchiveDialog, {
        projectId: this._projectId,
      }, {
        width: "500px",
      },
      fileUploadWrapper => {
        this.uploadArchive(fileUploadWrapper);
      }));
  }
}
