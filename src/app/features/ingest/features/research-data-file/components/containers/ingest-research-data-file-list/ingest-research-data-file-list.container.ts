/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-research-data-file-list.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  Input,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {IngestEditModeEnum} from "@app/features/ingest/enums/ingest-edit-mode.enum";
import {IngestResearchDataFileUploadDialog} from "@app/features/ingest/features/research-data-file/components/dialogs/ingest-research-data-file-upload/ingest-research-data-file-upload.dialog";
import {
  IngestResearchDataFileAction,
  ingestResearchDataFileActionNameSpace,
} from "@app/features/ingest/features/research-data-file/stores/research-data-file/ingest-research-data-file.action";
import {
  IngestResearchDataFileState,
  IngestResearchDataFileStateModel,
} from "@app/features/ingest/features/research-data-file/stores/research-data-file/ingest-research-data-file.state";
import {ingestResearchDataFileStatusHistoryNamespace} from "@app/features/ingest/features/research-data-file/stores/research-data-file/status-history/ingest-research-data-file-status-history.action";
import {IngestResearchDataFileStatusHistoryState} from "@app/features/ingest/features/research-data-file/stores/research-data-file/status-history/ingest-research-data-file-status-history.state";
import {IngestResearchDataFileUploadAction} from "@app/features/ingest/features/research-data-file/stores/research-data-file/upload/ingest-research-data-file-upload.action";
import {IngestResearchDataFileUploadState} from "@app/features/ingest/features/research-data-file/stores/research-data-file/upload/ingest-research-data-file-upload.state";
import {IngestHelper} from "@app/features/ingest/helpers/ingest.helper";
import {HederaResearchDataFileUploadWrapper} from "@app/features/ingest/models/hedera-research-data-file-upload-wrapper.model";
import {IngestState} from "@app/features/ingest/stores/ingest.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  ResearchDataFile,
  ResearchObjectType,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {FileViewModeEnum} from "@shared/enums/file-view-mode.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  IngestRoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataFileHelper} from "@shared/helpers/data-file.helper";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {SecurityService} from "@shared/services/security.service";
import {StoreDialogService} from "@shared/services/store-dialog.service";
import {sharedResearchObjectTypeActionNameSpace} from "@shared/stores/research-object-type/shared-research-object-type.action";
import {SharedResearchObjectTypeState} from "@shared/stores/research-object-type/shared-research-object-type.state";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  map,
  take,
  tap,
} from "rxjs/operators";
import {
  AbstractResourceRoutable,
  ButtonColorEnum,
  ConfirmDialog,
  DataTableActions,
  DataTableBulkActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DataTablePresentational,
  DeleteDialog,
  DialogUtil,
  FilePreviewDialog,
  FileStatusEnum,
  FileVisualizerHelper,
  FileVisualizerService,
  isNonEmptyArray,
  isTrue,
  isUndefined,
  labelSolidifyCore,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
  QueryParametersUtil,
  ResourceNameSpace,
  SOLIDIFY_CONSTANTS,
  Sort,
  StatusHistory,
  StatusHistoryDialog,
} from "solidify-frontend";

@Component({
  selector: "hedera-ingest-research-data-file-list-container",
  templateUrl: "./ingest-research-data-file-list.container.html",
  styleUrls: ["./ingest-research-data-file-list.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngestResearchDataFileListContainer extends AbstractResourceRoutable<ResearchDataFile, IngestResearchDataFileStateModel> {
  @Input()
  deleteAvailable: boolean = true;

  private readonly _KEY_QUERY_PARAMETERS: keyof ResearchDataFile = "status" as any;
  isLoadingDataFileObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, IngestResearchDataFileState);
  listDataFileObs: Observable<ResearchDataFile[]> = MemoizedUtil.list(this._store, IngestResearchDataFileState);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, IngestResearchDataFileState).pipe(
    tap(queryParameters => this.isInErrorStatusFilter = MappingObjectUtil.get(QueryParametersUtil.getSearchItems(queryParameters), this._KEY_QUERY_PARAMETERS) === Enums.ResearchDataFile.StatusEnum.IN_ERROR),
  );
  draggingResearchDataFile: ResearchDataFile | undefined = undefined;
  currentFolderObs: Observable<string> = MemoizedUtil.select(this._store, IngestResearchDataFileState, state => state.currentFolder);
  intermediateFoldersObs: Observable<string[]> = MemoizedUtil.select(this._store, IngestResearchDataFileState, state => state.intermediateFolders);
  foldersWithIntermediateFoldersObs: Observable<string[]> = MemoizedUtil.select(this._store, IngestResearchDataFileState, state => state.foldersWithIntermediateFolders);
  projectIdObs: Observable<string> = MemoizedUtil.select(this._store, IngestState, state => state.projectId);
  canEditObs: Observable<IngestEditModeEnum> = MemoizedUtil.select(this._store, IngestState, state => state.canEdit).pipe(
    distinctUntilChanged(),
    tap(canEdit => {
      if (canEdit !== IngestEditModeEnum.none) {
        this.actions = [...this.actions];
      }
    }), // Force method computeContext on datatable
  );
  detailRouteToInterpolate: string;

  currentFileViewMode: FileViewModeEnum = FileViewModeEnum.FolderView;

  @ViewChild("dataTablePresentational")
  readonly dataTablePresentational: DataTablePresentational<ResearchDataFile>;

  columns: DataTableColumns<ResearchDataFile>[];

  actions: DataTableActions<ResearchDataFile>[] = [
    {
      logo: IconNameEnum.download,
      callback: (researchDataFile: ResearchDataFile) => this.downloadDataFile(this._projectId, researchDataFile),
      placeholder: current => LabelTranslateEnum.download,
      displayOnCondition: current => current.status === Enums.ResearchDataFile.StatusEnum.COMPLETED,
      isWrapped: false,
    },
    {
      logo: IconNameEnum.preview,
      callback: (researchDataFile: ResearchDataFile) => this._showPreview(researchDataFile),
      placeholder: current => LabelTranslateEnum.showPreview,
      displayOnCondition: (researchDataFile: ResearchDataFile) => {
        const dataFile = DataFileHelper.dataFileAdapter(researchDataFile as any);
        dataFile.status = FileStatusEnum.READY;
        return FileVisualizerHelper.canHandle(this._fileVisualizerService.listFileVisualizer, {
          dataFile: dataFile,
          fileExtension: FileVisualizerHelper.getFileExtension(researchDataFile.fileName),
        });
      },
      isWrapped: false,
    },
    {
      logo: IconNameEnum.delete,
      callback: (researchDataFile: ResearchDataFile) => this._deleteDataFile(this._projectId, researchDataFile),
      placeholder: current => LabelTranslateEnum.delete,
      displayOnCondition: (researchDataFile: ResearchDataFile) => {
        const canEdit = MemoizedUtil.selectSnapshot(this._store, IngestState, state => state.canEdit);
        return canEdit === IngestEditModeEnum.full && this.deleteAvailable;
      },
    },
    {
      logo: IconNameEnum.error,
      callback: (researchDataFile: ResearchDataFile) => this._putInErrorDataFile(researchDataFile),
      placeholder: current => LabelTranslateEnum.putInError,
      displayOnCondition: (researchDataFile: ResearchDataFile) => this._securityService.isRootOrAdmin(),
    },
  ];

  bulkActions: DataTableBulkActions<ResearchDataFile>[] = [
    {
      icon: IconNameEnum.delete,
      callback: (list: ResearchDataFile[]) => this._bulkDeleteDataFile(list.map(s => s.resId)),
      labelToTranslate: () => LabelTranslateEnum.deleteSelection,
      displayCondition: (list: ResearchDataFile[]) => this.canEditObs.pipe(map(canEdit => canEdit === IngestEditModeEnum.full)),
      color: ButtonColorEnum.primary,
    },
    {
      icon: IconNameEnum.error,
      callback: (list: ResearchDataFile[]) => this._bulkPutInErrorDataFile(list),
      labelToTranslate: () => LabelTranslateEnum.putInErrorSelection,
      displayCondition: (list: ResearchDataFile[]) => this._securityService.isRootOrAdmin(),
      color: ButtonColorEnum.primary,
    },
  ];

  columnsToSkippedFilter: keyof ResearchDataFile[] | string[] = [
    IngestHelper.KEY_PROJECT,
  ];

  get fileViewModeEnum(): typeof FileViewModeEnum {
    return FileViewModeEnum;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get dataTestEnum(): typeof DataTestEnum {
    return DataTestEnum;
  }

  get editModeEnum(): typeof IngestEditModeEnum {
    return IngestEditModeEnum;
  }

  private get _projectId(): string {
    return MemoizedUtil.selectSnapshot(this._store, IngestState, state => state.projectId);
  }

  readonly KEY_PARAM_NAME: keyof ResearchDataFile & string = undefined;
  isInErrorStatusFilter: boolean = false;

  sharedResearchObjectTypeSort: Sort<ResearchObjectType> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedResearchObjectTypeActionNameSpace: ResourceNameSpace = sharedResearchObjectTypeActionNameSpace;
  sharedResearchObjectTypeState: typeof SharedResearchObjectTypeState = SharedResearchObjectTypeState;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _securityService: SecurityService,
              private readonly _fileVisualizerService: FileVisualizerService,
              protected readonly _storeDialogService: StoreDialogService) {
    super(StateEnum.ingest_researchDataFile, _injector, StateEnum.ingest);

    this.subscribe(this.projectIdObs.pipe(
      distinctUntilChanged(),
      tap(projectId => {
        this._computeDetailRouteToInterpolate();
        this._store.dispatch(new IngestResearchDataFileAction.GetListFolder(projectId));
        let queryParameters = MemoizedUtil.queryParametersSnapshot(this._store, IngestResearchDataFileState);
        queryParameters = QueryParametersUtil.resetToFirstPage(queryParameters);
        this.onQueryParametersEvent(queryParameters);
      }),
    ));

    this.columns = [
      {
        field: "fileName",
        header: LabelTranslateEnum.fileName,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.ascending,
        isSortable: true,
        isFilterable: true,
      },
      {
        field: "accessibleFrom",
        header: LabelTranslateEnum.accessibleFrom,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        sortableField: "accessibleFrom" as any,
        filterableField: "accessibleFrom" as any,
        isSortable: true,
        isFilterable: true,
        translate: true,
        width: "100px",
        filterEnum: Enums.ResearchDataFile.AccessibleFromEnumTranslate,
        alignment: "center",
      },
      {
        field: "researchObjectType.name" as any,
        header: LabelTranslateEnum.researchObjectType,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        sortableField: "researchObjectTypeId" as any,
        filterableField: "researchObjectTypeId" as any,
        isSortable: true,
        isFilterable: true,
        resourceNameSpace: this.sharedResearchObjectTypeActionNameSpace,
        resourceState: this.sharedResearchObjectTypeState as any,
        searchableSingleSelectSort: this.sharedResearchObjectTypeSort,
        resourceLabelCallback: (value: ResearchObjectType) => value.name,
        resourceLabelKey: "name",
        alignment: "center",
        // component: DataTableComponentHelper.get(DataTableComponentEnum.container)
      },
      {
        field: "fileSize",
        header: LabelTranslateEnum.size,
        type: DataTableFieldTypeEnum.size,
        order: OrderEnum.none,
        sortableField: "fileSize" as any,
        filterableField: "fileSize" as any,
        isSortable: true,
        isFilterable: false,
        translate: true,
        alignment: "right",
        width: "100px",
      },
      {
        field: "status",
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isSortable: true,
        isFilterable: true,
        translate: true,
        filterEnum: Enums.ResearchDataFile.StatusEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.status),
        alignment: "center",
        maxWidth: "180px",
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: true,
        width: "40px",
      },
    ];
  }

  onQueryParametersEvent(queryParameters: QueryParameters, withRefresh: boolean = true): void {
    queryParameters = QueryParametersUtil.clone(queryParameters);
    MappingObjectUtil.set(QueryParametersUtil.getSearchItems(queryParameters), IngestResearchDataFileUploadState.PROJECT_ID_FIELD, this._projectId);
    this._store.dispatch(new IngestResearchDataFileAction.ChangeQueryParameters(this._projectId, queryParameters, true, withRefresh));
  }

  private _computeDetailRouteToInterpolate(): void {
    this.detailRouteToInterpolate = `${AppRoutesEnum.ingest}/${this._projectId}/${IngestRoutesEnum.researchDataFile}/{${SOLIDIFY_CONSTANTS.RES_ID}}`;
  }

  private _bulkDeleteDataFile(listId: string[]): Observable<boolean> {
    return DialogUtil.open(this._dialog, DeleteDialog, {
        name: undefined,
        resourceNameSpace: ingestResearchDataFileActionNameSpace,
        message: LabelTranslateEnum.areYouSureYouWantToDeleteTheSelectedFiles,
      }, {
        minWidth: "500px",
      },
      isConfirmed => {
        if (isTrue(isConfirmed)) {
          this._bulkDeleteDataFileWithoutConfirmation(listId);
        }
      }).pipe(
      map(result => isTrue(result)),
    );
  }
  private _bulkPutInErrorDataFile(listDataFile: ResearchDataFile[]): void {
    this._store.dispatch(new IngestResearchDataFileAction.PutInErrorAll(listDataFile.map(r => r.resId)));
  }

  private _bulkDeleteDataFileWithoutConfirmation(listId: string[]): void {
    this._store.dispatch(new IngestResearchDataFileAction.DeleteAll(listId));
  }

  private _deleteDataFile(parentId: string, dataFile: ResearchDataFile): void {
    const deleteData = this._storeDialogService.deleteData(StateEnum.ingest_researchDataFile);
    deleteData.name = dataFile.fileName;
    this.subscribe(DialogUtil.open(this._dialog, DeleteDialog, deleteData,
      {
        width: "400px",
      },
      isConfirmed => {
        if (isTrue(isConfirmed)) {
          const currentFolder = MemoizedUtil.selectSnapshot(this._store, IngestResearchDataFileState, state => state.currentFolder);
          this._store.dispatch(new IngestResearchDataFileAction.Delete(parentId, dataFile.resId, currentFolder));
        }
      }));
  }

  private _putInErrorDataFile(dataFile: ResearchDataFile): void {
    this._store.dispatch(new IngestResearchDataFileAction.PutInError(dataFile));
  }

  private _showPreview(researchDataFile: ResearchDataFile): void {
    const dataFile = DataFileHelper.dataFileAdapter(researchDataFile as any);
    dataFile.status = FileStatusEnum.READY;
    DialogUtil.open(this._dialog, FilePreviewDialog, {
        fileInput: {
          dataFile: dataFile,
        },
        fileDownloadUrl: `${ApiEnum.ingestResearchDataFiles}/${researchDataFile.resId}/${ApiActionNameEnum.DOWNLOAD}`, // TODO URL WITH PARENT
      },
      {
        width: "max-content",
        maxWidth: "90vw",
        height: "min-content",
      });
  }

  downloadDataFile(parentId: string, dataFile: ResearchDataFile): void {
    this._store.dispatch(new IngestResearchDataFileAction.Download(parentId, dataFile));
  }

  selectAndDelete(): void {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: labelSolidifyCore.coreCeleteDialogConfirmDeleteAction,
      messageToTranslate: LabelTranslateEnum.areYouSureYouWantToDeleteTheSelectedFiles,
      confirmButtonToTranslate: LabelTranslateEnum.yesImSure,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.warn,
    }, undefined, (isConfirmed: boolean) => {
      if (isTrue(isConfirmed)) {
        this.subscribe(this.dataTablePresentational.selectAllResult().pipe(
          take(1),
          tap(selection => {
            this._bulkDeleteDataFileWithoutConfirmation(selection);
            this.dataTablePresentational.cleanSelection();
          }),
        ));
      }
    }));
  }

  upload($event: HederaResearchDataFileUploadWrapper): void {
    this._store.dispatch(new IngestResearchDataFileUploadAction.UploadFile(this._projectId, $event));
  }

  dragStart($event: ResearchDataFile): void {
    this.draggingResearchDataFile = $event;
  }

  dragEnd($event: ResearchDataFile): void {
    this.draggingResearchDataFile = undefined;
  }

  showHistory(researchDataFile: ResearchDataFile): void {
    const isLoadingHistoryObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, IngestResearchDataFileStatusHistoryState);
    const queryParametersHistoryObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, IngestResearchDataFileStatusHistoryState, state => state.queryParameters);
    const historyObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, IngestResearchDataFileStatusHistoryState, state => state.history);
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: researchDataFile.projectId,
      resourceResId: researchDataFile.resId,
      name: researchDataFile.fileName,
      statusHistory: historyObs,
      isLoading: isLoadingHistoryObs,
      queryParametersObs: queryParametersHistoryObs,
      state: ingestResearchDataFileStatusHistoryNamespace,
      statusEnums: Enums.ResearchDataFile.StatusEnumTranslate,
    }, {
      width: environment.modalWidth,
    });
  }

  selectFolder(folderFullName: string | undefined): void {
    if (isUndefined(folderFullName)) {
      this.currentFileViewMode = FileViewModeEnum.FlatView;
    } else {
      this.currentFileViewMode = FileViewModeEnum.FolderView;
    }
    this._store.dispatch(new IngestResearchDataFileAction.ChangeCurrentFolder(folderFullName, true));
  }

  downloadFolder(folderName: string): void {
    this._store.dispatch(new IngestResearchDataFileAction.DownloadFolder(this._projectId, folderName));
  }

  deleteFolder(folderName: string): void {
    this.subscribe(DialogUtil.open(this._dialog, DeleteDialog, {
        name: folderName,
        resourceNameSpace: ingestResearchDataFileActionNameSpace,
        message: MARK_AS_TRANSLATABLE("researchDataFile.dialog.deleteFolder.message"),
      },
      {
        minWidth: "500px",
      },
      isConfirmed => {
        if (isTrue(isConfirmed)) {
          this._store.dispatch(new IngestResearchDataFileAction.DeleteFolder(this._projectId, folderName));
        }
      }));
  }

  moveDataFile(dataFile: ResearchDataFile): void {
    // this._store.dispatch(new .Move(this._resId, {
    //   resId: dataFile.resId,
    //   relativeLocation: dataFile.relativeLocation,
    // }));
  }

  uploadInFolder(fullFolderPath: string): void {
    this.subscribe(DialogUtil.open(this._dialog, IngestResearchDataFileUploadDialog, {
        subDirectory: fullFolderPath,
        projectId: this._projectId,
      }, {
        width: "500px",
        disableClose: true,
      },
      listFilesUploadWrapper => {
        if (isNonEmptyArray(listFilesUploadWrapper)) {
          listFilesUploadWrapper.forEach(f => this.upload(f));
        }
      }));
  }
}
