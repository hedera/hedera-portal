/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-research-data-file.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ResearchDataFile} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  CompositionAction,
  CompositionNameSpace,
  Result,
  TypeDefaultAction,
} from "solidify-frontend";

const state = StateEnum.ingest_researchDataFile;

export namespace IngestResearchDataFileAction {
  @TypeDefaultAction(state)
  export class ChangeQueryParameters extends CompositionAction.ChangeQueryParameters {
  }

  @TypeDefaultAction(state)
  export class GetAll extends CompositionAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends CompositionAction.GetAllSuccess<ResearchDataFile> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends CompositionAction.GetAllFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends CompositionAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends CompositionAction.GetByIdSuccess<ResearchDataFile> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends CompositionAction.GetByIdFail {
  }

  @TypeDefaultAction(state)
  export class Update extends CompositionAction.Update<ResearchDataFile> {
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends CompositionAction.UpdateSuccess<ResearchDataFile> {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends CompositionAction.UpdateFail<ResearchDataFile> {
  }

  @TypeDefaultAction(state)
  export class Create extends CompositionAction.Create<ResearchDataFile> {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends CompositionAction.CreateSuccess<ResearchDataFile> {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends CompositionAction.CreateFail<ResearchDataFile> {
  }

  @TypeDefaultAction(state)
  export class Delete extends CompositionAction.Delete {
    constructor(public parentId: string, public resId: string, public fullFolderName: string) {
      super(parentId, resId);
    }
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends CompositionAction.DeleteSuccess {
    constructor(public parentAction: Delete, public parentId: string) {
      super(parentAction, parentId);
    }
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends CompositionAction.DeleteFail {
  }

  @TypeDefaultAction(state)
  export class Clean extends CompositionAction.Clean {
  }

  export class Refresh extends BaseAction {
    static readonly type: string = `[${state}] Refresh`;

    constructor(public parentId: string) {
      super();
    }
  }

  export class RefreshSuccess extends BaseSubActionSuccess<Refresh> {
    static readonly type: string = `[${state}] Refresh Success`;
  }

  export class RefreshFail extends BaseSubActionFail<Refresh> {
    static readonly type: string = `[${state}] Refresh Fail`;
  }

  export class Download {
    static readonly type: string = `[${state}] Download`;

    constructor(public parentId: string, public dataFile: ResearchDataFile) {
    }
  }

  export class DownloadFolder {
    static readonly type: string = `[${state}] Download Folder`;

    constructor(public projectId: string, public fullFolderName: string) {
    }
  }

  export class DeleteFolder extends BaseAction {
    static readonly type: string = `[${state}] Delete Folder`;

    constructor(public projectId: string, public fullFolderName: string) {
      super();
    }
  }

  export class DeleteFolderSuccess extends BaseSubActionSuccess<DeleteFolder> {
    static readonly type: string = `[${state}] Delete Folder Success`;
  }

  export class DeleteFolderFail extends BaseSubActionFail<DeleteFolder> {
    static readonly type: string = `[${state}] Delete Folder Fail`;
  }

  export class GetListFolder extends BaseAction {
    static readonly type: string = `[${state}] Get List Folder`;

    constructor(public projectId: string) {
      super();
    }
  }

  export class GetListFolderSuccess extends BaseSubActionSuccess<GetListFolder> {
    static readonly type: string = `[${state}] Get List Folder Success`;

    constructor(public parentAction: GetListFolder, public folder: string[]) {
      super(parentAction);
    }
  }

  export class GetListFolderFail extends BaseSubActionFail<GetListFolder> {
    static readonly type: string = `[${state}] Get List Folder Fail`;

    constructor(public parentAction: GetListFolder) {
      super(parentAction);
    }
  }

  export class ChangeCurrentFolder {
    static readonly type: string = `[${state}] Change Current Folder`;

    constructor(public newCurrentFolder: string, public refreshNewFolderContent: boolean = false) {
    }
  }

  export class DeleteAll extends BaseAction {
    static readonly type: string = `[${state}] Delete All`;

    constructor(public listResId: string[]) {
      super();
    }
  }

  export class DeleteAllSuccess extends BaseSubActionSuccess<DeleteAll> {
    static readonly type: string = `[${state}] Delete All Success`;
  }

  export class DeleteAllFail extends BaseSubActionFail<DeleteAll> {
    static readonly type: string = `[${state}] Delete All Fail`;
  }

  export class PutInError extends BaseAction {
    static readonly type: string = `[${state}] Put In Error`;

    constructor(public dataFile: ResearchDataFile) {
      super();
    }
  }

  export class PutInErrorSuccess extends BaseSubActionSuccess<PutInError> {
    static readonly type: string = `[${state}] Put In Error Success`;

    constructor(public parentAction: PutInError, public result: Result) {
      super(parentAction);
    }
  }

  export class PutInErrorFail extends BaseSubActionFail<PutInError> {
    static readonly type: string = `[${state}] Put In Error Fail`;

    constructor(public parentAction: PutInError, public result?: Result) {
      super(parentAction);
    }
  }

  export class PutInErrorAll extends BaseAction {
    static readonly type: string = `[${state}] Put In Error All`;

    constructor(public listResId: string[]) {
      super();
    }
  }

  export class PutInErrorAllSuccess extends BaseSubActionSuccess<PutInErrorAll> {
    static readonly type: string = `[${state}] Put In Error All Success`;
  }

  export class PutInErrorAllFail extends BaseSubActionFail<PutInErrorAll> {
    static readonly type: string = `[${state}] Put In Error All Fail`;
  }

  export class Move extends BaseAction {
    static readonly type: string = `[${state}] Move`;

    constructor(public parentId: string, public dataFile: ResearchDataFile) {
      super();
    }
  }

  export class MoveSuccess extends BaseSubActionSuccess<Move> {
    static readonly type: string = `[${state}] Move Success`;

    constructor(public parentAction: Move, public result: ResearchDataFile) {
      super(parentAction);
    }
  }

  export class MoveFail extends BaseSubActionFail<Move> {
    static readonly type: string = `[${state}] Move Fail`;
  }

  export class RefreshTotal extends BaseAction {
    static readonly type: string = `[${state}] Refresh Total`;

    constructor(public parentId: string) {
      super();
    }
  }

  export class RefreshTotalSuccess extends BaseSubActionSuccess<RefreshTotal> {
    static readonly type: string = `[${state}] Refresh Total Success`;

    constructor(public parentAction: RefreshTotal, public total: number) {
      super(parentAction);
    }
  }

  export class RefreshTotalFail extends BaseSubActionFail<RefreshTotal> {
    static readonly type: string = `[${state}] Refresh Total Fail`;
  }

  export class Reload extends BaseAction {
    static readonly type: string = `[${state}] Reload`;

    constructor(public projectId: string) {
      super();
    }
  }

  export class ReloadSuccess extends BaseSubActionSuccess<Reload> {
    static readonly type: string = `[${state}] Reload Success`;

    constructor(public parentAction: Reload, public result: Result) {
      super(parentAction);
    }
  }

  export class ReloadFail extends BaseSubActionFail<Reload> {
    static readonly type: string = `[${state}] Reload Fail`;
  }

  // export class GetListCurrentStatus extends BaseAction {
  //   static readonly type: string = `[${state}] Get List Current Status`;
  //
  //   constructor(public parentId: string) {
  //     super();
  //   }
  // }
  //
  // export class GetListCurrentStatusSuccess extends BaseSubActionSuccess<GetListCurrentStatus> {
  //   static readonly type: string = `[${state}] Get List Current Status Folder Success`;
  //
  //   constructor(public parentAction: GetListCurrentStatus, public listCurrentStatus: MappingObject<Enums.DataFile.StatusEnum, number>) {
  //     super(parentAction);
  //   }
  // }
  //
  // export class GetListCurrentStatusFail extends BaseSubActionFail<GetListCurrentStatus> {
  //   static readonly type: string = `[${state}] Get List Current Status Folder Fail`;
  // }

}

export const ingestResearchDataFileActionNameSpace: CompositionNameSpace = IngestResearchDataFileAction;
