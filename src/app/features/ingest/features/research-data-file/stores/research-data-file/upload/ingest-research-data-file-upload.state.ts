/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-research-data-file-upload.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {IngestResearchDataFileAction} from "@app/features/ingest/features/research-data-file/stores/research-data-file/ingest-research-data-file.action";
import {
  IngestResearchDataFileUploadAction,
  ingestResearchDataFileUploadActionNameSpace,
} from "@app/features/ingest/features/research-data-file/stores/research-data-file/upload/ingest-research-data-file-upload.action";
import {HederaResearchDataFileUploadWrapper} from "@app/features/ingest/models/hedera-research-data-file-upload-wrapper.model";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {Enums} from "@enums";
import {HederaEnvironment} from "@environments/environment.defaults.model";
import {
  ResearchDataFile,
  SystemProperty,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  AppSystemPropertyState,
  defaultUploadStateInitValue,
  ENVIRONMENT,
  isNotNullNorUndefinedNorWhiteString,
  LABEL_TRANSLATE,
  LabelTranslateInterface,
  MemoizedUtil,
  NotificationService,
  UploadState,
  UploadStateModel,
} from "solidify-frontend";

export interface IngestResearchDataFileUploadStateModel extends UploadStateModel<ResearchDataFile, HederaResearchDataFileUploadWrapper> {
}

@Injectable()
@State<IngestResearchDataFileUploadStateModel>({
  name: StateEnum.ingest_researchDataFile_upload,
  defaults: {
    ...defaultUploadStateInitValue(),
  },
  children: [],
})
export class IngestResearchDataFileUploadState extends UploadState<IngestResearchDataFileUploadStateModel, ResearchDataFile> {
  private readonly _FILE_KEY: string = "file";
  static readonly PROJECT_ID_FIELD: string = "projectId";
  static readonly RELATIVE_LOCATION: string = "relativeLocation";
  static readonly RESEARCH_OBJECT_TYPE_ID_FIELD: string = "researchObjectTypeId";
  private readonly _ACCESSIBLE_FROM_FIELD: string = "accessibleFrom";
  private readonly _MIME_TYPE_PARAM: string = "mimeType";
  private readonly _CHECKSUM_MD5: string = "checksumMd5";
  private readonly _CHECKSUM_MD5_ORIGIN: string = "checksumMd5Origin";
  private readonly _CHECKSUM_SHA1: string = "checksumSha1";
  private readonly _CHECKSUM_SHA1_ORIGIN: string = "checksumSha1Origin";
  private readonly _CHECKSUM_SHA256: string = "checksumSha256";
  private readonly _CHECKSUM_SHA256_ORIGIN: string = "checksumSha256Origin";
  private readonly _CHECKSUM_CRC32: string = "checksumCrc32";
  private readonly _CHECKSUM_CRC32_ORIGIN: string = "checksumCrc32Origin";

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              @Inject(ENVIRONMENT) protected readonly _environment: HederaEnvironment,
              @Inject(LABEL_TRANSLATE) protected readonly _labelTranslateInterface: LabelTranslateInterface,
              protected readonly _translate: TranslateService,
  ) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: ingestResearchDataFileUploadActionNameSpace,
      customUploadUrlSuffix: action => action.isArchive ? ApiActionNameEnum.UPLOAD_ZIP : ApiActionNameEnum.UPLOAD,
      callbackAfterAllUploadFinished: action => _store.dispatch(new IngestResearchDataFileAction.Refresh(action.parentId)),
      fileSizeLimit: () => +MemoizedUtil.selectSnapshot(this._store, AppSystemPropertyState<SystemProperty>, state => state.current.uploadFileSizeLimit),
    }, _environment, _labelTranslateInterface, _translate);
  }

  protected get _urlResource(): string {
    return ApiEnum.ingestResearchDataFiles;
  }

  protected override _generateUploadFormData(action: IngestResearchDataFileUploadAction.UploadFile): FormData {
    const formData = new FormData();
    const fileUploadWrapper = action.fileUploadWrapper as HederaResearchDataFileUploadWrapper;
    if (isNotNullNorUndefinedNorWhiteString(fileUploadWrapper.mimeType)) {
      formData.append(this._MIME_TYPE_PARAM, fileUploadWrapper.mimeType);
    }
    formData.append(this._FILE_KEY, fileUploadWrapper.file, fileUploadWrapper.file.name);
    formData.append(IngestResearchDataFileUploadState.PROJECT_ID_FIELD, action.parentId);
    formData.append(this._ACCESSIBLE_FROM_FIELD, fileUploadWrapper.accessibleFrom);
    formData.append(IngestResearchDataFileUploadState.RELATIVE_LOCATION, fileUploadWrapper.relativeLocation);
    // MappingObjectUtil.forEach(fileUploadWrapper.checksums, (checksum, algo: Enums.DataFile.Checksum.AlgoEnum) => {
    //   const algoKey = this._getChecksumKey(algo);
    //   if (isNullOrUndefined(algoKey)) {
    //     return;
    //   }
    //   formData.append(algoKey, checksum);
    // });
    // MappingObjectUtil.forEach(fileUploadWrapper.checksumsOrigin, (origin: Enums.DataFile.Checksum.OriginEnum, algo: Enums.DataFile.Checksum.AlgoEnum) => {
    //   const originKey = this._getChecksumOriginKey(algo);
    //   if (isNullOrUndefined(originKey)) {
    //     return;
    //   }
    //   formData.append(originKey, origin);
    // });
    return formData;
  }

  private _getChecksumKey(checksumAlgo: Enums.DataFile.Checksum.AlgoEnum): string | undefined {
    switch (checksumAlgo) {
      case Enums.DataFile.Checksum.AlgoEnum.MD5:
        return this._CHECKSUM_MD5;
      case Enums.DataFile.Checksum.AlgoEnum.SHA1:
        return this._CHECKSUM_SHA1;
      case Enums.DataFile.Checksum.AlgoEnum.SHA256:
        return this._CHECKSUM_SHA256;
      case Enums.DataFile.Checksum.AlgoEnum.CRC32:
        return this._CHECKSUM_CRC32;
    }
    return undefined;
  }

  private _getChecksumOriginKey(checksumAlgo: Enums.DataFile.Checksum.AlgoEnum): string | undefined {
    switch (checksumAlgo) {
      case Enums.DataFile.Checksum.AlgoEnum.MD5:
        return this._CHECKSUM_MD5_ORIGIN;
      case Enums.DataFile.Checksum.AlgoEnum.SHA1:
        return this._CHECKSUM_SHA1_ORIGIN;
      case Enums.DataFile.Checksum.AlgoEnum.SHA256:
        return this._CHECKSUM_SHA256_ORIGIN;
      case Enums.DataFile.Checksum.AlgoEnum.CRC32:
        return this._CHECKSUM_CRC32_ORIGIN;
    }
    return undefined;
  }
}
