/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - ingest-research-data-file.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {
  IngestResearchDataFileAction,
  ingestResearchDataFileActionNameSpace,
} from "@app/features/ingest/features/research-data-file/stores/research-data-file/ingest-research-data-file.action";
import {
  IngestResearchDataFileStatusHistoryState,
  IngestResearchDataFileStatusHistoryStateModel,
} from "@app/features/ingest/features/research-data-file/stores/research-data-file/status-history/ingest-research-data-file-status-history.state";
import {
  IngestResearchDataFileUploadState,
  IngestResearchDataFileUploadStateModel,
} from "@app/features/ingest/features/research-data-file/stores/research-data-file/upload/ingest-research-data-file-upload.state";
import {IngestHelper} from "@app/features/ingest/helpers/ingest.helper";
import {IngestState} from "@app/features/ingest/stores/ingest.state";
import {environment} from "@environments/environment";
import {ResearchDataFile} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataFileHelper} from "@shared/helpers/data-file.helper";
import {SecurityService} from "@shared/services/security.service";
import {defaultStatusHistoryInitValue} from "@shared/stores/status-history/status-history.state";
import {Observable} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  CompositionState,
  defaultResourceStateInitValue,
  defaultUploadStateInitValue,
  DownloadService,
  FolderTreePresentational,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  OverrideDefaultAction,
  QueryParameters,
  QueryParametersUtil,
  ResourceStateModel,
  Result,
  ResultActionStatusEnum,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export interface IngestResearchDataFileStateModel extends ResourceStateModel<ResearchDataFile> {
  canCreate: boolean;
  foldersWithIntermediateFolders: string[];
  intermediateFolders: string[];
  currentFolder: string;
  folders: string[];
  totalCount: number;
  [StateEnum.ingest_researchDataFile_upload]: IngestResearchDataFileUploadStateModel;
  [StateEnum.ingest_researchDataFile_history]: IngestResearchDataFileStatusHistoryStateModel;
}

export const defaultIngestResearchDataFileStateModelValue: () => IngestResearchDataFileStateModel = () =>
  ({
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultPageSize),
    canCreate: false,
    currentFolder: "" + DataFileHelper.ROOT,
    folders: [],
    foldersWithIntermediateFolders: [],
    intermediateFolders: [],
    totalCount: 0,
    [StateEnum.ingest_researchDataFile_upload]: {...defaultUploadStateInitValue()},
    [StateEnum.ingest_researchDataFile_history]: {...defaultStatusHistoryInitValue()},
  });

@Injectable()
@State<IngestResearchDataFileStateModel>({
  name: StateEnum.ingest_researchDataFile,
  defaults: {
    ...defaultIngestResearchDataFileStateModelValue(),
  },
  children: [
    IngestResearchDataFileUploadState,
    IngestResearchDataFileStatusHistoryState,
  ],
})
export class IngestResearchDataFileState extends CompositionState<IngestResearchDataFileStateModel, ResearchDataFile> {

  private readonly _KEY_RELATIVE_LOCATION: keyof ResearchDataFile = "relativeLocation";
  private readonly _KEY_PROJECT_ID: keyof ResearchDataFile = "projectId";
  private readonly _ZIP_EXTENSION: string = ".zip";
  private readonly _FOLDER_DOWNLOAD_PREFIX: string = "researchDataFile_";
  private readonly _SEPARATOR: string = DataFileHelper.SEPARATOR;

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _securityService: SecurityService,
              protected readonly _downloadService: DownloadService,
              protected readonly _httpClient: HttpClient) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: ingestResearchDataFileActionNameSpace,
      resourceName: ApiResourceNameEnum.RESEARCH_DATA_FILES,
      // notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("ingest.researchDataFile.notification.resource.create"),
      // notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("ingest.researchDataFile.notification.resource.delete"),
      // notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("ingest.researchDataFile.notification.resource.update"),
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.ingestResearchDataFiles;
  }

  protected override _evaluateSubResourceUrl(parentId: string): string {
    return this._urlResource;
  }

  @Action(IngestResearchDataFileAction.Refresh)
  refresh(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.Refresh): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new IngestResearchDataFileAction.GetAll(action.parentId, undefined, true),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(IngestResearchDataFileAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(IngestResearchDataFileAction.GetAllFail)),
        ],
      },
      {
        action: new IngestResearchDataFileAction.GetListFolder(action.parentId),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(IngestResearchDataFileAction.GetListFolderSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(IngestResearchDataFileAction.GetListFolderFail)),
        ],
      },
      {
        action: new IngestResearchDataFileAction.RefreshTotal(action.parentId),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(IngestResearchDataFileAction.RefreshTotalSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(IngestResearchDataFileAction.RefreshTotalFail)),
        ],
      },
    ]).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new IngestResearchDataFileAction.RefreshSuccess(action));
        } else {
          ctx.dispatch(new IngestResearchDataFileAction.RefreshFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(IngestResearchDataFileAction.RefreshSuccess)
  refreshSuccess(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.RefreshSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(IngestResearchDataFileAction.RefreshFail)
  refreshFail(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.RefreshFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(IngestResearchDataFileAction.Download)
  download(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.Download): void {
    const url = `${this._urlResource}/${action.dataFile.resId}/${ApiActionNameEnum.DOWNLOAD}`;
    const fileName = action.dataFile.fileName;
    const mimetype = action.dataFile.mimeType;
    if (environment.fileExtensionToCompleteWithRealExtensionOnDownload.findIndex(e => fileName.endsWith("." + e)) >= 0) {
      // Allow to download dua and thumbnail with good extension
      this.subscribe(this._downloadService.downloadInMemory(url, fileName, true, mimetype));
    } else {
      this._downloadService.download(url, fileName, action.dataFile.fileSize, false, mimetype);
    }
  }

  @Action(IngestResearchDataFileAction.DeleteAll)
  deleteAll(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.DeleteAll): Observable<string[]> {
    return this._apiService.delete<string[]>(`${this._urlResource}`, action.listResId)
      .pipe(
        tap(collection => {
          ctx.dispatch(new IngestResearchDataFileAction.DeleteAllSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new IngestResearchDataFileAction.DeleteAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(IngestResearchDataFileAction.DeleteAllSuccess)
  deleteAllSuccess(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.DeleteAllSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: QueryParametersUtil.resetToFirstPage(ctx.getState().queryParameters),
    });
    ctx.dispatch(new IngestResearchDataFileAction.Refresh(this.projectId));
  }

  @Action(IngestResearchDataFileAction.DeleteAllFail)
  deleteAllFail(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.DeleteAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: QueryParametersUtil.resetToFirstPage(ctx.getState().queryParameters),
    });
    this._notificationService.showWarning(LabelTranslateEnum.unableToDeleteAllSelectedDataFile);
    ctx.dispatch(new IngestResearchDataFileAction.Refresh(this.projectId));
  }

  @OverrideDefaultAction()
  @Action(IngestResearchDataFileAction.Delete)
  delete(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.Delete): Observable<ResearchDataFile> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.deleteById<ResearchDataFile>(url, action.resId)
      .pipe(
        tap(() => {
          ctx.dispatch(new IngestResearchDataFileAction.DeleteSuccess(action, action.parentId));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new IngestResearchDataFileAction.DeleteFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @OverrideDefaultAction()
  @Action(IngestResearchDataFileAction.DeleteSuccess)
  deleteSuccess(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.DeleteSuccess): void {
    const computeCurrentFolder = FolderTreePresentational.computeCurrentFolderAfterDelete(ctx.getState().intermediateFolders, ctx.getState().foldersWithIntermediateFolders, action.parentAction.fullFolderName);
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      currentFolder: computeCurrentFolder,
    });
    ctx.dispatch(new IngestResearchDataFileAction.Refresh(action.parentId));
  }

  private get projectId(): string {
    return MemoizedUtil.selectSnapshot(this._store, IngestState, state => state.projectId);
  }

  @Action(IngestResearchDataFileAction.DownloadFolder)
  downloadFolder(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.DownloadFolder): void {
    const fileName = this._FOLDER_DOWNLOAD_PREFIX + action.projectId;
    const url = `${this._urlResource}/${ApiActionNameEnum.DOWNLOAD}?${this._KEY_RELATIVE_LOCATION}=${action.fullFolderName}&${this._KEY_PROJECT_ID}=${action.projectId}`;
    this._downloadService.download(url, fileName + this._ZIP_EXTENSION, null, false);
  }

  @Action(IngestResearchDataFileAction.DeleteFolder)
  deleteFolder(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.DeleteFolder): Observable<string> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.post<string>(`${this._urlResource}/${ApiActionNameEnum.DELETE_FOLDER}?${this._KEY_RELATIVE_LOCATION}=${action.fullFolderName}&${this._KEY_PROJECT_ID}=${action.projectId}`)
      .pipe(
        tap(result => {
          ctx.dispatch(new IngestResearchDataFileAction.DeleteFolderSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new IngestResearchDataFileAction.DeleteFolderFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(IngestResearchDataFileAction.DeleteFolderSuccess)
  deleteFolderSuccess(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.DeleteFolderSuccess): void {
    const computeCurrentFolder = FolderTreePresentational.computeCurrentFolderAfterDelete(ctx.getState().intermediateFolders, ctx.getState().foldersWithIntermediateFolders, action.parentAction.fullFolderName);
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      currentFolder: computeCurrentFolder,
    });
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("notification.researchDataFile.folder.deleteWithSuccess"));
    ctx.dispatch(new IngestResearchDataFileAction.Refresh(action.parentAction.projectId));
  }

  @Action(IngestResearchDataFileAction.DeleteFolderFail)
  deleteFolderFail(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.DeleteFolderFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.researchDataFile.folder.deleteFail"));
  }

  @Action(IngestResearchDataFileAction.GetListFolder)
  getListFolder(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.GetListFolder): Observable<string[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._httpClient.get<string[]>(`${this._urlResource}/${ApiActionNameEnum.LIST_FOLDERS}?projectId=${action.projectId}`)
      .pipe(
        tap((listFolder: string[]) => {
          ctx.dispatch(new IngestResearchDataFileAction.GetListFolderSuccess(action, listFolder));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new IngestResearchDataFileAction.GetListFolderFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(IngestResearchDataFileAction.GetListFolderSuccess)
  getListFolderSuccess(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.GetListFolderSuccess): void {
    const folders = [...action.folder];
    const foldersWithIntermediateFolders = action.folder;
    const intermediateFolders = DataFileHelper.createIntermediateFolders(foldersWithIntermediateFolders);
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      folders,
      foldersWithIntermediateFolders,
      intermediateFolders,
    });
  }

  @Action(IngestResearchDataFileAction.GetListFolderFail)
  getListFolderFail(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.GetListFolderFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @OverrideDefaultAction()
  @Action(IngestResearchDataFileAction.GetAll)
  getAll<U>(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.GetAll): Observable<CollectionTyped<U>> {
    let queryParameters = action.queryParameters;
    if (isNullOrUndefined(queryParameters)) {
      queryParameters = QueryParametersUtil.clone(ctx.getState().queryParameters);
    }
    const currentFolder = ctx.getState().currentFolder;
    if (isNullOrUndefined(currentFolder)) {
      MappingObjectUtil.delete(QueryParametersUtil.getSearchItems(queryParameters), this._KEY_RELATIVE_LOCATION);

    } else {
      MappingObjectUtil.set(QueryParametersUtil.getSearchItems(queryParameters), this._KEY_RELATIVE_LOCATION, ctx.getState().currentFolder);
    }
    ctx.patchState({
      queryParameters: queryParameters,
    });
    return super.getAll<U>(ctx, action);
  }

  @Action(IngestResearchDataFileAction.Move)
  move(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.Move): Observable<ResearchDataFile> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._httpClient.patch<ResearchDataFile>(`${this._urlResource}/${action.dataFile.resId}`, {
      relativeLocation: action.dataFile.relativeLocation,
    } as ResearchDataFile)
      .pipe(
        tap(result => {
          ctx.dispatch(new IngestResearchDataFileAction.MoveSuccess(action, result));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new IngestResearchDataFileAction.MoveFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(IngestResearchDataFileAction.MoveSuccess)
  moveSuccess(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.MoveSuccess): void {
    const relativeLocation = action.parentAction.dataFile.relativeLocation;
    ctx.dispatch(new IngestResearchDataFileAction.ChangeCurrentFolder(relativeLocation, true));
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("notification.researchDataFile.file.move.success"));
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(IngestResearchDataFileAction.MoveFail)
  moveFail(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.MoveFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.researchDataFile.file.move.fail"));
  }

  @Action(IngestResearchDataFileAction.ChangeCurrentFolder)
  changeCurrentFolder(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.ChangeCurrentFolder): void {
    ctx.patchState({
      currentFolder: action.newCurrentFolder,
    });

    const projectId = MemoizedUtil.selectSnapshot(this._store, IngestState, state => state.projectId);
    if (!isNullOrUndefined(action.newCurrentFolder)) {
      ctx.dispatch(new IngestResearchDataFileAction.GetListFolder(projectId));
    }
    if (action.refreshNewFolderContent) {
      let queryParameters = QueryParametersUtil.clone(ctx.getState().queryParameters);
      queryParameters = QueryParametersUtil.resetToFirstPage(queryParameters);
      ctx.dispatch(new IngestResearchDataFileAction.GetAll(projectId, queryParameters));
    }
  }

  @Action(IngestResearchDataFileAction.RefreshTotal)
  refreshTotal(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.RefreshTotal): Observable<number> {
    const queryParameters = new QueryParameters(environment.minimalPageSizeToRetrievePaginationInfo);
    MappingObjectUtil.set(QueryParametersUtil.getSearchItems(queryParameters), IngestHelper.KEY_PROJECT, action.parentId);
    return this._apiService.getCollection<ResearchDataFile>(this._urlResource, queryParameters)
      .pipe(
        map((collection: CollectionTyped<ResearchDataFile>) => {
          const totalItems = collection._page.totalItems;
          ctx.dispatch(new IngestResearchDataFileAction.RefreshTotalSuccess(action, totalItems));
          return totalItems;
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new IngestResearchDataFileAction.RefreshTotalFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(IngestResearchDataFileAction.RefreshTotalSuccess)
  refreshTotalSuccess(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.RefreshTotalSuccess): void {
    ctx.patchState({
      totalCount: action.total,
    });
  }

  @Action(IngestResearchDataFileAction.Reload)
  reload(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.Reload): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<undefined, Result>(`${this._urlResource}/${ApiActionNameEnum.RELOAD}`, undefined, {
      [this._KEY_PROJECT_ID]: action.projectId,
    }).pipe(
      tap(result => {
        if (result?.status === ResultActionStatusEnum.EXECUTED) {
          ctx.dispatch(new IngestResearchDataFileAction.ReloadSuccess(action, result));
        } else {
          ctx.dispatch(new IngestResearchDataFileAction.ReloadFail(action));
        }
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new IngestResearchDataFileAction.ReloadFail(action));
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(IngestResearchDataFileAction.ReloadSuccess)
  reloadSuccess(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.ReloadSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(action.result.message);
    setTimeout(() => {
      ctx.dispatch(new IngestResearchDataFileAction.Refresh(action.parentAction.projectId));
    }, 1000);
  }

  @Action(IngestResearchDataFileAction.ReloadFail)
  reloadFail(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.ReloadFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.researchDataFile.file.reload.fail"));
  }

  @Action(IngestResearchDataFileAction.PutInError)
  putInError(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.PutInError): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<void, Result>(`${this._urlResource}/${action.dataFile.resId}/${ApiActionNameEnum.PUT_IN_ERROR}`, null)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new IngestResearchDataFileAction.PutInErrorSuccess(action, result));
          } else {
            ctx.dispatch(new IngestResearchDataFileAction.PutInErrorFail(action, result));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new IngestResearchDataFileAction.PutInErrorFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(IngestResearchDataFileAction.PutInErrorSuccess)
  putInErrorSuccess(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.PutInErrorSuccess): void {
    this._notificationService.showSuccess(action.result?.message);
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(IngestResearchDataFileAction.PutInErrorFail)
  putInErrorFail(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.PutInErrorFail): void {
    if (isNotNullNorUndefinedNorWhiteString(action.result?.message)) {
      if (action.result.status === ResultActionStatusEnum.NON_APPLICABLE) {
        this._notificationService.showInformation(action.result?.message);
      } else if (action.result.status === ResultActionStatusEnum.NOT_EXECUTED) {
        this._notificationService.showWarning(action.result?.message);
      }
    } else {
      this._notificationService.showError(LabelTranslateEnum.unableToPutInError);
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(IngestResearchDataFileAction.PutInErrorAll)
  putInErrorAll(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.PutInErrorAll): Observable<void> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<string[], void>(`${this._urlResource}/${ApiActionNameEnum.PUT_IN_ERROR}`,
      action.listResId,
    ).pipe(
      tap(result => {
        ctx.dispatch(new IngestResearchDataFileAction.PutInErrorAllSuccess(action));
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new IngestResearchDataFileAction.PutInErrorAllFail(action));
        IngestHelper.displayErrorMessageOnBulkAction(this._notificationService, error, LabelTranslateEnum.unableToPutInErrorAllSelectedElements);
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(IngestResearchDataFileAction.PutInErrorAllSuccess)
  putInErrorAllSuccess(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.PutInErrorAllSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: QueryParametersUtil.resetToFirstPage(ctx.getState().queryParameters),
    });
    setTimeout(() => {
      ctx.dispatch(new IngestResearchDataFileAction.Refresh(this.projectId));
    }, 1000);
  }

  @Action(IngestResearchDataFileAction.PutInErrorAllFail)
  putInErrorAllFail(ctx: SolidifyStateContext<IngestResearchDataFileStateModel>, action: IngestResearchDataFileAction.PutInErrorAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showWarning(LabelTranslateEnum.unableToPutInErrorAllSelectedElements);
    ctx.dispatch(new IngestResearchDataFileAction.Refresh(this.projectId));
  }
}
