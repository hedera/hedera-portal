/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - home.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  NavigationExtras,
  Params,
} from "@angular/router";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {PortalSearch} from "@home/models/portal-search.model";
import {SearchCondition} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {ViewModeTableEnum} from "@shared/enums/view-mode-table.enum";
import {Observable} from "rxjs";
import {
  FacetHelper,
  isEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MappingObject,
  MappingObjectUtil,
  SsrUtil,
  StringUtil,
} from "solidify-frontend";

export class HomeHelper {
  static SEARCH_QUERY_PARAM: string = "search";
  static SEARCH_GLOBAL_PARAM: string = "globalSearch";
  static VIEW_QUERY_PARAM: string = "view";
  static VALUES_SEPARATOR: string = ";";
  static QUERY_PARAM_SEPARATOR: string = "&";
  static QUERY_PARAM_VALUE_AFFECTION: string = "=";
  static QUERY_PARAM_SEARCH_MODE_IS_QUERY: string = "searchAsQuery";

  static LIST_FACET_USE_OPERATOR_AND: Enums.Facet.Name[] = environment.listFacetUseOperatorAnd;

  static getDefaultSearchQueryParams(): any {
    return {[this.SEARCH_QUERY_PARAM]: "*"};
  }

  static navigateToSearch(store: Store, portalSearch: PortalSearch): Observable<any> {
    let queryParams = {} as MappingObject<string, string>;

    if (isNonEmptyString(portalSearch.searchText)) {
      if (portalSearch.searchModeIsQuery) {
        queryParams = {[this.SEARCH_QUERY_PARAM]: SsrUtil.window?.encodeURIComponent(portalSearch.searchText)} as MappingObject<string, string>;
      } else {
        queryParams = {[this.SEARCH_GLOBAL_PARAM]: SsrUtil.window?.encodeURIComponent(portalSearch.searchText)} as MappingObject<string, string>;
      }
    }

    if (portalSearch.searchModeIsQuery) {
      MappingObjectUtil.set(queryParams, this.QUERY_PARAM_SEARCH_MODE_IS_QUERY, "true");
    }

    if (isNotNullNorUndefined(portalSearch.facetsSelected)) {
      MappingObjectUtil.forEach(portalSearch.facetsSelected, ((value, key) => {
        const valueString = value.map(v => SsrUtil.window?.encodeURIComponent(v)).join(this.VALUES_SEPARATOR);
        MappingObjectUtil.set(queryParams, key as string, valueString);
      }));
    }

    let completeSearchString = undefined;
    MappingObjectUtil.forEach(queryParams, ((value: string, key: string) => {
      if (isNotNullNorUndefined(completeSearchString)) {
        completeSearchString = completeSearchString + this.QUERY_PARAM_SEPARATOR;
      } else {
        completeSearchString = StringUtil.stringEmpty;
      }
      completeSearchString = completeSearchString + key + this.QUERY_PARAM_VALUE_AFFECTION + value;
    }));

    const navigationExtras: NavigationExtras = {
      [this.VIEW_QUERY_PARAM]: portalSearch.viewModeTable,
    };
    if (isNotNullNorUndefined(completeSearchString)) {
      navigationExtras[this.SEARCH_QUERY_PARAM] = completeSearchString;
    }

    return store.dispatch(new Navigate([RoutesEnum.homeSearch], navigationExtras));
  }

  static extractSearchInfosFromUrl(params: Params): SearchInfos {
    let searchEncoded = undefined;

    const viewTabMode = params[this.VIEW_QUERY_PARAM];
    const queryParamSearch = params[this.SEARCH_QUERY_PARAM];
    if (!isNullOrUndefined(queryParamSearch)) {
      searchEncoded = queryParamSearch;
    }

    let searchString = StringUtil.stringEmpty;
    const facetsSelected = {} as MappingObject<Enums.Facet.Name, string[]>;
    let searchModeIsQuery: boolean = false;
    if (isNotNullNorUndefined(searchEncoded)) {
      const listParams = searchEncoded.split(this.QUERY_PARAM_SEPARATOR);
      listParams.forEach(p => {
        const splitParam = p.split(this.QUERY_PARAM_VALUE_AFFECTION);
        const decodedValue = SsrUtil.window?.decodeURIComponent(splitParam[1]);
        if (splitParam[0] === this.SEARCH_QUERY_PARAM || splitParam[0] === this.SEARCH_GLOBAL_PARAM) {
          searchString = decodedValue;
        } else if (splitParam[0] === this.QUERY_PARAM_SEARCH_MODE_IS_QUERY) {
          searchModeIsQuery = true;
        } else {
          if (splitParam.length > 1) {
            MappingObjectUtil.set(facetsSelected, splitParam[0], decodedValue.split(this.VALUES_SEPARATOR));
          }
        }
      });
    }

    return {
      search: searchString,
      searchModeIsQuery: searchModeIsQuery,
      facetsSelected: facetsSelected,
      viewTabMode: viewTabMode,
    } as SearchInfos;
  }

  static getFacetsSelectedForBackend(facetsSelected: MappingObject<Enums.Facet.Name, string[]>): SearchCondition[] {
    const facetsSelectedForBackend: SearchCondition[] = [];
    MappingObjectUtil.forEach(facetsSelected, (value, key) => {
      if (isNullOrUndefined(value) || isEmptyArray(value)) {
        return;
      }
      if (this.LIST_FACET_USE_OPERATOR_AND.indexOf(key as Enums.Facet.Name) === -1) {
        // FOR OR, equal value =  [ "orgUnitName1", "orgUnitName2" ]

        const facet = {
          field: key,
          type: Enums.SearchCondition.Type.TERM,
          booleanClauseType: Enums.SearchCondition.BooleanClauseType.MUST,
        } as SearchCondition;

        if (value.length === 1) {
          const v = value[0];
          if (FacetHelper.isExcludedValue(v)) {
            facet.booleanClauseType = Enums.SearchCondition.BooleanClauseType.MUST_NOT;
          }
          facet.value = FacetHelper.cleanExcludedValue(v);
          facetsSelectedForBackend.push(facet);
        } else {
          const selectedValues = [];
          const discardedValues = [];
          value.forEach(v => {
            if (FacetHelper.isExcludedValue(v)) {
              discardedValues.push(FacetHelper.cleanExcludedValue(v));
            } else {
              selectedValues.push(v);
            }
          });

          if (selectedValues.length > 0) {
            const selectedFacet = {
              field: key,
              type: Enums.SearchCondition.Type.TERM,
              booleanClauseType: Enums.SearchCondition.BooleanClauseType.MUST,
              terms: selectedValues,
            } as SearchCondition;
            facetsSelectedForBackend.push(selectedFacet);
          }

          if (discardedValues.length > 0) {
            const discardFacet = {
              field: key,
              type: Enums.SearchCondition.Type.TERM,
              booleanClauseType: Enums.SearchCondition.BooleanClauseType.MUST_NOT,
              terms: discardedValues,
            } as SearchCondition;
            facetsSelectedForBackend.push(discardFacet);
          }
        }
      } else {
        // FOR AND, equal value = "orgUnitName1", need to create a second search facet with "orgUnitName2"
        value.forEach(v => {
          facetsSelectedForBackend.push({
            field: key,
            type: Enums.SearchCondition.Type.TERM,
            value: FacetHelper.cleanExcludedValue(v),
            booleanClauseType: FacetHelper.isExcludedValue(v) ? Enums.SearchCondition.BooleanClauseType.MUST_NOT : Enums.SearchCondition.BooleanClauseType.MUST,
          });
        });
      }
    });
    return facetsSelectedForBackend;
  }
}

interface SearchInfos {
  search: string;
  searchModeIsQuery: boolean;
  facetsSelected: MappingObject<Enums.Facet.Name, string[]>;
  viewTabMode: ViewModeTableEnum;
}
