/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - metadata.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  AbstractResearchElementMetadataResult,
  ResearchDataFileMetadata,
  ResearchDataFileMetadataList,
  ResearchDataFileMetadataResult,
  ResearchObjectMetadata,
  ResearchObjectMetadataResult,
} from "@models";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  MappingObject,
  MappingObjectUtil,
  ObjectUtil,
} from "solidify-frontend";

export class MetadataUtil {
  static XML_KEY: string = "xml";
  static WEB_KEY: string = "web";
  static WIDTH_KEY: string = "width";
  static HEIGHT_KEY: string = "height";
  static LIST_KEY_TO_HIDE: string[] = [this.XML_KEY, this.WEB_KEY, this.WIDTH_KEY, this.HEIGHT_KEY];

  static getResearchElementType(researchElementMetadataResult: AbstractResearchElementMetadataResult): Enums.SearchMetadata.ResearchElementTypeEnum {
    return researchElementMetadataResult.metadata.itemType;
  }

  static isResearchObject(researchElementMetadataResult: AbstractResearchElementMetadataResult): boolean {
    return this.getResearchElementType(researchElementMetadataResult) === Enums.SearchMetadata.ResearchElementTypeEnum.ResearchObject;
  }

  static isResearchDataFile(researchElementMetadataResult: AbstractResearchElementMetadataResult): boolean {
    return this.getResearchElementType(researchElementMetadataResult) === Enums.SearchMetadata.ResearchElementTypeEnum.ResearchDataFile;
  }

  static getResearchObjectResult(researchElementMetadataResult: AbstractResearchElementMetadataResult): ResearchObjectMetadataResult | undefined {
    if (this.isResearchObject(researchElementMetadataResult)) {
      return researchElementMetadataResult as ResearchObjectMetadataResult;
    }
    return undefined;
  }

  static getResearchObject(researchElementMetadataResult: AbstractResearchElementMetadataResult): ResearchObjectMetadata | undefined {
    return this.getResearchObjectResult(researchElementMetadataResult)?.metadata;
  }

  static getResearchDataFileResult(researchElementMetadataResult: AbstractResearchElementMetadataResult): ResearchDataFileMetadataResult | undefined {
    if (this.isResearchDataFile(researchElementMetadataResult)) {
      return researchElementMetadataResult as ResearchDataFileMetadataResult;
    }
    return undefined;
  }

  static getResearchDataFile(researchElementMetadataResult: AbstractResearchElementMetadataResult): ResearchDataFileMetadata | undefined {
    return this.getResearchDataFileResult(researchElementMetadataResult)?.metadata;
  }

  static getXml(researchElementMetadataResult: AbstractResearchElementMetadataResult): string {
    return researchElementMetadataResult.metadata.metadataList.xml;
  }

  static getFileUrl(researchDataFile: ResearchDataFileMetadataResult): string {
    return `${ApiEnum.access}/${ApiResourceNameEnum.PROJECT}/${researchDataFile.metadata.researchProject.shortName}/${ApiResourceNameEnum.RESEARCH_DATA_FILES}${researchDataFile.metadata.relativeLocation}${researchDataFile.metadata.relativeLocation.endsWith("/") ? "" : "/"}${researchDataFile.metadata.fileName}`;
  }

  static getFileDownloadUrl(researchDataFile: ResearchDataFileMetadataResult): string {
    return `${this.getFileUrl(researchDataFile)}/${ApiActionNameEnum.DOWNLOAD}`;
  }

  static getIiifImageUrlForThumbnail(researchDataFile: ResearchDataFileMetadataResult): string {
    return `${ApiEnum.iiif}/${environment.iiifProtocolVersion}/${researchDataFile?.metadata?.researchProject?.shortName}${researchDataFile.metadata.relativeLocation}${researchDataFile.metadata.relativeLocation.endsWith("/") ? "" : "/"}${researchDataFile.metadata.fileName}/full/^!800,800/0/default.jpg`;
  }

  /**
   * Could be iiif link
   * @param researchElementMetadataResult
   */
  static getWebUrl(researchElementMetadataResult: AbstractResearchElementMetadataResult): string {
    return (researchElementMetadataResult.metadata.metadataList as ResearchDataFileMetadataList)?.web;
  }

  static getMetadataWithoutKeyToHide(researchElementMetadataResult: AbstractResearchElementMetadataResult): MappingObject<string, string> {
    const metadata = ObjectUtil.clone(researchElementMetadataResult.metadata.metadataList);
    this.LIST_KEY_TO_HIDE.forEach(key => {
      MappingObjectUtil.delete(metadata, key);
    });
    return metadata;
  }

  static getSize(researchElementMetadataResult: AbstractResearchElementMetadataResult): number | undefined {
    if (researchElementMetadataResult.metadata.itemType === Enums.SearchMetadata.ResearchElementTypeEnum.ResearchDataFile) {
      return this.getResearchDataFile(researchElementMetadataResult).fileSize;
    }
    return undefined;
  }
}
