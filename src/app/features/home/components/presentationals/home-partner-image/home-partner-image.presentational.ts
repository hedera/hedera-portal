/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - home-partner-image.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {isNotNullNorUndefined} from "solidify-frontend";

@Component({
  selector: "hedera-home-partner-image",
  templateUrl: "./home-partner-image.presentational.html",
  styleUrls: ["./home-partner-image.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomePartnerImagePresentational extends SharedAbstractPresentational {
  @Input()
  image: ImageInfos;

  preventDefault($event: MouseEvent, url: string): void {
    if (isNotNullNorUndefined(url)) {
      return;
    }
    $event.preventDefault();
  }
}

export interface ImageInfos {
  imageName: string;
  altText: string;
  url: string;
}
