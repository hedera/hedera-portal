/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - home-main-page-content.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  ChangeDetectionStrategy,
  Component,
  Output,
} from "@angular/core";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {ObservableUtil} from "solidify-frontend";

@Component({
  selector: "hedera-home-main-page-content",
  templateUrl: "./home-main-page-content.presentational.html",
  styleUrls: ["./home-main-page-content.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeMainPageContentPresentational extends SharedAbstractPresentational {
  private readonly _goToSearchBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("goToSearch")
  readonly goToSearchObs: Observable<void> = ObservableUtil.asObservable(this._goToSearchBS);

  private readonly _goToDatasetOrLoginBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("goToDatasetOrLogin")
  readonly goToDatasetOrLoginObs: Observable<void> = ObservableUtil.asObservable(this._goToDatasetOrLoginBS);

  goToDataset(): void {
    this._goToDatasetOrLoginBS.next();
  }

  goToSearchResult(): void {
    this._goToSearchBS.next();
  }
}
