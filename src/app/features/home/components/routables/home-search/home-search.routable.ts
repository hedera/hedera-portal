/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - home-search.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {MatSlideToggleChange} from "@angular/material/slide-toggle";
import {
  ActivatedRoute,
  Params,
} from "@angular/router";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {
  AppRoutesEnum,
  RoutesEnum,
} from "@app/shared/enums/routes.enum";
import {AppState} from "@app/stores/app.state";
import {Enums} from "@enums";
import {HomeSearchHelpDialog} from "@home/components/dialogs/home-search-help/home-search-help.dialog";
import {HomeHelper} from "@home/helpers/home.helper";
import {PortalSearch} from "@home/models/portal-search.model";
import {
  AbstractResearchElementMetadataResult,
  SystemProperty,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {TourEnum} from "@shared/enums/tour.enum";
import {ViewModeTableEnum} from "@shared/enums/view-mode-table.enum";
import {ResearchObjectMetadataResultHelper} from "@shared/helpers/research-object-metadata-result.helper";
import {HomeAction} from "@shared/stores/home/home.action";
import {HomeState} from "@shared/stores/home/home.state";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {
  AppSystemPropertyState,
  DataTableActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DialogUtil,
  ExtendEnum,
  Facet,
  FacetNamePartialEnum,
  isTrue,
  LoginMode,
  MappingObject,
  MemoizedUtil,
  OAuth2Service,
  OrderEnum,
  Paging,
  QueryParameters,
  QueryParametersUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-home-search-routable",
  templateUrl: "./home-search.routable.html",
  styleUrls: ["./home-search.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeSearchRoutable extends SharedAbstractPresentational implements OnInit, OnDestroy {
  searchObs: Observable<string> = MemoizedUtil.select(this._store, HomeState, state => state.search);
  searchModeIsQueryObs: Observable<boolean> = MemoizedUtil.select(this._store, HomeState, state => state.searchModeIsQuery);
  facetsSelectedObs: Observable<MappingObject<Enums.Facet.Name, string[]>> = MemoizedUtil.select(this._store, HomeState, state => state.facetsSelected);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, HomeState, state => state.queryParameters);
  viewModeTableEnumObs: Observable<ViewModeTableEnum> = MemoizedUtil.select(this._store, HomeState, state => state.viewModeTableEnum);
  totalObs: Observable<number> = MemoizedUtil.select(this._store, HomeState, state => state.total);
  listObs: Observable<AbstractResearchElementMetadataResult[]> = MemoizedUtil.select(this._store, HomeState, state => state.list);
  facetsObs: Observable<Facet[]> = MemoizedUtil.select(this._store, HomeState, state => state.facets);
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, HomeState);
  systemPropertyObs: Observable<SystemProperty> = MemoizedUtil.select(this._store, AppSystemPropertyState, state => state.current);
  isInTourModeObs: Observable<boolean> = MemoizedUtil.select(this._store, AppState, state => state.isInTourMode);
  isLoggedInObs: Observable<boolean> = MemoizedUtil.select(this._store, AppState, state => state.isLoggedIn);
  isFacetClosed: boolean = true;

  get viewModeTableEnum(): typeof ViewModeTableEnum {
    return ViewModeTableEnum;
  }

  get tourEnum(): typeof TourEnum {
    return TourEnum;
  }

  columns: DataTableColumns[] = [...ResearchObjectMetadataResultHelper.columns,
    {
      field: "metadata.itemType",
      header: LabelTranslateEnum.type,
      type: DataTableFieldTypeEnum.string,
      order: OrderEnum.none,
      filterEnum: Enums.SearchMetadata.ResearchElementMetadataEnumTranslate,
      translate: true,
      isSortable: true,
      sortableField: "item-types" as any,
      isFilterable: false,
    },
  ];

  actions: DataTableActions<AbstractResearchElementMetadataResult>[] = [
    // {
    //   logo: IconNameEnum.download,
    //   callback: current => this.download(current),
    //   placeholder: current => LabelTranslateEnum.download,
    //   disableCondition: current => false,
    //   displayOnCondition: current => MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isLoggedIn)/* || current.accessLevel === Enums.Access.AccessEnum.PUBLIC*/,
    // },
  ];

  constructor(private readonly _store: Store,
              private readonly _translate: TranslateService,
              private readonly _route: ActivatedRoute,
              private readonly _actions$: Actions,
              private readonly _oauthService: OAuth2Service,
              protected readonly _dialog: MatDialog,
  ) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribe(this._observeSearchQueryParam());
  }

  private _observeSearchQueryParam(): Observable<Params> {
    return this._route.queryParams.pipe(tap(params => {
      const searchInfos = HomeHelper.extractSearchInfosFromUrl(params);
      this._store.dispatch(new HomeAction.Search(true, searchInfos.search, searchInfos.searchModeIsQuery, searchInfos.facetsSelected, searchInfos.viewTabMode));
    }));
  }

  private _requestAccessDataset(researchObjectMetadataResult: AbstractResearchElementMetadataResult): void {
  }

  searchText(search: string): void {
    const facetsSelected = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.facetsSelected);
    const viewMode = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.viewModeTableEnum);
    const searchModeIsQuery: boolean = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.searchModeIsQuery);
    this._search(search, searchModeIsQuery, facetsSelected, viewMode);
  }

  searchFacet(facetsSelected: MappingObject<ExtendEnum<FacetNamePartialEnum>, string[]>): void {
    const search = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.search);
    const viewMode = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.viewModeTableEnum);
    const searchModeIsQuery: boolean = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.searchModeIsQuery);
    this._search(search, searchModeIsQuery, facetsSelected, viewMode);
  }

  searchQueryToggle(searchModeIsQuery: boolean): void {
    const search = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.search);
    const facetsSelected = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.facetsSelected);
    const viewMode = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.viewModeTableEnum);
    this._search(search, searchModeIsQuery, facetsSelected, viewMode);
  }

  switchViewMode(viewMode: ViewModeTableEnum): void {
    const search = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.search);
    const facetsSelected = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.facetsSelected);
    const searchModeIsQuery: boolean = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.searchModeIsQuery);
    this._search(search, searchModeIsQuery, facetsSelected, viewMode);
  }

  searchModeChange($event: MatSlideToggleChange): void {
    const searchModeIsQuery: boolean = isTrue($event.checked);
    this.searchQueryToggle(searchModeIsQuery);
  }

  private _search(searchTerm: string, searchModeIsQuery: boolean, facetsSelected: MappingObject<ExtendEnum<FacetNamePartialEnum>, string[]>, viewModeTableEnum: ViewModeTableEnum = ViewModeTableEnum.list): void {
    const portalSearch = {
      searchText: searchTerm,
      searchModeIsQuery: searchModeIsQuery,
      facetsSelected: facetsSelected,
      viewModeTable: viewModeTableEnum,
    } as PortalSearch;

    HomeHelper.navigateToSearch(this._store, portalSearch);
  }

  onQueryParametersEvent($event: QueryParameters): void {
    this._store.dispatch(new HomeAction.ChangeQueryParameters($event));
  }

  showDetail(researchObjectMetadataResult: AbstractResearchElementMetadataResult): void {
    this._store.dispatch(new Navigate([RoutesEnum.homeDetail, researchObjectMetadataResult.resId]));
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  download(researchObjectMetadataResult: AbstractResearchElementMetadataResult): void {
  }

  addToCart(researchObjectMetadataResult: AbstractResearchElementMetadataResult): void {
  }

  navigate($event: RoutesEnum): void {
    this._store.dispatch(new Navigate([$event]));
  }

  goToDataset(): void {
    const isLogged = MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isLoggedIn);
    if (isLogged) {
      this._store.dispatch(new Navigate([RoutesEnum.ingest]));
    } else {
      this._oauthService.initAuthorizationCodeFlow(LoginMode.STANDARD, AppRoutesEnum.ingest);
    }
  }

  pageChange(paging: Paging): void {
    const queryParameters = QueryParametersUtil.clone(MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.queryParameters));
    queryParameters.paging = paging;
    this._store.dispatch(new HomeAction.ChangeQueryParameters(queryParameters));
  }

  openHelp(): void {
    DialogUtil.open(this._dialog, HomeSearchHelpDialog);
  }
}
