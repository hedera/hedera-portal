/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - home-page.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {
  AppRoutesEnum,
  RoutesEnum,
} from "@app/shared/enums/routes.enum";
import {AppState} from "@app/stores/app.state";
import {environment} from "@environments/environment";
import {HomeHelper} from "@home/helpers/home.helper";
import {PortalSearch} from "@home/models/portal-search.model";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {ThemeEnum} from "@shared/enums/theme.enum";
import {TourEnum} from "@shared/enums/tour.enum";
import {Observable} from "rxjs";
import {
  LoginMode,
  MemoizedUtil,
  OAuth2Service,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-home-page-routable",
  templateUrl: "./home-page.routable.html",
  styleUrls: ["./home-page.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomePageRoutable extends SharedAbstractPresentational implements OnInit, OnDestroy {
  themeObs: Observable<ThemeEnum> = MemoizedUtil.select(this._store, AppState, state => state.theme);
  searchValueInUrl: string = StringUtil.stringEmpty;
  twitterEnabled: boolean = environment.twitterEnabled;

  get backgroundImage(): string {
    return `url("./assets/themes/${environment.theme}/home-image.jpg")`;
  }

  get currentThemeName(): string {
    return StringUtil.convertToPascalCase(environment.theme);
  }

  constructor(private readonly _store: Store,
              private readonly _translate: TranslateService,
              private readonly _route: ActivatedRoute,
              private readonly _actions$: Actions,
              private readonly _oauthService: OAuth2Service) {
    super();
  }

  get tourEnum(): typeof TourEnum {
    return TourEnum;
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  search(searchTerm: string): void {
    const portalSearch = {
      searchText: searchTerm,
    } as PortalSearch;
    HomeHelper.navigateToSearch(this._store, portalSearch);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  navigate($event: RoutesEnum): void {
    this._store.dispatch(new Navigate([$event]));
  }

  goToDataset(): void {
    const isLogged = MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isLoggedIn);
    if (isLogged) {
      this._store.dispatch(new Navigate([RoutesEnum.ingest]));
    } else {
      this._oauthService.initAuthorizationCodeFlow(LoginMode.STANDARD, AppRoutesEnum.ingest);
    }
  }

  getToSearch(): void {
    this._store.dispatch(new Navigate([RoutesEnum.homeSearch], HomeHelper.getDefaultSearchQueryParams()));
  }
}
