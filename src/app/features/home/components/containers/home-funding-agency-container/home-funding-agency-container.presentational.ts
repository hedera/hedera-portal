/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - home-funding-agency-container.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from "@angular/core";
import {
  ArchiveFundingAgency,
  FundingAgency,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractContainer} from "@shared/components/containers/shared-abstract/shared-abstract.container";
import {SharedFundingAgencyOverlayPresentational} from "@shared/components/presentationals/shared-funding-agency-overlay/shared-funding-agency-overlay.presentational";
import {SharedFundingAgencyAction} from "@shared/stores/funding-agency/shared-funding-agency.action";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
  StoreUtil,
  Type,
} from "solidify-frontend";

@Component({
  selector: "hedera-home-funding-agency-container",
  templateUrl: "./home-funding-agency-container.presentational.html",
  styleUrls: ["./home-funding-agency-container.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeFundingAgencyContainer extends SharedAbstractContainer {
  private _archiveFundingAgency: ArchiveFundingAgency;

  @Input()
  set archiveFundingAgency(value: ArchiveFundingAgency) {
    this._archiveFundingAgency = value;
    this._retrieveFundingAgency();
  }

  get archiveFundingAgency(): ArchiveFundingAgency {
    return this._archiveFundingAgency;
  }

  private _fundingAgency: FundingAgency | undefined;

  private _retrieveFundingAgency(): void {
    this._fundingAgency = undefined;

    if (isNullOrUndefined(this.archiveFundingAgency) || isNullOrUndefined(this.archiveFundingAgency.identifier)) {
      return;
    }

    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new SharedFundingAgencyAction.GetByIdentifierId(this.archiveFundingAgency.identifier),
      SharedFundingAgencyAction.GetByIdentifierIdSuccess,
      result => {
        this._fundingAgency = result.fundingAgency;
        this._changeDetector.detectChanges();
      },
      SharedFundingAgencyAction.GetByIdentifierIdFail,
      result => {
        this._fundingAgency = undefined;
        this._changeDetector.detectChanges();
      }));
  }

  get fundingAgency(): FundingAgency {
    if (isNotNullNorUndefined(this._fundingAgency)) {
      return this._fundingAgency;
    }
    return {
      name: this.archiveFundingAgency.name,
      url: this.archiveFundingAgency.identifierUrl,
    };
  }

  fundingAgencyOverlayComponent: Type<SharedFundingAgencyOverlayPresentational> = SharedFundingAgencyOverlayPresentational;

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }
}
