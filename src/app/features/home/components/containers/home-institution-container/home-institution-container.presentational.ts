/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - home-institution-container.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from "@angular/core";
import {
  ArchiveInstitution,
  Institution,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractContainer} from "@shared/components/containers/shared-abstract/shared-abstract.container";
import {SharedInstitutionOverlayPresentational} from "@shared/components/presentationals/shared-institution-overlay/shared-institution-overlay.presentational";
import {SharedInstitutionAction} from "@shared/stores/institution/shared-institution.action";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
  StoreUtil,
  Type,
} from "solidify-frontend";

@Component({
  selector: "hedera-home-institution-container",
  templateUrl: "./home-institution-container.presentational.html",
  styleUrls: ["./home-institution-container.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeInstitutionContainer extends SharedAbstractContainer {
  private _archiveInstitution: ArchiveInstitution;

  @Input()
  set archiveInstitution(value: ArchiveInstitution) {
    this._archiveInstitution = value;
    this._retrieveInstitution();
  }

  get archiveInstitution(): ArchiveInstitution {
    return this._archiveInstitution;
  }

  private _institution: Institution | undefined;

  private _retrieveInstitution(): void {
    this._institution = undefined;

    if (isNullOrUndefined(this.archiveInstitution) || isNullOrUndefined(this.archiveInstitution.identifier)) {
      return;
    }

    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new SharedInstitutionAction.GetByIdentifierId(this.archiveInstitution.identifier),
      SharedInstitutionAction.GetByIdentifierIdSuccess,
      result => {
        this._institution = result.institution;
        this._changeDetector.detectChanges();
      },
      SharedInstitutionAction.GetByIdentifierIdFail,
      result => {
        this._institution = undefined;
        this._changeDetector.detectChanges();
      }));
  }

  get institution(): Institution {
    if (isNotNullNorUndefined(this._institution)) {
      return this._institution;
    }
    return {
      name: this.archiveInstitution.name,
      url: this.archiveInstitution.identifierUrl,
    };
  }

  institutionOverlayComponent: Type<SharedInstitutionOverlayPresentational> = SharedInstitutionOverlayPresentational;

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }
}
