/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - home-license-container.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from "@angular/core";
import {
  ArchiveLicense,
  License,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractContainer} from "@shared/components/containers/shared-abstract/shared-abstract.container";
import {SharedLicenseOverlayPresentational} from "@shared/components/presentationals/shared-license-overlay/shared-license-overlay.presentational";
import {SharedLicenseAction} from "@shared/stores/license/shared-license.action";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
  StoreUtil,
  Type,
} from "solidify-frontend";

@Component({
  selector: "hedera-home-license-container",
  templateUrl: "./home-license-container.presentational.html",
  styleUrls: ["./home-license-container.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeLicenseContainer extends SharedAbstractContainer {
  private _archiveLicense: ArchiveLicense;

  @Input()
  set archiveLicense(value: ArchiveLicense) {
    this._archiveLicense = value;
    this._retrieveLicense();
  }

  get archiveLicense(): ArchiveLicense {
    return this._archiveLicense;
  }

  private _license: License | undefined;

  private _retrieveLicense(): void {
    this._license = undefined;

    if (isNullOrUndefined(this.archiveLicense) || isNullOrUndefined(this.archiveLicense.identifier)) {
      return;
    }

    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new SharedLicenseAction.GetByIdentifierId(this.archiveLicense.identifier),
      SharedLicenseAction.GetByIdentifierIdSuccess,
      result => {
        this._license = result.license;
        this._changeDetector.detectChanges();
      },
      SharedLicenseAction.GetByIdentifierIdFail,
      result => {
        this._license = undefined;
        this._changeDetector.detectChanges();
      }));
  }

  get license(): License {
    if (isNotNullNorUndefined(this._license)) {
      return this._license;
    }
    return {
      title: this.archiveLicense.name,
      url: this.archiveLicense.url,
    };
  }

  licenseOverlayComponent: Type<SharedLicenseOverlayPresentational> = SharedLicenseOverlayPresentational;

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }
}
