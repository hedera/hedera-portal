/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - home-search-help.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {
  FormValidationHelper,
  MARK_AS_TRANSLATABLE,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "hedera-home-search-help-dialog",
  templateUrl: "./home-search-help.dialog.html",
  styleUrls: ["./home-search-help.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeSearchHelpDialog extends SharedAbstractDialog<HomeSearchHelpDialogData, string> implements OnInit {
  form: FormGroup;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  helpInfo: HelpInfo[] = [
    {
      character: "*",
      explain: MARK_AS_TRANSLATABLE("home.search.help.asterisk.explain"),
      example: MARK_AS_TRANSLATABLE("home.search.help.asterisk.example"),
    },
    {
      character: "?",
      explain: MARK_AS_TRANSLATABLE("home.search.help.questionMark.explain"),
      example: MARK_AS_TRANSLATABLE("home.search.help.questionMark.example"),
    },
    {
      character: "AND",
      explain: MARK_AS_TRANSLATABLE("home.search.help.and.explain"),
      example: MARK_AS_TRANSLATABLE("home.search.help.and.example"),
    },
    {
      character: "\"...\"",
      explain: MARK_AS_TRANSLATABLE("home.search.help.quote.explain"),
      example: MARK_AS_TRANSLATABLE("home.search.help.quote.example"),
    },
  ];

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              protected readonly _dialogRef: MatDialogRef<HomeSearchHelpDialog>,
              private readonly _fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public readonly data: HomeSearchHelpDialogData) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.form = this._fb.group({
      [this.formDefinition.message]: ["", [Validators.required, SolidifyValidator]],
    });
  }

  onSubmit(): void {
    this.submit(this.form.get(this.formDefinition.message).value);
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() message: string;
}

export interface HomeSearchHelpDialogData {
}

interface HelpInfo {
  character: string;
  explain: string;
  example: string;
}
