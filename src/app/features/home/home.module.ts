/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - home.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgIf} from "@angular/common";
import {NgModule} from "@angular/core";
import {HomeRoutingModule} from "@app/features/home/home-routing.module";
import {SharedModule} from "@app/shared/shared.module";
import {HomeFundingAgencyContainer} from "@home/components/containers/home-funding-agency-container/home-funding-agency-container.presentational";
import {HomeInstitutionContainer} from "@home/components/containers/home-institution-container/home-institution-container.presentational";
import {HomeLicenseContainer} from "@home/components/containers/home-license-container/home-license-container.presentational";
import {HomeSearchHelpDialog} from "@home/components/dialogs/home-search-help/home-search-help.dialog";
import {HomeMainButtonPresentational} from "@home/components/presentationals/home-main-button/home-main-button.presentational";
import {HomeMainPageContentPresentational} from "@home/components/presentationals/home-main-page-content/home-main-page-content.presentational";
import {HomePartnerImagePresentational} from "@home/components/presentationals/home-partner-image/home-partner-image.presentational";
import {HomePartnersPresentational} from "@home/components/presentationals/home-partners/home-partners.presentational";
import {HomeSearchBarPresentational} from "@home/components/presentationals/home-search-bar/home-search-bar.presentational";
import {HomePageRoutable} from "@home/components/routables/home-page/home-page.routable";
import {HomeSearchRoutable} from "@home/components/routables/home-search/home-search.routable";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {SolidifyFrontendSearchModule} from "solidify-frontend";

const routables = [
  HomePageRoutable,
  HomeSearchRoutable,
];
const dialogs = [
  HomeSearchHelpDialog,
];
const containers = [
  HomeLicenseContainer,
  HomeInstitutionContainer,
  HomeFundingAgencyContainer,
];
const presentationals = [
  HomeSearchBarPresentational,
  HomeMainPageContentPresentational,
  HomePartnersPresentational,
  HomePartnerImagePresentational,
  HomeMainButtonPresentational,
];
const services = [];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...presentationals,
    ...dialogs,
  ],
  imports: [
    SharedModule,
    HomeRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([]),
    SolidifyFrontendSearchModule,
    NgIf,
  ],
  exports: [
    ...routables,
  ],
  providers: [
    ...services,
  ],
})
export class HomeModule {
}
