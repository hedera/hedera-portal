/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - home-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {
  AppRoutesEnum,
  HomePageRoutesEnum,
} from "@app/shared/enums/routes.enum";
import {HederaRoutes} from "@app/shared/models/hedera-route.model";
import {HomePageRoutable} from "@home/components/routables/home-page/home-page.routable";
import {HomeSearchRoutable} from "@home/components/routables/home-search/home-search.routable";
import {SharedResearchElementDetailRoutable} from "@shared/components/routables/shared-research-element-detail/shared-research-element-detail.routable";
import {SharedResearchObjectNotFoundRoutable} from "@shared/components/routables/shared-research-object-not-found/shared-research-object-not-found.routable";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {ResearchElementGuardService} from "@shared/guards/research-element-guard.service";
import {HomeState} from "@shared/stores/home/home.state";
import {CombinedGuardService} from "solidify-frontend";

const routes: HederaRoutes = [
  {
    path: "",
    component: HomePageRoutable,
    data: {},
  },
  {
    path: HomePageRoutesEnum.search,
    component: HomeSearchRoutable,
  },
  {
    path: HomePageRoutesEnum.detail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: SharedResearchElementDetailRoutable,
    data: {
      breadcrumbMemoizedSelector: HomeState.currentTitle,
      guards: [ResearchElementGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: HomePageRoutesEnum.notFound + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: SharedResearchObjectNotFoundRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.researchObjectNotFound,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {
}
