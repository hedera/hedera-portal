/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - twitter.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {DOCUMENT} from "@angular/common";
import {
  Inject,
  Injectable,
  Renderer2,
} from "@angular/core";
import {WINDOW} from "@app/app.module";
import {TwitterMetadata} from "@app/features/home/models/twitter-metadata.model";
import {Observable} from "rxjs";
import {AbstractBaseService} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class TwitterService extends AbstractBaseService {
  private readonly _TWITTER_SCRIPT_ID: string = "twitter-wjs";
  private readonly _TWITTER_WIDGET_URL: string = "https://platform.twitter.com/widgets.js";
  private readonly _SCRIPT: string = "script";
  private readonly _ATTRIBUTE_ID: string = "id";
  private readonly _ATTRIBUTE_SRC: string = "src";
  readonly TWITTER_WINDOW_ID: string = "twttr";

  constructor(@Inject(DOCUMENT) private readonly _document: Document,
              @Inject(WINDOW) private readonly _window: Window) {
    super();
  }

  loadScript(renderer: Renderer2): Observable<TwitterMetadata> {
    return new Observable(observer => {
      const twitterMetadata = this._initTwitterComponent(renderer);
      twitterMetadata.ready((metadata: TwitterMetadata) => {
        observer.next(metadata);
        observer.complete();
      });
    });
  }

  private _initTwitterComponent(renderer: Renderer2): TwitterMetadata {
    if (this._getTwitterScript()) {
      return this._window[this.TWITTER_WINDOW_ID] || {};
    }

    const scriptTwitter = this._createScript(renderer);
    const firstScriptTag = this._getFirstScriptTag();
    renderer.insertBefore(firstScriptTag.parentNode, scriptTwitter, firstScriptTag);

    return this._initTwitterMetadata();
  }

  private _createScript(renderer: Renderer2): Element {
    const script = renderer.createElement(this._SCRIPT);
    renderer.setAttribute(script, this._ATTRIBUTE_ID, this._TWITTER_SCRIPT_ID);
    renderer.setAttribute(script, this._ATTRIBUTE_SRC, this._TWITTER_WIDGET_URL);
    return script;
  }

  private _getFirstScriptTag(): Element {
    return this._document.getElementsByTagName(this._SCRIPT)[0];
  }

  private _getTwitterScript(): HTMLElement {
    return this._document.getElementById(this._TWITTER_SCRIPT_ID);
  }

  private _initTwitterMetadata(): TwitterMetadata {
    const twitterMetadata: TwitterMetadata = {};
    twitterMetadata._e = [];
    twitterMetadata.ready = (metadata: TwitterMetadata) => twitterMetadata._e.push(metadata);
    this._window[this.TWITTER_WINDOW_ID] = twitterMetadata;
    return twitterMetadata;
  }
}


