/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - sparql.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgSwitchCase} from "@angular/common";
import {NgModule} from "@angular/core";
import {MatIconModule} from "@angular/material/icon";
import {YasguiPresentational} from "@app/features/sparql/components/presentationals/yasgui/yasgui.presentational";
import {SparqlQueryRoutable} from "@app/features/sparql/components/routables/sparql-home/sparql-query.routable";
import {SparqlRoutingModule} from "@app/features/sparql/sparql-routing.module";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {ModuleLoadedEnum} from "@shared/enums/module-loaded.enum";
import {SsrUtil} from "solidify-frontend";

const routables = [
  SparqlQueryRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  YasguiPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    SparqlRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([]),
    MatIconModule,
    NgSwitchCase,
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class SparqlModule {
  constructor() {
    if (SsrUtil.window) {
      SsrUtil.window[ModuleLoadedEnum.sparqlModuleLoaded] = true;
    }
  }
}
