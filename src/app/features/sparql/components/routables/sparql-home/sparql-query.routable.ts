/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - sparql-query.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {Project} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {ApiEnum} from "@shared/enums/api.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {
  SharedProjectAction,
  sharedProjectActionNameSpace,
} from "@shared/stores/project/shared-project.action";
import {SharedProjectState} from "@shared/stores/project/shared-project.state";
import {HederaUserPreferencesUtil} from "@shared/utils/hedera-user-preferences.util";
import {pipe} from "rxjs";
import {
  take,
  tap,
} from "rxjs/operators";
import {
  CookieConsentService,
  CookieConsentUtil,
  CookieType,
  FormValidationHelper,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  KeyValue,
  OrderEnum,
  PropertyName,
  ResourceNameSpace,
  SolidifyValidator,
  Sort,
  SsrUtil,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-sparql-query-routable",
  templateUrl: "./sparql-query.routable.html",
  styleUrls: ["./sparql-query.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SparqlQueryRoutable extends SharedAbstractRoutable {
  readonly FORMAT_DISABLED: Enums.Rml.RmlFormatEnum[] = [Enums.Rml.RmlFormatEnum.SQL];
  readonly INITIAL_QUERY: string = `
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    SELECT * WHERE { ?sub ?pred ?obj . }
    LIMIT 10`;

  endpoint: string = undefined;

  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  form: FormGroup;

  rmlFormatEnum: KeyValue[] = Enums.Rml.RmlFormatEnumTranslate;

  sharedProjectSort: Sort = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedProjectNameSpace: ResourceNameSpace = sharedProjectActionNameSpace;
  sharedProjectState: typeof SharedProjectState = SharedProjectState;

  lastSelectionProjectLocalStorageKey: LocalStorageEnum = undefined;

  isNoProject: boolean = false;

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  constructor(private readonly _store: Store,
              private readonly _fb: FormBuilder,
              private readonly _cookieConsentService: CookieConsentService,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _actions$: Actions,
              private readonly _translate: TranslateService) {
    super();

    if (SsrUtil.isBrowser) {
      this.subscribe(this._cookieConsentService.watchCookiePreferenceUpdateObs(CookieType.localStorage, LocalStorageEnum.singleSelectLastSelectionProject)
        .pipe(
          tap(p => {
            this._setLastSelectionProjectKeyIfFeatureEnabled();
            this._changeDetector.detectChanges();
          }),
        ));
      this._setLastSelectionProjectKeyIfFeatureEnabled();
    }

    this.form = this._fb.group({
      [this.formDefinition.projectId]: [undefined, [Validators.required, SolidifyValidator]],
      [this.formDefinition.projectShortName]: [undefined, [Validators.required, SolidifyValidator]],
      [this.formDefinition.format]: [Enums.Rml.RmlFormatEnum.JSON, [Validators.required, SolidifyValidator]],
    });

    this.subscribe(this.form.get(this.formDefinition.projectShortName).valueChanges.pipe(
      SsrUtil.isServer ? take(1) : pipe(),
      tap(projectShortName => {
        if (isNullOrUndefinedOrWhiteString(projectShortName)) {
          this.endpoint = undefined;
        } else {
          this.endpoint = `${ApiEnum.sparqlQuery}/${projectShortName}`;
        }
        this._changeDetector.detectChanges();
      })));

    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new SharedProjectAction.GetAll(),
      SharedProjectAction.GetAllSuccess,
      result => {
        const defaultProject = HederaUserPreferencesUtil.getCurrentProject(result.list?._data);
        if (isNullOrUndefined(defaultProject)) {
          this.isNoProject = true;
        } else {
          this.form.get(this.formDefinition.projectId).setValue(defaultProject?.resId);
          this.form.get(this.formDefinition.projectShortName).setValue(defaultProject?.shortName);
        }
        this._changeDetector.detectChanges();
      },
      SharedProjectAction.GetAllSuccess,
      () => {
        this.isNoProject = true;
      }));
  }

  private _setLastSelectionProjectKeyIfFeatureEnabled(): void {
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.singleSelectLastSelectionProject)) {
      this.lastSelectionProjectLocalStorageKey = LocalStorageEnum.singleSelectLastSelectionProject;
    } else {
      this.lastSelectionProjectLocalStorageKey = undefined;
    }
  }

  projectChange(project: Project): void {
    HederaUserPreferencesUtil.setCurrentProject(project.resId);
    this.form.get(this.formDefinition.projectShortName).setValue(project?.shortName);
  }

  openSparqlHelp(): void {
    SsrUtil.window.open(environment.sparqlHelpLink, "_blank");
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() projectId: string;
  @PropertyName() projectShortName: string;
  @PropertyName() format: string;
}
