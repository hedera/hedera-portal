/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - yasgui.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
} from "@angular/core";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {Store} from "@ngxs/store";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {SecurityService} from "@shared/services/security.service";
import Yasgui from "@triply/yasgui";
import Tab from "@triply/yasgui/build/ts/src/Tab";
import {tap} from "rxjs/operators";
import {
  ClipboardUtil,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  MappingObject,
  MappingObjectUtil,
  MemoizedUtil,
  NotificationService,
  OAuth2Service,
  SOLIDIFY_CONSTANTS,
  SsrUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-yasgui",
  templateUrl: "./yasgui.presentational.html",
  styleUrls: ["./yasgui.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class YasguiPresentational extends SharedAbstractPresentational implements OnInit, AfterViewInit {
  readonly YASGUI_ID: string = "yasgui";
  private readonly _MAIN_TAB_NAME: string = "main";
  private readonly _MAIN_TAB_ID: string = "main-tab";

  private _yasgui: Yasgui;

  private _endpoint: string;
  private _format: Enums.Rml.RmlFormatEnum = Enums.Rml.RmlFormatEnum.JSON;

  @Input()
  set endpoint(value: string) {
    this._endpoint = value;
    this._initTabs(value);
  }

  @Input()
  set format(format: Enums.Rml.RmlFormatEnum) {
    this._format = format;
    this._tab?.setRequestConfig({
      acceptHeaderSelect: this._getAcceptHeaderValue(this._format),
    });
  }

  @Input()
  initialQuery: string;

  private _tab: Tab;

  get isDarkModeEnable(): boolean {
    return MemoizedUtil.selectSnapshot(this._store, environment.appState, state => state.darkMode);
  }

  constructor(private readonly _changeDetector: ChangeDetectorRef,
              private readonly _store: Store,
              private readonly _oAuth2Service: OAuth2Service,
              private readonly _securityService: SecurityService,
              private readonly _notificationService: NotificationService) {
    super();
  }

  setTheme(): void {
    if (isNullOrUndefined(this._tab)) {
      return;
    }
    const theme = this.isDarkModeEnable ? environment.codeEditorThemeDark : environment.codeEditorThemeLight;
    this._tab?.getYasqe().setOption("theme" as any, theme as any);
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();

    const yarguiElement = SsrUtil.document.getElementById(this.YASGUI_ID);
    this._yasgui = new Yasgui(yarguiElement, {});
    this._initTabs(this._endpoint);

    this._changeDetector.detectChanges();

    this.subscribe(MemoizedUtil.select(this._store, environment.appState, state => state.darkMode).pipe(
      tap(() => this.setTheme()),
    ));
  }

  private _deleteAllTab(): void {
    MappingObjectUtil.keys(this._yasgui._tabs).forEach(tabId => {
      this._yasgui.getTab(tabId).close();
    });
  }

  private _initTabs(endpoint: string): void {
    if (isNullOrUndefined(this._yasgui)) {
      return;
    }
    let query = this._yasgui.getTab(this._MAIN_TAB_ID)?.getQuery();
    this._deleteAllTab();
    if (isNullOrUndefinedOrWhiteString(endpoint)) {
      return;
    }
    const headers: MappingObject<string, string> = {};
    if (this._securityService.isLoggedIn()) {
      const accessToken = this._oAuth2Service.getAccessToken();
      if (isNotNullNorUndefinedNorWhiteString(accessToken)) {
        MappingObjectUtil.set(headers, "Authorization", "Bearer " + accessToken);
      }
    }
    this._tab = this._yasgui.addTab(true, {
      ...Yasgui.Tab.getDefaults(),
      name: this._MAIN_TAB_NAME,
      id: this._MAIN_TAB_ID,
      requestConfig: {
        endpoint: endpoint,
        headers: headers,
      } as any,
    });
    if (isNullOrUndefinedOrWhiteString(query)) {
      query = this.initialQuery;
    }
    this._tab.setQuery(query);
    this._tab.setRequestConfig({
      acceptHeaderSelect: this._getAcceptHeaderValue(this._format),
    });
    this.setTheme();
  }

  run(): void {
    this._tab?.query();
  }

  clear(): void {
    this._tab?.setQuery(SOLIDIFY_CONSTANTS.STRING_EMPTY);
  }

  reset(): void {
    this._tab?.setQuery(this.initialQuery);
  }

  copyQueryToClipboard(): void {
    const query = this._tab?.getQuery();
    if (isNotNullNorUndefined(query) && ClipboardUtil.copyStringToClipboard(query)) {
      this._notificationService.showInformation(LabelTranslateEnum.copiedQueryToClipboard);
    }
  }

  private _getAcceptHeaderValue(format: Enums.Rml.RmlFormatEnum): string {
    switch (format) {
      case Enums.Rml.RmlFormatEnum.XML:
        return "application/sparql-results+xml";
      case Enums.Rml.RmlFormatEnum.CSV:
        return "text/csv";
      case Enums.Rml.RmlFormatEnum.JSON:
      case Enums.Rml.RmlFormatEnum.SQL:
      default:
        return "application/sparql-results+json";
    }
  }
}
