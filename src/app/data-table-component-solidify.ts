/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - data-table-component-solidify.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {SharedProjectNameContainer} from "@shared/components/containers/shared-project-name/shared-project-name.container";
import {
  SharedResourceRoleMemberContainer,
  SharedResourceRoleMemberContainerMode,
} from "@shared/components/containers/shared-resource-role-member/shared-resource-role-member-container.component";
import {SharedAccessLevelPresentational} from "@shared/components/presentationals/shared-access-level/shared-access-level.presentational";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {UserInfoDataTableColumn} from "@shared/models/user-info-data-table-column.model";
import {
  DataTableComponent,
  DataTableComponentInput,
  EnumUtil,
  KeyValue,
  LogoWrapperContainer,
  MappingObject,
  StatusPresentational,
  ValueType,
} from "solidify-frontend";

export const dataTableComponentSolidify: MappingObject<DataTableComponentEnum, DataTableComponent> = {
  [DataTableComponentEnum.status]: {
    componentType: StatusPresentational,
    inputs: [
      {
        key: "statusModel",
        valueType: ValueType.isCallback,
        callbackValue: (rowData, col) => EnumUtil.getKeyValue(col.filterEnum as KeyValue[], DataTableComponentHelper.getData(rowData, col)),
      },
      {
        key: "label",
        valueType: ValueType.isStatic,
        staticValue: undefined,
      },
      {
        key: "takeAllWidth",
        valueType: ValueType.isStatic,
        staticValue: true,
      },
    ],
  } as DataTableComponent,
  [DataTableComponentEnum.logo]: {
    componentType: LogoWrapperContainer,
    inputs: [
      {
        key: "isUser",
        valueType: ValueType.isAttributeOnCol,
        attributeOnCol: "isUser",
      },
      {
        key: "logoNamespace",
        valueType: ValueType.isAttributeOnCol,
        attributeOnCol: "resourceNameSpace",
      },
      {
        key: "logoState",
        valueType: ValueType.isAttributeOnCol,
        attributeOnCol: "resourceState",
      },
      {
        key: "userInfo",
        valueType: ValueType.isRowData,
      },
      {
        key: "idResource",
        valueType: ValueType.isCallback,
        callbackValue: (rowData, col: UserInfoDataTableColumn<any>) => col.idResource(rowData),
      },
      {
        key: "isLogoPresent",
        valueType: ValueType.isCallback,
        callbackValue: (rowData, col: UserInfoDataTableColumn<any>) => col.isLogoPresent(rowData),
      },
    ],
  } as DataTableComponent,
  [DataTableComponentEnum.accessLevel]: {
    componentType: SharedAccessLevelPresentational,
    inputs: [
      {
        key: "accessLevel",
        valueType: ValueType.isColData,
      },
      {
        key: "withTooltip",
        valueType: ValueType.isStatic,
        staticValue: true,
      },
    ],
  },
  [DataTableComponentEnum.projectName]: {
    componentType: SharedProjectNameContainer,
    inputs: [
      {
        key: "id",
        valueType: ValueType.isColData,
      } as DataTableComponentInput,
      {
        key: "displayOverlay",
        valueType: ValueType.isStatic,
        staticValue: true,
      } as DataTableComponentInput,
    ],
  },
  [DataTableComponentEnum.projectMember]: {
    componentType: SharedResourceRoleMemberContainer,
    inputs: [
      {
        key: "resource",
        valueType: ValueType.isRowData,
      } as DataTableComponentInput,
      {
        key: "mode",
        valueType: ValueType.isStatic,
        staticValue: SharedResourceRoleMemberContainerMode.project,
      } as DataTableComponentInput,
    ],
  },
} as MappingObject<DataTableComponentEnum, DataTableComponent>;
