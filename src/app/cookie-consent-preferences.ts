/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - cookie-consent-preferences.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {
  CookiePreference,
  CookieType,
  MARK_AS_TRANSLATABLE,
} from "solidify-frontend";

export const cookieConsentPreferences: CookiePreference[] = [
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.privacyPolicyAndTermsOfUseAccepted,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.privacyPolicyAndTermsOfUseAccepted.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.privacyPolicyAndTermsOfUseAccepted.description"),
    visible: () => true,
  },
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.notificationInboxPending,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.notificationInboxPending.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.notificationInboxPending.description"),
    visible: () => true,
  },
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.browseProjectShowOnlyMyProjects,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.browseProjectShowOnlyMine.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.browseProjectShowOnlyMine.description"),
    visible: () => true,
  },
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.currentProject,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.currentProject.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.currentProject.description"),
    visible: () => true,
  },
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.singleSelectLastSelectionProject,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.singleSelectLastSelectionProject.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.singleSelectLastSelectionProject.description"),
    visible: () => true,
  },
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.multiSelectLastSelectionContributor,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.multiSelectLastSelectionContributor.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.multiSelectLastSelectionContributor.description"),
    visible: () => true,
  },
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.preferredRdfFormat,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.preferredRdfFormat.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.preferredRdfFormat.description"),
    visible: () => true,
  },
];
