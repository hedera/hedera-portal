/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - meta-info-list.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AbstractResearchElementMetadataResult,
  Project,
  PublicOntology,
} from "@models";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  isNotNullNorUndefinedNorWhiteString,
  MARK_AS_TRANSLATABLE,
  MetaInfo,
  MetaUtil,
} from "solidify-frontend";

const TITLE_MAX_CHAR: number = 60;
const ELLIPSIS: string = "...";

const META_NAME_KEYWORDS: string = "keywords";

export const LIST_RESEARCH_OBJECT_META_NAME: string[] = [
  META_NAME_KEYWORDS,
  // META_NAME_ARCHIVE_ABSTRACT,
  // META_NAME_ARCHIVE_TYPE,
  // META_NAME_ARCHIVE_DOI,
  // META_NAME_ARCHIVE_TITLE,
  // META_NAME_ARCHIVE_AUTHOR,
  // META_NAME_ARCHIVE_DATE,
  // META_NAME_ARCHIVE_AUTHORS,
  // META_NAME_ARCHIVE_KEYWORDS,
];
export const metaInfoResearchElement: MetaInfo<AbstractResearchElementMetadataResult> = {
  regex: /^\/home\/detail\/.*/,
  titleToTranslateCallback: (translate, researchElementMetadataResult) => {
    const suffix = translate.instant(MARK_AS_TRANSLATABLE("meta.researchObject.suffixTitle"));
    const title: string = researchElementMetadataResult.metadata.uri;
    const concatenation: string = title + suffix;
    if (concatenation.length <= TITLE_MAX_CHAR) {
      return concatenation;
    }
    return title.substring(0, TITLE_MAX_CHAR - (suffix.length + ELLIPSIS.length)) + ELLIPSIS + suffix;
  },
  descriptionToTranslateCallback: (translate, researchObjectMetadataResult) => translate.instant(MARK_AS_TRANSLATABLE("meta.researchObject.description"), researchObjectMetadataResult),
  extraMetaTreatmentCallback: (document, meta, translateService, researchObjectMetadataResult) => {
    const metadata = researchObjectMetadataResult.metadata;
    MetaUtil.cleanListMetaByName(meta, LIST_RESEARCH_OBJECT_META_NAME);

    MetaUtil.setMetaTagNameArrayWithSeparator(meta, META_NAME_KEYWORDS, researchObjectMetadataResult.metadata.researchProject.keyword);
    // MetaUtil.setMetaTagNameArrayWithSeparator(meta, META_NAME_ARCHIVE_KEYWORDS, researchObjectMetadataResult.keywords);
    // MetaUtil.setMetaTagName(meta, META_NAME_ARCHIVE_ABSTRACT, researchObjectMetadataResult.description);
    // MetaUtil.setMetaTagName(meta, META_NAME_ARCHIVE_TYPE, MetadataUtil.getType(metadata) === "DATASET" ? LabelTranslateEnum.dataset : MetadataUtil.getType(metadata));
    // MetaUtil.setMetaTagName(meta, META_NAME_ARCHIVE_DOI, MetadataUtil.getDOI(metadata));
    // MetaUtil.setMetaTagName(meta, META_NAME_ARCHIVE_TITLE, researchObjectMetadataResult.title);
    // const listAuthors = researchObjectMetadataResult.contributors?.map(c => c.creatorName);
    // MetaUtil.setMetaTagNameForEachArray(meta, META_NAME_ARCHIVE_AUTHOR, listAuthors);
    // MetaUtil.setMetaTagName(meta, META_NAME_ARCHIVE_DATE, DateUtil.convertDateToDateTimeString(MetadataUtil.getSubmittedDate(metadata)));
    // MetaUtil.setMetaTagNameArrayWithSeparator(meta, META_NAME_ARCHIVE_AUTHORS, listAuthors, "; ");
    // if (researchObjectMetadataResult.withThumbnail) {
    //   MetaUtil.setMetaTagProperty(meta, "og:image", `${ApiEnum.accessPublicMetadata}/${researchObjectMetadataResult.resId}/${ApiActionNameEnum.THUMBNAIL}`);
    // }
  },
  bypassAutomaticMeta: true,
};

export const metaInfoBrowseOntologyDetail: MetaInfo<PublicOntology> = {
  regex: /^\/browse\/ontology\/detail\/.*/,
  titleToTranslateCallback: (translate, ontology) => {
    const suffix = translate.instant(MARK_AS_TRANSLATABLE("meta.browseOntologyDetail.suffixTitle"));
    const title: string = `${ontology.name} (${ontology.version})`;
    const concatenation: string = title + suffix;
    if (concatenation.length <= TITLE_MAX_CHAR) {
      return concatenation;
    }
    return title.substring(0, TITLE_MAX_CHAR - (suffix.length + ELLIPSIS.length)) + ELLIPSIS + suffix;
  },
  descriptionToTranslateCallback: (translate, ontology) => {
    if (isNotNullNorUndefinedNorWhiteString(ontology.description)) {
      return ontology.description;
    }
    return translate.instant(MARK_AS_TRANSLATABLE("meta.browseOntologyDetail.description"), ontology);
  },
  bypassAutomaticMeta: true,
};

export const metaInfoBrowseProjectDetail: MetaInfo<Project> = {
  regex: /^\/browse\/project\/detail\/.*/,
  titleToTranslateCallback: (translate, project) => {
    const suffix = translate.instant(MARK_AS_TRANSLATABLE("meta.browseProjectDetail.suffixTitle"));
    const title: string = `${project.name}`;
    const concatenation: string = title + suffix;
    if (concatenation.length <= TITLE_MAX_CHAR) {
      return concatenation;
    }
    return title.substring(0, TITLE_MAX_CHAR - (suffix.length + ELLIPSIS.length)) + ELLIPSIS + suffix;
  },
  descriptionToTranslateCallback: (translate, project) => {
    if (isNotNullNorUndefinedNorWhiteString(project.description)) {
      return project.description;
    }
    return translate.instant(MARK_AS_TRANSLATABLE("meta.browseProjectDetail.description"), project);
  },
  bypassAutomaticMeta: true,
};

export const metaInfoSparql: MetaInfo<void> = {
  regex: /^\/sparql*/,
  titleToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaSparqlTitle),
  descriptionToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaSparqlDescription),
};

export const metaInfoAbout: MetaInfo<void> = {
  regex: /^\/about*/,
  titleToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaAboutTitle),
  descriptionToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaAboutDescription),
};

export const metaInfoBrowseProject: MetaInfo<void> = {
  regex: /^\/browse\/project*/,
  titleToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaBrowseProjectTitle),
  descriptionToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaBrowseProjectDescription),
};

export const metaInfoBrowseOntology: MetaInfo<void> = {
  regex: /^\/browse\/ontology$/,
  titleToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaBrowseOntologyTitle),
  descriptionToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaBrowseOntologyDescription),
};

export const metaInfoAdmin: MetaInfo<void> = {
  regex: /^\/admin\/*/,
  titleToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaAdminTitle),
  descriptionToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaGeneralDescription),
};

export const metaInfoDefault: MetaInfo<void> = {
  regex: /^.*/,
  titleToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaGeneralTitle),
  descriptionToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaGeneralDescription),
};

export const metaInfoList: MetaInfo<any>[] = [
  metaInfoSparql,
  metaInfoAbout,
  metaInfoBrowseProject,
  metaInfoBrowseProjectDetail,
  metaInfoBrowseOntology,
  metaInfoBrowseOntologyDetail,
  metaInfoAdmin,
  metaInfoDefault,
];
