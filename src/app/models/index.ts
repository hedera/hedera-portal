/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - index.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

/* eslint-disable no-restricted-imports,@typescript-eslint/consistent-type-definitions */
import {Enums} from "@app/enums";
import {Link} from "@app/generated-api/model/link.partial.model";
import {SparqlResultRow} from "@app/generated-api/model/sparql-result-row.partial.model";
import {
  ApplicationRole as SolidifyApplicationRole,
  BaseResource,
  BaseResourceAvatar,
  BaseResourceExtended as SolidifyBaseResourceExtended,
  BaseResourceLogo,
  FacetProperty,
  Label as SolidifyLabel,
  MappingObject,
  OverrideType,
  SolidifyDataFileModel,
  SolidifyFile,
  SolidifyFileUploadType,
  SolidifyRole,
  SystemPropertyPartial as SolidifySystemPropertyPartial,
  User as SolidifyUser,
  UserApplicationRoleEnum,
} from "solidify-frontend";
import {ApplicationRole as ApplicationRolePartial} from "../generated-api/model/application-role.partial.model";
import {ChangeInfo as ChangeInfoPartial} from "../generated-api/model/change-info.partial.model";
import {FundingAgency as FundingAgencyPartial} from "../generated-api/model/funding-agency.partial.model";
import {IIIFCollectionEntry as IIIFCollectionEntryPartial} from "../generated-api/model/iiif-collection-entry.partial.model";
import {IIIFCollectionItem as IIIFCollectionItemPartial} from "../generated-api/model/iiif-collection-item.partial.model";
import {IIIFCollectionSettings as IIIFCollectionSettingsPartial} from "../generated-api/model/iiif-collection-settings.partial.model";
import {IIIFCollection as IIIFCollectionPartial} from "../generated-api/model/iiif-collection.partial.model";
import {IIIFManifestEntry as IIIFManifestEntryPartial} from "../generated-api/model/iiif-manifest-entry.partial.model";
import {IIIFManifest as IIIFManifestPartial} from "../generated-api/model/iiif-manifest.partial.model";
import {Institution as InstitutionPartial} from "../generated-api/model/institution.partial.model";
import {Language as LanguagePartial} from "../generated-api/model/language.partial.model";
import {License as LicensePartial} from "../generated-api/model/license.partial.model";
import {MappingParameters as MappingParametersPartial} from "../generated-api/model/mapping-parameters.partial.model";
import {MetadataType as MetadataTypePartial} from "../generated-api/model/metadata-type.partial.model";
import {Ontology as OntologyPartial} from "../generated-api/model/ontology.partial.model";
import {Person as PersonPartial} from "../generated-api/model/person.partial.model";
import {Project as ProjectPartial} from "../generated-api/model/project.partial.model";
import {PublicOntology as PublicOntologyPartial} from "../generated-api/model/public-ontology.partial.model";
import {RdfDatasetFile as RdfDatasetFilePartial} from "../generated-api/model/rdf-dataset-file.partial.model";
import {ResearchDataFile as ResearchDataFilePartial} from "../generated-api/model/research-data-file.partial.model";
import {ResearchObjectType as ResearchObjectTypePartial} from "../generated-api/model/research-object-type.partial.model";
import {Rml as RmlPartial} from "../generated-api/model/rml.partial.model";
import {Role as RolePartial} from "../generated-api/model/role.partial.model";
import {SourceDatasetFile as SourceDatasetFilePartial} from "../generated-api/model/source-dataset-file.partial.model";
import {SourceDataset as SourceDatasetPartial} from "../generated-api/model/source-dataset.partial.model";
import {SystemProperties as SystemPropertyPartial} from "../generated-api/model/system-properties.partial.model";
import {User as UserPartial} from "../generated-api/model/user.partial.model";
import IdentifiersEnum = Enums.IdentifiersEnum;

export type RdfDatasetFile = OverrideType<OverrideType<RdfDatasetFilePartial, AbstractResearchElementMetadata>, {
  statusImport?: Enums.RdfDatasetFile.StatusImportEnum;
  sourceDatasetFile: SourceDatasetFile;
  rdfFormat: Enums.FormatEnum;
}> & BaseResourceExtended;

export type AbstractResearchElementMetadataResult<TResult extends AbstractResearchElementMetadata = AbstractResearchElementMetadata> = {
  index: string;
  metadata: TResult;
  oaimetadata: string;
  resId: string;
  _links?: {
    [key: string]: Link;
  };
} & BaseResource;

export type ResearchDataFileMetadataResult = AbstractResearchElementMetadataResult<ResearchDataFileMetadata>;
export type ResearchObjectMetadataResult = AbstractResearchElementMetadataResult<ResearchObjectMetadata>;

export type AbstractResearchElementMetadata = {
  hederaId?: string;
  itemType?: Enums.SearchMetadata.ResearchElementTypeEnum;
  metadataList?: MappingObject<string, any> & AbstractResearchElementMetadataList;
  objectData?: string;
  researchObjectType?: ResearchObjectType;
  researchProject?: ResearchObjectProject;
  uri?: string;
  checksums?: DataFileChecksum[];
} & BaseResourceExtended;

export interface AbstractResearchElementMetadataList {
  xml: string;
}

export interface ResearchDataFileMetadata extends AbstractResearchElementMetadata {
  metadataList: MappingObject<string, any> & ResearchDataFileMetadataList;
  accessibleFrom: Enums.ResearchDataFile.AccessibleFromEnum;
  fileName: string;
  fileSize: number;
  mimeType: string;
  relativeLocation: string;
}

export interface ResearchObjectMetadata extends AbstractResearchElementMetadata {
  metadataList: MappingObject<string, any> & ResearchObjectMetadataList;
  sourceId: string;
  objectType: Enums.ResearchObject.ObjectTypeEnum;
}

export interface ResearchDataFileMetadataList extends AbstractResearchElementMetadataList {
  height?: string;
  width?: string;
  web?: string;
}

export interface ResearchObjectMetadataList extends AbstractResearchElementMetadataList {
  filename?: string;
  hasMimeType?: string;
  height?: string;
  type?: string;
  value?: string;
  width?: string;
}

export type ResearchObjectProject = {
  accessPublic: boolean;
  closingDate: string;
  shortName: string;
  description: string;
  id: string;
  keyword: string[];
  name: string;
  openingDate: string;
  url: string;
};

export type BaseResourceExtended = {
  creation?: ChangeInfo;
  lastUpdate?: ChangeInfo;
} & SolidifyBaseResourceExtended;

export type ChangeInfo = OverrideType<ChangeInfoPartial>;

export interface Checksum {
  checksumAlgo?: Enums.DataFile.Checksum.AlgoEnum;
  checksumType?: Enums.DataFile.Checksum.TypeEnum;
  checksumOrigin?: Enums.DataFile.Checksum.OriginEnum;
  checksum?: string;
  creationTime?: string;
}

export interface DataFileChecksum {
  checksum?: string;
  checksumAlgo?: Enums.DataFile.Checksum.AlgoEnum;
  checksumType?: Enums.DataFile.Checksum.TypeEnum;
  checksumOrigin: Enums.DataFile.Checksum.OriginEnum;
  creationTime?: string;
}

export type DownloadToken = {
  user?: User;
  archiveId?: string;
  token?: string;
} & BaseResourceExtended;

export interface FileList {
  files?: string[];
  puids?: string[];
}

export interface ArchiveFundingAgency {
  name?: string;
  identifierType?: string;
  identifierUrl?: string;
  identifier?: string;
}

export type FundingAgency = OverrideType<FundingAgencyPartial, {
  projects?: Project[];
  identifiers?: MappingObject<IdentifiersEnum, string>;
}> & BaseResourceLogo & BaseResourceExtended;

export type IndexFieldAlias = {
  labels?: Label[];
  indexName: string;
  alias: string;
  field: string;
  facet: boolean;
  system: boolean;
  facetMinCount: number;
  facetLimit: number;
  facetOrder: number;
  facetDefaultVisibleValues: number;
} & BaseResourceExtended;

export interface ArchiveInstitution {
  name?: string;
  identifierType?: string;
  identifierUrl?: string;
  identifier?: string;
}

export type Institution = OverrideType<InstitutionPartial, {
  identifiers?: MappingObject<IdentifiersEnum, string>;
  logo?: SolidifyFile;
  role?: Role;
  projects?: Project[];
}> & BaseResourceLogo & BaseResourceExtended;

export type Rml = OverrideType<RmlPartial, {
  rdfFile?: DataFile;
  rdfFileChange?: SolidifyFileUploadType; // For portal usage
}> & BaseResourceExtended;

export type MappingParameters = OverrideType<MappingParametersPartial, {}> & BaseResourceExtended;

export type Language = OverrideType<LanguagePartial> & BaseResourceExtended;

export interface ArchiveLicense {
  name?: string;
  url?: string;
  identifierType?: string;
  identifier?: string;
}

export type License = OverrideType<LicensePartial, {
  odConformance?: Enums.License.OdConformanceEnum;
  osdConformance?: Enums.License.OsdConformanceEnum;
  status?: Enums.License.StatusEnum;
  logo?: SolidifyFile;
}> & BaseResourceExtended & BaseResourceLogo;

export type Project = OverrideType<ProjectPartial, {
  fundingAgencies?: FundingAgency[];
  institutions?: Institution[];
  rmls?: Rml[];
  iiifManifestResearchObjectType?: ResearchObjectType;
  researchDataFileResearchObjectType?: ResearchObjectType;
  researchObjectTypes?: ResearchObjectType[];
  keywords?: string[];
  defaultLicense?: License;
  role?: Role | null;
}> & BaseResourceLogo;

export type Person = OverrideType<PersonPartial, {
  projects?: Project[];
  institutions?: Institution[];
}> & BaseResourceAvatar & BaseResourceExtended;

export type Role = OverrideType<RolePartial> & BaseResourceExtended & SolidifyRole;

export type Ontology = OverrideType<OntologyPartial, {
  format?: Enums.FormatEnum;
  ontologyFile?: DataFile;
  ontologyFileChange?: SolidifyFileUploadType; // For portal usage
}> & BaseResourceExtended;

export type PublicOntology = OverrideType<PublicOntologyPartial> & BaseResource;

export type SourceDataset = OverrideType<SourceDatasetPartial, {}> & BaseResourceExtended;

export type SourceDatasetFile = OverrideType<OverrideType<SourceDatasetFilePartial, AbstractResearchElementMetadata>, {
  status?: Enums.SourceDataFile.StatusEnum;
  type?: Enums.SourceDataFile.TypeEnum;
  researchDataFile?: ResearchDataFile;
}> & BaseResourceExtended;

export type ResearchDataFile = OverrideType<OverrideType<ResearchDataFilePartial, AbstractResearchElementMetadata>, {
  project?: Project;
  status?: Enums.ResearchDataFile.StatusEnum;
  accessibleFrom?: Enums.ResearchDataFile.AccessibleFromEnum;
  researchObjectType?: ResearchObjectType;
}> & BaseResourceExtended;

export type RdfObject = OverrideType<SparqlResultRow, {}> & BaseResource;

export type ResearchObjectType = OverrideType<ResearchObjectTypePartial, {
  ontology?: Ontology;
}> & BaseResourceExtended;

export type ScheduledTask = {
  resId?: string;
  name: string;
  taskType: Enums.ScheduledTask.TaskTypeEnum;
  cronExpression: string;
  lastExecutionStart?: string;
  lastExecutionEnd?: string;
  enabled?: boolean;
  nextExecution?: string;
} & BaseResourceExtended;

export type SystemProperty = OverrideType<SystemPropertyPartial, {
  searchFacets: FacetProperty[] | any;
  checksums?: string[];
  orcidClientId: string;
  uploadFileSizeLimit: number;
}> & SolidifySystemPropertyPartial;

export type IIIFCollection = OverrideType<IIIFCollectionPartial, {
  items?: IIIFCollectionItem[];
}>;

export type IIIFCollectionItem = OverrideType<IIIFCollectionItemPartial, {}>;

export type IIIFManifestEntry = OverrideType<IIIFManifestEntryPartial, {}> & BaseResourceExtended;

export type IIIFManifest = OverrideType<IIIFManifestPartial, {}>;

export type IIIFCollectionEntry = OverrideType<IIIFCollectionEntryPartial, {}>;

export type IIIFCollectionSettings = OverrideType<IIIFCollectionSettingsPartial, {
  project: Project;
}> & BaseResourceExtended;

export type UserInfo = {
  resId?: string;
  firstName?: string;
  lastName?: string;
  externalUid?: string;
} & BaseResource;

export type ApplicationRole = OverrideType<ApplicationRolePartial, {
  resId?: UserApplicationRoleEnum;
}> & SolidifyApplicationRole & OverrideType<BaseResourceExtended, {
  resId?: UserApplicationRoleEnum;
}>;

export type User = OverrideType<UserPartial, {
  applicationRole?: ApplicationRole;
  person?: Person;
  accessToken?: string;
  refreshToken?: string;
  lastLoginIpAddress?: string;
  lastLoginTime?: string;
}> & BaseResourceExtended & SolidifyUser;

export type MetadataType = OverrideType<MetadataTypePartial> & BaseResource;

export interface VirusCheck {
  checkDate?: string;
  details?: string;
  tool?: Tool;
}

export interface FileFormat {
  tool?: Tool;
}

export interface Tool {
  description?: string;
  name?: string;
  puid?: string;
  version?: string;
}

export type Label = OverrideType<SolidifyLabel, {
  languageCode?: Enums.Language.LanguageEnum;
  text?: string;
}>;

export interface SearchCondition {
  type?: Enums.SearchCondition.Type;
  booleanClauseType?: Enums.SearchCondition.BooleanClauseType;
  field?: string;
  fields?: string[];
  value?: string;
  terms?: string[];
  upperValue?: string;
  lowerValue?: string;
  nestedConditions?: SearchCondition[];
}

export type DataFile<TPackage extends BaseResource = any> = {
  status?: Enums.ResearchDataFile.StatusEnum;
  fileContent?: string;
} & SolidifyFile & SolidifyDataFileModel & BaseResourceExtended;
