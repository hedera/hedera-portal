/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - app.component.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  Renderer2,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {Router} from "@angular/router";
import {UserProfileDialog} from "@app/components/dialogs/user-profile/user-profile.dialog";
import {RoutesEnum} from "@app/shared/enums/routes.enum";
import {appActionNameSpace} from "@app/stores/app.action";
import {AppState} from "@app/stores/app.state";
import {AppUserLogoState} from "@app/stores/user/user-logo/app-user-logo.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {HomeHelper} from "@home/helpers/home.helper";
import {PortalSearch} from "@home/models/portal-search.model";
import {User} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {ApplicationRolePermissionEnum} from "@shared/enums/application-role-permission.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {TourEnum} from "@shared/enums/tour.enum";
import {SecurityService} from "@shared/services/security.service";
import {HederaLabelUtil} from "@shared/utils/hedera-label.util";
import {PermissionUtil} from "@shared/utils/permission.util";
import {TourService} from "ngx-ui-tour-md-menu";
import {
  combineLatest,
  Observable,
} from "rxjs";
import {
  filter,
  map,
  take,
  tap,
} from "rxjs/operators";
import {
  AppConfigService,
  AppStatusService,
  BreakpointService,
  CookieConsentService,
  DialogUtil,
  FrontendVersion,
  GlobalBanner,
  GoogleAnalyticsService,
  isNotNullNorUndefined,
  isNullOrUndefined,
  LoggingService,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  MetaService,
  NotificationService,
  ScrollService,
  SOLIDIFY_CONSTANTS,
  SolidifyApplicationAbstractAppComponent,
  SolidifyGlobalBannerState,
  SsrUtil,
  TransferStateService,
  UrlQueryParamHelper,
} from "solidify-frontend";

@Component({
  selector: "hedera-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent extends SolidifyApplicationAbstractAppComponent {
  logoTransparent: string = `./assets/themes/${this.environment.theme}/toolbar-hedera-image-transparent.svg`;

  currentModule: string;
  isHomePage: boolean;

  userRolesObs: Observable<Enums.UserApplicationRole.UserApplicationRoleEnum[]> = MemoizedUtil.select(this._store, AppState, state => state.userRoles);
  currentPhotoUserObs: Observable<string> = MemoizedUtil.select(this._store, AppUserLogoState, state => state.file);

  frontendVersionObs: Observable<FrontendVersion> = MemoizedUtil.select(this._store, AppState, state => state.frontendVersion);

  globalBannerObs: Observable<GlobalBanner> = MemoizedUtil.select(this._store, SolidifyGlobalBannerState, state => state.globalBanner);

  get localStorageEnum(): typeof LocalStorageEnum {
    return LocalStorageEnum;
  }

  get routesEnum(): typeof RoutesEnum {
    return RoutesEnum;
  }

  get tourEnum(): typeof TourEnum {
    return TourEnum;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get labelUtil(): typeof HederaLabelUtil {
    return HederaLabelUtil;
  }

  get ssrUtil(): typeof SsrUtil {
    return SsrUtil;
  }

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _router: Router,
              protected readonly _translate: TranslateService,
              protected readonly _renderer: Renderer2,
              protected readonly _injector: Injector,
              protected readonly _notificationService: NotificationService,
              protected readonly _loggingService: LoggingService,
              public readonly appStatusService: AppStatusService,
              public readonly breakpointService: BreakpointService,
              protected readonly _dialog: MatDialog,
              public readonly tourService: TourService,
              protected readonly _scrollService: ScrollService,
              protected readonly _googleAnalyticsService: GoogleAnalyticsService,
              protected readonly _cookieConsentService: CookieConsentService,
              protected readonly _appConfigService: AppConfigService,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _metaService: MetaService,
              protected readonly _securityService: SecurityService,
              protected readonly _transferState: TransferStateService,
  ) {
    super(_store,
      _actions$,
      _router,
      _translate,
      _renderer,
      _injector,
      _notificationService,
      _loggingService,
      appStatusService,
      breakpointService,
      _dialog,
      _scrollService,
      _googleAnalyticsService,
      _cookieConsentService,
      _appConfigService,
      _changeDetector,
      _metaService,
      _transferState,
      _securityService,
    );
    this._listenCurrentModule();
    if (SsrUtil.isBrowser) {
      this.subscribe(this._observeLoggedUserForCart());
      this._checkOrcidQueryParam();
      this._activeScriptDiffer([]);
    }
  }

  private _observeLoggedUserForCart(): Observable<boolean> {
    return combineLatest([this.isLoggedObs, this.userRolesObs])
      .pipe(
        map(([isLogged, userRoles]) => this.computeDisplayCart(isLogged, userRoles)),
      );
  }

  private _checkOrcidQueryParam(): void {
    if (SsrUtil.isServer) {
      return;
    }
    if (UrlQueryParamHelper.currentUrlContainsQueryParam(environment.orcidQueryParam)) {
      this.subscribe(this.currentUserObs.pipe(
        filter(user => isNotNullNorUndefined(user)),
        take(1),
        tap((user: User) => {
          DialogUtil.open(this._dialog, UserProfileDialog, user, {
            width: "90%",
          });
          this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("notification.orcid.associated"), {orcid: user?.person?.orcid});
        }),
      ));
    }
  }

  private _listenCurrentModule(): void {
    if (SsrUtil.isServer) {
      this._computeCurrentModule(SsrUtil.window.location.pathname);
    } else {
      this.subscribe(this.urlStateObs.pipe(tap(urlState => this._computeCurrentModule(urlState?.url))));
    }
  }

  private _computeCurrentModule(url: string): void {
    this.ignoreGrid = false;
    this.isHomePage = false;
    this.currentModule = undefined;

    if (isNullOrUndefined(url)) {
      return;
    }

    if (url === SOLIDIFY_CONSTANTS.URL_SEPARATOR) {
      this.currentModule = undefined;
      this.ignoreGrid = true;
      return;
    }

    const urlHomeDetail = SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.homeDetail;
    if (url.startsWith(urlHomeDetail)) {
      this.currentModule = undefined;
      if (url.lastIndexOf(SOLIDIFY_CONSTANTS.URL_SEPARATOR) === urlHomeDetail.length) {
        this.ignoreGrid = true;
      }
      return;
    }

    if (url === SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.about ||
      url === SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.releaseNotes ||
      url === SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.changelog) {
      this.currentModule = undefined;
      this.isHomePage = false;
      this.ignoreGrid = true;
      return;
    }

    if (url.startsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.homeSearch)) {
      this.currentModule = undefined;
      this.isHomePage = true;
      this.ignoreGrid = true;
      return;
    }

    if (url.startsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.homePage)) {
      this.currentModule = RoutesEnum.homePage;
      this.isHomePage = true;
      this.ignoreGrid = true;
      return;
    }

    if (url === SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.colorCheck) {
      this.currentModule = undefined;
      this.isHomePage = true;
      this.ignoreGrid = true;
      return;
    }

    if (url.startsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.ingest)) {
      this.currentModule = RoutesEnum.ingest;
      return;
    }

    if (url.startsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.admin)) {
      this.currentModule = RoutesEnum.admin;
      return;
    }

    if (url.startsWith(SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.browse)) {
      this.currentModule = RoutesEnum.browse;
      return;
    }
  }

  private _activeScriptDiffer(listScript: Element[]): void {
    listScript.forEach(script => {
      const value = this._getValueScriptDiffer(script);
      if (isNullOrUndefined(value)) {
        return;
      }
      this._renderer.setAttribute(script, this._ATTRIBUTE_SRC, value);
      this._renderer.removeAttribute(script, this._ATTRIBUTE_SRC_DIFFER_KEY);
    });
  }

  navigateToHome(): void {
    this.navigate(RoutesEnum.homePage);
  }

  computeDisplayCart(isLogged: boolean, userRoles: Enums.UserApplicationRole.UserApplicationRoleEnum[]): boolean {
    this.displayCart = PermissionUtil.isUserHavePermission(isLogged, ApplicationRolePermissionEnum.userPermission, userRoles);
    return this.displayCart;
  }

  search(searchTerm: string): void {
    const portalSearch = {
      searchText: searchTerm,
    } as PortalSearch;
    HomeHelper.navigateToSearch(this._store, portalSearch);
  }

  openUserGuideSidebar(): void {
    this._store.dispatch(new appActionNameSpace.ChangeDisplaySidebarUserGuide(true));
  }

  initiateNewLogin(): void {
    const currentPath = SsrUtil.window?.location.pathname + SsrUtil.window?.location.search;
    this._store.dispatch(new appActionNameSpace.InitiateNewLogin(currentPath));
  }
}
