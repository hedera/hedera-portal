/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - app.component.spec.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  TestBed,
  waitForAsync,
} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {
  AppModule,
  appModuleState,
} from "@app/app.module";
import {environment} from "@environments/environment";
import {NgxsModule} from "@ngxs/store";
import {ENVIRONMENT} from "solidify-frontend";
import {AppComponent} from "./app.component";

describe("AppComponent", () => {
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        NgxsModule.forRoot([
          ...appModuleState,
        ]),
        AppModule,
      ],
      providers: [
        {
          provide: ENVIRONMENT,
          useValue: environment,
        },
      ],
    }).compileComponents();
  }));

  xit("should create the app", () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  xit(`should have a title not empty`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.getTitle()).not.toBe("");
  });

  xit("should render image with unige logo", () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector("div.logo>img").src).toContain("unigelogo-white.svg");
  });
});
