/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - app-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {ApplicationRolePermissionEnum} from "@app/shared/enums/application-role-permission.enum";
import {AppRoutesEnum} from "@app/shared/enums/routes.enum";
import {ApplicationRoleGuardService} from "@app/shared/guards/application-role-guard.service";
import {HederaRoutes} from "@app/shared/models/hedera-route.model";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {IsManagerOfAtLeastOneProjectGuardService} from "@shared/guards/is-manager-of-at-least-one-project-guard.service";
import {WaitAppInitializedGuardService} from "@shared/guards/wait-app-initialized-guard.service";
import {
  AboutRoutable,
  ChangelogRoutable,
  ColorCheckRoutable,
  CombinedGuardService,
  ComponentAppSummaryRoutable,
  DevGuardService,
  IconAppSummaryRoutable,
  MaintenanceModeRoutable,
  PageNotFoundRoutable,
  PreventLeaveGuardService,
  PreventLeaveMaintenanceGuardService,
  ReleaseNotesRoutable,
  ServerOfflineModeRoutable,
  UnableToLoadAppRoutable,
  UrlQueryParamHelper,
} from "solidify-frontend";

const routes: HederaRoutes = [
  {
    path: AppRoutesEnum.home,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./features/home/home.module").then(m => m.HomeModule),
    data: {
      permission: ApplicationRolePermissionEnum.noPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.ingest,
    // @ts-ignore Dynamic import
    loadChildren: () => import("@app/features/ingest/ingest.module").then(m => m.IngestModule),
    data: {
      breadcrumb: LabelTranslateEnum.ingest,
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.browse,
    // @ts-ignore Dynamic import
    loadChildren: () => import("@app/features/browse/browse.module").then(m => m.BrowseModule),
    data: {
      breadcrumb: LabelTranslateEnum.browse,
      guards: [WaitAppInitializedGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.admin,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./features/admin/admin.module").then(m => m.AdminModule),
    data: {
      breadcrumb: LabelTranslateEnum.administration,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [IsManagerOfAtLeastOneProjectGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.sparql,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./features/sparql/sparql.module").then(m => m.SparqlModule),
    data: {
      breadcrumb: LabelTranslateEnum.sparqlQuery,
      permission: ApplicationRolePermissionEnum.noPermission,
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.icons,
    component: IconAppSummaryRoutable,
    data: {
      guards: [DevGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.components,
    component: ComponentAppSummaryRoutable,
    data: {
      guards: [DevGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.colorCheck,
    component: ColorCheckRoutable,
    data: {
      guards: [DevGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.maintenance,
    component: MaintenanceModeRoutable,
    canDeactivate: [PreventLeaveMaintenanceGuardService],
  },
  {
    path: AppRoutesEnum.about,
    component: AboutRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.about,
    },
  },
  {
    path: AppRoutesEnum.releaseNotes,
    component: ReleaseNotesRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.releaseNotes,
    },
  },
  {
    path: AppRoutesEnum.changelog,
    component: ChangelogRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.changelog,
    },
  },
  {
    path: AppRoutesEnum.serverOffline,
    component: ServerOfflineModeRoutable,
    canDeactivate: [PreventLeaveGuardService],
  },
  {
    path: AppRoutesEnum.unableToLoadApp,
    component: UnableToLoadAppRoutable,
    canDeactivate: [PreventLeaveGuardService],
  },
  {
    path: AppRoutesEnum.root,
    redirectTo: "/" + AppRoutesEnum.home + UrlQueryParamHelper.getQueryParamOAuth2(),
    pathMatch: "full",
  },
  {
    path: AppRoutesEnum.index,
    redirectTo: "/" + AppRoutesEnum.home + UrlQueryParamHelper.getQueryParamOAuth2(),
    pathMatch: "full",
  },
  {
    path: "**",
    component: PageNotFoundRoutable,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: "enabledBlocking",
  })],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
