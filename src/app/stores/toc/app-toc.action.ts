/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - app-toc.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
} from "solidify-frontend";

const state = StateEnum.application_toc;

export namespace AppTocAction {
  export class GetAllDocumentation extends BaseAction {
    static readonly type: string = `[${state}] Get All Documentation`;
  }

  export class GetAllDocumentationSuccess extends BaseSubActionSuccess<GetAllDocumentation> {
    static readonly type: string = `[${state}] Get All Documentation Success`;

    constructor(public parentAction: GetAllDocumentation) {
      super(parentAction);
    }
  }

  export class GetAllDocumentationFail extends BaseSubActionFail<GetAllDocumentation> {
    static readonly type: string = `[${state}] Get All Documentation Fail`;
  }

  export class GetToolsGuide extends BaseAction {
    static readonly type: string = `[${state}] Get Tools Guide`;
  }

  export class GetToolsGuideSuccess extends BaseSubActionSuccess<GetToolsGuide> {
    static readonly type: string = `[${state}] Get Tools Guide Success`;

    constructor(public parentAction: GetToolsGuide, public toolsGuide: string) {
      super(parentAction);
    }
  }

  export class GetToolsGuideFail extends BaseSubActionFail<GetToolsGuide> {
    static readonly type: string = `[${state}] Get Tools Guide Fail`;
  }

  export class GetIntegrationGuide extends BaseAction {
    static readonly type: string = `[${state}] Get Integration Guide`;
  }

  export class GetIntegrationGuideSuccess extends BaseSubActionSuccess<GetIntegrationGuide> {
    static readonly type: string = `[${state}] Get Integration Guide Success`;

    constructor(public parentAction: GetIntegrationGuide, public integrationGuide: string) {
      super(parentAction);
    }
  }

  export class GetIntegrationGuideFail extends BaseSubActionFail<GetIntegrationGuide> {
    static readonly type: string = `[${state}] Get Integration Guide Fail`;
  }

  export class ComputeApis extends BaseAction {
    static readonly type: string = `[${state}] Compute Apis`;
  }

  export class GetUserDocumentation extends BaseAction {
    static readonly type: string = `[${state}] Get User Documentation`;
  }

  export class GetUserDocumentationSuccess extends BaseSubActionSuccess<GetUserDocumentation> {
    static readonly type: string = `[${state}] Get User Documentation Success`;

    constructor(public parentAction: GetUserDocumentation, public userDocumentation: string) {
      super(parentAction);
    }
  }

  export class GetUserDocumentationFail extends BaseSubActionFail<GetUserDocumentation> {
    static readonly type: string = `[${state}] Get User Documentation Fail`;
  }
}

export const appTocActionNameSpace = AppTocAction;
