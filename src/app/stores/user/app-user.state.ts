/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - app-user.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {AppPersonAction} from "@app/stores/person/app-person.action";
import {
  AppUserAction,
  appUserActionNameSpace,
} from "@app/stores/user/app-user.action";
import {AppUserLogoAction} from "@app/stores/user/user-logo/app-user-logo.action";
import {AppUserLogoState} from "@app/stores/user/user-logo/app-user-logo.state";
import {ApmService} from "@elastic/apm-rum-angular";
import {environment} from "@environments/environment";
import {User} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {AppTourService} from "@shared/services/app-tour.service";
import {SecurityService} from "@shared/services/security.service";
import {
  ApiService,
  defaultResourceStateInitValue,
  isNotNullNorUndefined,
  isNullOrUndefined,
  NotificationService,
  QueryParameters,
  SolidifyAppUserState,
  SolidifyAppUserStateModel,
  SolidifyStateContext,
} from "solidify-frontend";

export const defaultAppUserStateModel: () => AppUserStateModel = () =>
  ({
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
  });

export interface AppUserStateModel extends SolidifyAppUserStateModel<User> {
}

@Injectable()
@State<AppUserStateModel>({
  name: StateEnum.application_user,
  defaults: {
    ...defaultAppUserStateModel(),
  },
  children: [
    AppUserLogoState,
  ],
})
export class AppUserState extends SolidifyAppUserState<AppUserStateModel, User> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _httpClient: HttpClient,
              protected readonly _appTourService: AppTourService,
              private readonly _dialog: MatDialog,
              private readonly _securityService: SecurityService,
              protected readonly _apmService: ApmService) {
    super(_apiService, _store, _notificationService, _actions$, _httpClient, {
      nameSpace: appUserActionNameSpace,
      apiActionEnumAuthenticated: ApiActionNameEnum.AUTHENTICATED,
    }, environment, _apmService);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminUsers;
  }

  @Action(AppUserAction.GetCurrentUserSuccess)
  getCurrentUserSuccess(ctx: SolidifyStateContext<AppUserStateModel>, action: AppUserAction.GetCurrentUserSuccess): void {
    ctx.patchState({
      current: action.user,
    });

    const apm = this._apmService.apm;
    if (isNotNullNorUndefined(apm)) {
      apm.setUserContext({
        id: action.user.resId,
        email: action.user.email,
        username: action.user.firstName + " " + action.user.lastName,
      });
    }

    if (isNotNullNorUndefined(action.user.person)) {
      ctx.dispatch(new AppPersonAction.AddCurrentPerson(action.user.person));
      // ctx.dispatch(new AppPersonInstitutionAction.GetAll(action.user.person.resId));
    }

    if (isNotNullNorUndefined(action.user?.person?.avatar)) {
      ctx.dispatch(new AppUserLogoAction.GetFile(action.user.person.resId));
    } else {
      ctx.dispatch(new AppUserLogoAction.Clean());
    }

    if (isNullOrUndefined(action.user.lastLoginTime)) {
      this._appTourService.runFullTour();
    }
  }
}
