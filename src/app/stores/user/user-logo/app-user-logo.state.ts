/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - app-user-logo.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {Injectable} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {appUserLogoActionNameSpace} from "@app/stores/user/user-logo/app-user-logo.action";
import {environment} from "@environments/environment";
import {User} from "@models";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {AppTourService} from "@shared/services/app-tour.service";
import {SecurityService} from "@shared/services/security.service";
import {
  ApiService,
  defaultResourceFileStateInitValue,
  DownloadService,
  NotificationService,
  QueryParameters,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
} from "solidify-frontend";

export interface AppUserStateModel extends ResourceFileStateModel<User> {
}

@Injectable()
@State<AppUserStateModel>({
  name: StateEnum.application_user_logo,
  defaults: {
    ...defaultResourceFileStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
  },
})
export class AppUserLogoState extends ResourceFileState<AppUserStateModel, User> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService,
              private readonly _appTourService: AppTourService,
              private readonly _dialog: MatDialog,
              private readonly _securityService: SecurityService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: appUserLogoActionNameSpace,
    }, _downloadService, ResourceFileStateModeEnum.avatar, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminUsers;
  }

  protected get _urlFileResource(): string {
    return ApiEnum.adminPeople;
  }
}
