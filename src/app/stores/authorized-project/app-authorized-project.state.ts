/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - app-authorized-project.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {
  AppAuthorizedProjectAction,
  appAuthorizedProjectNameSpace,
} from "@app/stores/authorized-project/app-authorized-project.action";
import {environment} from "@environments/environment";
import {Project} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  defaultResourceStateInitValue,
  MappingObjectUtil,
  NotificationService,
  QueryParameters,
  ResourceState,
  ResourceStateModel,
  SolidifyStateContext,
} from "solidify-frontend";

export interface AppAuthorizedProjectStateModel extends ResourceStateModel<Project> {
}

// WARNING : IN SOME CASE WE NEED TO FILTER TO GET ONLY OPENED ORG UNIT
const getQueryParameter: () => QueryParameters = () => {
  const queryParameters = new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo);
  MappingObjectUtil.set(queryParameters.search.searchItems, "openOnly", "false");
  return queryParameters;
};

@Injectable()
@State<AppAuthorizedProjectStateModel>({
  name: StateEnum.application_authorizedProject,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: getQueryParameter(),
  },
})
export class AppAuthorizedProjectState extends ResourceState<AppAuthorizedProjectStateModel, Project> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: appAuthorizedProjectNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminAuthorizedProjects;
  }

  @Action(AppAuthorizedProjectAction.AddOrUpdateInList)
  addOrUpdateInList(ctx: SolidifyStateContext<AppAuthorizedProjectStateModel>, action: AppAuthorizedProjectAction.AddOrUpdateInList): void {
    const project = action.model;
    const indexExistingProject = ctx.getState().list?.findIndex(o => o.resId === project.resId);
    if (indexExistingProject >= 0) {
      ctx.patchState({
        isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      });
      const simulatedParentAction = new AppAuthorizedProjectAction.AddInListById(project.resId, true, true);
      ctx.dispatch(new AppAuthorizedProjectAction.AddInListByIdSuccess(simulatedParentAction, project, indexExistingProject));
    } else {
      ctx.dispatch(new AppAuthorizedProjectAction.AddInList(project));
    }
  }
}
