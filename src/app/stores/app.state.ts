/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - app.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {
  Inject,
  Injectable,
  LOCALE_ID,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {AppExtendAction} from "@app/stores/app.action";
import {AppAuthorizedProjectAction} from "@app/stores/authorized-project/app-authorized-project.action";
import {
  AppAuthorizedProjectState,
  AppAuthorizedProjectStateModel,
} from "@app/stores/authorized-project/app-authorized-project.state";
import {AppMemberProjectAction} from "@app/stores/member-project/app-member-project.action";
import {
  AppMemberProjectState,
  AppMemberProjectStateModel,
} from "@app/stores/member-project/app-member-project.state";
import {
  AppPersonState,
  AppPersonStateModel,
} from "@app/stores/person/app-person.state";
import {
  AppTocState,
  AppTocStateModel,
} from "@app/stores/toc/app-toc.state";
import {AppUserAction} from "@app/stores/user/app-user.action";
import {
  AppUserState,
  defaultAppUserStateModel,
} from "@app/stores/user/app-user.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  ApplicationRole,
  Person,
  Project,
  SystemProperty,
  User,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {Token} from "@shared/models/token.model";
import {SecurityService} from "@shared/services/security.service";
import {SharedRoleAction} from "@shared/stores/role/shared-role.action";
import {HighlightLoader} from "ngx-highlightjs";
import {
  ApiService,
  APP_OPTIONS,
  AppBannerState,
  AppBannerStateModel,
  AppCarouselState,
  AppConfigService,
  AppDoiState,
  AppSystemPropertyState,
  AppSystemPropertyStateModel,
  defaultSolidifyApplicationAppStateInitValue,
  isNullOrUndefined,
  NativeNotificationService,
  NotificationService,
  OAuth2Service,
  ofSolidifyActionCompleted,
  PollingHelper,
  PrivacyPolicyTermsOfUseApprovalDialog,
  SolidifyAppAction,
  SolidifyApplicationAppOptions,
  SolidifyApplicationAppState,
  SolidifyApplicationAppStateModel,
  SolidifyAppUserStateModel,
  SolidifyStateContext,
  SsrUtil,
  StoreUtil,
  TransferStateService,
} from "solidify-frontend";

export interface AppStateModel extends SolidifyApplicationAppStateModel {
  userRoles: Enums.UserApplicationRole.UserApplicationRoleEnum[];
  [StateEnum.application_person]: AppPersonStateModel | undefined;
  [StateEnum.application_authorizedProject]: AppAuthorizedProjectStateModel | undefined;
  [StateEnum.application_memberProject]: AppMemberProjectStateModel | undefined;
  [StateEnum.application_toc]: AppTocStateModel | undefined;
  [StateEnum.application_systemProperty]: AppSystemPropertyStateModel<SystemProperty>;
  [StateEnum.application_banner]: AppBannerStateModel | undefined;
}

@Injectable()
@State<AppStateModel>({
  name: StateEnum.application,
  defaults: {
    ...defaultSolidifyApplicationAppStateInitValue(),
    theme: environment.theme,
    userRoles: [Enums.UserApplicationRole.UserApplicationRoleEnum.guest],
    [StateEnum.application_person]: undefined,
    [StateEnum.application_authorizedProject]: undefined,
    [StateEnum.application_memberProject]: undefined,
    [StateEnum.application_toc]: undefined,
    [StateEnum.application_systemProperty]: undefined,
    [StateEnum.application_banner]: undefined,
  },
  children: [
    AppUserState,
    AppPersonState,
    AppAuthorizedProjectState,
    AppMemberProjectState,
    AppTocState,
    AppSystemPropertyState<SystemProperty>,
    AppBannerState,
    AppDoiState,
    AppCarouselState,
  ],
})
export class AppState extends SolidifyApplicationAppState<AppStateModel> {
  constructor(@Inject(LOCALE_ID) protected readonly _locale: string,
              protected readonly _store: Store,
              protected readonly _translate: TranslateService,
              protected readonly _oauthService: OAuth2Service,
              protected readonly _actions$: Actions,
              protected readonly _apiService: ApiService,
              protected readonly _httpClient: HttpClient,
              protected readonly _notificationService: NotificationService,
              @Inject(APP_OPTIONS) protected readonly _optionsState: SolidifyApplicationAppOptions,
              protected readonly _hljsLoader: HighlightLoader,
              protected readonly _nativeNotificationService: NativeNotificationService,
              protected readonly _securityService: SecurityService,
              protected readonly _appConfigService: AppConfigService,
              protected readonly _dialog: MatDialog,
              protected readonly _transferState: TransferStateService,
  ) {
    super(_locale,
      _store,
      _translate,
      _oauthService,
      _actions$,
      _apiService,
      _httpClient,
      _notificationService,
      _optionsState,
      environment,
      _hljsLoader,
      _nativeNotificationService,
      _appConfigService,
      _dialog,
      PrivacyPolicyTermsOfUseApprovalDialog,
      _securityService,
      _transferState,
    );
  }

  @Selector()
  static listAuthorizedProject(state: AppStateModel): Project[] {
    return state.application_authorizedProject.list;
  }

  @Selector()
  static listAuthorizedProjectId(state: AppStateModel): string[] {
    const list = this.listAuthorizedProject(state);
    if (isNullOrUndefined(list)) {
      return [];
    }
    return list.map(o => o.resId);
  }

  @Selector()
  static currentPerson(state: AppStateModel): Person {
    return state.application_person.current;
  }

  @Selector()
  static currentUser(state: AppStateModel): User {
    return state.application_user.current;
  }

  @Selector()
  static currentUserApplicationRole(state: AppStateModel): ApplicationRole {
    const currentPersonRole = this.currentUser(state);
    if (isNullOrUndefined(currentPersonRole)) {
      return undefined;
    }
    return currentPersonRole.applicationRole;
  }

  @Selector()
  static currentUserApplicationRoleResId(state: AppStateModel): string | undefined {
    const applicationRole = this.currentUserApplicationRole(state);
    if (isNullOrUndefined(applicationRole)) {
      return undefined;
    }
    return applicationRole.resId;
  }

  @Action(AppExtendAction.StartPollingNotification)
  startPollingNotification(ctx: SolidifyStateContext<AppStateModel>, action: AppExtendAction.StartPollingNotification): void {
    if (SsrUtil.isServer) {
      return;
    }
    if (ctx.getState().isLoggedIn) {
      this._store.dispatch(new AppExtendAction.UpdateNotificationInbox());
    }

    this.subscribe(PollingHelper.startPollingObs({
      waitingTimeBeforeStartInSecond: environment.refreshNotificationInboxAvailableIntervalInSecond,
      initialIntervalRefreshInSecond: environment.refreshNotificationInboxAvailableIntervalInSecond,
      incrementInterval: true,
      resetIntervalWhenUserMouseEvent: true,
      maximumIntervalRefreshInSecond: environment.refreshNotificationInboxAvailableIntervalInSecond * 10,
      filter: () => ctx.getState().isLoggedIn,
      actionToDo: () => {
        this._store.dispatch(new AppExtendAction.UpdateNotificationInbox());
      },
    }));
  }

  override doLoginSuccess(ctx: SolidifyStateContext<AppStateModel>, action: SolidifyAppAction.LoginSuccess): Token {
    const tokenDecoded = super.doLoginSuccess(ctx, action) as Token;

    ctx.patchState({
      isLoggedIn: true,
      userRoles: tokenDecoded.authorities,
      token: tokenDecoded,
    });

    this.subscribe(StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new AppUserAction.GetCurrentUser(),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AppUserAction.GetCurrentUserSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AppUserAction.GetCurrentUserFail)),
        ],
      },
      {
        action: new AppAuthorizedProjectAction.GetAll(),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedProjectAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedProjectAction.GetAllFail)),
        ],
      },
      {
        action: new AppMemberProjectAction.GetAll(),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AppMemberProjectAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AppMemberProjectAction.GetAllFail)),
        ],
      },
      {
        action: new SharedRoleAction.GetAll(undefined, false, false),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedRoleAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedRoleAction.GetAllFail)),
        ],
      },
    ]));

    return tokenDecoded;
  }

  override _treatmentAfterRevokeToken(ctx: SolidifyStateContext<AppStateModel>): void {
    super._treatmentAfterRevokeToken(ctx);
    ctx.patchState({
      ignorePreventLeavePopup: true,
      isLoggedIn: false,
      userRoles: [Enums.UserApplicationRole.UserApplicationRoleEnum.guest],
      token: undefined,
      application_user: {...defaultAppUserStateModel()} as SolidifyAppUserStateModel,
    });
  }
}
