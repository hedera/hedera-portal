/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - app-person.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {
  AppPersonAction,
  appPersonActionNameSpace,
} from "@app/stores/person/app-person.action";
import {AppPersonInstitutionState} from "@app/stores/person/institution/app-people-institution.state";
import {Person} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  defaultResourceStateInitValue,
  DispatchMethodEnum,
  isNullOrUndefined,
  NotificationService,
  OverrideDefaultAction,
  QueryParameters,
  ResourceState,
  ResourceStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
} from "solidify-frontend";
import {environment} from "../../../environments/environment";

export interface AppPersonStateModel extends ResourceStateModel<Person> {
}

@Injectable()
@State<AppPersonStateModel>({
  name: StateEnum.application_person,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
  },
  children: [
    AppPersonInstitutionState,
  ],
})
export class AppPersonState extends ResourceState<AppPersonStateModel, Person> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: appPersonActionNameSpace,
      keepCurrentStateAfterUpdate: true,
      updateSubResourceDispatchMethod: DispatchMethodEnum.PARALLEL,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminPeople;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AppPersonStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AppPersonStateModel): boolean {
    return true;
  }

  // Override method here to form to use url override
  @OverrideDefaultAction()
  @Action(AppPersonAction.GetById)
  getById(ctx: SolidifyStateContext<AppPersonStateModel>, action: AppPersonAction.GetById): Observable<Person> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      current: null,
    });

    return this._apiService.getById<Person>(ApiEnum.adminPeople, action.id)
      .pipe(
        tap((model: Person) => {
          ctx.dispatch(new AppPersonAction.GetByIdSuccess(action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AppPersonAction.GetByIdFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(AppPersonAction.AddCurrentPerson)
  getCurrentUserSuccess(ctx: SolidifyStateContext<AppPersonStateModel>, action: AppPersonAction.AddCurrentPerson): void {
    ctx.patchState({
      current: action.model,
    });
  }
}
