/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - index.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

/* eslint-disable no-restricted-imports */
import {GlobalBanner as GlobalBannerPartial} from "@app/generated-api/model/global-banner.partial.model";
import {License as LicensePartial} from "@app/generated-api/model/license.partial.model";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {KeyValueInfo} from "@shared/models/key-value-info.model";
import {KeyValueLogo} from "@shared/models/key-value-logo.model";
import {
  ColorHexaEnum,
  FacetNamePartialEnum,
  KeyValue,
  MARK_AS_TRANSLATABLE,
  StatusModel,
  StringUtil,
} from "solidify-frontend";
import {Project as ProjectPartial} from "../generated-api/model/project.partial.model";
import {RdfDatasetFile as RdfDatasetFilePartial} from "../generated-api/model/rdf-dataset-file.partial.model";
import {ResearchDataFile as ResearchDataFilePartial} from "../generated-api/model/research-data-file.partial.model";
import {Rml as RmlPartial} from "../generated-api/model/rml.partial.model";
import {SourceDatasetFile as SourceDatasetFilePartial} from "../generated-api/model/source-dataset-file.partial.model";
import {SourceDataset as SourceDatasetPartial} from "../generated-api/model/source-dataset.partial.model";
/* eslint-enable no-restricted-imports */

export namespace Enums {
  export namespace HighlightJS {
    export type HighlightLanguageEnum =
      "xml"
      | "json"
      | "shexc";
    export const HighlightLanguageEnum = {
      xml: "xml" as HighlightLanguageEnum,
      json: "json" as HighlightLanguageEnum,
      shexc: "shexc" as HighlightLanguageEnum,
    };
  }

  export namespace Module {
    export type Name =
      "access"
      | "admin"
      | "authorization"
      | "index"
      | "ingest";
    export const Name = {
      access: "access" as Name,
      admin: "admin" as Name,
      authorization: "authorization" as Name,
      index: "index" as Name,
      ingest: "ingest" as Name,
    };
  }

  export namespace SearchMetadata {
    export type ResearchElementTypeEnum =
      "ResearchObject"
      | "ResearchDataFile";
    export const ResearchElementTypeEnum = {
      ResearchObject: "ResearchObject" as ResearchElementTypeEnum,
      ResearchDataFile: "ResearchDataFile" as ResearchElementTypeEnum,
    };

    export const ResearchElementMetadataEnumTranslate: KeyValue[] = [
      {
        key: ResearchElementTypeEnum.ResearchObject,
        value: MARK_AS_TRANSLATABLE("enum.searchMetadata.researchElementType.researchObject"),
      },
      {
        key: ResearchElementTypeEnum.ResearchDataFile,
        value: MARK_AS_TRANSLATABLE("enum.searchMetadata.researchElementType.researchDataFile"),
      },
    ];
  }

  export namespace Facet {
    type FacetNameExtendEnum =
      "metadata-versions"
      | "projects"
      | "item-types"
      | "object-types"
      | "access-levels"
      | "data-tags"
      | "contributors"
      | "formats"
      | "paths"
      | "types"
      | "retentions"
      | "archive-types"
      | "institutions"
      | "institution-descriptions";
    const FacetNameExtendEnum = {
      METADATA_VERSION_FACET: "metadata-versions" as FacetNameExtendEnum,
      PROJECTS: "projects" as FacetNameExtendEnum,
      ITEM_TYPES: "item-types" as FacetNameExtendEnum,
      OBJECT_TYPES: "object-types" as FacetNameExtendEnum,
      ACCESS_LEVEL_FACET: "access-levels" as FacetNameExtendEnum,
      DATA_TAG_FACET: "data-tags" as FacetNameExtendEnum,
      CREATOR_FACET: "contributors" as FacetNameExtendEnum,
      FORMAT_FACET: "formats" as FacetNameExtendEnum,
      PATH_FACET: "paths" as FacetNameExtendEnum,
      TYPE_FACET: "types" as FacetNameExtendEnum,
      RETENTION_FACET: "retentions" as FacetNameExtendEnum,
      ARCHIVE_FACET: "archive-types" as FacetNameExtendEnum,
      INSTITUION_FACET: "institutions" as FacetNameExtendEnum,
      INSTITUION_DESC_FACET: "institution-descriptions" as FacetNameExtendEnum,
    };
    export const Name = {...FacetNamePartialEnum, ...FacetNameExtendEnum} as Omit<typeof FacetNamePartialEnum, keyof typeof FacetNameExtendEnum> & typeof FacetNameExtendEnum;
    export type Name = typeof Name[keyof typeof Name];
  }

  export namespace SearchCriteria {
    export type OperationTypeEnum =
      "BETWEEN"
      | "CONTAINS"
      | "ENDS_WITH"
      | "EQUALITY"
      | "GREATER_EQUAL"
      | "GREATER_THAN"
      | "LESS_EQUAL"
      | "LESS_THAN"
      | "NEGATION"
      | "ORACLE_CONTAINS"
      | "STARTS_WITH";
    export const OperationTypeEnum = {
      BETWEEN: "BETWEEN" as OperationTypeEnum,
      CONTAINS: "CONTAINS" as OperationTypeEnum,
      ENDS_WITH: "ENDS_WITH" as OperationTypeEnum,
      EQUALITY: "EQUALITY" as OperationTypeEnum,
      GREATER_EQUAL: "GREATER_EQUAL" as OperationTypeEnum,
      GREATER_THAN: "GREATER_THAN" as OperationTypeEnum,
      LESS_EQUAL: "LESS_EQUAL" as OperationTypeEnum,
      LESS_THAN: "LESS_THAN" as OperationTypeEnum,
      NEGATION: "NEGATION" as OperationTypeEnum,
      ORACLE_CONTAINS: "ORACLE_CONTAINS" as OperationTypeEnum,
      STARTS_WITH: "STARTS_WITH" as OperationTypeEnum,
    };
  }

  export namespace SearchCondition {
    export type Type =
      "MATCH"
      | "TERM"
      | "RANGE"
      | "QUERY"
      | "SIMPLE_QUERY"
      | "NESTED_BOOLEAN";

    export const Type = {
      MATCH: "MATCH" as Type,
      TERM: "TERM" as Type,
      RANGE: "RANGE" as Type,
      QUERY: "QUERY" as Type,
      SIMPLE_QUERY: "SIMPLE_QUERY" as Type,
      NESTED_BOOLEAN: "NESTED_BOOLEAN" as Type,
    };

    export type BooleanClauseType =
      "MUST"
      | "FILTER"
      | "SHOULD"
      | "MUST_NOT";

    export const BooleanClauseType = {
      MUST: "MUST" as BooleanClauseType,
      FILTER: "FILTER" as BooleanClauseType,
      SHOULD: "SHOULD" as BooleanClauseType,
      MUST_NOT: "MUST_NOT" as BooleanClauseType,
    };
  }

  export namespace DataFile {
    export namespace Checksum {
      export type AlgoEnum = "CRC32" | "MD5" | "SHA1" | "SHA256";
      export const AlgoEnum = {
        CRC32: "CRC32" as AlgoEnum,
        MD5: "MD5" as AlgoEnum,
        SHA1: "SHA1" as AlgoEnum,
        SHA256: "SHA256" as AlgoEnum,
      };

      export type TypeEnum = "COMPLETE" | "PARTIAL";
      export const ChecksumTypeEnum = {
        COMPLETE: "COMPLETE" as TypeEnum,
        PARTIAL: "PARTIAL" as TypeEnum,
      };

      export type OriginEnum = "HEDERA" | "USER" | "PORTAL";
      export const OriginEnum = {
        HEDERA: "HEDERA" as OriginEnum,
        USER: "USER" as OriginEnum,
        PORTAL: "PORTAL" as OriginEnum,
      };

      export const OriginEnumTranslate: KeyValue[] = [
        {
          key: OriginEnum.HEDERA,
          value: MARK_AS_TRANSLATABLE("checksumOriginEnum.hedera"),
        },
        {
          key: OriginEnum.USER,
          value: MARK_AS_TRANSLATABLE("checksumOriginEnum.user"),
        },
        {
          key: OriginEnum.PORTAL,
          value: MARK_AS_TRANSLATABLE("checksumOriginEnum.portal"),
        },
      ];
    }
  }

  export namespace SourceDataset {
    export type StatusEnum = SourceDatasetPartial.StatusEnum;
    export const StatusEnum = SourceDatasetPartial.StatusEnum;
    export const StatusEnumTranslate: StatusModel[] = [
      {
        key: StatusEnum.ACTIVE,
        value: MARK_AS_TRANSLATABLE("enum.sourceDataset.status.active"),
        backgroundColorHexa: ColorHexaEnum.orange,
      },
      {
        key: StatusEnum.PRE_PUBLISHED,
        value: MARK_AS_TRANSLATABLE("enum.sourceDataset.status.prePublished"),
        backgroundColorHexa: ColorHexaEnum.blue,
      },
      {
        key: StatusEnum.PUBLISHED,
        value: MARK_AS_TRANSLATABLE("enum.sourceDataset.status.published"),
        backgroundColorHexa: ColorHexaEnum.green,
      },
    ];
  }

  export namespace SourceDataFile {
    export type TypeEnum = SourceDatasetFilePartial.TypeEnum;
    export const TypeEnum = SourceDatasetFilePartial.TypeEnum;
    export const TypeEnumTranslate: KeyValue[] = [
      {
        key: TypeEnum.CSV,
        value: MARK_AS_TRANSLATABLE("enum.sourceDataFile.type.csv"),
      },
      {
        key: TypeEnum.XML,
        value: MARK_AS_TRANSLATABLE("enum.sourceDataFile.type.xml"),
      },
      {
        key: TypeEnum.JSON,
        value: MARK_AS_TRANSLATABLE("enum.sourceDataFile.type.json"),
      },
      {
        key: TypeEnum.RDF,
        value: MARK_AS_TRANSLATABLE("enum.sourceDataFile.type.rdf"),
      },
    ];

    export type StatusEnum = SourceDatasetFilePartial.StatusEnum;
    export const StatusEnum = SourceDatasetFilePartial.StatusEnum;
    export const StatusEnumTranslate: StatusModel[] = [
      {
        key: StatusEnum.NOT_TRANSFORMED,
        value: MARK_AS_TRANSLATABLE("enum.sourceDatasetFile.status.notTransformed"),
        backgroundColorHexa: ColorHexaEnum.orange,
      },
      {
        key: StatusEnum.TO_TRANSFORM,
        value: MARK_AS_TRANSLATABLE("enum.sourceDatasetFile.status.toTransform"),
      },
      {
        key: StatusEnum.TRANSFORMING,
        value: MARK_AS_TRANSLATABLE("enum.sourceDatasetFile.status.transforming"),
      },
      {
        key: StatusEnum.TRANSFORMED,
        value: MARK_AS_TRANSLATABLE("enum.sourceDatasetFile.status.transformed"),
        backgroundColorHexa: ColorHexaEnum.green,
      },
      {
        key: StatusEnum.TO_CHECK,
        value: MARK_AS_TRANSLATABLE("enum.sourceDatasetFile.status.toCheck"),
      },
      {
        key: StatusEnum.CHECKING,
        value: MARK_AS_TRANSLATABLE("enum.sourceDatasetFile.status.checking"),
      },
      {
        key: StatusEnum.CHECKED,
        value: MARK_AS_TRANSLATABLE("enum.sourceDatasetFile.status.checked"),
        backgroundColorHexa: ColorHexaEnum.green,
      },
      {
        key: StatusEnum.IN_ERROR,
        value: MARK_AS_TRANSLATABLE("enum.sourceDatasetFile.status.inError"),
        backgroundColorHexa: ColorHexaEnum.red,
      },
    ];

  }

  export namespace ResearchObject {
    export type ObjectTypeEnum = "File" | "Manifest" | "Managed" | "Other";
    export const ObjectTypeEnum = {
      FILE: "File" as ObjectTypeEnum,
      MANIFEST: "Manifest" as ObjectTypeEnum,
      MANAGED: "Managed" as ObjectTypeEnum,
      OTHER: "Other" as ObjectTypeEnum,
    };
    export const ObjectTypeEnumTranslate: KeyValueLogo[] = [
      {
        key: ObjectTypeEnum.FILE,
        value: MARK_AS_TRANSLATABLE("enum.researchObject.objectTypeEnum.file"),
        icon: IconNameEnum.files,
      },
      {
        key: ObjectTypeEnum.MANIFEST,
        value: MARK_AS_TRANSLATABLE("enum.researchObject.objectTypeEnum.manifest"),
        icon: IconNameEnum.manifest,
      },
      {
        key: ObjectTypeEnum.MANAGED,
        value: MARK_AS_TRANSLATABLE("enum.researchObject.objectTypeEnum.managed"),
        icon: IconNameEnum.managed,
      },
      {
        key: ObjectTypeEnum.OTHER,
        value: MARK_AS_TRANSLATABLE("enum.researchObject.objectTypeEnum.other"),
        icon: IconNameEnum.other,
      },
    ];
  }

  export namespace ResearchDataFile {
    export type AccessibleFromEnum = ResearchDataFilePartial.AccessibleFromEnum;
    export const AccessibleFromEnum = ResearchDataFilePartial.AccessibleFromEnum;
    export const AccessibleFromEnumTranslate: KeyValue[] = [
      {
        key: AccessibleFromEnum.IIIF,
        value: MARK_AS_TRANSLATABLE("enum.researchDataFile.accessibleFrom.iiif"),
      },
      {
        key: AccessibleFromEnum.IIIF_MANIFEST,
        value: MARK_AS_TRANSLATABLE("enum.researchDataFile.accessibleFrom.iiifManifest"),
      },
      {
        key: AccessibleFromEnum.TEI,
        value: MARK_AS_TRANSLATABLE("enum.researchDataFile.accessibleFrom.tei"),
      },
      {
        key: AccessibleFromEnum.WEB,
        value: MARK_AS_TRANSLATABLE("enum.researchDataFile.accessibleFrom.web"),
      },
    ];
    export type StatusEnum = ResearchDataFilePartial.StatusEnum;
    export const StatusEnum = ResearchDataFilePartial.StatusEnum;
    export const StatusEnumTranslate: StatusModel[] = [
      {
        key: StatusEnum.CREATED,
        value: MARK_AS_TRANSLATABLE("enum.researchDataFile.status.created"),
      },
      {
        key: StatusEnum.UPLOADED,
        value: MARK_AS_TRANSLATABLE("enum.researchDataFile.status.uploaded"),
        backgroundColorHexa: ColorHexaEnum.orange,
      },
      {
        key: StatusEnum.PROCESSING,
        value: MARK_AS_TRANSLATABLE("enum.researchDataFile.status.processing"),
      },
      {
        key: StatusEnum.REFERENCED,
        value: MARK_AS_TRANSLATABLE("enum.researchDataFile.status.referenced"),
      },
      {
        key: StatusEnum.STORED,
        value: MARK_AS_TRANSLATABLE("enum.researchDataFile.status.stored"),
      },
      {
        key: StatusEnum.IN_ERROR,
        value: MARK_AS_TRANSLATABLE("enum.researchDataFile.status.inError"),
        backgroundColorHexa: ColorHexaEnum.red,
      },
      {
        key: StatusEnum.COMPLETED,
        value: MARK_AS_TRANSLATABLE("enum.researchDataFile.status.completed"),
        backgroundColorHexa: ColorHexaEnum.green,
      },
      {
        key: StatusEnum.CHANGE_RELATIVE_LOCATION,
        value: MARK_AS_TRANSLATABLE("enum.researchDataFile.status.changeRelativeLocation"),
      },
    ];

  }

  export namespace Access {
    export type RdfFormatEnum = "TURTLE" | "N-TRIPLES" | "JSON-LD" | "RDF/XML" | "RDF/JSON" | "N3";
    export const RdfFormatEnum = {
      TURTLE: "TURTLE" as RdfFormatEnum,
      N_TRIPLES: "N-TRIPLES" as RdfFormatEnum,
      JSON_LD: "JSON-LD" as RdfFormatEnum,
      RDF_XML: "RDF/XML" as RdfFormatEnum,
      RDF_JSON: "RDF/JSON" as RdfFormatEnum,
      N3: "N3" as RdfFormatEnum,
    };

    export const RdfFormatEnumTranslate: KeyValue[] = [
      {
        key: RdfFormatEnum.TURTLE,
        value: MARK_AS_TRANSLATABLE("enum.access.formatRdf.turtle"),
      },
      {
        key: RdfFormatEnum.N_TRIPLES,
        value: MARK_AS_TRANSLATABLE("enum.access.formatRdf.nTriples"),
      },
      {
        key: RdfFormatEnum.JSON_LD,
        value: MARK_AS_TRANSLATABLE("enum.access.formatRdf.jsonLd"),
      },
      {
        key: RdfFormatEnum.RDF_XML,
        value: MARK_AS_TRANSLATABLE("enum.access.formatRdf.rdfXml"),
      },
      {
        key: RdfFormatEnum.RDF_JSON,
        value: MARK_AS_TRANSLATABLE("enum.access.formatRdf.rdfJson"),
      },
      {
        key: RdfFormatEnum.N3,
        value: MARK_AS_TRANSLATABLE("enum.access.formatRdf.n3"),
      },
    ];

    export type AccessEnum = "PUBLIC" | "RESTRICTED" | "CLOSED";
    export const AccessEnum = {
      PUBLIC: "PUBLIC" as AccessEnum,
      RESTRICTED: "RESTRICTED" as AccessEnum,
      CLOSED: "CLOSED" as AccessEnum,
    };
    export const AccessEnumTranslate: KeyValue[] = [
      {
        key: AccessEnum.PUBLIC,
        value: MARK_AS_TRANSLATABLE("enum.access.public"),
      },
      {
        key: AccessEnum.RESTRICTED,
        value: MARK_AS_TRANSLATABLE("enum.access.restricted"),
      },
      {
        key: AccessEnum.CLOSED,
        value: MARK_AS_TRANSLATABLE("enum.access.closed"),
      },
    ];

    export const EmbargoAccessLevelEnum: KeyValue[] = AccessEnumTranslate;

    export const EmbargoAccessLevelEnumList: (access: AccessEnum) => KeyValue[] = access => {
      if (access === AccessEnum.PUBLIC) {
        return [
          {
            key: AccessEnum.RESTRICTED,
            value: MARK_AS_TRANSLATABLE("enum.access.restricted"),
          },
          {
            key: AccessEnum.CLOSED,
            value: MARK_AS_TRANSLATABLE("enum.access.closed"),
          },
        ];
      } else if (access === AccessEnum.RESTRICTED) {
        return [
          {
            key: AccessEnum.CLOSED,
            value: MARK_AS_TRANSLATABLE("enum.access.closed"),
          },
        ];
      } else {
        return [];
      }
    };
  }

  export namespace DataSensitivity {
    export type DataSensitivityEnum = "UNDEFINED" | "BLUE" | "GREEN" | "YELLOW" | "ORANGE" | "RED" | "CRIMSON";
    export const DataSensitivityEnum = {
      UNDEFINED: "UNDEFINED" as DataSensitivityEnum,
      BLUE: "BLUE" as DataSensitivityEnum,
      GREEN: "GREEN" as DataSensitivityEnum,
      YELLOW: "YELLOW" as DataSensitivityEnum,
      ORANGE: "ORANGE" as DataSensitivityEnum,
      RED: "RED" as DataSensitivityEnum,
      CRIMSON: "CRIMSON" as DataSensitivityEnum,
    };

    export const DataSensitivityEnumTranslate: KeyValueInfo[] = [
      {
        key: DataSensitivityEnum.UNDEFINED,
        value: MARK_AS_TRANSLATABLE("enum.dataSensitivity.undefined"),
      },
      {
        key: DataSensitivityEnum.BLUE,
        value: MARK_AS_TRANSLATABLE("enum.dataSensitivity.blue"),
        infoToTranslate: MARK_AS_TRANSLATABLE("enum.dataSensitivityInfo.blue"),
      },
      {
        key: DataSensitivityEnum.GREEN,
        value: MARK_AS_TRANSLATABLE("enum.dataSensitivity.green"),
        infoToTranslate: MARK_AS_TRANSLATABLE("enum.dataSensitivityInfo.green"),
      },
      {
        key: DataSensitivityEnum.YELLOW,
        value: MARK_AS_TRANSLATABLE("enum.dataSensitivity.yellow"),
        infoToTranslate: MARK_AS_TRANSLATABLE("enum.dataSensitivityInfo.yellow"),
      },
      {
        key: DataSensitivityEnum.ORANGE,
        value: MARK_AS_TRANSLATABLE("enum.dataSensitivity.orange"),
        infoToTranslate: MARK_AS_TRANSLATABLE("enum.dataSensitivityInfo.orange"),
      },
      {
        key: DataSensitivityEnum.RED,
        value: MARK_AS_TRANSLATABLE("enum.dataSensitivity.red"),
        infoToTranslate: MARK_AS_TRANSLATABLE("enum.dataSensitivityInfo.red"),
      },
      {
        key: DataSensitivityEnum.CRIMSON,
        value: MARK_AS_TRANSLATABLE("enum.dataSensitivity.crimson"),
        infoToTranslate: MARK_AS_TRANSLATABLE("enum.dataSensitivityInfo.crimson"),
      },
    ];
  }

  export namespace Language {
    export type LanguageEnum =
      "fr"
      | "de"
      | "en";
    export const LanguageEnum = {
      fr: "fr" as LanguageEnum,
      de: "de" as LanguageEnum,
      en: "en" as LanguageEnum,
    };

    export const LanguageEnumTranslate: KeyValue[] = [
      {
        key: LanguageEnum.en,
        value: MARK_AS_TRANSLATABLE("language.english"),
      },
      {
        key: LanguageEnum.de,
        value: MARK_AS_TRANSLATABLE("language.german"),
      },
      {
        key: LanguageEnum.fr,
        value: MARK_AS_TRANSLATABLE("language.french"),
      },
    ];
  }

  export namespace License {
    export type OdConformanceEnum = LicensePartial.OdConformanceEnum;
    export const OdConformanceEnum = LicensePartial.OdConformanceEnum;
    export const OdConformanceEnumTranslate: KeyValue[] = [
      {
        key: License.OdConformanceEnum.EMPTY,
        value: StringUtil.stringEmpty,
      },
      {
        key: License.OdConformanceEnum.APPROVED,
        value: MARK_AS_TRANSLATABLE("enum.license.odConformance.approved"),
      },
      {
        key: License.OdConformanceEnum.NOT_REVIEWED,
        value: MARK_AS_TRANSLATABLE("enum.license.odConformance.notReviewed"),
      },
      {
        key: License.OdConformanceEnum.REJECTED,
        value: MARK_AS_TRANSLATABLE("enum.license.odConformance.rejected"),
      },
    ];
    export type OsdConformanceEnum = LicensePartial.OsdConformanceEnum;
    export const OsdConformanceEnum = LicensePartial.OsdConformanceEnum;
    export const OsdConformanceEnumTranslate: KeyValue[] = [
      {
        key: License.OsdConformanceEnum.EMPTY,
        value: StringUtil.stringEmpty,
      },
      {
        key: License.OsdConformanceEnum.APPROVED,
        value: MARK_AS_TRANSLATABLE("enum.license.osdConformance.approved"),
      },
      {
        key: License.OsdConformanceEnum.NOT_REVIEWED,
        value: MARK_AS_TRANSLATABLE("enum.license.osdConformance.notReviewed"),
      },
      {
        key: License.OsdConformanceEnum.REJECTED,
        value: MARK_AS_TRANSLATABLE("enum.license.osdConformance.rejected"),
      },
    ];
    export type StatusEnum = LicensePartial.StatusEnum;
    export const StatusEnum = LicensePartial.StatusEnum;
    export const StatusEnumTranslate: KeyValue[] = [
      {
        key: License.StatusEnum.ACTIVE,
        value: MARK_AS_TRANSLATABLE("enum.license.status.active"),
      },
      {
        key: License.StatusEnum.SUPERSEDED,
        value: MARK_AS_TRANSLATABLE("enum.license.status.superseded"),
      },
      {
        key: License.StatusEnum.RETIRED,
        value: MARK_AS_TRANSLATABLE("enum.license.status.retired"),
      },
    ];
  }

  export namespace MetadataType {
    export type MetadataFormatEnum = "CUSTOM" | "JSON" | "SCHEMA_LESS" | "XML";
    export const MetadataFormatEnum = {
      CUSTOM: "CUSTOM" as MetadataFormatEnum,
      JSON: "JSON" as MetadataFormatEnum,
      SCHEMA_LESS: "SCHEMA_LESS" as MetadataFormatEnum,
      XML: "XML" as MetadataFormatEnum,
    };
    export const MetadataFormatEnumTranslate: KeyValue[] = [
      {
        key: MetadataType.MetadataFormatEnum.SCHEMA_LESS,
        value: MARK_AS_TRANSLATABLE("enum.metadataFormatEnum.schemaless"),
      },
      {
        key: MetadataType.MetadataFormatEnum.JSON,
        value: MARK_AS_TRANSLATABLE("enum.metadataFormatEnum.json"),
      },
      {
        key: MetadataType.MetadataFormatEnum.XML,
        value: MARK_AS_TRANSLATABLE("enum.metadataFormatEnum.xml"),
      },
      {
        key: MetadataType.MetadataFormatEnum.CUSTOM,
        value: MARK_AS_TRANSLATABLE("enum.metadataFormatEnum.custom"),
      },
    ];
  }

  export namespace Notification {
    export type StatusEnum = "PROCESSED" | "PENDING" | "REFUSED";
    export const StatusEnum = {
      PROCESSED: "PROCESSED" as StatusEnum,
      PENDING: "PENDING" as StatusEnum,
      REFUSED: "REFUSED" as StatusEnum,
    };
    export const StatusEnumTranslate: StatusModel[] = [
      {
        key: StatusEnum.PROCESSED,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationStatus.processed"),
        backgroundColorHexa: ColorHexaEnum.green,
      },
      {
        key: StatusEnum.REFUSED,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationStatus.refused"),
        backgroundColorHexa: ColorHexaEnum.red,
      },
      {
        key: StatusEnum.PENDING,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationStatus.pending"),
        backgroundColorHexa: ColorHexaEnum.blue,
      },
    ];

    export type CategoryEnum = "INFO" | "REQUEST";
    export const CategoryEnum = {
      INFO: "INFO" as CategoryEnum,
      REQUEST: "REQUEST" as CategoryEnum,
    };
    export const CategoryEnumTranslate: KeyValue[] = [
      {
        key: CategoryEnum.REQUEST,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationCategory.request"),
      },
      {
        key: CategoryEnum.INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationCategory.info"),
      },
    ];

    export type TypeEnum =
      "ACCESS_DATASET_REQUEST"
      | "JOIN_ORGUNIT_REQUEST"
      | "CREATE_ORGUNIT_REQUEST"
      | "IN_ERROR_SIP_INFO"
      | "IN_ERROR_AIP_INFO"
      | "VALIDATE_DEPOSIT_REQUEST"
      | "MY_APPROVED_DEPOSIT_INFO"
      | "MY_COMPLETED_DEPOSIT_INFO"
      | "MY_INDIRECT_COMPLETED_DEPOSIT_INFO"
      | "CREATED_DEPOSIT_INFO"
      | "IN_ERROR_DEPOSIT_INFO"
      | "IN_ERROR_DIP_INFO"
      | "IN_ERROR_DOWNLOADED_AIP_INFO"
      | "APPROVE_DISPOSAL_REQUEST"
      | "APPROVE_DISPOSAL_BY_ORGUNIT_REQUEST"
      | "MY_COMPLETED_ARCHIVE_INFO"
      | "COMPLETED_ARCHIVE_INFO";
    export const TypeEnum = {
      ACCESS_DATASET_REQUEST: "ACCESS_DATASET_REQUEST" as TypeEnum,
      JOIN_ORGUNIT_REQUEST: "JOIN_ORGUNIT_REQUEST" as TypeEnum,
      CREATE_ORGUNIT_REQUEST: "CREATE_ORGUNIT_REQUEST" as TypeEnum,
      IN_ERROR_SIP_INFO: "IN_ERROR_SIP_INFO" as TypeEnum,
      IN_ERROR_AIP_INFO: "IN_ERROR_AIP_INFO" as TypeEnum,
      VALIDATE_DEPOSIT_REQUEST: "VALIDATE_DEPOSIT_REQUEST" as TypeEnum,
      MY_APPROVED_DEPOSIT_INFO: "MY_APPROVED_DEPOSIT_INFO" as TypeEnum,
      MY_COMPLETED_DEPOSIT_INFO: "MY_COMPLETED_DEPOSIT_INFO" as TypeEnum,
      MY_INDIRECT_COMPLETED_DEPOSIT_INFO: "MY_INDIRECT_COMPLETED_DEPOSIT_INFO" as TypeEnum,
      CREATED_DEPOSIT_INFO: "CREATED_DEPOSIT_INFO" as TypeEnum,
      IN_ERROR_DEPOSIT_INFO: "IN_ERROR_DEPOSIT_INFO" as TypeEnum,
      IN_ERROR_DIP_INFO: "IN_ERROR_DIP_INFO" as TypeEnum,
      IN_ERROR_DOWNLOADED_AIP_INFO: "IN_ERROR_DOWNLOADED_AIP_INFO" as TypeEnum,
      APPROVE_DISPOSAL_REQUEST: "APPROVE_DISPOSAL_REQUEST" as TypeEnum,
      APPROVE_DISPOSAL_BY_ORGUNIT_REQUEST: "APPROVE_DISPOSAL_BY_ORGUNIT_REQUEST" as TypeEnum,
      MY_COMPLETED_ARCHIVE_INFO: "MY_COMPLETED_ARCHIVE_INFO" as TypeEnum,
      COMPLETED_ARCHIVE_INFO: "COMPLETED_ARCHIVE_INFO" as TypeEnum,
    };
    export const TypeEnumTranslate: KeyValue[] = [
      {
        key: TypeEnum.ACCESS_DATASET_REQUEST,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.accessDatasetRequest"),
      },
      {
        key: TypeEnum.JOIN_ORGUNIT_REQUEST,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.joinOrgUnitRequest"),
      },
      {
        key: TypeEnum.CREATE_ORGUNIT_REQUEST,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.createOrgUnitRequest"),
      },
      {
        key: TypeEnum.IN_ERROR_SIP_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.inErrorSipInfo"),
      },
      {
        key: TypeEnum.IN_ERROR_AIP_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.inErrorAipInfo"),
      },
      {
        key: TypeEnum.VALIDATE_DEPOSIT_REQUEST,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.validateDepositRequest"),
      },
      {
        key: TypeEnum.MY_APPROVED_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.myApprovedDepositInfo"),
      },
      {
        key: TypeEnum.MY_COMPLETED_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.myCompletedDepositInfo"),
      },
      {
        key: TypeEnum.MY_INDIRECT_COMPLETED_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.myIndirectCompletedDepositInfo"),
      },
      {
        key: TypeEnum.CREATED_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.createdDepositInfo"),
      },
      {
        key: TypeEnum.IN_ERROR_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.inErrorDepositInfo"),
      },
      {
        key: TypeEnum.IN_ERROR_DIP_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.inErrorDipInfo"),
      },
      {
        key: TypeEnum.IN_ERROR_DOWNLOADED_AIP_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.inErrorDownloadedAipInfo"),
      },
      {
        key: TypeEnum.APPROVE_DISPOSAL_REQUEST,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.approveDisposalRequest"),
      },
      {
        key: TypeEnum.APPROVE_DISPOSAL_BY_ORGUNIT_REQUEST,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.approveDisposalByOrgunitRequest"),
      },
      {
        key: TypeEnum.MY_COMPLETED_ARCHIVE_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.myCompletedArchiveInfo"),
      },
      {
        key: TypeEnum.COMPLETED_ARCHIVE_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.completedArchiveInfo"),
      },
    ];
  }

  export namespace Archive {
    export type RatingTypeEnum =
      "QUALITY"
      | "USEFULNESS";
    export const RatingTypeEnum = {
      QUALITY: "QUALITY" as RatingTypeEnum,
      USEFULNESS: "USEFULNESS" as RatingTypeEnum,
    };
    export const RatingTypeEnumTranslate: KeyValue[] = [
      {
        key: RatingTypeEnum.QUALITY,
        value: MARK_AS_TRANSLATABLE("home.archive.ranking.quality"),
      },
      {
        key: RatingTypeEnum.USEFULNESS,
        value: MARK_AS_TRANSLATABLE("home.archive.ranking.usefulness"),
      },
    ];
  }

  export namespace ScheduledTask {
    export type TaskTypeEnum =
      "NOTIFY_DEPOSITS_TO_VALIDATE"
      | "NOTIFY_APPROVED_DEPOSITS_CREATOR"
      | "NOTIFY_COMPLETED_DEPOSITS_CREATOR"
      | "NOTIFY_COMPLETED_DEPOSITS_CONTRIBUTORS"
      | "NOTIFY_FORGOTTEN_DEPOSITS_TO_VALIDATE"
      | "NOTIFY_DEPOSITS_IN_ERROR"
      | "NOTIFY_COMPLETED_ARCHIVES_CREATOR"
      | "NOTIFY_COMPLETED_ARCHIVES_APPROVERS"
      | "NOTIFY_ORG_UNIT_CREATION_TO_VALIDATE"
      | "NOTIFY_JOIN_MEMBER_ORG_UNIT_TO_VALIDATE";
    export const TaskTypeEnum = {
      DEPOSITS_TO_VALIDATE: "NOTIFY_DEPOSITS_TO_VALIDATE" as TaskTypeEnum,
      APPROVED_DEPOSITS_CREATOR: "NOTIFY_APPROVED_DEPOSITS_CREATOR" as TaskTypeEnum,
      COMPLETED_DEPOSITS_CREATOR: "NOTIFY_COMPLETED_DEPOSITS_CREATOR" as TaskTypeEnum,
      COMPLETED_DEPOSITS_CONTRIBUTORS: "NOTIFY_COMPLETED_DEPOSITS_CONTRIBUTORS" as TaskTypeEnum,
      FORGOTTEN_DEPOSITS_TO_VALIDATE: "NOTIFY_FORGOTTEN_DEPOSITS_TO_VALIDATE" as TaskTypeEnum,
      DEPOSITS_IN_ERROR: "NOTIFY_DEPOSITS_IN_ERROR" as TaskTypeEnum,
      COMPLETED_ARCHIVES_CREATOR: "NOTIFY_COMPLETED_ARCHIVES_CREATOR" as TaskTypeEnum,
      COMPLETED_ARCHIVES_APPROVERS: "NOTIFY_COMPLETED_ARCHIVES_APPROVERS" as TaskTypeEnum,
      ORG_UNIT_CREATION_TO_VALIDATE: "NOTIFY_ORG_UNIT_CREATION_TO_VALIDATE" as TaskTypeEnum,
      JOIN_MEMBER_ORG_UNIT_TO_VALIDATE: "NOTIFY_JOIN_MEMBER_ORG_UNIT_TO_VALIDATE" as TaskTypeEnum,
    };

    export const TaskTypeEnumTranslate: KeyValue[] = [
      {
        key: TaskTypeEnum.DEPOSITS_TO_VALIDATE,
        value: MARK_AS_TRANSLATABLE("enum.scheduledTask.taskType.depositsToValidate"),
      },
      {
        key: TaskTypeEnum.APPROVED_DEPOSITS_CREATOR,
        value: MARK_AS_TRANSLATABLE("enum.scheduledTask.taskType.approvedDepositsCreator"),
      },
      {
        key: TaskTypeEnum.COMPLETED_DEPOSITS_CREATOR,
        value: MARK_AS_TRANSLATABLE("enum.scheduledTask.taskType.completedDepositsCreator"),
      },
      {
        key: TaskTypeEnum.COMPLETED_DEPOSITS_CONTRIBUTORS,
        value: MARK_AS_TRANSLATABLE("enum.scheduledTask.taskType.completedDepositsContributors"),
      },
      {
        key: TaskTypeEnum.DEPOSITS_IN_ERROR,
        value: MARK_AS_TRANSLATABLE("enum.scheduledTask.taskType.depositsInError"),
      },
      {
        key: TaskTypeEnum.COMPLETED_ARCHIVES_CREATOR,
        value: MARK_AS_TRANSLATABLE("enum.scheduledTask.taskType.completedArchivesCreator"),
      },
      {
        key: TaskTypeEnum.COMPLETED_ARCHIVES_APPROVERS,
        value: MARK_AS_TRANSLATABLE("enum.scheduledTask.taskType.completedArchivesApprovers"),
      },
      {
        key: TaskTypeEnum.ORG_UNIT_CREATION_TO_VALIDATE,
        value: MARK_AS_TRANSLATABLE("enum.scheduledTask.taskType.orgUnitCreationToValidate"),
      },
      {
        key: TaskTypeEnum.JOIN_MEMBER_ORG_UNIT_TO_VALIDATE,
        value: MARK_AS_TRANSLATABLE("enum.scheduledTask.taskType.joinMemberOrgUnitToValidate"),
      },
    ];
  }

  export namespace Role {
    export type RoleEnum =
      "CREATOR"
      | "MANAGER"
      | "VISITOR";
    export const RoleEnum = {
      CREATOR: "CREATOR" as RoleEnum,
      MANAGER: "MANAGER" as RoleEnum,
      VISITOR: "VISITOR" as RoleEnum,
    };
  }

  export namespace UserApplicationRole {
    export type UserApplicationRoleEnum =
      "ROOT"
      | "ADMIN"
      | "USER"
      | "TRUSTED_CLIENT"
      | "GUEST";
    export const UserApplicationRoleEnum = {
      root: "ROOT" as UserApplicationRoleEnum,
      admin: "ADMIN" as UserApplicationRoleEnum,
      user: "USER" as UserApplicationRoleEnum,
      trusted_client: "TRUSTED_CLIENT" as UserApplicationRoleEnum,
      guest: "GUEST" as UserApplicationRoleEnum,
    };
    export const UserApplicationRoleEnumTranslate: KeyValue[] = [
      {
        key: UserApplicationRoleEnum.admin,
        value: MARK_AS_TRANSLATABLE("admin.user.roles.admin"),
      },
      {
        key: UserApplicationRoleEnum.root,
        value: MARK_AS_TRANSLATABLE("admin.user.roles.root"),
      },
      {
        key: UserApplicationRoleEnum.user,
        value: MARK_AS_TRANSLATABLE("admin.user.roles.user"),
      },
      {
        key: UserApplicationRoleEnum.trusted_client,
        value: MARK_AS_TRANSLATABLE("admin.user.roles.trustedClient"),
      },
    ];
  }

  export namespace GlobalBanner {
    export type TypeEnum = GlobalBannerPartial.TypeEnum;
    export const TypeEnum = GlobalBannerPartial.TypeEnum;
    export const TypeEnumTranslate: KeyValue[] = [
      {
        key: TypeEnum.CRITICAL,
        value: MARK_AS_TRANSLATABLE("enum.globalBanner.type.critical"),
      },
      {
        key: TypeEnum.WARNING,
        value: MARK_AS_TRANSLATABLE("enum.globalBanner.type.warning"),
      },
      {
        key: TypeEnum.INFO,
        value: MARK_AS_TRANSLATABLE("enum.globalBanner.type.info"),
      },
    ];
  }

  export namespace NotificationType {
    export type NotificationTypeEnum =
      "VALIDATE_DEPOSIT_REQUEST"
      | "MY_APPROVED_DEPOSIT_INFO"
      | "MY_COMPLETED_DEPOSIT_INFO"
      | "IN_ERROR_DEPOSIT_INFO"
      | "MY_INDIRECT_COMPLETED_DEPOSIT_INFO"
      | "MY_COMPLETED_ARCHIVE_INFO"
      | "COMPLETED_ARCHIVE_INFO"
      | "CREATE_ORGUNIT_REQUEST"
      | "JOIN_ORGUNIT_REQUEST";
    export const NotificationTypeEnum = {
      VALIDATE_DEPOSIT_REQUEST: "VALIDATE_DEPOSIT_REQUEST" as NotificationTypeEnum,
      MY_APPROVED_DEPOSIT_INFO: "MY_APPROVED_DEPOSIT_INFO" as NotificationTypeEnum,
      MY_COMPLETED_DEPOSIT_INFO: "MY_COMPLETED_DEPOSIT_INFO" as NotificationTypeEnum,
      IN_ERROR_DEPOSIT_INFO: "IN_ERROR_DEPOSIT_INFO" as NotificationTypeEnum,
      MY_INDIRECT_COMPLETED_DEPOSIT_INFO: "MY_INDIRECT_COMPLETED_DEPOSIT_INFO" as NotificationTypeEnum,
      MY_COMPLETED_ARCHIVE_INFO: "MY_COMPLETED_ARCHIVE_INFO" as NotificationTypeEnum,
      COMPLETED_ARCHIVE_INFO: "COMPLETED_ARCHIVE_INFO" as NotificationTypeEnum,
      CREATE_ORGUNIT_REQUEST: "CREATE_ORGUNIT_REQUEST" as NotificationTypeEnum,
      JOIN_ORGUNIT_REQUEST: "JOIN_ORGUNIT_REQUEST" as NotificationTypeEnum,
    };
    export const NotificationTypeEnumTranslate: KeyValue[] = [
      {
        key: NotificationTypeEnum.MY_APPROVED_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.myApprovedDepositInfo"),
      },
      {
        key: NotificationTypeEnum.MY_COMPLETED_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.myCompletedDepositInfo"),
      },
      {
        key: NotificationTypeEnum.MY_COMPLETED_ARCHIVE_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.myCompletedArchiveInfo"),
      },
      {
        key: NotificationTypeEnum.MY_INDIRECT_COMPLETED_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.myIndirectCompletedDepositInfo"),
      },
      {
        key: NotificationTypeEnum.VALIDATE_DEPOSIT_REQUEST,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.validateDepositRequest"),
      },
      {
        key: NotificationTypeEnum.COMPLETED_ARCHIVE_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.completedArchiveInfo"),
      },
      {
        key: NotificationTypeEnum.IN_ERROR_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.inErrorDepositInfo"),
      },
      {
        key: NotificationTypeEnum.CREATE_ORGUNIT_REQUEST,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.createOrgUnitRequest"),
      },
      {
        key: NotificationTypeEnum.JOIN_ORGUNIT_REQUEST,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.joinOrgUnitRequest"),
      },
    ];
    export const NotificationTypeShortEnumTranslate: KeyValue[] = [
      {
        key: NotificationTypeEnum.MY_APPROVED_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.myApprovedDepositInfo"),
      },
      {
        key: NotificationTypeEnum.MY_COMPLETED_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.myCompletedDepositInfo"),
      },
      {
        key: NotificationTypeEnum.MY_COMPLETED_ARCHIVE_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.myCompletedArchiveInfo"),
      },
      {
        key: NotificationTypeEnum.MY_INDIRECT_COMPLETED_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.myIndirectCompletedDepositInfo"),
      },
      {
        key: NotificationTypeEnum.VALIDATE_DEPOSIT_REQUEST,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.validateDepositRequest"),
      },
      {
        key: NotificationTypeEnum.COMPLETED_ARCHIVE_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.completedArchiveInfo"),
      },
      {
        key: NotificationTypeEnum.IN_ERROR_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.inErrorDepositInfo"),
      },
      {
        key: NotificationTypeEnum.CREATE_ORGUNIT_REQUEST,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.createOrgUnitRequest"),
      },
      {
        key: NotificationTypeEnum.JOIN_ORGUNIT_REQUEST,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.joinOrgUnitRequest"),
      },
    ];
  }

  export type IdentifiersEnum =
    "CrossrefFunder"
    | "GRID"
    | "ISNI"
    | "ROR"
    | "SPDX"
    | "WEB"
    | "Wikidata";
  export const IdentifiersEnum = {
    CrossrefFunder: "CrossrefFunder" as IdentifiersEnum,
    GRID: "GRID" as IdentifiersEnum,
    ISNI: "ISNI" as IdentifiersEnum,
    ROR: "ROR" as IdentifiersEnum,
    SPDX: "SPDX" as IdentifiersEnum,
    WEB: "WEB" as IdentifiersEnum,
    Wikidata: "Wikidata" as IdentifiersEnum,
  };

  export type IdentifierTypeEnum =
    "RES_ID"
    | "ORCID"
    | "ROR_ID"
    | "SPDX_ID";
  export const IdentifierTypeEnum = {
    RES_ID: "RES_ID" as IdentifierTypeEnum,
    ORCID: "ORCID" as IdentifierTypeEnum,
    ROR_ID: "ROR_ID" as IdentifierTypeEnum,
    SPDX_ID: "SPDX_ID" as IdentifierTypeEnum,
  };

  export namespace Ontology {
    export type RdfCategoryEnum = "RDF-Class" | "RDF-Property";
    export const RdfCategoryEnum = {
      ONTOLOGY_RDF_CLASS: "RDF-Class" as RdfCategoryEnum,
      ONTOLOGY_RDF_PROPERTY: "RDF-Property" as RdfCategoryEnum,
    };
  }

  export type FormatEnum = RdfDatasetFilePartial.RdfFormatEnum;
  export const FormatEnum = RdfDatasetFilePartial.RdfFormatEnum;
  export const FormatEnumTranslate: KeyValue[] = [
    {
      key: FormatEnum.JSON_LD,
      value: MARK_AS_TRANSLATABLE("enum.rdfFormat.jsonLd"),
    },
    {
      key: FormatEnum.RDF_XML,
      value: MARK_AS_TRANSLATABLE("enum.rdfFormat.rdfXml"),
    },
    {
      key: FormatEnum.RDF_JSON,
      value: MARK_AS_TRANSLATABLE("enum.rdfFormat.rdfJson"),
    },
    {
      key: FormatEnum.N_TRIPLES,
      value: MARK_AS_TRANSLATABLE("enum.rdfFormat.nTriples"),
    },
    {
      key: FormatEnum.TURTLE,
      value: MARK_AS_TRANSLATABLE("enum.rdfFormat.turtle"),
    },
    {
      key: FormatEnum.N3,
      value: MARK_AS_TRANSLATABLE("enum.rdfFormat.n3"),
    },
    {
      key: FormatEnum.N_QUADS,
      value: MARK_AS_TRANSLATABLE("enum.rdfFormat.nQuads"),
    },
    {
      key: FormatEnum.TRIG,
      value: MARK_AS_TRANSLATABLE("enum.rdfFormat.trig"),
    },
    {
      key: FormatEnum.TRIX,
      value: MARK_AS_TRANSLATABLE("enum.rdfFormat.trix"),
    },
  ];

  export namespace RdfDatasetFile {
    export type StatusImportEnum = RdfDatasetFilePartial.StatusImportEnum;
    export const StatusImportEnum = RdfDatasetFilePartial.StatusImportEnum;
    export const StatusImportEnumTranslate: StatusModel[] = [
      {
        key: StatusImportEnum.NOT_IMPORTED,
        value: MARK_AS_TRANSLATABLE("enum.rdfDatasetFile.statusImport.notImported"),
        backgroundColorHexa: ColorHexaEnum.orange,
      },
      {
        key: StatusImportEnum.TO_IMPORT,
        value: MARK_AS_TRANSLATABLE("enum.rdfDatasetFile.statusImport.toImport"),
      },
      {
        key: StatusImportEnum.IMPORTING,
        value: MARK_AS_TRANSLATABLE("enum.rdfDatasetFile.statusImport.importing"),
      },

      {
        key: StatusImportEnum.TO_REPLACE,
        value: MARK_AS_TRANSLATABLE("enum.rdfDatasetFile.statusImport.toReplace"),
      },
      {
        key: StatusImportEnum.REPLACING,
        value: MARK_AS_TRANSLATABLE("enum.rdfDatasetFile.statusImport.replacing"),
      },
      {
        key: StatusImportEnum.TO_REIMPORT,
        value: MARK_AS_TRANSLATABLE("enum.rdfDatasetFile.statusImport.toReimport"),
      },
      {
        key: StatusImportEnum.IMPORTED,
        value: MARK_AS_TRANSLATABLE("enum.rdfDatasetFile.statusImport.imported"),
        backgroundColorHexa: ColorHexaEnum.green,
      },
      {
        key: StatusImportEnum.IN_ERROR,
        value: MARK_AS_TRANSLATABLE("enum.rdfDatasetFile.statusImport.inError"),
        backgroundColorHexa: ColorHexaEnum.red,
      },
    ];

  }

  export namespace Rml {
    export type RmlFormatEnum = RmlPartial.FormatEnum;
    export const RmlFormatEnum = RmlPartial.FormatEnum;
    export const RmlFormatEnumTranslate: KeyValue[] = [
      {
        key: Rml.RmlFormatEnum.CSV,
        value: MARK_AS_TRANSLATABLE("enum.rml.format.csv"),
      },
      {
        key: Rml.RmlFormatEnum.XML,
        value: MARK_AS_TRANSLATABLE("enum.rml.format.xml"),
      },
      {
        key: Rml.RmlFormatEnum.SQL,
        value: MARK_AS_TRANSLATABLE("enum.rml.format.sql"),
      },
      {
        key: Rml.RmlFormatEnum.JSON,
        value: MARK_AS_TRANSLATABLE("enum.rml.format.json"),
      },
    ];
  }

  export namespace Project {
    export type StorageTypeEnum = ProjectPartial.StorageTypeEnum;
    export const StorageTypeEnum = ProjectPartial.StorageTypeEnum;
    export const StorageTypeEnumTranslate: KeyValue[] = [
      {
        key: ProjectPartial.StorageTypeEnum.FILE,
        value: MARK_AS_TRANSLATABLE("enum.storage.type.file"),
      },
      {
        key: ProjectPartial.StorageTypeEnum.OBJECT,
        value: MARK_AS_TRANSLATABLE("enum.storage.type.object"),
      },
    ];
  }
}
