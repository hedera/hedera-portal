/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - icons.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {
  IconInfos,
  IconLibEnum,
} from "solidify-frontend";

export const icons: IconInfos[] = [
  {
    name: IconNameEnum.project,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "building",
  },
  {
    name: IconNameEnum.ingest,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "file-upload",
  },
  {
    name: IconNameEnum.home,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "home",
  },
  {
    name: IconNameEnum.order,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "archive",
  },
  {
    name: IconNameEnum.browse,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "users",
  },
  {
    name: IconNameEnum.preservationPlanning,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "boxes",
  },
  {
    name: IconNameEnum.administration,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "cogs",
  },
  {
    name: IconNameEnum.profile,
    lib: IconLibEnum.materialIcon,
    icon: "account_circle",
  },
  {
    name: IconNameEnum.token,
    lib: IconLibEnum.materialIcon,
    icon: "vpn_key",
  },
  {
    name: IconNameEnum.logout,
    lib: IconLibEnum.materialIcon,
    icon: "power_settings_new",
  },
  {
    name: IconNameEnum.login,
    lib: IconLibEnum.materialIcon,
    icon: "account_circle",
  },
  {
    name: IconNameEnum.resId,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "key",
  },
  {
    name: IconNameEnum.history,
    lib: IconLibEnum.materialIcon,
    icon: "history",
  },
  {
    name: IconNameEnum.docDev,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "code",
  },
  {
    name: IconNameEnum.docUser,
    lib: IconLibEnum.materialIcon,
    icon: "help",
  },
  {
    name: IconNameEnum.about,
    lib: IconLibEnum.materialIcon,
    icon: "info",
  },
  {
    name: IconNameEnum.cart,
    lib: IconLibEnum.materialIcon,
    icon: "shopping_cart",
  },
  {
    name: IconNameEnum.addToCart,
    lib: IconLibEnum.materialIcon,
    icon: "add_shopping_cart",
  },
  {
    name: IconNameEnum.search,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "search",
  },
  {
    name: IconNameEnum.back,
    lib: IconLibEnum.materialIcon,
    icon: "navigate_before",
  },
  {
    name: IconNameEnum.refresh,
    lib: IconLibEnum.materialIcon,
    icon: "refresh",
  },
  {
    name: IconNameEnum.synchronize,
    lib: IconLibEnum.materialIcon,
    icon: "sync",
  },
  {
    name: IconNameEnum.delete,
    lib: IconLibEnum.materialIcon,
    icon: "delete",
  },
  {
    name: IconNameEnum.deleteAll,
    lib: IconLibEnum.materialIcon,
    icon: "delete_forever",
  },
  {
    name: IconNameEnum.create,
    lib: IconLibEnum.materialIcon,
    icon: "add",
  },
  {
    name: IconNameEnum.internalLink,
    lib: IconLibEnum.materialIcon,
    icon: "open_in_new ",
  },
  {
    name: IconNameEnum.externalLink,
    lib: IconLibEnum.materialIcon,
    icon: "language",
  },
  {
    name: IconNameEnum.edit,
    lib: IconLibEnum.materialIcon,
    icon: "edit",
  },
  {
    name: IconNameEnum.add,
    lib: IconLibEnum.materialIcon,
    icon: "add",
  },
  {
    name: IconNameEnum.close,
    lib: IconLibEnum.materialIcon,
    icon: "close",
  },
  {
    name: IconNameEnum.archiveBrowsing,
    lib: IconLibEnum.materialIcon,
    icon: "list",
  },
  {
    name: IconNameEnum.download,
    lib: IconLibEnum.materialIcon,
    icon: "file_download",
  },
  {
    name: IconNameEnum.sendRequest,
    lib: IconLibEnum.materialIcon,
    icon: "announcement",
  },
  {
    name: IconNameEnum.accessControlled,
    lib: IconLibEnum.materialIcon,
    icon: "announcement",
  },
  {
    name: IconNameEnum.roleManager,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "user-shield",
  },
  {
    name: IconNameEnum.roleCreator,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "user-edit",
  },
  {
    name: IconNameEnum.roleVisitor,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "user",
  },
  {
    name: IconNameEnum.memberWithoutRole,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "eye",
  },
  {
    name: IconNameEnum.myOrder,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "user",
  },
  {
    name: IconNameEnum.allOrder,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "users",
  },
  {
    name: IconNameEnum.metadata,
    lib: IconLibEnum.materialIcon,
    icon: "assignment",
  },
  {
    name: IconNameEnum.files,
    lib: IconLibEnum.materialIcon,
    icon: "insert_drive_file",
  },
  {
    name: IconNameEnum.associateArchive,
    lib: IconLibEnum.materialIcon,
    icon: "link",
  },
  {
    name: IconNameEnum.collection,
    lib: IconLibEnum.materialIcon,
    icon: "collections_bookmark",
  },
  {
    name: IconNameEnum.navigate,
    lib: IconLibEnum.materialIcon,
    icon: "forward",
  },
  {
    name: IconNameEnum.reserveDoi,
    lib: IconLibEnum.materialIcon,
    icon: "library_books",
  },
  {
    name: IconNameEnum.submit,
    lib: IconLibEnum.materialIcon,
    icon: "done_all",
  },
  {
    name: IconNameEnum.approve,
    lib: IconLibEnum.materialIcon,
    icon: "check",
  },
  {
    name: IconNameEnum.unapprove,
    lib: IconLibEnum.materialIcon,
    icon: "close",
  },
  {
    name: IconNameEnum.wait,
    lib: IconLibEnum.materialIcon,
    icon: "access_time",
  },
  {
    name: IconNameEnum.save,
    lib: IconLibEnum.materialIcon,
    icon: "save",
  },
  {
    name: IconNameEnum.uploadFile,
    lib: IconLibEnum.materialIcon,
    icon: "publish",
  },
  {
    name: IconNameEnum.uploadStructuredFiles,
    lib: IconLibEnum.materialIcon,
    icon: "unarchive",
  },
  {
    name: IconNameEnum.linkToResearchDataFile,
    lib: IconLibEnum.materialIcon,
    icon: "link",
  },
  {
    name: IconNameEnum.filesView,
    lib: IconLibEnum.materialIcon,
    icon: "list",
  },
  {
    name: IconNameEnum.foldersView,
    lib: IconLibEnum.materialIcon,
    icon: "folder_open",
  },
  {
    name: IconNameEnum.preview,
    lib: IconLibEnum.materialIcon,
    icon: "remove_red_eye",
  },
  {
    name: IconNameEnum.star,
    lib: IconLibEnum.materialIcon,
    icon: "star",
  },
  {
    name: IconNameEnum.starEmpty,
    lib: IconLibEnum.materialIcon,
    icon: "star_border",
  },
  {
    name: IconNameEnum.move,
    lib: IconLibEnum.materialIcon,
    icon: "redo",
  },
  {
    name: IconNameEnum.information,
    lib: IconLibEnum.materialIcon,
    icon: "info",
  },
  {
    name: IconNameEnum.dashboard,
    lib: IconLibEnum.materialIcon,
    icon: "dashboard",
  },
  {
    name: IconNameEnum.contributor,
    lib: IconLibEnum.materialIcon,
    icon: "group",
  },
  {
    name: IconNameEnum.license,
    lib: IconLibEnum.image,
    icon: "dua-implicit.svg",
  },
  {
    name: IconNameEnum.requestInbox,
    lib: IconLibEnum.materialIcon,
    icon: "mail",
  },
  {
    name: IconNameEnum.requestSent,
    lib: IconLibEnum.materialIcon,
    icon: "send",
  },
  {
    name: IconNameEnum.sip,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "paper-plane",
  },
  {
    name: IconNameEnum.dip,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "cart-arrow-down",
  },
  {
    name: IconNameEnum.aip,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "archive",
  },
  {
    name: IconNameEnum.monitoring,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "user-md",
  },
  {
    name: IconNameEnum.jobs,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "robot",
  },
  {
    name: IconNameEnum.archivingStatus,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "clipboard-check",
  },
  {
    name: IconNameEnum.aipDownloaded,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "cart-arrow-down",
  },
  {
    name: IconNameEnum.storagion,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "server",
  },
  {
    name: IconNameEnum.simpleChecksum,
    lib: IconLibEnum.materialIcon,
    icon: "refresh",
  },
  {
    name: IconNameEnum.doubleChecksum,
    lib: IconLibEnum.materialIcon,
    icon: "sync",
  },
  {
    name: IconNameEnum.reindex,
    lib: IconLibEnum.materialIcon,
    icon: "repeat",
  },
  {
    name: IconNameEnum.check,
    lib: IconLibEnum.materialIcon,
    icon: "check",
  },
  {
    name: IconNameEnum.init,
    lib: IconLibEnum.materialIcon,
    icon: "power",
  },
  {
    name: IconNameEnum.run,
    lib: IconLibEnum.materialIcon,
    icon: "play_arrow",
  },
  {
    name: IconNameEnum.resume,
    lib: IconLibEnum.materialIcon,
    icon: "play_circle_filled",
  },
  {
    name: IconNameEnum.resumeAll,
    lib: IconLibEnum.materialIcon,
    icon: "play_circle_outline",
  },
  {
    name: IconNameEnum.notIgnore,
    lib: IconLibEnum.materialIcon,
    icon: "check_circle",
  },
  {
    name: IconNameEnum.orderReady,
    lib: IconLibEnum.materialIcon,
    icon: "check_circle",
  },
  {
    name: IconNameEnum.orderInProgress,
    lib: IconLibEnum.materialIcon,
    icon: "autorenew",
  },
  {
    name: IconNameEnum.orderInError,
    lib: IconLibEnum.materialIcon,
    icon: "error",
  },
  {
    name: IconNameEnum.archiveTypes,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "box",
  },
  {
    name: IconNameEnum.submissionPolicies,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "user-check",
  },
  {
    name: IconNameEnum.preservationPolicies,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "history",
  },
  {
    name: IconNameEnum.disseminationPolicies,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "cart-arrow-down",
  },
  {
    name: IconNameEnum.licenses,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "copyright",
  },
  {
    name: IconNameEnum.globalBanners,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "exclamation-triangle",
  },
  {
    name: IconNameEnum.institutions,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "university",
  },
  {
    name: IconNameEnum.users,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "user-circle",
  },
  {
    name: IconNameEnum.researchObjects,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "microscope",
  },
  {
    name: IconNameEnum.roles,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "id-badge",
  },
  {
    name: IconNameEnum.oaiMetadataPrefixes,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "passport",
  },
  {
    name: IconNameEnum.oaiSets,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "object-group",
  },
  {
    name: IconNameEnum.peoples,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "user",
  },
  {
    name: IconNameEnum.fundingAgencies,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "hand-holding-usd",
  },
  {
    name: IconNameEnum.indexFieldAliases,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "search",
  },
  {
    name: IconNameEnum.archiveAcl,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "lock",
  },
  {
    name: IconNameEnum.languages,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "language",
  },
  {
    name: IconNameEnum.metadataTypes,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "table",
  },
  {
    name: IconNameEnum.notifications,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "envelope",
  },
  {
    name: IconNameEnum.passwordVisible,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "eye",
  },
  {
    name: IconNameEnum.passwordHide,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "eye-slash",
  },
  {
    name: IconNameEnum.testFile,
    lib: IconLibEnum.materialIcon,
    icon: "check",
  },
  {
    name: IconNameEnum.folderEmpty,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "folder",
  },
  {
    name: IconNameEnum.folderOpened,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "folder-open",
  },
  {
    name: IconNameEnum.folderClosed,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "folder-plus",
  },
  {
    name: IconNameEnum.expandAll,
    lib: IconLibEnum.materialIcon,
    icon: "unfold_more",
  },
  {
    name: IconNameEnum.collapseAll,
    lib: IconLibEnum.materialIcon,
    icon: "unfold_less",
  },
  {
    name: IconNameEnum.send,
    lib: IconLibEnum.materialIcon,
    icon: "send",
  },
  {
    name: IconNameEnum.clear,
    lib: IconLibEnum.materialIcon,
    icon: "clear",
  },
  {
    name: IconNameEnum.reset,
    lib: IconLibEnum.materialIcon,
    icon: "restart_alt",
  },
  {
    name: IconNameEnum.clean,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "broom",
  },
  {
    name: IconNameEnum.done,
    lib: IconLibEnum.materialIcon,
    icon: "done",
  },
  {
    name: IconNameEnum.update,
    lib: IconLibEnum.materialIcon,
    icon: "sync",
  },
  {
    name: IconNameEnum.emptyCart,
    lib: IconLibEnum.materialIcon,
    icon: "remove_shopping_cart",
  },
  {
    name: IconNameEnum.copyToClipboard,
    lib: IconLibEnum.materialIcon,
    icon: "filter_none",
  },
  {
    name: IconNameEnum.notFound,
    lib: IconLibEnum.materialIcon,
    icon: "mood_bad",
  },
  {
    name: IconNameEnum.up,
    lib: IconLibEnum.materialIcon,
    icon: "keyboard_arrow_up",
  },
  {
    name: IconNameEnum.down,
    lib: IconLibEnum.materialIcon,
    icon: "keyboard_arrow_down",
  },
  {
    name: IconNameEnum.personAdd,
    lib: IconLibEnum.materialIcon,
    icon: "person_add",
  },
  {
    name: IconNameEnum.redo,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "redo-alt",
  },
  {
    name: IconNameEnum.sort,
    lib: IconLibEnum.materialIcon,
    icon: "format_list_numbered_rtl",
  },
  {
    name: IconNameEnum.clearCache,
    lib: IconLibEnum.materialIcon,
    icon: "delete_sweep",
  },
  {
    name: IconNameEnum.warning,
    lib: IconLibEnum.materialIcon,
    icon: "warning",
  },
  {
    name: IconNameEnum.autoUpdate,
    lib: IconLibEnum.materialIcon,
    icon: "autorenew",
  },
  {
    name: IconNameEnum.fingerprint,
    lib: IconLibEnum.materialIcon,
    icon: "fingerprint",
  },
  {
    name: IconNameEnum.http,
    lib: IconLibEnum.materialIcon,
    icon: "http",
  },
  {
    name: IconNameEnum.closeChip,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "times-circle",
  },
  {
    name: IconNameEnum.success,
    lib: IconLibEnum.materialIcon,
    icon: "check_circle",
  },
  {
    name: IconNameEnum.error,
    lib: IconLibEnum.materialIcon,
    icon: "error",
  },
  {
    name: IconNameEnum.zoomOut,
    lib: IconLibEnum.materialIcon,
    icon: "zoom_out",
  },
  {
    name: IconNameEnum.zoomIn,
    lib: IconLibEnum.materialIcon,
    icon: "zoom_in",
  },
  {
    name: IconNameEnum.unableToLoadApp,
    lib: IconLibEnum.materialIcon,
    icon: "bug_report",
  },
  {
    name: IconNameEnum.menuButtons,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "ellipsis-v",
  },
  {
    name: IconNameEnum.orcid,
    lib: IconLibEnum.image,
    icon: "orcid.svg",
  },
  {
    name: IconNameEnum.orcidText,
    lib: IconLibEnum.image,
    icon: "orcid-text.svg",
  },
  {
    name: IconNameEnum.ror,
    lib: IconLibEnum.image,
    icon: "ROR.svg",
  },
  {
    name: IconNameEnum.spdx,
    lib: IconLibEnum.image,
    icon: "SPDX.svg",
  },
  {
    name: IconNameEnum.doi,
    lib: IconLibEnum.image,
    icon: "doi.svg",
  },
  {
    name: IconNameEnum.ark,
    lib: IconLibEnum.image,
    icon: "ark.png",
  },
  {
    name: IconNameEnum.crossref,
    lib: IconLibEnum.image,
    icon: "crossref.svg",
  },
  {
    name: IconNameEnum.grid,
    lib: IconLibEnum.image,
    icon: "grid.svg",
  },
  {
    name: IconNameEnum.isni,
    lib: IconLibEnum.image,
    icon: "isni.svg",
  },
  {
    name: IconNameEnum.wikidata,
    lib: IconLibEnum.image,
    icon: "wikidata.svg",
  },
  {
    name: IconNameEnum.archive,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "book",
  },
  {
    name: IconNameEnum.unigeBlack,
    lib: IconLibEnum.image,
    icon: "unigelogo-black.svg",
  },
  {
    name: IconNameEnum.unigeWhite,
    lib: IconLibEnum.image,
    icon: "unigelogo-white.svg",
  },
  {
    name: IconNameEnum.theme,
    lib: IconLibEnum.materialIcon,
    icon: "style",
  },
  {
    name: IconNameEnum.darkMode,
    lib: IconLibEnum.materialIcon,
    icon: "bedtime",
  },
  {
    name: IconNameEnum.lightMode,
    lib: IconLibEnum.materialIcon,
    icon: "brightness_5",
  },
  {
    name: IconNameEnum.creationDate,
    lib: IconLibEnum.materialIcon,
    icon: "event_note",
  },
  {
    name: IconNameEnum.defaultValue,
    lib: IconLibEnum.materialIcon,
    icon: "settings",
  },
  {
    name: IconNameEnum.change,
    lib: IconLibEnum.materialIcon,
    icon: "swap_horiz",
  },
  {
    name: IconNameEnum.trueValue,
    lib: IconLibEnum.materialIcon,
    icon: "check_circle_outline",
  },
  {
    name: IconNameEnum.falseValue,
    lib: IconLibEnum.materialIcon,
    icon: "highlight_off",
  },
  {
    name: IconNameEnum.dispose,
    lib: IconLibEnum.materialIcon,
    icon: "delete_forever",
  },
  {
    name: IconNameEnum.approveDisposal,
    lib: IconLibEnum.materialIcon,
    icon: "check",
  },
  {
    name: IconNameEnum.extendRetention,
    lib: IconLibEnum.materialIcon,
    icon: "schedule",
  },
  {
    name: IconNameEnum.listView,
    lib: IconLibEnum.materialIcon,
    icon: "view_list",
  },
  {
    name: IconNameEnum.tilesView,
    lib: IconLibEnum.materialIcon,
    icon: "view_module",
  },
  {
    name: IconNameEnum.accessLevelRestricted,
    lib: IconLibEnum.image,
    icon: "restricted.svg",
  },
  {
    name: IconNameEnum.accessLevelClosed,
    lib: IconLibEnum.image,
    icon: "closed.svg",
  },
  {
    name: IconNameEnum.accessLevelPublic,
    lib: IconLibEnum.image,
    icon: "public.svg",
  },
  {
    name: IconNameEnum.accessLevelUndefined,
    lib: IconLibEnum.image,
    icon: "access-level-undefined.svg",
  },
  {
    name: IconNameEnum.uploadImage,
    lib: IconLibEnum.materialIcon,
    icon: "add_a_photo",
  },
  {
    name: IconNameEnum.help,
    lib: IconLibEnum.materialIcon,
    icon: "help",
  },
  {
    name: IconNameEnum.maintenance,
    lib: IconLibEnum.materialIcon,
    icon: "settings",
  },
  {
    name: IconNameEnum.offline,
    lib: IconLibEnum.materialIcon,
    icon: "power_settings_new",
  },
  {
    name: IconNameEnum.viewNumber,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "eye",
  },
  {
    name: IconNameEnum.downloadNumber,
    lib: IconLibEnum.materialIcon,
    icon: "get_app",
  },
  {
    name: IconNameEnum.sortUndefined,
    lib: IconLibEnum.materialIcon,
    icon: "filter_list",
  },
  {
    name: IconNameEnum.sortAscending,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "sort-amount-up-alt",
  },
  {
    name: IconNameEnum.sortDescending,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "sort-amount-down",
  },
  {
    name: IconNameEnum.noPreview,
    lib: IconLibEnum.materialIcon,
    icon: "insert_drive_file",
  },
  {
    name: IconNameEnum.fullScreenEnter,
    lib: IconLibEnum.materialIcon,
    icon: "fullscreen",
  },
  {
    name: IconNameEnum.fullScreenLeave,
    lib: IconLibEnum.materialIcon,
    icon: "fullscreen_exit",
  },
  {
    name: IconNameEnum.guidedTour,
    lib: IconLibEnum.materialIcon,
    icon: "beenhere",
  },
  {
    name: IconNameEnum.dataSensitivity,
    lib: IconLibEnum.materialIcon,
    icon: "local_offer",
  },
  {
    name: IconNameEnum.plus,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "plus-square",
  },
  {
    name: IconNameEnum.minus,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "minus-square",
  },
  {
    name: IconNameEnum.rate,
    lib: IconLibEnum.materialIcon,
    icon: "stars",
  },
  {
    name: IconNameEnum.markAsUnread,
    lib: IconLibEnum.materialIcon,
    icon: "markunread",
  },
  {
    name: IconNameEnum.markAsRead,
    lib: IconLibEnum.materialIcon,
    icon: "drafts",
  },
  {
    name: IconNameEnum.privacy,
    lib: IconLibEnum.materialIcon,
    icon: "vpn_lock",
  },
  {
    name: IconNameEnum.more,
    lib: IconLibEnum.materialIcon,
    icon: "more_horiz",
  },
  {
    name: IconNameEnum.downArrow,
    lib: IconLibEnum.materialIcon,
    icon: "arrow_drop_down",
  },
  {
    name: IconNameEnum.putInError,
    lib: IconLibEnum.materialIcon,
    icon: "error_outline",
  },
  {
    name: IconNameEnum.next,
    lib: IconLibEnum.materialIcon,
    icon: "navigate_next",
  },
  {
    name: IconNameEnum.checksums,
    lib: IconLibEnum.materialIcon,
    icon: "pin",
  },
  {
    name: IconNameEnum.checkCompliance,
    lib: IconLibEnum.materialIcon,
    icon: "check_circle",
  },
  {
    name: IconNameEnum.abort,
    lib: IconLibEnum.materialIcon,
    icon: "redo",
  },
  {
    name: IconNameEnum.dragToSort,
    lib: IconLibEnum.materialIcon,
    icon: "drag_handle",
  },
  {
    name: IconNameEnum.disabledScheduled,
    lib: IconLibEnum.materialIcon,
    icon: "hourglass_disabled",
  },
  {
    name: IconNameEnum.enabledScheduled,
    lib: IconLibEnum.materialIcon,
    icon: "hourglass_bottom",
  },
  {
    name: IconNameEnum.stop,
    lib: IconLibEnum.materialIcon,
    icon: "stop",
  },
  {
    name: IconNameEnum.excludedFacet,
    lib: IconLibEnum.materialIcon,
    icon: "do_disturb",
  },
  {
    name: IconNameEnum.keyword,
    lib: IconLibEnum.materialIcon,
    icon: "label",
  },
  {
    name: IconNameEnum.language,
    lib: IconLibEnum.materialIcon,
    icon: "translate",
  },
  {
    name: IconNameEnum.contentType,
    lib: IconLibEnum.materialIcon,
    icon: "category",
  },
  {
    name: IconNameEnum.readme,
    lib: IconLibEnum.materialIcon,
    icon: "description",
  },
  {
    name: IconNameEnum.ontologies,
    lib: IconLibEnum.materialIcon,
    icon: "view_list",
  },
  {
    name: IconNameEnum.readme,
    lib: IconLibEnum.materialIcon,
    icon: "description",
  },
  {
    name: IconNameEnum.rml,
    lib: IconLibEnum.materialIcon,
    icon: "linear_scale",
  },
  {
    name: IconNameEnum.logInMfa,
    lib: IconLibEnum.materialIcon,
    icon: "admin_panel_settings",
  },
  {
    name: IconNameEnum.applyRml,
    lib: IconLibEnum.materialIcon,
    icon: "arrow_circle_up",
  },
  {
    name: IconNameEnum.rdf,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "file-import",
  },
  {
    name: IconNameEnum.researchDataFile,
    lib: IconLibEnum.fontAwesomeSolid,
    icon: "file",
  },
  {
    name: IconNameEnum.addInTripleStore,
    lib: IconLibEnum.materialIcon,
    icon: "publish",
  },
  {
    name: IconNameEnum.deleteFromTripleStore,
    lib: IconLibEnum.materialIcon,
    icon: "delete_forever",
  },
  {
    name: IconNameEnum.replaceInTripleStore,
    lib: IconLibEnum.materialIcon,
    icon: "published_with_changes",
  },
  {
    name: IconNameEnum.correct,
    lib: IconLibEnum.materialIcon,
    icon: "done",
  },
  {
    name: IconNameEnum.incorrect,
    lib: IconLibEnum.materialIcon,
    icon: "dangerous",
  },
  {
    name: IconNameEnum.reload,
    lib: IconLibEnum.materialIcon,
    icon: "autorenew",
  },
  {
    name: IconNameEnum.rdfClasses,
    lib: IconLibEnum.materialIcon,
    icon: "class",
  },
  {
    name: IconNameEnum.rdfProperties,
    lib: IconLibEnum.materialIcon,
    icon: "toc",
  },
  {
    name: IconNameEnum.iiif,
    lib: IconLibEnum.image,
    icon: "iiif.svg",
  },
  {
    name: IconNameEnum.manifest,
    lib: IconLibEnum.materialIcon,
    icon: "text_snippet",
  },
  {
    name: IconNameEnum.managed,
    lib: IconLibEnum.materialIcon,
    icon: "tune",
  },
  {
    name: IconNameEnum.other,
    lib: IconLibEnum.materialIcon,
    icon: "quiz",
  },
];
