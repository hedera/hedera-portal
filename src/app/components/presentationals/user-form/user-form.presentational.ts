/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - user-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
} from "@angular/core";
import {
  FormArray,
  FormBuilder,
  Validators,
} from "@angular/forms";
import {BaseFormDefinition} from "@app/shared/models/base-form-definition.model";
import {Enums} from "@enums";
import {
  Project,
  User,
} from "@models";
import {SharedAbstractFormPresentational} from "@shared/components/presentationals/shared-abstract-form/shared-abstract.presentational";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  EnumUtil,
  isNullOrUndefined,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "hedera-user-form",
  templateUrl: "./user-form.presentational.html",
  styleUrls: ["./user-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserFormPresentational extends SharedAbstractFormPresentational<User> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  applicationRolesNames: string[] = EnumUtil.convertToArray(Enums.UserApplicationRole.UserApplicationRoleEnum, [Enums.UserApplicationRole.UserApplicationRoleEnum.trusted_client]);

  @Input()
  listProject: Project[];

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _bindFormTo(user: User): void {
    this.form = this._fb.group({
      [this.formDefinition.externalUid]: [user.externalUid, [Validators.required, SolidifyValidator]],
      [this.formDefinition.firstName]: [user.firstName, [Validators.required, SolidifyValidator]],
      [this.formDefinition.lastName]: [user.lastName, [Validators.required, SolidifyValidator]],
      [this.formDefinition.homeOrganization]: [user.homeOrganization, [Validators.required, SolidifyValidator]],
      [this.formDefinition.email]: [user.email, [Validators.required, SolidifyValidator]],
      [this.formDefinition.person]: isNullOrUndefined(user.person) ? [] : [user.person.resId],
      [this.formDefinition.accessToken]: [user.accessToken, [Validators.required, SolidifyValidator]],
      [this.formDefinition.refreshToken]: [user.refreshToken, [Validators.required, SolidifyValidator]],
      [this.formDefinition.applicationRole]: [isNullOrUndefined(user.applicationRole?.resId) ? Enums.UserApplicationRole.UserApplicationRoleEnum.user : user.applicationRole.resId, [Validators.required, SolidifyValidator]],
    });
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.externalUid]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.firstName]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.lastName]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.homeOrganization]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.email]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.person]: [""],
      [this.formDefinition.accessToken]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.refreshToken]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.applicationRole]: ["", [Validators.required, SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(user: User): User {
    return user;
  }

  get getApplicationRolesNamesArray(): FormArray {
    return this.form.controls.applicationRoles.value as FormArray;
  }

  protected override _disableSpecificField(): void {
    this.form.get(this.formDefinition.homeOrganization).disable();
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() externalUid: string;
  @PropertyName() firstName: string;
  @PropertyName() lastName: string;
  @PropertyName() homeOrganization: string;
  @PropertyName() email: string;
  @PropertyName() person: string;
  @PropertyName() accessToken: string;
  @PropertyName() refreshToken: string;
  @PropertyName() applicationRole: string;
}
