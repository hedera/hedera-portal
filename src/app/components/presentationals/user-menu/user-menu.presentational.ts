/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - user-menu.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
  Output,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {UserProfileDialog} from "@app/components/dialogs/user-profile/user-profile.dialog";
import {MenuToolbar} from "@app/components/presentationals/main-toolbar/abstract-main-toolbar/abstract-main-toolbar.presentational";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {environment} from "@environments/environment";
import {User} from "@models";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {ThemeEnum} from "@shared/enums/theme.enum";
import {Token} from "@shared/models/token.model";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  DialogUtil,
  ObservableUtil,
  SOLIDIFY_CONSTANTS,
  TokenDialog,
} from "solidify-frontend";

@Component({
  selector: "hedera-user-menu",
  templateUrl: "./user-menu.presentational.html",
  styleUrls: ["./user-menu.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserMenuPresentational extends SharedAbstractPresentational {
  isDemoMode: boolean = environment.isDemoMode;
  isProduction: boolean = environment.production;

  @Input()
  user: User;

  @Input()
  photoUser: string;

  private get _theme(): ThemeEnum | undefined {
    return this._themeChangeBS.getValue();
  }

  private set _theme(value: ThemeEnum | undefined) {
    this._themeChangeBS.next(value);
  }

  get theme(): ThemeEnum | undefined {
    return this._theme;
  }

  @Input()
  set theme(theme: ThemeEnum | undefined) {
    this._theme = theme;
  }

  private get _darkMode(): boolean | undefined {
    return this._darkModeChangeBS.getValue();
  }

  private set _darkMode(value: boolean | undefined) {
    this._darkModeChangeBS.next(value);
  }

  get darkMode(): boolean | undefined {
    return this._darkMode;
  }

  @Input()
  set darkMode(darkMode: boolean | undefined) {
    this._darkMode = darkMode;
  }

  @Input()
  listMenuAdmin: MenuToolbar[];

  private readonly _DEFAULT_INITIAL: string = "U";

  initial: string = this._DEFAULT_INITIAL;

  // Logout
  private readonly _logoutBS: BehaviorSubject<void | undefined> = new BehaviorSubject<void | undefined>(undefined);
  @Output("logoutChange")
  readonly logoutObs: Observable<void | undefined> = ObservableUtil.asObservable(this._logoutBS);

  private readonly _navigateBS: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);
  @Output("navigateChange")
  readonly navigateObs: Observable<string | undefined> = ObservableUtil.asObservable(this._navigateBS);

  private readonly _userGuideOpenBS: BehaviorSubject<void | undefined> = new BehaviorSubject<void | undefined>(undefined);
  @Output("userGuideOpen")
  readonly userGuideOpenObs: Observable<void | undefined> = ObservableUtil.asObservable(this._userGuideOpenBS);

  private readonly _themeChangeBS: BehaviorSubject<ThemeEnum | undefined> = new BehaviorSubject<ThemeEnum | undefined>(undefined);
  @Output("themeChange")
  readonly themeChangeObs: Observable<ThemeEnum | undefined> = ObservableUtil.asObservable(this._themeChangeBS);

  private readonly _darkModeChangeBS: BehaviorSubject<boolean | undefined> = new BehaviorSubject<boolean | undefined>(undefined);
  @Output("darkModeChange")
  readonly darkModeChangeObs: Observable<boolean | undefined> = ObservableUtil.asObservable(this._darkModeChangeBS);

  constructor(private readonly _dialog: MatDialog) {
    super();
  }

  logout(): void {
    this._logoutBS.next();
  }

  displayToken(): void {
    DialogUtil.open(this._dialog, TokenDialog, {
      extraInformation: [
        {
          key: LabelTranslateEnum.role,
          value: (token: Token) => token.authorities.join(SOLIDIFY_CONSTANTS.COMMA + " "),
        },
      ],
    });
  }

  profileInfo(): void {
    DialogUtil.open(this._dialog, UserProfileDialog, this.user, {
      width: "90%",
    });
  }

  openUserGuide(): void {
    this._userGuideOpenBS.next();
  }

  toggleDarkMode(): void {
    this._darkModeChangeBS.next(!this._darkModeChangeBS.value);
  }

  navigate(path: string): void {
    this._navigateBS.next(path);
  }
}
