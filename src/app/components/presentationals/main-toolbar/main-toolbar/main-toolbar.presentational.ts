/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - main-toolbar.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {
  ChangeDetectionStrategy,
  Component,
  Input,
  Output,
} from "@angular/core";
import {Enums} from "@enums";
import {User} from "@models";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {ThemeEnum} from "@shared/enums/theme.enum";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  FrontendVersion,
  ObservableUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-main-toolbar",
  templateUrl: "./main-toolbar.presentational.html",
  styleUrls: ["./main-toolbar.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainToolbarPresentational extends SharedAbstractPresentational {
  @Input()
  logged: boolean = false;

  @Input()
  frontendVersion: FrontendVersion;

  @Input()
  currentPath: string;

  @Input()
  language: Enums.Language.LanguageEnum;

  @Input()
  userRoles: Enums.UserApplicationRole.UserApplicationRoleEnum[];

  @Input()
  user: User;

  @Input()
  photoUser: string;

  @Input()
  numberArchiveInCart: number;

  @Input()
  numberMyOrderReadyNew: number;

  @Input()
  numberPendingRequestNotificationInbox: number;

  @Input()
  theme: ThemeEnum;

  @Input()
  darkMode: boolean;

  @Input()
  logo: string;

  @Input()
  displayCart: boolean;

  @Input()
  isHomePage: boolean;

  private readonly _themeBS: BehaviorSubject<ThemeEnum | undefined> = new BehaviorSubject<ThemeEnum | undefined>(undefined);
  @Output("themeChange")
  readonly themeObs: Observable<ThemeEnum | undefined> = ObservableUtil.asObservable(this._themeBS);

  private readonly _darkModeBS: BehaviorSubject<boolean | undefined> = new BehaviorSubject<boolean | undefined>(undefined);
  @Output("darkModeChange")
  readonly darkModeObs: Observable<boolean | undefined> = ObservableUtil.asObservable(this._darkModeBS);

  // Navigate
  private readonly _navigateBS: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);
  @Output("navigateChange")
  readonly navigateObs: Observable<string | undefined> = ObservableUtil.asObservable(this._navigateBS);

  // Language
  private readonly _languageBS: BehaviorSubject<Enums.Language.LanguageEnum | undefined> = new BehaviorSubject<Enums.Language.LanguageEnum | undefined>(undefined);
  @Output("languageChange")
  readonly languageObs: Observable<Enums.Language.LanguageEnum | undefined> = ObservableUtil.asObservable(this._languageBS);

  // Logout
  private readonly _logoutBS: BehaviorSubject<void | undefined> = new BehaviorSubject<void | undefined>(undefined);
  @Output("logoutChange")
  readonly logoutObs: Observable<void | undefined> = ObservableUtil.asObservable(this._logoutBS);

  // Login
  private readonly _loginBS: BehaviorSubject<void | undefined> = new BehaviorSubject<void | undefined>(undefined);
  @Output("loginChange")
  readonly loginObs: Observable<void | undefined> = ObservableUtil.asObservable(this._loginBS);

  // UserGuide
  private readonly _userGuideOpenBS: BehaviorSubject<void | undefined> = new BehaviorSubject<void | undefined>(undefined);
  @Output("userGuideOpen")
  readonly userGuideOpenObs: Observable<void | undefined> = ObservableUtil.asObservable(this._userGuideOpenBS);

  //Search
  private readonly _searchBS: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);
  @Output("searchChange")
  readonly searchObs: Observable<string | undefined> = ObservableUtil.asObservable(this._searchBS);

  changeTheme(theme: ThemeEnum): void {
    if (this._themeBS.value !== theme) {
      this._themeBS.next(theme);
    }
  }

  changeDarkMode(darkMode: boolean): void {
    if (this._darkModeBS.value !== darkMode) {
      this._darkModeBS.next(darkMode);
    }
  }

  changeLanguage(language: Enums.Language.LanguageEnum): void {
    if (this._languageBS.value !== language) {
      this._languageBS.next(language);
    }
  }

  changeNavigate(navigate: string): void {
    this._navigateBS.next(navigate);
  }

  logout(): void {
    this._logoutBS.next();
  }

  login(): void {
    this._loginBS.next();
  }

  openUserGuide(): void {
    this._userGuideOpenBS.next();
  }

  search(value: string): void {
    this._searchBS.next(value);
  }
}
