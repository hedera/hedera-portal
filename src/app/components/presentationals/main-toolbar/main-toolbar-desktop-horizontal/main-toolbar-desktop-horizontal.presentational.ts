/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - main-toolbar-desktop-horizontal.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
  Output,
} from "@angular/core";
import {AbstractMainToolbarPresentational} from "@app/components/presentationals/main-toolbar/abstract-main-toolbar/abstract-main-toolbar.presentational";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {SecurityService} from "@shared/services/security.service";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  BreakpointService,
  ObservableUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-main-toolbar-desktop-horizontal",
  templateUrl: "./main-toolbar-desktop-horizontal.presentational.html",
  styleUrls: ["./main-toolbar-desktop-horizontal.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainToolbarDesktopHorizontalPresentational extends AbstractMainToolbarPresentational {
  @Input()
  logo: string;

  @Input()
  displayCart: boolean;

  @Input()
  isHomePage: boolean;

  private readonly _searchBS: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);
  @Output("searchChange")
  readonly searchObs: Observable<string | undefined> = ObservableUtil.asObservable(this._searchBS);

  constructor(protected readonly _securityService: SecurityService,
              public readonly breakpointService: BreakpointService) {
    super(_securityService);
  }

  navigateToHome(): void {
    this._navigateBS.next(RoutesEnum.homePage);
  }

  search(value: string): void {
    this._searchBS.next(value);
  }
}
