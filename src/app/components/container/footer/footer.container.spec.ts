/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - footer.container.spec.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {HttpClientTestingModule} from "@angular/common/http/testing";
import {
  ComponentFixture,
  TestBed,
  waitForAsync,
} from "@angular/core/testing";
import {MatSnackBar} from "@angular/material/snack-bar";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {AppState} from "@app/stores/app.state";
import {AppPersonState} from "@app/stores/person/app-person.state";
import {AppUserState} from "@app/stores/user/app-user.state";
import {environment} from "@environments/environment";
import {
  TranslatePipe,
  TranslateService,
} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {SharedModule} from "@shared/shared.module";
import {
  ENVIRONMENT,
  MockTranslatePipe,
  MockTranslateService,
  SNACK_BAR,
} from "solidify-frontend";

import {FooterContainer} from "./footer.container";

xdescribe("FooterPresentational", () => {
  let component: FooterContainer;
  let fixture: ComponentFixture<FooterContainer>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [FooterContainer],
      imports: [
        HttpClientTestingModule,
        NoopAnimationsModule,
        NgxsModule.forRoot([AppState, AppUserState, AppPersonState]),
        SharedModule,
      ],
      providers: [
        {
          provide: TranslateService,
          useClass: MockTranslateService,
        },
        {
          provide: TranslatePipe,
          useClass: MockTranslatePipe,
        },
        {
          provide: ENVIRONMENT,
          useValue: environment,
        },
        {
          provide: SNACK_BAR,
          useClass: MatSnackBar,
        },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(() => fixture.detectChanges()).not.toThrow();
  });
});
