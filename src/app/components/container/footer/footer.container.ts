/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - footer.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
  Output,
} from "@angular/core";
import {AppState} from "@app/stores/app.state";
import {AppTocState} from "@app/stores/toc/app-toc.state";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {SharedAbstractContainer} from "@shared/components/containers/shared-abstract/shared-abstract.container";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  CookieConsentService,
  MappingObject,
  MemoizedUtil,
  ObservableUtil,
} from "solidify-frontend";

@Component({
  selector: "hedera-footer-container",
  templateUrl: "./footer.container.html",
  styleUrls: ["./footer.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterContainer extends SharedAbstractContainer {
  isLoadingDocObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, AppTocState);
  toolsGuideObs: Observable<string> = MemoizedUtil.select(this._store, AppTocState, state => state.toolsGuide);
  integrationGuideObs: Observable<string> = MemoizedUtil.select(this._store, AppTocState, state => state.integrationGuide);
  apisObs: Observable<MappingObject<string, string>> = MemoizedUtil.select(this._store, AppTocState, state => state.apis);
  darkModeObs: Observable<boolean> = MemoizedUtil.select(this._store, AppState, state => state.darkMode);

  @Input()
  updateVersion: string | undefined;

  private readonly _updateBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("updateChange")
  readonly updateObs: Observable<void> = ObservableUtil.asObservable(this._updateBS);

  constructor(private readonly _store: Store,
              private readonly _cookieConsentService: CookieConsentService) {
    super();
  }

  update(): void {
    this._updateBS.next();
  }

  openCookieConsentSideBar(): void {
    this._cookieConsentService.openPersonalizationSidebar();
  }

  navigateToAbout(): void {
    this._store.dispatch(new Navigate([RoutesEnum.about]));
  }
}
