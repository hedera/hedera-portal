/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - server-environment.project.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AppRoutesEnum,
  BrowseProjectRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {ServerEnvironment} from "./server-environment.defaults.model";

export const serverEnvironmentProject: Partial<ServerEnvironment> = {
  projectName: "hedera-portal",
  isActiveMQCacheClean: false,
  activeMQResourceCacheEvictMap: {},
  activeMQEnv: "hedera",
  ssrRoutes: [
    {
      name: "home",
      path: `/${RoutesEnum.homePage}`,
      urlMatcher: `^\/$|^\/${RoutesEnum.homePage}$`,
      resource: "home",
      withId: false,
    },
    {
      name: "about",
      path: `/${RoutesEnum.about}`,
      withId: false,
      cacheForbidden: true,
    },
    {
      name: "research-element",
      path: `/${RoutesEnum.homeDetail}/${AppRoutesEnum.paramId}`,
      pathIdKey: AppRoutesEnum.paramId,
      resource: "research-element",
      withId: true,
      cacheForbidden: true,
    },
    {
      name: "browse-project",
      path: `/${RoutesEnum.browseProject}`,
      withId: false,
      cacheForbidden: true,
    },
    {
      name: "browse-project-detail-metadata",
      path: `/${RoutesEnum.browseProjectDetail}/${AppRoutesEnum.paramId}`,
      pathIdKey: AppRoutesEnum.paramId,
      urlMatcher: `^\/${RoutesEnum.browseProjectDetail}\/${AppRoutesEnum.paramId}$|^\/${RoutesEnum.browseProjectDetail}\/${AppRoutesEnum.paramId}\/${BrowseProjectRoutesEnum.metadata}$`,
      resource: "project",
      withId: true,
      cacheForbidden: true,
    },
    {
      name: "browse-ontology",
      path: `/${RoutesEnum.browseOntology}`,
      withId: false,
      cacheForbidden: true,
    },
    {
      name: "browse-ontology-detail",
      path: `/${RoutesEnum.browseOntologyDetail}/${AppRoutesEnum.paramId}`,
      pathIdKey: AppRoutesEnum.paramId,
      resource: "ontology",
      withId: true,
      cacheForbidden: true,
    },
    /*{
      name: "sparql",
      path: `/${RoutesEnum.sparql}`,
      withId: false,
      cacheForbidden: true,
    },
    {
      name: "browse-project-detail-research-object",
      path: `/${RoutesEnum.browseProjectDetail}/${AppRoutesEnum.paramId}/${BrowseProjectRoutesEnum.researchObject}`,
      pathIdKey: AppRoutesEnum.paramId,
      resource: "project",
      withId: true,
      cacheForbidden: true,
    },
    {
      name: "browse-project-detail-research-object-detail",
      path: `/${RoutesEnum.browseProjectDetail}/${AppRoutesEnum.paramId}/${BrowseProjectRoutesEnum.researchObject}/:id2`,
      pathIdKey: ":id2",
      resource: "research-object",
      withId: true,
      cacheForbidden: true,
    },
    {
      name: "browse-project-detail-research-data-file",
      path: `/${RoutesEnum.browseProjectDetail}/${AppRoutesEnum.paramId}/${BrowseProjectRoutesEnum.researchDataFile}`,
      pathIdKey: AppRoutesEnum.paramId,
      resource: "project",
      withId: true,
      cacheForbidden: true,
    },
    {
      name: "browse-project-detail-research-data-file-detail",
      path: `/${RoutesEnum.browseProjectDetail}/${AppRoutesEnum.paramId}/${BrowseProjectRoutesEnum.researchDataFile}/:id2`,
      pathIdKey: ":id2",
      resource: "research-data-file",
      withId: true,
      cacheForbidden: true,
    },
    {
      name: "browse-project-detail-rdf-object",
      path: `/${RoutesEnum.browseProjectDetail}/${AppRoutesEnum.paramId}/${BrowseProjectRoutesEnum.rdfObject}`,
      pathIdKey: AppRoutesEnum.paramId,
      resource: "project",
      withId: true,
      cacheForbidden: true,
    },
    {
      name: "browse-project-detail-manifest",
      path: `/${RoutesEnum.browseProjectDetail}/${AppRoutesEnum.paramId}/${BrowseProjectRoutesEnum.manifest}`,
      pathIdKey: AppRoutesEnum.paramId,
      resource: "project",
      withId: true,
      cacheForbidden: true,
    },
    {
      name: "browse-project-detail-iiif-collection-detail",
      path: `/${RoutesEnum.browseProjectDetail}/${AppRoutesEnum.paramId}/${BrowseProjectRoutesEnum.iiifCollection}/:id2`,
      pathIdKey: ":id2",
      resource: "iiif-collection",
      withId: true,
      cacheForbidden: true,
    },
    {
      name: "browse-project-detail-iiif-collection",
      path: `/${RoutesEnum.browseProjectDetail}/${AppRoutesEnum.paramId}/${BrowseProjectRoutesEnum.iiifCollection}`,
      pathIdKey: AppRoutesEnum.paramId,
      resource: "project",
      withId: true,
      cacheForbidden: true,
    },*/
  ],
};
