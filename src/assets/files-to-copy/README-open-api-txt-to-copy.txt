Do not edit file inside this folder manually !
Generated automatically with `npm run generate-models` command.

This models file are partials and need to be re-exported into `src\app\models\index.ts`.
This allow to add missing attribute transparently for the rest of the app.

How to reexport into `src\app\models\index.ts`
import {Deposit as DepositPartial} from "../generated-api/model/deposit.partial.model";
export interface Deposit extends DepositPartial {
  extraProperty?: number;
}

Usage :
The rest of the app need to import model only form `src\app\models\index.ts` :
import {Deposit} from "@models";