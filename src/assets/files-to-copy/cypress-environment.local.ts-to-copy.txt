import {CypressEnvironmentModel} from "./cypress-environment.model";

export const cypressEnvironmentLocal: CypressEnvironmentModel | any = {
  tokenAdmin: "DEFINE HERE TOKEN ADMIN",
  tokenRoot: "DEFINE HERE TOKEN ROOT",
  tokenUser: "DEFINE HERE TOKEN USER",
};
