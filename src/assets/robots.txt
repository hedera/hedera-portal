User-agent: *
Disallow: /dataset
Disallow: /admin
Disallow: /shiblogin
Disallow: /shiblogin-mfa
Disallow: /oauth
Sitemap: /sitemap.xml
Crawl-delay: 40
