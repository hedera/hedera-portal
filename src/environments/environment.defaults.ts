/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - environment.defaults.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

// This file should contains all the defaults settings shared in all environment files
// If you want to override properties for specific environment, do it in these files

import {ImageDisplayModeEnum} from "@app/shared/enums/image-display-mode.enum";
import {ThemeEnum} from "@app/shared/enums/theme.enum";
import {Enums} from "@enums";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {ViewModeTableEnum} from "@shared/enums/view-mode-table.enum";
import {defaultSolidifyApplicationEnvironment} from "solidify-frontend";
import {HederaEnvironment} from "./environment.defaults.model";

export const defaultEnvironment: HederaEnvironment = {
  ...defaultSolidifyApplicationEnvironment,
  appTitle: "Hedera",
  appDescription: LabelTranslateEnum.metaGeneralDescription,
  imageOpenGraph: "themes/hedera/toolbar-header-image.png",
  listTagsComponentsWhereClickAllowToEnterInEditMode: [
    "hedera-shared-table-project-role-container",
    "hedera-shared-table-person-role-container",
    "hedera-shared-table-project-container",
    "solidify-rich-text-editor",
    "hedera-shared-code-editor",
    "hedera-shared-sparql-code-editor",
  ],
  authorization: "http://localhost:4500",
  defaultLanguage: Enums.Language.LanguageEnum.en,
  appLanguages: [Enums.Language.LanguageEnum.en, Enums.Language.LanguageEnum.fr],
  appLanguagesTranslate: Enums.Language.LanguageEnumTranslate,
  appThemes: [ThemeEnum.hedera],
  appThemesTranslate: [
    {key: ThemeEnum.hedera, value: "hedera"},
  ],
  theme: ThemeEnum.hedera,
  routeHomePage: RoutesEnum.homePage,
  routeSegmentEdit: AppRoutesEnum.edit,
  displayPrivacyPolicyAndTermsOfUseApprovalDialog: false,
  fileExtensionToCompleteWithRealExtensionOnDownload: ["dua", "thumbnail", "readme"],
  aboutModulesToIgnore: ["oaiPmh"],
  swaggerModulesToIgnore: ["oaiInfo", "oaiPmh", "authorization"],
  iiifProtocolVersion: "3",
  useDownloadToken: false,

  // Base URL For Module Applications (override by AppAction.LoadModules)
  admin: "http://localhost:2222/hedera-administration/admin",

  // Documentation
  documentationTocApisGuide: "hedera-APIs-toc.html",
  documentationTocIntegrationGuide: "hedera-IntegrationGuide-toc.html",
  documentationTocToolsGuide: "hedera-ToolsGuide-toc.html",
  documentationTocUserGuidePath: undefined,
  documentationTocUserGuide: undefined,

  // Orcid
  orcidQueryParam: "orcid",
  orcidAuthorizeUrl: "https://orcid.org/oauth/authorize",

  // Access request
  accessRequestRoute: "accessRequest",

  institutionUrl: "https://www.unige.ch/",
  spdxUrl: "https://spdx.org/licenses/",

  // Home
  defaultHomeViewModeTableEnum: ViewModeTableEnum.list,
  defaultPageSizeHomePage: 10,
  defaultHomeProjectSize: 1000,
  carouselUrl: undefined,
  frequencyChangeCarouselTileInSecond: 5,
  frequencyCleanCacheArchiveAccessRightInSecond: 60,

  twitterEnabled: true,
  twitterAccount: "UNIGEnews",

  searchFacetValuesTranslations: [
    ...Enums.Access.AccessEnumTranslate.map(v => ({
      name: Enums.Facet.Name.ACCESS_LEVEL_FACET,
      value: v.key,
      isFrontendTranslation: true,
      labelToTranslate: v.value,
    })),
    ...Enums.DataSensitivity.DataSensitivityEnumTranslate.map(v => ({
      name: Enums.Facet.Name.DATA_TAG_FACET,
      value: v.key,
      isFrontendTranslation: true,
      labelToTranslate: v.value,
    })),
  ],

  urlNationalArchivePronom: "https://www.nationalarchives.gov.uk/PRONOM/",
  sparqlHelpLink: "https://www.w3.org/TR/sparql11-query/",

  // Polling
  refreshTabStatusCounterIntervalInSecond: 60,
  refreshOrderAvailableIntervalInSecond: 5,
  refreshNotificationInboxAvailableIntervalInSecond: 60,
  refreshDatasetSubmittedIntervalInSecond: 2,
  refreshSipProcessIntervalInSecond: 2,

  // Errors
  httpErrorKeyToSkipInErrorHandler: [],

  archivalStorageName: [
    "File",
    "S2",
  ],
  defaultStorageIndex: 0,
  fileSizeLimitForChecksumGenerationInMegabytes: 100,

  listFacetUseOperatorAnd: [Enums.Facet.Name.CREATOR_FACET],

  oaiProviderCustomParam: "smartView=hedera_oai2.xsl",

  avatarSizeDefault: 300,
  avatarSizeMapping: {
    [ImageDisplayModeEnum.MAIN]: 300,
    [ImageDisplayModeEnum.UPLOAD]: 145,
    [ImageDisplayModeEnum.IN_TILE]: 200,
    [ImageDisplayModeEnum.IN_OVERLAY]: 125,
    [ImageDisplayModeEnum.IN_LIST]: 45,
    [ImageDisplayModeEnum.NEXT_FORM_FIELD]: 45,
  },
  imageSizeDefault: 400,
  imageSizeMapping: {
    [ImageDisplayModeEnum.MAIN]: 400,
    [ImageDisplayModeEnum.UPLOAD]: 200,
    [ImageDisplayModeEnum.IN_TILE]: 200,
    [ImageDisplayModeEnum.IN_OVERLAY]: 125,
    [ImageDisplayModeEnum.IN_LIST]: 50,
    [ImageDisplayModeEnum.NEXT_FORM_FIELD]: 50,
  },
  uploadImageDefaultAspectRatio: 1,
  uploadAvatarDefaultAspectRatio: 1,

  visualizationTextFileHighlightLanguages: [
    Enums.HighlightJS.HighlightLanguageEnum.xml,
    Enums.HighlightJS.HighlightLanguageEnum.json,
    Enums.HighlightJS.HighlightLanguageEnum.shexc,
  ],
  visualizationTextFileHighlightMaxFileSizeInMegabytes: 0.5,

  codeEditorThemeDark: "material-darker",
  codeEditorThemeLight: "eclipse",
};
