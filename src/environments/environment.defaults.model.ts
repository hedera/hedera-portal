/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - hedera Portal - environment.defaults.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */



import {Enums} from "@enums";
import {ViewModeTableEnum} from "@shared/enums/view-mode-table.enum";
import {DefaultSolidifyApplicationEnvironment} from "solidify-frontend";

export interface HederaEnvironment extends DefaultSolidifyApplicationEnvironment {
  // Base URL For Module Applications (override by AppAction.LoadModules)
  authorization?: string;
  admin: string;
  ingest?: string;
  access?: string;
  sparql?: string;
  linkedData?: string;
  ontologies?: string;
  iiif?: string;
  swaggerModulesToIgnore: string[];
  iiifProtocolVersion?: string;

  defaultStorageIndex: number;
  archivalStorageName: string[];

  // Documentation
  documentationTocApisGuide: string | undefined;
  documentationTocIntegrationGuide: string | undefined;
  documentationTocToolsGuide: string | undefined;
  documentationTocUserGuidePath: string | undefined;
  documentationTocUserGuide: string | undefined;

  // Orcid properties
  orcidQueryParam: string;
  orcidAuthorizeUrl: string;

  // Access request
  accessRequestRoute: string;

  // Others
  institutionUrl: string;
  spdxUrl: string;

  // Home
  carouselUrl: string | undefined;
  defaultPageSizeHomePage: number;
  defaultHomeProjectSize: number;
  defaultHomeViewModeTableEnum: ViewModeTableEnum;
  frequencyChangeCarouselTileInSecond: number;
  frequencyCleanCacheArchiveAccessRightInSecond: number;

  urlNationalArchivePronom: string;

  doiLink: string;
  sparqlHelpLink: string;

  // Polling
  refreshTabStatusCounterIntervalInSecond: number;
  refreshOrderAvailableIntervalInSecond: number;
  refreshNotificationInboxAvailableIntervalInSecond: number;
  refreshDatasetSubmittedIntervalInSecond: number;
  refreshSipProcessIntervalInSecond: number;

  fileSizeLimitForChecksumGenerationInMegabytes: number;

  listFacetUseOperatorAnd: Enums.Facet.Name[];

  // Theme used should be defined in main.scss
  codeEditorThemeDark: string;
  codeEditorThemeLight: string;
}
