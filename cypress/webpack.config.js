var path = require('path');

module.exports = {
    mode: 'development',
    resolve: {
        extensions: ['.ts', '.js'],
        alias: {
            '@src': path.resolve(__dirname, '../src'),
            '@shared': path.resolve(__dirname, '../src/app/shared'),
            '@cypress-environments': path.resolve(__dirname, './environments'),
            '@environments': path.resolve(__dirname, '../src/environments'),
            '@app': path.resolve(__dirname, '../src/app'),
            '@admin': path.resolve(__dirname, '../src/app/features/admin'),
            '@ingest': path.resolve(__dirname, '../src/app/features/ingest'),
            '@home': path.resolve(__dirname, '../src/app/features/home'),
            '@preservation-planning': path.resolve(__dirname, '../src/app/features/preservation-planning'),
            '@preservation-space': path.resolve(__dirname, '../src/app/features/preservation-space'),
            '@order': path.resolve(__dirname, '../src/app/features/order'),
            '@models': path.resolve(__dirname, '../src/app/models/index'),
            '@enums': path.resolve(__dirname, '../src/app/enums/index'),
            'solidify-frontend': path.resolve(__dirname, '../node_modules/solidify-frontend')
        }
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                exclude: [/node_modules/],
                use: [
                    {
                        loader: 'ts-loader',
                        options: {
                            // skip typechecking for speed
                            transpileOnly: true
                        }
                    }
                ]
            }
        ]
    }
}
