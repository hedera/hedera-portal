import {HttpStatusCode} from "@angular/common/http";
import {Enums} from "@src/app/enums";
import {DataTestEnum} from "@src/app/shared/enums/data-test.enum";
import {HttpVerbEnum} from "@src/app/shared/enums/http-verb.enum";
import {ModuleLoadedEnum} from "@src/app/shared/enums/module-loaded.enum";
import {TestHelper} from "./test-helper";

describe("Dataset Page", () => {
  beforeEach(() => {
    TestHelper.login(Enums.UserApplicationRole.UserApplicationRoleEnum.root);
    TestHelper.waitXhr("preingest/datasets?*", () => {
      cy.wait(2000); // MANDATORY FOR NOT RANDOM ERROR...
      TestHelper.getTestData(DataTestEnum.linkMenuIngest).click();
    }, {
      expectedHttpCode: undefined,
    });

    TestHelper.waitModuleLoaded(ModuleLoadedEnum.ingestModuleLoaded);
  });

  it("visit ingest", () => {
    TestHelper.getTestData(DataTestEnum.ingestDataTable).should("exist");
  });

  it("should create a ingest", () => {
    cy.get("#add-dataset-btn").click();
    TestHelper.getTestData(DataTestEnum.ingestTitle).type("Dataset Test 1");
    TestHelper.getTestData(DataTestEnum.ingestDescription).type("Description of ingest");
    TestHelper.getTestData(DataTestEnum.ingestPublicationDate).invoke("val").should("not.be.empty");
    // simulate click event on the drop down
    TestHelper.getTestData(DataTestEnum.ingestAccessLevel).first().click().get("mat-option").contains("Public").click();
    TestHelper.getTestData(DataTestEnum.ingestAddMeAuthor).click();

    /*    TestHelper.getTestData(DataTestEnum.datasetLicenseId)
              .click()
              .get("solidify-searchable-single-select-content")
              .click()
              .get("li")
              .contains("CC BY-NC 4.0 (Creative Commons Attribution-NonCommercial 4.0 International)")
              .click();*/

    TestHelper.getTestData(DataTestEnum.ingestLanguage).first().click().get("mat-option").contains("en").click();

    cy.server();
    TestHelper.waitXhr("preingest/datasets", () => TestHelper.getTestData(DataTestEnum.save).click(), {
      method: HttpVerbEnum.POST,
      expectedHttpCode: HttpStatusCode.Ok,
    });

    //click submit button to save
    cy.route("GET", TestHelper.xhrBaseUrl + "preingest/datasets/*/data?*").as("datasetData");
    cy.route("GET", TestHelper.xhrBaseUrl + "preingest/datasets/*/aip?*").as("datasetAip");

    cy.wait("@datasetData").then((xhr) => {
      expect(xhr.status).to.eq(200);
    });
    cy.wait("@datasetAip").then((xhr) => {
      expect(xhr.status).to.eq(200);
    });

    //open upload file dialog
    cy.get("#dataset-upload-primary-data").click();

    // fill form
    // TestHelper.getTestData(DataTestEnum.datasetDataCategory).first().click().get("mat-option").contains("Primary").click();
    TestHelper.getTestData(DataTestEnum.ingestDataType).first().click().get("mat-option").contains("Reference").click();

    // load mock data from a fixture or construct here
    const fileName = "example.json";
    cy.fixture(fileName).then(fileContent => {
      cy.get("#file-upload").upload({fileContent, fileName, mimeType: "application/json"});
    });

    cy.get("#dataset-upload-confirm").click();

    // navigate to upload file tab
    cy.get("#tab-files").click();

    cy.route({
      url: TestHelper.xhrBaseUrl + "data?size=10&page=0&relativeLocation=/",
      method: "GET",
    }).as("listFiles");

    cy.wait("@listFiles").then((xhr) => {
      expect(xhr.status).to.eq(200);
    });

    TestHelper.getTestData(DataTestEnum.ingestFileDataTable).then($table => {
      const rowsCount = $table.find("tbody").find("tr").length;
      //expect to have at least one row with data
      expect(rowsCount).to.be.greaterThan(1);
    });

  });

});
