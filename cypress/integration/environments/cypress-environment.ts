import {cypressEnvironmentLocal} from "./cypress-environment.local";
import {CypressEnvironmentModel} from "./cypress-environment.model";

export const cypressEnvironment: CypressEnvironmentModel = {
  rootUrl: "http://localhost:4500",
  localAuth: false,
  ...cypressEnvironmentLocal,
};
