import {HttpStatusCode} from "@angular/common/http";
import {Enums} from "@src/app/enums";
import {ApiResourceNameEnum} from "@src/app/shared/enums/api-resource-name.enum";
import {DataTestEnum} from "@src/app/shared/enums/data-test.enum";
import {HttpVerbEnum} from "@src/app/shared/enums/http-verb.enum";
import {ModuleLoadedEnum} from "@src/app/shared/enums/module-loaded.enum";
import {dataTestAttributeName} from "@src/app/shared/shared.const";
import {
  isEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  SessionStorageHelper,
} from "solidify-frontend";
import {cypressEnvironment} from "./environments/cypress-environment";
import Chainable = Cypress.Chainable;
import WaitXHR = Cypress.WaitXHR;

export class TestHelper {
  static xhrBaseUrl: string = "http*://**/";

  private static _waitAppReady(): void {
    cy.server();
    this.waitXhr(ApiResourceNameEnum.SYSTEM_PROPERTY, () => cy.visit(cypressEnvironment.rootUrl));
    cy.wait(250); // In some case app is not still init after the request to ApiResourceNameEnum.SYSTEM_PROPERTY
  }

  static login(applicationRole: Enums.UserApplicationRole.UserApplicationRoleEnum): void {
    let token: string | undefined = undefined;
    if (!cypressEnvironment.localAuth) {
      switch (applicationRole) {
        case Enums.UserApplicationRole.UserApplicationRoleEnum.root:
          token = cypressEnvironment.tokenRoot;
          break;
        case Enums.UserApplicationRole.UserApplicationRoleEnum.admin:
          token = cypressEnvironment.tokenAdmin;
          break;
        case Enums.UserApplicationRole.UserApplicationRoleEnum.user:
          token = cypressEnvironment.tokenUser;
          break;
        case Enums.UserApplicationRole.UserApplicationRoleEnum.guest:
        default:
          this.logout();
          return;
      }
      if (isNotNullNorUndefined(token)) {
        SessionStorageHelper.setItem("access_token", token as string);
      }
    }

    this._waitAppReady();

    if (isNotNullNorUndefined(token)) {
      return;
    }
    // For dev env only
    const accessToken = SessionStorageHelper.getItem("access_token");
    if (isNullOrUndefined(accessToken) || isEmptyString(accessToken)) {
      this.getTestData(DataTestEnum.loginHorizontalInput).click();
    }
  }

  static logout(): void {
    SessionStorageHelper.removeItem("access_token");
    SessionStorageHelper.removeItem("refresh_token");
    this._waitAppReady();
  }

  static testData(dataTest: DataTestEnum): string {
    return `[${dataTestAttributeName}=${dataTest}]`;
  }

  static getTestData(dataTest: DataTestEnum): Cypress.Chainable<JQuery<HTMLElement>> {
    return cy.get(this.testData(dataTest));
  }

  static waitModuleLoaded(module: ModuleLoadedEnum): Cypress.Chainable<Window[any]> {
    return cy.window().its(module as any);
  }

  static waitXhrs(listUrls: string[], triggerCallback: () => any | undefined): Cypress.Chainable<Cypress.WaitXHR[]> {
    const listAlias: string[] = [];
    const baseNameAliasXhr = "xhr";
    listUrls.forEach((url, index) => {
      const nameAliasXhr = baseNameAliasXhr + index;
      cy.route(TestHelper.xhrBaseUrl + url).as(nameAliasXhr);
      listAlias.push("@" + nameAliasXhr);
    });
    if (isNotNullNorUndefined(triggerCallback)) {
      triggerCallback();
    }
    return cy.wait(listAlias);
  }

  static waitXhr(url: string, triggerCallback: () => any | undefined, opts?: WaitXhrOptions): Chainable<WaitXHR> {
    let options = new WaitXhrOptions();
    if (isNotNullNorUndefined(opts)) {
      options = opts as WaitXhrOptions;
    }
    if (isNullOrUndefined(options.method)) {
      cy.route(this.xhrBaseUrl + url).as("xhr");
    } else {
      cy.route(options.method, this.xhrBaseUrl + url).as("xhr");
    }
    if (isNotNullNorUndefined(triggerCallback)) {
      triggerCallback();
    }
    return cy.wait("@xhr").then((xhr) => {
      if (isNotNullNorUndefined(options.expectedHttpCode)) {
        expect(xhr.status).to.eq(options.expectedHttpCode);
      }
    });
  }

  static clickBackdrop(): void {
    cy.get(".cdk-overlay-backdrop").click();
  }
}

export class WaitXhrOptions {
  method?: HttpVerbEnum = HttpVerbEnum.GET;
  expectedHttpCode?: number = HttpStatusCode.Ok;
}
