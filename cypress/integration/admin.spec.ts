import {Enums} from "@src/app/enums";
import {DataTestEnum} from "@src/app/shared/enums/data-test.enum";
import {ModuleLoadedEnum} from "@src/app/shared/enums/module-loaded.enum";
import {ProjectHelper} from "./project-helper";
import {TestHelper} from "./test-helper";

describe("Admin Page", () => {
  beforeEach(() => {
    TestHelper.login(Enums.UserApplicationRole.UserApplicationRoleEnum.root);
    cy.get(".avatar-wrapper").first().click();
    cy.wait(1000); // MANDATORY FOR NOT RANDOM ERROR...
    TestHelper.getTestData(DataTestEnum.linkMenuAdmin).click();
    cy.wait(1000); // MANDATORY FOR NOT RANDOM ERROR...
    TestHelper.waitModuleLoaded(ModuleLoadedEnum.adminModuleLoaded);
  });

  it("visit admin page", () => {
    cy.get("#admin-home-title").contains("Administration");
    // this assert depends on the user right
    cy.get("mat-card").should("have.length", 18);
  });

  it("visit organization unit", () => {
    TestHelper.getTestData(DataTestEnum.adminTileProject).click();
    cy.get("table").find("tr").its("length").should("be.gt", 1);
    // go back
    TestHelper.getTestData(DataTestEnum.back).click();
  });

  it("visit submission policy", () => {
    TestHelper.getTestData(DataTestEnum.adminTileSubmissionPolicy).click();
    cy.get("table").find("tr").its("length").should("be.gt", 1);
    //go back
    TestHelper.getTestData(DataTestEnum.back).click();
  });

  it("visit oauth2 client", () => {
    TestHelper.getTestData(DataTestEnum.adminTileOauth2).click();
    // assert that we have at least two client
    cy.get("table").find("tr").its("length").should("be.gt", 1);
    //go back
    TestHelper.getTestData(DataTestEnum.back).click();
  });

  it("visit user", () => {
    TestHelper.getTestData(DataTestEnum.adminTileUsers).click();
    // assert that we have at least two person
    cy.get("table").find("tr").its("length").should("be.gt", 1);
    //go back
    TestHelper.getTestData(DataTestEnum.back).click();
  });

  it("create org unit", () => {
    ProjectHelper.createOrgUnit(undefined);
  });
});
