import {HttpStatusCode} from "@angular/common/http";
import {Enums} from "@src/app/enums";
import {DataTestEnum} from "@src/app/shared/enums/data-test.enum";
import {HttpVerbEnum} from "@src/app/shared/enums/http-verb.enum";
import {ModuleLoadedEnum} from "@src/app/shared/enums/module-loaded.enum";
import {
  Guid,
  isNotNullNorUndefined,
} from "solidify-frontend";
import {TestHelper} from "./test-helper";

export class ProjectHelper {
  static projectBaseNamePermanent: string = "[Cypress Permanent Test Data] Org Unit";
  static projectBaseNameTemporary: string = "[Cypress Temporary Test Data] Org Unit";

  private static _generateProjectName(role: Enums.Role.RoleEnum | undefined, forPrerequisiteTest: boolean): string {
    let projectName = forPrerequisiteTest ? this.projectBaseNamePermanent : this.projectBaseNameTemporary;
    let forRoleText = "";
    if (isNotNullNorUndefined(role)) {
      forRoleText = " for " + role;
    } else {
      forRoleText = " for " + "NOBODY";
    }
    projectName = projectName + forRoleText;
    if (!forPrerequisiteTest) {
      projectName = projectName + " " + Guid.MakeNew().ToString().toLowerCase();
    }
    return projectName;
  }

  static createIfNotExistProjectForPrerequisiteTest(role: Enums.Role.RoleEnum | undefined): string {
    const projectName = this._generateProjectName(role, true);
    TestHelper.login(Enums.UserApplicationRole.UserApplicationRoleEnum.root);

    // Go to admin page
    cy.get(".avatar-wrapper").first().click();
    TestHelper.getTestData(DataTestEnum.linkMenuAdmin).click();
    TestHelper.waitModuleLoaded(ModuleLoadedEnum.adminModuleLoaded);

    // Go to org unit list page
    TestHelper.getTestData(DataTestEnum.adminTileProject).click();
    TestHelper.waitModuleLoaded(ModuleLoadedEnum.adminProjectModuleLoaded);

    TestHelper.getTestData(DataTestEnum.adminProjectListSearchName).type(projectName, {delay: 0, release: false});

    cy.get("solidify-data-table").should("attr", "data-test-info-number-line").then((numberElement: number | any) => {
      if (numberElement === "1") {
        return;
      } else if (numberElement === "0") {
        this.createProject(role, true);
      } else {
        expect(+numberElement).to.be.lte(1);
      }
    });

    return projectName;
  }

  static createProject(role: Enums.Role.RoleEnum | undefined, forPrerequisiteTest: boolean = false): string {
    const projectName = this._generateProjectName(role, forPrerequisiteTest);
    TestHelper.login(Enums.UserApplicationRole.UserApplicationRoleEnum.root);

    // Go to admin page
    cy.get(".avatar-wrapper").first().click();
    TestHelper.getTestData(DataTestEnum.linkMenuAdmin).click();
    TestHelper.waitModuleLoaded(ModuleLoadedEnum.adminModuleLoaded);

    // Go to org unit list page
    TestHelper.getTestData(DataTestEnum.adminTileProject).click();

    // Go to org unit creation page
    TestHelper.waitXhrs([
      "admin/preservation-policies?*",
      "admin/submission-policies?*",
      "admin/dissemination-policies?*",
      "admin/roles?*",
    ], () => TestHelper.getTestData(DataTestEnum.create).click());

    TestHelper.getTestData(DataTestEnum.adminProjectName).type(projectName);

    TestHelper.getTestData(DataTestEnum.adminProjectSubmissionPolicy)
      .click()
      .get("solidify-multi-select-default-value-content li")
      .first()
      .click();

    TestHelper.clickBackdrop();

    TestHelper.getTestData(DataTestEnum.adminProjectPreservationPolicy)
      .click()
      .get("solidify-multi-select-default-value-content li")
      .first()
      .click();

    TestHelper.clickBackdrop();

    if (isNotNullNorUndefined(role)) {
      TestHelper.getTestData(DataTestEnum.sharedPersonProjectRoleButtonAdd).click();

      cy.route(TestHelper.xhrBaseUrl + "admin/people?*")
        .as("personInit");
      cy.route(TestHelper.xhrBaseUrl + "admin/people/search?*")
        .as("personSearch");
      TestHelper.getTestData(DataTestEnum.sharedPersonProjectRoleInputPerson)
        .last()
        .click()
        .wait("@personInit")
        .get("solidify-searchable-single-select-content input")
        .type("USER Last Name")
        .wait("@personSearch")
        .get("solidify-searchable-single-select-content li")
        .first()
        .click();

      TestHelper.getTestData(DataTestEnum.sharedPersonProjectRoleInputRole)
        .last()
        .click();
      TestHelper.getTestData(role as any).click();
    }

    TestHelper.waitXhr("admin/projects/*", () => TestHelper.getTestData(DataTestEnum.save).click(), {
      expectedHttpCode: HttpStatusCode.Ok,
      method: HttpVerbEnum.GET,
    });
    return projectName;
  }
}
