import {HttpStatusCode} from "@angular/common/http";
import {Enums} from "@enums";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {HttpVerbEnum} from "@shared/enums/http-verb.enum";
import {TestHelper} from "../test-helper";

describe("Home Page", () => {
  beforeEach(() => {
    TestHelper.login(Enums.UserApplicationRole.UserApplicationRoleEnum.guest);
  });

  it("check the title", () => {
    cy.title().should("eq", "hedera - Portal");
  });

  it("check that login is visible", () => {
    TestHelper.getTestData(DataTestEnum.loginHorizontalInput).should("be", "visible");
  });

  it("check that search of public archive is working", () => {
    cy.wait(100);
    cy.server();
    TestHelper.waitXhr("access/metadata/search?size=*&page=0&query=hedera", () => TestHelper.getTestData(DataTestEnum.homeSearchInput)
      .type("hedera"), {
      method: HttpVerbEnum.POST,
      expectedHttpCode: HttpStatusCode.Ok,
    });

    TestHelper.waitXhr("access/metadata/search?size=*&page=0&query=hedera", () => TestHelper.getTestData(DataTestEnum.homeButtonViewList).click(), {
      method: HttpVerbEnum.POST,
      expectedHttpCode: HttpStatusCode.Ok,
    });

    TestHelper.getTestData(DataTestEnum.homeDataTableSearch).should("exist");

    TestHelper.waitXhr("access/metadata/search?size=*&page=0*", () => {
      cy.get(TestHelper.testData(DataTestEnum.homeFacet + "-access-levels-PUBLIC") + " [type=\"checkbox\"]").not("[disabled]")
        .click({force: true}).should("be.checked");
    }, {
      method: HttpVerbEnum.POST,
      expectedHttpCode: HttpStatusCode.Ok,
    });

    TestHelper.getTestData(DataTestEnum.homeDataTableSearch).then($table => {
      const list = $table.find("tbody").find("tr");
      const rowsCount = list.length;
      //expect to have at least one row with data
      expect(rowsCount).to.be.greaterThan(1);

      TestHelper.waitXhr("access/metadata/*", () => list[0].click(), {
        method: HttpVerbEnum.GET,
        expectedHttpCode: HttpStatusCode.Ok,
      });
    });

    TestHelper.getTestData(DataTestEnum.homeArchiveDetailDownloadButton).should("exist");
    TestHelper.getTestData(DataTestEnum.homeArchiveDetailLinkFilesOrCollections).should("exist");
  });

  it("check that search of restricted archive is working", () => {
    cy.wait(100);
    cy.server();
    TestHelper.waitXhr("access/metadata/search?size=*&page=0&query=hedera", () => TestHelper.getTestData(DataTestEnum.homeSearchInput)
      .type("hedera"), {
      method: HttpVerbEnum.POST,
      expectedHttpCode: HttpStatusCode.Ok,
    });

    TestHelper.waitXhr("access/metadata/search?size=*&page=0&query=hedera", () => TestHelper.getTestData(DataTestEnum.homeButtonViewList).click(), {
      method: HttpVerbEnum.POST,
      expectedHttpCode: HttpStatusCode.Ok,
    });

    TestHelper.getTestData(DataTestEnum.homeDataTableSearch).should("exist");

    TestHelper.waitXhr("access/metadata/search?size=*&page=0*", () => {
      cy.get(TestHelper.testData(DataTestEnum.homeFacet + "-access-levels-RESTRICTED") + " [type=\"checkbox\"]").not("[disabled]")
        .click({force: true}).should("be.checked");
    }, {
      method: HttpVerbEnum.POST,
      expectedHttpCode: HttpStatusCode.Ok,
    });

    TestHelper.getTestData(DataTestEnum.homeDataTableSearch).then($table => {
      const list = $table.find("tbody").find("tr");
      const rowsCount = list.length;
      //expect to have at least one row with data
      expect(rowsCount).to.be.greaterThan(1);

      TestHelper.waitXhr("access/metadata/*", () => list[0].click(), {
        method: HttpVerbEnum.GET,
        expectedHttpCode: HttpStatusCode.Ok,
      });
    });

    TestHelper.getTestData(DataTestEnum.homeArchiveDetailDownloadButton).should("not.exist");
    TestHelper.getTestData(DataTestEnum.homeArchiveDetailRequestAccessButton).should("exist");
    TestHelper.getTestData(DataTestEnum.homeArchiveDetailLinkFilesOrCollections).should("not.exist");
  });

  it("check that search of closed archive is working", () => {
    cy.wait(100);
    cy.server();
    TestHelper.waitXhr("access/metadata/search?size=*&page=0&query=hedera", () => TestHelper.getTestData(DataTestEnum.homeSearchInput)
      .type("hedera"), {
      method: HttpVerbEnum.POST,
      expectedHttpCode: HttpStatusCode.Ok,
    });

    TestHelper.waitXhr("access/metadata/search?size=*&page=0&query=hedera", () => TestHelper.getTestData(DataTestEnum.homeButtonViewList).click(), {
      method: HttpVerbEnum.POST,
      expectedHttpCode: HttpStatusCode.Ok,
    });

    TestHelper.getTestData(DataTestEnum.homeDataTableSearch).should("exist");

    TestHelper.waitXhr("access/metadata/search?size=*&page=0*", () => {
      cy.get(TestHelper.testData(DataTestEnum.homeFacet + "-access-levels-CLOSED") + " [type=\"checkbox\"]").not("[disabled]")
        .click({force: true}).should("be.checked");
    }, {
      method: HttpVerbEnum.POST,
      expectedHttpCode: HttpStatusCode.Ok,
    });

    TestHelper.getTestData(DataTestEnum.homeDataTableSearch).then($table => {
      const list = $table.find("tbody").find("tr");
      const rowsCount = list.length;
      //expect to have at least one row with data
      expect(rowsCount).to.be.greaterThan(1);

      TestHelper.waitXhr("access/metadata/*", () => list[0].click(), {
        method: HttpVerbEnum.GET,
        expectedHttpCode: HttpStatusCode.Ok,
      });
    });

    TestHelper.getTestData(DataTestEnum.homeArchiveDetailDownloadButton).should("not.exist");
    TestHelper.getTestData(DataTestEnum.homeArchiveDetailRequestAccessButton).should("exist");
    TestHelper.getTestData(DataTestEnum.homeArchiveDetailLinkFilesOrCollections).should("not.exist");
  });
});
