#### [hedera-1.0.2](https://gitlab.unige.ch/hedera/hedera-portal/compare/hedera-1.0.1...hedera-1.0.2) (2024-10-17)

##### New Features

* **home:**  [HEDERA-360] display research object and datafile differently ([fe1ae7a8](https://gitlab.unige.ch/hedera/hedera-portal/commit/fe1ae7a8e83ea8fe5282062fb54134bf6116567f))

##### Bug Fixes

*  [HEDERA-369] use iiif for thumbnail ([c5fe4a63](https://gitlab.unige.ch/hedera/hedera-portal/commit/c5fe4a63a16a4f67b92b9eb2835a9d0e4ca3eabb))

##### Code Style Changes

* **home:**
  *  allow to display thumbnail for file other than image but managed by iiif ([b53529cd](https://gitlab.unige.ch/hedera/hedera-portal/commit/b53529cdaba9b048cb32fa700730b0075e2079e4))
  *  improve resolution of thumbnail ([a9a6da09](https://gitlab.unige.ch/hedera/hedera-portal/commit/a9a6da092f872266109afe4a4e2154bbcd9f5914))

