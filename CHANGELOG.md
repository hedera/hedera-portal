#### [hedera-1.0.2](https://gitlab.unige.ch/hedera/hedera-portal/compare/hedera-1.0.1...hedera-1.0.2) (2024-10-17)

##### New Features

* **home:**  [HEDERA-360] display research object and datafile differently ([fe1ae7a8](https://gitlab.unige.ch/hedera/hedera-portal/commit/fe1ae7a8e83ea8fe5282062fb54134bf6116567f))

##### Bug Fixes

*  [HEDERA-369] use iiif for thumbnail ([c5fe4a63](https://gitlab.unige.ch/hedera/hedera-portal/commit/c5fe4a63a16a4f67b92b9eb2835a9d0e4ca3eabb))

##### Code Style Changes

* **home:**
  *  allow to display thumbnail for file other than image but managed by iiif ([b53529cd](https://gitlab.unige.ch/hedera/hedera-portal/commit/b53529cdaba9b048cb32fa700730b0075e2079e4))
  *  improve resolution of thumbnail ([a9a6da09](https://gitlab.unige.ch/hedera/hedera-portal/commit/a9a6da092f872266109afe4a4e2154bbcd9f5914))

#### [hedera-1.0.1](https://gitlab.unige.ch/hedera/hedera-portal/compare/hedera-1.0.0...hedera-1.0.1) (2024-10-11)

##### Bug Fixes

* **home:**  [HEDERA-368] use relative location in download link of research data file ([1389303a](https://gitlab.unige.ch/hedera/hedera-portal/commit/1389303a9d52d83c08520f669ed804bd1cd1eeae))
*  avoid to display global banner on SSR ([0a9a0ba3](https://gitlab.unige.ch/hedera/hedera-portal/commit/0a9a0ba343008df05b042b94c062b0650a6f15c3))

##### Code Style Changes

*  increase grid width ([e610c38e](https://gitlab.unige.ch/hedera/hedera-portal/commit/e610c38e91e1eeaa62ef8fb59ea37aadc108dc19))

#### [hedera-1.0.0](https://gitlab.unige.ch/hedera/hedera-portal/compare/hedera-1.0.0-RC7...hedera-1.0.0) (2024-09-13)

##### New Features

* **iiif-collection-settings:**  make description mandatory ([8d5cea34](https://gitlab.unige.ch/hedera/hedera-portal/commit/8d5cea34181f2bcbc48f2e0bd9eed61fc0748b79))
*  add button to allow to navigate between browse, project and admin ([894a6a4d](https://gitlab.unige.ch/hedera/hedera-portal/commit/894a6a4d276368dcd2b203cda5889431c73a6270))
*  add button to allow to navigate between browse, project and admin ([b3ca10e1](https://gitlab.unige.ch/hedera/hedera-portal/commit/b3ca10e18e77cd2dd4ae26c3eaf4f2473c922305))

##### Other Changes

*  add button to allow to navigate between browse, project and admin" ([346a252d](https://gitlab.unige.ch/hedera/hedera-portal/commit/346a252d9422155c83e658f90c3aae5ebc38f2e0))

##### Code Style Changes

*  move navigation buttons between explore, import and admin ([b45955f2](https://gitlab.unige.ch/hedera/hedera-portal/commit/b45955f2ad58752ad963bc1090582cd3e4b9c633))

#### [hedera-1.0.0-RC7](https://gitlab.unige.ch/hedera/hedera-portal/compare/hedera-1.0.0-RC6...hedera-1.0.0-RC7) (2024-07-25)

##### Bug Fixes

* **browse:**  avoid not found error when refresh page on research object / data file detail ([18321c1a](https://gitlab.unige.ch/hedera/hedera-portal/commit/18321c1accf4a4255ca01c9f0730869b162fd663))
*  avoid ssr error on LOCALE_ID ([b8541d49](https://gitlab.unige.ch/hedera/hedera-portal/commit/b8541d4911dcc6e8184d1a32eba2504e8d07ef35))


## [1.0.0-RC6](https://gitlab.unige.ch/hedera/hedera-portal/compare/hedera-1.0.0-RC5...hedera-1.0.0-RC6) (2024-07-15)


### Features

* **ingest:** add button to purge RDF ([660eb41](https://gitlab.unige.ch/hedera/hedera-portal/commit/660eb410f8a8fe948609e25fd2ef0b2327e1bd17))

## [1.0.0-RC5](https://gitlab.unige.ch/hedera/hedera-portal/compare/hedera-1.0.0-RC4...hedera-1.0.0-RC5) (2024-07-10)


### Features

* [HEDERA-342] add project statistics ([afaad84](https://gitlab.unige.ch/hedera/hedera-portal/commit/afaad84ca9abfda6bb0162cafcb98e3557d88f4f))
* **admin:** improve admin research object type ([bf47abf](https://gitlab.unige.ch/hedera/hedera-portal/commit/bf47abfcec05a625c5793162ae48a720aeb3b440))
* **browse:** allow to use pagination on IIIF manifest and IIIF collection list ([5da3709](https://gitlab.unige.ch/hedera/hedera-portal/commit/5da3709f4c1f5d6a5ce4094a6639d6538c8bf779))


### Bug Fixes

* **browser:** manage correctly show detail on iiif item ([b347b32](https://gitlab.unige.ch/hedera/hedera-portal/commit/b347b329c8ec36e21afcd3456bc14843d3f17e3e))
* **deposit:** don't lose authorized project state value when change of project ([4d31f8e](https://gitlab.unige.ch/hedera/hedera-portal/commit/4d31f8e196c0ab7604581aa5eb18176b0682fc07))
* **home:** [HEDERA-345] allow to use image without extension as thumbnail ([15bab96](https://gitlab.unige.ch/hedera/hedera-portal/commit/15bab9600179297b6b0da8e02b1b962dd5742bc5))
* **research object type:** display version of ontology ([68e476a](https://gitlab.unige.ch/hedera/hedera-portal/commit/68e476aa06dc156af8f9d2dbf74f775740f9425f))

## [1.0.0-RC4](https://gitlab.unige.ch/hedera/hedera-portal/compare/hedera-1.0.0-RC3...hedera-1.0.0-RC4) (2024-06-07)

## [1.0.0-RC3](https://gitlab.unige.ch/hedera/hedera-portal/compare/hedera-1.0.0-RC2...hedera-1.0.0-RC3) (2024-06-06)


### Features

* display label in IIIF manifests & IIIF collections ([0e92013](https://gitlab.unige.ch/hedera/hedera-portal/commit/0e920135074878ff9a5fe124dec97aa96a43ae99))

## [1.0.0-RC2](https://gitlab.unige.ch/hedera/hedera-portal/compare/hedera-1.0.0-RC1...hedera-1.0.0-RC2) (2024-05-30)


### Features

* **project space:** add ontologies list in project space ([a5b2c52](https://gitlab.unige.ch/hedera/hedera-portal/commit/a5b2c52c7e3ca4af26f11f5a8e9319dbc741682c))
* **sparql:** add button to copy sparql query ([f7b509f](https://gitlab.unige.ch/hedera/hedera-portal/commit/f7b509f6cc7f542be7a892d033fae234580f491e))
* **ssr:** add meta for public pages ([f287afb](https://gitlab.unige.ch/hedera/hedera-portal/commit/f287afb1a4183e85084a16741bbcdea615d3b423))
* **ssr:** add ssr route config for research element ([e147363](https://gitlab.unige.ch/hedera/hedera-portal/commit/e1473631b7f72bac56daccdcc8eb8a40306cc4b9))
* support external ontology flag ([5835270](https://gitlab.unige.ch/hedera/hedera-portal/commit/5835270fa848e2e8cb7c43c3ee9100c69bcf4346))


### Bug Fixes

* lint error ([2955921](https://gitlab.unige.ch/hedera/hedera-portal/commit/295592123a9911f98c65a368519097d0f88812cb))
* **meta:** display default description on ontology and project when description is not provided ([7dad2f7](https://gitlab.unige.ch/hedera/hedera-portal/commit/7dad2f7f39ee8269dba8a52e26d80ed3f98f0f66))
* move user guide link on smartphone screen ([6ef2eab](https://gitlab.unige.ch/hedera/hedera-portal/commit/6ef2eaba0b2f4fc41c66ee38325b924f2327d4bd))
* **ontology:** download ontology and improve display of version in list ([fa42e62](https://gitlab.unige.ch/hedera/hedera-portal/commit/fa42e62b133d4da172aea2642cecb0613654df66))
* **sparql code editor:** add button to copy query ([2857eac](https://gitlab.unige.ch/hedera/hedera-portal/commit/2857eac9571f8bbe6c69f1332b9bed3f53c14f09))
* **SSR:** add redirect to avoid metadata in url to see project detail in browse ([420b6da](https://gitlab.unige.ch/hedera/hedera-portal/commit/420b6daee69d4c394ce016aa0a9105a38208a668))
* **SSR:** add SSR config to manage browse ontology and project ([8722010](https://gitlab.unige.ch/hedera/hedera-portal/commit/87220105e6588b64fbd6b3814ac48fb83c7d4ece))

## [1.0.0-RC1](https://gitlab.unige.ch/hedera/hedera-portal/compare/hedera-0.0.3...hedera-1.0.0-RC1) (2024-05-07)


### Features

* [HEDERA-209] make ResearchObjectType details query not mandatory ([e2313ad](https://gitlab.unige.ch/hedera/hedera-portal/commit/e2313ad48103776be4cc79cc80548cc49615a8f9))
* [Hedera-211] list of projects in project-space ([fccecac](https://gitlab.unige.ch/hedera/hedera-portal/commit/fccecac088eb334c0d5f2ca62fd56e64b9859d63))
* [Hedera-223] Adapt the RML file used to generate researchDataFile metadata ([8cf6224](https://gitlab.unige.ch/hedera/hedera-portal/commit/8cf62247015398bb22af8026b2fe68cb4c4048c0))
* [Hedera-224] add iiifManifestResearchObjectType and researchDataFileResearchObjectType in project ([68f6772](https://gitlab.unige.ch/hedera/hedera-portal/commit/68f67728aeea4f822066258e8ccef0d8d424439e))
* [Hedera-231] delete operation of RdfDatasetFile from triplestore ([fe2a3c4](https://gitlab.unige.ch/hedera/hedera-portal/commit/fe2a3c40dcfde73e4f5cd554345e703d2b9fec55))
* [Hedera-239] able to import multiple RDFDataFiles in triplestore ([c9cec8f](https://gitlab.unige.ch/hedera/hedera-portal/commit/c9cec8f190b40817b16595213e0431257bdbe1a0))
* [Hedera-267] add new type of accessible mode (WEB) for research data file ([e7c037d](https://gitlab.unige.ch/hedera/hedera-portal/commit/e7c037d7d5246b5d6eafe596cbb67cb267e06383))
* [HEDERA-325] allow to put in error all dataset types ([01db2ac](https://gitlab.unige.ch/hedera/hedera-portal/commit/01db2ac581c93a4f24bd2e64e8f57bf7458eecd0))
* add sparql code editor ([87e6453](https://gitlab.unige.ch/hedera/hedera-portal/commit/87e64539630b61f01915258f011fea7004a26c90))
* **admin:** add iiif collection settings admin module ([9ee067c](https://gitlab.unige.ch/hedera/hedera-portal/commit/9ee067c7f8efa8b4970b58578d9c425a124f034d))
* allow to copy list error message on bulk action result ([9f0daa9](https://gitlab.unige.ch/hedera/hedera-portal/commit/9f0daa9d22b19b448477b30b23cc8d6283fd4a5a))
* allow to navigate from rdf object to research object and allow to copy id ([7cd8dc9](https://gitlab.unige.ch/hedera/hedera-portal/commit/7cd8dc9dcce3bcd9dbbcb04094d6af7cd90f9b52))
* allow to see iiif link ([9f2f20d](https://gitlab.unige.ch/hedera/hedera-portal/commit/9f2f20d6781a58ac96207de220a51420d6d50d84))
* display overlay on ontology and research object type ([caacde3](https://gitlab.unige.ch/hedera/hedera-portal/commit/caacde3cc748bc8804965fd0711e789fde59854d))
* **home detail:** display buttons to see iiif infos ([cba73b2](https://gitlab.unige.ch/hedera/hedera-portal/commit/cba73b2e2b71f54ccb579c6f9d76cb9f5e920c43))
* **home detail:** display image of research datafile as thumbnail ([4d7f878](https://gitlab.unige.ch/hedera/hedera-portal/commit/4d7f878b6be0ab8db6c5abc7c9156c415e760f88))
* **home detail:** display rdf in research object / datafile ([2269353](https://gitlab.unige.ch/hedera/hedera-portal/commit/22693534e9b2c346dad4ec0f1cfe80d3d7be20e3))
* **home-research-object:** improve public page ([1d7afdb](https://gitlab.unige.ch/hedera/hedera-portal/commit/1d7afdbff3b3ba2dbb256b43458585031a1f956e))
* **home:** [HEDERA-39] add detail page for research object ([148ee87](https://gitlab.unige.ch/hedera/hedera-portal/commit/148ee878f74141122775dd4e5773f43b2835e416))
* **home:** allow to navigate research object using ld endpoint ([1c45e22](https://gitlab.unige.ch/hedera/hedera-portal/commit/1c45e22b85011ee625ebca60582e31a7c8f95824))
* **home:** display link on url metadata ([e87b064](https://gitlab.unige.ch/hedera/hedera-portal/commit/e87b06434aa0373d35c9aa5fc1e629cc918577b7))
* **home:** display list of research object ([73440ec](https://gitlab.unige.ch/hedera/hedera-portal/commit/73440ecc8d6bf9ec6ce2e3ece4fc7d19f0861647))
* **import:** [HEDERA-288] add new button to transform RDF ([1134250](https://gitlab.unige.ch/hedera/hedera-portal/commit/1134250d8cc7ca70b6653a65fec047b1f9875598))
* **import:** [HEDERA-293] add button to put in error research data file ([f557c68](https://gitlab.unige.ch/hedera/hedera-portal/commit/f557c6881ca5fa08623742793f071f1770cdb9e0))
* **import:** select rml value if only one value available on apply rml dialog ([f61eb3f](https://gitlab.unige.ch/hedera/hedera-portal/commit/f61eb3fa3c747d0a117496cfdbee1d21fff6b4cb))
* improve research object search result ([5f843de](https://gitlab.unige.ch/hedera/hedera-portal/commit/5f843decd5f3b896000b8143f4234c15523f13ac))
* integrate yasgui component ([3eb637c](https://gitlab.unige.ch/hedera/hedera-portal/commit/3eb637c586dbfb2ee2f30156dc5f0e9e6429b323))
* ontology mandatory for research object type ([170a358](https://gitlab.unige.ch/hedera/hedera-portal/commit/170a3589b3d7f5f7b770b19be1af1caea32ead78))
* **ontology:** [HEDERA-283-284] display classes and properties and allow to check part of ([44b7454](https://gitlab.unige.ch/hedera/hedera-portal/commit/44b74549756b5484184ef1b8fe58016a9da515f9))
* **project space:** [HEDERA-39] add research object browsing ([e537d4f](https://gitlab.unige.ch/hedera/hedera-portal/commit/e537d4ff97de6fc84a43cf0bf459449c9a0c526d))
* **project space:** display manifest and iiif collections tabs on project space ([865eb02](https://gitlab.unige.ch/hedera/hedera-portal/commit/865eb02dec294321b3c6a5a305d6396807098a21))
* **project space:** display research data file ([67cb537](https://gitlab.unige.ch/hedera/hedera-portal/commit/67cb537527d177f670ce9de70ad3c8a0d9a59296))
* **project-space:** [HEDERA-203] allow public user to navigate through project and research objets ([dcdef3c](https://gitlab.unige.ch/hedera/hedera-portal/commit/dcdef3ce5bbc8ac07443073c68d9247af9acf07c))
* **ProjectSpace:** [HEDERA-322] display list of iiif collections of project ([3070a3b](https://gitlab.unige.ch/hedera/hedera-portal/commit/3070a3be1887fb10d30773e5755894df855bfda0))
* **research data file:** [HEDERA-247] allow to reload all research data files ([853787a](https://gitlab.unige.ch/hedera/hedera-portal/commit/853787a70015858ce3b0774a8af4d53eea9de0fe))
* **research data file:** improve display research data file ([f1dc0e3](https://gitlab.unige.ch/hedera/hedera-portal/commit/f1dc0e3ba674b92396f4f501e0569e1161f08d37))
* **source-dataset:** [HEDERA-212] display number of source dataset file in source dataset list ([c94a1ed](https://gitlab.unige.ch/hedera/hedera-portal/commit/c94a1ed117bf3b5929b285310449391507d677bd))
* **source-dataset:** display source dataset status ([8c761d9](https://gitlab.unige.ch/hedera/hedera-portal/commit/8c761d91fcced144340995996a0fac507f034a80))
* **SourceDatasetFile:** [HEDERA-316] support research data file as source dataset file ([6ac6b27](https://gitlab.unige.ch/hedera/hedera-portal/commit/6ac6b27f9898828dd599bcb5f1127ce89202a78b))
* **sparql:** [HEDERA-240] allow to execute SparQL ([db1d994](https://gitlab.unige.ch/hedera/hedera-portal/commit/db1d9949fd4771006d7d26ab4ee71cd522a52e28))


### Bug Fixes

* [Hedera-225] error when deleting a researchDataFile from detail page ([5deba50](https://gitlab.unige.ch/hedera/hedera-portal/commit/5deba5048e85f65644f348c8620928b524240289))
* [HEDERA-287] optimize preview for large text files ([0603b4a](https://gitlab.unige.ch/hedera/hedera-portal/commit/0603b4aeefdcf6e9887b0d02e5d425d10c1f9e2f))
* [HEDERA-305] do not allow whitespace and special characters in file name of ResearchDataFile ([8ffdb1d](https://gitlab.unige.ch/hedera/hedera-portal/commit/8ffdb1dbbb30947baf9ce746523e3e742dc004ec))
* **access:** allow to display thumbnail on private project ([fe79897](https://gitlab.unige.ch/hedera/hedera-portal/commit/fe79897b311e4f04cebd1c41079a9ed74d873f7d))
* **access:** allow to navigate internally in app using link on research element page ([3bf1a6a](https://gitlab.unige.ch/hedera/hedera-portal/commit/3bf1a6ae2e87ef53283d1f88c73f3019d85a4b62))
* **access:** allow to run sparql query on private project when connected ([5c3ec9b](https://gitlab.unige.ch/hedera/hedera-portal/commit/5c3ec9ba1e700259221425f51230ecb84fc8dcb7))
* **access:** restore button that allow to navigate between research object and research data file ([d8c6b1d](https://gitlab.unige.ch/hedera/hedera-portal/commit/d8c6b1d02e44beb1c08635794d19b5cdf30feee7))
* **admin:** avoid error when create new person ([a52fc47](https://gitlab.unige.ch/hedera/hedera-portal/commit/a52fc4785af41df8c15dd6c5fc8d1f829869e2d6))
* allow internal redirect on research element page ([7fe1cba](https://gitlab.unige.ch/hedera/hedera-portal/commit/7fe1cba9017a13a38fbeb05c94bf2feae8d00568))
* avoid 404 error prompted on research element with project that doesn't have thumbnail ([4e0f687](https://gitlab.unige.ch/hedera/hedera-portal/commit/4e0f68752223d68e61580eac964b55826318921a))
* avoid null point exception on authorized project state ([a731647](https://gitlab.unige.ch/hedera/hedera-portal/commit/a7316477743dcc8830da9d8cd0c0f3215ebe74db))
* breadcrumb for import module ([46f2855](https://gitlab.unige.ch/hedera/hedera-portal/commit/46f28555b82195cde43b5fb5cdedb55186270768))
* display correctly sparql error from backend ([814be24](https://gitlab.unige.ch/hedera/hedera-portal/commit/814be245daaa752231cc1a923a76193e077a9f82))
* **folder-tree:** change to root folder when project change ([1669377](https://gitlab.unige.ch/hedera/hedera-portal/commit/1669377f5f0f21d69074a164a9d43223081e0ba3))
* **home detail:** [HEDERA-298] use thumbnail from project ([22e345b](https://gitlab.unige.ch/hedera/hedera-portal/commit/22e345bff30d25015760eac23ce324ab87fe1b18))
* **home detail:** add token when calling endpoints of linked data ([c878e07](https://gitlab.unige.ch/hedera/hedera-portal/commit/c878e0737085fe5c81bcb88b04adbb637bb1cf90))
* **home detail:** avoid null pointer exception on object type ([3e35859](https://gitlab.unige.ch/hedera/hedera-portal/commit/3e35859389b08c5d9a1e931ede8c1b61895b2387))
* **home detail:** display copy link button ([e34c5f2](https://gitlab.unige.ch/hedera/hedera-portal/commit/e34c5f2f9da24e58d685463c987ecf12f5322126))
* **home detail:** display manifest button for research object of type manifest ([4dda275](https://gitlab.unige.ch/hedera/hedera-portal/commit/4dda2752307a3f4338359b73fe86d6813076dd01))
* **home detail:** display objet type, download button and manifest button ([bfa0481](https://gitlab.unige.ch/hedera/hedera-portal/commit/bfa048197cac29b37aed9c5d5af8a946646b7246))
* **home detail:** manage all types of research data file to display correctly buttons ([6a4700b](https://gitlab.unige.ch/hedera/hedera-portal/commit/6a4700bfa4d5416b1dd119d90022e78ad1d367bf))
* **home:** [HEDERA-328] allow to download or consult iiif resource on private project with download token ([94f6214](https://gitlab.unige.ch/hedera/hedera-portal/commit/94f6214bb2632bfc340d4a5468bd97a3f3e3e49f))
* **home:** clean associated element when change of research element ([a5dcc46](https://gitlab.unige.ch/hedera/hedera-portal/commit/a5dcc46cd66dd4b7766232b0123b4ccbe042c0f9))
* **home:** display thumbnail on research object landing page when associated research data file exist ([f7bec7d](https://gitlab.unige.ch/hedera/hedera-portal/commit/f7bec7d0fb3f35fb56876c12336d11c2c2bf6bb8))
* **home:** fix search pagination ([321f2f6](https://gitlab.unige.ch/hedera/hedera-portal/commit/321f2f6d77cad5415e9bc087e35b4b96520ec8d6))
* **home:** manifest url ([b40d5be](https://gitlab.unige.ch/hedera/hedera-portal/commit/b40d5beba0b90ca966efea30c7510f251dede995))
* **home:** missing link injection in RDF/XML ([6308867](https://gitlab.unige.ch/hedera/hedera-portal/commit/63088674a6394a8a1160c8e9cead55b94eb4b5f1))
* **home:** use web proxy to download thumbnail image ([7a19416](https://gitlab.unige.ch/hedera/hedera-portal/commit/7a194166017ed5f83debbd5f1954d1ecf2870f90))
* **i18n:** inverted translations ([f61c171](https://gitlab.unige.ch/hedera/hedera-portal/commit/f61c171aadb15c1fac70f42d1eb4b242ba71a531))
* **import:** [HEDERA-289] display result message when apply rml ([3ef2f02](https://gitlab.unige.ch/hedera/hedera-portal/commit/3ef2f022398b3c221e17288443112001ef4212ec))
* **import:** [HEDERA-327] display put in error button on research data file detail ([dd8113b](https://gitlab.unige.ch/hedera/hedera-portal/commit/dd8113bbba6c8760d16515d842a60c996a3a01c4))
* **import:** disable file name check on frontend side ([97ac7c6](https://gitlab.unige.ch/hedera/hedera-portal/commit/97ac7c63aff37d034d8a7fd91e98041c5dd045e5))
* **import:** disable redirect on rdf tab after source dataset file transform/apply rml ([5e7b926](https://gitlab.unige.ch/hedera/hedera-portal/commit/5e7b926ba1141371ef4041263ac3085d39b3b537))
* **import:** order of button put in error ([7bce52a](https://gitlab.unige.ch/hedera/hedera-portal/commit/7bce52a2c1a752079d807d3d573ed15a475b0357))
* **ingest:** [HEDERA-192] avoid that project combobox open when change of tab ([6f9130c](https://gitlab.unige.ch/hedera/hedera-portal/commit/6f9130ca91942e1e5fb60f96f1af80eeece68639))
* **ingest:** [HEDERA-193] display upload in progress only on concerned dataset/project ([87714b8](https://gitlab.unige.ch/hedera/hedera-portal/commit/87714b8d0195f097dbef0a52f72ac6b5e68ad020))
* **ingest:** reset all state when change of project ([fe89ebb](https://gitlab.unige.ch/hedera/hedera-portal/commit/fe89ebbfc02ba3fb4b8b05660afa4ab619a099f3))
* **ingest:** wait one second after apply rml or transform before redirect to rdf dataset file page ([b1915e3](https://gitlab.unige.ch/hedera/hedera-portal/commit/b1915e3f182c667fd103d4a84910c0587f58abc2))
* lint ([dd21cac](https://gitlab.unige.ch/hedera/hedera-portal/commit/dd21cac4e72194e21742b7150e6415b6b21e71b3))
* lint ([14ea875](https://gitlab.unige.ch/hedera/hedera-portal/commit/14ea875412235ee34d7ea7bf88cab6c63dce51e6))
* **preview:** allow to preview file without extension ([88da85f](https://gitlab.unige.ch/hedera/hedera-portal/commit/88da85fc691c28e5e047b950b3731674d2be224c))
* **project space:** hide has data column on project list ([ce7e161](https://gitlab.unige.ch/hedera/hedera-portal/commit/ce7e161ab4034997dcee009b62cb9cc4602e167f))
* **project space:** use access project endpoint ([86081d9](https://gitlab.unige.ch/hedera/hedera-portal/commit/86081d9a67ee18b8f81863f0dabdfd9d231052e9))
* remove logo from source dataset ([eb0c65c](https://gitlab.unige.ch/hedera/hedera-portal/commit/eb0c65cb328d8dfdf9683b0ab5ba381e7104d332))
* remove sort from queryParameters in projectSpace children list ([e543421](https://gitlab.unige.ch/hedera/hedera-portal/commit/e54342114b83a0a846bc3185ba6a1f1d38993466))
* **search:** update search string to find similar objects on details page ([9a3a056](https://gitlab.unige.ch/hedera/hedera-portal/commit/9a3a056d22e211199aec5b8ddb45ccee67bf51fe))
* **source-dataset:** [HEDERA-197] filter format file to upload ([0e8666c](https://gitlab.unige.ch/hedera/hedera-portal/commit/0e8666cf411708dacd0ca6a5fe3aa88797173fdb))
* **table person role:** [HEDERA-234] avoid to scroll to top when use pagination ([9904c58](https://gitlab.unige.ch/hedera/hedera-portal/commit/9904c5885ee7b63681686c10b50621789c2a7128))
* use new format enum value ([5ed9021](https://gitlab.unige.ch/hedera/hedera-portal/commit/5ed90212c72b70b435a5d406a900473254c992dc))

### [0.0.3](https://gitlab.unige.ch/hedera/hedera-portal/compare/hedera-0.0.2...hedera-0.0.3) (2024-01-30)


### Features

* [HEDERA-130/154] add storage type and container in project ([95bfc88](https://gitlab.unige.ch/hedera/hedera-portal/commit/95bfc88515b74d0475d086fee6f8fe61faed68f1))
* [hedera-149] support research data file status ([d172cbb](https://gitlab.unige.ch/hedera/hedera-portal/commit/d172cbb3bda5f668cffd208da4c4ce31e4386610))
* [Hedera-151] Allow a tree structure for directory in research data files ([9c61de3](https://gitlab.unige.ch/hedera/hedera-portal/commit/9c61de3fd7a7979240a34d1755474828c3645ba2))
* [Hedera-178] add page for source dataset file ([0b995d9](https://gitlab.unige.ch/hedera/hedera-portal/commit/0b995d953bf11cbb2f421ddb1cc2d3e0837e01dd))
* [Hedera-179] detail page for RdfDatasetFile ([9e32b81](https://gitlab.unige.ch/hedera/hedera-portal/commit/9e32b81d7c557f84b444dc49f1c87222a98a99d8))
* add 'hasData' in project ([c170eeb](https://gitlab.unige.ch/hedera/hedera-portal/commit/c170eeb14fb32b71253ea6c79010538cf40476ef))
* add 'rdfType' field for research object type ([806e6e7](https://gitlab.unige.ch/hedera/hedera-portal/commit/806e6e766fd247028dc897a75d869f28d6494a22))
* add history button in RdfDatasetFile table ([734054e](https://gitlab.unige.ch/hedera/hedera-portal/commit/734054e73ab4b9e8d1afb32dc9269b9c396042f1))
* add history for source dataset files ([519a0f8](https://gitlab.unige.ch/hedera/hedera-portal/commit/519a0f8bcb97403dba85082416b64e28432bac39))
* add status history button in detail page for RdfDatasetFile ([980ca1d](https://gitlab.unige.ch/hedera/hedera-portal/commit/980ca1d4285b66672cb9dca252a9c55a822dead4))
* **dataset source:** [HEDERA-134] allow to upload zip ([5aa49c3](https://gitlab.unige.ch/hedera/hedera-portal/commit/5aa49c30136a0c84b65cb2cb1fe693eaf91f645a))
* **dataset-rdf:** [HEDERA-164] allow multiple delete ([15622e6](https://gitlab.unige.ch/hedera/hedera-portal/commit/15622e6c090242982002e6171167fd931c3c3fa6))
* **dataset:** [HEDERA-164] allow multiple apply RML ([92397b6](https://gitlab.unige.ch/hedera/hedera-portal/commit/92397b6916804518e39b7650e023b975030e1186))
* **dataset:** [HEDERA-164] allow multiple delete ([f7648b8](https://gitlab.unige.ch/hedera/hedera-portal/commit/f7648b8c36a692f616e44d6ab240247ba1918f88))
* **dataset:** display counter on tabs ([006c01e](https://gitlab.unige.ch/hedera/hedera-portal/commit/006c01edf2073dce83cd5c42c6dd6d294107318a))
* generic component for detail of dataset ([b4700d2](https://gitlab.unige.ch/hedera/hedera-portal/commit/b4700d2981ba3f03c73e9d4e148a09dcf2f3e673))
* Move imported status from SourceDatasetFile to RdfDatasetFile ([4dec9ba](https://gitlab.unige.ch/hedera/hedera-portal/commit/4dec9ba81bad3e544c30a1c4da18848b84fdd22f))
* **research data file:** display only research object type of project ([ca78aed](https://gitlab.unige.ch/hedera/hedera-portal/commit/ca78aed8fa32a608b94f1c3e98ee3f72d58c0b79))
* **research-file:** [HEDERA-173] detail page for datafile ([747eb4b](https://gitlab.unige.ch/hedera/hedera-portal/commit/747eb4bc8c8df383345277bb1e3365fcf7ba4c24))


### Bug Fixes

* [Hedera-191] show validations errors when updating ontology/rml file ([feb17b3](https://gitlab.unige.ch/hedera/hedera-portal/commit/feb17b329385a3241d93933d02c8f432ca17e683))
* adapt openapi file ([bf0d204](https://gitlab.unige.ch/hedera/hedera-portal/commit/bf0d2040d7c4c954fa07ea10d4bf52e290ee513b))
* add metadata and rdf metadata for detail page of research datafile ([6094713](https://gitlab.unige.ch/hedera/hedera-portal/commit/6094713af4fd5a96b2d2b98ab6cbfe2780d90a26))
* add missing 'datasetSourceId' paramter on 'See generated RDF' button link ([2e1f507](https://gitlab.unige.ch/hedera/hedera-portal/commit/2e1f5072d967d90f1c2e36bef00b13eb2e9981fe))
* **admin project:** [HEDERA-88] avoid to submit person without role ([4077484](https://gitlab.unige.ch/hedera/hedera-portal/commit/4077484f913a3c2f78381d7f2b2c55be7f92ee0b))
* **admin-project:** use list of project and not authorized project ([5e37957](https://gitlab.unige.ch/hedera/hedera-portal/commit/5e379579200f173398160c8a95ff28c610de61d9))
* **admin-rml:** avoid error message when upload rml ([0f4337f](https://gitlab.unige.ch/hedera/hedera-portal/commit/0f4337f403c5dff7a042ed83f4a8c9f83c1fbbbc))
* **carousel:** [HEDERA-115] update carousel content ([3fd2b83](https://gitlab.unige.ch/hedera/hedera-portal/commit/3fd2b83adaad11427a5d36c07fcfac284c8bee93))
* close dialog after apply rml ([a2f55b6](https://gitlab.unige.ch/hedera/hedera-portal/commit/a2f55b60113a00d4ad415f758a4ef3ff967cb77f))
* counter of rdf file ([576293c](https://gitlab.unige.ch/hedera/hedera-portal/commit/576293c132407f52ad88154e65fc39eb8da8b8db))
* **dataset-rdf:** display result message when import or replace dataset ([1777c3f](https://gitlab.unige.ch/hedera/hedera-portal/commit/1777c3ff32e41ab8ee93b469c0e7023b309a7024))
* **dataset-source:** sticky header ([323ffd4](https://gitlab.unige.ch/hedera/hedera-portal/commit/323ffd4c87598118e078f28d36f0c34436dd3d57))
* **dataset:** display correctly status history ([aa28a4d](https://gitlab.unige.ch/hedera/hedera-portal/commit/aa28a4d8ee15d34de10e4ca44e756cd42847478e))
* **dataset:** don't share query parameters between tabs ([819551d](https://gitlab.unige.ch/hedera/hedera-portal/commit/819551dd8f2f8ed634ab015ad3b76e9844a6185f))
* **dataset:** preserve last selected project ([8c36159](https://gitlab.unige.ch/hedera/hedera-portal/commit/8c361593a7dd98915084cc8af383d98685c58d5e))
* do not redirect to SourceDataset when deleting a RDFDatasetFile ([73740b5](https://gitlab.unige.ch/hedera/hedera-portal/commit/73740b5a2eb94667d177cfa31957701acbae0a6e))
* download action on rdf dataset file and navigation to dataset source file ([a8d8541](https://gitlab.unige.ch/hedera/hedera-portal/commit/a8d85413d76da1749103d8ea02fc9f437980d343))
* folder tree component to manage specific case ([47b3c7e](https://gitlab.unige.ch/hedera/hedera-portal/commit/47b3c7ec97f0c3137016b471601ff64f3285c942))
* **folder-tree:** target folder after deletion ([c2f38f4](https://gitlab.unige.ch/hedera/hedera-portal/commit/c2f38f4cf0d52fcc73c66e904b32b17e76f27ee1))
* **home:** [HEDERA-117] remove archive references ([a18d7ce](https://gitlab.unige.ch/hedera/hedera-portal/commit/a18d7cea29b7d42bf67231076bace1f264e6b57c))
* **i18n:** add JSON DataFileType ([f11fac9](https://gitlab.unige.ch/hedera/hedera-portal/commit/f11fac9a6eb8f5e249183e6fe39add07aedf8cc0))
* **i18n:** fix typo for relativeLocation ([1f65526](https://gitlab.unige.ch/hedera/hedera-portal/commit/1f6552608e671ed797028252dbdf0e63e4857600))
* **i18n:** typo for seeDatasetSource ([0a9f049](https://gitlab.unige.ch/hedera/hedera-portal/commit/0a9f0495621d40322cc94ea711c8c95d1415aa6d))
* **list-file:** fix source dataset file, rdf dataset file and research data file ([1d51de6](https://gitlab.unige.ch/hedera/hedera-portal/commit/1d51de64f7bdf642ea7e2bd7a2b59b9e86a6b35a))
* not able to delete SourceDataset and ResearchDataFile ([d6bba59](https://gitlab.unige.ch/hedera/hedera-portal/commit/d6bba5989e7c38ad238b814a54c6905435818b41))
* **rdf-list:** [HEDERA-183] allow to filter by source dataset file ([fc1214d](https://gitlab.unige.ch/hedera/hedera-portal/commit/fc1214d52579d4e41523b6a815a4205bc5d8a9f2))
* **rdf-list:** switch icons between add and replace in triple store ([d96b3c9](https://gitlab.unige.ch/hedera/hedera-portal/commit/d96b3c9a802c7e7ebe0284cfacad05f0815f325e))
* RdfDatasetFile list columns size ([0ef026b](https://gitlab.unige.ch/hedera/hedera-portal/commit/0ef026b9dfa203573b5743cad99cb9096eb72813))
* **RdfDatasetFile:** refresh after importing to fuseki ([b1a0cc0](https://gitlab.unige.ch/hedera/hedera-portal/commit/b1a0cc0db8ae2c53288431106ba07ce119d9f721))
* **research-file:** list not filtered correctly first time ([5242b01](https://gitlab.unige.ch/hedera/hedera-portal/commit/5242b0126243e8e0cfeb736bf98318933e1dd83e))
* **research-file:** refresh folder list when change of project ([1351d2c](https://gitlab.unige.ch/hedera/hedera-portal/commit/1351d2ca755df92cdc655685cfb4a8b958dbd4d6))
* **ResearchDataFileDetail:** hide metadata components when preview is in fullscreen ([04ef186](https://gitlab.unige.ch/hedera/hedera-portal/commit/04ef1868ff0d625c50512b07a2de76f94f4b58ea))
* **ResearchDataFile:** fix style ([d35fb63](https://gitlab.unige.ch/hedera/hedera-portal/commit/d35fb63816b64225cc6d84d78535efb09c38dbea))
* **source-dataset:** allow to visualize uploaded dataset logo ([868c83c](https://gitlab.unige.ch/hedera/hedera-portal/commit/868c83c7df1902118b5c454f9cb5af58b53d6097))
* **source-dataset:** back to detail, disable last import date and color status ([ddfe062](https://gitlab.unige.ch/hedera/hedera-portal/commit/ddfe0624403abd3a2b9cecc471b1c2314c0dcac0))
* **source-dataset:** error on logo when update ([9c76cbf](https://gitlab.unige.ch/hedera/hedera-portal/commit/9c76cbfd05463cc3ac0b54f33b7b5331c9ddc203))
* **source-dataset:** upload file dialog init type ([e1f2529](https://gitlab.unige.ch/hedera/hedera-portal/commit/e1f25298e460f1c14cb0ba2c96ff9f4c87e498b7))
* **SourceDataFile:** [Hedera-187] Refresh once apply rml ([27d09ec](https://gitlab.unige.ch/hedera/hedera-portal/commit/27d09ec4375ccd39a6bd53a9f34814ac607b643c))
* update status translation and color ([dba3e9f](https://gitlab.unige.ch/hedera/hedera-portal/commit/dba3e9fefaf84c0845d6bde2f19f670726682406))

### [0.0.2](https://gitlab.unige.ch/hedera/hedera-portal/compare/hedera-0.0.1...hedera-0.0.2) (2023-12-13)


### Features

* [HEDERA-104] add access public property to project ([d219796](https://gitlab.unige.ch/hedera/hedera-portal/commit/d219796e04ea1a61c752942ca77007c3be541394))
* [HEDERA-121] rename research object to research object type ([1d843e1](https://gitlab.unige.ch/hedera/hedera-portal/commit/1d843e1bb758262bfce9d726f219f3da0700c282))
* [HEDERA-131] Manage SourceDataset and SourceDatasetFile ([351346e](https://gitlab.unige.ch/hedera/hedera-portal/commit/351346e60971a65ad9602c1e467500cd685d10e0))
* add column in list of source and research data file ([70b62ee](https://gitlab.unige.ch/hedera/hedera-portal/commit/70b62ee69e1de32f8de1c3d28bf2b9d7865bf94b))
* add page to test app colors ([5774ff2](https://gitlab.unige.ch/hedera/hedera-portal/commit/5774ff21cf67c11730e3804d461d6ba1ed8ad0bb))
* added tab to list all rdf dataset file ([60f8bc8](https://gitlab.unige.ch/hedera/hedera-portal/commit/60f8bc830ed9ef6c11ef5bff4a84ebf6c57db381))
* **admin:** add base URI field to Ontology ([a7b81f0](https://gitlab.unige.ch/hedera/hedera-portal/commit/a7b81f09fc403cff631137bfdd6bd2d8c4e7a24b))
* **admin:** add new field in research object type ([abe0a4d](https://gitlab.unige.ch/hedera/hedera-portal/commit/abe0a4d11a6a63708ee5d694b2ab4afa15fa709b))
* **dataset:** allow to select insert mode and triple store dataset id ([4582095](https://gitlab.unige.ch/hedera/hedera-portal/commit/4582095d4ac01e3c8d7ceeda225dd970329cbb78))
* **dataset:** allow to submit RDF in triple store ([703b362](https://gitlab.unige.ch/hedera/hedera-portal/commit/703b3623e5a481dab68fe62054cb745bb398e4bd))
* **dataset:** finalized integration for RDF publication in triple store ([649a55e](https://gitlab.unige.ch/hedera/hedera-portal/commit/649a55e700bf57d6195003e6f07082cc4b1a54e0))
* detail and edit SourceDataset pages take full page ([f94f9c5](https://gitlab.unige.ch/hedera/hedera-portal/commit/f94f9c5e0942c6f99d99b6cbd7baac88fa759d11))
* display progress bar when upload file ([b4b3ac4](https://gitlab.unige.ch/hedera/hedera-portal/commit/b4b3ac4f1d3300c8807276aef3a6e8fd3ea6f14d))
* display research data file ([c99e775](https://gitlab.unige.ch/hedera/hedera-portal/commit/c99e7750aa0b60f4644df1f9f0f7c2d5a1a3b39e))
* ontology ([5bcc449](https://gitlab.unige.ch/hedera/hedera-portal/commit/5bcc44908b95ede73f9d821563f5dda9add94255))
* **research data file:** allow to download or preview research data file ([a4d39c2](https://gitlab.unige.ch/hedera/hedera-portal/commit/a4d39c24f12293d508d9368f1fa4ee50e06ec5b3))
* schema for RdfDatasetFile module ([1e15fe5](https://gitlab.unige.ch/hedera/hedera-portal/commit/1e15fe5bb2784682ac4a72039e96d802cb4eb653))
* schema for RdfDatasetFile module ([07bc4b5](https://gitlab.unige.ch/hedera/hedera-portal/commit/07bc4b5b542af240decaf23692b089c40e72a8f4))
* **source dataset:** allow to apply RML ([100bdfc](https://gitlab.unige.ch/hedera/hedera-portal/commit/100bdfc232326636d1b30df8b49fccae3a40ee98))
* **source dataset:** allow to manage source datafile ([c4fb542](https://gitlab.unige.ch/hedera/hedera-portal/commit/c4fb542aa1c5cd4cb3389d4cacb8b52621b0ad52))
* update to solidify 5.0.0-beta24 to improve performance ([18eb5dc](https://gitlab.unige.ch/hedera/hedera-portal/commit/18eb5dc2d594e16acd624297444877e9c794e1da))


### Bug Fixes

* [hedera-100] add link between ontology and research object type ([917fb82](https://gitlab.unige.ch/hedera/hedera-portal/commit/917fb820a19434adc49bb18f8bee8ffa8f146f6a))
* [HEDERA-107] rename ontology file property ([8873102](https://gitlab.unige.ch/hedera/hedera-portal/commit/8873102244b267027febacd5551b257f7c84b00e))
* [HEDERA-131] fix SourceDataset logo ([8f9ee85](https://gitlab.unige.ch/hedera/hedera-portal/commit/8f9ee8587032f5190ad34cb3a20676969472a5ab))
* allow to delete rdf dataset ([bcc043b](https://gitlab.unige.ch/hedera/hedera-portal/commit/bcc043b3cc381746cc6d57baea2367c1df1fd0b6))
* change project when selecting one in the dataset combobox ([ae7857d](https://gitlab.unige.ch/hedera/hedera-portal/commit/ae7857d8ba6b71d052f1cabc56e26baa57f38c73))
* **dataset:** avoid multiple call to backend endpoint ([0fe9dd2](https://gitlab.unige.ch/hedera/hedera-portal/commit/0fe9dd24465035b337a6a7b5f21c0a8c9fd3eafa))
* **dataset:** lose detail of project when F5 ([caaa1c5](https://gitlab.unige.ch/hedera/hedera-portal/commit/caaa1c50db97ace09e0b97dcd0e9e6ad6b7c65f8))
* **dataset:** provide project id as tripleStoreId ([3bba6d8](https://gitlab.unige.ch/hedera/hedera-portal/commit/3bba6d84b5dc34eedf0d6cae35bc8cb32d47eeb8))
* **home:** [HEDERA-116] remove swiss university logo in funding organization ([5873722](https://gitlab.unige.ch/hedera/hedera-portal/commit/58737222b6b7e3787d0af141cfafedb321435099))
* **i18n:** fix some french labels ([b7fc73d](https://gitlab.unige.ch/hedera/hedera-portal/commit/b7fc73de6ceeef2a901d0ea00b3e1924e0d72377))
* not need Memoized.selectBase for dataset state ([2a9c9fd](https://gitlab.unige.ch/hedera/hedera-portal/commit/2a9c9fd08555edf78566faacfb77685523385a05))
* take previous hedera-api ([9128715](https://gitlab.unige.ch/hedera/hedera-portal/commit/9128715ac775ca314aad35424c1c16566148a721))

### 0.0.1 (2023-11-01)


### Features

* [HEDERA-50] administration for institutions ([8ab2a74](https://gitlab.unige.ch/hedera/hedera-portal/commit/8ab2a747484c8997f2cb280b403a0ce51ec278ac))
* [Hedera-54] manage ontologies ([b5b84d2](https://gitlab.unige.ch/hedera/hedera-portal/commit/b5b84d2fd4a644c82295d2932147f12ad4f3ce41))
* [hedera-61] manage rml ([188b19e](https://gitlab.unige.ch/hedera/hedera-portal/commit/188b19ebec8051541e7f9625596a03b169e32eab))
* [HEDERA-68] administration for people ([2e73d39](https://gitlab.unige.ch/hedera/hedera-portal/commit/2e73d397d6b589ae5a846b28dbc42e3c5e20e96f))
* [hedera-75] start using openApi ([0ac049d](https://gitlab.unige.ch/hedera/hedera-portal/commit/0ac049d457e6893dc1881eba40f3096eb5a77919))
* [hedera-84] add file to ontology panel ([7b88be8](https://gitlab.unige.ch/hedera/hedera-portal/commit/7b88be80202b74a9e52c7f74929664b12c2ca461))
* [HEDERA-87] relation between project and rml ([54eac48](https://gitlab.unige.ch/hedera/hedera-portal/commit/54eac489c89c385e86461fcd24f1c08372fac95c))
* [HEDERA-90] relation between project and research object ([b9c3aeb](https://gitlab.unige.ch/hedera/hedera-portal/commit/b9c3aebab9073c0a82e26b718f8ade140561181a))
* **admin project:** [hedera-70-52] manage project and project role ([8455d18](https://gitlab.unige.ch/hedera/hedera-portal/commit/8455d18e0e189c1a82d26cde899bb90c4b5cff7b))
* display swagger apis in footer menu ([36afbc0](https://gitlab.unige.ch/hedera/hedera-portal/commit/36afbc0276c32e1c47be324ddc51c570a5ed44c3))
* research object ([951736e](https://gitlab.unige.ch/hedera/hedera-portal/commit/951736ead870b19b0b29a22f19b8747c4090c002))


### Bug Fixes

* [HEDERA-83] Changes in project role are not reflected in the list ([3e77289](https://gitlab.unige.ch/hedera/hedera-portal/commit/3e77289468554831fff5d0fe13a6424303bc3b3d))
* [HEDERA-89] research object sparql textarea should no contain a vertical scroll ([9077fac](https://gitlab.unige.ch/hedera/hedera-portal/commit/9077fac1f9a0e2845117f7f2cde0db13a9425181))
* add placeholder for adding rml in project form ([d8db96a](https://gitlab.unige.ch/hedera/hedera-portal/commit/d8db96a46ed2e7f70649b4cf2c808043414921ce))
* **admin home:** rml tile should be visible for admins ([08aea8e](https://gitlab.unige.ch/hedera/hedera-portal/commit/08aea8e8fe2b6deb0c8c1b45107e90410a03c4eb))
* **amin project:** fix problem with pagination in project panel ([c2073bd](https://gitlab.unige.ch/hedera/hedera-portal/commit/c2073bd3b628950d06ed8d6ff4cfb5a7e8c5784c))
* avoid error at startup ([6543fbc](https://gitlab.unige.ch/hedera/hedera-portal/commit/6543fbc056daeb38137323bb70c8d894fa50d488))
* disable initial critical css in SSR ([6058b2c](https://gitlab.unige.ch/hedera/hedera-portal/commit/6058b2c6c5f07acf4cbcccfc6a8c16926a37c8b3))
* disable warning in SSR ([8530c8f](https://gitlab.unige.ch/hedera/hedera-portal/commit/8530c8fbe0882a239fccd4cfc39e881ccaa2f57c))
* MR ([f55e5a2](https://gitlab.unige.ch/hedera/hedera-portal/commit/f55e5a2ffd1dfd1358b33c10d650d17afc05b931))
* remove authorized institution and institution member notion ([a81821e](https://gitlab.unige.ch/hedera/hedera-portal/commit/a81821e1be0a24832507da394a7b5a01e2deb753))
* update copyright date ([6f482ab](https://gitlab.unige.ch/hedera/hedera-portal/commit/6f482ab2d6d12839b71560a83e113e82fb2f72e3))

## 0.0.0 (2019-06-24)

### Features

* **hedera-portal:** add oauth2 code flow ([1d7ad2e](https://gitlab.unige.ch///commit/1d7ad2e))
* **hedera-portal:** add refresh token flow ([cad9997](https://gitlab.unige.ch///commit/cad9997)), closes [#hedera-595](https://gitlab.unige.ch///issues/hedera-595)

### Tests

* **hedera-portal:** correct jasmine tests ([17433d3](https://gitlab.unige.ch///commit/17433d3))
