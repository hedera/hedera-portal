module.exports = {
    extends: [
        "./node_modules/solidify-frontend/.eslintrc.cjs",
    ],
    ignorePatterns: [
        "node_modules/**/*",
        "scripts/**/*",
        "src/app/generated-api",
        "src/environments/environment.local.ts",
        "src/server-environments/server-environment.local.ts",
        "proxy.conf.*"
    ],
    overrides: [
        {
            files: [
                "*.ts"
            ],
            parserOptions: {
                tsconfigRootDir: __dirname,
                project: [
                    "tsconfig.json",
                    "e2e/tsconfig.json"
                ],
                createDefaultProgram: true
            },
            extends: [],
            plugins: [],
            rules: {
                "@angular-eslint/component-selector": [
                    "error",
                    {
                        "type": "element",
                        "prefix": "hedera",
                        "style": "kebab-case"
                    }
                ],
                "@angular-eslint/directive-selector": [
                    "error",
                    {
                        "type": "attribute",
                        "prefix": "hedera",
                        "style": "camelCase"
                    }
                ],
                "solidify/copyright-header": ["error",
                    {
                        "file": "copyright-header.js",
                        "inceptionYear": "2022",
                    }
                ],
            }
        },
        {
            files: [
                "*.html"
            ],
            extends: [],
            rules: {}
        }
    ]
}
